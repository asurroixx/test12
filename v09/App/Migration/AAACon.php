<?php

namespace App\Controllers;

require dirname(__DIR__) . '/../vendor/autoload.php';

use \Core\View;
use App\Models\StaffMo;
use App\Models\MarketMo;
use App\Models\ManagerMo;
use App\Models\MarketKeyMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class AAACon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

    //
	// public function Render($data=null)
	// {
	// 	View::renderTemplate('page/staff/staff.html');
	// }

	//staff.html 데이터테이블 리스트 로드

    public function __construct() {
        $this::MigrationMarketManager();
    }



    public function MigrationPartner()
    {
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A_ID,
        A_NAME,
        A_CODE
        FROM ebuyyy.AA_AGENT_PARTNER
        WHERE A_STAT=0
        ");
        $query->execute();
        $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

        //벌크인서트 본 서버 partner
        $dbName= self::MainDBName;
        $createTime=date('Y-m-d H:i:s');
        $query = "INSERT INTO $dbName.Partner (code, name, createTime, migrationIDX) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['A_ID'];
            $bulkString[] = "(:code".$idx.", :name".$idx.", :createTime, :migrationIDX".$idx.")";
        }

        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);

        // 바인딩 변수 설정
        foreach ($migrationResult as $key2) {
            $pIDX=$key2['A_ID'];
            $pName=$key2['A_NAME'];
            $pCode=$key2['A_CODE'];

            $result->bindValue(":code" . $pIDX, $pCode);
            $result->bindValue(":name" . $pIDX, $pName);
            $result->bindValue(":migrationIDX" . $pIDX, $pIDX);
        }
        $result->bindValue(":createTime", $createTime);
        $result->execute();
        // 벌크인서트 본 서버 partner

        //벌크인서트 샌드박스 서버 partner

        $sandboxDb = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $query = "INSERT INTO $dbName.Partner (code, name, createTime, migrationIDX) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['A_ID'];
            $bulkString[] = "(:code".$idx.", :name".$idx.", :createTime, :migrationIDX".$idx.")";
        }

        $query .= implode(", ", $bulkString);
        $result2 = $sandboxDb->prepare($query);

        // 바인딩 변수 설정
        foreach ($migrationResult as $key2) {
            $pIDX=$key2['A_ID'];
            $pName=$key2['A_NAME'];
            $pCode=$key2['A_CODE'];

            $result2->bindValue(":code" . $pIDX, $pCode.'_SANDBOX');
            $result2->bindValue(":name" . $pIDX, $pName);
            $result2->bindValue(":migrationIDX" . $pIDX, $pIDX);
        }
        $result2->bindValue(":createTime", $createTime);
        $result2->execute();
        // 벌크인서트 샌드박스 서버 partner

        $resultData = ['data'=>$result,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function MigrationMarket()
    {
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.M_ID AS idx,
        A.M_CODE AS code,
        A.M_NAME AS name,
        CASE A.stat
            WHEN 1 THEN 412101
            WHEN 2 THEN 412201
            ELSE 412202 END
        AS statusIDX,
        B.A_MANAGER_CALL AS phoneNumber,
        B.A_FEE_PER_PARTNER AS marketDepositFeePer,
        0 AS marketWithdrawalFeePer,
        0 AS marketSettleBankwireFeePer,
        B.feePerAgentPayCrypto AS marketSettleCryptoFeePer,
        B.feePerReversePayCrypto AS marketTopupCryptoFeePer,
        0 AS marketTopupBankwireFeePer,
        B.minusLimit AS marketMinusLimitAmount,
        B.A_CHARGE_MIN AS marketDepositMinAmount,
        B.A_CHARGE_MAX AS marketDepositMaxAmount,
        C.idx AS partnerIDX
        FROM ebuyyy.AA_AGENT_PARTNER_MARKET AS A
        JOIN ebuyyy.AA_AGENT_PARTNER AS B ON A.M_P_ID = B.A_ID
        JOIN ebuy.Partner AS C ON C.migrationIDX = B.A_ID
        WHERE B.A_STAT=0 AND A.stat =1
        ");
        $query->execute();
        $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

        //-------------------------------벌크인서트 본 서버 마켓 추가---------------------------------
        $dbName= self::MainDBName;

        // 없는컬럼
        $categoryIDX=1;
        $depositStatusIDX=413101;
        $withdrawalStatusIDX=414101;
        $hostUrl='';
        $serviceUrl='';
        $createTime=date('Y-m-d H:i:s');

        $query = "INSERT INTO $dbName.Market (
            code,
            name,
            partnerIDX,
            categoryIDX,
            statusIDX,
            depositStatusIDX,
            withdrawalStatusIDX,
            phoneNumber,
            hostUrl,
            serviceUrl,
            marketDepositFeePer,
            marketWithdrawalFeePer,
            marketSettleBankwireFeePer,
            marketSettleCryptoFeePer,
            marketTopupCryptoFeePer,
            marketTopupBankwireFeePer,
            marketMinusLimitAmount,
            marketDepositMinAmount,
            marketDepositMaxAmount,
            createTime,
            migrationIDX
        ) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['idx'];
            $bulkString[] = "(
            :code".$idx.",
            :name".$idx.",
            :partnerIDX".$idx.",
            :categoryIDX,
            :statusIDX".$idx.",
            :depositStatusIDX,
            :withdrawalStatusIDX,
            :phoneNumber".$idx.",
            :hostUrl,
            :serviceUrl,
            :marketDepositFeePer".$idx.",
            :marketWithdrawalFeePer".$idx.",
            :marketSettleBankwireFeePer".$idx.",
            :marketSettleCryptoFeePer".$idx.",
            :marketTopupCryptoFeePer".$idx.",
            :marketTopupBankwireFeePer".$idx.",
            :marketMinusLimitAmount".$idx.",
            :marketDepositMinAmount".$idx.",
            :marketDepositMaxAmount".$idx.",
            :createTime,
            :migrationIDX".$idx."
            )";
        }

        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);

        // 바인딩 변수 설정
        foreach ($migrationResult as $key2) {
            $idx=$key2['idx'];
            $code=$key2['code'];
            $name=$key2['name'];
            $partnerIDX = $key2['partnerIDX'];
            $statusIDX=$key2['statusIDX'];
            $phoneNumber=$key2['phoneNumber'];
            $marketDepositFeePer=$key2['marketDepositFeePer'];
            $marketWithdrawalFeePer=$key2['marketWithdrawalFeePer'];
            $marketSettleBankwireFeePer=$key2['marketSettleBankwireFeePer'];
            $marketSettleCryptoFeePer=$key2['marketSettleCryptoFeePer'];
            $marketTopupCryptoFeePer=$key2['marketTopupCryptoFeePer'];
            $marketTopupBankwireFeePer=$key2['marketTopupBankwireFeePer'];
            $marketMinusLimitAmount=$key2['marketMinusLimitAmount'];
            $marketDepositMinAmount=$key2['marketDepositMinAmount'];
            $marketDepositMaxAmount=$key2['marketDepositMaxAmount'];

            $result->bindValue(":code" . $idx, $code);
            $result->bindValue(":name" . $idx, $name);
            $result->bindValue(":partnerIDX" . $idx, $partnerIDX);
            $result->bindValue(":statusIDX" . $idx, $statusIDX);
            $result->bindValue(":phoneNumber" . $idx, $phoneNumber);
            $result->bindValue(":marketDepositFeePer" . $idx, $marketDepositFeePer);
            $result->bindValue(":marketWithdrawalFeePer" . $idx, $marketWithdrawalFeePer);
            $result->bindValue(":marketSettleBankwireFeePer" . $idx, $marketSettleBankwireFeePer);
            $result->bindValue(":marketSettleCryptoFeePer" . $idx, $marketSettleCryptoFeePer);
            $result->bindValue(":marketTopupCryptoFeePer" . $idx, $marketTopupCryptoFeePer);
            $result->bindValue(":marketTopupBankwireFeePer" . $idx, $marketTopupBankwireFeePer);
            $result->bindValue(":marketMinusLimitAmount" . $idx, $marketMinusLimitAmount);
            $result->bindValue(":marketDepositMinAmount" . $idx, $marketDepositMinAmount);
            $result->bindValue(":marketDepositMaxAmount" . $idx, $marketDepositMaxAmount);
            $result->bindValue(":migrationIDX" . $idx, $idx);
        }


        $result->bindValue(":categoryIDX", $categoryIDX);
        $result->bindValue(":depositStatusIDX", $depositStatusIDX);
        $result->bindValue(":withdrawalStatusIDX", $withdrawalStatusIDX);
        $result->bindValue(":hostUrl", $hostUrl);
        $result->bindValue(":serviceUrl", $serviceUrl);
        $result->bindValue(":createTime", $createTime);
        $result->execute();

        //-------------------------------벌크인서트 본 서버 마켓 추가---------------------------------
        $ipAddress  = $this->GetIPaddress();


        //------------------------------------------------샌드박스 마켓 벌크인서트--------------------------------
        $sandboxDb = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;

        $query = "INSERT INTO $dbName.Market (
            targetMarketIDX,
            code,
            name,
            partnerIDX,
            categoryIDX,
            statusIDX,
            depositStatusIDX,
            withdrawalStatusIDX,
            phoneNumber,
            hostUrl,
            serviceUrl,
            marketDepositFeePer,
            marketWithdrawalFeePer,
            marketSettleBankwireFeePer,
            marketSettleCryptoFeePer,
            marketTopupCryptoFeePer,
            marketTopupBankwireFeePer,
            marketMinusLimitAmount,
            marketDepositMinAmount,
            marketDepositMaxAmount,
            createTime,
            migrationIDX
        ) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['idx'];
            $bulkString[] = "(
            :targetMarketIDX".$idx.",
            :code".$idx.",
            :name".$idx.",
            :partnerIDX".$idx.",
            :categoryIDX,
            :statusIDX".$idx.",
            :depositStatusIDX,
            :withdrawalStatusIDX,
            :phoneNumber".$idx.",
            :hostUrl,
            :serviceUrl,
            :marketDepositFeePer".$idx.",
            :marketWithdrawalFeePer".$idx.",
            :marketSettleBankwireFeePer".$idx.",
            :marketSettleCryptoFeePer".$idx.",
            :marketTopupCryptoFeePer".$idx.",
            :marketTopupBankwireFeePer".$idx.",
            :marketMinusLimitAmount".$idx.",
            :marketDepositMinAmount".$idx.",
            :marketDepositMaxAmount".$idx.",
            :createTime,
            :migrationIDX".$idx."
            )";
        }

        $query .= implode(", ", $bulkString);
        $result2 = $sandboxDb->prepare($query);

        $EbuySandboxApiDBName= self::EbuySandboxApiDBName;
        // 바인딩 변수 설정
        foreach ($migrationResult as $key2) {
            $idx=$key2['idx'];
            $code=$key2['code'];
            $name=$key2['name'];
            $partnerIDX = $key2['partnerIDX'];
            $statusIDX=$key2['statusIDX'];
            $phoneNumber=$key2['phoneNumber'];
            $marketDepositFeePer=$key2['marketDepositFeePer'];
            $marketWithdrawalFeePer=$key2['marketWithdrawalFeePer'];
            $marketSettleBankwireFeePer=$key2['marketSettleBankwireFeePer'];
            $marketSettleCryptoFeePer=$key2['marketSettleCryptoFeePer'];
            $marketTopupCryptoFeePer=$key2['marketTopupCryptoFeePer'];
            $marketTopupBankwireFeePer=$key2['marketTopupBankwireFeePer'];
            $marketMinusLimitAmount=$key2['marketMinusLimitAmount'];
            $marketDepositMinAmount=$key2['marketDepositMinAmount'];
            $marketDepositMaxAmount=$key2['marketDepositMaxAmount'];

            $result2->bindValue(":targetMarketIDX" . $idx, $idx);
            $result2->bindValue(":code" . $idx, $code.'_SANDBOX');
            $result2->bindValue(":name" . $idx, $name);
            $result2->bindValue(":partnerIDX" . $idx, $partnerIDX);
            $result2->bindValue(":statusIDX" . $idx, $statusIDX);
            $result2->bindValue(":phoneNumber" . $idx, $phoneNumber);
            $result2->bindValue(":marketDepositFeePer" . $idx, $marketDepositFeePer);
            $result2->bindValue(":marketWithdrawalFeePer" . $idx, $marketWithdrawalFeePer);
            $result2->bindValue(":marketSettleBankwireFeePer" . $idx, $marketSettleBankwireFeePer);
            $result2->bindValue(":marketSettleCryptoFeePer" . $idx, $marketSettleCryptoFeePer);
            $result2->bindValue(":marketTopupCryptoFeePer" . $idx, $marketTopupCryptoFeePer);
            $result2->bindValue(":marketTopupBankwireFeePer" . $idx, $marketTopupBankwireFeePer);
            $result2->bindValue(":marketMinusLimitAmount" . $idx, $marketMinusLimitAmount);
            $result2->bindValue(":marketDepositMinAmount" . $idx, $marketDepositMinAmount);
            $result2->bindValue(":marketDepositMaxAmount" . $idx, $marketDepositMaxAmount);
            $result2->bindValue(":migrationIDX" . $idx, $idx);
        }
        $result2->bindValue(":categoryIDX", $categoryIDX);
        $result2->bindValue(":depositStatusIDX", $depositStatusIDX);
        $result2->bindValue(":withdrawalStatusIDX", $withdrawalStatusIDX);
        $result2->bindValue(":hostUrl", $hostUrl);
        $result2->bindValue(":serviceUrl", $serviceUrl);
        $result2->bindValue(":createTime", $createTime);
        $result2->execute();
        //------------------------------------------------샌드박스 마켓 벌크인서트--------------------------------


        //------------------------------------------------샌드박스 마켓 키 발급--------------------------------
        function ApiKeyString($length)  {
            $characters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            $string_generated = "";
            $nmr_loops = $length;
            while ($nmr_loops--){
                $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            $issetString=MarketKeyMo::MarketkApiKeyChkByMarketKey($string_generated);
            if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
                return ApiKeyString($length);
            }else{
                return $string_generated;
            }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
        }

        foreach ($migrationResult as $key) {
            $marketCode=$key['code'].'_SANDBOX';
            $marketApiKey=ApiKeyString(64);//회사코드는 10자리
            $stat5=$sandboxDb->prepare("INSERT INTO $EbuySandboxApiDBName.MarketKey
                (marketCode,marketKey,ip,statusIDX,managerIDX,createTime)
                VALUES
                (:marketCode,:marketKey,:ip,:statusIDX,:managerIDX,:createTime)
            ");
            $stat5->bindValue(':marketCode', $marketCode);
            $stat5->bindValue(':marketKey', $marketApiKey);//임시
            $stat5->bindValue(':ip', $ipAddress);
            $stat5->bindValue(':statusIDX', 429101);
            $stat5->bindValue(':managerIDX', 0);
            $stat5->bindValue(':createTime', $createTime);
            $stat5->execute();
        }
        //------------------------------------------------샌드박스 마켓 키 발급--------------------------------

        //------------------모든 샌드박스 마켓 발란스 증가 1,000,000 $ ---------------------------------
        $marketBalance=$this->SandBoxAllMarketBalanceUp();
        //------------------모든 샌드박스 마켓 발란스 증가 1,000,000 $ ---------------------------------





        $resultData = ['data'=>$result,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function SandBoxAllMarketBalanceUp()
    {
        $db = static::GetSandboxDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        idx
        FROM $dbName.Market
        ");
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        $createTime=date('Y-m-d H:i:s');

        foreach ($result as $key) {
            $idx=$key['idx'];
            $stat8=$db->prepare("INSERT INTO $dbName.MarketBalance
                 (marketIDX,statusIDX,targetIDX,amount,createTime)
                 VALUES
                 (:marketIDX,:statusIDX,:targetIDX,:amount,:createTime)
             ");

             $stat8->bindValue(':marketIDX', $idx);
             $stat8->bindValue(':statusIDX', 419101);
             $stat8->bindValue(':targetIDX', 0);
             $stat8->bindValue(':amount', 1000000);
             $stat8->bindValue(':createTime', $createTime);
             $stat8->execute();
        }

    }

    public function MigrationMarketManager(){
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        A.email_address AS email,
        A.user_name AS name,
        A.partner_lev AS gradeIDX,
        A.partner_position AS position,
        A.last_login,
        A.partner_code,
        B.idx AS partnerIDX,
        C.idx AS marketIDX
        FROM ebuyyy.AA_SIGNUP AS A
        LEFT JOIN ebuy.Partner AS B ON A.partner_id = B.migrationIDX
        LEFT JOIN ebuy.Market AS C ON B.idx = C.partnerIDX
        WHERE A.partner_lev = 1 AND A.status = 0
        AND A.email_address !='asurroixx@gmail.com' AND A.email_address !='mo@hsmcloudipia.com' AND B.idx >0
        GROUP BY C.idx ORDER BY A.last_login DESC
        ");
        $query->execute();
        $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

        $ipAddress = $this->GetIPaddress();
        $createTime=date("Y-m-d H:i:s");
        $gradeIDX=1;
        $statusIDX=402101;

        $dataDbKey=self::dataDbKey;
        $dbName= self::MainDBName;


        //-------------------마켓매니저------------------------------

        $query = "INSERT INTO $dbName.MarketManager (
            name,
            marketIDX,
            managerIDX,
            gradeIDX,
            statusIDX,
            position,
            createTime,
            migrationIDX
        ) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['marketIDX'];
            $bulkString[] = "(
            AES_ENCRYPT(:name".$idx.", :dataDbKey),
            :marketIDX".$idx.",
            :managerIDX".$idx.",
            :gradeIDX,
            :statusIDX,
            :position".$idx.",
            :createTime,
            :migrationIDX".$idx."
        )";
        }

        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);

        // 바인딩 변수 설정
        foreach ($migrationResult as $key2) {
            $idx=$key2['marketIDX'];
            $name=$key2['name'];
            $marketIDX=$key2['marketIDX'];
            $position=$key2['position'];

            $email=$key2['email'];
            $issetEmailData=ManagerMo::IssetManagerEmail($email);
            if(isset($issetEmailData['idx'])){
                $managerIDX=$issetEmailData['idx'];
            }else{

                $createTime=date("Y-m-d H:i:s");
                $stat1=$db->prepare("INSERT INTO $dbName.Manager
                    (email,ipAddress,createTime,migrationIDX)
                    VALUES
                    (AES_ENCRYPT(:email, '$dataDbKey'),:ipAddress,:createTime,:migrationIDX)
                ");
                $stat1->bindValue(':email', $email);
                $stat1->bindValue(':ipAddress', $ipAddress);
                $stat1->bindValue(':createTime', $createTime);
                $stat1->bindValue(':migrationIDX', $idx);
                $stat1->execute();
                $managerIDX = $db->lastInsertId();
            }
            $result->bindValue(":name" . $idx, $name);
            $result->bindValue(":marketIDX" . $idx, $marketIDX);
            $result->bindValue(":managerIDX" . $idx, $managerIDX);
            $result->bindValue(":position" . $idx, $position);
            $result->bindValue(":migrationIDX" . $idx, $idx);
        }
        $result->bindValue(":gradeIDX", $gradeIDX);
        $result->bindValue(":statusIDX", $statusIDX);
        $result->bindValue(":createTime", $createTime);
        $result->bindValue(":dataDbKey", $dataDbKey);
        $result->execute();

        //-------------------마켓매니저------------------------------

        //-------------------------------샌드박스 마켓매니저------------------------------------------

        $dbsand = static::GetSandboxDB();
        $dbsand2 = static::GetSandboxDB();

        $dbNameSand= self::EbuySandboxDBName;

        $query = "INSERT INTO $dbName.MarketManager (
            name,
            marketIDX,
            managerIDX,
            gradeIDX,
            statusIDX,
            position,
            createTime,
            migrationIDX
        ) VALUES ";
        $bulkString = [];

        foreach ($migrationResult as $key) {
            $idx=$key['marketIDX'];
            $bulkString[] = "(
            AES_ENCRYPT(:name".$idx.", :dataDbKey),
            :marketIDX".$idx.",
            :managerIDX".$idx.",
            :gradeIDX,
            :statusIDX,
            :position".$idx.",
            :createTime,
            :migrationIDX".$idx."
        )";
        }

        $query .= implode(", ", $bulkString);
        $result2 = $dbsand->prepare($query);

        foreach ($migrationResult as $key) {
            $idx=$key['marketIDX'];
            $name=$key['name'];
            $marketIDX=$key['marketIDX'];
            $position=$key['position'];
            $email=$key['email'];


            $email=trim($email);
            $Sel = $dbsand->prepare("SELECT
            idx,
            AES_DECRYPT(email,:dataDbKey) AS email
            FROM $dbNameSand.Manager
            WHERE AES_DECRYPT(email,:dataDbKey)=:email
            ");
            $Sel->bindValue(':dataDbKey', $dataDbKey);
            $Sel->bindValue(':email', $email);
            $Sel->execute();
            $adminList=$Sel->fetch(PDO::FETCH_ASSOC);

            if(isset($adminList['idx'])){
                $managerIDX = $adminList['idx'];
            }else{
                 $query=$dbsand2->prepare("INSERT INTO $dbNameSand.Manager (email,ipAddress, createTime,migrationIDX)
                   VALUES (AES_ENCRYPT(:email, '$dataDbKey'), :ipAddress, :createTime , :migrationIDX)
                ");
                $query->bindValue(':email', $email);
                $query->bindValue(':ipAddress', $ipAddress);
                $query->bindValue(':createTime', $createTime);
                $query->bindValue(':migrationIDX', $idx);
                $query->execute();
                $managerIDX=$dbsand2->lastInsertId();
            }


            $result2->bindValue(":name" . $idx, $name);
            $result2->bindValue(":marketIDX" . $idx, $marketIDX);
            $result2->bindValue(":managerIDX" . $idx, $managerIDX);
            $result2->bindValue(":position" . $idx, $position);
            $result2->bindValue(":migrationIDX" . $idx, $idx);

            // $managerDupleChkInSand = ManagerMo::issetEmailInSand($email);
            // $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($marketIDX);
            // if(isset($marketIDXInSandboxSel['idx'])){
            //     $sandboxMarketIDX = $marketIDXInSandboxSel['idx'];
            //     if(!isset($managerDupleChkInSand['idx'])){
            //         $query=$dbsand->prepare("INSERT INTO $dbNameSand.Manager (email,ipAddress, createTime,migrationIDX)
            //            VALUES (AES_ENCRYPT(:email, '$dataDbKey'), :ipAddress, :createTime , :migrationIDX)
            //         ");
            //         $query->bindValue(':email', $email);
            //         $query->bindValue(':ipAddress', $ipAddress);
            //         $query->bindValue(':createTime', $createTime);
            //         $query->bindValue(':migrationIDX', $idx);
            //         $query->execute();
            //         $managerIDX=$dbsand->lastInsertId();
            //     }else{
            //         $managerIDX=$managerDupleChkInSand['idx'];
            //     }
            // }
            // $result2->bindValue(":name" . $idx, $name);
            // $result2->bindValue(":marketIDX" . $idx, $marketIDX);
            // $result2->bindValue(":managerIDX" . $idx, $managerIDX);
            // $result2->bindValue(":position" . $idx, $position);
            // $result2->bindValue(":migrationIDX" . $idx, $idx);

        }
        $result2->bindValue(":gradeIDX", $gradeIDX);
        $result2->bindValue(":statusIDX", $statusIDX);
        $result2->bindValue(":createTime", $createTime);
        $result2->bindValue(":dataDbKey", $dataDbKey);
        $result2->execute();

        //-------------------------------샌드박스 마켓매니저------------------------------------------

        $resultData = ['data'=>$result,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

    }



}


$dd=new AAACon();
