<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\MarketMo;
use App\Models\MarketWithdrawalLogMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMo;
use App\Models\ClientMileageMo;




use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ApiWithdrawalCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		$marketListData=MarketMo::GetMarketListData();
		$arr = ['marketListData'=>$marketListData];
		View::renderTemplate('page/apiWithdrawal/apiWithdrawal.html',$arr);
	}

	public function GetMarketWithdrawalAllData()
    {
    	// if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
        //     $errMsg='startDate 정보가 없습니다.';
        //     $errOn=$this::errExport($errMsg);
        // }
        // if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
        //     $errMsg='endDate 정보가 없습니다.';
        //     $errOn=$this::errExport($errMsg);
        // }
        $startDate=$_POST['startDate'] ?? '';
        $endDate=$_POST['endDate'] ?? '';
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $withdrawalDataPack=MarketWithdrawalLogMo::GetWithdrawalData($dataArr);

        $getMarketDataPack=MarketMo::GetMarketAllListData();


        $result = [];

        foreach ($withdrawalDataPack as $withdrawalList) {
            $matchingCode = $withdrawalList['marketCode'];

            // Find the corresponding 'name' for the 'code'
            $name = '';
            foreach ($getMarketDataPack as $item) {
                if ($item['code'] === $matchingCode) {
                    $name = $item['name'];
                    break; // Stop searching once a match is found
                }
            }

            $withdrawalList['name'] = $name;
            
            $result[] = $withdrawalList;
        }

        $resultData = ['result'=>'t','data'=>$result];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }

    public function ApuWithdrowalDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketUserEmail'])||empty($_POST['marketUserEmail'])){
            $errMsg='marketUserEmail 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketUserEmail=$_POST['marketUserEmail'];
      

        $clientDataPack=ClientMarketListMo::GetClientIDX($marketUserEmail);
        $marketIDX = $clientDataPack['marketIDX'];
        $clientIDX = $clientDataPack['clientIDX'];

        $getTargetWithdrawalDataPack=MarketWithdrawalLogMo::GetTargetWithdrawalData($targetIDX);

        $createTime = $getTargetWithdrawalDataPack['createTime'];
        $ci = $getTargetWithdrawalDataPack['ci'];
        $marketCode = $getTargetWithdrawalDataPack['marketCode'];
        $amount = $getTargetWithdrawalDataPack['amount'];
        $invoiceID = $getTargetWithdrawalDataPack['invoiceID'];
        $statusIDX = $getTargetWithdrawalDataPack['statusIDX'];

        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $clientAllDataPack=ClientMo::GetTargetClientData($clientIDX);
        $withdrawalCount=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['idxCount'];
        $latestCreateTime=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['createTime'];
     

        $phone = $clientAllDataPack['phone'];
        $name = $clientAllDataPack['name'];
        $createTime = $clientAllDataPack['createTime'];
        $accountNumber = $clientAllDataPack['accountNumber'];


        $renderData= [
            'marketIDX'=>$marketIDX,
            'clientIDX'=>$clientIDX,
            'createTime'=>$createTime,
            'ci'=>$ci,
            'marketCode'=>$marketCode,
            'requestAmount'=>$amount,
            'invoiceID'=>$invoiceID,
            'statusIDX'=>$statusIDX,
            'allAmount'=>$getMileage,
            'phone'=>$phone,
            'name'=>$name,
            'latestCreateTime'=>$latestCreateTime,
            'withdrawalCount'=>$withdrawalCount,
            'createTime'=>$createTime,
            'accountNumber'=>$accountNumber,
            'clientEmail'=>$marketUserEmail
        ];

        View::renderTemplate('page/apiWithdrawal/apiWithdrawalDetail.html',$renderData);
    }

}