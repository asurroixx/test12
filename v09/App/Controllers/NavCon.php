<?php

namespace App\Controllers;

use \Core\View;
use App\Models\StaffMenuMo;
// use App\Models\StaffGroupMo;
use App\Models\StaffAlarmMo;
use \Core\GlobalsVariable;

use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class NavCon extends \Core\Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function NavLoad($data=null)
    {
        $loginIDX = GlobalsVariable::GetGlobals('loginIDX');
        $nowPage='';
        $domainUriArr = explode('/', $_SERVER['REQUEST_URI']);
        $nowPage = end($domainUriArr);
        $nowPage = explode('?', $nowPage)[0];

        $db = static::getDB();
        $stat1=$db->prepare("UPDATE ebuy.StaffAlarm AS A
            LEFT JOIN ebuy.StaffMenu AS B ON A.menuIDX=B.idx
            SET A.viewStatusIDX=305101
            WHERE A.staffIDX='$loginIDX' AND B.url='$nowPage'
        ");
        $stat1->execute();

        $gradeIDX = GlobalsVariable::GetGlobals('loginGrade');
        $menuList = StaffMenuMo::GetGradeMenuList($gradeIDX);

        $menuListArr=[];
        foreach ($menuList as $key ) {
            $idx=$key['idx'];
            $name=$key['name'];
            $url=$key['url'];
            $status=$key['status'];
            $permission=$key['permission'];
            $targetPageStat=0;
            if($nowPage==$url){
                $targetPageStat=1;
            }
            $loginIDX= GlobalsVariable::GetGlobals('loginIDX');
            $targetArr=['menuIDX'=>$idx,'gradeIDX'=>$gradeIDX,'loginIDX'=>$loginIDX];
            if($status==3){
                $childMenuList = StaffMenuMo::GetChildGradeMenuList($targetArr);
                $childPack=[];
                foreach ($childMenuList as $key2) {
                    $childIDX=$key2['idx'];
                    $childMomIDX=$key2['momIDX'];
                    $childName=$key2['name'];
                    $childUrl=$key2['url'];
                    $childStatus=$key2['status'];
                    $childPermission=$key2['permission'];
                    $count=$key2['count'];
                    $childTargetPageStat=0;

                    if($childStatus==3){
                        if($nowPage==$childUrl){
                            $targetPageStat=1;
                            $childTargetPageStat=1;
                        }
                        $childPack[]=array(
                            'childIDX'=>$childIDX,
                            'childName'=>$childName,
                            'childUrl'=>$childUrl,
                            'childTargetPageStat'=>$childTargetPageStat,
                            'childPermission'=>$childPermission,
                            'count'=>$count,
                        );
                    }
                }
            }
            $menuListArr[]=array(
                'idx'=>$idx,
                'name'=>$name,
                'url'=>$url,
                'status'=>$status,
                'targetPageStat'=>$targetPageStat,
                'permission'=>$permission,
                'childPack'=>$childPack,
            );
        }
        $MenuResult = ['data'=>$menuListArr,'result'=>'t','nowPage'=>$nowPage,];
        $result=json_encode($MenuResult,JSON_UNESCAPED_UNICODE);
        echo $result;
        return $MenuResult;
    }

    public function GetAlarmResetData($data=null)
    {
        if(!isset($_POST['staffIDX'])||empty($_POST['staffIDX'])){
            $errMsg='staffIDX is empty.';
            $errOn=$this::errExportDo($errMsg,'n');
        }
        $staffIDX = $_POST['staffIDX'];
        $resetData = StaffAlarmMo::GetResetCount($staffIDX);
        $totalData = StaffAlarmMo::GetTotalCount($staffIDX);
        $dataResult = ['result'=>'t','data'=>$resetData,'totalData'=>$totalData];
        $result=json_encode($dataResult,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}