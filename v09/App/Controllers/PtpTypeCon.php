<?php
namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\BankMo;
use App\Models\PeerToPeerTypeMo;
use PDO;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PtpTypeCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        View::renderTemplate('page/ptpType/ptpType.html');
	}//렌더

	//bank.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=PeerToPeerTypeMo::GetPeerToPeerTypeData();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //bank.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $issetStatusVal=PeerToPeerTypeMo::PeerToPeerTypeStatusData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $statusIDX=$issetStatusVal['statusIDX'];
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.PeerToPeerType SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $ex='상태가 '.$statusIDX.'에서 '.$statusVal.'(으)로 변경됐습니다.';
        $logIDX=$this->StaffLogInsert($statusVal,$targetIDX);
        $logEx=$this->StaffLogExInsert($logIDX,$statusIDX,$statusVal,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
    //bank.html 이름 및 코드 추가
    public function PeerToPeerTypeInsert()
    {

        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['price'])||empty($_POST['price'])){
            $errMsg='price 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $name=trim($_POST['name']);
        $price=trim($_POST['price']);

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.PeerToPeerType
            (name,statusIDX,price)
            VALUES
            (:name,'331301',:price)
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':price', $price);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();

        $ex = '전자화폐 '.$name . ' 등록 (단가 : '.$price.'원)';
        $logIDX = $this->StaffLogInsert(331101, $targetIDX);
        $logEx = $this->StaffLogExInsert($logIDX, 0, 0, $ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
    //bank.html 이름 및 코드 업데이트
    public function PeerToPeerTypeUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['price'])||empty($_POST['price'])){
            $errMsg='price 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=trim($_POST['name']);
        $price=trim($_POST['price']);

        $issetVal=PeerToPeerTypeMo::PeerToPeerTypeStatusData($targetIDX);
        if(isset($issetVal['idx'])){
            $targetName=$issetVal['name'];
            $targetPrice=$issetVal['price'];
        }

        $step1=0;
        $step2=0;

        $commaPrice = number_format($price,0);
        $commaTargetPrice = number_format($targetPrice,0);

        //이름
        if($name!=$targetName){
            $step1=1;
            $ex=''.$name.' 전자화폐 이름 '.$targetName.'에서 '.$name.'로 수정';
            $logIDX = $this->StaffLogInsert(331201, $targetIDX);
            $logEx = $this->StaffLogExInsert($logIDX, 0, 0, $ex);
        }
        //단가
        if($price!=$targetPrice){
            $step2=1;
            $ex=''.$name.' 전자화폐 단가를 '.$commaTargetPrice.'에서 '.$commaPrice.'로 수정';
            $logIDX = $this->StaffLogInsert(331201, $targetIDX);
            $logEx = $this->StaffLogExInsert($logIDX, 0, 0, $ex);
        }


        if($step1==0 && $step2==0 ){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.PeerToPeerType SET
            name=:name,
            price=:price
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':price', $price);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        // $ex = '전자화폐 이름 '.$name . '으로 수정';
        // $logIDX = $this->StaffLogInsert(331201, $targetIDX);
        // $logEx = $this->StaffLogExInsert($logIDX, 0, 0, $ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}