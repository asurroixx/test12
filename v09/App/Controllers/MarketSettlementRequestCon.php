<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketSettlementMo;
use App\Models\MarketSettlementBankwireMo;
use App\Models\MarketSettlementCryptoWalletMo;
use App\Models\MarketSettlementKRWMo;
use App\Models\StaffMemoMo;
use App\Models\PortalLogMo;
use App\Models\StaffMo;



use App\Models\MarketKeyMo;


use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketSettlementRequestCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/marketSettlementRequest/marketSettlementRequest.html');
	}

	//marketSettlementRequest.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=MarketSettlementMo::GetMarketSettlementData($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['changeStatus'])||empty($_POST['changeStatus'])){
            $errMsg='changeStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $value='';
        if(isset($_POST['value'])||!empty($_POST['value'])){
        	$value=$_POST['value'];
        }
        $targetIDX=$_POST['targetIDX'];
        $marketIDX=$_POST['marketIDX'];
        $changeStatus=$_POST['changeStatus'];
        $db = static::getDB();
        $dbName= self::MainDBName;
        $nowTime=date("Y-m-d H:i:s");

        $possibleMarket=MarketMo::MarketStatusData($marketIDX);
        if(!isset($possibleMarket['idx'])||empty($possibleMarket['idx'])){
            $errMsg='존재하지않은 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketStatusIDX=$possibleMarket['statusIDX'];
        if($marketStatusIDX==412201){
            $errMsg='비활성된 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($marketStatusIDX==412202){
            $errMsg='계약만료된 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $duplicationInspection=MarketSettlementMo::GetDetail($targetIDX);
         if(!isset($duplicationInspection['idx'])||empty($duplicationInspection['idx'])){
            $errMsg='존재하지않은 신청건입니다.';
            $errOn=$this::errExport($errMsg);
        }
        $beforeStatus=$duplicationInspection['statusIDX'];
        $getAmount=$duplicationInspection['nonFeeAmount'];
        $getCreateTime=$duplicationInspection['createTime'];
        $marketName=$duplicationInspection['marketName'];
        $managerEmail=$duplicationInspection['managerEmail'];

        // 417101 크립토신청
        // 417102 뱅크와이어 신청
        // 417103 원화신청
        switch ($changeStatus) {
            case '417301':// 확인으로 바꾼다고? -> 이전 신청인가 확인
                if( $beforeStatus != 417101 && $beforeStatus != 417102 && $beforeStatus != 417103){
                    // $errMsg='이미 처리된 신청건입니다.';
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
                $expectedTime=$value;
                $update =$db->prepare("UPDATE $dbName.MarketSettlement SET
                    statusIDX = :changeStatus,
                    expectedTime = :expectedTime
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                if ($expectedTime !== '') {
				    $update->bindValue(':expectedTime', $expectedTime);
				} else {
                	$expectedTime = (is_null($value)) ? null : 'NULL';
				    $update->bindValue(':expectedTime',$expectedTime,PDO::PARAM_NULL);
				}
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();
                $dateTime = new \DateTime($expectedTime);
                $emailExpectedTime=$dateTime->format('M.d, Y, \a\t g:i:s A');
                $emailParamVal = [
                    'marketname'=>$marketName,
                    'manageremail'=>$managerEmail,
                    'expectedtime'=>$emailExpectedTime,
                    'amount'=>$getAmount
                ];

            break;
            case '417201'://완료로 바꾼다고? -> 이전 확인인가 확인
                // if($beforeStatus == 417201 || $beforeStatus == 417211){
                if($beforeStatus != 417301){
                    // $errMsg='이미 처리된 신청건입니다.';
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
                $hash=$value;
                $update =$db->prepare("UPDATE $dbName.MarketSettlement SET
                    statusIDX = :changeStatus,
                    hash = :hash,
                    completeTime = :nowTime
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                $update->bindValue(':hash', $hash);
                $update->bindValue(':nowTime', $nowTime);
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();

                $emailParamVal = [
                    'marketname'=>$marketName,
                    'manageremail'=>$managerEmail,
                    'hash'=>$hash,
                    'amount'=>$getAmount
                ];

            break;
            case '417211'://취소로 바꾼다고? -> 이전 신청 이거나 확인인가 확인
                // if($beforeStatus != 417301 || $beforeStatus == 417201 || $beforeStatus == 417211){
                if($beforeStatus != 417101 && $beforeStatus != 417102 && $beforeStatus != 417103 && $beforeStatus != 417301){
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
            	$amount=$value;
            	$update =$db->prepare("UPDATE $dbName.MarketSettlement SET
                    statusIDX = :changeStatus,
                    completeTime = :nowTime
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                $update->bindValue(':nowTime', $nowTime);
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();

                //마일리지테이블에 쌓기
                $insert=$db->prepare("INSERT INTO $dbName.MarketBalance
                    (
                        marketIDX,
                        statusIDX,
                        targetIDX,
                        amount,
                        createTime
                    )
                    VALUES
                    (
                        :marketIDX,
                        :changeStatus,
                        :targetIDX,
                        :amount,
                        :nowTime
                    )
                ");
                $insert->bindValue(':marketIDX', $marketIDX);
                $insert->bindValue(':changeStatus', $changeStatus);
                $insert->bindValue(':targetIDX', $targetIDX);
                $insert->bindValue(':amount', $amount);
                $insert->bindValue(':nowTime', $nowTime);
                $insert->execute();

                $emailParamVal = [
                    'marketname'=>$marketName,
                    'amount'=>$amount
                ];

            break;
            default:
        }

        $type = 'web';
        $staffIDX = '';
        if(isset($_POST['type'])||!empty($_POST['type'])){
            $type = $_POST['type'];
            $staffIDX = $_POST['staffIDX'];
        }

        $createTime=date("Y-m-d H:i:s");
        $ipAddress = $this->GetIPaddress();

        if($type == 'app'){
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db->prepare("INSERT INTO $dbName.PortalLog
                (statusIDX,targetIDX,staffIDX,createTime,ip)
                VALUES
                (:changeStatus,:targetIDX,:staffIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':changeStatus', $changeStatus);
            $stat2->bindValue(':targetIDX', $targetIDX);
            $stat2->bindValue(':staffIDX', $staffIDX);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', $ipAddress);
            $stat2->execute();
        }else{
            static::PortalLogInsert($changeStatus,$targetIDX);
        }


        $resultData = ['result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);

        //20231108 dj 소켓알람
        if($changeStatus=='417201' || $changeStatus=='417211'){
            //완료와 취소는 알람을 쏴줍니다.
            $alarmParam=[
                'targetIDX'=>$targetIDX,
                'statusIDX'=>$changeStatus,
                'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
                'param'=>$emailParamVal
            ];
            PortalAlarmCon::sendAlarmToPortal($alarmParam);
        }

        //이메일을 보내주자
        $emailParam=[
            'targetIDX'=>$targetIDX,
            'statusIDX'=>$changeStatus,
            'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
            'param'=>$emailParamVal,
            'type'=>'group'
        ];
        StaffEmailCon::sendEmailToPortal($emailParam);
        //이메일을 보내주자
    }

    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketIDX=$_POST['marketIDX'];
        $dataPack=MarketSettlementMo::GetDetail($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='디테일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketPack=MarketMo::GetMarketDetailData($marketIDX);
        if(!isset($marketPack['idx'])||empty($marketPack['idx'])){
            $errMsg='마켓 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $requestStatusIDX=$dataPack['requestStatusIDX'];
        $statusIDX=$dataPack['statusIDX'];
        //완료or취소 처리한 스태프정보가져오기
        $completePack=[];
        if($statusIDX=='417201'||$statusIDX=='417211'){
            $logArr=['statusIDX'=>$statusIDX,'targetIDX'=>$targetIDX];
            $completePack=PortalLogMo::GetCompleteStaffInfoData($logArr);
            if(!isset($completePack['idx'])||empty($completePack['idx'])){
                $errMsg='완료 or 취소가 됐는데 스탶로그가없다고? 이상해';
                $errOn=$this::errExport($errMsg);
            }
        }
        $methodIDX=$dataPack['methodIDX'];

        switch ($requestStatusIDX) {
            case '417101'://크립토
                $requestTypePack=MarketSettlementCryptoWalletMo::GetMarketSettlementInfo($methodIDX);
            break;
            case '417102'://뱅크와이어
                $requestTypePack=MarketSettlementBankwireMo::GetMarketSettlementInfo($methodIDX);
            break;
            case '417103'://원화
                $requestTypePack=MarketSettlementKRWMo::GetMarketSettlementInfo($methodIDX);
            break;
            default:
        }
        if(!isset($requestTypePack['idx'])||empty($requestTypePack['idx'])){
            $errMsg='요청한 정산방법에 대한 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $renderData=[
            'targetIDX'=>$targetIDX,
            'marketIDX'=>$marketIDX,
            'dataPack'=>$dataPack,
            'marketPack'=>$marketPack,
            'requestTypePack'=>$requestTypePack,
            'completePack'=>$completePack,
        ];
        View::renderTemplate('page/marketSettlementRequest/marketSettlementRequestDetail.html',$renderData);
    }

    public function HashUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['hash'])||empty($_POST['hash'])){
            $errMsg='hash 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $hash=$_POST['hash'];
        $db = static::getDB();
        $dbName= self::MainDBName;

        $update =$db->prepare("UPDATE $dbName.MarketSettlement SET
            hash = :hash
            WHERE idx = :targetIDX
        ");
        $update->bindValue(':hash', $hash);
        $update->bindValue(':targetIDX', $targetIDX);
        $update->execute();
        $resultData = ['result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }



}