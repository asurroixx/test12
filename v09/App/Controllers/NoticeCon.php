<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\NoticeMo;
use App\Models\FilesMo;


use App\Models\PortalEmailMsgMo;
use App\Models\MarketManagerMo;
use App\Models\ManagerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class NoticeCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

    //렌더
	public function Render($data=null)
	{
        $FileMaxSize=self::FileMaxSize;
        $FileAbledExt=self::FileAbledExt;
        $FileAbledExt = implode(', ', $FileAbledExt);
        $FileAbledCount=self::FileAbledCount;
        $renderData=[
            'FileMaxSize'=>$FileMaxSize,
            'FileAbledExt'=>$FileAbledExt,
            'FileAbledCount'=>$FileAbledCount,
        ];
		View::renderTemplate('page/notice/notice.html',$renderData);
	}

	//bank.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=NoticeMo::DataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //notice.html 디테일 로드
    public function NoticeDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=NoticeMo::GetNoticeDetailData($targetIDX);
            $filePack=FilesMo::GetFileData($targetIDX);
        }else if($pageType=='ins'){
            $dataPack='';
            $targetIDX='';
            $filePack='';
        }
        $renderData=[
            //ins
            'pageType'=>$pageType,
            //
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'filePack'=>$filePack,
        ];
        View::renderTemplate('page/notice/noticeDetail.html',$renderData);

    }

    //notice.html 추가
    public function NoticeInsert()
    {
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['mailingStatusIDX'])||empty($_POST['mailingStatusIDX'])){
            $errMsg='mailingStatusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['popupStatusIDX'])||empty($_POST['popupStatusIDX'])){
            $errMsg='popupStatusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //파일 유효성 체크
        $filesArr = '';
        if(isset($_FILES['files'])){
            $filesArr = $_FILES['files'];
            $fileUploadChk = $this::fileUploadChk($_FILES['files']);
        }
        //파일 유효성 체크



        $title=$_POST['title'];
        $content=$_POST['content'];

        $statusIDX=$_POST['statusIDX'];
        $mailingStatusIDX=$_POST['mailingStatusIDX'];
        $popupStatusIDX=$_POST['popupStatusIDX'];

        $popupCountData=NoticeMo::GetPopupCount();
        $count=$popupCountData['popupCount'];
        if($popupStatusIDX==342601 && $count > 1){
            $errMsg='팝업은 최대 2개까지 등록이 가능합니다.';
            $errOn=$this::errExport($errMsg);
        }


        $createTime=date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Notice
            (title,content,createTime,staffIDX,statusIDX,mailingStatusIDX,popupStatusIDX)
            VALUES
            (:title,:content,:createTime,:staffIDX,:statusIDX,:mailingStatusIDX,:popupStatusIDX)
        ");

        $stat1->bindValue(':title', $title);
        $stat1->bindValue(':content', $content);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':staffIDX', $staffIDX);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':mailingStatusIDX', $mailingStatusIDX);
        $stat1->bindValue(':popupStatusIDX', $popupStatusIDX);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(342101,$targetIDX);

        

        //파일이 있다면 파일을 보낼것
        if(isset($_FILES['files'])){
            $fileUpload = $this::fileUpload($_FILES['files'],'notice');//파일업로드
            $fileDbIns = $this::fileDbIns($fileUpload,$statusIDX,$targetIDX);//디비인서트
        }
        //파일이 있다면 파일을 보낼것

        $alarmParam=[
            'targetIDX'=>$targetIDX,
            'statusIDX'=>$statusIDX,
            'marketIDX'=>0, //모든 마켓에게 다 보내려면 0으로 보내주세요
            'param'=>[]
        ];
        PortalAlarmCon::sendAlarmToPortal($alarmParam);


        // if($mailingStatusIDX==342501){
        //     //이메일을 보내주자
        //     $emailParamVal = ['title'=>$title,'content'=>$content];
        //     $emailParam=[
        //         'targetIDX'=>$targetIDX,
        //         'statusIDX'=>$statusIDX,
        //         'marketIDX'=>0, //모든 매니저에게 다 보내려면 0으로 하고 type all로
        //         'type'=>'all',
        //         'param'=>$emailParamVal,
        //     ];
        //     StaffEmailCon::sendEmailToPortal($emailParam);
        //     //이메일을 보내주자
        // }



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;


    }

    //notice.html 업데이트
    public function NoticeUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['popupStatusIDX'])||empty($_POST['popupStatusIDX'])){
            $errMsg='popupStatusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //파일 유효성 체크
        $filesArr = '';
        if(isset($_FILES['files'])){
            $filesArr = $_FILES['files'];
            $fileUploadChk = $this::fileUploadChk($_FILES['files']);
        }
        //파일 유효성 체크



        $targetIDX=$_POST['targetIDX'];
        $title=$_POST['title'];
        $content=$_POST['content'];

        $statusIDX=$_POST['statusIDX'];
        $popupStatusIDX=$_POST['popupStatusIDX'];

        $statusVal=$_POST['statusVal'];
        $popupStatusVal=$_POST['popupStatusVal'];

        $popupCountData=NoticeMo::GetPopupCount($targetIDX);
        $count=$popupCountData['popupCount'];
        if($popupStatusIDX==342601 && $count > 1){
            $errMsg='팝업은 최대 2개까지 등록이 가능합니다.';
            $errOn=$this::errExport($errMsg);
        }

        //파일이 있다면 파일을 보낼것
        $fileCount=0;
        if(isset($_FILES['files'])){
            $fileCount=count($_FILES['files']['name']);
            if ($fileCount > 0) {
                $fileUpload = $this::fileUpload($_FILES['files'], 'notice'); // 파일 업로드
                $fileDbIns = $this::fileDbIns($fileUpload, 342201, $targetIDX); // 디비 인서트

                foreach ($_FILES['files']['name'] as $key) {
                    $fileName = $key;

                    $ex = $fileName . ' 업로드가 완료되었습니다.';
                    $logIDX = $this->StaffLogInsert(342201, $targetIDX);
                    $logEx = $this->StaffLogExInsert($logIDX, 0, 0, $ex);
                }
            }
        }
        //파일이 있다면 파일을 보낼것

        //해당 타겟 공지사항 가져오기
        $targetData=NoticeMo::GetTargetData($targetIDX);
        if(!isset($targetData['idx'])){
            $errMsg='타겟이 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        $targetTitle=$targetData['title'];
        $targetContent=$targetData['content'];
        $targetStatusIDX=$targetData['statusIDX'];
        $targetPopupStatusIDX=$targetData['popupStatusIDX'];

        $targetStatusVal=$targetData['statusVal'];
        $targetPopupStatusVal=$targetData['popupStatusVal'];

        $step1=0;
        $step2=0;
        $step3=0;
        $step4=0;

        //제목
        if($title!=$targetTitle){
            $step1=1;
            $ex='제목이 '.$targetTitle.'에서 '.$title.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(342201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //내용
        if($content!=$targetContent){
            $step2=1;
            $ex='공지사항 내용이 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(342201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //statusIDX
        if($statusIDX!=$targetStatusIDX){
            $step3=1;
            $ex='상태가 '.$targetStatusVal.'에서 '.$statusVal.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(342201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$targetStatusIDX,$statusIDX,$ex);

            if($statusIDX==342402){
                $alarmParam=[
                    'targetIDX'=>$targetIDX,
                    'statusIDX'=>342402,
                    'marketIDX'=>0, //모든 마켓에게 다 보내려면 0으로 보내주세요
                    'param'=>[]
                ];
                PortalAlarmCon::sendAlarmToPortal($alarmParam);
            }
        }
        //popupStatusIDX
        if($popupStatusIDX!=$targetPopupStatusIDX){
            $step4=1;
            $ex='팝업 상태가 '.$targetPopupStatusVal.'에서 '.$popupStatusVal.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(342201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$targetPopupStatusIDX,$popupStatusIDX,$ex);

        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0 && $fileCount == 0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Notice SET
            title=:title,
            content=:content,
            statusIDX=:statusIDX,
            popupStatusIDX=:popupStatusIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':title', $title);
        $stat1->bindValue(':content', $content);
        $stat1->bindValue(':statusIDX', $statusIDX);
        if($statusIDX == 342403){
            $stat1->bindValue(':popupStatusIDX', 342602);
        }else{
            $stat1->bindValue(':popupStatusIDX', $popupStatusIDX);
        }

        $stat1->execute();



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    // //notice.html status 업데이트
    // public function StatusUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
    //         $errMsg='statusVal 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusVal=$_POST['statusVal'];

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.Notice SET
    //         statusIDX=:statusVal
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':statusVal', $statusVal);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     $this->StaffLogInsert($statusVal,$targetIDX);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //notice.html 삭제버튼 status 업데이트
    public function DeleteStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=342301;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Notice SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->StaffLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //notice.html 삭제버튼 status 업데이트
    public function NoticeMailSend()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $title=$_POST['title'];
        $content=$_POST['content'];
        $mailingStatusIDX=342501;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Notice SET
            mailingStatusIDX=:mailingStatusIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':mailingStatusIDX', $mailingStatusIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $this->StaffLogInsert($mailingStatusIDX,$targetIDX);

        //이메일을 보내주자
        // $emailParamVal = ['title'=>$title,'content'=>$content];
        // $emailParam=[
        //     'targetIDX'=>$targetIDX,
        //     'statusIDX'=>342401,
        //     'marketIDX'=>0, //모든 마켓에게 다 보내려면 0으로 보내주세요
        //     'param'=>$emailParamVal,
        //     'type'=>'all'
        // ];
        // StaffEmailCon::sendEmailToPortal($emailParam);
        //이메일을 보내주자

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //파일 정보 db 인서트
    protected static function fileDbIns($fileUpload=[],$statusIDX=null,$targetIDX=null){
        /* statusIDX = 해당 행위 statusIDX / targetIDX = 해당 행위 타겟idx / $createIDX = 누가 행위했는지 (스태프)*/
        $db = static::getDB();
        $dbName= self::MainDBName;
        $targetIDX = $targetIDX;
        $statusIDX = $statusIDX; // 파일 저장 스테이터스 (공지사항)
        $createTime = date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        foreach ($fileUpload as $key ) {
            $thisOrignName  = $key['orignName'];
            $thisServerName = $key['serverName'];
            $thisExt        = $key['ext'];


            $stat2=$db->prepare("INSERT INTO $dbName.Files
                (serverName,orignName,ext,targetIDX,statusIDX,createIDX,createTime)
                VALUES
                (:serverName,:orignName,:ext,:targetIDX,:statusIDX,:createIDX,:createTime)
            ");
            $stat2->bindValue(':serverName', $thisServerName);
            $stat2->bindValue(':orignName', $thisOrignName);
            $stat2->bindValue(':ext', $thisExt);
            $stat2->bindValue(':targetIDX', $targetIDX);
            $stat2->bindValue(':statusIDX', $statusIDX);
            $stat2->bindValue(':createIDX', $staffIDX);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->execute();
        }

    }

    // 파일 다운 및 띄우기
    public function FileDownOrImageView()
    {
        $fileIDX = $_POST['fileIDX'] ?? '';
        if(!$fileIDX) $this::errExport('비 정상적인 접근입니다.');
        // 파일 가져오기
        $fileDownLoadPack = self::fileDownload([$fileIDX]);
        $filePack = $fileDownLoadPack['data'];
        $ext     = $filePack[0]['ext'];
        $fileUri = $filePack[0]['uri'];
        $type    = 'file';
        // // 이미지인가?
        if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png', 'txt', 'pdf'])) {
            $type = 'img';
        }
        $result  = ['result' => 't','uri' => $fileUri, 'type' => $type];
        echo json_encode($result,JSON_UNESCAPED_UNICODE);
    }






}