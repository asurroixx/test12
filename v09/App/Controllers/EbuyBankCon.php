<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\BankMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class EbuyBankCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$bankPack=BankMo::GetBankData();
		$renderData=['bankPack'=>$bankPack];
		View::renderTemplate('page/ebuyBank/ebuyBank.html',$renderData);
	}//렌더

	//ebuyBank.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=EbuyBankMo::GetEbuyBankData();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //ebuyBankDetail.html 디테일 로드
    public function EbuyBankDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $detailPack=EbuyBankMo::GetEbuyBankDetail($targetIDX);
            $bankPack='';
        }else if($pageType=='ins'){
            $targetIDX='';
            $detailPack='';
            $bankPack=BankMo::GetBankData();
        }
        // $ebuyBankPack=EbuyBankMo::GetEbuyBankStatusData();
        $renderData=[
            //pub
            'pageType'=>$pageType,
            //upd
            'targetIDX'=>$targetIDX,
            'detailPack'=>$detailPack,
            //ins
            'bankPack'=>$bankPack,
        ];
        View::renderTemplate('page/ebuyBank/ebuyBankDetail.html',$renderData);

    }

    //ebuyBank.html 이바이 은행 추가
    public function EbuyBankInsert()
    {
        if(!isset($_POST['bankIDX'])||empty($_POST['bankIDX'])){
            $errMsg='bankIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['accountNumber'])||empty($_POST['accountNumber'])){
            $errMsg='accountNumber 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['accountHolder'])||empty($_POST['accountHolder'])){
            $errMsg='accountHolder 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $bankIDX=$_POST['bankIDX'];
        $accountNumber=$_POST['accountNumber'];
        $accountHolder=$_POST['accountHolder'];
        $createTime=date("Y-m-d H:i:s");

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.EbuyBank
            (bankIDX,accountNumber,accountHolder,statusIDX,createTime)
            VALUES
            ('$bankIDX','$accountNumber','$accountHolder',323301,'$createTime')
        ");
        $stat1->execute();

		$targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(323101,$targetIDX);


        //api서버도 동일하게 추가
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("INSERT INTO $apiDbName.EbuyBank
            (bankIDX,accountNumber,accountHolder,statusIDX,createTime)
            VALUES
            (:bankIDX,:accountNumber,:accountHolder,:statusIDX,:createTime)
        ");

        $stat2->bindValue(':bankIDX', $bankIDX);
        $stat2->bindValue(':accountNumber', $accountNumber);
        $stat2->bindValue(':accountHolder', $accountHolder);
        $stat2->bindValue(':statusIDX', 323301);
        $stat2->bindValue(':createTime', $createTime);
        $stat2->execute();

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //ebuyBank.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.EbuyBank SET
            statusIDX='$statusVal'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $this->StaffLogInsert($statusVal,$targetIDX);


        //api서버도 동일하게 추가
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("UPDATE $apiDbName.EbuyBank SET
            statusIDX=:statusIDX
            WHERE idx=:targetIDX
        ");
        $stat2->bindValue(':statusIDX', $statusVal);
        $stat2->bindValue(':targetIDX', $targetIDX);
        $stat2->execute();

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //ebuyBank.html 정보 업데이트
    public function EbuyBankUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['accountHolder'])||empty($_POST['accountHolder'])){
            $errMsg='accountHolder 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['accountNumber'])||empty($_POST['accountNumber'])){
            $errMsg='accountNumber 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $accountHolder=$_POST['accountHolder'];
        $accountNumber=$_POST['accountNumber'];

        $step1=0;
        $step2=0;

        $issetData=EbuyBankMo::issetEbuyBankData($targetIDX);
        if(isset($issetData['idx'])){
            $issetAccountHolder=$issetData['accountHolder'];
            $issetAccountNumber=$issetData['accountNumber'];
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //예금주
        if($accountHolder!=$issetAccountHolder){
            $step1=1;
            $ex1='예금주가 '.$issetAccountHolder.'에서 '.$accountHolder.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(323201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex1);
        }
        //등급이름
        if($accountNumber!=$issetAccountNumber){
            $step2=1;
            $ex2='계좌번호가 '.$issetAccountNumber.'에서 '.$accountNumber.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(323201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex2);
        }

        if($step1==0 && $step2==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }






        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.EbuyBank SET
            accountNumber='$accountNumber',
            accountHolder='$accountHolder'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        //api서버도 동일하게 추가
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("UPDATE $apiDbName.EbuyBank SET
            accountNumber=:accountNumber,
            accountHolder=:accountHolder
            WHERE idx=:targetIDX
        ");
        $stat2->bindValue(':accountNumber', $accountNumber);
        $stat2->bindValue(':accountHolder', $accountHolder);
        $stat2->bindValue(':targetIDX', $targetIDX);
        $stat2->execute();

        // $this->StaffLogInsert(323201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }




}