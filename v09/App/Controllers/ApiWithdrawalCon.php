<?php

namespace App\Controllers;

use \Core\View;
use App\Models\MarketMo;
use App\Models\MarketWithdrawalLogMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMo;
use App\Models\ClientMileageMo;

class ApiWithdrawalCon extends \Core\Controller
{
    public function Render($data=null)
    {
        $marketListData = MarketMo::GetMarketListData();
        $arr            = ['marketListData'=>$marketListData];

        View::renderTemplate('page/apiWithdrawal/apiWithdrawal.html',$arr);
    }

    public function GetMarketWithdrawalAllData()
    {
        $startDate = $_POST['startDate'] ?? '';
        $endDate   = $_POST['endDate'] ?? '';

        if(!$startDate) $this::errExport('startDate 정보가 없습니다.');
        if(!$endDate) $this::errExport('endDate 정보가 없습니다.');

        $dataArr = ['startDate' => $startDate, 'endDate' => $endDate];
        $withdrawalDataPack = MarketWithdrawalLogMo::GetWithdrawalData($dataArr);  // API 리스트 가져오기

        $result           = [];
        $marketUserEmails = [];
        
        // 로그에서 이메일만 배열로 분리
        foreach ($withdrawalDataPack as $data) {
            $marketUserEmail = $data['marketUserEmail'];
            
            if (!empty($marketUserEmail)) {
                $marketUserEmails[] = $marketUserEmail;
            }
        }

        // 분리된 이메일만 검색
        $namePack = ClientMarketListMo::GetArrClientNamePack($marketUserEmails);
        foreach ($withdrawalDataPack as &$DepositList) {
            $matchingCode    = $DepositList['marketCode'];
            $marketUserEmail = $DepositList['marketUserEmail'];
            $name            = '-';
            $clientName      = '-';
            foreach ($namePack as $item) {
                    if ($item['code'] == $matchingCode && strtolower($item['referenceId']) == strtolower($marketUserEmail)) {
                    $name = $item['name'];
                    $clientName = $item['clientName'];
                }
            }
            $DepositList['name']       = $name;
            $DepositList['clientName'] = $clientName;
            $result[] = $DepositList;
        }

        $resultData = ['result'=>'t','data'=>$result];

        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }


    //오른쪽 디테일 창
    public function ApiDetailFormLoad()
    {
        $targetIDX       = $_POST['targetIDX'] ?? '';
        $marketUserEmail = $_POST['marketUserEmail'] ?? '';
        if(!$targetIDX) $this::errExport('targetIDX 정보가 없습니다.');
        if(!$marketUserEmail) $this::errExport('marketUserEmail 정보가 없습니다.');

        // 타겟 디테일 구하기
        $detailPack   = MarketWithdrawalLogMo::GetTargetWithdrawalData($targetIDX);
        if(!$detailPack) $this::errExport('디테일 정보가 없습니다.');
        // 에러 메시지
        $log               = $detailPack['log'] ?? '';
        $logDecode         = json_decode($log, true);
        $detailPack['msg'] = $logDecode['msg'] ?? '';
        // 클라이언트 정보 있으면 구하기
        $clientIDX=0;
        $clientDataPack = ClientMarketListMo::GetClientIDX($marketUserEmail);
        if($clientDataPack){
            $clientIDX = $clientDataPack['clientIDX'];
            $clientAllDataPack = ClientMo::GetTargetClientData($clientIDX);
            $getMileage        = ClientDetailMo::getMileage($clientIDX);
            $DepositCount      = ClientMileageMo::GetApiWithdrawalCount($clientIDX);
        }
        
        $clientDetailPack = [
            'phone'            => $clientAllDataPack['phone'] ?? '-',
            'name'             => $clientAllDataPack['name'] ?? '-',
            'clientEmail'      => $clientAllDataPack['email'] ?? '-',
            'accountNumber'    => $clientAllDataPack['accountNumber'] ?? '-',
            'allAmount'        => $getMileage ?? '-',
            'DepositCount'     => $DepositCount['idxCount'] ?? '-',
            'latestCreateTime' => $DepositCount['createTime'] ?? '-'
        ];

        $renderData = [
            'data'   => $detailPack,
            'client' => $clientDetailPack,
            'clientIDX'=>$clientIDX,

        ];

        View::renderTemplate('page/apiWithdrawal/apiWithdrawalDetail.html',$renderData);
    }

    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=MarketWithdrawalLogMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function ClientDetailLoad($data=null){
        if(!isset($_POST['marketCode'])||empty($_POST['marketCode'])){
            $errMsg='marketCode 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketUserEmail'])||empty($_POST['marketUserEmail'])){
            $errMsg='marketUserEmail 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketCode=$_POST['marketCode'];
        $marketUserEmail=$_POST['marketUserEmail'];
        $dataArr=array(
            'marketCode'=>$marketCode,
            'marketUserEmail'=>$marketUserEmail,
        );
        $clientIDXData=ClientMarketListMo::GetClientIDXData($dataArr);

        if(isset($clientIDXData['idx'])&&!empty($clientIDXData['idx'])){
            $clientIDX = $clientIDXData['clientIDX'];
            static::ClientDetail($clientIDX);
        }

    }






}