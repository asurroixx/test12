<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketManagerMo;
use App\Models\MarketManagerGradeMo;
use App\Models\MarketMo;
use App\Models\MarketManagerPositionMo;
use App\Models\ManagerLoginHistoryMo;
use App\Models\ManagerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketManagerCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=MarketMo::GetMarketListData();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/marketManager/marketManager.html',$renderData);
	}//렌더

	//marketManager.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=MarketManagerMo::GetDatatableList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 디테일 로드
    public function MarketManagerDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $pageType=$_POST['pageType'];
        $recentLogin='';

        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg);
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=MarketManagerMo::GetGradeDetail($targetIDX);
            if(!isset($dataPack['managerIDX'])||empty($dataPack['managerIDX'])){
                $errMsg='managerIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg);
            }
            $managerIDX=$dataPack['managerIDX'];
            //231226 최근 로그인 추가함 !!!
            $getRecentLogin=ManagerLoginHistoryMo::GetRecentLoginTime($managerIDX);
            if(!isset($getRecentLogin['recentLoginTime'])||empty($getRecentLogin['recentLoginTime'])){
                $recentLogin='-';
            }
            $recentLogin=$getRecentLogin['recentLoginTime'];
            $marketPack='';
            $managerListPack='';
        }else if($pageType=='ins'){
        	$targetIDX='';
			$dataPack='';
            $managerListPack='';
            $marketPack=MarketMo::GetMarketListData();
        }else if($pageType=='hand'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg);
            }
            $targetIDX=$_POST['targetIDX'];
            $managerListPack=MarketManagerMo::GetMarketManagerList($targetIDX);
            $dataPack='';
            $marketPack='';
        }

        $gradePack=MarketManagerGradeMo::GetDatatableList();
        // $positionPack=MarketManagerPositionMo::GetPositionListData();

        $renderData=[
            'targetIDX'=>$targetIDX,
            'recentLogin'=>$recentLogin,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
            'gradePack'=>$gradePack,
            'marketPack'=>$marketPack,
            // 'positionPack'=>$positionPack,
            'managerListPack'=>$managerListPack,
        ];
        View::renderTemplate('page/marketManager/marketManagerDetail.html',$renderData);
    }

    //main.js 로그인히스토리
    public function ManagerLoginHistory()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=MarketManagerMo::GetGradeDetail($targetIDX);
        if(!isset($dataPack['managerIDX'])||empty($dataPack['managerIDX'])){
            $errMsg='managerIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $managerIDX=$dataPack['managerIDX'];
        $data=ManagerLoginHistoryMo::GetLoginHisList($managerIDX);
        $resultData = ['result'=>'t','data'=>$data];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->PortalLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 삭제버튼 status 업데이트
    public function DeleteStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=402202;


        $getDetail = MarketManagerMo::GetGradeDetail($targetIDX);
        if(!isset($getDetail['idx'])){
            $errMsg='존재하지 않는 IDX.';
            $errOn=$this::errExport($errMsg);
        }

        $targetEmail = $getDetail['email'];
        $targetMarketIDX =  $getDetail['marketIDX'];


        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->PortalLogInsert($statusVal,$targetIDX);





        //샌드박스에도 매니저를 삭제시켜주자.
        $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($targetMarketIDX);
        if(isset($marketIDXInSandboxSel['idx'])){
            $sandboxMarketIDX=$marketIDXInSandboxSel['idx'];
            $paramArr = ['marketIDX'=>$sandboxMarketIDX, 'email'=>$targetEmail];

            $getMMIDX = MarketManagerMo::getMarketManagerIDXInSandbox($paramArr);
            if(isset($getMMIDX['idx'])){
                $sandboxMMIDX = $getMMIDX['idx'];
                $dbsand = static::GetSandboxDB();
                $dbNameSand= self::EbuySandboxDBName;
                $query = $dbsand->prepare("UPDATE $dbNameSand.MarketManager SET statusIDX=:statusIDX WHERE idx=:idx");
                $query->bindValue(':statusIDX', 402202);
                $query->bindValue(':idx', $sandboxMMIDX);
                $query->execute();
            }
        }
        //샌드박스에도 매니저를 삭제시켜주자.









        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 마켓 매니저 인서트
    public function MarketManagerInsert()
    {

        $dataDbKey=self::dataDbKey;
        if(!isset($_POST['email'])||empty($_POST['email'])){
            $errMsg='email 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        //마켓 선택을 안함
        $marketIDX=$_POST['marketIDX'];
        if($marketIDX=='none'){
            $errMsg='마켓을 선택해주세요.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['position'])||empty($_POST['position'])){
            $errMsg='position 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //이메일 형식
        $email=$_POST['email']; $email=trim($email);
        $mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
            $errMsg='이메일 형식이 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }

        $name=$_POST['name'];
        $gradeIDX=$_POST['gradeIDX'];
        $statusIDX=$_POST['statusIDX'];
        $position=$_POST['position'];

        //해당 마켓에 등급이 Super가 있으면 Super 등급으로는 인서트 불가
        if($gradeIDX==1){
            $issetSuperGrade=MarketManagerMo::issetManagerGradeData($marketIDX);
            $superGradeCount=$issetSuperGrade['count'];
            if($superGradeCount > 0){
                $errMsg=$issetSuperGrade['name'].'마켓의 Super 등급이 존재합니다.';
                $errOn=$this::errExport($errMsg);
            }
        }

        $ipAddress = $this->GetIPaddress();
        $createTime=date("Y-m-d H:i:s");
        $issetEmailData=ManagerMo::IssetManagerEmail($email);
        //이메일 조사 후 매니저 테이블에 이메일 없으면 인서트 / 있으면 그해당 idx 가져오기 (managerIDX)
        if(isset($issetEmailData['idx'])){
            $managerIDX=$issetEmailData['idx'];
        }else{
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat1=$db->prepare("INSERT INTO $dbName.Manager
                (email,ipAddress,createTime)
                VALUES
                (AES_ENCRYPT(:email, '$dataDbKey'),:ipAddress,:createTime)
            ");
            $stat1->bindValue(':email', $email);
            $stat1->bindValue(':ipAddress', $ipAddress);
            $stat1->bindValue(':createTime', $createTime);
            $stat1->execute();
            $managerIDX = $db->lastInsertId();
        }

        //해당 마켓에 마켓 매니저가 있다면 인서트 불가
        $managerArr=['marketIDX'=>$marketIDX,'managerIDX'=>$managerIDX];
        $issetManagerIDX=MarketManagerMo::issetManagerData($managerArr);
        if(isset($issetManagerIDX['idx'])){
            $errMsg='이미 등록된 마켓 매니저 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        //유효성 끝나면 마켓매니저 테이블에 인서트
        $db2 = static::getDB();
        $dbName= self::MainDBName;
        $stat2=$db2->prepare("INSERT INTO $dbName.MarketManager
            (marketIDX,managerIDX,gradeIDX,statusIDX,position,name,createTime)
            VALUES
            (:marketIDX,:managerIDX,:gradeIDX,:statusIDX,:position,AES_ENCRYPT(:name, '$dataDbKey'),:createTime)
        ");

        $stat2->bindValue(':marketIDX', $marketIDX);
        $stat2->bindValue(':managerIDX', $managerIDX);
        $stat2->bindValue(':gradeIDX', $gradeIDX);
        $stat2->bindValue(':statusIDX', $statusIDX);
        $stat2->bindValue(':position', $position);
        $stat2->bindValue(':name', $name);
        $stat2->bindValue(':createTime', $createTime);

        $stat2->execute();
        $targetIDX = $db2->lastInsertId();

        //포탈로그
        $this->PortalLogInsert(401101,$targetIDX);



         //샌드박스에도 매니저를 만들어주자.
        $dbsand = static::GetSandboxDB();
        $dbNameSand= self::EbuySandboxDBName;
        $managerDupleChkInSand = ManagerMo::issetEmailInSand($email);
        $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($marketIDX);
        if(isset($marketIDXInSandboxSel['idx'])){
            $sandboxMarketIDX = $marketIDXInSandboxSel['idx'];
            if(!isset($managerDupleChkInSand['idx'])){
                $query=$dbsand->prepare("INSERT INTO $dbNameSand.Manager (email,ipAddress, createTime)
                    VALUES (AES_ENCRYPT(:email, '$dataDbKey'), :ipAddress, :createTime)
                ");
                $query->bindValue(':email', $email);
                $query->bindValue(':ipAddress', $ipAddress);
                $query->bindValue(':createTime', $createTime);
                $query->execute();
                $managerIDX=$dbsand->lastInsertId();
            }else{
                $managerIDX=$managerDupleChkInSand['idx'];
            }

            $query=$dbsand->prepare("INSERT INTO $dbNameSand.MarketManager (marketIDX,managerIDX,gradeIDX, statusIDX,position,name,createTime)
                VALUES (:marketIDX, :managerIDX, :gradeIDX, :statusIDX,:position,AES_ENCRYPT(:name, '$dataDbKey'),:createTime)
            ");
            $query->bindValue(':marketIDX', $sandboxMarketIDX);
            $query->bindValue(':managerIDX', $managerIDX);
            $query->bindValue(':gradeIDX', $gradeIDX);
            $query->bindValue(':statusIDX', 402101);
            $query->bindValue(':position', $position);
            $query->bindValue(':name', $name);
            $query->bindValue(':createTime', $createTime);
            $query->execute();
        }
        //샌드박스에도 매니저를 만들어주자.

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 마켓 매니저 업데이트
    public function MarketManagerUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
            $errMsg='gradeName 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['position'])||empty($_POST['position'])){
            $errMsg='position 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];
        $gradeIDX=$_POST['gradeIDX'];
        $gradeName=$_POST['gradeName'];
        $position=$_POST['position'];

        $marketIDX=$_POST['marketIDX'];
        //해당 마켓에 등급이 Super가 있으면 Super 등급으로는 업데이트 불가

        $issetSuperGrade=MarketManagerMo::issetSuperManagerGradeData($marketIDX);
        //있으면 일단 이 마켓엔 슈퍼등급이 있음
        if(isset($issetSuperGrade['idx'])){
            $superIDX=$issetSuperGrade['idx'];
            $superGradeIDX=$issetSuperGrade['gradeIDX'];
            $issetMarketName=$issetSuperGrade['name'];

            if($superIDX!=$targetIDX){
                //슈퍼등급이 아닌데 넘어온값이 슈퍼등급이야? 그럼 안돼
                if($gradeIDX==$superGradeIDX){
                    $errMsg=$issetMarketName.'마켓의 Super 등급이 존재합니다.';
                    $errOn=$this::errExport($errMsg);
                }
            }

        }



        //업데이트가 가능한 상태인가
        $targetData=MarketManagerMo::GetTargetUpdateData($targetIDX);
        if(!isset($targetData['idx'])){
            $errMsg='타겟이 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        $targetGradeIDX = $targetData['gradeIDX'];
        $targetPosition = $targetData['position'];
        $targetName = $targetData['name'];
        $targetStatusIDX=$targetData['statusIDX'];
        $targetGradeName=$targetData['gradeName'];

        if($targetStatusIDX==402202){
            $errMsg='이미 삭제된 이메일 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $step1=0;
        $step2=0;
        $step3=0;

        if($gradeIDX!=$targetGradeIDX){
            $step1=1;
            $ex='grade :  '.$targetGradeName.' -> '.$gradeName.'';
            $logIDX=$this->PortalLogInsert(401201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }

        if($position!=$targetPosition){
            $step2=1;
            $ex='position :  '.$targetPosition.' -> '.$position.'';
            $logIDX=$this->PortalLogInsert(401201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }

        if($name!=$targetName){
            $step3=1;
            $ex='managerName : '.$targetName.' -> '.$name.'';
            $logIDX=$this->PortalLogInsert(401201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0 && $step2==0 && $step3==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $db = static::getDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
            gradeIDX=:gradeIDX,
            position=:position,
            name=AES_ENCRYPT(:name, '$dataDbKey')
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':gradeIDX', $gradeIDX);
        $stat1->bindValue(':position', $position);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        // $logIDX=$this->PortalLogInsert(401201,$targetIDX);
        //포탈로그ex
        // $this->PortalLogExInsert($logIDX,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 마켓 매니저 슈퍼등급 업데이트
    public function MarketManagerGradeUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
            $errMsg='gradeName 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['email'])||empty($_POST['email'])){
            $errMsg='email 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketIDX=$_POST['marketIDX'];
        $gradeIDX=$_POST['gradeIDX'];
        $gradeName=$_POST['gradeName'];
        $email=$_POST['email'];
        $issetSuperGrade=MarketManagerMo::issetSuperManagerGradeData($marketIDX);
        $superIDX=$issetSuperGrade['idx'];
        $superEmail=$issetSuperGrade['email'];
        $superGradeName=$issetSuperGrade['gradeName'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        //슈퍼등급 업데이트
        $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
            gradeIDX=:gradeIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':gradeIDX', 1);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();


        $ex2='grade : '.$gradeName.' -> '.$superGradeName.' , '.$superEmail.' -> '.$email.'';
        $logIDX2=$this->PortalLogInsert(401201,$targetIDX);
        $this->PortalLogExInsert($logIDX2,$superIDX,$targetIDX,$ex2);

        //원래 슈퍼등급이였던 매니저는 하이 클래스로 변경
        $stat2=$db->prepare("UPDATE $dbName.MarketManager SET
            gradeIDX=:gradeIDX
            WHERE idx=:superIDX
        ");
        $stat2->bindValue(':gradeIDX', 2);
        $stat2->bindValue(':superIDX', $superIDX);
        $stat2->execute();

        $ex='grade : '.$superGradeName.' -> '.$gradeName.' , '.$superEmail.' -> '.$email.'';
        $logIDX=$this->PortalLogInsert(401201,$superIDX);
        $this->PortalLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}