<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketManagerGradeMo;
use App\Models\MarketManagerPermissionMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketManagerGradeCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/marketManagerGrade/marketManagerGrade.html');
	}//렌더

    //퍼미션로드
    public function AccessLoad()
    {
        $marketManagerInfo =MarketManagerPermissionMo::GetPermissionList();
        $resultData = ['data'=>$marketManagerInfo,'result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }
    //편집퍼미션로드
    public function AccessForEditLoad()
    {
        $accessMom =MarketManagerPermissionMo::GetPermissionForEditMomList();
        $menuListArr=[];
        foreach ($accessMom as $key ) {
            $momIDX='';
            $accessChildArr=[];
            if(isset($key['momIDX'])||empty($key['momIDX'])){
                $momIDX=$key['momIDX'];
                $accessChildArr=MarketManagerPermissionMo::GetPermissionForEditChildList($momIDX);
            }

            $menuListArr[]=array(
                'momIDX'=>$momIDX,
                'momContent'=>$key['momContent'],
                'momSeq'=>$key['momSeq'],
                'accessChildArr'=>$accessChildArr
            );
        }
        $resultData = ['data'=>$menuListArr,'result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }

	//marketManagerGrade.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=MarketManagerGradeMo::GetDatatableList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 디테일 로드
    public function MarketManagerGradeDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=MarketManagerGradeMo::GetGradeDetail($targetIDX);
        }else if($pageType=='ins'){
        	$targetIDX='';
			$dataPack='';
        }
        $renderData=[
            'targetIDX'=>$targetIDX,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/marketManagerGrade/marketManagerGradeDetail.html',$renderData);

    }

    public function MarketManagerPermissionDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        $targetIDX='';
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
        }
        $pmArr=['pageType'=>$pageType,'gradeIDX'=>$targetIDX];
        $permissionPack=MarketManagerPermissionMo::GetGradePermissionList($pmArr);
        $resultData = ['data'=>$permissionPack,'result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }



    //marketManagerGrade.html 등급 추가
    // 231227(삭제할수가없어서 추가도못하게해버려..)
    // public function MarketManagerGradeInsert()
    // {
    //     if(!isset($_POST['name'])||empty($_POST['name'])){
    //         $errMsg='name 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg,'n');
    //     }
    //     $permissionArr=[];
    //     if(isset($_POST['permissionArr'])||!empty($_POST['permissionArr'])){
    //         $permissionArr=$_POST['permissionArr'];
    //     }
    //     $name=$_POST['name']; $name=trim($name);
    //     $issetNameChk=MarketManagerGradeMo::issetNameData($name);
    //     if(isset($issetNameChk['name'])){
    //         $errMsg='중복된 등급이름 입니다.';
    //         $errOn=$this::errExport($errMsg,'n');
    //     }

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;

    //     $stat1=$db->prepare("INSERT INTO $dbName.MarketManagerGrade
    //         (name)
    //         VALUES
    //         (:name)
    //     ");
    //     $stat1->bindValue(':name', $name);
    //     $stat1->execute();
    //     $gradeIDX = $db->lastInsertId();
    //     $this->StaffLogInsert(327101,$gradeIDX);

    //     foreach ($permissionArr as $key) {
    //         if(
    //             isset($key['idx'])&&!empty($key['idx'])||
    //             isset($key['abled'])&&!empty($key['abled'])
    //         ){
    //             $idx=$key['idx'];
    //             $abled=$key['abled'];
    //             $insert=$db->prepare("INSERT $dbName.MarketManagerPermission
    //                 (permissionLabelIDX,gradeIDX,abled)
    //                 VALUES
    //                 (:idx,:gradeIDX,:abled)
                    
    //             ");
    //             $insert->bindValue(':gradeIDX', $gradeIDX);
    //             $insert->bindValue(':idx', $idx);
    //             $insert->bindValue(':abled', $abled);
    //             $insert->execute();
    //         }
    //     }

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //marketManagerGrade.html 등급 업데이트
    public function MarketManagerGradeUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $permissionArr=[];
        if(isset($_POST['permissionArr'])||!empty($_POST['permissionArr'])){
            $permissionArr=$_POST['permissionArr'];
        }
        $gradeIDX=$_POST['targetIDX'];
        $name=$_POST['name'];

        $issetData=MarketManagerGradeMo::issetMarketManagerGradeData($gradeIDX);
        if(isset($issetData['idx'])){
            $issetIDX=$issetData['idx'];
            $issetName=$issetData['name'];
        }else{
            $errMsg='해당 등급 정보가 없습니다.'.$gradeIDX;
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        foreach ($permissionArr as $key) {
            if(
                isset($key['idx'])&&!empty($key['idx'])||
                isset($key['after'])&&!empty($key['after'])
            ){
                $idx=$key['idx'];
                $after=$key['after'];
                $update=$db->prepare("UPDATE $dbName.MarketManagerPermission SET
                    abled=:after
                    WHERE permissionLabelIDX=:idx AND gradeIDX=:gradeIDX
                ");
                $update->bindValue(':gradeIDX', $gradeIDX);
                $update->bindValue(':idx', $idx);
                $update->bindValue(':after', $after);
                $update->execute();
                $pmTxt='비허용';
                if($after=='Y'){
                    $pmTxt='허용';
                }
                $ex='권한ID '.$idx.' 의 등급권한이 '.$pmTxt.'(으)로 변경되었습니다.';
                $logIDX=$this->StaffLogInsert(327201,$gradeIDX);
                $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
            }
        }

        if($name!=$issetName){
            $stat1=$db->prepare("UPDATE $dbName.MarketManagerGrade SET
                name=:name
                WHERE idx=:gradeIDX
            ");
            $stat1->bindValue(':name', $name);
            $stat1->bindValue(':gradeIDX', $gradeIDX);
            $stat1->execute();
            $ex='등급명이 '.$issetName.'에서 '.$name.'(으)로 변경되었습니다.';
            $logIDX=$this->StaffLogInsert(327201,$gradeIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$issetIDX,$gradeIDX,$ex);
        }

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketManagerGrade.html 권한 편집
    public function PermissonUpdate()
    {
        if(!isset($_POST['seqArr'])&&empty($_POST['seqArr'])&&
            !isset($_POST['textArr'])&&empty($_POST['textArr'])
        ){
            $errMsg='변동사항이 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $textArr=[];
        $seqArr=[];
        if(isset($_POST['textArr'])||!empty($_POST['textArr'])){
            $textArr=$_POST['textArr'];
        }
        if(isset($_POST['seqArr'])||!empty($_POST['seqArr'])){
            $seqArr=$_POST['seqArr'];
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        foreach ($textArr as $key) {
            if(
                isset($key['idx'])&&!empty($key['idx'])||
                isset($key['after'])&&!empty($key['after'])||
                isset($key['before'])&&!empty($key['before'])
            ){
                $idx=$key['idx'];
                $after=$key['after'];
                $before=$key['before'];
                $update=$db->prepare("UPDATE $dbName.MarketManagerPermissionLabel SET
                    content=:after
                    WHERE idx=:idx
                ");
                $update->bindValue(':idx', $idx);
                $update->bindValue(':after', $after);
                $update->execute();
                $pmTxt='비허용';
                if($after=='Y'){
                    $pmTxt='허용';
                }
                $ex='권한('.$idx.') 의 권한내용이 '.$before.' 에서 '.$after.'(으)로 변경되었습니다.';
                $logIDX=$this->StaffLogInsert(327301,0);
                $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
            }
        }

        $momSeq=1;
        foreach ($seqArr as $key) {
            if(isset($key['id'])&&!empty($key['id'])){
                $momIDX=$key['id'];
                $stat1=$db->prepare("UPDATE $dbName.MarketManagerPermissionLabel SET
                    seq=:momSeq,
                    momIDX=0
                    WHERE idx=:momIDX ORDER BY seq
                ");
                $stat1->bindValue(':momSeq', $momSeq);
                $stat1->bindValue(':momIDX', $momIDX);
                $stat1->execute();
                if(isset($key['children'])&&!empty($key['children'])){
                    $childPack=$key['children'];
                    $childSeq=1;
                    foreach ($childPack as $key2) {
                        $childrenIDX=$key2['id'];
                        $stat2=$db->prepare("UPDATE $dbName.MarketManagerPermissionLabel SET
                            seq=:childSeq,
                            momIDX=:momIDX
                            WHERE idx=:childrenIDX ORDER BY seq
                        ");
                        $stat2->bindValue(':childSeq', $childSeq);
                        $stat2->bindValue(':momIDX', $momIDX);
                        $stat2->bindValue(':childrenIDX', $childrenIDX);
                        $stat2->execute();
                        $childSeq+=1;
                    }
                }
                $momSeq+=1;
            }
        }
        if($momSeq>1){
            $ex='권한의 순서가 변경되었습니다.';
            $logIDX=$this->StaffLogInsert(327301,0);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }



}