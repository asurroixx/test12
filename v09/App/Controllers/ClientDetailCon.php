<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientDetailMo;
use App\Models\ClientMarketListMo;
use App\Models\StaffMo;
use App\Models\ChatClientTokenMo;
use App\Models\StaffMemoMo;
use App\Models\ClientAllAmountMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */
class ClientDetailCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

    //렌더
    public static function Render($data=null,$staffIDX=null)
    {

        $clientIDX = $data;
        if($staffIDX!=null){
            //채팅서버에서 씁니다.
            $loginIDX=$staffIDX;
            //채팅서버에서 씁니다.
        }else{
            $loginIDX=GlobalsVariable::GetGlobals('loginIDX');
        }

        //총 마일리지
        $getMileage=ClientDetailMo::getMileage($clientIDX);

        //클라이언트 디테일
        $dataPack=ClientDetailMo::GetClientDetailData($clientIDX);
        $emailPack=ClientDetailMo::GetTargetClientEmailLog($clientIDX);
        $bankPack=ClientDetailMo::GetTargetClientData($clientIDX);

        //클라이언트 입금 내역
        $clientMileageDepositDetailPack=ClientDetailMo::GetTotalClientMileageDeposit($clientIDX);
        $clientMileageDepositPack=ClientDetailMo::GetClientMileageDepositDataTable($clientIDX);

        //클라이언트 출금 내역
        $clientMileageWithdrawalDetailPack=ClientDetailMo::GetTotalClientMileageWithDrawal($clientIDX);
        $clientMileageWithdrawalPack=ClientDetailMo::GetClientMileageWithdrawalDataTable($clientIDX);

        //클라이언트 히스토리
        $clientHistory=ClientDetailMo::GetClientLogList($clientIDX);

        //클라이언트 메모 내역
        $statusIDX=371101;
        $arr=['targetIDX'=>$clientIDX,'statusIDX'=>$statusIDX,'staffIDX'=>$loginIDX];
        $memoData=StaffMemoMo::GetMemoList($arr);

        //클라이언트 마일리지 변동내역
        $clientMileageChangeHistory=ClientDetailMo::GetClientMileageVariationHistoryTable($clientIDX);

        //클라이언트 마켓리스트 이메일 구하기
        $clientMarketList=ClientMarketListMo::GetClientMarketListData($clientIDX);
        $referenceIdArr = array();
        $referenceIds = array();
        //중복되는 이메일은 제거
        foreach ($clientMarketList as $data) {
            $referenceId = $data['referenceId'];
            if (!in_array($referenceId, $referenceIds)) {
                $referenceIds[] = $referenceId;
                $referenceIdArr[] = $data;
            }
        }


        //클라이언트 지불대행 내역
        $clientDepositDetailPack=ClientDetailMo::GetTotalClientDeposit($clientIDX);
        $clientDepositPack='';
        if(!empty($referenceIdArr)){
            $clientDepositPack=ClientDetailMo::GetClientDepositDataTable($referenceIdArr);
        }


        //클라이언트 출금대행 내역
        $clientWithdrawalDetailPack=ClientDetailMo::GetTotalClientWithdrawal($clientIDX);
        $clientWithdrawalPack='';
        if(!empty($referenceIdArr)){
            $clientWithdrawalPack=ClientDetailMo::GetClientWithdrawalDataTable($referenceIdArr);
        }


        //P2P전자화폐 계정리스트

        $clientPtpAccountPack=ClientDetailMo::GetClientPtpAccountList($clientIDX);
        //P2P구매내역
        $clientPtpData=ClientDetailMo::GetClientPeerToPeerData($clientIDX);

        $ClientAllAmountSumData=ClientAllAmountMo::ClientAllAmountSumData($clientIDX);
        $ClientAllAmountData=ClientAllAmountMo::ClientAllAmountData($clientIDX);

        $renderData=[
            'targetIDX'=>$clientIDX,
            'getMileage'=>$getMileage,
            'dataPack'=>$dataPack,
            'emailPack'=>$emailPack,
            'bankPack'=>$bankPack,
            'memoData'=>$memoData,
            'clientMileageDepositDetailPack'=>$clientMileageDepositDetailPack,
            'clientMileageDepositPack'=>$clientMileageDepositPack,
            'clientMileageWithdrawalDetailPack'=>$clientMileageWithdrawalDetailPack,
            'clientMileageWithdrawalPack'=>$clientMileageWithdrawalPack,
            'clientHistory'=>$clientHistory,
            'clientMileageChangeHistory'=>$clientMileageChangeHistory,
            'clientMarketList'=>$clientMarketList,
            'clientDepositDetailPack'=>$clientDepositDetailPack,
            'clientDepositPack'=>$clientDepositPack,
            'clientWithdrawalDetailPack'=>$clientWithdrawalDetailPack,
            'clientWithdrawalPack'=>$clientWithdrawalPack,
            'clientPtpAccountPack'=>$clientPtpAccountPack,
            'clientPtpData'=>$clientPtpData,
            'ClientAllAmountSumData'=>$ClientAllAmountSumData,
            'ClientAllAmountData'=>$ClientAllAmountData,
        ];
        View::renderTemplate('page/clientDetail/clientDetail.html',$renderData);
    }


    public static function RenderFromChat($data=null)
    {
        if(!isset($_POST['staffIDX']) || empty($_POST['staffIDX'])){
            $errMsg='필수값이 없습니다';
            $errOn=$this::errExport($errMsg);
        }
         if(!isset($_POST['clientIDX']) || empty($_POST['clientIDX'])){
            $errMsg='필수값이 없습니다';
            $errOn=$this::errExport($errMsg);
        }
        $staffIDX=$_POST['staffIDX'];
        $clientIDX=$_POST['clientIDX'];

        static::Render($clientIDX,$staffIDX);
    }

}