<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StatusMo;
use App\Models\MarketMo;
use App\Models\MarketSettlementMo;
use App\Models\MarketSettlementBankwireMo;
use App\Models\MarketSettlementCryptoWalletMo;
use App\Models\MarketSettlementKRWMo;
use App\Models\StaffMemoMo;
use App\Models\PortalLogMo;


use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class StaffMemoCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	

    public function MemoListLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $loginIDX=GlobalsVariable::GetGlobals('loginIDX');


        $arr=['targetIDX'=>$targetIDX,'statusIDX'=>$statusIDX,'staffIDX'=>$loginIDX];

        $memoData=StaffMemoMo::GetMemoList($arr);
        $resultData = ['result'=>'t','data'=>$memoData];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }

    public function MemoInsert()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['memoVal'])||empty($_POST['memoVal'])){
            $errMsg='memoVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $memoVal=trim($_POST['memoVal']);
        $db = static::getDB();
        $dbName= self::MainDBName;
        $loginIDX=GlobalsVariable::GetGlobals('loginIDX');
        $createTime=date("Y-m-d H:i:s");
        $ipAddress = static::GetIPaddress();
        $insert=$db->prepare("INSERT INTO $dbName.StaffMemo
            (
                statusIDX,
                staffIDX,
                targetIDX,
                memo,
                ip,
                createTime
            )
            VALUES
            (
                :statusIDX,
                :staffIDX,
                :targetIDX,
                :memoVal,
                :ipAddress,
                :createTime
            )
        ");
        $insert->bindValue(':statusIDX', $statusIDX);
        $insert->bindValue(':staffIDX', $loginIDX);
        $insert->bindValue(':targetIDX', $targetIDX);
        $insert->bindValue(':memoVal', $memoVal);
        $insert->bindValue(':ipAddress', $ipAddress);
        $insert->bindValue(':createTime', $createTime);
        $insert->execute();
        $resultData = ['result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }



    public function MemoDelete()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $db = static::getDB();
        $dbName= self::MainDBName;
        $position = 3;
        $replacementChar = '2';
        if ($position < strlen($statusIDX)) {
            $statusIDX[$position] = $replacementChar;
        }
        $inactiveStatus=$statusIDX;

        $update =$db->prepare("UPDATE $dbName.StaffMemo SET
            statusIDX = :inactiveStatus
            WHERE idx = :targetIDX
        ");
        $update->bindValue(':inactiveStatus', $inactiveStatus);
        $update->bindValue(':targetIDX', $targetIDX);
        $update->execute();
        $resultData = ['result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }



}