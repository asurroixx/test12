<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\QnaMo;
use App\Models\QnaAnswerMo;
use App\Models\QnaCategoryMo;
use App\Models\EmailMsgMo;
use App\Models\MarketManagerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class QnaCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */


	public function Render($data=null)
	{
        $FileMaxSize=self::FileMaxSize;
        $FileAbledExt=self::FileAbledExt;
        $FileAbledExt = implode(', ', $FileAbledExt);
        $FileAbledCount=self::FileAbledCount;

		$marketPack=MarketMo::GetMarketAllListData();
		$renderData=[
			'marketPack'=>$marketPack,
            'FileMaxSize'=>$FileMaxSize,
            'FileAbledExt'=>$FileAbledExt,
            'FileAbledCount'=>$FileAbledCount,
		];
		View::renderTemplate('page/qna/qna.html',$renderData);
	}//렌더

    //qna.html 날짜별 리셋 데이터
    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=QnaMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna.html 솔팅별 리셋 데이터
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $columnsVal=$_POST['columnsVal'];
        $totalCountData=QnaMo::GetTotalSortingData($columnsVal);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	//qna.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=QnaMo::DataTableListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna.html 해당 채팅
    public function ChattingFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=QnaAnswerMo::GetContentData($targetIDX);
        $detailPack=QnaMo::GetDetailData($targetIDX);
        $cateArr    = '';
        $isImage    = ''; // 이 qna에 이미지가 있는가?

        $imgFileIDXArr = [];
        foreach ($dataPack as $data) {
            $ext = $data['ext'];

            // 'jpg', 'jpeg', 'png' 중 하나인지 확인
            if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif', 'txt', 'pdf'])) {
                // 조건을 만족하는 경우의 처리
                $isImage = 't';
                $fileIDX = $data['fileIDX'];
                if($fileIDX!==null){
                    $imgFileIDXArr[]=$fileIDX;
                }
            }
        }

        if($isImage === 't'){
            // 썸네일 들고오기
            $thumbnailPack = self::fileDownload($imgFileIDXArr);
            $thumbnailPack = $thumbnailPack['data'];

            // 말풍선에 썸네일 입히기
            $thumbnailMap = [];
            foreach ($thumbnailPack as $thumbnail) {
                $thumbnailMap[$thumbnail['idx']] = $thumbnail['uri'];
            }

            foreach ($dataPack as &$data) {
                $idx = $data['idx'];
                if (isset($thumbnailMap[$idx])) {
                    $data['srcFileName'] = $thumbnailMap[$idx];
                }
            }
        }



        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'detailPack'=>$detailPack,

        ];
        View::renderTemplate('page/qna/chatting.html',$renderData);
    }

    //qna.html 해당 디테일
    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=QnaMo::GetDetailData($targetIDX);
        $marketManagerIDX=$dataPack['marketManagerIDX'];
        $beforeArr=['targetIDX'=>$targetIDX,'marketManagerIDX'=>$marketManagerIDX];
        $beforeData=QnaMo::GetBeforeData($beforeArr);
        // 디테일 카테고리
        $tmpCateList_1=0;
        $tmpCateList_2=0;
        $tmpCate1IDX=0;
        $tmpCate2IDX=0;
        $tmpCate1param = array(
            'parentIDX'=>'',
            'type'=>1,
        );
        $tmpCateList_1 = QnaCategoryMo::GetCategoryListForParentIDX($tmpCate1param);

        $qnaCategoryIDX=$dataPack['qnaCategoryIDX'];
        $tmpCategoryData = QnaCategoryMo::GetCategoryDataForIDX($qnaCategoryIDX);
        $cateIDX=$tmpCategoryData['idx'];//
        $cateMomIDX=$tmpCategoryData['momIDX'];
        if($cateMomIDX=='0'){//1차라는 소리: 1차리스트만 뽑으면 됨
            $tmpCate1IDX=$qnaCategoryIDX;
        }else{
            $tmpCate1IDX=$cateMomIDX;
            $tmpCate2IDX=$qnaCategoryIDX;
            $tmpCate2param = array(
                'parentIDX' => $cateMomIDX,
                'type' => 2,
            );
            $tmpCateList_2 = QnaCategoryMo::GetCategoryListForParentIDX($tmpCate2param);
        }
        // 디테일 카테고리
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'beforeData'=>$beforeData,
            'cate1IDX'=>$tmpCate1IDX,
            'cate2IDX'=>$tmpCate2IDX,
            'cateList_1'=>$tmpCateList_1,
            'cateList_2'=>$tmpCateList_2,
        ];
        View::renderTemplate('page/qna/qnaDetail.html',$renderData);
    }

    //qna.html 말풍선 보내기 스태프 > 포탈
    public function QnaContentInsert()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['completeChk'])||empty($_POST['completeChk'])){
            $errMsg='completeChk 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $content=$_POST['content'];
        $completeChk=$_POST['completeChk'];
        $statusIDX=341101;
        $createTime=date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        $targetData=QnaMo::GetTargetIssetData($targetIDX);
        if(isset($targetData['idx'])&&!empty($targetData['idx'])){
            $targetStatusIDX=$targetData['statusIDX'];
        }
        $marketIDX=$targetData['marketIDX'];
        $email=$targetData['email'];
        $sandArr=['marketIDX'=>$marketIDX,'email'=>$email];
        $sandboxManagerData=MarketManagerMo::getMarketManagerIDXInSandbox($sandArr);
        $marketData=MarketMo::MarketStatusData($marketIDX);
        if(!isset($marketData['idx'])||empty($marketData['idx'])){
            $errMsg='marketidx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketName=$marketData['name'];

        //1222 포탈,샌드박스 알람 부분
        if($targetStatusIDX==340101){
            $errMsg='이미 답변완료된 상태입니다.';
            $errOn=$this::errExport($errMsg);
        }


        //파일 유효성 체크

        if(isset($_FILES['files'])){
            $filesArr = $_FILES['files'];
            $fileUploadChk = $this::fileUploadChk($_FILES['files']);
        }
        //파일 유효성 체크





        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.QnaAnswer
            (qnaIDX,content,statusIDX,createTime,staffIDX)
            VALUES
            (:targetIDX,:content,:statusIDX,:createTime,:staffIDX)
        ");

        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':content', $content);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':staffIDX', $staffIDX);
        $stat1->execute();




        //파일이 있다면 파일을 보낼것
        if(isset($_FILES['files'])){
            $fileUpload = $this::fileUpload($_FILES['files'],'qna');//파일업로드
            $db = static::getDB();
            $dbName= self::MainDBName;
            $fileAnswer = 341102; // 파일 저장 스테이터스
            $createTime = date("Y-m-d H:i:s");

            foreach ($fileUpload as $key ) {
                $thisOrignName  = $key['orignName'];
                $thisServerName = $key['serverName'];
                $thisExt        = $key['ext'];

                //파일 말풍선
                $stat2=$db->prepare("INSERT INTO $dbName.QnaAnswer
                    (qnaIDX,content,statusIDX,createTime)
                    VALUES
                    (:qnaIDX,:content,:statusIDX,:createTime)
                ");

                $stat2->bindValue(':qnaIDX', $targetIDX);
                $stat2->bindValue(':content', $thisOrignName);
                $stat2->bindValue(':statusIDX', $fileAnswer);//말풍선 추가
                $stat2->bindValue(':createTime', $createTime);
                $stat2->execute();
                $answerIDX = $db->lastInsertId();
                $stat2=$db->prepare("INSERT INTO $dbName.Files
                    (serverName,orignName,ext,targetIDX,statusIDX,createIDX,createTime)
                    VALUES
                    (:serverName,:orignName,:ext,:targetIDX,:statusIDX,:createIDX,:createTime)
                ");
                $stat2->bindValue(':serverName', $thisServerName);
                $stat2->bindValue(':orignName', $thisOrignName);
                $stat2->bindValue(':ext', $thisExt);
                $stat2->bindValue(':targetIDX', $answerIDX);
                $stat2->bindValue(':statusIDX', $fileAnswer);//말풍선 추가
                $stat2->bindValue(':createIDX', $staffIDX);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->execute();
            }
        }
        //파일이 있다면 파일을 보낼것



        $ticketCode=$targetData['ticketCode'];


        $devMarketManagerIDX=$sandboxManagerData['idx'];


        if($completeChk=='Y'){
            $systemParam=[
                'targetIDX'=>$targetIDX,
                'marketIDX'=>$marketIDX,
                'ticketCode'=>$ticketCode,
                'createTime'=>$createTime,
                'staffIDX'=>$staffIDX,
                'devMarketManagerIDX'=>$devMarketManagerIDX
            ];
            $systemInsert=$this->systemContentInsert($systemParam);

            // 타겟팅 이메일 보내줘야됨

            //이메일을 보내주자
            $mailStatusIDX=340101;
            $idx = $targetData['marketManagerIDX'];
            $email = $targetData['email'];
            $managerParam=['idx'=>$idx,'email'=>$email];
            $managerPack=[$managerParam];
            // $emailParamVal = ['marketname'=>$marketName,'cont'=>$marketName];
            $emailParamVal = [''];
            $emailParam=[
                'targetIDX'=>0,
                'statusIDX'=>$mailStatusIDX,
                'marketIDX'=>0, //모든 매니저에게 다 보내려면 0으로 하고 type all로
                'type'=>'target',
                'idxPack'=>$managerPack, //idx와 email을 다보내주세요. ex)[[idx=>1, email=>dfdf@fdfd.com]]
                'param'=>$emailParamVal,
            ];
            $emailGo = StaffEmailCon::sendEmailToPortal($emailParam);


        }



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna.html 답변완료 버튼 클릭 시
    public function QnaContentComplete()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $targetData=QnaMo::GetTargetIssetData($targetIDX);
        if(isset($targetData['idx'])&&!empty($targetData['idx'])){
            $targetStatusIDX=$targetData['statusIDX'];
        }
        $marketIDX=$targetData['marketIDX'];
        $email=$targetData['email'];
        $sandArr=['marketIDX'=>$marketIDX,'email'=>$email];
        $sandboxManagerData=MarketManagerMo::getMarketManagerIDXInSandbox($sandArr);
        $devMarketManagerIDX=$sandboxManagerData['idx'];

        $ticketCode=$targetData['ticketCode'];

        if($targetStatusIDX==340101){
            $errMsg='이미 답변완료된 상태입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $statusIDX=$_POST['statusIDX'];
        $createTime=date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Qna SET
            statusIDX=:statusIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $systemParam=[
            'targetIDX'=>$targetIDX,
            'marketIDX'=>$marketIDX,
            'ticketCode'=>$ticketCode,
            'createTime'=>$createTime,
            'staffIDX'=>$staffIDX,
            'devMarketManagerIDX'=>$devMarketManagerIDX,
        ];
        $systemInsert=$this->systemContentInsert($systemParam);

        //이메일을 보내주자
        $statusIDX=340101;
        $idx = $targetData['marketManagerIDX'];
        $email = $targetData['email'];
        $managerParam=['idx'=>$idx,'email'=>$email];
        $managerPack=[$managerParam];
        $emailParamVal = [];
        $emailParam=[
            'targetIDX'=>0,
            'statusIDX'=>$statusIDX,
            'marketIDX'=>0, //모든 매니저에게 다 보내려면 0으로 하고 type all로
            'type'=>'target',
            'idxPack'=>$managerPack, //idx와 email을 다보내주세요. ex)[[idx=>1, email=>dfdf@fdfd.com]]
            'param'=>$emailParamVal,
        ];
        $emailGo = StaffEmailCon::sendEmailToPortal($emailParam);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

    }

    public function systemContentInsert($data=[]){
        $targetIDX=$data['targetIDX'];
        $marketIDX=$data['marketIDX'];
        $ticketCode=$data['ticketCode'];
        $createTime=$data['createTime'];
        $staffIDX=$data['staffIDX'];
        $devMarketManagerIDX=$data['devMarketManagerIDX'];

        //답변완료 할때마다 시스템 언어 추가
        $completeAnswer='Answer is Complete';
        $completeStaffAnswer='답변이 완료되었습니다';
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.QnaAnswer
            (qnaIDX,content,staffContent,statusIDX,createTime,staffIDX)
            VALUES
            (:targetIDX,:content,:staffContent,:statusIDX,:createTime,:staffIDX)
        ");

        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':content', $completeAnswer);
        $stat1->bindValue(':staffContent', $completeStaffAnswer);
        $stat1->bindValue(':statusIDX', 341103);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':staffIDX', $staffIDX);
        $stat1->execute();


        $stat2=$db->prepare("UPDATE $dbName.Qna SET
            statusIDX=:statusIDX
            WHERE idx=:targetIDX
        ");
        $stat2->bindValue(':statusIDX', 340101);
        $stat2->bindValue(':targetIDX', $targetIDX);
        $stat2->execute();




        $diffTicket=substr($ticketCode, 0, 2);// 앞 두글자에 따라 알람을 보내는게 다름
        if($diffTicket=='PO'){
            $alarmParam=[
                'targetIDX'=>$targetIDX,
                'statusIDX'=>340101,
                'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
                'param'=>[]
            ];
            PortalAlarmCon::sendAlarmToPortal($alarmParam);
        }elseif($diffTicket=='SA'){
            $alarmParam=[
                'targetIDX'=>$targetIDX,
                'statusIDX'=>340101,
                'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
                'param'=>[]
            ];
            PortalAlarmCon::sendAlarmToPortal($alarmParam);

            //샌드박스 문의를 따로 분류 해야됨

            $alarmParam=[
                'targetIDX'=>$targetIDX,
                'statusIDX'=>340101,
                'marketIDX'=>$devMarketManagerIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
                'param'=>[]
            ];
            DevAlarmCon::sendAlarmToDev($alarmParam);
        }
    }

    /* ---------------------------------------카테고리 영역--------------------------------------------- */

    //qna.html 카테고리 컨펌 로드
    public function QnaCategoryConfirmLoad()
    {
        View::renderTemplate('page/qna/categoryForm.html');
    }

    //categoryForm.html 카테고리 리스트
    public function QnaCategoryFormList()
    {
        $categoryList = QnaCategoryMo::GetCategoryList();
        $categoryListArr=[];
        foreach ($categoryList as $key ) {
            $idx=$key['idx'];
            $momIDX=$key['momIDX'];
            $cateName=$key['name'];
            $seq=$key['seq'];
            $statusIDX=$key['statusIDX'];
            $childCategoryList = QnaCategoryMo::GetChildCategoryList($idx);
            $childCategoryListArr=[];
            foreach ($childCategoryList as $key) {
                $childIDX=$key['idx'];
                $childCateName=$key['name'];
                $childStatusIDX=$key['statusIDX'];

                $childCategoryListArr[]=array(
                    'childIDX'=>$childIDX,
                    'childCateName'=>$childCateName,
                    'childStatusIDX'=>$childStatusIDX
                );
            }

            $categoryListArr[]=array(
                'idx'=>$idx,
                'momIDX'=>$momIDX,
                'name'=>$cateName,
                'seq'=>$seq,
                'statusIDX'=>$statusIDX,
                'childCategoryListArr'=>$childCategoryListArr
            );
        }

        $menuResult = ['result'=>'t','data'=>$categoryListArr];
        $result=json_encode($menuResult,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html seq 수정
    public function MenuUpdate()
    {
        $dbName=self::MainDBName;
        $dataArr=[];
        if(isset($_POST['data'])){
            $dataArr=$_POST['data'];
        };

        $momSeq=1;

        foreach ($dataArr as $key) {
            if(
                isset($key['id'])&&!empty($key['id'])
            ){
                $momIDX=$key['id'];
                $db = static::getDB();
                $stat1=$db->prepare("UPDATE $dbName.QnaCategory SET
                    seq='$momSeq',
                    momIDX=0
                    WHERE idx='$momIDX' AND statusIDX=345401 ORDER BY seq
                ");
                $stat1->execute();

                if(
                    isset($key['children'])&&!empty($key['children'])
                ){

                    $childPack=$key['children'];
                    $childSeq=1;
                    foreach ($childPack as $key2) {
                        $childrenIDX=$key2['id'];
                        $db = static::getDB();
                        $stat2=$db->prepare("UPDATE $dbName.QnaCategory SET
                            seq='$childSeq',
                            momIDX='$momIDX'
                            WHERE idx='$childrenIDX' AND statusIDX=345401 ORDER BY seq
                        ");
                        $stat2->execute();
                        $childSeq+=1;
                    }
                }
                $momSeq+=1;
            }
        }

        $dataArr = array(
            'result'=>'t'
        );
        echo json_encode($dataArr,JSON_UNESCAPED_UNICODE);

    }

    //categoryForm.html 카테고리 추가
    public function QnaCategoryInsert()
    {
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $name=$_POST['name'];
        $name=trim($name);
        $statusIDX=345401;
        $issetName=QnaCategoryMo::GetIssetNameData($name);
        if(isset($issetName['name'])&&!empty($issetName['name'])){
            $errMsg='이미 등록된 카테고리 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.QnaCategory
            (momIDX,name,seq,statusIDX)
            VALUES
            (:momIDX,:name,:seq,:statusIDX)
        ");

        $stat1->bindValue(':momIDX', 0);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':seq', 0);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $logIns=$this->StaffLogInsert(345101,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 메뉴 숨김
    public function CheeseEyeAction()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $dbName=self::MainDBName;
        $targetIDX=$_POST['targetIDX'];
        $menuList = QnaCategoryMo::CheeseEyeAction($targetIDX);
        $idx=$menuList['idx'];
        $statusIDX=$menuList['statusIDX'];
        $db = static::getDB();
        if($statusIDX==345501){
            $stat1=$db->prepare("UPDATE $dbName.QnaCategory SET
                statusIDX=345401
                WHERE idx='$idx'
            ");
            $stat1->execute();
        }elseif($statusIDX==345401){
             $stat2=$db->prepare("UPDATE $dbName.QnaCategory SET
                statusIDX=345501
                WHERE idx='$idx'
            ");
            $stat2->execute();
        }
        if($statusIDX==345401){
            $statusVal=345501;
        }else{
            $statusVal=345401;
        }

        $logIns=$this->StaffLogInsert($statusVal,$targetIDX);

        $result=['result'=>'t'];
        $result=json_encode($result,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 해당 카테고리명 가져올때
    public function GetTargetNameData()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $namePack = QnaCategoryMo::GetTargetData($targetIDX);
        $result=['result'=>'t','namePack'=>$namePack];
        $result=json_encode($result,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 카테고리 수정
    public function QnaCategoryUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];
        $name=trim($name);
        $issetName=QnaCategoryMo::GetIssetNameData($name);
        if(isset($issetName['name'])&&!empty($issetName['name'])){
            $errMsg='이미 등록된 카테고리 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.QnaCategory SET
            name=:name
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $logIns=$this->StaffLogInsert(345201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 메뉴 삭제
    public function QnaCategoryDelete()
    {
        $momIDX='';
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $DeleteData=QnaCategoryMo::CategoryDeleteCheck($targetIDX);
        if(isset($DeleteData['momIDX'])){
            $momIDX=$DeleteData['momIDX'];
        }
        if($targetIDX==$momIDX){
            $errMsg='하위 메뉴가 등록되있어 삭제가 불가합니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("
            DELETE FROM $dbName.QnaCategory WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $logIns=$this->StaffLogInsert(345301,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna.html 카테고리 select Change시
    public function CategorySelectChange($data=null)
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['type'])||empty($_POST['type'])){
            $errMsg='type 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $tmpParentIDX=$_POST['idx'];
        $tmpType=$_POST['type'];
        $tmpCate2param = array(
            'parentIDX' => $tmpParentIDX,
            'type' => $tmpType,
        );
        $tmpResultData = QnaCategoryMo::GetCategoryListForParentIDX($tmpCate2param);
        $resultData = ['result'=>'t','data'=>$tmpResultData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qnaDetail.html 디테일 카테고리 수정
    public function DetailQnaCategoryChange()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['qnaCategoryIDX'])||empty($_POST['qnaCategoryIDX'])){
            $errMsg='qnaCategoryIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $qnaCategoryIDX=$_POST['qnaCategoryIDX'];
        $issetData=QnaCategoryMo::GetTargetData($qnaCategoryIDX);
        if(!isset($issetData['idx'])&&empty($issetData['idx'])){
            $errMsg='해당 카테고리가 존재하지 않습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $changeCateName=$issetData['name'];
        $targetData=QnaMo::GetTargetIssetData($targetIDX);
        $targetCategoryIDX=$targetData['qnaCategoryIDX'];
        $targetCategoryName=$targetData['categoryName'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Qna SET
            qnaCategoryIDX=:qnaCategoryIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':qnaCategoryIDX', $qnaCategoryIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $systemContent='Category changed from '.$targetCategoryName.' → '.$changeCateName.'';
        $staffContent='카테고리를 '.$targetCategoryName.'에서 '.$changeCateName.'(으)로 변경됐습니다';

        $createTime=date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat2=$db->prepare("INSERT INTO $dbName.QnaAnswer
            (qnaIDX,content,staffContent,statusIDX,createTime,staffIDX)
            VALUES
            (:qnaIDX,:content,:staffContent,:statusIDX,:createTime,:staffIDX)
        ");

        $stat2->bindValue(':qnaIDX', $targetIDX);
        $stat2->bindValue(':content', $systemContent);
        $stat2->bindValue(':staffContent', $staffContent);
        $stat2->bindValue(':statusIDX', 341103);
        $stat2->bindValue(':createTime', $createTime);
        $stat2->bindValue(':staffIDX', $staffIDX);
        $stat2->execute();




        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    // 파일 다운 및 띄우기
    public function FileDownOrImageView()
    {
        $fileIDX = $_POST['fileIDX'] ?? '';
        if(!$fileIDX) $this::errExport('비 정상적인 접근입니다.');

        // 파일 가져오기
        $fileDownLoadPack = self::fileDownload([$fileIDX]);
        $filePack = $fileDownLoadPack['data'];
        // $parts = explode('/', $fileUri);
        // $fileName = end($parts);
        $ext     = $filePack[0]['ext'];
        $fileUri = $filePack[0]['uri'];
        $type    = 'file';

        // // 이미지인가?
        if (in_array($ext, ['jpg', 'jpeg', 'gif', 'png', 'txt', 'pdf'])) {
            $type = 'img';

        }else{
            // header('Content-Type: application/octet-stream');
            // header('Content-Disposition: attachment; filename="' . $fileName . '"');
            // header('Content-Length: ' . strlen($fileContent));
            // header('Content-Transfer-Encoding: binary');

            // echo $fileContent;
            // exit;
        }

         $result  = ['result' => 't','uri' => $fileUri, 'type' => $type];
            echo json_encode($result,JSON_UNESCAPED_UNICODE);


        // if($fileName!="" && $filePath!="" && $fakeName!=""){
        //     $fileFullPath=$filePath.'/'.$fileName;

        // }
    }

}