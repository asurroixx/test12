<?php

namespace App\Controllers;

use \Core\View;
use App\Models\AllApiLogMo;
use App\Models\MarketMo;
use App\Models\ClientMarketListMo;


class ApiAllLogCon extends \Core\Controller
{
    public function Render($data=null)
    {
        $marketListData=MarketMo::GetMarketListData();
        $arr = ['marketListData'=>$marketListData];
        View::renderTemplate('page/apiAllLog/apiAllLog.html',$arr);
    }

    public function GetMarketAllApiLogData($data=null)
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $this::errExport('Unauthorized Access Detected.');
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $this::errExport('Unauthorized Access Detected.');
        }

        $startDate  = $_POST['startDate'] ?? '';
        $endDate    = $_POST['endDate'] ?? '';
        
        $dataArr=array(
            'startDate'  => $startDate,
            'endDate'    => $endDate,
        );

        $dataPack   = AllApiLogMo::GetMarketAllApiData($dataArr);

        // $namePack = ClientMarketListMo::GetClientNamePack();

        // // 이름넣기
        // $result = [];
        // foreach ($dataPack as &$key) {
        //     $matchingCode    = $key['marketCode'];
        //     $marketName            = '-';
        //     foreach ($namePack as $item) {
        //             if ($item['code'] == $matchingCode) {
        //             $marketName = $item['name'];
        //         }
        //     }
        //     $key['marketName']       = $marketName;

        //     $result[] = $key;
        // }


        $resultData = ['result'=>'t','data'=>$dataPack];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);

    }

}