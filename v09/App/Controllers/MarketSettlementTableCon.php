<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketSettlementCryptoWalletMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketSettlementTableCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		// $marketIDX=28;
        $type = 'A';
        $dataPack = MarketMo::GetMarketSortingListData($type);
        if (isset($_GET['marketIDX']) && !empty($_GET['marketIDX'])) {
            $marketIDX = $_GET['marketIDX'];
        } else {
            // $_GET['marketIDX']가 설정되어 있지 않거나 비어있는 경우, 데이터의 첫 번째 마켓의 idx 가져오기
            $marketIDX = !empty($dataPack) ? $dataPack[0]['idx'] : null;
        }
		$renderData=['dataPack'=>$dataPack,'marketIDX'=>$marketIDX];
		View::renderTemplate('page/marketSettlementTable/marketSettlementTable.html',$renderData);
	}//렌더

    //marketSettlementTable.html 마켓 솔팅
    public function MarketSorting()
    {
        if(!isset($_POST['type'])||empty($_POST['type'])){
            $errMsg='type 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $type=$_POST['type'];

        $dataPack=MarketMo::GetMarketSortingListData($type);
        $marketIDX = !empty($dataPack) ? $dataPack[0]['idx'] : null;
        $resultData = ['data'=>$dataPack,'marketIDX'=>$marketIDX,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketSettlementTable.html 데이터테이블
    public function DataTableListLoad()
    {
        $dataPack=MarketSettlementCryptoWalletMo::GetDataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketSettlementTable.html 디테일 로드
    public function MarketSettlementTableDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=MarketSettlementCryptoWalletMo::GetDetailLoad($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/marketSettlementTable/marketSettlementTableDetail.html',$renderData);
    }


}