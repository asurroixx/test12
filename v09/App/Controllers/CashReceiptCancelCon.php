<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientCashCancelReceiptMo;
use App\Models\MarketDepositLogMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class CashReceiptCancelCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		View::renderTemplate('page/cashReceiptCancel/cashReceiptCancel.html');
	}

	public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=ClientCashCancelReceiptMo::GetDataTableListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientCashCancelReceiptMo::GetDetail($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='디테일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $detailPack   = MarketDepositLogMo::GetTargetDepositData($dataPack['targetIDX']);
        if(!$detailPack) $this::errExport('거래 디테일 정보가 없습니다.');
        // 에러 메시지
        $log               = $detailPack['log'] ?? '';
        $logDecode         = json_decode($log, true);
        $detailPack['msg'] = $logDecode['msg'] ?? '';
        $renderData=[
            'info'=>$dataPack,
            'data'=>$detailPack,
        ];
        View::renderTemplate('page/cashReceiptCancel/cashReceiptCancelDetail.html',$renderData);
    }

    public function PowerTryAgain()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['reIDX'])||empty($_POST['reIDX'])){
            $errMsg='reIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $reIDX=$_POST['reIDX'];
        $getdata=ClientCashCancelReceiptMo::GetDetail($targetIDX);
        if(!isset($getdata['idx'])||empty($getdata['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['statusIDX'])||empty($getdata['statusIDX'])){
            $errMsg='해당 정보의 상태가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($getdata['statusIDX']!=811902 AND $getdata['statusIDX']!=811903){
            $errMsg='발행실패 상태인 건만 재발행이 가능합니다. 현재상태 : '.$getdata['statusIDX'].' (811902, 811903 가능)';
            $errOn=$this::errExport($errMsg);
        }

        $nowStatusIDX=$getdata['statusIDX'];
        $clientIDX=$getdata['clientIDX'];
        $nextStatus=811701;

        $nowTime=date("Y-m-d H:i:s");
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $stat1=$db->prepare("UPDATE $dbName.ClientCashCancelReceipt SET
            statusIDX=:statusIDX
            ,resultTime=:resultTime
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $nextStatus);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':resultTime', $nowTime);
        $stat1->execute();

        $logIDX=static::ClientLogInsert($nextStatus,$reIDX,$clientIDX);
        $ex=$nowStatusIDX.'상태에서 재발행 시작함';
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function getFulldata()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getdata=ClientCashCancelReceiptMo::GetFulldata($targetIDX);
        if(!isset($getdata[0]['idx'])||empty($getdata[0]['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        function unicode_decode($str) {
            return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($matches) {
                return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16BE');
            }, $str);
        }
        $resultData = ['result'=>'t','data'=>unicode_decode(stripslashes(json_encode($getdata,JSON_UNESCAPED_UNICODE)))];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}