<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMileageDepositMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MileageBlockCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//mileageBlock.html 렌더
	public function Render($data=null)
	{
		View::renderTemplate('page/mileageBlock/mileageBlock.html');
	}

	//mileageBlock.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=ClientMileageDepositMo::GetUserAndCronCancelList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileage.html 해당 거래 디테일 로드
    public function MileageBlockDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMileageDepositMo::GetTargetUserAndCronCancelData($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/mileageBlock/mileageBlockDetail.html',$renderData);
    }

    //mileageBlock.html 공통 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

    //mileageBlock.html 2건이상 데이터 기타취소로 바꿔주기
    public function DepositUpdate($data=null){
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX = $_POST['targetIDX'];
        $dataPack = ClientMileageDepositMo::GetTargetUserAndCronCancelData($targetIDX);

        $loginEmail=GlobalsVariable::GetGlobals('loginEmail');
        $loginName=GlobalsVariable::GetGlobals('loginName');


        $db = static::getDB();
        $dbName= self::MainDBName;


        $dataArr=[];
        foreach ($dataPack as $key) {
            $depositIDX = $key['idx'];
            $depositAmount = $key['amount'];
            $depositStatus = $key['status'];
            $depositTime = $key['createTime'];
            $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                statusIDX=:statusIDX
                WHERE idx=:idx
            ");
            $stat1->bindValue(':statusIDX', 203211);
            $stat1->bindValue(':idx', $depositIDX);
            $stat1->execute();
            $dataArr[] = $depositIDX;
            // $ex='스태프에 의한 취소 상태 변경 [ 신청시간(KST) : '.$depositTime.' | 금액 : '.$depositAmount.' | 이전상태 : '.$depositStatus.' ] / 관리자 : '.$loginName.' ('.$loginEmail.')';
            // $logIDX=$this->ClientLogInsert(203211,$depositIDX,$targetIDX);
            // $logEx=$this->ClientLogExInsert($logIDX,0,0,$ex);
        }

        //foreach에 depositIDX를 넣어준다 (1개만 쌓는걸로)
        $ex2='스태프에 의한 입금 금지 상태 해제 ('.implode(', ', $dataArr).')/ 처리관리자 : '.$loginName.' ('.$loginEmail.')';
        $logIDX2=$this->ClientLogInsert(203211,$targetIDX,$targetIDX);
        $logEx=$this->ClientLogExInsert($logIDX2,0,0,$ex2);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;



    }

}