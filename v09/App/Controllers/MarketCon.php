<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\PartnerMo;
use App\Models\MarketKeyMo;
use App\Models\MarketBalanceMo;

use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=MarketMo::GetMarketAllListData();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/market/market.html',$renderData);
	}//렌더

    //market.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=MarketMo::GetDatatableList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //market.html 발란스 가져오기
    public function getBalance()
    {
        if(!isset($_POST['marketCode'])||empty($_POST['marketCode'])){
            $errMsg='marketCode 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $marketCode=$_POST['marketCode'];
        $getBalance=MarketBalanceMo::getBalance($marketCode);
        $resultData = ['getBalance'=>$getBalance,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	//marketDetail.html 디테일 로드
    public function MarketDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            if(!isset($_POST['marketCode'])||empty($_POST['marketCode'])){
                $errMsg='marketCode 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }

            $targetIDX=$_POST['targetIDX'];
            $marketCode=$_POST['marketCode'];
            $dataPack=MarketMo::GetMarketDetailData($targetIDX);
            $getBalance=MarketBalanceMo::getBalance($marketCode);
            // $getApiKey=MarketKeyMo::GetMarketApiKey($marketCode);
            // $apiKey = $getApiKey['marketKey'];






            $partnerPack='';
            // $statusPack=StatusMo::GetStatusTwoAndThree();
        }else if($pageType=='ins'){
            $partnerPack=PartnerMo::GetPartnerListData();
            $dataPack='';
            $targetIDX='';
            // $apiKey = '';
            $getBalance='';

        }
        // $bankPack=BankMo::GetBankData();
        $renderData=[
            //ins
            'pageType'=>$pageType,
            'partnerPack'=>$partnerPack,
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'getBalance'=>$getBalance,
            
        ];
        View::renderTemplate('page/market/marketDetail.html',$renderData);

    }

    //market.html 마켓 추가하기
    public function MarketInsert()
    {
        if(!isset($_POST['partnerIDX'])||empty($_POST['partnerIDX'])){
            $errMsg='partnerIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['phoneNumber'])||empty($_POST['phoneNumber'])){
            $errMsg='phoneNumber 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositFeePer'])||empty($_POST['marketDepositFeePer'])){
            $errMsg='marketDepositFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketWithdrawalFeePer'])){
            $errMsg='marketWithdrawalFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketSettleBankwireFeePer'])||empty($_POST['marketSettleBankwireFeePer'])){
            $errMsg='marketSettleBankwireFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketSettleCryptoFeePer'])||empty($_POST['marketSettleCryptoFeePer'])){
            $errMsg='marketSettleCryptoFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketTopupBankwireFeePer'])||empty($_POST['marketTopupBankwireFeePer'])){
            $errMsg='marketTopupBankwireFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketTopupCryptoFeePer'])||empty($_POST['marketTopupCryptoFeePer'])){
            $errMsg='marketTopupCryptoFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketMinusLimitAmount'])){
            $errMsg='marketMinusLimitAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositMinAmount'])||empty($_POST['marketDepositMinAmount'])){
            $errMsg='marketDepositMinAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositMaxAmount'])||empty($_POST['marketDepositMaxAmount'])){
            $errMsg='marketDepositMaxAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['hostUrl'])||empty($_POST['hostUrl'])){
            $errMsg='hostUrl 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['serviceUrl'])||empty($_POST['serviceUrl'])){
            $errMsg='serviceUrl 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        // if(!isset($_POST['marketApiKey'])||empty($_POST['marketApiKey'])){
        //     $errMsg='marketApiKey 정보가 없습니다.';
        //     $errOn=$this::errExport($errMsg);
        // }

        $partnerIDX=$_POST['partnerIDX'];
        if($partnerIDX=='none'){
            $errMsg='업체를 선택해주세요.';
            $errOn=$this::errExport($errMsg);
        }


        $name=$_POST['name'];
        $phoneNumber=$_POST['phoneNumber'];
        $marketDepositFeePer=$_POST['marketDepositFeePer'];//지불대행수수료
        $marketWithdrawalFeePer=$_POST['marketWithdrawalFeePer'];//출금대행수수료
        $marketSettleBankwireFeePer=$_POST['marketSettleBankwireFeePer'];//정산 bankwire
        $marketSettleCryptoFeePer=$_POST['marketSettleCryptoFeePer'];//정산 crypto
        $marketTopupBankwireFeePer=$_POST['marketTopupBankwireFeePer'];//역정산 bankwire
        $marketTopupCryptoFeePer=$_POST['marketTopupCryptoFeePer'];//역정산 crypto
        $marketMinusLimitAmount=$_POST['marketMinusLimitAmount'];//마이너스한도
        $marketDepositMinAmount=$_POST['marketDepositMinAmount'];//입금 최소한도
        $marketDepositMaxAmount=$_POST['marketDepositMaxAmount'];//입금 최대한도
        $hostUrl=$_POST['hostUrl'];//host url
        $serviceUrl=$_POST['serviceUrl'];//service url
        // $marketApiKey=$_POST['marketApiKey'];//마켓 api key



        //마켓코드생성
        function GenerateString($length)  {
            $characters= "abcdefghijklmnopqrstuvwxyz1234567890";
            $string_generated = "";
            $nmr_loops = $length;
            while ($nmr_loops--){
                $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
            }

            $issetString=MarketMo::GetMarketCode($string_generated);
            if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
                return GenerateString($length);
            }else{
                return $string_generated;
            }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
        }
        $marketNewCode=GenerateString(7);//회사코드는 10자리

        $createTime=date("Y-m-d H:i:s");
        $ipAddress  = $this->GetIPaddress();
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');




        $db = static::getDB();
        $dbName= self::MainDBName;
        


        $stat1=$db->prepare("INSERT INTO $dbName.Market
            (
                partnerIDX,
                categoryIDX,
                statusIDX,
                depositStatusIDX,
                withdrawalStatusIDX,
                marketDepositFeePer,
                marketWithdrawalFeePer,
                marketSettleBankwireFeePer,
                marketSettleCryptoFeePer,
                marketTopupBankwireFeePer,
                marketTopupCryptoFeePer,
                marketMinusLimitAmount,
                marketDepositMinAmount,
                marketDepositMaxAmount,
                code,
                name,
                createTime,
                phoneNumber,
                hostUrl,
                serviceUrl
            )
            VALUES
            (
                :partnerIDX,
                :categoryIDX,
                :statusIDX,
                :depositStatusIDX,
                :withdrawalStatusIDX,
                :marketDepositFeePer,
                :marketWithdrawalFeePer,
                :marketSettleBankwireFeePer,
                :marketSettleCryptoFeePer,
                :marketTopupBankwireFeePer,
                :marketTopupCryptoFeePer,
                :marketMinusLimitAmount,
                :marketDepositMinAmount,
                :marketDepositMaxAmount,
                :code,
                :name,
                :createTime,
                :phoneNumber,
                :hostUrl,
                :serviceUrl
            )
        ");

        $stat1->bindValue(':partnerIDX', $partnerIDX);
        $stat1->bindValue(':categoryIDX', 1);//임시
        $stat1->bindValue(':statusIDX', 412202);
        $stat1->bindValue(':depositStatusIDX', 413101);
        $stat1->bindValue(':withdrawalStatusIDX', 414101);
        $stat1->bindValue(':marketDepositFeePer', $marketDepositFeePer);
        $stat1->bindValue(':marketWithdrawalFeePer', $marketWithdrawalFeePer);
        $stat1->bindValue(':marketSettleBankwireFeePer', $marketSettleBankwireFeePer);
        $stat1->bindValue(':marketSettleCryptoFeePer', $marketSettleCryptoFeePer);
        $stat1->bindValue(':marketTopupBankwireFeePer', $marketTopupBankwireFeePer);
        $stat1->bindValue(':marketTopupCryptoFeePer', $marketTopupCryptoFeePer);
        $stat1->bindValue(':marketMinusLimitAmount', $marketMinusLimitAmount);
        $stat1->bindValue(':marketDepositMinAmount', $marketDepositMinAmount);
        $stat1->bindValue(':marketDepositMaxAmount', $marketDepositMaxAmount);
        $stat1->bindValue(':code', $marketNewCode);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':phoneNumber', $phoneNumber);
        $stat1->bindValue(':hostUrl', $hostUrl);
        $stat1->bindValue(':serviceUrl', $serviceUrl);
        $stat1->execute();
        $targetIDX = $db->lastInsertId();
        //포탈로그
        $logIDX=$this->PortalLogInsert(411101,$targetIDX);


        /*---------------------------샌드박스----------------------------*/
        //20240223 _SANDBOX 빼기
        // $sandboxCode=$marketNewCode.'_SANDBOX';
        $sandboxCode=$marketNewCode;
        $sandboxDb = static::GetSandboxDB();
        $sandboxDb2 = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $stat2=$sandboxDb->prepare("INSERT INTO $dbName.Market
            (
                partnerIDX,
                categoryIDX,
                statusIDX,
                depositStatusIDX,
                withdrawalStatusIDX,
                marketDepositFeePer,
                marketWithdrawalFeePer,
                marketSettleBankwireFeePer,
                marketSettleCryptoFeePer,
                marketTopupBankwireFeePer,
                marketTopupCryptoFeePer,
                marketMinusLimitAmount,
                marketDepositMinAmount,
                marketDepositMaxAmount,
                targetMarketIDX,
                code,
                name,
                createTime,
                phoneNumber,
                hostUrl,
                serviceUrl
            )
            VALUES
            (
                :partnerIDX,
                :categoryIDX,
                :statusIDX,
                :depositStatusIDX,
                :withdrawalStatusIDX,
                :marketDepositFeePer,
                :marketWithdrawalFeePer,
                :marketSettleBankwireFeePer,
                :marketSettleCryptoFeePer,
                :marketTopupBankwireFeePer,
                :marketTopupCryptoFeePer,
                :marketMinusLimitAmount,
                :marketDepositMinAmount,
                :marketDepositMaxAmount,
                :targetMarketIDX,
                :code,
                :name,
                :createTime,
                :phoneNumber,
                :hostUrl,
                :serviceUrl
            )
        ");

        $stat2->bindValue(':partnerIDX', $partnerIDX);
        $stat2->bindValue(':categoryIDX', 1);//임시
        $stat2->bindValue(':statusIDX', 412101);
        $stat2->bindValue(':depositStatusIDX', 413101);
        $stat2->bindValue(':withdrawalStatusIDX', 414101);
        $stat2->bindValue(':marketDepositFeePer', $marketDepositFeePer);
        $stat2->bindValue(':marketWithdrawalFeePer', $marketWithdrawalFeePer);
        $stat2->bindValue(':marketSettleBankwireFeePer', $marketSettleBankwireFeePer);
        $stat2->bindValue(':marketSettleCryptoFeePer', $marketSettleCryptoFeePer);
        $stat2->bindValue(':marketTopupBankwireFeePer', $marketTopupBankwireFeePer);
        $stat2->bindValue(':marketTopupCryptoFeePer', $marketTopupCryptoFeePer);
        $stat2->bindValue(':marketMinusLimitAmount', $marketMinusLimitAmount);
        $stat2->bindValue(':marketDepositMinAmount', $marketDepositMinAmount);
        $stat2->bindValue(':marketDepositMaxAmount', $marketDepositMaxAmount);
        $stat2->bindValue(':targetMarketIDX', $targetIDX);
        $stat2->bindValue(':code', $sandboxCode);
        $stat2->bindValue(':name', $name);
        $stat2->bindValue(':createTime', $createTime);
        $stat2->bindValue(':phoneNumber', $phoneNumber);
        $stat2->bindValue(':hostUrl', $hostUrl);
        $stat2->bindValue(':serviceUrl', $serviceUrl);
        $stat2->execute();
        $sandboxTargetIDX = $sandboxDb->lastInsertId();
        //포탈로그
        $logIDX=$this->SandboxPortalLogInsert(411101,$sandboxTargetIDX);



        //샌드박스 마켓발란스 채워주기
        $stat8=$sandboxDb->prepare("INSERT INTO $dbName.MarketBalance
            (marketIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:marketIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");

        $stat8->bindValue(':marketIDX', $sandboxTargetIDX);
        $stat8->bindValue(':statusIDX', 419101);
        $stat8->bindValue(':targetIDX', 0);
        $stat8->bindValue(':amount', 1000000);
        $stat8->bindValue(':createTime', $createTime);
        $stat8->execute();

        // 231130 포탈 마켓콘에서도 ApiKeyString 쓰고있음 변경시 포탈 마켓콘도 변경해주세여 ////////////
         //apiKey생성
        function ApiKeyString($length)  {
            $characters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            $string_generated = "";
            $nmr_loops = $length;
            while ($nmr_loops--){
                $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            $issetString=MarketKeyMo::MarketkApiKeyChkByMarketKey($string_generated);
            if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
                return GenerateString($length);
            }else{
                return $string_generated;
            }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
        }
        $marketApiKey=ApiKeyString(64);//회사코드는 10자리
        // 231130 포탈 마켓콘에서도 ApiKeyString 쓰고있음 변경시 포탈 마켓콘도 변경해주세여/////////

        $EbuySandboxApiDBName= self::EbuySandboxApiDBName;
        $stat5=$sandboxDb2->prepare("INSERT INTO $EbuySandboxApiDBName.MarketKey
            (marketCode,marketKey,ip,statusIDX,managerIDX,createTime)
            VALUES
            (:marketCode,:marketKey,:ip,:statusIDX,:managerIDX,:createTime)
        ");

        $stat5->bindValue(':marketCode', $sandboxCode);
        $stat5->bindValue(':marketKey', $marketApiKey);//임시
        $stat5->bindValue(':ip', $ipAddress);
        $stat5->bindValue(':statusIDX', 429101);
        $stat5->bindValue(':managerIDX', $staffIDX);
        $stat5->bindValue(':createTime', $createTime);
        $stat5->execute();
        $sandboxTargetIDX2 = $sandboxDb2->lastInsertId();
        $logIDX=$this->SandboxPortalLogInsert(429101,$sandboxTargetIDX2);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //market.html 마켓 업데이트
    public function MarketUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositFeePer'])){
            $errMsg='marketDepositFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketWithdrawalFeePer'])){
            $errMsg='marketWithdrawalFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketSettleBankwireFeePer'])){
            $errMsg='marketSettleBankwireFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketSettleCryptoFeePer'])){
            $errMsg='marketSettleCryptoFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketTopupBankwireFeePer'])){
            $errMsg='marketTopupBankwireFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketTopupCryptoFeePer'])){
            $errMsg='marketTopupCryptoFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketMinusLimitAmount'])){
            $errMsg='marketMinusLimitAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositMinAmount'])){
            $errMsg='marketDepositMinAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketDepositMaxAmount'])){
            $errMsg='marketDepositMaxAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketCode'])){
            $errMsg='marketCode 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['hostUrl'])){
            $errMsg='hostUrl 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['serviceUrl'])){
            $errMsg='serviceUrl 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //비상연락망은 유효성 안걸음

        $targetIDX=$_POST['targetIDX'];
        $phoneNumber=$_POST['phoneNumber'];//비상연락망
        $marketDepositFeePer=$_POST['marketDepositFeePer'];//지불대행수수료
        $marketWithdrawalFeePer=$_POST['marketWithdrawalFeePer'];//출금대행수수료
        $marketSettleBankwireFeePer=$_POST['marketSettleBankwireFeePer'];//정산 bankwire
        $marketSettleCryptoFeePer=$_POST['marketSettleCryptoFeePer'];//정산 crypto
        $marketTopupBankwireFeePer=$_POST['marketTopupBankwireFeePer'];//역정산 bankwire
        $marketTopupCryptoFeePer=$_POST['marketTopupCryptoFeePer'];//역정산 crypto
        $marketMinusLimitAmount=$_POST['marketMinusLimitAmount'];//마이너스한도
        $marketDepositMinAmount=$_POST['marketDepositMinAmount'];//입금 최소한도
        $marketDepositMaxAmount=$_POST['marketDepositMaxAmount'];//입금 최대한도
        $statusIDX=$_POST['statusIDX'];
        $statusVal=$_POST['statusVal'];
        $hostUrl=$_POST['hostUrl'];
        $serviceUrl=$_POST['serviceUrl'];

        //업데이트가 가능한 상태인가
        $targetData=MarketMo::GetTargetUpdateData($targetIDX);
        if(!isset($targetData['idx'])){
            $errMsg='타겟이 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        // $marketCode=$_POST['marketCode'];
        // $marketKeyData=MarketKeyMo::GetIssetMarketKey($marketCode);

        // $apiDb = static::GetApiDB();
        // $apiDBName= self::EbuyApiDBName;

        // $createTime=date("Y-m-d H:i:s");
        // $ipAddress  = $this->GetIPaddress();
        // $staffIDX=GlobalsVariable::GetGlobals('loginIDX');

        // if($statusIDX==412101){
        //     //정상으로 바꿔줄때 (차단이나 계약만료에서 정상으로 갈때)

        //     //만약 마켓코드 보내서 정상인 마켓키가 없다면 하나 넣어줘야함
        //     if(!isset($marketKeyData['idx'])&&empty($marketKeyData['idx'])){
        //         //apiKey생성
        //         function ApiKeyString($length)  {
        //             $characters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        //             $string_generated = "";
        //             $nmr_loops = $length;
        //             while ($nmr_loops--){
        //                 $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
        //             }
        //             $issetString=MarketKeyMo::IssetMarketKey($string_generated);
        //             if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
        //                 return GenerateString($length);
        //             }else{
        //                 return $string_generated;
        //             }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
        //         }
        //         $marketApiKey=ApiKeyString(64);

        //         $stat0=$apiDb->prepare("INSERT INTO $apiDBName.MarketKey
        //             (marketCode,marketKey,ip,statusIDX,managerIDX,createTime)
        //             VALUES
        //             (:marketCode,:marketKey,:ip,:statusIDX,:managerIDX,:createTime)
        //         ");

        //         $stat0->bindValue(':marketCode', $marketCode);
        //         $stat0->bindValue(':marketKey', $marketApiKey);//임시
        //         $stat0->bindValue(':ip', $ipAddress);
        //         $stat0->bindValue(':statusIDX', 429101);
        //         $stat0->bindValue(':managerIDX', $staffIDX);
        //         $stat0->bindValue(':createTime', $createTime);
        //         $stat0->execute();
        //     }

        // }

        $targetPhoneNumber=$targetData['phoneNumber'];//타겟 비상연락망
        $targetDepositFeePer=$targetData['marketDepositFeePer'];//타겟 지불대행수수료
        $targetWithdrawalFeePer=$targetData['marketWithdrawalFeePer'];//타겟 출금대행수수료
        $targetSettleBankwireFeePer=$targetData['marketSettleBankwireFeePer'];//타겟 정산 bankwire
        $targetSettleCryptoFeePer=$targetData['marketSettleCryptoFeePer'];//타겟 정산 crypto
        $targetTopupBankwireFeePer=$targetData['marketTopupBankwireFeePer'];//타겟 역정산 bankwire
        $targetTopupCryptoFeePer=$targetData['marketTopupCryptoFeePer'];//타겟 역정산 crypto
        $targetMinusLimitAmount=$targetData['marketMinusLimitAmount'];//타겟 마이너스한도
        $targetDepositMinAmount=$targetData['marketDepositMinAmount'];//타겟 입금 최소한도
        $targetDepositMaxAmount=$targetData['marketDepositMaxAmount'];//타겟 입금 최대한도
        $targetStatusIDX=$targetData['statusIDX'];
        $targetStatusVal=$targetData['statusVal'];
        $targetHostUrl=$targetData['hostUrl'];
        $targetServiceUrl=$targetData['serviceUrl'];


        $step1=0;//지불대행수수료
        $step2=0;//출금대행수수료
        $step3=0;//정산 bankwire
        $step4=0;//정산 crypto
        $step5=0;//역정산 bankwire
        $step6=0;//역정산 crypto
        $step7=0;//마이너스한도
        $step8=0;//입금 최소한도
        $step9=0;//입금 최대한도
        $step10=0;//비상연락망
        $step11=0;//상태
        $step12=0;//hostUrl
        $step13=0;//serviceUrl

        //지불대행수수료
        if($marketDepositFeePer!=$targetDepositFeePer){
            $step1=1;
            $ex='지불대행수수료가 '.$targetDepositFeePer.'에서 '.$marketDepositFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //출금대행수수료
        if($marketWithdrawalFeePer!=$targetWithdrawalFeePer){
            $step2=1;
            $ex='출금대행수수료가 '.$targetWithdrawalFeePer.'에서 '.$marketWithdrawalFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //정산 bankwire
        if($marketSettleBankwireFeePer!=$targetSettleBankwireFeePer){
            $step3=1;
            $ex='정산 bankwire가 '.$targetSettleBankwireFeePer.'에서 '.$marketSettleBankwireFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //정산 crypto
        if($marketSettleCryptoFeePer!=$targetSettleCryptoFeePer){
            $step4=1;
            $ex='정산 crypto가 '.$targetSettleCryptoFeePer.'에서 '.$marketSettleCryptoFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //역정산 bankwire
        if($marketTopupBankwireFeePer!=$targetTopupBankwireFeePer){
            $step5=1;
            $ex='역정산 bankwire가 '.$targetTopupBankwireFeePer.'에서 '.$marketTopupBankwireFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //역정산 crypto
        if($marketTopupCryptoFeePer!=$targetTopupCryptoFeePer){
            $step6=1;
            $ex='역정산 crypto가 '.$targetTopupCryptoFeePer.'에서 '.$marketTopupCryptoFeePer.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //마이너스한도
        if($marketMinusLimitAmount!=$targetMinusLimitAmount){
            $step7=1;
            $ex='마이너스한도가 '.$targetMinusLimitAmount.'에서 '.$marketMinusLimitAmount.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //입금 최소한도
        if($marketDepositMinAmount!=$targetDepositMinAmount){
            $step8=1;
            $ex='입금 최소한도가 '.$targetDepositMinAmount.'에서 '.$marketDepositMinAmount.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //입금 최대한도
        if($marketDepositMaxAmount!=$targetDepositMaxAmount){
            $step9=1;
            $ex='입급 최대한도가'.$targetDepositMaxAmount.'에서 '.$marketDepositMaxAmount.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //비상연락망
        if($phoneNumber!=$targetPhoneNumber){
            $step10=1;
            $ex='비상연락망이 '.$targetPhoneNumber.'에서 '.$phoneNumber.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //상태
        if($statusIDX!=$targetStatusIDX){
            $step11=1;
            $ex='상태가 '.$targetStatusVal.'에서 '.$statusVal.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //hostUrl
        if($hostUrl!=$targetHostUrl){
            $step12=1;
            $ex='hostUrl이 '.$targetHostUrl.'에서 '.$hostUrl.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }
        //serviceUrl
        if($serviceUrl!=$targetServiceUrl){
            $step13=1;
            $ex='serviceUrl이 '.$targetServiceUrl.'에서 '.$serviceUrl.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert(411201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0 && $step5==0 && $step6==0 && $step7==0 && $step8==0 && $step9==0 && $step10==0 && $step11==0 && $step12==0 && $step13==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Market SET
            phoneNumber=:phoneNumber,
            marketDepositFeePer=:marketDepositFeePer,
            marketWithdrawalFeePer=:marketWithdrawalFeePer,
            marketSettleBankwireFeePer=:marketSettleBankwireFeePer,
            marketSettleCryptoFeePer=:marketSettleCryptoFeePer,
            marketTopupBankwireFeePer=:marketTopupBankwireFeePer,
            marketTopupCryptoFeePer=:marketTopupCryptoFeePer,
            marketMinusLimitAmount=:marketMinusLimitAmount,
            marketDepositMinAmount=:marketDepositMinAmount,
            marketDepositMaxAmount=:marketDepositMaxAmount,
            statusIDX=:statusIDX,
            hostUrl=:hostUrl,
            serviceUrl=:serviceUrl
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':phoneNumber', $phoneNumber);
        $stat1->bindValue(':marketDepositFeePer', $marketDepositFeePer);
        $stat1->bindValue(':marketWithdrawalFeePer', $marketWithdrawalFeePer);
        $stat1->bindValue(':marketSettleBankwireFeePer', $marketSettleBankwireFeePer);
        $stat1->bindValue(':marketSettleCryptoFeePer', $marketSettleCryptoFeePer);
        $stat1->bindValue(':marketTopupBankwireFeePer', $marketTopupBankwireFeePer);
        $stat1->bindValue(':marketTopupCryptoFeePer', $marketTopupCryptoFeePer);
        $stat1->bindValue(':marketMinusLimitAmount', $marketMinusLimitAmount);
        $stat1->bindValue(':marketDepositMinAmount', $marketDepositMinAmount);
        $stat1->bindValue(':marketDepositMaxAmount', $marketDepositMaxAmount);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':hostUrl', $hostUrl);
        $stat1->bindValue(':serviceUrl', $serviceUrl);
        $stat1->execute();


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    // //market.html status 업데이트
    // public function StatusUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
    //         $errMsg='statusVal 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusVal=$_POST['statusVal'];

    //     $issetStatusVal=MarketMo::MarketStatusData($targetIDX);
    //     if(isset($issetStatusVal['idx'])){
    //         $statusIDX=$issetStatusVal['statusIDX'];
    //         if($statusIDX==412202){
    //             $errMsg='계약이 만료된 마켓입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }else{
    //         $errMsg='해당마켓 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     //최상 상태 변경시 지불,출금 대행도 차단할 수 있거나 정상 처리 해야한다.
    //     $depositStatusIDX='';
    //     $withdrawalStatusIDX='';
    //     if($statusIDX==412101){
    //         $depositStatusIDX=413201;
    //         $withdrawalStatusIDX=414201;
    //     }else{
    //         $depositStatusIDX=413101;
    //         $withdrawalStatusIDX=414101;
    //     }

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.Market SET
    //         statusIDX=:statusVal,
    //         depositStatusIDX=:depositStatusIDX,
    //         withdrawalStatusIDX=:withdrawalStatusIDX
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':statusVal', $statusVal);
    //     $stat1->bindValue(':depositStatusIDX', $depositStatusIDX);
    //     $stat1->bindValue(':withdrawalStatusIDX', $withdrawalStatusIDX);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     //포탈로그
    //     $this->PortalLogInsert($statusVal,$targetIDX);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //market.html 지불대행 status 업데이트
    public function DepositStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $issetStatusVal=MarketMo::MarketStatusData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $statusIDX=$issetStatusVal['statusIDX'];
            if($statusIDX==412202){
                $errMsg='계약이 만료된 마켓입니다.';
                $errOn=$this::errExport($errMsg);
            }
            if($statusIDX==412201){
                $errMsg='최상상태가 차단된 상태입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='해당마켓 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Market SET
            depositStatusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->PortalLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //market.html 출금대행 status 업데이트
    public function WithdrawalStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $issetStatusVal=MarketMo::MarketStatusData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $statusIDX=$issetStatusVal['statusIDX'];
            if($statusIDX==412202){
                $errMsg='계약이 만료된 마켓입니다.';
                $errOn=$this::errExport($errMsg);
            }
            if($statusIDX==412201){
                $errMsg='최상상태가 차단된 상태입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='해당마켓 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Market SET
            withdrawalStatusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->PortalLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //market.html 마켓발란스 업데이트
    public function MarketBalanceUpdate()
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['amount'])||empty($_POST['amount'])){
            $errMsg='amount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['exVal'])||empty($_POST['exVal'])){
            $errMsg='스태프사유를 입력해주세요.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['portalExVal'])||empty($_POST['portalExVal'])){
            $errMsg='포탈사유를 입력해주세요.';
            $errOn=$this::errExport($errMsg);
        }
        if(!is_numeric($_POST['amount'])) {
            $errMsg = '발란스는 숫자만 입력 가능합니다.';
            $errOn = $this::errExport($errMsg);
        }
        $marketIDX=$_POST['marketIDX'];
        $statusIDX=$_POST['statusIDX'];
        $amount=$_POST['amount'];
        $exVal=$_POST['exVal'];
        $portalExVal=$_POST['portalExVal'];

        $issetStatusVal=MarketMo::MarketStatusData($marketIDX);
        if(isset($issetStatusVal['idx'])){
            $targetStatusIDX=$issetStatusVal['statusIDX'];
            if($targetStatusIDX==412201){
                $errMsg='상태가 차단된 마켓입니다';
                $errOn=$this::errExport($errMsg);
            }
        }

        $marketCode=$issetStatusVal['code'];
        $getBalance=MarketBalanceMo::getBalance($marketCode);

        if($statusIDX==419201){
            if($getBalance < $amount){
                $errMsg='차감 발란스가 현재 발란스보다 높습니다.';
                $errOn=$this::errExport($errMsg);
            }
        }

        $createTime=date("Y-m-d H:i:s");

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.MarketBalance
            (marketIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:marketIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");

        $stat1->bindValue(':marketIDX', $marketIDX);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->bindValue(':amount', $amount);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $marketBalanceIDX = $db->lastInsertId();
        //포탈 발란스 히스토리 전용 (포탈 > 발란스히스토리 > 디테일 에서 보임)
        $logIDX2=$this->PortalLogInsert(419102,$marketBalanceIDX);
        $logEx2=$this->PortalLogExInsert($logIDX2,0,0,$portalExVal);


        $ex=$exVal.' ($'.number_format($amount,2).')';
        //포탈로그
        $logIDX=$this->PortalLogInsert($statusIDX,$marketIDX);
        $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}