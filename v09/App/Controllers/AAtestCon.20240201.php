<?php

namespace App\Controllers;
use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\SystemMo;
use App\Models\ClientMileageDepositMo;
use App\Models\EbuyBankLogMo;
use App\Models\StaffMo;




use App\Models\MarketKeyMo;
use App\Models\MarketMo;


/**
 * Home controller
 *
 * PHP version 7.0
 */

class AAtestCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */




 public static function TestMinJob(){
        //시스템에서 입금대기마감시간 가져오기

        $systemclosingTimeIDX=8;
        $getSystemclosingTime=SystemMo::GetSystemStatusInfo($systemclosingTimeIDX);
        if(!isset($getSystemclosingTime['idx'])&&empty($getSystemclosingTime['idx'])){
            $errMsg='systemclosingTimeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $systemclosingTime=$getSystemclosingTime['value'];
        //입금대기시간 + 뱅크로그잡도는시간2분 + 매칭잡도는시간1분
        //최대 오차 범위인 3분 더함
        $systemclosingTime=$systemclosingTime+3;

        //입금대기시간이 지난 자동입금신청건 가져오기
        $getAutoDepositList=ClientMileageDepositMo::MinJobAutoMileageDepositList($systemclosingTime);

        $db = static::getDB();
        $dbName= self::MainDBName;

        $apidb = static::GetApiDB();
        $apidbName= self::EbuyApiDBName;
        $createTime=date("Y-m-d H:i:s");

        //20240110 수동입금신청이 아닌 수동신청보류로 바꿔줌
        //$passiveStatusIDX=203101 > 203104

        //1. 자동입금신청 -> 수동입금신청으로 변경


        $passiveStatusIDX=203104;
        foreach ($getAutoDepositList as $key) {
            $autoDepositIDX=$key['idx'];
            $autoDepositClientIDX=$key['clientIDX'];
            $update =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                statusIDX = :passiveStatusIDX,
                completeTime = :completeTime
                WHERE idx = :autoDepositIDX
            ");
            $update->bindValue(':passiveStatusIDX', $passiveStatusIDX);
            $update->bindValue(':completeTime', $createTime);
            $update->bindValue(':autoDepositIDX', $autoDepositIDX);
            $update->execute();
            //로그엔 하나의 신청건에 자동과 수동이 둘 다 쌓여있음

            $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                VALUES
                (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':statusIDX', $passiveStatusIDX);
            $stat2->bindValue(':targetIDX', $autoDepositIDX);
            $stat2->bindValue(':clientIDX', $autoDepositClientIDX);
            $stat2->bindValue(':loginIDX', 0);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', 0);
            $stat2->execute();

            static::socketTable($autoDepositIDX);
        }

        //2. 자동처리 시스템 상태 가져오기
        $systemMatchingIDX=10;
        $getSystemMatchingStatus=SystemMo::GetSystemStatusInfo($systemMatchingIDX);



        if(!isset($getSystemMatchingStatus['idx'])&&empty($getSystemMatchingStatus['idx'])){
            $errMsg='systemMatchingStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $systemMatchingStatus=$getSystemMatchingStatus['status'];

        // 시스템 실행잠금이면 나가라
        if($systemMatchingStatus==2){
            exit();
        }

        //3. EbuyBankLog테이블과 ClientMileageDeposit테이블 매칭시작
        //EbuyBankLog테이블에서 처리 전인 건들 가져오기
        $getEbuyBankLogList=EbuyBankLogMo::MinJobEbuyBankLogList();

        $ebuyBankLogStatusIDX=101102;


        foreach ($getEbuyBankLogList as $key) {
            $ebuyBankLogIDX=$key['idx'];
            $depositAmount=$key['depositAmount'];
            $ebuyBankIDX=$key['ebuyBankIDX'];
            $clientBankIDX=$key['clientBankIDX'];
            $clientAccountHolder=$key['clientAccountHolder'];
            $tradeTime=$key['tradeTime'];
                
            
            //한번 읽은 EbuyBankLog데이터들은 매칭이 됐든 안됐든 처리완료 처리하기
            // $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
            //     statusIDX = :ebuyBankLogStatusIDX
            //     WHERE idx = :ebuyBankLogIDX
            // ");
            // $update->bindValue(':ebuyBankLogStatusIDX', $ebuyBankLogStatusIDX);
            // $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
            // $update->execute();
            //스크래핑 업데이트 (소켓테이블보다 먼저되면 안됨)
            // static::ScrapingData($ebuyBankLogIDX);

            //ClientMileageDeposit테이블 테이블조사하기
            // 입금한 계좌주 + 입금한 금액 + 입금한 은행 + (트레이드시간 > 신청시간)
            $matchingArr=[
                'depositAmount'=>$depositAmount,
                'ebuyBankIDX'=>$ebuyBankIDX,
                'clientBankIDX'=>$clientBankIDX,
                'clientAccountHolder'=>$clientAccountHolder,
                'tradeTime'=>$tradeTime
            ];
            //  $matchingArr=[
            //     'depositAmount'=>50000,
            //     'ebuyBankIDX'=>3,
            //     'clientBankIDX'=>29,
            //     'clientAccountHolder'=>'박제이',
            //     'tradeTime'=>'2024-01-29 11:05:24'
            // ];
            $getAutoDepositMatchingData=ClientMileageDepositMo::MinJobAutoMileageDepositMatching($matchingArr);



            // 4. 매칭성공시 성공완료처리하기
            if(isset($getAutoDepositMatchingData['idx'])&&!empty($getAutoDepositMatchingData['idx'])){

                $matchingDepositIDX=$getAutoDepositMatchingData['idx'];
                echo $matchingDepositIDX;
                exit();
                $matchingDepositClientIDX=$getAutoDepositMatchingData['clientIDX'];
                $matchingDepositAmount=$getAutoDepositMatchingData['amount'];
                // $rn=$getAutoDepositMatchingData['rn'];


                $amount = str_replace(',', '', $matchingDepositAmount); 
                $amountAsInt = intval($amount); 
                $matchingDepositStatusIDX=203202;

                //입금신청건 완료처리
                $depositUpdate =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                    statusIDX = :matchingDepositStatusIDX,
                    completeTime = :completeTime
                    WHERE idx = :matchingDepositIDX
                ");
                $depositUpdate->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
                $depositUpdate->bindValue(':completeTime', $createTime);
                $depositUpdate->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $depositUpdate->execute();

                //마일리지테이블에 쌓기
                $insert=$db->prepare("INSERT INTO $dbName.ClientMileage
                    (
                        clientIDX,
                        statusIDX,
                        targetIDX,
                        amount,
                        createTime
                    )
                    VALUES
                    (
                        :matchingDepositClientIDX,
                        :matchingDepositStatusIDX,
                        :matchingDepositIDX,
                        :amountAsInt,
                        :createTime
                    )
                ");
                $insert->bindValue(':matchingDepositClientIDX', $matchingDepositClientIDX);
                $insert->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
                $insert->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $insert->bindValue(':amountAsInt', $amountAsInt);
                $insert->bindValue(':createTime', $createTime);
                $insert->execute();

                //EbuyBankLog테이블에 입금신청건IDX 업데이트
                $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
                    targetIDX = :matchingDepositIDX
                    WHERE idx = :ebuyBankLogIDX
                ");
                $update->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
                $update->execute();

                $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                    (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                    VALUES
                    (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
                ");
                $stat2->bindValue(':statusIDX', $matchingDepositStatusIDX);
                $stat2->bindValue(':targetIDX', $matchingDepositIDX);
                $stat2->bindValue(':clientIDX', $matchingDepositClientIDX);
                $stat2->bindValue(':loginIDX', 0);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':ipAddress', 0);
                $stat2->execute();
                // static::socketTable($matchingDepositIDX);
                // $alarmArr=[
                //     'clientIDX'=>$matchingDepositClientIDX,
                //     'statusIDX'=>$matchingDepositStatusIDX,
                //     'targetIDX'=>$matchingDepositIDX,
                //     'param'=>['amount'=>$amount]
                // ];

                // $AppMainIoUri= self::AppMainIoUri;
                // $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                // static::sendCurl($alarmArr,$AppMainIoAddr);

                // $scrParam=[
                //     'rn'=>$rn,
                //     'statusIDX'=>$matchingDepositStatusIDX,
                // ];
                // $AppMainIoAddr =$AppMainIoUri.'/rechargeResultAlarm';
                // static::sendCurl($scrParam,$AppMainIoAddr);

            }
        }
    }
    // public static function TestMinJob(){

    //     $oldDB = static::GetOldDB();
    //     $newDB = static::GetDB();

    //     $now = new \DateTime('now', new \DateTimeZone('UTC'));
    //     $now->setTimezone(new \DateTimeZone('Asia/Seoul'));
    //     $nowInSeoul = $now->format('Y-m-d H:i:s');
    //     $createTime=date('Y-m-d H:i:s');


    //     $result1 = $oldDB->query("SELECT
    //         MH_ID AS targetIDX,
    //         MH_SRL AS migrationIDX,
    //         MH_MILEAGE AS amount,
    //         MH_ACTION,
    //         MH_REG AS time
    //     FROM ebuy.AA_MILEAGE_HIS
    //     WHERE MH_REG >= '{$nowInSeoul}' - INTERVAL 10 MINUTE
    //     ORDER BY MH_REG DESC
    //     ");
    //     $result1f=$result1->fetchAll(PDO::FETCH_ASSOC);

    //     foreach ($result1f as $key => $item) {
    //         $targetIDX=$item['targetIDX'];
    //         $migrationIDX=$item['migrationIDX'];
    //         $amount=$item['amount'];
    //         $MH_ACTION=$item['MH_ACTION'];
    //         $time=$item['time'];
    //         $koreaTimeZone = new \DateTimeZone('Asia/Seoul');
    //         $koreaDateTime = new \DateTime($time, $koreaTimeZone);
    //         $koreaDateTime->setTimezone(new \DateTimeZone('UTC'));
    //         $utcTime = $koreaDateTime->format('Y-m-d H:i:s');
    //         if($MH_ACTION==6){
    //             $statusIDX='203201,203202';
    //             }else if($MH_ACTION==20){
    //                 $statusIDX='204211,204212';
    //             }else if($MH_ACTION==5){
    //                 $statusIDX='204101,204102';
    //             }else if($MH_ACTION==8){
    //                 $statusIDX='205101';
    //             }else if($MH_ACTION==18){
    //                 $statusIDX='908102';
    //             }else if($MH_ACTION==7){
    //                 $statusIDX='205201';
    //             }else if($MH_ACTION==16){
    //                 $statusIDX='906101';
    //         }
    //         $result1f[$key]['statusIDX'] = $statusIDX;

    //         $result2 = $newDB->query("SELECT
    //             idx
    //         FROM ebuy.Client
    //         WHERE migrationIDX='$migrationIDX'
    //         ");
    //         $result2=$result2->fetch(PDO::FETCH_ASSOC);

    //         if(isset($result2['idx'])){
    //             $clientIDX=$result2['idx'];
    //             $result1f[$key]['clientIDX'] = $clientIDX;

    //             $result3 = $newDB->query("SELECT
    //                 idx,
    //                 migrationIDX,
    //                 statusIDX
    //             FROM ebuy.ClientMileage
    //             WHERE clientIDX='$clientIDX' AND statusIDX IN ('$statusIDX') AND amount = '$amount' AND createTime >= NOW() - INTERVAL 20 MINUTE;
    //             ");
    //             $result3=$result3->fetch(PDO::FETCH_ASSOC);

    //             if(isset($result3['idx'])){//뉴바이에있다!
    //                 $result1f[$key]['AAA'] = 'AAAA';


    //                 $stat1=$newDB->prepare("
    //                     DELETE FROM ebuy.AAJJTest
    //                     WHERE targetIDX='$targetIDX'
    //                 ");
    //                 $stat1->execute();

    //             }else{//뉴바이에없엉

    //                 $result4 = $newDB->query("SELECT
    //                     idx
    //                 FROM ebuy.AAJJTest
    //                 WHERE targetIDX='$targetIDX'
    //                 ");
    //                 $result4=$result4->fetch(PDO::FETCH_ASSOC);
    //                 if(!isset($result4['idx'])){
    //                     $insertStmt = $newDB->prepare("INSERT INTO ebuy.AAJJTest (
    //                         clientIDX, 
    //                         statusIDX,
    //                         amount,
    //                         createTime,
    //                         targetIDX,
    //                         migrationIDX
    //                     )VALUES(
    //                         :clientIDX,
    //                         :statusIDX,
    //                         :amount,
    //                         :createTime,
    //                         :targetIDX,
    //                         :migrationIDX)
    //                     ");
    //                     $insertStmt->bindValue(':clientIDX', $clientIDX);
    //                     $insertStmt->bindValue(':amount', $amount);
    //                     $insertStmt->bindValue(':statusIDX', $statusIDX);
    //                     $insertStmt->bindValue(':createTime', $createTime);
    //                     $insertStmt->bindValue(':targetIDX', $targetIDX);
    //                     $insertStmt->bindValue(':migrationIDX', $migrationIDX);
    //                     $insertStmt->execute();
    //                 }


    //             }
    //         }






    //     }

    //     echo json_encode(['data'=>$result1f],JSON_UNESCAPED_UNICODE);
    // }

    public static function JunmoXJunmo(){



// $date = "2024-01-23 07:48:39";//utc 시간
// $timestamp = strtotime("+9 hours", strtotime($date));
// $marketCode='exness';
// $feePer=2;
// $fee='2000';
// $note='settlement';
// $decreaseBalance='100000';

//         $oldDB = static::GetOldDB();
//         $tmpKst = date('Y-m-d H:i:s', strtotime('+9 hours'));
//         $tmpPayAmount=$decreaseBalance*1-$fee*1;
//         $result1 = $oldDB->query("SELECT
//         M_P_ID
//         ,M_P_CODE
//         ,M_P_NAME
//         FROM ebuy.AA_AGENT_PARTNER_MARKET
//         WHERE M_CODE = '$marketCode'
//         LIMIT 1
//         ");
//         $result1f=$result1->fetchAll(PDO::FETCH_ASSOC);
//         foreach ($result1f as $row) {
//             $M_P_ID=$row['M_P_ID'];
//             $M_P_NAME=$row['M_P_NAME'];
//             $M_P_CODE=$row['M_P_CODE'];
//             $query = $oldDB->prepare("INSERT INTO ebuy.AA_AGNET_P_SETTLEPAY
//                 (
//                     APS_P_ID
//                     ,APS_P_NAME
//                     ,APS_P_CODE
//                     ,APS_REG
//                     ,APS_STAT
//                     ,APS_FEE_PER
//                     ,APS_TOTAL_FEE
//                     ,APS_APPLY_QTY_TOTAL
//                     ,decreaseAmount
//                     ,payUsdAmount
//                     ,sysType
//                     ,balanceOutType
//                     ,APS_P_EX
//                 )
//                 VALUES
//                 (
//                     :APS_P_ID
//                     ,:APS_P_NAME
//                     ,:APS_P_CODE
//                     ,:APS_REG
//                     ,:APS_STAT
//                     ,:APS_FEE_PER
//                     ,:APS_TOTAL_FEE
//                     ,:APS_APPLY_QTY_TOTAL
//                     ,:decreaseAmount
//                     ,:payUsdAmount
//                     ,:sysType
//                     ,:balanceOutType
//                     ,:APS_P_EX
//                 )
//             ");
//             $query->bindValue(':APS_P_ID', $M_P_ID);
//             $query->bindValue(':APS_P_NAME', $M_P_NAME);
//             $query->bindValue(':APS_P_CODE', $M_P_CODE);
//             $query->bindValue(':APS_REG', $tmpKst);
//             $query->bindValue(':APS_STAT', 0);
//             $query->bindValue(':APS_FEE_PER', $feePer);
//             $query->bindValue(':APS_TOTAL_FEE', $fee);
//             $query->bindValue(':APS_APPLY_QTY_TOTAL', $decreaseBalance);
//             $query->bindValue(':decreaseAmount', $decreaseBalance);
//             $query->bindValue(':payUsdAmount', $tmpPayAmount);
//             $query->bindValue(':sysType', 1);
//             $query->bindValue(':balanceOutType', 2);
//             $query->bindValue(':APS_P_EX', $note);
//             $result = $query->execute();

//             $query3 =$oldDB->query("UPDATE ebuy.AA_AGENT_PARTNER SET
//                 A_REMAIN_TOTAL = A_REMAIN_TOTAL-$decreaseBalance
//                 WHERE A_ID = '$M_P_ID'
//             ");

//             $query2 = $oldDB->prepare("INSERT INTO ebuy.AA_AGNET_P_QTY_HIS
//                 (
//                     APQH_P_ID
//                     ,APQH_P_NAME
//                     ,APQH_P_CODE
//                     ,APQH_REG
//                     ,APQH_QTY
//                     ,APQH_BEFORE_QTY
//                     ,APQH_AFTER_QTY
//                     ,APQH_TYPE
//                     ,APQH_STAT
//                     ,APQH_CON
//                     ,sysType
//                 )
//                 VALUES
//                 (
//                     :APQH_P_ID
//                     ,:APQH_P_NAME
//                     ,:APQH_P_CODE
//                     ,:APQH_REG
//                     ,:APQH_QTY
//                     ,:APQH_BEFORE_QTY
//                     ,:APQH_AFTER_QTY
//                     ,:APQH_TYPE
//                     ,:APQH_STAT
//                     ,:APQH_CON
//                     ,:sysType
//                 )
//             ");
//             $query2->bindValue(':APQH_P_ID', $M_P_ID);
//             $query2->bindValue(':APQH_P_NAME', $M_P_NAME);
//             $query2->bindValue(':APQH_P_CODE', $M_P_CODE);
//             $query2->bindValue(':APQH_REG', $tmpKst);
//             $query2->bindValue(':APQH_QTY', $decreaseBalance);
//             $query2->bindValue(':APQH_BEFORE_QTY', 0);
//             $query2->bindValue(':APQH_AFTER_QTY', 0);
//             $query2->bindValue(':APQH_TYPE', 1);
//             $query2->bindValue(':APQH_STAT', 3);
//             $query2->bindValue(':APQH_CON', 'Balance Out History');
//             $query2->bindValue(':sysType', 1);
//             $result = $query2->execute();
//         }





    }












































    //public static function TestMinJob(){
        // $type='day';
        // $getList=MarketKeyMo::GetMarketKeyDeletionTime($type);
        // $apidb = static::GetApiDB();
        // $apidbName= self::EbuyApiDBName;
        // $db = static::getDB();
        // $dbName= self::MainDBName;
        // $createTime=date("Y-m-d H:i:s");
        // foreach ($getList as $key) {
        //     $idx=$key['idx'];
        //     $updateStatusIDX = 139101;//삭제상태
        //     $marketCode=$key['marketCode'];
        //     $marketKey=$key['marketKey'];

        //     $getMarketIDX=MarketMo::GetMarketCode($marketCode);
        //     if(isset($getMarketIDX['idx'])||!empty($getMarketIDX['idx'])){
        //         $marketIDX=$getMarketIDX['idx'];
        //         $marketname=$getMarketIDX['name'];

        //         // $query = $apidb->prepare("UPDATE $apidbName.MarketKey SET
        //         //     statusIDX=:statusIDX
        //         // WHERE idx=:idx");
        //         // $query->bindValue(':statusIDX',$updateStatusIDX);
        //         // $query->bindValue(':idx', $idx);
        //         // $query->execute();

        //         // $stat2=$db->prepare("INSERT INTO $dbName.PortalLog
        //         //     (statusIDX,targetIDX,createTime)
        //         //     VALUES
        //         //     (:statusIDX,:targetIDX,:createTime)
        //         // ");
        //         // $stat2->bindValue(':statusIDX', $updateStatusIDX);
        //         // $stat2->bindValue(':targetIDX', $idx);
        //         // $stat2->bindValue(':createTime', $createTime);
        //         // $stat2->execute();

        //         $getNewMarketKey=MarketKeyMo::GetMarketkKey($marketCode);
        //         //정상키 못가져오면 오류기때문에(삭제대기상태 마켓키가있음 정상키가 무조건 있어야함) 이메일 안보내는게 맞는거같음..
        //         if(isset($getNewMarketKey['idx'])&&!empty($getNewMarketKey['idx'])){
        //             //이메일을 보내주자
        //             $newMarketKey=$getNewMarketKey['marketKey'];
        //             $newstringLength = strlen($newMarketKey);
        //             $newapi = str_repeat('*', $newstringLength - 10) . substr($newMarketKey, -10);

        //             $oldstringLength = strlen($marketKey);
        //             $oldapi = str_repeat('*', $oldstringLength - 10) . substr($marketKey, -10);
        //             //이메일을 보내주자
        //             $emailParamVal = [
        //                 'marketname'=>$marketname,
        //                 'oldapi'=>$oldapi,
        //                 'newapi'=>$newapi,
        //             ];
        //             $emailParam=[
        //                 'targetIDX'=>$idx,
        //                 'statusIDX'=>$updateStatusIDX,
        //                 'marketIDX'=>$marketIDX,
        //                 'param'=>$emailParamVal,
        //                 'type'=>'group'
        //             ];
        //             StaffEmailCon::sendEmailToPortal($emailParam);
        //         }
        //     }
        // }
    //}

	// public static function TestMinJob(){
    //     //시스템에서 입금대기마감시간 가져오기
    //     $systemclosingTimeIDX=8;
    //     $getSystemclosingTime=SystemMo::GetSystemStatusInfo($systemclosingTimeIDX);
    //     if(!isset($getSystemclosingTime['idx'])&&empty($getSystemclosingTime['idx'])){
    //         $errMsg='systemclosingTimeIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $systemclosingTime=$getSystemclosingTime['value'];
    //     //입금대기시간 + 뱅크로그잡도는시간2분 + 매칭잡도는시간1분
    //     //최대 오차 범위인 3분 더함
    //     $systemclosingTime=$systemclosingTime+3;

    //     //입금대기시간이 지난 자동입금신청건 가져오기
    //     $getAutoDepositList=ClientMileageDepositMo::MinJobAutoMileageDepositList($systemclosingTime);

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;

    //     $apidb = static::GetApiDB();
    //     $apidbName= self::EbuyApiDBName;
    //     $createTime=date("Y-m-d H:i:s");

    //     //20240110 수동입금신청이 아닌 수동신청보류로 바꿔줌
    //     //$passiveStatusIDX=203101 > 203104

    //     //1. 자동입금신청 -> 수동입금신청으로 변경
    //     $passiveStatusIDX=203104;
    //     foreach ($getAutoDepositList as $key) {
    //         $autoDepositIDX=$key['idx'];
    //         $autoDepositClientIDX=$key['clientIDX'];
    //         $update =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
    //             statusIDX = :passiveStatusIDX
    //             WHERE idx = :autoDepositIDX
    //         ");
    //         $update->bindValue(':passiveStatusIDX', $passiveStatusIDX);
    //         $update->bindValue(':autoDepositIDX', $autoDepositIDX);
    //         $update->execute();
    //         //로그엔 하나의 신청건에 자동과 수동이 둘 다 쌓여있음
    //         static::ClientLogInsert($passiveStatusIDX,$autoDepositIDX,$autoDepositClientIDX);
    //     }

    //     //2. 자동처리 시스템 상태 가져오기
    //     $systemMatchingIDX=10;
    //     $getSystemMatchingStatus=SystemMo::GetSystemStatusInfo($systemMatchingIDX);

    //     if(!isset($getSystemMatchingStatus['idx'])&&empty($getSystemMatchingStatus['idx'])){
    //         $errMsg='systemMatchingStatus 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $systemMatchingStatus=$getSystemMatchingStatus['status'];

    //     // 시스템 실행잠금이면 나가라
    //     if($systemMatchingStatus==2){
    //         exit();
    //     }

    //     //3. EbuyBankLog테이블과 ClientMileageDeposit테이블 매칭시작

    //     //EbuyBankLog테이블에서 처리 전인 건들 가져오기
    //     $getEbuyBankLogList=EbuyBankLogMo::MinJobEbuyBankLogList();



    //     $ebuyBankLogStatusIDX=101102;
    //     foreach ($getEbuyBankLogList as $key) {
    //         $ebuyBankLogIDX=$key['idx'];
    //         $depositAmount=$key['depositAmount'];
    //         $ebuyBankIDX=$key['ebuyBankIDX'];
    //         $clientBankIDX=$key['clientBankIDX'];
    //         $clientAccountHolder=$key['clientAccountHolder'];
    //         $tradeTime=$key['tradeTime'];

    //         //한번 읽은 EbuyBankLog데이터들은 매칭이 됐든 안됐든 처리완료 처리하기
    //         $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
    //             statusIDX = :ebuyBankLogStatusIDX
    //             WHERE idx = :ebuyBankLogIDX
    //         ");
    //         $update->bindValue(':ebuyBankLogStatusIDX', $ebuyBankLogStatusIDX);
    //         $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
    //         $update->execute();

    //         //ClientMileageDeposit테이블 테이블조사하기
    //         // 입금한 계좌주 + 입금한 금액 + 입금한 은행 + (트레이드시간 > 신청시간)
    //         $matchingArr=[
    //             'depositAmount'=>$depositAmount,
    //             'ebuyBankIDX'=>$ebuyBankIDX,
    //             'clientBankIDX'=>$clientBankIDX,
    //             'clientAccountHolder'=>$clientAccountHolder,
    //             'tradeTime'=>$tradeTime
    //         ];
    //         $getAutoDepositMatchingData=ClientMileageDepositMo::MinJobAutoMileageDepositMatching($matchingArr);
    //         // 4. 매칭성공시 성공완료처리하기
    //         if(isset($getAutoDepositMatchingData['idx'])&&!empty($getAutoDepositMatchingData['idx'])){
    //             $matchingDepositIDX=$getAutoDepositMatchingData['idx'];
    //             $matchingDepositClientIDX=$getAutoDepositMatchingData['clientIDX'];
    //             $matchingDepositAmount=$getAutoDepositMatchingData['amount'];
    //             $amount = str_replace(',', '', $matchingDepositAmount);
    //             $amountAsInt = intval($amount);
    //             $matchingDepositStatusIDX=203202;
    //             //입금신청건 완료처리
    //             $depositUpdate =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
    //                 statusIDX = :matchingDepositStatusIDX,
    //                 completeTime = :completeTime
    //                 WHERE idx = :matchingDepositIDX
    //             ");
    //             $depositUpdate->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
    //             $depositUpdate->bindValue(':completeTime', $createTime);
    //             $depositUpdate->bindValue(':matchingDepositIDX', $matchingDepositIDX);
    //             $depositUpdate->execute();

    //             //마일리지테이블에 쌓기
    //             $insert=$db->prepare("INSERT INTO $dbName.ClientMileage
    //                 (
    //                     clientIDX,
    //                     statusIDX,
    //                     targetIDX,
    //                     amount,
    //                     createTime
    //                 )
    //                 VALUES
    //                 (
    //                     :matchingDepositClientIDX,
    //                     :matchingDepositStatusIDX,
    //                     :matchingDepositIDX,
    //                     :amountAsInt,
    //                     :createTime
    //                 )
    //             ");
    //             $insert->bindValue(':matchingDepositClientIDX', $matchingDepositClientIDX);
    //             $insert->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
    //             $insert->bindValue(':matchingDepositIDX', $matchingDepositIDX);
    //             $insert->bindValue(':amountAsInt', $amountAsInt);
    //             $insert->bindValue(':createTime', $createTime);
    //             $insert->execute();

    //             //EbuyBankLog테이블에 입금신청건IDX 업데이트
    //             $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
    //                 targetIDX = :matchingDepositIDX
    //                 WHERE idx = :ebuyBankLogIDX
    //             ");
    //             $update->bindValue(':matchingDepositIDX', $matchingDepositIDX);
    //             $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
    //             $update->execute();
    //             static::ClientLogInsert($matchingDepositStatusIDX,$matchingDepositIDX,$matchingDepositClientIDX);
    //             //유저에게 알람을 쏴주자
    //             $alarmArr=[
    //                 'clientIDX'=>$matchingDepositClientIDX,
    //                 'statusIDX'=>203202,
    //                 'targetIDX'=>1,
    //                 'param'=>['amount'=>$matchingDepositAmount]
    //             ];
    //             $AppMainIoUri= self::AppIoUri;
    //             $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
    //             static::SendCurl($alarmArr,$AppMainIoAddr);
    //             //모든 행위가 다끝나면 입금신청내역 데이터테이블 소켓 쏴주기 20231025
    //             //소켓 데이터테이블
    //             $getStaffList=StaffMo::GetStaffList();
    //             $staffPack = [];
    //             foreach ($getStaffList as $key) {
    //                 $staffPack[] = $key['idx'];
    //             }
    //             $DomainUri=self::DomainUri;
    //             $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$matchingDepositIDX];
    //             $uri = $DomainUri.':16001/statusUpdate';
    //             self::sendCurl($dataPack,$uri);
    //             //소켓 데이터테이블
    //         }

    //     }


    // }

}