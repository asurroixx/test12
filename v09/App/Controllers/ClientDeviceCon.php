<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMo;
use App\Models\ClientGradeMo;
use App\Models\ClientDetailMo;
use App\Models\ClientDeviceMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientDeviceCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		// $gradePack=ClientGradeMo::GetClientGradeList();
		// $renderData=['gradePack'=>$gradePack,];
		View::renderTemplate('page/clientDevice/clientDevice.html');
	}

	//clientDevice.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=ClientDeviceMo::GetDataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //clientDevice.html 유저 공통 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

}