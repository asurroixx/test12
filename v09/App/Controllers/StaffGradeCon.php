<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffGradeMo;
use App\Models\StaffMenuMo;
use App\Models\StaffMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class StaffGradeCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{

		View::renderTemplate('page/staffGrade/staffGrade.html');
	}

	//staffGrade.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $staffList=StaffGradeMo::GetStaffGradeList();
        $resultData = ['data'=>$staffList,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //statusDetail.html 디테일 로드
    public function StaffGradeDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=StaffGradeMo::GetStaffGradeDetail($targetIDX);
            $menuPack=StaffMenuMo::GetGradeMenuList($targetIDX);
            $groupPack=StaffMo::GetStaffGradeListData($targetIDX);
            $menuListArr=[];
            foreach ($menuPack as $key) {
                $menuIDX=$key['idx'];
                $menuName=$key['name'];
                $permission=$key['permission'];
                $childMenuListArr=[];
                $loginIDX= GlobalsVariable::GetGlobals('loginIDX');
                $childArr=array(
                    'menuIDX'=>$menuIDX,
                    'gradeIDX'=>$targetIDX,
                    'loginIDX'=>$loginIDX,
                );
                $ChildMenuList = StaffMenuMo::GetChildGradeMenuList($childArr);
                foreach ($ChildMenuList as $key2) {
                    $childIDX=$key2['idx'];
                    $childName=$key2['name'];
                    $childPermission=$key2['permission'];

                    $childMenuListArr[]=array(
                        'childIDX'=>$childIDX,
                        'childName'=>$childName,
                        'childPermission'=>$childPermission,
                    );
                }
                $menuListArr[]=array(
                    'menuIDX'=>$menuIDX,
                    'menuName'=>$menuName,
                    'permission'=>$permission,
                    'childMenuListArr'=>$childMenuListArr,
                );
            }
        }else if($pageType=='ins'){
            $dataPack='';
            $targetIDX='';
            $menuListArr='';
            $groupPack='';
        }

        $renderData=[
            'targetIDX'=>$targetIDX,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
            'menuListArr'=>$menuListArr,
            'groupPack'=>$groupPack,
        ];
        View::renderTemplate('page/staffGrade/staffGradeDetail.html',$renderData);

    }

    //status.html status 추가
    public function GradeInsert()
    {
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $name=$_POST['name'];
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.StaffGrade
            (name)
            VALUES
            ('$name')
        ");
        $stat1->execute();
        $gradeIDX = $db->lastInsertId();
        $menuPack=StaffMenuMo::GetAllMenuIDXData();
        foreach ($menuPack as $key) {
            $menuIDX=$key['idx'];
            $db2 = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db2->prepare("INSERT INTO $dbName.StaffGroup
                (gradeIDX,menuIDX,permission)
                VALUES
                ('$gradeIDX','$menuIDX',3)
            ");
            $stat2->execute();
        }

        $this->StaffLogInsert(322101,$gradeIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //StaffGrade.html StaffGrade 업데이트
    public function GradeUpdate()
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['tmpData'])||empty($_POST['tmpData'])){
            $errMsg='tmpData 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $idx=$_POST['idx'];
        $gradeIDX=$_POST['gradeIDX'];
        $name=$_POST['name'];
        $tmpData=$_POST['tmpData'];

        $issetStaffData=StaffGradeMo::issetStaffGradeData($idx);
        if(isset($issetStaffData['idx'])){
            $issetIDX=$issetStaffData['idx'];
            $issetName=$issetStaffData['name'];
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $step1=0;
        //등급이름
        if($name!=$issetName){
            $step1=1;
            $ex2='등급이름이 '.$issetName.'에서 '.$name.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(322201,$idx);
            $logEx=$this->StaffLogExInsert($logIDX,$issetIDX,$gradeIDX,$ex2);
        }

        if($step1==0){
            $logIDX=$this->StaffLogInsert(322201,$idx);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,'스태프 권한이 수정됐습니다.');
        }


        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("
            DELETE FROM $dbName.StaffGroup WHERE gradeIDX='$idx'
        ");
        $stat1->execute();

        foreach ($tmpData as $key) {
            $menuIDX=$key['menuIDX'];
            $permission=$key['permission'];
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db->prepare("INSERT INTO $dbName.StaffGroup
                (gradeIDX,menuIDX,permission)
                VALUES
                ('$idx','$menuIDX','$permission')
            ");
            $stat2->execute();
        }

        $stat3=$db->prepare("UPDATE $dbName.StaffGrade SET
            name='$name'
            WHERE idx='$idx'
        ");
        $stat3->execute();



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}