<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMo;
use App\Models\ClientGradeMo;
use App\Models\ClientLogMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientLogCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		// $gradePack=ClientGradeMo::GetClientGradeList();
		// $renderData=['gradePack'=>$gradePack,];
		View::renderTemplate('page/clientLog/clientLog.html');
	}

	//clientLog.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $dateArr=[
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        ];
        $dataPack=ClientLogMo::GetDataTableListLoad($dateArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //clientLog.html 유저 공통 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

    //clientLog.html 오른쪽 디테일 로드
    public function ClientRightDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientLogMo::GetClientDetailData($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/clientLog/clientLogDetail.html',$renderData);
    }

}