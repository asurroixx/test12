<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;

use App\Models\MarketMo;
use PDO;

class LockCon extends \Core\Controller
{
    //렌더
    public function Render($data=null)
    {
        View::renderTemplate('page/lock/lock.html');
    }

    public function TestAAAA($data=null)
    {



        if(!isset($_POST['table'])||empty($_POST['table'])){
            $errMsg='table 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['value'])||empty($_POST['value'])){
            $errMsg='value 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['type'])||empty($_POST['type'])){
            $errMsg='type 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $table=$_POST['table'];
        $value=$_POST['value'];
        $type=$_POST['type'];

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        //암호화 > 원문
        if($type=='E'){
            switch ($table) {
                case 'ClientMarketList':
                    $Sel = $db->prepare("SELECT
                    idx,
                    marketIDX,
                    clientIDX,
                    AES_DECRYPT(referenceId, :dataDbKey) AS referenceId,
                    statusIDX,
                    ip,
                    createTime,
                    note,
                    migrationIDX
                    FROM $dbName.$table
                    WHERE referenceId=UNHEX(:value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'Client':
                    $Sel = $db->prepare("SELECT
                    ci,
                    gender,
                    AES_DECRYPT(name, :dataDbKey) AS name,
                    AES_DECRYPT(birth, :dataDbKey) AS birth,
                    AES_DECRYPT(phone, :dataDbKey) AS phone,
                    AES_DECRYPT(email, :dataDbKey) AS email,
                    gradeIDX,
                    statusIDX,
                    deviceIDX,
                    createTime,
                    migrationTermsTime,
                    migrationIDX,
                    emergency,
                    withdrawalBlock
                    FROM $dbName.$table
                    WHERE (name=UNHEX(:value) or birth=UNHEX(:value) or phone=UNHEX(:value) or email=UNHEX(:value))
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'Email':
                    $Sel = $db->prepare("SELECT
                    AES_DECRYPT(email, :dataDbKey) AS email
                    FROM $dbName.$table
                    WHERE email=UNHEX(:value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'Staff':
                    $Sel = $db->prepare("SELECT
                    idx,
                    AES_DECRYPT(email, :dataDbKey) AS email,
                    AES_DECRYPT(name, :dataDbKey) AS name,
                    createTime,
                    statusIDX,
                    gradeIDX,
                    clientIDX,
                    appActiveStat,
                    appSettlementStat
                    FROM $dbName.$table
                    WHERE (email=UNHEX(:value) or name=UNHEX(:value))
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'Manager':
                    $Sel = $db->prepare("SELECT
                    AES_DECRYPT(A.email, :dataDbKey) AS email,
                    AES_DECRYPT(B.name, :dataDbKey) AS name
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.MarketManager AS B ON A.idx=B.managerIDX
                    WHERE (A.email=UNHEX(:value) or B.name=UNHEX(:value))
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'ClientBank':
                    $Sel = $db->prepare("SELECT
                    A.idx,
                    AES_DECRYPT(A.accountNumber, :dataDbKey) AS accountNumber,
                    AES_DECRYPT(A.accountHolder, :dataDbKey) AS accountHolder,
                    B.name AS bankName,
                    B.code AS bankCode,
                    AES_DECRYPT(C.name, :dataDbKey) AS clientName,
                    AES_DECRYPT(C.birth, :dataDbKey) AS clientBirth,
                    AES_DECRYPT(C.phone, :dataDbKey) AS clientPhone,
                    AES_DECRYPT(C.email, :dataDbKey) AS clientEmail
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
                    LEFT JOIN $dbName.Client AS C ON A.clientIDX = C.idx
                    WHERE (A.accountHolder=UNHEX(:value) or A.accountNumber=UNHEX(:value))
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                case 'ClientPtpAccount':
                    $Sel = $db->prepare("SELECT
                    A.idx,
                    AES_DECRYPT(A.account, :dataDbKey) AS account,
                    B.name AS typeName,
                    AES_DECRYPT(C.name, :dataDbKey) AS clientName,
                    AES_DECRYPT(C.birth, :dataDbKey) AS clientBirth,
                    AES_DECRYPT(C.phone, :dataDbKey) AS clientPhone,
                    AES_DECRYPT(C.email, :dataDbKey) AS clientEmail
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.PeerToPeerType AS B ON A.PeerToPeerTypeIDX = B.idx
                    LEFT JOIN $dbName.Client AS C ON A.clientIDX = C.idx
                    WHERE A.account=UNHEX(:value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetch(PDO::FETCH_ASSOC);
                break;
                default:
                break;
            }




        //원문 > 암호화
        }else{
            switch ($table) {
                case 'ClientMarketList':
                    $Sel = $db->prepare("SELECT
                    idx,
                    marketIDX,
                    clientIDX,
                    statusIDX,
                    ip,
                    createTime,
                    note,
                    migrationIDX
                    FROM $dbName.$table
                    WHERE AES_DECRYPT(referenceId, :dataDbKey) = :value
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'Client':
                    $Sel = $db->prepare("SELECT
                    idx,
                    ci,
                    gender,
                    AES_DECRYPT(name, :dataDbKey) AS name,
                    AES_DECRYPT(birth, :dataDbKey) AS birth,
                    AES_DECRYPT(phone, :dataDbKey) AS phone,
                    AES_DECRYPT(email, :dataDbKey) AS email,
                    gradeIDX,
                    statusIDX,
                    deviceIDX,
                    createTime,
                    migrationTermsTime,
                    migrationIDX,
                    emergency,
                    withdrawalBlock
                    FROM $dbName.$table
                    WHERE (AES_DECRYPT(name, :dataDbKey) = :value or AES_DECRYPT(birth, :dataDbKey) = :value or AES_DECRYPT(phone, :dataDbKey) = :value or AES_DECRYPT(email, :dataDbKey) = :value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'Email':
                    $Sel = $db->prepare("SELECT
                    idx,
                    AES_DECRYPT(email, :dataDbKey) AS email,
                    title,
                    statusIDX,
                    targetIDX,
                    createTime,
                    sendStatusIDX
                    FROM $dbName.$table
                    WHERE AES_DECRYPT(email, :dataDbKey) = :value
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'Staff':
                    $Sel = $db->prepare("SELECT
                    idx,
                    AES_DECRYPT(email, :dataDbKey) AS email,
                    AES_DECRYPT(name, :dataDbKey) AS name,
                    ipAddress,
                    createTime,
                    statusIDX,
                    gradeIDX,
                    clientIDX,
                    appActiveStat,
                    appSettlementStat
                    FROM $dbName.$table
                    WHERE (AES_DECRYPT(email, :dataDbKey) = :value or AES_DECRYPT(name, :dataDbKey) = :value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'Manager':
                    $Sel = $db->prepare("SELECT
                    A.idx,
                    AES_DECRYPT(A.email, :dataDbKey) AS email,
                    A.createTime AS managerCreateTime,
                    B.idx AS marketMangerIDX,
                    AES_DECRYPT(B.name, :dataDbKey) AS name,
                    B.marketIDX AS marketIDX,
                    B.gradeIDX AS gradeIDX,
                    B.position AS position,
                    B.createTime AS marketManagerCreateTime
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.MarketManager AS B ON A.idx=B.managerIDX
                    WHERE (AES_DECRYPT(A.email, :dataDbKey) = :value or AES_DECRYPT(B.name, :dataDbKey) = :value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'ClientBank':
                    $Sel = $db->prepare("SELECT
                    A.idx,
                    AES_DECRYPT(A.accountHolder, :dataDbKey) AS accountHolder,
                    AES_DECRYPT(A.accountNumber, :dataDbKey) AS accountNumber,
                    A.createTime,
                    A.migrationIDX,
                    B.name AS bankName,
                    B.code AS bankCode,
                    AES_DECRYPT(C.name, :dataDbKey) AS clientName,
                    AES_DECRYPT(C.birth, :dataDbKey) AS clientBirth,
                    AES_DECRYPT(C.phone, :dataDbKey) AS clientPhone,
                    AES_DECRYPT(C.email, :dataDbKey) AS clientEmail
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
                    LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
                    WHERE (AES_DECRYPT(A.accountHolder, :dataDbKey) = :value or AES_DECRYPT(A.accountNumber, :dataDbKey) = :value)
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                case 'ClientPtpAccount':
                    $Sel = $db->prepare("SELECT
                    A.idx,
                    AES_DECRYPT(A.account, :dataDbKey) AS account,
                    B.name AS typeName,
                    AES_DECRYPT(C.name, :dataDbKey) AS clientName,
                    AES_DECRYPT(C.birth, :dataDbKey) AS clientBirth,
                    AES_DECRYPT(C.phone, :dataDbKey) AS clientPhone,
                    AES_DECRYPT(C.email, :dataDbKey) AS clientEmail
                    FROM $dbName.$table AS A
                    LEFT JOIN $dbName.PeerToPeerType AS B ON A.PeerToPeerTypeIDX = B.idx
                    LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
                    WHERE AES_DECRYPT(A.account, :dataDbKey) = :value
                    ");
                    $Sel->bindValue(':dataDbKey', $dataDbKey);
                    $Sel->bindValue(':value', $value);
                    $Sel->execute();
                    $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
                break;
                default:
                break;
            }
        }



        $resultData = ['result'=>'t','data'=>$result];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

    }

}