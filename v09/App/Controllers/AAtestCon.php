<?php

namespace App\Controllers;
use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\SystemMo;
use App\Models\ClientMileageDepositMo;
use App\Models\EbuyBankLogMo;
use App\Models\StaffMo;




use App\Models\MarketKeyMo;
use App\Models\MarketMo;


/**
 * Home controller
 *
 * PHP version 7.0
 */

class AAtestCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */


    public static function aa(){
        $db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("SELECT
		A.idx
		, A.CancelAmt
		, A.CancelMsg
		, A.ClientCashReceipt_TID
		, B.idx AS reIDX
		, B.clientIDX
		FROM $dbName.ClientCashCancelReceipt AS A
		JOIN $dbName.ClientCashReceipt AS B ON B.idx= A.ClientCashReceiptIDX
		WHERE A.statusIDX = 811701
		AND NOT EXISTS (
			SELECT 1
			FROM $dbName.ClientCashCancelReceipt
			WHERE statusIDX = 811801
		)
		LIMIT 1
		");
		$query->execute();
		$sel=$query->fetch(PDO::FETCH_ASSOC);
		if(isset($sel['idx'])){
            $idx=$sel['idx'];
            $ClientCashReceiptIDX=$sel['reIDX'];
			$clientIDX=$sel['clientIDX'];
            $nowTime=date("Y-m-d H:i:s");
			$nextStatus=811801;
			$insEX='현금영수증취소 대기중 -> 발행중 상태로 업데이트되었습니다.';
            $update =$db->prepare("UPDATE $dbName.ClientCashCancelReceipt SET
				statusIDX = :changeStatus
				,resultTime = :tmpTime
				,result_msg = :tmpMsg
				WHERE idx = :targetIDX
			");
			$update->bindValue(':changeStatus', $nextStatus);
			$update->bindValue(':tmpTime', $nowTime);
			$update->bindValue(':tmpMsg', $insEX);
			$update->bindValue(':targetIDX', $idx);
			$update->execute();
            if($clientIDX*1>1){
				$db = static::getDB();
				$dbName= self::MainDBName;
				$stat2=$db->prepare("INSERT INTO $dbName.ClientLog
					(statusIDX,targetIDX,clientIDX,createTime)
					VALUES
					(:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
				");
				$stat2->bindValue(':updateStatusIDX', $nextStatus);
				$stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
				$stat2->bindValue(':clientIDX', $clientIDX);
				$stat2->bindValue(':createTime', $nowTime);
				$stat2->execute();
				$ClientLogIDX=$db->lastInsertId();
				$stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
					(logIDX,ex)
					VALUES
					(:logIDX,:ex)
				");
				$stat3->bindValue(':logIDX', $ClientLogIDX);
				$stat3->bindValue(':ex', $insEX);
				$stat3->execute();
			}
            $tid=$sel['ClientCashReceipt_TID'];
			$amount=$sel['CancelAmt'];
			$msg=$sel['CancelMsg'];

			$curl = curl_init();
            curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://api.mydatahub.co.kr/kwichub/Cash/Cancel',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => 'UTF-8',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS =>'{
			        "TID" : "'.$tid.'",
			        "CancelAmt" : "'.$amount.'",
			        "CancelMsg" : "'.$msg.'",
			        "PartialCancelCode" : "0",
			        "ReceiptSubNum" : "1588800568"
				}',
				CURLOPT_HTTPHEADER => array(
					'Authorization: Token ab75a3f450024a119835de4352bc1fc60b5fdbd6',
					'Content-Type: application/json;charset=UTF-8'
				)
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$json=json_decode($response, true);
			$erro=$json["errMsg"];
			$receiveJsonData=json_encode($json);
            if($erro=="success"){
				$responseData=$json["data"];
				$res_ResultCode=$responseData["ResultCode"];
				$res_ResultMsg=$responseData["ResultMsg"];
				if($res_ResultCode=='2001'){//정상!
					$nextStatus=811901;
				}else{// 2001 외 실패
					$nextStatus=811902;
				}
				$nowTime=date("Y-m-d H:i:s");
				$db     = static::GetApiDB();
				$dbName = self::EbuyApiDBName;
				$update =$db->prepare("UPDATE $dbName.ClientCashCancelReceipt SET
					statusIDX = :changeStatus
					,resultTime = :resultTime
					,result_code = :result_code
					,result_msg = :result_msg
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $nextStatus);
				$update->bindValue(':resultTime', $nowTime);
				$update->bindValue(':result_code', $res_ResultCode);
				$update->bindValue(':result_msg', $res_ResultMsg);
				$update->bindValue(':targetIDX', $idx);
				$update->execute();
				if($clientIDX*1>1){
					$db = static::getDB();
					$dbName= self::MainDBName;
					$stat2=$db->prepare("INSERT INTO $dbName.ClientLog
						(statusIDX,targetIDX,clientIDX,createTime)
						VALUES
						(:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
					");
					$stat2->bindValue(':updateStatusIDX', $nextStatus);
					$stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
					$stat2->bindValue(':clientIDX', $clientIDX);
					$stat2->bindValue(':createTime', $nowTime);
					$stat2->execute();
					$ClientLogIDX=$db->lastInsertId();
					$stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
						(logIDX,ex)
						VALUES
						(:logIDX,:ex)
					");
					$stat3->bindValue(':logIDX', $ClientLogIDX);
					$stat3->bindValue(':ex', $res_ResultMsg);
					$stat3->execute();
				}
			}else{
                $nextStatus=811903;
                $insEX='통신오류발생: '.$erro;
                $nowTime=date("Y-m-d H:i:s");
                $db     = static::GetApiDB();
                $dbName = self::EbuyApiDBName;
                $update =$db->prepare("UPDATE $dbName.ClientCashCancelReceipt SET
                    statusIDX = :changeStatus
                    ,resultTime = :resultTime
                    ,result_msg = :result_msg
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $nextStatus);
                $update->bindValue(':resultTime', $nowTime);
                $update->bindValue(':result_msg', $insEX);
                $update->bindValue(':targetIDX', $idx);
                $update->execute();
                if($clientIDX*1>1){
                    $db = static::getDB();
                    $dbName= self::MainDBName;
                    $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                        (statusIDX,targetIDX,clientIDX,createTime)
                        VALUES
                        (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                    ");
                    $stat2->bindValue(':updateStatusIDX', $nextStatus);
                    $stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
                    $stat2->bindValue(':clientIDX', $clientIDX);
                    $stat2->bindValue(':createTime', $nowTime);
                    $stat2->execute();
                    $ClientLogIDX=$db->lastInsertId();
                    $stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
                        (logIDX,ex)
                        VALUES
                        (:logIDX,:ex)
                    ");
                    $stat3->bindValue(':logIDX', $ClientLogIDX);
                    $stat3->bindValue(':ex', $insEX);
                    $stat3->execute();
                }
            }
            $nowTime=date("Y-m-d H:i:s");
			$db     = static::GetApiDB();
			$dbName = self::EbuyApiDBName;
			$insert =$db->prepare("INSERT INTO $dbName.ClientCashCancelReceiptFullData
				(
					ClientCashCancelReceiptIDX,
					fullData,
					createTime
				)
				VALUES
				(
					:ClientCashCancelReceiptIDX,
					:fullData,
					:createTime
				)
			");
			$insert->bindValue(':ClientCashCancelReceiptIDX', $idx);
			$insert->bindValue(':fullData', $receiveJsonData);
			$insert->bindValue(':createTime', $nowTime);
			$insert->execute();
        }
        $resultData = ['result'=>'t','aa'=>$tid,'bb'=>$amount,'cc'=>$msg];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}