<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketBalanceMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketBalanceHistoryCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{

        $dataPack=MarketMo::GetMarketAllListData();
        $renderData=[
            'dataPack'=>$dataPack
        ];
		View::renderTemplate('page/marketBalanceHistory/marketBalanceHistory.html',$renderData);
	}//렌더

	//marketBalanceHistory.html 데이터테이블
    public function DataTableListLoad($data=null)
    {
        $dataPack=MarketBalanceMo::GetDatatableList();
        $resultData = [
	        'result'=>'t',
            'data'=>$dataPack
	    ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    // public function logExUpdate()
    // {
    //     if(!isset($_POST['idx'])||empty($_POST['idx'])){
    //         $errMsg='idx 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg,'n');
    //     }
    //     if(!isset($_POST['memo'])||empty($_POST['memo'])){
    //         $errMsg='memo 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg,'n');
    //     }
    //     $idx=$_POST['idx'];
    //     $memo=$_POST['memo'];

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.PortalLogEx SET
    //         ex=:memo
    //         WHERE targetIDX=:idx
    //     ");
    //     $stat1->bindValue(':memo', $memo);
    //     $stat1->bindValue(':idx', $idx);
    //     $stat1->execute();

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

}