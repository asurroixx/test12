<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\EbuyBankLogMo;
use App\Models\MileageMo;
use App\Models\StaffMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MileageCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//임시
    public function tmpLogInsert($data=null){
        if(!isset($_POST['clientBankIDX'])&&empty($_POST['clientBankIDX'])){
            $errMsg='clientBankIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientBank'])&&empty($_POST['clientBank'])){
            $errMsg='clientBank 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientAccountHolder'])&&empty($_POST['clientAccountHolder'])){
            $errMsg='clientAccountHolder 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['withdrawalAmount'])&&empty($_POST['withdrawalAmount'])){
            $errMsg='withdrawalAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['depositAmount'])&&empty($_POST['depositAmount'])){
            $errMsg='depositAmount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['ebuyBankIDX'])&&empty($_POST['ebuyBankIDX'])){
            $errMsg='ebuyBankIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $clientBankIDX=$_POST['clientBankIDX'];
        $clientBank=$_POST['clientBank'];
        $clientAccountHolder=$_POST['clientAccountHolder'];
        $withdrawalAmount=$_POST['withdrawalAmount'];
        $depositAmount=$_POST['depositAmount'];
        $ebuyBankIDX=$_POST['ebuyBankIDX'];
        $createTime=date("Y-m-d H:i:s");

        $db = static::GetApiDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.EbuyBankLog
            (withdrawalAmount,depositAmount,afterAmount,clientBankIDX,clientBank,clientAccountHolder,ebuyBankIDX,tradeTime,statusIDX,targetIDX)
            VALUES
            (:withdrawalAmount,:depositAmount,:afterAmount,:clientBankIDX,:clientBank,:clientAccountHolder,:ebuyBankIDX,:tradeTime,:statusIDX,:targetIDX)
        ");

        $stat1->bindValue(':withdrawalAmount', $withdrawalAmount);
        $stat1->bindValue(':depositAmount', $depositAmount);
        $stat1->bindValue(':afterAmount', 0);
        $stat1->bindValue(':clientBankIDX', $clientBankIDX);
        $stat1->bindValue(':clientBank', $clientBank);
        $stat1->bindValue(':clientAccountHolder', $clientAccountHolder);
        $stat1->bindValue(':ebuyBankIDX', $ebuyBankIDX);
        $stat1->bindValue(':tradeTime', $createTime);
        $stat1->bindValue(':statusIDX', 101101);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->execute();


		$scrapingIDX = $db->lastInsertId();

        //소켓 데이터테이블
        $socketTable=$this->socketTable($scrapingIDX);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	//mileage.html 렌더
	public function Render($data=null)
	{
		$bankPack=EbuyBankMo::GetEbuyBankData();
		$renderData=['bankPack'=>$bankPack];
		View::renderTemplate('page/mileage/mileage.html',$renderData);
	}

	//mileage.html 날짜별 리셋 데이터
    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=EbuyBankLogMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileage.html 솔팅별 리셋 데이터
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $columnsVal=$_POST['columnsVal'];
        $totalCountData=EbuyBankLogMo::GetTotalSortingData($columnsVal);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


	//mileage.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=EbuyBankLogMo::GetDataTableListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileage.html 해당 거래 디테일 로드
    public function MileageDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=EbuyBankLogMo::GetMileageDetail($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/mileage/mileageDetail.html',$renderData);
    }

}