<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use \Core\Controller;
use \Core\DefinAll;
use App\Models\DevAlarmMsgMo;
use App\Models\MarketManagerMo;
use App\Models\StaffMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class DevAlarmCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public static function sendAlarmToDev($data=null)
    {
        $statusIDX = $data['statusIDX'];
        $targetIDX = $data['targetIDX'];
        $marketIDX = $data['marketIDX'];
        $paramVal = $data["param"];


        $getAlarmMsg = DevAlarmMsgMo::getDevAlarmMsg($statusIDX);
        if(!isset($getAlarmMsg['idx'])){
            $errMsg='기준 알람내용이 존재하지 않습니다.';
            // $errOn=self::errExport($errMsg);
            return false;
        }

        $title = $getAlarmMsg['title'];
        $content = $getAlarmMsg['con'];
        $param = $getAlarmMsg['param'];
        $paramArr = explode(",", $param);

        foreach ($paramArr as $key ) {
            if (isset($paramVal[$key])) {
                $fullKey="{{".$key."}}";
                $replaceKey = $paramVal[$key];
                $content = str_replace($fullKey, $replaceKey, $content);
            }
        }

        //메뉴IDX 넣을때 샌드박스 db portalMenuStd 메뉴IDX로 넣어야함
        if($statusIDX=='340101'){
            //Qna관련
            $menuIDX=107;
        }elseif($statusIDX=='417201'||$statusIDX=='417211'){
            //정산관련
            $menuIDX=82;
        }elseif($statusIDX=='418201'||$statusIDX=='418211'){
            //역정산관련
            $menuIDX=86;
        }elseif($statusIDX=='906101'){
            //Deposit 관련
            $menuIDX=81;
        }elseif($statusIDX=='908101'){
            //Withdrawal 관련
            $menuIDX=80;
        }elseif($statusIDX=='351101'||$statusIDX=='351102'||$statusIDX=='351201'){
            //White Ip
            $menuIDX=84;
        }else{
            $menuIDX=0;
        }

        //샌드박스 db의 devAlarm 테이블이 쌓기
        $dbName= self::EbuySandboxDBName;
        $db = static::GetSandboxDB();
        $paramArr =['marketIDX'=>$marketIDX, 'statusIDX'=>$statusIDX];
        $getMarketManagerList=MarketManagerMo::getMarketManagerListForSandboxAlarm($paramArr);

        $createTime=date('Y-m-d H:i:s');

        $marketManagerIDXPack = [];

        if($statusIDX==340101){

            $marketManagerIDXPack[] = $marketIDX; // 실제론 샌드박스 마켓매니저idx만 들어있음
            $stat1=$db->prepare("INSERT INTO $dbName.PortalAlarm
                (marketManagerIDX, statusIDX, targetIDX, con, createTime,viewStatusIDX,menuIDX)
                VALUES
                (:marketManagerIDX, :statusIDX , :targetIDX, :con, :createTime , :viewStatusIDX, :menuIDX)
            ");

            $stat1->bindValue(':marketManagerIDX', $marketIDX);
            $stat1->bindValue(':statusIDX', $statusIDX);
            $stat1->bindValue(':targetIDX', $targetIDX);
            $stat1->bindValue(':con', $content);
            $stat1->bindValue(':createTime', $createTime);
            $stat1->bindValue(':viewStatusIDX', 405201);
            $stat1->bindValue(':menuIDX', $menuIDX);
            $stat1->execute();
        }else{
            //인서트를 샌드박스 db에 해야함
            $query = "INSERT INTO $dbName.PortalAlarm (marketManagerIDX, statusIDX, targetIDX, con, createTime,viewStatusIDX,menuIDX) VALUES ";
            $bulkString = [];

            foreach ($getMarketManagerList as $key) {
                $marketManagerIDX = $key['idx'];
                $alarmSettingIDX = $key['alarmSettingIDX'];
                if($alarmSettingIDX==0 || $alarmSettingIDX==null || $alarmSettingIDX==""){
                    //알람받을거야
                    $marketManagerIDXPack[] = $key['idx'];
                }
                $bulkString[] = "(:marketManagerIDX".$marketManagerIDX.", :statusIDX , :targetIDX, :con, :createTime , :viewStatusIDX".$marketManagerIDX.", :menuIDX)";
            }
            $query .= implode(", ", $bulkString);





            $result = $db->prepare($query);
            // 바인딩 변수 설정
            foreach ($getMarketManagerList as $key) {
                $marketManagerIDX = $key['idx'];
                $alarmSettingIDX = $key['alarmSettingIDX'];
                if($alarmSettingIDX>0){
                    $viewStatusIDX=405101;
                }else{
                    $viewStatusIDX=405201;
                }
                $paramName = ":marketManagerIDX".$marketManagerIDX;
                $result->bindValue($paramName, $marketManagerIDX);
                $paramName = ":viewStatusIDX".$marketManagerIDX;
                $result->bindValue($paramName, $viewStatusIDX);
            }
            $result->bindValue(":statusIDX", $statusIDX);
            $result->bindValue(":targetIDX", $targetIDX);
            $result->bindValue(":con", $content);
            $result->bindValue(":createTime", $createTime);
            $result->bindValue(":menuIDX", $menuIDX);
            $result->execute();
            //샌드박스 db의 devAlarm 테이블이 쌓기
        }




        //dev에 socket통신 하기
        $DevIoUri=self::DevIoUri;
        $dataPack = ['marketManagerIDXPack'=>$marketManagerIDXPack, 'menuIDX'=>$menuIDX, 'con'=>$content,'marketIDX'=>$marketIDX,'statusIDX'=>$statusIDX,'targetIDX'=>$targetIDX];
        $uri = $DevIoUri.'/DevAlarm';
        self::sendCurl($dataPack,$uri);
        //dev에 socket통신 하기




    }



}