<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffMo;
use App\Models\MarketMo;
use App\Models\ManagerMo;
use App\Models\MarketKeyMo;
use App\Models\ZZZTestMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class AAACon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

    //
	// public function Render($data=null)
	// {
	// 	View::renderTemplate('page/staff/staff.html');
	// }

	//staff.html 데이터테이블 리스트 로드


    // public function MigrationPartner()
    // {
    //     $db = static::GetDB();
    //     $query = $db->prepare("SELECT
    //     A_ID,
    //     A_NAME,
    //     A_CODE
    //     FROM ebuyyy.AA_AGENT_PARTNER
    //     ");
    //     $query->execute();
    //     $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

    //     //벌크인서트 본 서버 partner
    //     $dbName= self::MainDBName;
    //     $createTime=date('Y-m-d H:i:s');
    //     $query = "INSERT INTO $dbName.Partner (code, name, createTime, migrationIDX) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['A_ID'];
    //         $bulkString[] = "(:code".$idx.", :name".$idx.", :createTime, :migrationIDX".$idx.")";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result = $db->prepare($query);

    //     // 바인딩 변수 설정
    //     foreach ($migrationResult as $key2) {
    //         $pIDX=$key2['A_ID'];
    //         $pName=$key2['A_NAME'];
    //         $pCode=$key2['A_CODE'];

    //         $result->bindValue(":code" . $pIDX, $pCode);
    //         $result->bindValue(":name" . $pIDX, $pName);
    //         $result->bindValue(":migrationIDX" . $pIDX, $pIDX);
    //     }
    //     $result->bindValue(":createTime", $createTime);
    //     // $result->execute();
    //     // 벌크인서트 본 서버 partner

    //     //벌크인서트 샌드박스 서버 partner

    //     $sandboxDb = static::GetSandboxDB();
    //     $dbName= self::EbuySandboxDBName;
    //     $query = "INSERT INTO $dbName.Partner (code, name, createTime, migrationIDX) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['A_ID'];
    //         $bulkString[] = "(:code".$idx.", :name".$idx.", :createTime, :migrationIDX".$idx.")";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result2 = $sandboxDb->prepare($query);

    //     // 바인딩 변수 설정
    //     foreach ($migrationResult as $key2) {
    //         $pIDX=$key2['A_ID'];
    //         $pName=$key2['A_NAME'];
    //         $pCode=$key2['A_CODE'];

    //         $result2->bindValue(":code" . $pIDX, $pCode.'_SANDBOX');
    //         $result2->bindValue(":name" . $pIDX, $pName);
    //         $result2->bindValue(":migrationIDX" . $pIDX, $pIDX);
    //     }
    //     $result2->bindValue(":createTime", $createTime);
    //     // $result2->execute();
    //     // 벌크인서트 샌드박스 서버 partner

    //     $resultData = ['data'=>$result,'result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // public function MigrationMarket()
    // {
    //     $db = static::GetDB();
    //     $query = $db->prepare("SELECT
    //     A.M_ID AS idx,
    //     A.M_CODE AS code,
    //     A.M_NAME AS name,
    //     A.M_P_ID AS partnerIDX,
    //     CASE A.stat
    //         WHEN 1 THEN 412101
    //         WHEN 2 THEN 412201
    //         ELSE 412202 END
    //     AS statusIDX,
    //     B.A_MANAGER_CALL AS phoneNumber,
    //     B.A_FEE_PER_PARTNER AS marketDepositFeePer,
    //     0 AS marketWithdrawalFeePer,
    //     0 AS marketSettleBankwireFeePer,
    //     B.feePerAgentPayCrypto AS marketSettleCryptoFeePer,
    //     B.feePerReversePayCrypto AS marketTopupCryptoFeePer,
    //     0 AS marketTopupBankwireFeePer,
    //     B.minusLimit AS marketMinusLimitAmount,
    //     B.A_CHARGE_MIN AS marketDepositMinAmount,
    //     B.A_CHARGE_MAX AS marketDepositMaxAmount
    //     FROM ebuyyy.AA_AGENT_PARTNER_MARKET AS A
    //     JOIN ebuyyy.AA_AGENT_PARTNER AS B ON A.M_P_ID = B.A_ID
    //     ");
    //     $query->execute();
    //     $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

    //     //-------------------------------벌크인서트 본 서버 마켓 추가---------------------------------
    //     $dbName= self::MainDBName;

    //     // 없는컬럼
    //     $categoryIDX=1;
    //     $depositStatusIDX=413101;
    //     $withdrawalStatusIDX=414101;
    //     $hostUrl='https://www.portal.com';
    //     $serviceUrl='https://www.portal.com';
    //     $createTime=date('Y-m-d H:i:s');

    //     $query = "INSERT INTO $dbName.Market (
    //         code,
    //         name,
    //         partnerIDX,
    //         categoryIDX,
    //         statusIDX,
    //         depositStatusIDX,
    //         withdrawalStatusIDX,
    //         phoneNumber,
    //         hostUrl,
    //         serviceUrl,
    //         marketDepositFeePer,
    //         marketWithdrawalFeePer,
    //         marketSettleBankwireFeePer,
    //         marketSettleCryptoFeePer,
    //         marketTopupCryptoFeePer,
    //         marketTopupBankwireFeePer,
    //         marketMinusLimitAmount,
    //         marketDepositMinAmount,
    //         marketDepositMaxAmount,
    //         createTime,
    //         migrationIDX
    //     ) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['idx'];
    //         $bulkString[] = "(
    //         :code".$idx.",
    //         :name".$idx.",
    //         :partnerIDX".$idx.",
    //         :categoryIDX,
    //         :statusIDX".$idx.",
    //         :depositStatusIDX,
    //         :withdrawalStatusIDX,
    //         :phoneNumber".$idx.",
    //         :hostUrl,
    //         :serviceUrl,
    //         :marketDepositFeePer".$idx.",
    //         :marketWithdrawalFeePer".$idx.",
    //         :marketSettleBankwireFeePer".$idx.",
    //         :marketSettleCryptoFeePer".$idx.",
    //         :marketTopupCryptoFeePer".$idx.",
    //         :marketTopupBankwireFeePer".$idx.",
    //         :marketMinusLimitAmount".$idx.",
    //         :marketDepositMinAmount".$idx.",
    //         :marketDepositMaxAmount".$idx.",
    //         :createTime,
    //         :migrationIDX".$idx."
    //         )";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result = $db->prepare($query);

    //     // 바인딩 변수 설정
    //     foreach ($migrationResult as $key2) {
    //         $idx=$key2['idx'];
    //         $code=$key2['code'];
    //         $name=$key2['name'];
    //         $partnerIDX = $key2['partnerIDX'];
    //         $statusIDX=$key2['statusIDX'];
    //         $phoneNumber=$key2['phoneNumber'];
    //         $marketDepositFeePer=$key2['marketDepositFeePer'];
    //         $marketWithdrawalFeePer=$key2['marketWithdrawalFeePer'];
    //         $marketSettleBankwireFeePer=$key2['marketSettleBankwireFeePer'];
    //         $marketSettleCryptoFeePer=$key2['marketSettleCryptoFeePer'];
    //         $marketTopupCryptoFeePer=$key2['marketTopupCryptoFeePer'];
    //         $marketTopupBankwireFeePer=$key2['marketTopupBankwireFeePer'];
    //         $marketMinusLimitAmount=$key2['marketMinusLimitAmount'];
    //         $marketDepositMinAmount=$key2['marketDepositMinAmount'];
    //         $marketDepositMaxAmount=$key2['marketDepositMaxAmount'];

    //         $result->bindValue(":code" . $idx, $code);
    //         $result->bindValue(":name" . $idx, $name);
    //         $result->bindValue(":partnerIDX" . $idx, $partnerIDX);
    //         $result->bindValue(":statusIDX" . $idx, $statusIDX);
    //         $result->bindValue(":phoneNumber" . $idx, $phoneNumber);
    //         $result->bindValue(":marketDepositFeePer" . $idx, $marketDepositFeePer);
    //         $result->bindValue(":marketWithdrawalFeePer" . $idx, $marketWithdrawalFeePer);
    //         $result->bindValue(":marketSettleBankwireFeePer" . $idx, $marketSettleBankwireFeePer);
    //         $result->bindValue(":marketSettleCryptoFeePer" . $idx, $marketSettleCryptoFeePer);
    //         $result->bindValue(":marketTopupCryptoFeePer" . $idx, $marketTopupCryptoFeePer);
    //         $result->bindValue(":marketTopupBankwireFeePer" . $idx, $marketTopupBankwireFeePer);
    //         $result->bindValue(":marketMinusLimitAmount" . $idx, $marketMinusLimitAmount);
    //         $result->bindValue(":marketDepositMinAmount" . $idx, $marketDepositMinAmount);
    //         $result->bindValue(":marketDepositMaxAmount" . $idx, $marketDepositMaxAmount);
    //         $result->bindValue(":migrationIDX" . $idx, $idx);
    //     }


    //     $result->bindValue(":categoryIDX", $categoryIDX);
    //     $result->bindValue(":depositStatusIDX", $depositStatusIDX);
    //     $result->bindValue(":withdrawalStatusIDX", $withdrawalStatusIDX);
    //     $result->bindValue(":hostUrl", $hostUrl);
    //     $result->bindValue(":serviceUrl", $serviceUrl);
    //     $result->bindValue(":createTime", $createTime);
    //     // $result->execute();

    //     //-------------------------------벌크인서트 본 서버 마켓 추가---------------------------------
    //     $ipAddress  = $this->GetIPaddress();


    //     //------------------------------------------------샌드박스 마켓 벌크인서트--------------------------------
    //     $sandboxDb = static::GetSandboxDB();
    //     $dbName= self::EbuySandboxDBName;

    //     $query = "INSERT INTO $dbName.Market (
    //         targetMarketIDX,
    //         code,
    //         name,
    //         partnerIDX,
    //         categoryIDX,
    //         statusIDX,
    //         depositStatusIDX,
    //         withdrawalStatusIDX,
    //         phoneNumber,
    //         hostUrl,
    //         serviceUrl,
    //         marketDepositFeePer,
    //         marketWithdrawalFeePer,
    //         marketSettleBankwireFeePer,
    //         marketSettleCryptoFeePer,
    //         marketTopupCryptoFeePer,
    //         marketTopupBankwireFeePer,
    //         marketMinusLimitAmount,
    //         marketDepositMinAmount,
    //         marketDepositMaxAmount,
    //         createTime,
    //         migrationIDX
    //     ) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['idx'];
    //         $bulkString[] = "(
    //         :targetMarketIDX".$idx.",
    //         :code".$idx.",
    //         :name".$idx.",
    //         :partnerIDX".$idx.",
    //         :categoryIDX,
    //         :statusIDX".$idx.",
    //         :depositStatusIDX,
    //         :withdrawalStatusIDX,
    //         :phoneNumber".$idx.",
    //         :hostUrl,
    //         :serviceUrl,
    //         :marketDepositFeePer".$idx.",
    //         :marketWithdrawalFeePer".$idx.",
    //         :marketSettleBankwireFeePer".$idx.",
    //         :marketSettleCryptoFeePer".$idx.",
    //         :marketTopupCryptoFeePer".$idx.",
    //         :marketTopupBankwireFeePer".$idx.",
    //         :marketMinusLimitAmount".$idx.",
    //         :marketDepositMinAmount".$idx.",
    //         :marketDepositMaxAmount".$idx.",
    //         :createTime,
    //         :migrationIDX".$idx."
    //         )";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result2 = $sandboxDb->prepare($query);

    //     $EbuySandboxApiDBName= self::EbuySandboxApiDBName;
    //     // 바인딩 변수 설정
    //     foreach ($migrationResult as $key2) {
    //         $idx=$key2['idx'];
    //         $code=$key2['code'];
    //         $name=$key2['name'];
    //         $partnerIDX = $key2['partnerIDX'];
    //         $statusIDX=$key2['statusIDX'];
    //         $phoneNumber=$key2['phoneNumber'];
    //         $marketDepositFeePer=$key2['marketDepositFeePer'];
    //         $marketWithdrawalFeePer=$key2['marketWithdrawalFeePer'];
    //         $marketSettleBankwireFeePer=$key2['marketSettleBankwireFeePer'];
    //         $marketSettleCryptoFeePer=$key2['marketSettleCryptoFeePer'];
    //         $marketTopupCryptoFeePer=$key2['marketTopupCryptoFeePer'];
    //         $marketTopupBankwireFeePer=$key2['marketTopupBankwireFeePer'];
    //         $marketMinusLimitAmount=$key2['marketMinusLimitAmount'];
    //         $marketDepositMinAmount=$key2['marketDepositMinAmount'];
    //         $marketDepositMaxAmount=$key2['marketDepositMaxAmount'];

    //         $result2->bindValue(":targetMarketIDX" . $idx, $idx);
    //         $result2->bindValue(":code" . $idx, $code.'_SANDBOX');
    //         $result2->bindValue(":name" . $idx, $name);
    //         $result2->bindValue(":partnerIDX" . $idx, $partnerIDX);
    //         $result2->bindValue(":statusIDX" . $idx, $statusIDX);
    //         $result2->bindValue(":phoneNumber" . $idx, $phoneNumber);
    //         $result2->bindValue(":marketDepositFeePer" . $idx, $marketDepositFeePer);
    //         $result2->bindValue(":marketWithdrawalFeePer" . $idx, $marketWithdrawalFeePer);
    //         $result2->bindValue(":marketSettleBankwireFeePer" . $idx, $marketSettleBankwireFeePer);
    //         $result2->bindValue(":marketSettleCryptoFeePer" . $idx, $marketSettleCryptoFeePer);
    //         $result2->bindValue(":marketTopupCryptoFeePer" . $idx, $marketTopupCryptoFeePer);
    //         $result2->bindValue(":marketTopupBankwireFeePer" . $idx, $marketTopupBankwireFeePer);
    //         $result2->bindValue(":marketMinusLimitAmount" . $idx, $marketMinusLimitAmount);
    //         $result2->bindValue(":marketDepositMinAmount" . $idx, $marketDepositMinAmount);
    //         $result2->bindValue(":marketDepositMaxAmount" . $idx, $marketDepositMaxAmount);
    //         $result2->bindValue(":migrationIDX" . $idx, $idx);
    //     }
    //     $result2->bindValue(":categoryIDX", $categoryIDX);
    //     $result2->bindValue(":depositStatusIDX", $depositStatusIDX);
    //     $result2->bindValue(":withdrawalStatusIDX", $withdrawalStatusIDX);
    //     $result2->bindValue(":hostUrl", $hostUrl);
    //     $result2->bindValue(":serviceUrl", $serviceUrl);
    //     $result2->bindValue(":createTime", $createTime);
    //     // $result2->execute();
    //     //------------------------------------------------샌드박스 마켓 벌크인서트--------------------------------


    //     //------------------------------------------------샌드박스 마켓 키 발급--------------------------------
    //     function ApiKeyString($length)  {
    //         $characters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    //         $string_generated = "";
    //         $nmr_loops = $length;
    //         while ($nmr_loops--){
    //             $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
    //         }
    //         $issetString=MarketKeyMo::MarketkApiKeyChkByMarketKey($string_generated);
    //         if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
    //             return ApiKeyString($length);
    //         }else{
    //             return $string_generated;
    //         }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
    //     }

    //     foreach ($migrationResult as $key) {
    //         $marketCode=$key['code'].'_SANDBOX';
    //         $marketApiKey=ApiKeyString(64);//회사코드는 10자리
    //         $stat5=$sandboxDb->prepare("INSERT INTO $EbuySandboxApiDBName.MarketKey
    //             (marketCode,marketKey,ip,statusIDX,managerIDX,createTime)
    //             VALUES
    //             (:marketCode,:marketKey,:ip,:statusIDX,:managerIDX,:createTime)
    //         ");
    //         $stat5->bindValue(':marketCode', $marketCode);
    //         $stat5->bindValue(':marketKey', $marketApiKey);//임시
    //         $stat5->bindValue(':ip', $ipAddress);
    //         $stat5->bindValue(':statusIDX', 429101);
    //         $stat5->bindValue(':managerIDX', 0);
    //         $stat5->bindValue(':createTime', $createTime);
    //         // $stat5->execute();
    //     }
    //     //------------------------------------------------샌드박스 마켓 키 발급--------------------------------

    //     //------------------모든 샌드박스 마켓 발란스 증가 1,000,000 $ ---------------------------------
    //     $marketBalance=$this->SandBoxAllMarketBalanceUp();
    //     //------------------모든 샌드박스 마켓 발란스 증가 1,000,000 $ ---------------------------------





    //     $resultData = ['data'=>$result,'result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // public function SandBoxAllMarketBalanceUp()
    // {
    //     $db = static::GetSandboxDB();
    //     $dbName= self::MainDBName;
    //     $query = $db->prepare("SELECT
    //     idx
    //     FROM $dbName.Market
    //     ");
    //     $query->execute();
    //     $result=$query->fetchAll(PDO::FETCH_ASSOC);
    //     $createTime=date('Y-m-d H:i:s');

    //     foreach ($result as $key) {
    //         $idx=$key['idx'];
    //         $stat8=$db->prepare("INSERT INTO $dbName.MarketBalance
    //              (marketIDX,statusIDX,targetIDX,amount,createTime)
    //              VALUES
    //              (:marketIDX,:statusIDX,:targetIDX,:amount,:createTime)
    //          ");

    //          $stat8->bindValue(':marketIDX', $idx);
    //          $stat8->bindValue(':statusIDX', 419101);
    //          $stat8->bindValue(':targetIDX', 0);
    //          $stat8->bindValue(':amount', 1000000);
    //          $stat8->bindValue(':createTime', $createTime);
    //          $stat8->execute();
    //     }

    // }

    // public function MigrationMarketManager(){
    //     $db = static::GetDB();
    //     $query = $db->prepare("SELECT
    //     A.idx,
    //     A.email_address AS email,
    //     A.user_name AS name,
    //     A.partner_lev AS gradeIDX,
    //     A.partner_position AS position,
    //     D.idx AS marketIDX
    //     FROM ebuyyy.AA_SIGNUP AS A
    //     JOIN ebuyyy.AA_AGENT_PARTNER AS B ON A.partner_code = B.A_CODE
    //     JOIN ebuyyy.AA_AGENT_PARTNER_MARKET AS C ON B.A_CODE = C.M_P_CODE
    //     LEFT JOIN ebuy.Market AS D ON D.migrationIDX = C.M_ID
    //     WHERE A.partner_lev = 1 AND A.status = 0
    //     ");
    //     $query->execute();
    //     $migrationResult=$query->fetchAll(PDO::FETCH_ASSOC);

    //     $ipAddress = $this->GetIPaddress();
    //     $createTime=date("Y-m-d H:i:s");
    //     $gradeIDX=1;
    //     $statusIDX=402101;

    //     $dataDbKey=self::dataDbKey;
    //     $dbName= self::MainDBName;


    //     //-------------------마켓매니저------------------------------

    //     $query = "INSERT INTO $dbName.MarketManager (
    //         name,
    //         marketIDX,
    //         managerIDX,
    //         gradeIDX,
    //         statusIDX,
    //         position,
    //         createTime,
    //         migrationIDX
    //     ) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['idx'];
    //         $bulkString[] = "(
    //         AES_ENCRYPT(:name".$idx.", :dataDbKey),
    //         :marketIDX".$idx.",
    //         :managerIDX".$idx.",
    //         :gradeIDX,
    //         :statusIDX,
    //         :position".$idx.",
    //         :createTime,
    //         :migrationIDX".$idx."
    //     )";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result = $db->prepare($query);

    //     // 바인딩 변수 설정
    //     foreach ($migrationResult as $key2) {
    //         $idx=$key2['idx'];
    //         $name=$key2['name'];
    //         $marketIDX=$key2['marketIDX'];
    //         $position=$key2['position'];

    //         $email=$key2['email'];
    //         $issetEmailData=ManagerMo::IssetManagerEmail($email);
    //         if(isset($issetEmailData['idx'])){
    //             $managerIDX=$issetEmailData['idx'];
    //         }else{

    //             $createTime=date("Y-m-d H:i:s");
    //             $stat1=$db->prepare("INSERT INTO $dbName.Manager
    //                 (email,ipAddress,createTime,migrationIDX)
    //                 VALUES
    //                 (AES_ENCRYPT(:email, '$dataDbKey'),:ipAddress,:createTime,:migrationIDX)
    //             ");
    //             $stat1->bindValue(':email', $email);
    //             $stat1->bindValue(':ipAddress', $ipAddress);
    //             $stat1->bindValue(':createTime', $createTime);
    //             $stat1->bindValue(':migrationIDX', $idx);
    //             $stat1->execute();
    //             $managerIDX = $db->lastInsertId();
    //         }
    //         $result->bindValue(":name" . $idx, $name);
    //         $result->bindValue(":marketIDX" . $idx, $marketIDX);
    //         $result->bindValue(":managerIDX" . $idx, $managerIDX);
    //         $result->bindValue(":position" . $idx, $position);
    //         $result->bindValue(":migrationIDX" . $idx, $idx);
    //     }
    //     $result->bindValue(":gradeIDX", $gradeIDX);
    //     $result->bindValue(":statusIDX", $statusIDX);
    //     $result->bindValue(":createTime", $createTime);
    //     $result->bindValue(":dataDbKey", $dataDbKey);
    //     $result->execute();

    //     //-------------------마켓매니저------------------------------

    //     //-------------------------------샌드박스 마켓매니저------------------------------------------

    //     $dbsand = static::GetSandboxDB();
    //     $dbNameSand= self::EbuySandboxDBName;

    //     $query = "INSERT INTO $dbName.MarketManager (
    //         name,
    //         marketIDX,
    //         managerIDX,
    //         gradeIDX,
    //         statusIDX,
    //         position,
    //         createTime,
    //         migrationIDX
    //     ) VALUES ";
    //     $bulkString = [];

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['idx'];
    //         $bulkString[] = "(
    //         AES_ENCRYPT(:name".$idx.", :dataDbKey),
    //         :marketIDX".$idx.",
    //         :managerIDX".$idx.",
    //         :gradeIDX,
    //         :statusIDX,
    //         :position".$idx.",
    //         :createTime,
    //         :migrationIDX".$idx."
    //     )";
    //     }

    //     $query .= implode(", ", $bulkString);
    //     $result2 = $dbsand->prepare($query);

    //     foreach ($migrationResult as $key) {
    //         $idx=$key['idx'];
    //         $name=$key['name'];
    //         $marketIDX=$key['marketIDX'];
    //         $position=$key['position'];

    //         $email=$key['email'];
    //         $managerDupleChkInSand = ManagerMo::issetEmailInSand($email);
    //         $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($marketIDX);
    //         if(isset($marketIDXInSandboxSel['idx'])){
    //             $sandboxMarketIDX = $marketIDXInSandboxSel['idx'];
    //             if(!isset($managerDupleChkInSand['idx'])){
    //                 $query=$dbsand->prepare("INSERT INTO $dbNameSand.Manager (email,ipAddress, createTime,migrationIDX)
    //                    VALUES (AES_ENCRYPT(:email, '$dataDbKey'), :ipAddress, :createTime , :migrationIDX)
    //                 ");
    //                 $query->bindValue(':email', $email);
    //                 $query->bindValue(':ipAddress', $ipAddress);
    //                 $query->bindValue(':createTime', $createTime);
    //                 $query->bindValue(':migrationIDX', $idx);
    //                 $query->execute();
    //                 $managerIDX=$dbsand->lastInsertId();
    //             }else{
    //                 $managerIDX=$managerDupleChkInSand['idx'];
    //             }
    //         }
    //         $result2->bindValue(":name" . $idx, $name);
    //         $result2->bindValue(":marketIDX" . $idx, $marketIDX);
    //         $result2->bindValue(":managerIDX" . $idx, $managerIDX);
    //         $result2->bindValue(":position" . $idx, $position);
    //         $result2->bindValue(":migrationIDX" . $idx, $idx);

    //     }
    //     $result2->bindValue(":gradeIDX", $gradeIDX);
    //     $result2->bindValue(":statusIDX", $statusIDX);
    //     $result2->bindValue(":createTime", $createTime);
    //     $result2->bindValue(":dataDbKey", $dataDbKey);
    //     $result2->execute();

    //     //-------------------------------샌드박스 마켓매니저------------------------------------------

    //     $resultData = ['data'=>$result,'result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;

    // }

    public function TETETE($data=null){
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //loginIDX로 테스트 테이블에 자기 token값을 조사함
        /*
            1. WHERE 390201와 보낸사람의 idx와 loginIDX값이 같은 token값 1개 가져오기
                유효성
                1-1. 2개 이상 있으면 X
                1-2. 3분이 지난건 X
            2. 해당 토큰 값을 PageToken에 인서트 (?)
            3. 그 후 https://staff.ebuycompany.com/clientDetail?token='인서트한 토큰값'&clientIDX='보낸 clientIDX값' uri 생성 후
            4. uri result값에 넣어서 보냄
            5. 채팅 서버에서 페이지 로드 할때 테스트 테이블 업데이트 할 수 있나?

            아니면

            1. uri 생성함 (clientDetail은 router에서 안잡히게 수정함)
                1-1. uri = $DomainUri.'/ClientDetailCon/Render?clientIDX='.$clientIDX;
                1-2. uri를 암호화함 = $uriEncrypt = $this->encrypt($returnUri,$loginKey);
            2. 암호화된 값을 테스트 테이블 token에 넣을까?
            3. 그럼 로그인한 idx와 clientIDX를 보내면





        */








        /*

            1. 이걸 먼저 채팅 서버에서 한개 넣어놔야함 토큰값은 현재 로그인한 loginToken 구해서 인서트함

            요청하기 전에 미리 인서트를 해야함



            ===========================================




            2.
            3. 스태프에서 현재 로그인한 loginToken과 idx를 모델에 보낸 후 WHERE token , loginIDX  ORDER BY createTime DESC LIMIT 1 애 1개 가져오기
                3-0. 값없으면 X
                    3-0-1. 값이 없다는건 loginToken과 idx가 맞지 않다는것 > 로그인 토큰이 달라지면 안맞음
                3-2. 3분이 지난건 X
                3-3. statusIDX 309201이면 X
            4. 유효성 모든게 통과되면 난 만들었던 uri 만들고 암호화해서 던짐
                4-1. 던진 후 ZZZTest에 statusIDX 309101로 업데이트
            5. 채팅서버에서 받음
                5-1. 컨펌으로 보여주려면 코어 컨트롤러 함수를 보내고
                    $returnUri = static::ClientDetail($clientIDX);
                5-2. 새창으로 보여주려면
                    $DomainUri.'/ClientDetailCon/Render?clientIDX='.$clientIDX;를 암호화 한 후 return으로 보내줌
            6. 암호화된걸 받은값을 복호화 한 후 그걸 띄워서 보여줌 (?)


        */
        $clientIDX = $_POST['clientIDX'];
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');
        $staffData=StaffMo::issetStaffData($staffIDX);
        if(!isset($staffData['idx'])){
            $errMsg='스태프 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $loginIDX = $staffData['idx'];
        $loginToken = $staffData['loginToken'];
        $createTime = date("Y-m-d H:i:s");
        $statusIDX = 309201;

        //채팅서버에서 인서트 했다 침
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.ChatClientToken
            (token,createTime,loginIDX,statusIDX)
            VALUES
            (:token,:createTime,:loginIDX,:statusIDX)
        ");

        $stat1->bindValue(':token', $loginToken);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':loginIDX', $loginIDX);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->execute();


        // $paramArr=[
        //     'token'=>$loginToken,
        //     'idx'=>$loginIDX
        // ];



        // $ZZZTestData=ZZZTestMo::IssetTargetData($paramArr);
        // if(!isset($ZZZTestData)){
        //     $errMsg='데이터 없음.';
        //     $errOn=$this::errExport($errMsg);
        // }

        // $tokenCreateTime = strtotime($ZZZTestData[0]['createTime']);
        // $currentDateTime = strtotime(date("Y-m-d H:i:s"));

        // // 두 시간 간의 차이 계산 (분 단위)
        // $timeDifference = ($currentDateTime - $tokenCreateTime) / 60;


        // // 차이가 3분 이상인지 확인
        // if ($timeDifference >= 3) {
        //     $errMsg = '3분 이상 차이남.';
        //     $errOn = $this::errExport($errMsg);
        // }

        // $tokenStatusIDX=$ZZZTestData[0]['statusIDX'];
        // if($tokenStatusIDX == 309101){
        //     $errMsg='이미 사용된 데이터임';
        //     $errOn=$this::errExport($errMsg);
        // }


        // $DomainUri = self::DomainUri;
        $loginKey = self::LoginKey;
        $returnUri = 'clientIDX='.$clientIDX.'&idx='.$loginIDX;
        // $returnUri = static::ClientDetail($clientIDX);
        $uriEncrypt = $this->encrypt($returnUri,$loginKey);

        // print_r($uriEncrypt);




        // // $uriDecrypt = $this->decrypt($uriEncrypt,$loginKey);
        // $resultData = ['result'=>'t','data'=>$uriEncrypt];
        // $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        // echo $result;


        $resultData = ['result'=>'t','data'=>$uriEncrypt];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;



        // $targetIDX=$ZZZTestData[0]['idx'];
        // $changeStatusIDX = 309101;
        // $db = static::getDB();
        // $dbName= self::MainDBName;
        // $stat1=$db->prepare("UPDATE $dbName.ZZZTest SET
        //     statusIDX=:changeStatusIDX
        //     WHERE idx=:idx
        // ");
        // $stat1->bindValue(':changeStatusIDX', $changeStatusIDX);
        // $stat1->bindValue(':idx', $targetIDX);
        // $stat1->execute();




        //








        // $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
        // $uri = $DomainUri.':16001/ClientDetail';
        // $controller = new self(); // 현재 클래스의 인스턴스 생성
        // $curlSend = static::curlSend($dataPack,$uri);


        // $alarmParam=[
        //     'targetIDX'=>$targetIDX,
        //     'statusIDX'=>340101,
        //     'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
        //     'param'=>[]
        // ];
        // PortalAlarmCon::sendAlarmToPortal($alarmParam);



    }



}