<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientGradeMo;
use App\Models\ClientMileageMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class OldEbuyClientCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		$gradePack=ClientGradeMo::GetClientGradeList();
		$renderData=['gradePack'=>$gradePack];
		View::renderTemplate('page/oldEbuyClient/oldEbuyClient.html',$renderData);
	}

	// //MileageDepositCon 솔팅별 리셋 데이터
    public function DataTableListLoad($data=null){
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $Sel = $oldDb->prepare("SELECT
            user_name,
            M_MILEAGE,
            member_srl,
            last_login,
            M_LEVEL,
            M_LEVELNAME
        FROM $oldDbName.eb_member
        WHERE last_login >= '20231101000000'
        AND M_LEVEL!=1");


        $Sel->execute();
        $result=$Sel->fetchAll (PDO::FETCH_ASSOC);

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $dataPack = [];
        foreach ($result as $key) {
            $member_srl=$key['member_srl'];
            $user_name=$key['user_name'];
            $last_login = date('Y-m-d H:i:s', strtotime($key['last_login']));
            $M_MILEAGE=$key['M_MILEAGE'];
            $M_LEVEL =$key['M_LEVEL'];
            $M_LEVELNAME =$key['M_LEVELNAME'];

            //20240220넘버포맷을 하면 비교가 안돼!
            // $M_MILEAGE=number_format($M_MILEAGE);

            $Sel2 = $db->prepare("SELECT
            AES_DECRYPT(TRIM(name),:dataDbKey) AS clientName,
            AES_DECRYPT(name,:dataDbKey) AS noTrimClientName,
            idx,
            gradeIDX,
            CONCAT(statusIDX ,'-' ,(SELECT memo FROM $dbName.Status WHERE idx=statusIDX)) AS statusIDX

            FROM $dbName.Client WHERE migrationIDX=:member_srl");
            $Sel2->bindValue(':dataDbKey', $dataDbKey);
            $Sel2->bindValue(':member_srl', $member_srl);
            $Sel2->execute();
            $resultA = $Sel2->fetch(PDO::FETCH_ASSOC);

            $diffGradeStat = 'Y';
            $statusIDX=0;
            $clientIDX=0;
            $clientName='구)'.$user_name;
            $getMileage=0;
            $diffMileage=0;
            $diffMileageStat = 'Y';
            $diffNameStat = 'Y';
            if(isset($resultA['idx'])){
                $clientIDX = $resultA['idx'];
                $clientName=$resultA['clientName'];
                $noTrimClientName=$resultA['noTrimClientName'];
                $gradeIDX=$resultA['gradeIDX'];
                $statusIDX=$resultA['statusIDX'];
                $getMileage=ClientMileageMo::getMileage($clientIDX);

                //20240220넘버포맷을 하면 비교가 안돼!
                // $getMileage=number_format($getMileage);
                if($getMileage==$M_MILEAGE){
                    $diffMileageStat = 'N';
                }
                $diffMileage=$M_MILEAGE*1-$getMileage*1;

                if(
                    ($M_LEVEL==2 && $gradeIDX==1)
                    || ($M_LEVEL==3 && $gradeIDX==2)
                    || ($M_LEVEL==6 && $gradeIDX==3)
                ){
                    $diffGradeStat='N';
                }else{
                    $diffGradeStat=$gradeIDX.'-'.$M_LEVEL;
                }

                if($clientName==$user_name){
                    $diffNameStat='N';
                }else{
                    $diffNameStat = $noTrimClientName.'-'.$clientName.'-'.$user_name;
                }
                $dataPack[]=[
                    'idx'=>$clientIDX
                    ,'oldIDX'=>$member_srl
                    ,'statusIDX'=>$statusIDX

                    ,'last_login'=>$last_login

                    ,'name'=>$clientName
                    ,'diffNameStat'=>$diffNameStat

                    ,'oldMileage'=>$M_MILEAGE
                    ,'newMileage'=>$getMileage

                    ,'diffMileageStat'=>$diffMileageStat
                    ,'diffMileage'=>$diffMileage

                    ,'M_LEVEL'=>$M_LEVEL
                    ,'M_LEVELNAME'=>$M_LEVELNAME
                    ,'diffGradeStat'=>$diffGradeStat
                ];

                //20240220 구바이가 더 적다면 구바이한테는 그냥 마일리지를 줄테다
                if(
                    $M_MILEAGE<$getMileage
                    && $diffMileage>-5
                ){
                    //20240220 구바이가 더 적다면 구바이한테는 그냥 마일리지를 줄테다
                    // $stat1=$oldDb->prepare("UPDATE $oldDbName.eb_member SET
                    //     M_MILEAGE=:M_MILEAGE
                    //     WHERE member_srl=:member_srl
                    // ");
                    // $stat1->bindValue(':M_MILEAGE', $getMileage);
                    // $stat1->bindValue(':member_srl', $member_srl);
                    // $stat1->execute();
                }
            }else{
                //구바이에 게스트가 아닌데 뉴바이에 없다고?..
                $dataPack[]=[
                    'idx'=>$clientIDX
                    ,'oldIDX'=>$member_srl
                    ,'statusIDX'=>$statusIDX

                    ,'last_login'=>$last_login

                    ,'name'=>$clientName
                    ,'diffNameStat'=>$diffNameStat

                    ,'oldMileage'=>$M_MILEAGE
                    ,'newMileage'=>$getMileage

                    ,'diffMileageStat'=>$diffMileageStat
                    ,'diffMileage'=>$diffMileage

                    ,'M_LEVEL'=>$M_LEVEL
                    ,'M_LEVELNAME'=>$M_LEVELNAME
                    ,'diffGradeStat'=>$diffGradeStat
                ];
            }
        }


        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	// //client.html 데이터테이블 리스트 로드
 //    public function DataTableListLoad()
 //    {
 //        $dataPack=ClientMo::GetDataTableListLoad();
 //        $resultData = ['data'=>$dataPack,'result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 유저 공통 디테일 로드
 //    public function ClientDetailFormLoad()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        static::ClientDetail($targetIDX);
 //    }

 //    //client.html 디테일 로드
 //    public function ClientRightDetailFormLoad()
 //    {

 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg,'n');
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $dataPack=ClientMo::GetClientDetailData($targetIDX);
 //        $bankPack=ClientMo::GetTargetClientData($targetIDX);
 //        $gradePack=ClientGradeMo::GetClientGradeList();
 //        $getMileage=ClientDetailMo::getMileage($targetIDX);
 //        $renderData=[
 //            'targetIDX'=>$targetIDX,
 //            'dataPack'=>$dataPack,
 //            'bankPack'=>$bankPack,
 //            'gradePack'=>$gradePack,
 //            'getMileage'=>$getMileage
 //        ];
 //        View::renderTemplate('page/client/clientDetail.html',$renderData);
 //    }

 //    //client.html 클라이언트 업데이트
 //    public function ClientUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
 //            $errMsg='gradeIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
 //            $errMsg='gradeName 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $gradeIDX=$_POST['gradeIDX'];
 //        $gradeName=$_POST['gradeName'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $issetGradeIDX=$issetStatusVal['gradeIDX'];
 //            $issetGradeName=$issetStatusVal['gradeName'];

 //            if($issetGradeIDX==$gradeIDX){
 //                $errMsg='이전 등급과 동일한 등급입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $step1=0;

 //        if($gradeName!=$issetGradeName){
 //            $step1=1;
 //            $ex='등급이 '.$issetGradeName.'에서 '.$gradeName.'로 변경됐습니다';
 //            $logIDX=$this->ClientLogInsert(225102,$targetIDX,0);
 //            $logEx=$this->ClientLogExInsert($logIDX,$issetGradeIDX,$gradeIDX,$ex);
 //        }

 //        if($step1==0){
 //            $errMsg='변경된 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("UPDATE $dbName.Client SET
 //            gradeIDX=:gradeIDX
 //            WHERE idx=:targetIDX
 //        ");
 //        $stat1->bindValue(':gradeIDX', $gradeIDX);
 //        $stat1->bindValue(':targetIDX', $targetIDX);
 //        $stat1->execute();



 //        //클라이언트로그
 //        // $this->ClientLogInsert(225102,$targetIDX,0);


 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html status 업데이트
 //    public function StatusUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
 //            $errMsg='statusVal 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $statusVal=$_POST['statusVal'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $statusIDX=$issetStatusVal['statusIDX'];
 //            if($statusIDX==226201){
 //                $errMsg='이미 탈퇴한 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //            if($statusIDX==226203){
 //                $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }


 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("UPDATE $dbName.Client SET
 //            statusIDX=:statusVal
 //            WHERE idx=:targetIDX
 //        ");
 //        $stat1->bindValue(':statusVal', $statusVal);
 //        $stat1->bindValue(':targetIDX', $targetIDX);
 //        $stat1->execute();

 //        //클라이언트로그
 //        $this->ClientLogInsert($statusVal,$targetIDX,0);

 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 해당 클라이언트 발란스 가져오기
 //    public function getBalance()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $getBalance=ClientDetailMo::getMileage($targetIDX);
 //        $resultData = ['result'=>'t','getBalance'=>$getBalance];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 해당 클라이언트 발란스 업데이트 (정말 필요할때 해야함)
 //    public function ClientBalanceUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
 //            $errMsg='statusIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['amount'])||empty($_POST['amount'])){
 //            $errMsg='amount 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['exVal'])||empty($_POST['exVal'])){
 //            $errMsg='exVal 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!is_numeric($_POST['amount'])) {
 //            $errMsg = '발란스는 숫자만 입력 가능합니다.';
 //            $errOn = $this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $statusIDX=$_POST['statusIDX'];
 //        $amount=$_POST['amount'];
 //        $exVal=$_POST['exVal'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $targetStatusIDX=$issetStatusVal['statusIDX'];
 //            if($targetStatusIDX==226201){
 //                $errMsg='이미 탈퇴한 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }

 //            if($targetStatusIDX==226202){
 //                $errMsg='스태프에 의해 차단된 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }

 //            if($targetStatusIDX==226203){
 //                $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $getBalance=ClientDetailMo::getMileage($targetIDX);

 //        if($statusIDX==205201){
 //            if($getBalance < $amount){
 //                $errMsg='차감 발란스가 현재 발란스보다 높습니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }

 //        $createTime=date("Y-m-d H:i:s");

 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
 //            (clientIDX,statusIDX,targetIDX,amount,createTime)
 //            VALUES
 //            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
 //        ");

 //        $stat1->bindValue(':clientIDX', $targetIDX);
 //        $stat1->bindValue(':statusIDX', $statusIDX);
 //        $stat1->bindValue(':targetIDX', 0);
 //        $stat1->bindValue(':amount', $amount);
 //        $stat1->bindValue(':createTime', $createTime);
 //        $stat1->execute();
 //        $insertIDX = $db->lastInsertId();

 //        $ex=$exVal.' ('.$amount.'원)';


 //        $logIDX=static::ClientLogInsert($statusIDX,$targetIDX,$targetIDX);
 //        static::ClientLogExInsert($logIDX,0,0,$ex);

 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

}