<?php

namespace App\Controllers;

use \Core\View;
use App\Models\ClientAuthLogMo;


class ClientAuthLogCon extends \Core\Controller
{
    public function Render($data=null)
    {
        View::renderTemplate('page/clientAuthLog/clientAuthLog.html');
    }
    //client.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate']) || empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate']) || empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];



        $paramArr = ['startDate'=>$startDate,'endDate'=>$endDate];
        $dataPack=ClientAuthLogMo::GetDataTableListLoad($paramArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

     public function ClientAuthLogDetailFormLoad()
    {
        if(!isset($_POST['targetIDX']) || empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX = $_POST['targetIDX'];
        $dataPack=ClientAuthLogMo::GetDetailLoad($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/clientAuthLog/clientAuthLogDetail.html',$renderData);


    }


}