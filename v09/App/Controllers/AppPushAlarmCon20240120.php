<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StatusMo;
use App\Models\ClientMo;
use App\Models\ClientAlarmMsgMo;
// use App\Models\ClientAlarmMsgAllMo;
use App\Models\ClientNoticeMo;

use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */


class AppPushAlarmCon extends \Core\Controller
{

    //파마미터 예제
    // public static function djTest($data=null)
    // {
    //     $clientIDX = 'all' ; //이거나 idx 하나의 값
    //     $statusIDX = 311101;
    //     $targetIDX = 1;
    //     $paramVal = ['amount'=>20];

    //     $pack = ['clientIDX'=>$clientIDX , 'statusIDX'=>$statusIDX , 'targetIDX'=>$targetIDX, 'param'=>$paramVal];
    //     self::goPushAlarm($pack);
    // }

    // public function Render($data=null)
    // {
    //     View::renderTemplate('page/appPushAlarm/appPushAlarm.html');
    // }//렌더


    // 클라이언트 푸시알람은 클라이언트 공지사항과 통합시킬 예정입니다. 밑에거 주석!!!
    // public function writePushAlarm($data=null)
    // {
    //     if(!isset($_POST['title'])){
    //         $errMsg='파라미터가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['con'])){
    //         $errMsg='파라미터가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $title = $_POST['title'];
    //     $con = $_POST['con'];
    //     if( ($con=='' || $con ==null ) && ($title=='' || $title ==null )){
    //         $errMsg='제목이나 내용 둘 중에 하나는 내용을 입력해주세요.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $title = trim($title);
    //     $con = trim($con);

    //     if(!isset($_POST['statusIDX']) || empty($_POST['statusIDX'])){
    //         $errMsg='statusIDX가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $statusIDX=$_POST['statusIDX'];

    //     //공지사항과 이벤트혜택만 등록할 수 있어요.
    //     $arrowStatus = [311101 , 312101];
    //     if (!in_array($statusIDX, $arrowStatus)) {
    //         $errMsg='statusIDX 값이 유효하지 않습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     //DB등록
    //     $dbName= self::MainDBName;
    //     $db = static::GetDB();
    //     $result = $db->prepare("INSERT INTO $dbName.ClientAlarmMsgAll (  statusIDX, title, con)
    //     VALUES (:statusIDX, :title,:con )");
    //     $result->bindValue(':statusIDX', $statusIDX);
    //     $result->bindValue(':title', $title);
    //     $result->bindValue(':con', $con);
    //     if (!$result->execute()) {
    //         $errMsg='시스템 오류.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX = $db->lastInsertId();
    //     static::StaffLogInsert($statusIDX,$targetIDX);
    //     //DB등록


    //     //알람을 쏘자
    //     $pack = ['clientIDX'=>'all' , 'statusIDX'=>$statusIDX , 'targetIDX'=>$targetIDX];
    //     self::goPushAlarm($pack);
    //     //알람을 쏘자
    // }




    ///아래부터는 진짜 푸시알람을 쏘는 함수들 입니다. 다른 클래스에서도 많이 참조될거에요.
    public static function goPushAlarm($data=null)
    {
        if(!isset($data['clientIDX']) || empty($data['clientIDX'])){
            $errMsg='[푸시알람]clientIDX가 없습니다.';
            $errOn=self::errExport($errMsg);
        }
        if(!isset($data['statusIDX']) || empty($data['statusIDX'])){
            $errMsg='[푸시알람]statusIDX 없습니다.';
            $errOn=self::errExport($errMsg);
        }
        if(!isset($data['targetIDX']) || empty($data['targetIDX'])){
            $errMsg='[푸시알람]targetIDX가 없습니다.';
            $errOn=self::errExport($errMsg);
        }

        $clientIDX = $data['clientIDX'];
        $statusIDX = $data['statusIDX'];
        $targetIDX = $data['targetIDX'];

        $statusName='';
        if (in_array($statusIDX, array(203201, 203202, 203203,203211,203212,203214,203215))) {
            $statusName = 'balaceRecharge';
        }else if(in_array($statusIDX, array(204201, 204202, 204211,204212))){
            $statusName = 'balanceWithdrawal';
        }else if(in_array($statusIDX, array(906101))){
            $statusName = 'deposit';
        }else if(in_array($statusIDX, array(313401,313402,313404))){
            $statusName = 'notice';
        }






        if($clientIDX=='all'){
            $getCon = ClientNoticeMo::getClientAlarmMsg($targetIDX);
            if(!isset($getCon['idx'])){
                $errMsg='[푸시알람]전체알람의 idx가 존재하지 않습니다.';
                $errOn=self::errExport($errMsg);
            }
            $con=$getCon['pushAlarmContent'];
            $title=$getCon['title'];
        }else{
            $paramVal = $data['param'];
            $getAlarmMsg = ClientAlarmMsgMo::getClientAlarmMsg($statusIDX);
            if(!isset($getAlarmMsg['idx'])){
                $errMsg='[푸시알람]기준 알람내용이 존재하지 않습니다.';
                $errOn=self::errExport($errMsg);
            }
            $con=$getAlarmMsg['con'];
            $title=$getAlarmMsg['title'];
            $param=$getAlarmMsg['param'];
            $paramArr = explode(",", $param);
            foreach ($paramArr as $key ) {
                if (isset($paramVal[$key])) {
                    $fullKey="{{".$key."}}";
                    $replaceKey = $paramVal[$key];
                    $con = str_replace($fullKey, $replaceKey, $con);
                }
            }
        }


        if( ($con=='' || $con ==null ) && ($title=='' || $title ==null )){
            //제목과 컨텐츠 둘다 비어있는 경우
            $errMsg='[푸시알람]제목과 컨텐츠 모두 비어있습니다.';
            $errOn=self::errExport($errMsg);
        }


        $ableSettingStatusList=[ 313401 , 313402 , 313404 ]; //유저가 세팅 가능한 상태값
        $getClientParam =['clientIDX'=>$clientIDX , 'statusIDX'=>$statusIDX];
        $getClientList=ClientMo::getClientForPushAlarm($getClientParam);
        if(count($getClientList)<1){
            $errMsg='[푸시알람]알람을 보낼 유저가 없습니다.';
            $errOn=self::errExport($errMsg);
        }

        $clientIDXPack=[];
        $iosClientPack=[];
        $andClientPack=[];
        foreach ($getClientList as $key ) {
            $thisClientIDX = $key['idx'];
            $deviceType=$key['deviceType'];
            $pushToken=$key['pushToken'];


            $ableStatus = 0 ; // 유저가 알람을 보내도록 허락했니?
            if (in_array($statusIDX, $ableSettingStatusList)) {
                //유저가 알람을 차단할 수 있는 상태임.
                $ableStatusIDX=$key['statusIDX'];
                if($ableStatusIDX>0){
                    //알람 안받을거야
                    $ableStatus = 1; //허락안함
                }
            }

            if($ableStatus==0){
                if($deviceType== 'A'){
                    $andClientPack[] = $pushToken;
                }else if($deviceType == 'I'){
                    $iosClientPack[] = $pushToken;
                }
            }

            $clientIDXPack[] = $thisClientIDX;
        }

        //아이폰
        if(isset($iosClientPack)&&!empty($iosClientPack)){
            // if(count($iosClientPack)<2){
            //     $iosClientPack = array($iosClientPack[0]);
            // }
            $data = ["statusName"=>$statusName,'targetIDX'=>$targetIDX];

            $fields = [
                'registration_ids' => $iosClientPack,
                "data" => $data,
                'priority' => 'high',
                'notification' => [
                    "title" => $title,
                    "body" => $con,
                    "sound" => "default"
                ],
            ];
            self::goCurl($fields);

        }
        //안드로이드
        if(isset($andClientPack)&&!empty($andClientPack)){
            // if(count($andClientPack)<2){
            //     $andClientPack = array($andClientPack[0]);
            // }
            //안드로이드에서 notification를 쓰면 앱이 활성화 안되어있을때 푸쉬를 못받아요 무조건 $data 안에 보내야합니다.
            // $notification = [
            //     "title" => $title,
            //     "body" => $con,
            // ];
            $data = [
                "title" => $title,
                "body" => $con,
                "statusName"=>$statusName,
                "targetIDX"=>$targetIDX,
            ];

            $dataChange=json_encode($data,JSON_UNESCAPED_UNICODE);

            $fields = [
                'registration_ids' => $andClientPack,
                // 'notification' =>$notification,
                'data'=>["msgPack"=>$dataChange],
                'priority' =>"high"
            ];
            self::goCurl($fields);
        }



        if($clientIDX == 'all'){ //데이터 낭비니까 reg를 쌓을 때는 con과 title 모두 빈값으로 넣는다.
            $con = '';
        }
         //데이터 쌓기
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $createTime=date('Y-m-d H:i:s');
        $query = "INSERT INTO $dbName.ClientAlarm (clientIDX, statusIDX, targetIDX, con, createTime) VALUES ";
        $bulkString = [];
        foreach ($clientIDXPack as $key) {
            $tClientIDX = $key;
            $bulkString[] = "(:clientIDX".$tClientIDX.", :statusIDX , :targetIDX, :con, :createTime)";
        }
        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);
        // 바인딩 변수 설정
        foreach ($clientIDXPack as $key) {
            $tClientIDX = $key;
            $paramName = ":clientIDX".$tClientIDX;
            $result->bindValue($paramName, $tClientIDX);
        }
        $result->bindValue(":statusIDX", $statusIDX);
        $result->bindValue(":targetIDX", $targetIDX);
        $result->bindValue(":con", $con);
        $result->bindValue(":createTime", $createTime);
        $result->execute();
    }

    protected static function goCurl($data=null)
    {
        $fields=$data;
        $apiKey = 'AAAAIBu1M-E:APA91bFbhZBEYz1zG1J4xd1MGX27Nb_6EJNp_oI4stO25FhrAQeLhwSxEQBrjMXsUXRDux6g-wp1TIJJppVvlQYLXirK32lbX7mkATIwpBPFbupG5V01fA11rtzaOZQbQoih9TiB__UW';

        $headers = [
            'Content-Type:application/json',
            'Authorization:key=' . $apiKey
        ];
         $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields,JSON_UNESCAPED_UNICODE));
        $response = curl_exec($ch);
        curl_close($ch);

    }















}