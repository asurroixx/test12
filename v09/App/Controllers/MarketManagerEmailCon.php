<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketManagerMo;
use App\Models\EmailMsgMo;
use PDO;

/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketManagerEmailCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$marketPack=MarketMo::GetMarketAllListData();
		$renderData=[
			'marketPack'=>$marketPack,
		];
		View::renderTemplate('page/email/email.html',$renderData);
	}

	//qna.html 해당 디테일
    public function EmailSend()
    {
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['managerPack'])||empty($_POST['managerPack'])){
            $errMsg='managerPack 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['allTargetingStat'])||empty($_POST['allTargetingStat'])){
            $errMsg='allTargetingStat 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $title=$_POST['title'];
        $content=$_POST['content'];
        $marketIDX=$_POST['marketIDX'];
        $managerPack=$_POST['managerPack'];
        $allTargetingStat=$_POST['allTargetingStat'];


        $emailParamVal = ['title'=>$title,'content'=>$content];

        if($allTargetingStat=="Y"){
            //이메일을 보내주자
            $emailParam=[
                'targetIDX'=>0,
                'statusIDX'=>406102, //전체
                'marketIDX'=>0, //모든 매니저에게 다 보내려면 0으로 하고 type all로
                'type'=>'all',
                'param'=>$emailParamVal,
            ];
        }else{
            $emailParam=[
                'targetIDX'=>0,
                'statusIDX'=>406101, //타겟
                'marketIDX'=>0, //모든 매니저에게 다 보내려면 0으로 하고 type all로
                'type'=>'target',
                'idxPack'=>$managerPack, //idx와 email을 다보내주세요. ex)[idx=>1, email=>dfdf@fdfd.com]
                'param'=>$emailParamVal,
            ];
        }




        $emailGo = StaffEmailCon::sendEmailToPortal($emailParam);
        //이메일을 보내주자
        if($emailGo===true){
            $resultData = ['result'=>'t'];
        }else{
            $resultData = ['result'=>'f'];
        }
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna.html 해당 디테일
    public function MarketManagerListLoad($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        if($targetIDX=='none'){
        	$errMsg='마켓 리스트를 선택해주세요.';
            $errOn=$this::errExport($errMsg);
        }

        $dataPack=MarketManagerMo::GetMarketManagerListLoad($targetIDX);

        $resultData = ['result'=>'t','dataPack'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}