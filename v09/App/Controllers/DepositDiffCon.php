<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\DepositOldMo;
use App\Models\ClientMarketListMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class DepositDiffCon extends \Core\Controller
{

    public function Render($data=null)
    {
        $dataPack=MarketMo::GetMarketAllListData();
        $renderData=[
            'dataPack'=>$dataPack
        ];
        View::renderTemplate('page/depositDiff/depositDiff.html',$renderData);
    }//렌더

    //marketBalanceHistory.html 데이터테이블
    public function DataTableListLoad($data=null)
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=DepositOldMo::GetDatatableList($dataArr);

        $namePack = ClientMarketListMo::GetClientNamePack();

        // 이름넣기
        $result = [];
        foreach ($dataPack as &$key) {
            $matchingCode    = $key['code'];
            $marketUserEmail = $key['refereceId'];
            $marketName            = '-';
            foreach ($namePack as $item) {
                    if ($item['code'] == $matchingCode && strtolower($item['referenceId']) == strtolower($marketUserEmail)) {
                    $marketName = $item['name'];
                }
            }
            $key['marketName']       = $marketName;

            $result[] = $key;
        }

        $resultData = [
            'result'=>'t',
            'data'=>$result
        ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //민잡으로 옮길거임

    public function testst($data=null)
    {

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        //구바이 서버
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $date = date('Y-m-d H:i:s');
        $timestamp = strtotime($date . ' +9 hours');
        $timestamp = $timestamp - (2 * 60); // 2분 빼기
        $kst = date('Y-m-d H:i:s', $timestamp);


        // 일단 구바이 출금대행 목록을 가져오자
        $query = $oldDb->prepare("SELECT
            APH_ID AS idx,
            APH_NAME AS name,
            APH_AGENT_M_CODE AS marketCode,
            APH_AGENT_USER_ID AS refereceId,
            APH_APPLY_QTY AS amount,
            partnerOrderId AS invoiceId,
            APH_SUC_P_ADM_REG AS createTime
            FROM $oldDbName.AA_AGENT_PARTNER_HIS
            WHERE APH_STAT = 2 AND (APH_SUC_P_ADM_REG BETWEEN '2024-01-22 10:00:00' AND '$kst')
        ");
        $query->execute();
        $resultTable=$query->fetchAll(PDO::FETCH_ASSOC);

        // 구바이 데이터를 돌려서 하나씩 꺼내옴
        foreach ($resultTable as $key) {
            $idx = $key['idx'];
            $name = $key['name'];
            $marketCode = $key['marketCode'];
            $refereceId = $key['refereceId'];
            $amount = $key['amount'];
            $invoiceId = $key['invoiceId'];
            $createTime = $key['createTime'];

            $targetData=DepositOldMo::GetTargetData($idx);
            //받아온 oldIDX가 있으면 인서트하고 없으면 하지마라
            if(!isset($targetData['oldIDX'])&&empty($targetData['oldIDX'])){
                //차라리 여기서 처음부터 인서트 시키자
                $stat2 = $db->prepare("INSERT INTO $dbName.DepositOld
                    (code, name, refereceId, amount, invoiceId, createTime, status, oldIDX)
                    VALUES
                    (:code, :name, :refereceId, :amount, :invoiceId, :createTime, :status, :oldIDX)
                ");
                $stat2->bindValue(':code', $marketCode);
                $stat2->bindValue(':name', $name);
                $stat2->bindValue(':refereceId', $refereceId);
                $stat2->bindValue(':amount', $amount);
                $stat2->bindValue(':invoiceId', $invoiceId);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':status', 1);
                $stat2->bindValue(':oldIDX', $idx);
                $stat2->execute();
            }

            //가져온값들로 조사해보자
            $paramArr=[
                'marketCode'=>$marketCode,
                'refereceId'=>$refereceId,
                'amount'=>$amount,
                'invoiceId'=>$invoiceId,
            ];

            // 4개의 파람값 중복 데이터가 있는지 조사 (뉴바이)
            $newData=DepositOldMo::GetIssetNewDatatableList($paramArr);

            if(isset($newData['idx'])&&!empty($newData['idx'])){
                $newDataMarketCode = $newData['marketCode'];
                $newDataMarketUserEmail = $newData['marketUserEmail'];
                $newDataAmount = $newData['amount'];
                $newDataInvoiceID = $newData['invoiceID'];

                if ($newDataMarketCode == $marketCode &&
                    $newDataMarketUserEmail == $refereceId &&
                    $newDataAmount == $amount &&
                    $newDataInvoiceID == $invoiceId) {

                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET
                        status=3
                        WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->execute();
                }

                if($newDataMarketCode != $marketCode){
                    $memo = 'Job : 마켓코드가 맞지 않습니다. 뉴바이코드 : '.$newDataMarketCode .' / 구바이코드 : '.$marketCode;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataMarketUserEmail != $refereceId){
                    $memo = 'Job : 참조이메일이 맞지 않습니다. 뉴바이참ID : '.$newDataMarketUserEmail .' / 구바이참ID : '.$refereceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataAmount != $amount){
                    $memo = 'Job : 금액이 맞지 않습니다. 뉴바이금액 : '.$newDataAmount .' / 구바이금액 : '.$amount;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataInvoiceID != $invoiceId){
                    $memo = 'Job : InvoiceID가 맞지 않습니다. 뉴바이인ID : '.$newDataInvoiceID .' / 구바이인ID : '.$invoiceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }
            }

        }

        $resultData = [
            'result'=>'t',
        ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

    }

    public function StatusUpdate()
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $idx=$_POST['idx'];
        $memo=$_POST['memo'];

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $stat1=$db->prepare("UPDATE $dbName.DepositOld SET
            memo='$memo'
            WHERE idx='$idx'
        ");
        $stat1->execute();
        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}