<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use \Core\Controller;
use \Core\DefinAll;
use App\Models\PortalAlarmMsgMo;
use App\Models\MarketManagerMo;
use App\Models\StaffMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PortalAlarmCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public static function sendAlarmToPortal($data=null)
    {
        $statusIDX = $data['statusIDX'];
        $targetIDX = $data['targetIDX'];
        $marketIDX = $data['marketIDX'];
        $paramVal = $data["param"];


        $getAlarmMsg = PortalAlarmMsgMo::getPortalAlarmMsg($statusIDX);
        if(!isset($getAlarmMsg['idx'])){
            $errMsg='기준 알람내용이 존재하지 않습니다.';
            // $errOn=self::errExport($errMsg);
            return false;
        }

        $title = $getAlarmMsg['title'];
        $content = $getAlarmMsg['con'];
        $param = $getAlarmMsg['param'];
        $paramArr = explode(",", $param);

        foreach ($paramArr as $key ) {
            if (isset($paramVal[$key])) {
                $fullKey="{{".$key."}}";
                $replaceKey = $paramVal[$key];
                $content = str_replace($fullKey, $replaceKey, $content);
            }
        }


        if($statusIDX=='340101'||$statusIDX=='340201'){
            //Qna관련
            $menuIDX=89;
        }elseif($statusIDX=='342401'||$statusIDX=='342402'){
            //공지사항관련
            $menuIDX=91;
        }elseif($statusIDX=='417201'||$statusIDX=='417211'){
            //정산관련
            $menuIDX=82;
        }elseif($statusIDX=='418201'||$statusIDX=='418211'){
            //역정산관련
            $menuIDX=86;
        }elseif($statusIDX=='906101'){
            //Deposit 관련
            $menuIDX=81;
        }elseif($statusIDX=='908101'){
            //Withdrawal 관련
            $menuIDX=80;
        }elseif($statusIDX=='351101'||$statusIDX=='351102'||$statusIDX=='351201'){
            //White Ip
            $menuIDX=84;
        }else{
            $menuIDX=0;
        }

        //staffalarm 테이블이 쌓기
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $paramArr =['marketIDX'=>$marketIDX, 'statusIDX'=>$statusIDX];
        $getMarketManagerList=MarketManagerMo::getMarketManagerListForAlarm($paramArr);
        $createTime=date('Y-m-d H:i:s');
        $query = "INSERT INTO $dbName.PortalAlarm (marketManagerIDX, statusIDX, targetIDX, con, createTime,viewStatusIDX,menuIDX) VALUES ";
        $bulkString = [];
        $marketManagerIDXPack = [];
        foreach ($getMarketManagerList as $key) {
            $marketManagerIDX = $key['idx'];
            $alarmSettingIDX = $key['alarmSettingIDX'];
            if($alarmSettingIDX==0 || $alarmSettingIDX==null || $alarmSettingIDX==""){
                //알람받을거야
                $marketManagerIDXPack[] = $key['idx'];
            }
            $bulkString[] = "(:marketManagerIDX".$marketManagerIDX.", :statusIDX , :targetIDX, :con, :createTime , :viewStatusIDX".$marketManagerIDX.", :menuIDX)";
        }
        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);
        // 바인딩 변수 설정
        foreach ($getMarketManagerList as $key) {
            $marketManagerIDX = $key['idx'];
            $alarmSettingIDX = $key['alarmSettingIDX'];
            if($alarmSettingIDX>0){
                $viewStatusIDX=405101;
            }else{
                $viewStatusIDX=405201;
            }
            $paramName = ":marketManagerIDX".$marketManagerIDX;
            $result->bindValue($paramName, $marketManagerIDX);
            $paramName = ":viewStatusIDX".$marketManagerIDX;
            $result->bindValue($paramName, $viewStatusIDX);
        }
        $result->bindValue(":statusIDX", $statusIDX);
        $result->bindValue(":targetIDX", $targetIDX);
        $result->bindValue(":con", $content);
        $result->bindValue(":createTime", $createTime);
        $result->bindValue(":menuIDX", $menuIDX);
        $result->execute();
        //staffalarm 테이블이 쌓기


        //staff에 socket통신 하기
        $PortalIoUri=self::PortalIoUri;
        $dataPack = ['marketManagerIDXPack'=>$marketManagerIDXPack, 'menuIDX'=>$menuIDX, 'con'=>$content,'marketIDX'=>$marketIDX,'statusIDX'=>$statusIDX,'targetIDX'=>$targetIDX];
        $uri = $PortalIoUri.'/portalAlarm';
        self::sendCurl($dataPack,$uri);
        //staff에 socket통신 하기




    }



}