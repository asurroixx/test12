<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketTopupMo;
use App\Models\MarketSettlementCryptoWalletMo;
use App\Models\MarketMo;
use App\Models\PortalLogMo;
use App\Models\EbuyCryptoWalletMo;



use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketTopupRequestCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/marketTopupRequest/marketTopupRequest.html');
	}

	//데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=MarketTopupMo::GetMarketTopupData($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['changeStatus'])||empty($_POST['changeStatus'])){
            $errMsg='changeStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $value='';
        if(isset($_POST['value'])||!empty($_POST['value'])){
        	$value=$_POST['value'];
        }
        $targetIDX=$_POST['targetIDX'];
        $marketIDX=$_POST['marketIDX'];
        $changeStatus=$_POST['changeStatus'];
        $db = static::getDB();
        $dbName= self::MainDBName;
        $nowTime=date("Y-m-d H:i:s");

        $possibleMarket=MarketMo::MarketStatusData($marketIDX);
        if(!isset($possibleMarket['idx'])||empty($possibleMarket['idx'])){
            $errMsg='존재하지않은 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketStatusIDX=$possibleMarket['statusIDX'];
        if($marketStatusIDX==412201){
            $errMsg='비활성된 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($marketStatusIDX==412202){
            $errMsg='계약만료된 마켓입니다.';
            $errOn=$this::errExport($errMsg);
        }
        $duplicationInspection=MarketTopupMo::GetDetail($targetIDX);
         if(!isset($duplicationInspection['idx'])||empty($duplicationInspection['idx'])){
            $errMsg='존재하지않은 신청건입니다.';
            $errOn=$this::errExport($errMsg);
        }
        $beforeStatus=$duplicationInspection['statusIDX'];
        $getAmount=$duplicationInspection['nonFeeAmount'];
        $getCreateTime=$duplicationInspection['createTime'];
        $marketName=$duplicationInspection['marketName'];
        $method=$duplicationInspection['method'];
        $managerEmail=$duplicationInspection['managerEmail'];
        $ebuyWalletCoinCode='';
        $ebuyWalletMainnetCode='';
        $ebuyWalletAddr='';
        if($method=='Crypto'){
            $ebuyWalletIDX=$duplicationInspection['methodIDX'];
            $ebuyWallet=EbuyCryptoWalletMo::getEbuyTopupCrytoWalletForEmail($ebuyWalletIDX);
            if(!isset($ebuyWallet['idx']) || empty($ebuyWallet['idx'])){
                $errMsg='Unauthorized Access Detected.';
                $errOn=static::errExport($errMsg);
            }
            $ebuyWalletCoinCode=$ebuyWallet['coinCode'];
            $ebuyWalletMainnetCode=$ebuyWallet['mainnetCode'];
            $ebuyWalletAddr=$ebuyWallet['walletAddr'];
        }




        switch ($changeStatus) {
            case '418301'://확인
                if( $beforeStatus != 418101 && $beforeStatus != 418102 && $beforeStatus != 418103){
                    // $errMsg='이미 처리된 신청건입니다.';
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
                $update =$db->prepare("UPDATE $dbName.MarketTopup SET
                    statusIDX = :changeStatus
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();
                $emailParamVal = [
                    'marketname'=>$marketName,
                    'manageremail'=>$managerEmail,
                    'amount'=>$getAmount,
                    'ebuycoincode'=>$ebuyWalletCoinCode,
                    'ebuymainnetcode'=>$ebuyWalletMainnetCode,
                    'ebuyaddress'=>$ebuyWalletAddr
                ];
            break;
            case '418201'://완료
                if($beforeStatus != 418301){
                    // $errMsg='이미 처리된 신청건입니다.';
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
            	$amount=$value;
                $update =$db->prepare("UPDATE $dbName.MarketTopup SET
                    statusIDX = :changeStatus,
                    completeTime = :nowTime
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                $update->bindValue(':nowTime', $nowTime);
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();

                //마일리지테이블에 쌓기
                $insert=$db->prepare("INSERT INTO $dbName.MarketBalance
                    (
                        marketIDX,
                        statusIDX,
                        targetIDX,
                        amount,
                        createTime
                    )
                    VALUES
                    (
                        :marketIDX,
                        :changeStatus,
                        :targetIDX,
                        :amount,
                        :nowTime
                    )
                ");
                $insert->bindValue(':marketIDX', $marketIDX);
                $insert->bindValue(':changeStatus', $changeStatus);
                $insert->bindValue(':targetIDX', $targetIDX);
                $insert->bindValue(':amount', $amount);
                $insert->bindValue(':nowTime', $nowTime);
                $insert->execute();
                $emailParamVal = [
                    'marketname'=>$marketName,
                    'manageremail'=>$managerEmail,
                    'amount'=>$getAmount
                ];
            break;
            case '418211'://취소
                if($beforeStatus != 418101 && $beforeStatus != 418102 && $beforeStatus != 418103 && $beforeStatus != 418301){
                    $errMsg='오류가 발생하였습니다. 새로고침 후 다시 이용해주세요.('.$beforeStatus.')';
                    $errOn=$this::errExport($errMsg);
                }
            	$update =$db->prepare("UPDATE $dbName.MarketTopup SET
                    statusIDX = :changeStatus,
                    completeTime = :nowTime
                    WHERE idx = :targetIDX
                ");
                $update->bindValue(':changeStatus', $changeStatus);
                $update->bindValue(':nowTime', $nowTime);
                $update->bindValue(':targetIDX', $targetIDX);
                $update->execute();
                $emailParamVal = [
                    'marketname'=>$marketName,
                    'amount'=>$getAmount
                ];
            break;
            default:
        }
        //이메일을 보내주자

        $emailParam=[
            'targetIDX'=>$targetIDX,
            'statusIDX'=>$changeStatus,
            'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
            'param'=>$emailParamVal
        ];
        StaffEmailCon::sendEmailToPortal($emailParam);

        $type = 'web';
        $staffIDX = '';
        if(isset($_POST['type'])||!empty($_POST['type'])){
            $type = $_POST['type'];
            $staffIDX = $_POST['staffIDX'];
        }

        $createTime=date("Y-m-d H:i:s");
        $ipAddress = $this->GetIPaddress();

        if($type == 'app'){
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db->prepare("INSERT INTO $dbName.PortalLog
                (statusIDX,targetIDX,staffIDX,createTime,ip)
                VALUES
                (:changeStatus,:targetIDX,:staffIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':changeStatus', $changeStatus);
            $stat2->bindValue(':targetIDX', $targetIDX);
            $stat2->bindValue(':staffIDX', $staffIDX);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', $ipAddress);
            $stat2->execute();
        }else{
            static::PortalLogInsert($changeStatus,$targetIDX);
        }

        // static::PortalLogInsert($changeStatus,$targetIDX);

        $resultData = ['result'=>'t'];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);

        //20231108 dj 소켓알람
        if($changeStatus=='418201' || $changeStatus=='418211'){
            //완료와 취소는 알람을 쏴줍니다.
            $alarmParam=[
                'targetIDX'=>$targetIDX,
                'statusIDX'=>$changeStatus,
                'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
                'param'=>[]
            ];
            PortalAlarmCon::sendAlarmToPortal($alarmParam);
        }
    }

    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketIDX=$_POST['marketIDX'];
        $dataPack=MarketTopupMo::GetDetail($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='디테일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketPack=MarketMo::GetMarketDetailData($marketIDX);
        if(!isset($marketPack['idx'])||empty($marketPack['idx'])){
            $errMsg='마켓 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $requestStatusIDX=$dataPack['requestStatusIDX'];
        $statusIDX=$dataPack['statusIDX'];
        //완료or취소 처리한 스태프정보가져오기
        $completePack=[];
        if($statusIDX=='418201'||$statusIDX=='418211'){
            $logArr=['statusIDX'=>$statusIDX,'targetIDX'=>$targetIDX];
            $completePack=PortalLogMo::GetCompleteStaffInfoData($logArr);
            if(!isset($completePack['idx'])||empty($completePack['idx'])){
                $errMsg='완료 or 스태프취소가 됐는데 로그가없다고? 이상해'.$targetIDX;
                $errOn=$this::errExport($errMsg);
            }
        }
        $methodIDX=$dataPack['methodIDX'];
        $requestTypePack=[];

        switch ($requestStatusIDX) {
            case '418101'://크립토
                $requestTypePack=MarketSettlementCryptoWalletMo::GetMarketSettlementInfo($methodIDX);
			    if(!isset($requestTypePack['idx'])||empty($requestTypePack['idx'])){
		            $errMsg='요청한 정산방법에 대한 정보가 없습니다.';
		            $errOn=$this::errExport($errMsg);
		        }
            break;
            case '418102'://뱅크와이어
            break;
            case '418103'://원화
            break;
            default:
        }
        
        $renderData=[
            'targetIDX'=>$targetIDX,
            'marketIDX'=>$marketIDX,
            'dataPack'=>$dataPack,
            'marketPack'=>$marketPack,
            'requestTypePack'=>$requestTypePack,
            'completePack'=>$completePack,
        ];
        View::renderTemplate('page/marketTopupRequest/marketTopupRequestDetail.html',$renderData);
    }


}