<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\PortalLogMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PortalLogCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		$dataPack=MarketMo::GetMarketAllListData();
		$renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/portalLog/portalLog.html',$renderData);
	}

	//portalLog.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $dateArr=[
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        ];
        $dataPack=PortalLogMo::GetDataTableListLoad($dateArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //portalLog.html 오른쪽 디테일 로드
    public function PortalLogRightDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=PortalLogMo::GetClientDetailData($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/portalLog/portalLogDetail.html',$renderData);
    }

}