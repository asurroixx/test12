<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\SystemMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class SettingCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$dataPack=SystemMo::GetSystemData();
		$renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/setting/setting.html',$renderData);
	}//렌더

    public function RenderTest($data=null)
    {
        $dataPack=SystemMo::GetSystemData();
        $renderData=['dataPack'=>$dataPack];
        View::renderTemplate('page/setting/settingTest.html',$renderData);
    }//렌더

	//setting.html status 업데이트
    public function SystemStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['targetStatus'])||empty($_POST['targetStatus'])){
            $errMsg='targetStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $targetStatus=$_POST['targetStatus'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.System SET
            status='$targetStatus'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();
        $this->SystemLogInsert($targetIDX,$targetStatus);

        $resultData = ['result'=>'t','targetStatus'=>$targetStatus];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //setting.html value 업데이트
    public function SystemValueUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $value=$_POST['value'];

        if($targetIDX == '11'){
            if (is_numeric($value)) {
                // 소수점 두 번째 자리까지의 숫자인지 확인
                if (preg_match('/^-?\d+\.\d{1}$/', $value)) {
                    // $value는 소수점이 있으며 소수점 두 번째 자리까지의 숫자
                } else {
                    $errMsg='소수점 첫번째 자리까지만 입력이 가능합니다.';
                    $errOn=$this::errExport($errMsg,'n');
                }
            } else {
                $errMsg='문자는 입력하실 수 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.System SET
            value='$value'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $this->SystemLogInsert($targetIDX,$value);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}