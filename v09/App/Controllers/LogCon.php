<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StatusMo;
use App\Models\MarketMo;
use App\Models\MarketSettlementMo;
use App\Models\MarketSettlementBankwireMo;
use App\Models\MarketSettlementCryptoWalletMo;
use App\Models\MarketSettlementKRWMo;
use App\Models\StaffMemoMo;
use App\Models\ClientLogMo;
use App\Models\PortalLogMo;
use App\Models\StaffLogMo;
use App\Models\MarketWithdrawalLogMo;
use App\Models\MarketDepositLogMo;


use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class LogCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */



    public function LogListLoad()
    {
        $targetIDX=0;
        if(isset($_POST['targetIDX'])||!empty($_POST['targetIDX'])){
            $targetIDX=$_POST['targetIDX'];
        }
        if(!isset($_POST['logType'])||empty($_POST['logType'])){
            $errMsg='logType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $logType=$_POST['logType'];
        $data=[];
        $target='';

        switch ($logType) {
            case 'settlement'://카테고리
                $target='portal';
                $whereType ='like';
                $whereArr=['417'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'reverseSettlement'://카테고리
                $target='portal';
                $whereType ='like';
                $whereArr=['418'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'partner'://업체
                $target='portal';
                $whereType ='between';
                $whereArr=['start'=>415,'end'=>416];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'market'://마켓
                $target='portal';
                $whereType ='and';
                $whereArr=['411101','411201','412101','412201','412202','413101','413201','414101','414201','419101','419102','419201','420101','420201','420301'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'marketManager'://마켓매니저
                $target='portal';
                $whereType ='between';
                $whereArr=['start'=>401,'end'=>402];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'marketWhiteIp'://마켓매니저
                $target='portal';
                $whereType ='and';
                $whereArr=['428101','428102','428201','351101','351102','351201'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;
            case 'marketSettlement'://정산지갑관리
                $target='portal';
                $whereType ='and';
                $whereArr=['425101','425301'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=PortalLogMo::GetLogList($arr);
            break;

            case 'marketManagerPermissionHis'://매니저 등급및 권한 -> 권한 수정 히스토리
                $target='staff';
                $whereType ='and';
                $whereArr=['327301'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            // case 'reverseSettlement':
            //     $whereType ='and';
            //     $whereArr=['418101','418201','418211','418212','418301'];
            //     $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
            //     $data=PortalLogMo::GetLogList($arr);
            // break;
            // case 'client'://클라이언트
            //     $target='client';
            //     $whereType ='and';
            //     $whereArr=['220101','220102','220103','225101','225102','226101','226201','226202','226203','227101','228101','205101','205201','221101','229101','229201','224101'];
            //     $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
            //     $data=ClientLogMo::GetLogList($arr);
            // break;


            //2024 02 05 : 고객에 대한 메모는 무조건 고객 중심
            case 'client'://클라이언트
                $target='client';
                $data=ClientLogMo::GetClientLogList($targetIDX);
            break;
            case 'clientMileageDeposit'://클라이언트 입금신청내역
                $target='client';
                $whereType ='like';
                $whereArr=['203'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=ClientLogMo::GetLogList($arr);
            break;
            case 'clientMileageWithdrawal'://클라이언트 출금신청내역
                $target='client';
                $whereType ='like';
                $whereArr=['204'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=ClientLogMo::GetLogList($arr);
            break;
            case 'ptp'://피투피 20240116 jm
                $target='client';
                $whereType ='and';
                $whereArr=['250101','250201','250211','251101','251102','251201','251301','251401','251402','251403'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=ClientLogMo::GetLogList($arr);
            break;
            case 'clientMarketList'://클라이언트 마켓 리스트
                $target='client';
                $whereType ='and';
                $whereArr=['240101','240201'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=ClientLogMo::GetLogList($arr);
            break;
            case 'cashReceipt'://20240213
                $target     = 'client';
                $whereType  = 'between';
                $whereArr   = ['start'=>811,'end'=>812,];
                $arr=         ['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=ClientLogMo::GetLogList($arr);
            break;





            case 'staff'://스태프
                $target='staff';
                $whereType ='between';
                $whereArr=['start'=>301,'end'=>303,];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'staffGrade'://스태프등급
                $target='staff';
                $whereType ='and';
                $whereArr=['322101','322201'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'clientGrade'://클라이언트등급
                $target='staff';
                $whereType ='and';
                $whereArr=['325101','325201','325301','325401'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'marketManagerGrade'://마켓매니저등급
                $target='staff';
                $whereType ='and';
                $whereArr=['327101','327201'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'ebuyBank'://입출금 계좌관리
                $target='staff';
                $whereType ='and';
                $whereArr=['323101','323201','323301','323401'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'notice'://공지사항
                $target='staff';
                $whereType ='and';
                $whereArr=['342101','342201','342301','342401','342402','342403','342501','342502','342601','342602'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'faq'://faq
                $target='staff';
                $whereType ='and';
                $whereArr=['343101','343201','343301','343401','343501','346601'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'schedule'://캘린더
                $target='staff';
                $whereType ='and';
                $whereArr=['390201'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'clientNotice'://앱 공지사항
                $target='staff';
                $whereType ='and';
                $whereArr=['313101','313201','313301','313401','313402','313403','313501','313502','313601','313602'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;
            case 'clientFaq'://faq
                $target='staff';
                $whereType ='and';
                $whereArr=['314101','314201','314301','314401','314501','347601'];
                $arr=['targetIDX'=>$targetIDX,'whereType'=>$whereType,'whereArr'=>$whereArr];
                $data=StaffLogMo::GetLogList($arr);
            break;






            case 'clientApiWithdrawal'://클라이언트 api 출금
                $marketCode = $_POST['marketCode'] ?? '';
                if(!$marketCode)$this::errExport('marketCode 정보가 없습니다.');

                $target     = 'api';
                $whereType  = 'between';
                $whereArr   = ['start'=>908,'end'=>909,];
                $arr        = ['invoiceID'=>$targetIDX, 'marketCode' => $marketCode, 'whereType'=>$whereType, 'whereArr'=>$whereArr];
                $data       = MarketWithdrawalLogMo::GetLogList($arr);
            break;
            case 'clientApiDeposit'://클라이언트 api 입금
                $marketCode = $_POST['marketCode'] ?? '';
                if(!$marketCode)$this::errExport('marketCode 정보가 없습니다.');

                $target     = 'api';
                $whereType  = 'between';
                $whereArr   = ['start'=>904,'end'=>906,];
                $arr        = ['invoiceID'=>$targetIDX, 'marketCode' => $marketCode, 'whereType'=>$whereType, 'whereArr'=>$whereArr];
                $data       = MarketDepositLogMo::GetLogList($arr);
            break;








            default:
        }


        $resultData = ['result'=>'t','target'=>$target,'data'=>$data];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }


}