<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientMo;





/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientMarketListCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=MarketMo::GetMarketAllListData();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/clientMarketList/clientMarketList.html',$renderData);
	}

    //데이터테이블
    public function dataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=ClientMarketListMo::GetDataTableData($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //오른쪽 디테일
    public function ClientMarketListDetailFormLoad()
    {
        $pageType='upd';
        if(isset($_POST['pageType'])&&!empty($_POST['pageType'])){
            $pageType = $_POST['pageType'];
        }
        if($pageType == 'ins'){
            $dataPack = MarketMo::GetMarketAllListData();
            $targetIDX = '';
            $emailPack = '';
        }else{
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg);
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=ClientMarketListMo::GetDetailData($targetIDX);

            $marketIDX = $dataPack['marketIDX'];
            $referenceId = $dataPack['referenceId'];
            $paramArr=[
                'marketIDX'=>$marketIDX,
                'referenceId'=>$referenceId,
            ];
            //이 이메일이 어느 클라이언트들이 썼는지에 대한 히스토리 = 이건 targetIDX로는 알 수 없고, 마켓IDX와 참조ID로 가능함
            $emailPack=ClientMarketListMo::GetClientRefereceIDCheck($paramArr);
        }


        $renderData=[
            'pageType'=>$pageType,
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'emailPack'=>$emailPack,
        ];
        View::renderTemplate('page/clientMarketList/clientMarketListDetail.html',$renderData);
    }

    //공통 디테일
    public function ClientDetailLoad($data=null){
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $clientIDX=$_POST['clientIDX'];
        static::ClientDetail($clientIDX);
    }

    //비활성화 업데이트
    public function ClientMarketListUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMarketListMo::GetDetailData($targetIDX);
        if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
            $targetStatusIDX = $dataPack['statusIDX'];
            $targetClientIDX = $dataPack['clientIDX'];
            if($targetStatusIDX == 240201){
                $errMsg='이미 비활성된 이메일 입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='타겟 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $statusIDX=240201;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientMarketList SET
            statusIDX=:statusIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $logIDX=static::ClientLogInsert($statusIDX,$targetIDX,$targetClientIDX);

        $marketName = $dataPack['marketName'];
        $marketCode = $dataPack['marketCode'];
        $referenceId = $dataPack['referenceId'];
        $loginEmail=GlobalsVariable::GetGlobals('loginEmail');
        $loginName=GlobalsVariable::GetGlobals('loginName');

        $ex='스태프가 참조ID 삭제 이메일 : '.$referenceId.' | 마켓 : '.$marketName.' (코드:'.$marketCode.') || 처리자 : '.$loginName.' ('.$loginEmail.')';
        static::ClientLogExInsert($logIDX,0,0,$ex);









        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //클라이언트 마켓 리스트 인서트
    public function ClientMarketListInsert()
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['referenceId'])||empty($_POST['referenceId'])){
            $errMsg='referenceId 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientName'])||empty($_POST['clientName'])){
            $errMsg='clientName 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientPhone'])||empty($_POST['clientPhone'])){
            $errMsg='clientPhone 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $marketIDX=$_POST['marketIDX'];$marketIDX=trim($marketIDX);
        $referenceId=$_POST['referenceId'];$referenceId=trim($referenceId);
        $clientName=$_POST['clientName'];$clientName=trim($clientName);
        $clientPhone=$_POST['clientPhone'];$clientPhone=trim($clientPhone);

        $marketData=MarketMo::GetMarketIDXData($marketIDX);
        if(isset($marketData['idx'])&&!empty($marketData['idx'])){
            $marketCode = $marketData['code'];
            $marketName = $marketData['name'];
        }else{
            $errMsg='마켓 정보가 없습니다.';
            $errOn=$this->errExport($errMsg);
        }

        $mailCheck = filter_var($referenceId, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
            $errMsg='이메일 형식이 아닙니다.';
            $errOn=$this->errExport($errMsg);
        }

        $emailArr=[
            'marketCode'=>$marketCode,
            'marketUserEmail'=>$referenceId,
        ];
        //활성화된 이메일중 중복된거 있는지 조사
        $issetEmailData=ClientMarketListMo::GetClientIDXData($emailArr);
        if(isset($issetEmailData['idx'])&&!empty($issetEmailData['idx'])){
            $errMsg='이미 등록된 이메일이 존재합니다.';
            $errOn=$this->errExport($errMsg);
        }

        $clientArr=[
            'name'=>$clientName,
            'phone'=>$clientPhone,
        ];
        //이름+번호로 clientIDX 조사
        $issetEmailData=ClientMo::GetIssetClientIDXData($clientArr);
        if(isset($issetEmailData['idx'])&&!empty($issetEmailData['idx'])){
            $clientIDX = $issetEmailData['idx'];
            $clientStatusIDX = $issetEmailData['statusIDX'];
            if($clientStatusIDX == 226201){
                $errMsg='이미 탈퇴한 회원입니다.';
                $errOn=$this->errExport($errMsg);
            }
            if($clientStatusIDX == 226202){
                $errMsg='스태프가 차단한 회원입니다.';
                $errOn=$this->errExport($errMsg);
            }
            if($clientStatusIDX == 226203){
                $errMsg='앱 미가입한 회원입니다.';
                $errOn=$this->errExport($errMsg);
            }
        }else{
            $errMsg='해당 클라이언트가 존재하지 않습니다.';
            $errOn=$this->errExport($errMsg);
        }

        //유효성 통과

        $ipAddress=$this->GetIPaddress();
        $createTime = date("Y-m-d H:i:s");
        $statusIDX = 240101;
        $loginName=GlobalsVariable::GetGlobals('loginName');
        $loginEmail=GlobalsVariable::GetGlobals('loginEmail');

        $db = static::getDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $stat1=$db->prepare("INSERT INTO $dbName.ClientMarketList
            (marketIDX,clientIDX,referenceId,statusIDX,ip,createTime)
            VALUES
            (:marketIDX,:clientIDX,AES_ENCRYPT(:referenceId, '$dataDbKey'),:statusIDX,:ip,:createTime)
        ");

        $stat1->bindValue(':marketIDX', $marketIDX);
        $stat1->bindValue(':clientIDX', $clientIDX);
        $stat1->bindValue(':referenceId', $referenceId);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':ip', $ipAddress);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $insertIDX = $db->lastInsertId();
        $logIDX=static::ClientLogInsert($statusIDX,$insertIDX,0);
        $ex='스태프가 참조ID 추가 이메일 : '.$referenceId.' | 마켓 : '.$marketName.' (코드:'.$marketCode.') || 처리자 : '.$loginName.' ('.$loginEmail.')';
        static::ClientLogExInsert($logIDX,0,0,$ex);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    

}