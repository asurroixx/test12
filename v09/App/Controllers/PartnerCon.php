<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\BankMo;
use App\Models\PartnerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PartnerCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=PartnerMo::GetPartnerListData();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/partner/partner.html',$renderData);
	}//렌더

	//partnerDetail.html 디테일 로드
    public function PartnerDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=PartnerMo::GetPartnerDetail($targetIDX);
        }else if($pageType=='ins'){

            $targetIDX='';
            $dataPack='';
        }
        $renderData=[
            'targetIDX'=>$targetIDX,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/partner/partnerDetail.html',$renderData);

    }

    //partner.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=PartnerMo::GetDatatableList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //partner.html 업체 추가하기
    public function PartnerInsert()
    {
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $name=$_POST['name'];
        $createTime=date("Y-m-d H:i:s");

        function GenerateString($length)  {
            $characters= "abcdefghijklmnopqrstuvwxyz1234567890";
            $string_generated = "";
            $nmr_loops = $length;
            while ($nmr_loops--){
                $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
            }
            $issetString=PartnerMo::GetPartnerCode($string_generated);
            if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
                return GenerateString($length);
            }else{
                return $string_generated;
            }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
        }
        $partnerNewCode=GenerateString(7);
        //20240223 _SANDBOX 빼기
        // $sandboxPartnerNewCode=$partnerNewCode.'_SANDBOX';
        $sandboxPartnerNewCode=$partnerNewCode;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Partner
            (code,name,createTime)
            VALUES
            (:code,:name,:createTime)
        ");

        $stat1->bindValue(':code', $partnerNewCode);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $targetIDX = $db->lastInsertId();
        //포탈로그
        $logIDX=$this->PortalLogInsert(415101,$targetIDX);

        /*---------------------------------샌드박스----------------------------*/
        $sandboxDb = static::GetSandboxDB();
        $sandboxDBName= self::EbuySandboxDBName;

        $stat0=$sandboxDb->prepare("INSERT INTO $sandboxDBName.Partner
            (code,name,createTime)
            VALUES
            (:code,:name,:createTime)
        ");

        $stat0->bindValue(':code', $sandboxPartnerNewCode);
        $stat0->bindValue(':name', $name);
        $stat0->bindValue(':createTime', $createTime);
        $stat0->execute();

        //샌드박스 포탈로그
        $logIDX=$this->SandboxPortalLogInsert(415101,$targetIDX);



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //partner.html 업체 업데이트
    public function PartnerUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];

        $issetData=PartnerMo::GetPartnerDetail($targetIDX);
        if(isset($issetData['idx'])){
            $issetIDX=$issetData['idx'];
            $issetName=$issetData['name'];
        }else{
            $errMsg='해당 마켓매니저 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $step1=0;
        //등급
        if($name!=$issetName){
            $step1=1;
            $ex='등급이 '.$issetName.'에서 '.$name.'(으)로 변경됐습니다';
            $logIDX=$this->PortalLogInsert(415201,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,$issetIDX,$targetIDX,$ex);
        }

        if($step1==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Partner SET
            name=:name
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $logIDX=$this->PortalLogInsert(415201,$targetIDX);


        /*---------------------------------샌드박스----------------------------*/
        $sandboxDb = static::GetSandboxDB();
        $sandboxDBName= self::EbuySandboxDBName;
        $stat0=$sandboxDb->prepare("UPDATE $sandboxDBName.Partner SET
            name=:name
            WHERE idx=:targetIDX
        ");

        $stat0->bindValue(':name', $name);
        $stat0->bindValue(':targetIDX', $targetIDX);
        $stat0->execute();

        //샌드박스 포탈로그
        $logIDX=$this->SandboxPortalLogInsert(415201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}