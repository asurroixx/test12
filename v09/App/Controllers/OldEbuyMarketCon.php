<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketBalanceMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class OldEbuyMarketCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
        View::renderTemplate('page/oldEbuyMarket/oldEbuyMarket.html');
	}

	// //MileageDepositCon 솔팅별 리셋 데이터
    public function DataTableListLoad($data=null){


        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;


        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $Sel2 = $db->prepare("SELECT
        A.idx AS marketIDX,
        A.code AS marketCode,
        A.name AS marketName,
        B.code AS partCode
        FROM $dbName.Market AS A
        INNER JOIN $dbName.Partner AS B ON(A.partnerIDX = B.idx)
        ");
        $Sel2->execute();
        $resultA = $Sel2->fetchAll(PDO::FETCH_ASSOC);




        $monetixPack = [
            'idx'=>'0',
            'marketName'=>'Moneitx  --',
            'marketCode'=>'-',
            'partCode'=>'juo4rkp7',
            'newBalance'=>0,
            'oldBalance'=>0,
            'warningStat'=>"N"
        ];


        $dataPack = [];
        foreach ($resultA as $key ) {
            $marketIDX = $key['marketIDX'];
            $marketCode = $key['marketCode'];
            $marketName = $key['marketName'];
            $partCode = $key['partCode'];


            $getBalance = MarketBalanceMo::getBalance($marketCode);



            if($partCode == 'juo4rkp7'){ //모넥틱스의 경우

            }


            $Sel = $oldDb->prepare("SELECT
                A_ID,
                A_REMAIN_TOTAL,
                withdrawBalance
            FROM $oldDbName.AA_AGENT_PARTNER
            WHERE A_CODE = '$partCode'");

            $Sel->execute();
            $result=$Sel->fetch (PDO::FETCH_ASSOC);

            if(isset($result['A_ID'])){
                $oldIDX= $result['A_ID'];
                $oldMarketBalance = $result['A_REMAIN_TOTAL'];
                if($marketCode == 'zeromarkets1'){
                    $withdrawBalance = $result['withdrawBalance'];
                    $oldMarketBalance = $oldMarketBalance * 1 + $withdrawBalance *1;
                }

                if($partCode=='juo4rkp7'){


                    if (array_key_exists('marketName', $monetixPack) && array_key_exists('newBalance', $monetixPack)) {
                        $aa = $monetixPack['marketName'];
                        $bb = $aa.' '.$marketName.'('.number_format($getBalance,2).'), ';
                        $monetixPack['marketName'] = $bb;
                        $cc = $monetixPack['newBalance'];
                        $dd = $cc*1+ $getBalance*1;
                        $monetixPack['newBalance'] = $dd;
                        $monetixPack['oldBalance']=$oldMarketBalance;
                    }


                }
                //구바이 - 신바이

                $gap  = $oldMarketBalance * 1 - $getBalance * 1;


                $oldMarketBalance = number_format($oldMarketBalance,2);
                $getBalance = number_format($getBalance,2);
                $gap = number_format($gap,2);

                $warningStat="N";
                if($oldMarketBalance!=$getBalance){
                    $warningStat="Y";
                }
                if($partCode!='juo4rkp7' && $partCode!='demo1234'){
                    $dataPack[]=[
                        'idx'=>$marketIDX,
                        'marketCode'=>$marketCode,
                        'partCode'=>$partCode,
                        'marketName'=>$marketName,
                        'oldBalance'=>$oldMarketBalance,
                        'newBalance'=>$getBalance,
                        'gap' => $gap,
                        'warningStat'=>$warningStat
                    ];
                }

            }

        }

        if (isset($monetixPack['oldBalance']) && isset($monetixPack['newBalance'])) {
            if($monetixPack['oldBalance'] != $monetixPack['newBalance']){
                $monetixPack['warningStat'] = "Y";
            }
            $monetixGap = $monetixPack['oldBalance'] *1 - $monetixPack['newBalance'] *1;
            $monetixPack['oldBalance'] = number_format($monetixPack['oldBalance'],2);
            $monetixPack['newBalance'] = number_format($monetixPack['newBalance'],2);
            $monetixPack['gap'] = number_format($monetixGap,2);
            $dataPack[]=$monetixPack;
        }

        $resultData = ['result'=>'t','data'=>$dataPack,'aa'=>$monetixPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	// //client.html 데이터테이블 리스트 로드
 //    public function DataTableListLoad()
 //    {
 //        $dataPack=ClientMo::GetDataTableListLoad();
 //        $resultData = ['data'=>$dataPack,'result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 유저 공통 디테일 로드
 //    public function ClientDetailFormLoad()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        static::ClientDetail($targetIDX);
 //    }

 //    //client.html 디테일 로드
 //    public function ClientRightDetailFormLoad()
 //    {

 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg,'n');
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $dataPack=ClientMo::GetClientDetailData($targetIDX);
 //        $bankPack=ClientMo::GetTargetClientData($targetIDX);
 //        $gradePack=ClientGradeMo::GetClientGradeList();
 //        $getMileage=ClientDetailMo::getMileage($targetIDX);
 //        $renderData=[
 //            'targetIDX'=>$targetIDX,
 //            'dataPack'=>$dataPack,
 //            'bankPack'=>$bankPack,
 //            'gradePack'=>$gradePack,
 //            'getMileage'=>$getMileage
 //        ];
 //        View::renderTemplate('page/client/clientDetail.html',$renderData);
 //    }

 //    //client.html 클라이언트 업데이트
 //    public function ClientUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
 //            $errMsg='gradeIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
 //            $errMsg='gradeName 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $gradeIDX=$_POST['gradeIDX'];
 //        $gradeName=$_POST['gradeName'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $issetGradeIDX=$issetStatusVal['gradeIDX'];
 //            $issetGradeName=$issetStatusVal['gradeName'];

 //            if($issetGradeIDX==$gradeIDX){
 //                $errMsg='이전 등급과 동일한 등급입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $step1=0;

 //        if($gradeName!=$issetGradeName){
 //            $step1=1;
 //            $ex='등급이 '.$issetGradeName.'에서 '.$gradeName.'로 변경됐습니다';
 //            $logIDX=$this->ClientLogInsert(225102,$targetIDX,0);
 //            $logEx=$this->ClientLogExInsert($logIDX,$issetGradeIDX,$gradeIDX,$ex);
 //        }

 //        if($step1==0){
 //            $errMsg='변경된 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("UPDATE $dbName.Client SET
 //            gradeIDX=:gradeIDX
 //            WHERE idx=:targetIDX
 //        ");
 //        $stat1->bindValue(':gradeIDX', $gradeIDX);
 //        $stat1->bindValue(':targetIDX', $targetIDX);
 //        $stat1->execute();



 //        //클라이언트로그
 //        // $this->ClientLogInsert(225102,$targetIDX,0);


 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html status 업데이트
 //    public function StatusUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
 //            $errMsg='statusVal 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $statusVal=$_POST['statusVal'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $statusIDX=$issetStatusVal['statusIDX'];
 //            if($statusIDX==226201){
 //                $errMsg='이미 탈퇴한 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //            if($statusIDX==226203){
 //                $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }


 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("UPDATE $dbName.Client SET
 //            statusIDX=:statusVal
 //            WHERE idx=:targetIDX
 //        ");
 //        $stat1->bindValue(':statusVal', $statusVal);
 //        $stat1->bindValue(':targetIDX', $targetIDX);
 //        $stat1->execute();

 //        //클라이언트로그
 //        $this->ClientLogInsert($statusVal,$targetIDX,0);

 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 해당 클라이언트 발란스 가져오기
 //    public function getBalance()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $getBalance=ClientDetailMo::getMileage($targetIDX);
 //        $resultData = ['result'=>'t','getBalance'=>$getBalance];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

 //    //client.html 해당 클라이언트 발란스 업데이트 (정말 필요할때 해야함)
 //    public function ClientBalanceUpdate()
 //    {
 //        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
 //            $errMsg='targetIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
 //            $errMsg='statusIDX 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['amount'])||empty($_POST['amount'])){
 //            $errMsg='amount 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!isset($_POST['exVal'])||empty($_POST['exVal'])){
 //            $errMsg='exVal 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }
 //        if(!is_numeric($_POST['amount'])) {
 //            $errMsg = '발란스는 숫자만 입력 가능합니다.';
 //            $errOn = $this::errExport($errMsg);
 //        }
 //        $targetIDX=$_POST['targetIDX'];
 //        $statusIDX=$_POST['statusIDX'];
 //        $amount=$_POST['amount'];
 //        $exVal=$_POST['exVal'];

 //        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
 //        if(isset($issetStatusVal['idx'])){
 //            $targetStatusIDX=$issetStatusVal['statusIDX'];
 //            if($targetStatusIDX==226201){
 //                $errMsg='이미 탈퇴한 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }

 //            if($targetStatusIDX==226202){
 //                $errMsg='스태프에 의해 차단된 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }

 //            if($targetStatusIDX==226203){
 //                $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }else{
 //            $errMsg='해당 클라이언트 정보가 없습니다.';
 //            $errOn=$this::errExport($errMsg);
 //        }

 //        $getBalance=ClientDetailMo::getMileage($targetIDX);

 //        if($statusIDX==205201){
 //            if($getBalance < $amount){
 //                $errMsg='차감 발란스가 현재 발란스보다 높습니다.';
 //                $errOn=$this::errExport($errMsg);
 //            }
 //        }

 //        $createTime=date("Y-m-d H:i:s");

 //        $db = static::getDB();
 //        $dbName= self::MainDBName;
 //        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
 //            (clientIDX,statusIDX,targetIDX,amount,createTime)
 //            VALUES
 //            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
 //        ");

 //        $stat1->bindValue(':clientIDX', $targetIDX);
 //        $stat1->bindValue(':statusIDX', $statusIDX);
 //        $stat1->bindValue(':targetIDX', 0);
 //        $stat1->bindValue(':amount', $amount);
 //        $stat1->bindValue(':createTime', $createTime);
 //        $stat1->execute();
 //        $insertIDX = $db->lastInsertId();

 //        $ex=$exVal.' ('.$amount.'원)';


 //        $logIDX=static::ClientLogInsert($statusIDX,$targetIDX,$targetIDX);
 //        static::ClientLogExInsert($logIDX,0,0,$ex);

 //        $resultData = ['result'=>'t'];
 //        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
 //        echo $result;
 //    }

}