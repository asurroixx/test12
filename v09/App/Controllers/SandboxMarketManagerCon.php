<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\SandboxMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class SandboxMarketManagerCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=SandboxMo::MarketList();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/sandboxMarketManager/sandboxMarketManager.html',$renderData);
	}

	public function dataTableListLoad()
    {
        $dataPack=SandboxMo::DatatableMarketManager();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function DetailFormLoad()
    {
    	if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $pageType=$_POST['pageType'];
        $targetIDX=$_POST['targetIDX'];
        $dataPack=SandboxMo::DetailMarketManager($targetIDX);
        if(!isset($dataPack['managerIDX'])||empty($dataPack['managerIDX'])){
            $errMsg='managerIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $managerIDX=$dataPack['managerIDX'];
        //231226 최근 로그인 추가함 !!!
        $getRecentLogin=SandboxMo::GetRecentLoginTime($managerIDX);
        if(!isset($getRecentLogin['recentLoginTime'])||empty($getRecentLogin['recentLoginTime'])){
            $recentLogin='-';
        }
        $recentLogin=$getRecentLogin['recentLoginTime'];

        $renderData=[
            'targetIDX'=>$targetIDX,
            'recentLogin'=>$recentLogin,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/sandboxMarketManager/sandboxMarketManagerDetail.html',$renderData);
    }

    //main.js 로그인히스토리
    public function ManagerLoginHistory()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=SandboxMo::DetailMarketManager($targetIDX);
        if(!isset($dataPack['managerIDX'])||empty($dataPack['managerIDX'])){
            $errMsg='managerIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $managerIDX=$dataPack['managerIDX'];
        $data=SandboxMo::GetLoginHisList($managerIDX);
        $resultData = ['result'=>'t','data'=>$data];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }



}