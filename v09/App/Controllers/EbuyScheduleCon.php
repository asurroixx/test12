<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyScheduleMo;
use App\Models\EbuyOperatingTimeMo;
use App\Models\StaffLogMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class EbuyScheduleCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/ebuySchedule/ebuySchedule.html');
	}//렌더

    //calendar.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=EbuyScheduleMo::DataTableListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
    public function InsertSchedule($data=null) {
        try {

            $staffIDX=GlobalsVariable::GetGlobals('loginIDX');
            if(!isset($_POST['year'])||empty($_POST['year'])){
                $errMsg='No Param1.';
                $errOn=$this::errExport($errMsg);
            }
            $year = $_POST['year'];
            $year = trim($year);
            if (strlen($year) !== 4 || !ctype_digit($year)) {
                $errMsg='올바른 연도 형식이 아닙니다.';
                $errOn=$this::errExport($errMsg);
            }

            $lastDayOfMonth = date("t", strtotime($year . '-12-01'));
            $startDate = $year.'-01-01';
            $endDate = $year.'-12-'.$lastDayOfMonth;

            $db = static::getDB();
            $dbName= self::MainDBName;
            // $stat1=$db->prepare("
            //     DELETE FROM $dbName.EbuySchedule WHERE statusIDX = 330401 AND date BETWEEN date('$startDate') AND date('$endDate')
            // ");
            // $stat1->execute();
            $url = 'http://apis.data.go.kr/B090041/openapi/service/SpcdeInfoService/getRestDeInfo'; /*URL*/

            $holidayArr=[];
            for ($i = 1; $i <= 12; $i++) {
                $ch = curl_init();
                $queryParams = '?'.urlencode('serviceKey').'=IUpAoCHPrPTH%2BSCBSuIelR2KXw%2BHxUgTxq%2Fas2Jht7G373n9Pie1CvUICyeEhlaFUB3Crr0Lzqj%2FkAQUYnKq1A%3D%3D'; /*Service Key*/
                $queryParams .= '&'.urlencode('solYear').'='.urlencode($year);
                if ($i < 10) {
                    $queryParams .= '&'.urlencode('solMonth').'='.urlencode('0'.$i);
                } else {
                    $queryParams .= '&'.urlencode('solMonth').'='.urlencode($i);
                }
                curl_setopt($ch, CURLOPT_URL, $url . $queryParams);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $response = curl_exec($ch);
                curl_close($ch);

                $object = simplexml_load_string($response);
                $json = json_encode($object);
                $arr = json_decode($json, true);
                $arrHoliItems = $arr['body']['items'];

                if (count($arrHoliItems) != 0) { //공휴일 있는지 없는지 체크
                    $arrHoliDay = $arrHoliItems['item'];
                    // print_r("있음");

                } else {
                    // print_r("없음");
                    continue;
                }
                $monthHolidayArr=[];
                if(!isset($arrHoliDay[0])){
                   $monthHolidayArr[]=$arrHoliDay;
                }else{
                   $monthHolidayArr=$arrHoliDay;
                }
                for ($a = 0; $a < count($monthHolidayArr); $a++) {
                    $holiday = $monthHolidayArr[$a]['locdate'];
                    $name = $monthHolidayArr[$a]['dateName'];
                    $holidayDate = substr_replace($holiday, '-', 4, 0);
                    $holidayFullDate = substr_replace($holidayDate, '-', 7, 0);
                    $createTime = date("Y-m-d H:i:s");

                    $holidayArr[$holidayFullDate]=$name;
                }
            }


            $bulkString = [];
            $countDate=$startDate;
            $openTime = "09:00";
            $closeTime = "18:00";
            //staffalarm 테이블이 쌓기
            $dbName= self::MainDBName;
            $db = static::GetDB();
            $query = "INSERT IGNORE INTO $dbName.EbuySchedule (date,  statusIDX,openTime ,closeTime, memo ) VALUES ";


            for ($i=0; $i < 365; $i++) {
                $bulkString[] = "(:date".$i.",  :statusIDX".$i.",  :openTime ,:closeTime , :memo".$i.")";
                $countDate=date("Y-m-d",strtotime($countDate."+ 1day"));
            }
            $query .= implode(", ", $bulkString);
            $result = $db->prepare($query);
            $countDate=$startDate;
            for ($i=0; $i < 365; $i++) {

                $statusIDX = 390101; //일반 영업일
                $memo='';
                if(isset($holidayArr[$countDate])){
                    $dateName = $holidayArr[$countDate];
                    $statusIDX = 390104; //외부 공휴일
                    $memo = $holidayArr[$countDate];
                }
                $date = new \DateTime($countDate);
                $dayOfWeek = $date->format('N');
                if ($dayOfWeek == 6 || $dayOfWeek == 7) {

                    $statusIDX = 390102; //주말
                }

                $result->bindValue("date".$i, $countDate);
                $result->bindValue("statusIDX".$i, $statusIDX);
                $result->bindValue("memo".$i, $memo);
                $countDate=date("Y-m-d",strtotime($countDate."+ 1day"));
            }
            $result->bindValue(":openTime", $openTime);
            $result->bindValue(":closeTime", $closeTime);
            $result->execute();
            $arr=['result'=>'t','msg'=>'성공'];
            echo json_encode($arr,JSON_UNESCAPED_UNICODE);
        } catch (\Throwable $e) {$errMsg=$e->__toString();$errOn=$this::errExport($errMsg);}
    }

    public function dateUpdate($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['openTime'])||empty($_POST['openTime'])){
            $errMsg='openTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['closeTime'])||empty($_POST['closeTime'])){
            $errMsg='closeTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $memo="";
        if(isset($_POST['memo']) && !empty($_POST['memo'])){
            $memo=$_POST['memo'];
        }
        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $openTime=$_POST['openTime'].':00';
        $closeTime=$_POST['closeTime'].':00';


        //db정보꺼내오기
        $targetData=EbuyScheduleMo::GetUpdateTargetData($targetIDX);
        if(!isset($targetData['idx'])){
            $errMsg='타겟이 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        $targetStatusIDX=$targetData['statusIDX'];
        $targetOpenTime=$targetData['openTime'];
        $targetCloseTime=$targetData['closeTime'];
        $targetMemo=$targetData['memo'];

        if (strtotime($openTime) > strtotime($closeTime)) {
            $errMsg='오픈시간이 마감시간보다 늦을 수 없습니다';
            $errOn = $this::errExport($errMsg);
        }

        $step1=0;
        $step2=0;
        $step3=0;
        $step4=0;

        //statusIDX
        if($statusIDX!=$targetStatusIDX){
            $statusName='';
            if($statusIDX==390101){
                $statusName="영업일";
            }else if($statusIDX==390102){
                $statusName="주말";
            }else if($statusIDX==390103){
                $statusName="내부휴일";
            }else if($statusIDX==390104){
                $statusName="공휴일";
            }
            $targetStatusName='';
            if($targetStatusIDX==390101){
                $targetStatusName="영업일";
            }else if($targetStatusIDX==390102){
                $targetStatusName="주말";
            }else if($targetStatusIDX==390103){
                $targetStatusName="내부휴일";
            }else if($targetStatusIDX==390104){
                $targetStatusName="공휴일";
            }


            $step1=1;
            $ex='상태가 '.$targetStatusName.'에서 '.$statusName.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$targetStatusIDX,$statusIDX,$ex);
        }

        //openTime
        if($openTime!=$targetOpenTime){
            $step2=1;
            $ex='시작시간이 '.$targetOpenTime.'에서 '.$openTime.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        //closeTime
        if($closeTime!=$targetCloseTime){
            $step3=1;
            $ex='마감시간이 '.$targetCloseTime.'에서 '.$closeTime.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        //memo
        if($memo!=$targetMemo){
            $step4=1;
            $ex='메모를 '.$targetMemo.'에서 '.$memo.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.EbuySchedule SET
            statusIDX=:statusIDX,
            openTime=:openTime,
            closeTime=:closeTime,
            memo=:memo
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':openTime', $openTime);
        $stat1->bindValue(':closeTime', $closeTime);
        $stat1->bindValue(':memo', $memo);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
    //Calendar.html 디테일 로드
    public function ChangeOperateTimeForm()
    {
        $getOperateTime = EbuyOperatingTimeMo::getOperateTime();
        $lastStaff='';
        $whereType ='and';
        $whereArr=['390301'];
        $arr=['targetIDX'=>1,'whereType'=>$whereType,'whereArr'=>$whereArr];
        $data=StaffLogMo::GetLogList($arr);
        if(count($data)>0){
            $lastStaff=$data[0]['staffName'];
        }
        $openTime = $getOperateTime['openTime'];
        $closeTime = $getOperateTime['closeTime'];
        $renderArr=["openTime"=>$openTime,"closeTime"=>$closeTime,"lastStaff"=>$lastStaff];
        View::renderTemplate('page/ebuySchedule/operateTimeChangeForm.html',$renderArr);
    }


    //Calendar.html 디테일 로드
    public function InsertScheduleForm()
    {
        View::renderTemplate('page/ebuySchedule/insertScheduleForm.html');
    }

    public function changeOperateTime($data=null)
        {
            if(!isset($_POST['openTime'])||empty($_POST['openTime'])){
                $errMsg='openTime 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            if(!isset($_POST['closeTime'])||empty($_POST['closeTime'])){
                $errMsg='closeTime 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $openTime=$_POST['openTime'];
            $closeTime=$_POST['closeTime'];
            $setDate=date("Y-m-d");



            $getOperateTime = EbuyOperatingTimeMo::getOperateTime();
            $oldOpenTime = $getOperateTime['openTime'];
            $oldCloseTime = $getOperateTime['closeTime'];

            if($oldOpenTime==$openTime && $oldCloseTime==$closeTime){
                $errMsg='변경된 항목이 없습니다';
                $errOn = $this::errExport($errMsg);
            }

            $openTime=$_POST['openTime'].':00';
            $closeTime=$_POST['closeTime'].':00';
            if (strtotime($openTime) > strtotime($closeTime)) {
                $errMsg='개시시간이 마감시간보다 늦을 수 없습니다';
                $errOn = $this::errExport($errMsg);
            }


            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat1=$db->prepare("UPDATE $dbName.EbuySchedule SET
                openTime=:openTime,
                closeTime=:closeTime
                WHERE date >= :setDate
            ");
            $stat1->bindValue(':openTime', $openTime);
            $stat1->bindValue(':closeTime', $closeTime);
            $stat1->bindValue(':setDate', $setDate);
            $stat1->execute();

            $stat2=$db->prepare("UPDATE $dbName.EbuyOperatingTime SET
                openTime=:openTime,
                closeTime=:closeTime
                WHERE idx=:idx
            ");
            $stat2->bindValue(':openTime', $openTime);
            $stat2->bindValue(':closeTime', $closeTime);
            $stat2->bindValue(':idx', 1);
            $stat2->execute();

            $ex="영업시간이 ".$oldOpenTime."~".$oldCloseTime." 에서 ".$openTime."~".$closeTime."으로 변경되었습니다.";
            $logIDX=$this->StaffLogInsert(390301,1);
            $logEx=$this->StaffLogExInsert($logIDX,1,0,$ex);


            $resultData = ['result'=>'t'];
            $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
            echo $result;

    }
    //Calendar.html 디테일 로드
    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $renderData=[
            'targetIDX'=>$targetIDX,
        ];
        View::renderTemplate('page/ebuySchedule/ebuyScheduleDetail.html',$renderData);
    }
}