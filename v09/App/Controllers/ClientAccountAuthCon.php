<?php

namespace App\Controllers;

use \Core\View;
use App\Models\AccountAuthLogMo;


class ClientAccountAuthCon extends \Core\Controller
{
    public function Render($data=null)
    {
        View::renderTemplate('page/clientAccountAuth/clientAccountAuth.html');
    }

    public function DataTableListLoad()
    {
        $dataPack=AccountAuthLogMo::GetDataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}