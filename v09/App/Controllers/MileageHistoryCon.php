<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\ClientMileageMo;
use App\Models\MarketDepositLogMo;
use App\Models\MarketWithdrawalLogMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MileageHistoryCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{

		View::renderTemplate('page/mileageHistory/mileageHistory.html');
	}//렌더

    public function DataTableListLoad($data=null)
    {
    	if(!isset($_POST['startDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
		$startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=ClientMileageMo::GetDatatableList($dataArr);
        foreach ($dataPack as $key => $item) {
        	$statusIDX=$item['statusIDX'];
        	$targetIDX=$item['targetIDX'];
        	$marketName='-';
        	$marketUserEmail='-';
        	if($statusIDX==908102){//출금대행
        		$getMarket=MarketWithdrawalLogMo::GetTargetWithdrawalData($targetIDX);
        		if(isset($getMarket['marketCode'])||!empty($getMarket['marketCode'])){
	        		$marketCode =$getMarket['marketCode'];
        			$getMarketName=MarketMo::GetMarketCode($marketCode);
        			if(isset($getMarketName['idx'])||!empty($getMarketName['idx'])){
	        			$marketName =$getMarketName['name'];
        			}
	        		$marketUserEmail=$getMarket['marketUserEmail'];
        		}
        	}
        	if($statusIDX==906101){//지불대행
        		$getMarket=MarketDepositLogMo::GetTargetDepositData($targetIDX);
        		if(isset($getMarket['marketCode'])||!empty($getMarket['marketCode'])){
	        		$marketCode =$getMarket['marketCode'];
        			$getMarketName=MarketMo::GetMarketCode($marketCode);
        			if(isset($getMarketName['idx'])||!empty($getMarketName['idx'])){
	        			$marketName =$getMarketName['name'];
        			}
	        		$marketUserEmail=$getMarket['marketUserEmail'];
        		}
        	}
    		    $dataPack[$key]['marketName'] = $marketName;
    		    $dataPack[$key]['marketUserEmail'] = $marketUserEmail;
        }

        $resultData = [
	        'result'=>'t',
            'data'=>$dataPack
	    ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}