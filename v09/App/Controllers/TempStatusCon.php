<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffMo;
use App\Models\PartnerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class TempStatusCon extends \Core\Controller
{

    public function djTemp()
    {
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $insert =$db->prepare("INSERT INTO $dbName.Status
            (idx,memo)
            VALUES
            (417101,'발란스 정산 크립토 신청'),
            (417102,'발란스 정산 뱅크와이어 신청'),
            (417103,'발란스 정산 원화 신청'),
            (417201,'발란스 정산 완료'),
            (417211,'발란스 정산 취소(스태프가)'),
            (417212,'발란스 정산 취소(마켓매니저가)'),
            (418101,'발란스 역정산 크립토 신청'),
            (418102,'발란스 역정산 뱅크와이어 신청'),
            (418103,'발란스 역정산 원화 신청'),
            (418201,'발란스 역정산 완료'),
            (418211,'발란스 역정산 취소(스태프가)'),
            (418212,'발란스 역정산 취소(마켓매니저가)'),
            (419101,'발란스 스태프에 의한 증감'),
            (419201,'발란스 스태프에 의한 차감')
        ");
        $insert->execute();

    }

}