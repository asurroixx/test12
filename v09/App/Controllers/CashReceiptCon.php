<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientCashReceiptMo;
use App\Models\MarketDepositLogMo;




use App\Models\ClientMo;
use App\Models\ClientGradeMo;
use App\Models\ClientDetailMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class CashReceiptCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		View::renderTemplate('page/cashReceipt/cashReceipt.html');
	}



    public function DataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=ClientCashReceiptMo::GetDataTableListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientCashReceiptMo::GetDetail($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='디테일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //
        $detailPack   = MarketDepositLogMo::GetTargetDepositData($dataPack['targetIDX']);
        if(!$detailPack) $this::errExport('거래 디테일 정보가 없습니다.');
        // 에러 메시지
        $log               = $detailPack['log'] ?? '';
        $logDecode         = json_decode($log, true);
        $detailPack['msg'] = $logDecode['msg'] ?? '';

        $renderData=[
            'info'=>$dataPack,
            'data'=>$detailPack,
        ];
        View::renderTemplate('page/cashReceipt/cashReceiptDetail.html',$renderData);
    }

    public function DetailDataLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientCashReceiptMo::GetDetail($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='디테일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($dataPack['result_TID'])||empty($dataPack['result_TID'])||$dataPack['result_TID']===0){
            $errMsg='TID오류';
            $errOn=$this::errExport($errMsg);
        }
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function PowerTryAgain()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getdata=ClientCashReceiptMo::GetDetail($targetIDX);
        if(!isset($getdata['idx'])||empty($getdata['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['statusIDX'])||empty($getdata['statusIDX'])){
            $errMsg='해당 정보의 상태가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($getdata['statusIDX']!=811402 AND $getdata['statusIDX']!=811403){
            $errMsg='발행실패 상태인 건만 재발행이 가능합니다. 현재상태 : '.$getdata['statusIDX'].' (811402, 811403만 가능)';
            $errOn=$this::errExport($errMsg);
        }

        $nowStatusIDX=$getdata['statusIDX'];
        $clientIDX=$getdata['clientIDX'];
        $nextStatus=811201;

        $nowTime=date("Y-m-d H:i:s");
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $stat1=$db->prepare("UPDATE $dbName.ClientCashReceipt SET
            statusIDX=:statusIDX
            ,resultTime=:resultTime
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $nextStatus);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':resultTime', $nowTime);
        $stat1->execute();

        $logIDX=static::ClientLogInsert($nextStatus,$targetIDX,$clientIDX);
        $ex=$nowStatusIDX.'상태에서 재발행 시작함';
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function CashReceiptStop()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getdata=ClientCashReceiptMo::GetDetail($targetIDX);
        if(!isset($getdata['idx'])||empty($getdata['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['statusIDX'])||empty($getdata['statusIDX'])){
            $errMsg='해당 정보의 상태가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($getdata['statusIDX']!=811201){
            $errMsg='발행대기 상태인 건만 중지 가능합니다. 현재상태 : '.$getdata['statusIDX'].' (811201만 가능)';
            $errOn=$this::errExport($errMsg);
        }

        $nowStatusIDX=$getdata['statusIDX'];
        $clientIDX=$getdata['clientIDX'];
        $nextStatus=811101;

        $nowTime=date("Y-m-d H:i:s");
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $stat1=$db->prepare("UPDATE $dbName.ClientCashReceipt SET
            statusIDX=:statusIDX
            ,resultTime=:resultTime
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $nextStatus);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':resultTime', $nowTime);
        $stat1->execute();

        $logIDX=static::ClientLogInsert($nextStatus,$targetIDX,$clientIDX);
        $ex='현금영수증을 관리자가 중지함';
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
    public function getFulldata()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getdata=ClientCashReceiptMo::GetFulldata($targetIDX);
        if(!isset($getdata[0]['idx'])||empty($getdata[0]['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        function unicode_decode($str) {
            return preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($matches) {
                return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16BE');
            }, $str);
        }
        $resultData = ['result'=>'t','data'=>unicode_decode(stripslashes(json_encode($getdata,JSON_UNESCAPED_UNICODE)))];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function CashReceiptCancel()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['mag'])||empty($_POST['mag'])){
            $errMsg='mag 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $mag=trim($_POST['mag']);

        $getdata=ClientCashReceiptMo::GetData($targetIDX);
        if(!isset($getdata['idx'])||empty($getdata['idx'])){
            $errMsg='해당 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['statusIDX'])||empty($getdata['statusIDX'])){
            $errMsg='해당 정보의 상태가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['statusIDX'])||empty($getdata['statusIDX'])){
            $errMsg='해당 정보의 회원 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['ReceiptAmt'])||empty($getdata['ReceiptAmt'])){
            $errMsg='해당 정보의 금액 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($getdata['result_TID'])||empty($getdata['result_TID'])){
            $errMsg='해당 정보의 TID 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($getdata['statusIDX']!=811401){
            $errMsg='발행 완료 상태인 건만 취소 신청이 가능합니다.';
            $errOn=$this::errExport($errMsg);
        }

        $createTime=date("Y-m-d H:i:s");
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $nextStatus= 811701;
        $nowStatusIDX= $getdata['statusIDX'];
        $clientIDX= $getdata['clientIDX'];
        $amount= $getdata['ReceiptAmt'];
        $tid= $getdata['result_TID'];
        $stat1=$db->prepare("INSERT INTO $dbName.ClientCashCancelReceipt
            (
                statusIDX,
                ClientCashReceiptIDX,
                ClientCashReceipt_TID,
                CancelAmt,
                CancelMsg,
                createTime
                )
            VALUES
            (
                :statusIDX,
                :ClientCashReceiptIDX,
                :ClientCashReceipt_TID,
                :CancelAmt,
                :CancelMsg,
                :createTime
            )
        ");

        $stat1->bindValue(':statusIDX', $nextStatus);
        $stat1->bindValue(':ClientCashReceiptIDX', $targetIDX);
        $stat1->bindValue(':ClientCashReceipt_TID', $tid);
        $stat1->bindValue(':CancelAmt', $amount);
        $stat1->bindValue(':CancelMsg', $mag);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();

        $stat1=$db->prepare("UPDATE $dbName.ClientCashReceipt SET
            statusIDX=:statusIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', 812101);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $logIDX=static::ClientLogInsert($nextStatus,$targetIDX,$clientIDX);
        $ex=$nowStatusIDX.'상태에서 재발행 시작함';
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}