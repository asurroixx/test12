<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\AuthCenterMo;
use App\Models\BankMo;
use App\Models\EbuyScheduleMo;
use App\Models\StaffMo;
use App\Models\ClientMileageDepositMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class DashboardCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{

		View::renderTemplate('page/dashboard/dashboard.html');
	}//렌더

	//이미지 코드 테스트
	public function testAAA($data=null)
	{
		$dataPack=AuthCenterMo::aaa();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	//이미지 코드 테스트
	public function testBBB($data=null)
	{
		$dataPack=BankMo::GetBankData();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	//캘린더 테이블 공휴일 인서트 테스트
	public function testCCC($data=null)
	{
		if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['holiday'])||empty($_POST['holiday'])){
            $errMsg='holiday 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $name=$_POST['name'];
        $holiday=$_POST['holiday'];
        $memo=$_POST['memo'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Calendar
            (name,holiday,memo)
            VALUES
            (:name,:holiday,:memo)
        ");

        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':holiday', $holiday);
        $stat1->bindValue(':memo', $memo);
        $stat1->execute();
        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	//캘린더 테이블 공휴일 리스트
	public function testDDD($data=null)
	{
		$dataPack=CalendarMo::GetDateList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	//캘린더 테이블 공휴일 삭제
	public function testEEE($data=null)
	{
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("
        	DELETE FROM $dbName.Calendar WHERE idx='$targetIDX'
        ");
        $stat1->execute();
        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

    //캘린더 테이블 오픈,마감 시간 변경
    public function testFFF($data=null)
    {
        if(!isset($_POST['openTime'])||empty($_POST['openTime'])){
            $errMsg='openTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['closeTime'])||empty($_POST['closeTime'])){
            $errMsg='closeTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $openTime=$_POST['openTime'];
        $closeTime=$_POST['closeTime'];
        $setDate=date("Y-m-d");

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.EbuySchedule SET
            openTime=:openTime,
            closeTime=:closeTime
            WHERE date >= :setDate
        ");
        $stat1->bindValue(':openTime', $openTime);
        $stat1->bindValue(':closeTime', $closeTime);
        $stat1->bindValue(':setDate', $setDate);
        $stat1->execute();

        $stat2=$db->prepare("UPDATE $dbName.EbuyOperatingTime SET
            openTime=:openTime,
            closeTime=:closeTime
            WHERE idx=:idx
        ");
        $stat2->bindValue(':openTime', $openTime);
        $stat2->bindValue(':closeTime', $closeTime);
        $stat2->bindValue(':idx', 1);
        $stat2->execute();


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //임시 타겟팅 업데이트 (스케줄)
    public function testGGG($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['openTime'])||empty($_POST['openTime'])){
            $errMsg='openTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['closeTime'])||empty($_POST['closeTime'])){
            $errMsg='closeTime 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['memo'])||empty($_POST['memo'])){
            $errMsg='memo 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $openTime=$_POST['openTime'].':00';
        $closeTime=$_POST['closeTime'].':00';
        $memo=$_POST['memo'];

        //db정보꺼내오기
        $targetData=EbuyScheduleMo::GetUpdateTargetData($targetIDX);
        if(!isset($targetData['idx'])){
            $errMsg='타겟이 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        $targetStatusIDX=$targetData['statusIDX'];
        $targetOpenTime=$targetData['openTime'];
        $targetCloseTime=$targetData['closeTime'];
        $targetMemo=$targetData['memo'];

        if (strtotime($openTime) > strtotime($closeTime)) {
            $errMsg='오픈시간이 마감시간보다 늦을 수 없습니다';
            $errOn = $this::errExport($errMsg);
        }

        $step1=0;
        $step2=0;
        $step3=0;
        $step4=0;

        //statusIDX
        if($statusIDX!=$targetStatusIDX){
            $step1=1;
            $ex='statusIDX가 '.$targetStatusIDX.'에서 '.$statusIDX.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$targetStatusIDX,$statusIDX,$ex);
        }

        //openTime
        if($openTime!=$targetOpenTime){
            $step2=1;
            $ex='시작시간이 '.$targetOpenTime.'에서 '.$openTime.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        //closeTime
        if($closeTime!=$targetCloseTime){
            $step3=1;
            $ex='마감시간이 '.$targetCloseTime.'에서 '.$closeTime.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        //memo
        if($memo!=$targetMemo){
            $step4=1;
            $ex='메모를 '.$targetMemo.'에서 '.$memo.'(으)로 변경됐습니다.';
            $logIDX=$this->StaffLogInsert(390201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.EbuySchedule SET
            statusIDX=:statusIDX,
            openTime=:openTime,
            closeTime=:closeTime,
            memo=:memo
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':openTime', $openTime);
        $stat1->bindValue(':closeTime', $closeTime);
        $stat1->bindValue(':memo', $memo);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function djTest(){
        //답변예정시간을 찾아보자


        function holidayCount($kstTime){
            $explodeDate=explode(" ", $kstTime);
            $dayChk=$explodeDate[0];
            $timeChk=$explodeDate[1];
            $holidayChk = EbuyScheduleMo::GetResponseDate($dayChk);
            if(isset($holidayChk['idx'])){
                $statusIDX=$holidayChk['statusIDX'];
                $date=$holidayChk['date'];
                $openTime=$holidayChk['openTime'];
                $closeTime=$holidayChk['closeTime'];

                // 00:00:00 ~ 오픈시간 = 당일(오픈시간 + 3시간)
                // 오픈시간 ~ (마감시간 - 3시간전) = 마감시간
                // (마감시간 - 3시간전) ~ 23:59:59 = 익일(오픈시간 + 3시간)

                //오픈 3시간후
                $openAfterTime = date("H:i:s", strtotime($openTime) + 3 * 3600);
                //마감 3시간전
                $closeBeforeTime = date("H:i:s", strtotime($closeTime) - 3 * 3600);

                //영업일 아니면 다음날로 넘겨
                if($statusIDX!=390101){
                    $nextDate=date("Y-m-d 00:00:00",strtotime($dayChk."+ 1day"));
                    return holidayCount($nextDate);
                }

                //문의가 오픈시간 전에 들어오면 당일(오픈시간 + 3시간)
                if($dayChk == $date && $timeChk < $openTime){
                    $responseDate=$dayChk.' '.$openAfterTime;
                }
                //문의가 오픈시간 ~ 마감3시간전에 들어오면 당일 마감시간
                if($dayChk == $date && $timeChk > $openTime && $timeChk < $closeBeforeTime){
                    $responseDate=$dayChk.' '.$closeTime;
                }

                //문의가 마감3시간전에서 그날12시까지 들어오면 익일(오픈시간 + 3시간)
                if($dayChk == $date && $timeChk > $closeBeforeTime && $timeChk < "23:59:59"){
                    $dayChk=date("Y-m-d",strtotime($dayChk."+ 1day"));
                    $responseDate=$dayChk.' '.$openAfterTime;
                    //만약 다음날이 영업일이 아니면?
                    $nextHolidayChk=EbuyScheduleMo::GetResponseDate($dayChk);
                    $nextStatusIDX=$nextHolidayChk['statusIDX'];
                    if($nextStatusIDX!=390101){
                        return holidayCount($dayChk." 00:00:00");
                    }
                }

                //오늘이 영업일이 아닐때는 다음 영업날까지 12시로 해야됨 00:00:00은 스크립트로 보낸값
                if($dayChk == $date && $timeChk == "00:00:00"){
                    $responseDate=$dayChk.' '.$openAfterTime;
                }

            }

            // 한국 시간대를 다시 UTC로 바꿔줌 (UTC-9)
            $finalDate=date("Y-m-d H:i:s", strtotime($responseDate) - 9 * 3600);
            return $finalDate;

        }

        $utcTime = date("Y-m-d H:i:s");
        // 한국 시간대 시차를 더합니다 (UTC+9)
        $kstTime = date("Y-m-d H:i:s", strtotime($utcTime) + 9 * 3600);
        $finalDate = holidayCount($kstTime);


        $finalTxt= '답변예정일은 '.$finalDate.' 입니다';
        $resultData = ['result'=>'t','msg'=>$finalTxt];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


    public function ddddddd($data=null){
        $alarmArr=[
            'clientIDX'=>116291,
            'statusIDX'=>102201,
            'targetIDX'=>1,
            'param'=>['clientName'=>'김동주']
        ];
        $AppMainIoUri= self::AppMainIoUri;
        $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
        static::sendCurl($alarmArr,$AppMainIoAddr);
    }

    //앱 스태프 알람용
    public function TestJb($data=null)
    {
        $alarmData=ClientMileageDepositMo::ClientMileageDepositStaffAlarmList();

        $db = static::getDB();
        $dbName= self::MainDBName;

        foreach ($alarmData as $key) {
            $targetIDX = $key['idx'];
            $clientName = $key['name'];

            $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                appStaffAlarm=:appStaffAlarm
                WHERE idx=:idx
            ");
            $stat1->bindValue(':appStaffAlarm', 'Y');
            $stat1->bindValue(':idx', $targetIDX);
            $stat1->execute();

            $staffData=StaffMo::GetAppStaffAlarmList();
            foreach ($staffData as $key2) {
                $staffClientIDX = $key2['clientIDX'];
                $staffStatusIDX = $key2['statusIDX'];

                if($staffStatusIDX == 302101){
                     //유저에게 알람을 쏴주자
                    $alarmArr=[
                        'clientIDX'=>$staffClientIDX,
                        'statusIDX'=>102201,
                        'targetIDX'=>$targetIDX,
                        'param'=>['clientName'=>$clientName]
                    ];
                    $AppMainIoUri= self::AppMainIoUri;
                    $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                    static::sendCurl($alarmArr,$AppMainIoAddr);
                }
            }
        }

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //qna dayJob에 자동종료 함수
    public function TestJb2222222($data=null)
    {
        $createTime = date("Y-m-d H:i:s");
    //     $db = static::getDB();
    //     $dbName = self::MainDBName;
    //     $stat4 = $db->prepare("UPDATE $dbName.Qna SET
    //         statusIDX=:statusIDX
    //         WHERE expectedTime < SUBDATE(NOW(), INTERVAL 3 DAY) AND statusIDX=340101 AND ticketCode NOT LIKE 'SA%'
    //     ");
    //     $stat4->bindValue(':statusIDX', 340301);
    //     $stat4->execute();
    //     $updatedRowCount = $stat4->rowCount();


    //     $updatedQnaIdxList = [];

    //     if ($updatedRowCount > 0) {
    //         // 340301인 행의 idx를 가져옴
    //         $stat5 = $db->prepare("SELECT
    //         idx
    //         FROM $dbName.Qna WHERE statusIDX = 340301 ");
    //         $stat5->execute();
    //         $updatedQnaIdxList = $stat5->fetchAll(PDO::FETCH_COLUMN);

    //         $content = '3 days after the answer was completed, it was automatically terminated automatically.';
    //         $staffContent = '답변 완료 후 3일이 지나 자동 종료 되었습니다.';

    //         foreach ($updatedQnaIdxList as $qnaIDX) {
    //             // 해당 Qna 레코드의 상태를 확인
    //             $statCheckStatus = $db->prepare("SELECT statusIDX FROM $dbName.Qna WHERE idx = :qnaIDX");
    //             $statCheckStatus->bindValue(':qnaIDX', $qnaIDX);
    //             $statCheckStatus->execute();
    //             $qnaStatus = $statCheckStatus->fetch(PDO::FETCH_COLUMN);


    //             if ($qnaStatus == 340301) {
    //                 // 이미 340301로 업데이트 된 경우에만 QnaAnswer에 insert
    //                 $stat6 = $db->prepare("INSERT INTO $dbName.QnaAnswer
    //                     (qnaIDX, content, staffContent, statusIDX, createTime, staffIDX)
    //                     VALUES
    //                     (:qnaIDX, :content, :staffContent, :statusIDX, :createTime, :staffIDX)
    //                 ");

    //                 $stat6->bindValue(':qnaIDX', $qnaIDX);
    //                 $stat6->bindValue(':content', $content);
    //                 $stat6->bindValue(':staffContent', $staffContent);
    //                 $stat6->bindValue(':statusIDX', 341103);
    //                 $stat6->bindValue(':createTime', $createTime);
    //                 $stat6->bindValue(':staffIDX', 0);
    //                 $stat6->execute();
    //             }

    //             //시스템 답변을 했으니 자동종료로 다시 바꿔줘
    //             $stat7 = $db->prepare("UPDATE $dbName.Qna SET
    //                 statusIDX=:statusIDX
    //                 WHERE idx=:qnaIDX
    //             ");
    //             $stat7->bindValue(':statusIDX', 140301);
    //             $stat7->bindValue(':qnaIDX', $qnaIDX);
    //             $stat7->execute();
    //         }
    //     }
    }

}
