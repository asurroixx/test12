<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientCashReceiptSettingMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class CashReceiptSettingCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		View::renderTemplate('page/cashReceiptSetting/cashReceiptSetting.html');
	}

	public function DataTableListLoad()
    {
        $dataPack=ClientCashReceiptSettingMo::GetDataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}