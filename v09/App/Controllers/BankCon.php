<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\BankMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class BankCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{

		View::renderTemplate('page/bank/bank.html');
	}//렌더

	//bank.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=BankMo::GetBankData();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //bank.html 이름 및 코드 추가
    public function BankInsert()
    {

        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['code'])||empty($_POST['code'])){
            $errMsg='code 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $name=$_POST['name'];
        $code=$_POST['code'];
        $statusIDX=329301;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Bank
            (statusIDX,name,code)
            VALUES
            ('$statusIDX','$name','$code')
        ");
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(329101,$targetIDX);

        //api서버도 동일하게 추가
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("INSERT INTO $apiDbName.Bank
            (statusIDX,name,code)
            VALUES
            (:statusIDX,:name,:code)
        ");

        $stat2->bindValue(':statusIDX', $statusIDX);
        $stat2->bindValue(':name', $name);
        $stat2->bindValue(':code', $code);
        $stat2->execute();
        //api서버도 동일하게 추가

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //bank.html 이름 및 코드 업데이트
    public function BankUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['code'])||empty($_POST['code'])){
            $errMsg='code 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];
        $code=$_POST['code'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Bank SET
            name='$name',
            code='$code'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();


        $this->StaffLogInsert(329201,$targetIDX);


        //api서버도 동일하게 업데이트
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("UPDATE $apiDbName.Bank SET
            name=:name,
            code=:code
            WHERE idx=:idx
        ");
        $stat2->bindValue(':name', $name);
        $stat2->bindValue(':code', $code);
        $stat2->bindValue(':idx', $targetIDX);
        $stat2->execute();
        //api서버도 동일하게 업데이트

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //bank.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $issetStatusVal=BankMo::BankStatusData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $statusIDX=$issetStatusVal['statusIDX'];
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Bank SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->StaffLogInsert($statusVal,$targetIDX);

        //api서버도 동일하게 업데이트
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("UPDATE $apiDbName.Bank SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat2->bindValue(':statusVal', $statusVal);
        $stat2->bindValue(':targetIDX', $targetIDX);
        $stat2->execute();
        //api서버도 동일하게 업데이트

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}