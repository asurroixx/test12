<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffMenuMo;
use App\Models\StaffGradeMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MenuCon extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */

    //menu.html 랜더
    public function Render()
    {
        View::renderTemplate('page/menu/menu.html');
    }
    
    //menu.html 메뉴 리스트
    public function GetMenuListLoad()
    {

        $gradeIDX= GlobalsVariable::GetGlobals('loginGrade');
        $menuList = StaffMenuMo::GetGradeMenuList($gradeIDX);
        $menuListArr=[];
        $domain= $_SERVER['REQUEST_URI'];
        foreach ($menuList as $key ) {
            $idx=$key['idx'];
            $momIDX=$key['momIDX'];
            $menuName=$key['name'];
            $url=$key['url'];
            $seq=$key['seq'];
            $status=$key['status'];
            $memo=$key['memo'];
            $loginIDX= GlobalsVariable::GetGlobals('loginIDX');
            $childArr=['menuIDX'=>$idx,'gradeIDX'=>$gradeIDX,'loginIDX'=>$loginIDX];
            $staffChildMenuList = StaffMenuMo::GetChildGradeMenuList($childArr);
            $childMenuListArr=[];
            foreach ($staffChildMenuList as $key) {
                $childIDX=$key['idx'];
                $childMenuName=$key['name'];
                $childSubUrl=$key['url'];
                $childStatus=$key['status'];

                $childMenuListArr[]=array(
                    'childIDX'=>$childIDX,
                    'childMenuName'=>$childMenuName,
                    'childSubUrl'=>$childSubUrl,
                    'childStatus'=>$childStatus
                );
            }
            
            
            $menuListArr[]=array(
                'idx'=>$idx,
                'momIDX'=>$momIDX,
                'name'=>$menuName,
                'url'=>$url,
                'seq'=>$seq,
                'status'=>$status,
                'memo'=>$memo,
                'childMenuListArr'=>$childMenuListArr
            );
        }
        $menuResult = ['data'=>$menuListArr,'result'=>'t','domain'=>$domain];
        $result=json_encode($menuResult,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    // //menu.html seq 수정
    public function MenuUpdate()
    {
        $MainDBName=self::MainDBName;
        $dataArr=[];
        if(isset($_POST['data'])){
            $dataArr=$_POST['data'];
        };
       
        print_r($dataArr);
        $momSeq=1;

        $BasicTable=self::BasicTable['StaffMenu'];
        foreach ($dataArr as $key) {
            if(
                isset($key['id'])&&!empty($key['id'])
            ){
                $momIDX=$key['id'];
                $db = static::getDB();
                $stat1=$db->prepare("UPDATE $MainDBName.$BasicTable SET
                    seq='$momSeq',
                    momIDX=0
                    WHERE idx='$momIDX' AND status=3 ORDER BY seq
                ");
                $stat1->execute();

                if(
                    isset($key['children'])&&!empty($key['children'])
                ){
                    
                    $childPack=$key['children'];
                    $childSeq=1;
                    foreach ($childPack as $key2) {
                        $childrenIDX=$key2['id'];
                        $db = static::getDB();
                        $stat2=$db->prepare("UPDATE $MainDBName.$BasicTable SET
                            seq='$childSeq',
                            momIDX='$momIDX'
                            WHERE idx='$childrenIDX' AND status=3 ORDER BY seq
                        ");
                        $stat2->execute();
                        $childSeq+=1;
                    }
                }
                $momSeq+=1;
            }
        }
        
        $dataArr = array(
            'result'=>'t'
        );
        echo json_encode($dataArr,JSON_UNESCAPED_UNICODE);
        
    }

    // //menu.html 메뉴 숨김
    public function CheeseEyeAction()
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $MainDBName=self::MainDBName;
        $BasicTable=self::BasicTable['StaffMenu'];
        $targetIDX=$_POST['idx'];
        $menuList = StaffMenuMo::CheeseEyeAction($targetIDX);
        $idx=$menuList['idx'];
        $status=$menuList['status'];
        $db = static::getDB();
        if($status==2){
            $stat1=$db->prepare("UPDATE $MainDBName.$BasicTable SET
                status=3
                WHERE idx='$idx'
            ");
            $stat1->execute();
        }elseif($status==3){
             $stat2=$db->prepare("UPDATE $MainDBName.$BasicTable SET
                status=2
                WHERE idx='$idx'
            ");
            $stat2->execute();
        }
        $result=['result'=>'t'];
        $result=json_encode($result,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //menuConfirm.html 컨펌창 랜더
    public function MenuConfirm(){
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        $menuIDX=0;
        $menuName='';
        $menuUrl='';
        if($pageType=='ins'){
            
        }elseif($pageType=='upd'){
            if(!isset($_POST['idx'])||empty($_POST['idx'])){
                $errMsg='idx is empty.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['idx'];
            $menuData = StaffMenuMo::MenuConfirmData($targetIDX);
            $menuIDX=$menuData['idx'];
            $menuName=$menuData['name'];
            $menuUrl=$menuData['url'];

        }
        $menuArr=array(
            'idx'=>$menuIDX,
            'name'=>$menuName,
            'url'=>$menuUrl,
            'pageType'=>$pageType
        );
        View::renderTemplate('/page/menu/menuConfirm.html',$menuArr);
    }

    //menu.html 이름 및 url 변경
    public function MenuConfirmUpdate()
    {
        $MainDBName=self::MainDBName;
        $targetIDX='';
        $name='';
        $url='';
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['url'])||empty($_POST['url'])){
            $errMsg='url is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }

        $targetIDX=$_POST['idx'];
        $name=$_POST['name'];
        $name=trim($name);
        $url=$_POST['url'];
        $url=trim($url);

        $db = static::getDB();
        $BasicTable=self::BasicTable['StaffMenu'];

        $stat1=$db->prepare("UPDATE $MainDBName.$BasicTable SET
            name='$name',
            url='$url'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $this->StaffLogInsert(321201,$targetIDX);

        $dataArr = array(
            'result'=>'t'
        );
        $result=json_encode($dataArr,JSON_UNESCAPED_UNICODE);
        echo $result;

    }

    //menu.html 메뉴 삭제
    public function MenuConfirmDelete()
    {
        $MainDBName=self::MainDBName;
        $targetIDX='';
        $momIDX='';
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['idx'];
        $DeleteData=StaffMenuMo::MenuDeleteCheck($targetIDX);
        if(isset($DeleteData['momIDX'])){
            $momIDX=$DeleteData['momIDX'];
        }
        if($targetIDX==$momIDX){
            $dataArr=array(
                'result'=>'f',
                'msg'=>'하위 메뉴가 등록되있어 삭제가 불가합니다.'
            );
            echo json_encode($dataArr,JSON_UNESCAPED_UNICODE);
            return false;
        }

        $db = static::getDB();
        $BasicTable=self::BasicTable['StaffMenu'];
        $stat1=$db->prepare("DELETE FROM $MainDBName.$BasicTable
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();
        $stat2=$db->prepare("DELETE FROM $MainDBName.StaffGroup
            WHERE menuIDX='$targetIDX'
        ");
        $stat2->execute();

        $this->StaffLogInsert(321301,$targetIDX);
        
        $dataArr = array(
            'result'=>'t'
        );
        $result=json_encode($dataArr,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //menu.html 메뉴 추가
    public function MenuConfirmInsert()
    {
        $pageType=$_POST['pageType'];
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['url'])||empty($_POST['url'])){
            $errMsg='url is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $MainDBName=self::MainDBName;
        $name=$_POST['name'];
        $name=trim($name);
        $url=$_POST['url'];
        $url=trim($url);
        $status=$_POST['status'];
        $momIDX=$_POST['momIDX'];
        $db2 = static::getDB();

        // exit();
        $BasicTable=self::BasicTable['StaffMenu'];
        $stat2=$db2->prepare("INSERT INTO $MainDBName.$BasicTable
            (momIDX,name,url,seq,status)
            VALUES
            ('$momIDX','$name','$url',0,'$status')
        ");
        $stat2->execute();

        $gradePack=StaffGradeMo::GetStaffGradeList();
        $menuIDX = $db2->lastInsertId();
        foreach ($gradePack as $key) {
            $gradeIDX=$key['idx'];

            $db = static::getDB();
            $stat1=$db->prepare("INSERT INTO ebuy.StaffGroup
                (gradeIDX,menuIDX,permission)
                VALUES
                ('$gradeIDX','$menuIDX',3)
            ");
            $stat1->execute();
        }

        $this->StaffLogInsert(321101,$menuIDX);

        $dataArr = array(
            'result'=>'t'
        );
        $result=json_encode($dataArr,JSON_UNESCAPED_UNICODE);
        echo $result;

    }
}