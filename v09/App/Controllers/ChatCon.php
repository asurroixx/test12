<?php

namespace App\Controllers;

class ChatCon extends \Core\Controller
{
    public function Chat( $data = null )
    {
        $content = $_POST['content'] ?? '';

        $return = ['type' => 'text', 'answer' => $content.'에 대한 답변입니다.', 'delayMs' => 500];
        echo json_encode($return,JSON_UNESCAPED_UNICODE);

        /**
         * 특정 메시지 오면 DB 에서 찾아서 뿌려야할거 뿌려주기
         * ㄴ 이미지면 파일서버로 가서 이미지 템프 보여주기
         * 
         * msg.push({
         * type: "text",
         * value: "Ok, I'll show you an image!",
         * delayMs: 500
         * });
         * 
         * msg.push({
         * type: "image",
         * value: " https://upload.wikimedia.orgAptenodytes_forsteri_-Snow_Hill_Island%2C_Antarctica_-adults_and_juvenile-8.jpg"
         * });
         * 
         */

    }

}