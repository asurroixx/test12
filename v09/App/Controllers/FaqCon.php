<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\FaqMo;
use App\Models\FaqCategoryMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class FaqCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

    //렌더
	public function Render($data=null)
	{
		View::renderTemplate('page/faq/faq.html');
	}

	//faq.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=FaqMo::DataTableListLoad();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //faq.html 디테일 로드
    public function FaqDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=FaqMo::GetFaqDetailData($targetIDX);
        }else if($pageType=='ins'){
            $dataPack='';
            $targetIDX='';
        }
        $cateArr = FaqCategoryMo::GetCategoryList();
        $renderData=[
            //ins
            'pageType'=>$pageType,
            //
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'cateArr'=>$cateArr,
        ];
        View::renderTemplate('page/faq/faqDetail.html',$renderData);

    }

    //faq.html 추가
    public function FaqInsert()
    {
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['faqCategoryIDX'])||empty($_POST['faqCategoryIDX'])){
            $errMsg='faqCategoryIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $title=$_POST['title'];
        $content=$_POST['content'];
        $faqCategoryIDX=$_POST['faqCategoryIDX'];
        $createTime=date("Y-m-d H:i:s");
        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');
        $statusIDX=343401;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Faq
            (faqCategoryIDX,title,content,createTime,staffIDX,statusIDX)
            VALUES
            (:faqCategoryIDX,:title,:content,:createTime,:staffIDX,:statusIDX)
        ");

        $stat1->bindValue(':faqCategoryIDX', $faqCategoryIDX);
        $stat1->bindValue(':title', $title);
        $stat1->bindValue(':content', $content);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':staffIDX', $staffIDX);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(343101,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //faq.html 업데이트
    public function FaqUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['title'])||empty($_POST['title'])){
            $errMsg='title 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['content'])||empty($_POST['content'])){
            $errMsg='content 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $title=$_POST['title'];
        $content=$_POST['content'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Faq SET
            title=:title,
            content=:content
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->bindValue(':title', $title);
        $stat1->bindValue(':content', $content);
        $stat1->execute();

        $this->StaffLogInsert(343201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //faq.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Faq SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $this->StaffLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //faq.html 삭제버튼 status 업데이트
    public function DeleteStatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=343301;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Faq SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //포탈로그
        $this->StaffLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    /* ---------------------------------------카테고리 영역--------------------------------------------- */

    //faq.html 카테고리 컨펌 로드
    public function QnaCategoryConfirmLoad()
    {
        View::renderTemplate('page/faq/categoryForm.html');
    }

    //categoryForm.html 카테고리 리스트
    public function FaqCategoryFormList()
    {
        $categoryList = FaqCategoryMo::GetCategoryList();
        $categoryListArr=[];
        foreach ($categoryList as $key ) {
            $idx=$key['idx'];
            $momIDX=$key['momIDX'];
            $cateName=$key['name'];
            $seq=$key['seq'];
            $statusIDX=$key['statusIDX'];
            $childCategoryList = FaqCategoryMo::GetChildCategoryList($idx);
            $childCategoryListArr=[];
            foreach ($childCategoryList as $key) {
                $childIDX=$key['idx'];
                $childCateName=$key['name'];
                $childStatusIDX=$key['statusIDX'];

                $childCategoryListArr[]=array(
                    'childIDX'=>$childIDX,
                    'childCateName'=>$childCateName,
                    'childStatusIDX'=>$childStatusIDX
                );
            }

            $categoryListArr[]=array(
                'idx'=>$idx,
                'momIDX'=>$momIDX,
                'name'=>$cateName,
                'seq'=>$seq,
                'statusIDX'=>$statusIDX,
                'childCategoryListArr'=>$childCategoryListArr
            );
        }

        $menuResult = ['result'=>'t','data'=>$categoryListArr];
        $result=json_encode($menuResult,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html seq 수정
    public function MenuUpdate()
    {
        $dbName=self::MainDBName;
        $dataArr=[];
        if(isset($_POST['data'])){
            $dataArr=$_POST['data'];
        };

        $momSeq=1;

        foreach ($dataArr as $key) {
            if(
                isset($key['id'])&&!empty($key['id'])
            ){
                $momIDX=$key['id'];
                $db = static::getDB();
                $stat1=$db->prepare("UPDATE $dbName.FaqCategory SET
                    seq='$momSeq',
                    momIDX=0
                    WHERE idx='$momIDX' AND statusIDX=346401 ORDER BY seq
                ");
                $stat1->execute();

                if(
                    isset($key['children'])&&!empty($key['children'])
                ){

                    $childPack=$key['children'];
                    $childSeq=1;
                    foreach ($childPack as $key2) {
                        $childrenIDX=$key2['id'];
                        $db = static::getDB();
                        $stat2=$db->prepare("UPDATE $dbName.FaqCategory SET
                            seq='$childSeq',
                            momIDX='$momIDX'
                            WHERE idx='$childrenIDX' AND statusIDX=346401 ORDER BY seq
                        ");
                        $stat2->execute();
                        $childSeq+=1;
                    }
                }
                $momSeq+=1;
            }
        }

        $dataArr = array(
            'result'=>'t'
        );
        echo json_encode($dataArr,JSON_UNESCAPED_UNICODE);

    }

    //categoryForm.html 카테고리 추가
    public function FaqCategoryInsert()
    {
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $name=$_POST['name'];
        $name=trim($name);
        $statusIDX=346401;
        $issetName=FaqCategoryMo::GetIssetNameData($name);
        if(isset($issetName['name'])&&!empty($issetName['name'])){
            $errMsg='이미 등록된 카테고리 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.FaqCategory
            (momIDX,name,seq,statusIDX)
            VALUES
            (:momIDX,:name,:seq,:statusIDX)
        ");

        $stat1->bindValue(':momIDX', 0);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':seq', 0);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $logIns=$this->StaffLogInsert(346101,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 메뉴 숨김
    public function CheeseEyeAction()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $dbName=self::MainDBName;
        $targetIDX=$_POST['targetIDX'];
        $menuList = FaqCategoryMo::CheeseEyeAction($targetIDX);
        $idx=$menuList['idx'];
        $statusIDX=$menuList['statusIDX'];
        $db = static::getDB();
        if($statusIDX==346501){
            $stat1=$db->prepare("UPDATE $dbName.FaqCategory SET
                statusIDX=346401
                WHERE idx='$idx'
            ");
            $stat1->execute();
        }elseif($statusIDX==346401){
             $stat2=$db->prepare("UPDATE $dbName.FaqCategory SET
                statusIDX=346501
                WHERE idx='$idx'
            ");
            $stat2->execute();
        }
        if($statusIDX==346401){
            $statusVal=346501;
        }else{
            $statusVal=346401;
        }

        $logIns=$this->StaffLogInsert($statusVal,$targetIDX);

        $result=['result'=>'t'];
        $result=json_encode($result,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 해당 카테고리명 가져올때
    public function GetTargetNameData()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $namePack = FaqCategoryMo::GetTargetData($targetIDX);
        $result=['result'=>'t','namePack'=>$namePack];
        $result=json_encode($result,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 카테고리 수정
    public function FaqCategoryUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];
        $name=trim($name);
        $issetName=FaqCategoryMo::GetIssetNameData($name);
        if(isset($issetName['name'])&&!empty($issetName['name'])){
            $errMsg='이미 등록된 카테고리 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.FaqCategory SET
            name=:name
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $logIns=$this->StaffLogInsert(346201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //categoryForm.html 메뉴 삭제
    public function FaqCategoryDelete()
    {
        $momIDX='';
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX is empty.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $DeleteData=FaqCategoryMo::CategoryDeleteCheck($targetIDX);
        if(isset($DeleteData['momIDX'])){
            $momIDX=$DeleteData['momIDX'];
        }
        if($targetIDX==$momIDX){
            $errMsg='하위 메뉴가 등록되있어 삭제가 불가합니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("
            DELETE FROM $dbName.FaqCategory WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $logIns=$this->StaffLogInsert(346301,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //faqDetail.html 디테일 카테고리 수정
    public function DetailFaqCategoryChange()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['faqCategoryIDX'])||empty($_POST['faqCategoryIDX'])){
            $errMsg='faqCategoryIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $faqCategoryIDX=$_POST['faqCategoryIDX'];
        $issetData=FaqCategoryMo::GetTargetData($faqCategoryIDX);
        if(!isset($issetData['idx'])&&empty($issetData['idx'])){
            $errMsg='해당 카테고리가 존재하지 않습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $changeCateName=$issetData['name'];
        $targetData=FaqMo::GetTargetIssetData($targetIDX);
        $targetCategoryIDX=$targetData['faqCategoryIDX'];
        $targetCategoryName=$targetData['categoryName'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Faq SET
            faqCategoryIDX=:faqCategoryIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':faqCategoryIDX', $faqCategoryIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $step1=0;
        if($targetCategoryIDX!=$faqCategoryIDX){
            $step1=1;
            $ex='카테고리를 '.$targetCategoryName.'에서 '.$changeCateName.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(346601,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$targetCategoryIDX,$targetIDX,$ex);
        }
        if($step1==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }



}