<?php
namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;

use App\Models\ClientMo;
use App\Models\ClientPeerToPeerMo;
use App\Models\ClientPtpAccountMo;
use App\Models\PeerToPeerTypeMo;
use App\Models\ClientDetailMo;
use App\Models\ClientLogMo;

use App\Models\SystemMo;
use App\Models\ClientGradeMo;


use App\Models\XxxJunmoxxXMo;


use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PtpCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/ptp/ptp.html');
	}

	public function dataTableListLoad()
	{
		if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
			$errMsg='startDate 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
			$errMsg='endDate 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$startDate=$_POST['startDate'];
		$endDate=$_POST['endDate'];
		$dataArr=array(
			'startDate'=>$startDate,
			'endDate'=>$endDate,
		);
		$dataPack=ClientPeerToPeerMo::GetClientPeerToPeerData($dataArr);
		$resultData = ['data'=>$dataPack,'result'=>'t'];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}



	public function ClientPtpAccountSearch()
	{
		if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
			$errMsg='clientIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['ptpTypeIDX'])||empty($_POST['ptpTypeIDX'])){
			$errMsg='ptpTypeIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$clientIDX=$_POST['clientIDX'];
		$ptpTypeIDX=$_POST['ptpTypeIDX'];
		$dataArr=array(
			'clientIDX'=>$clientIDX,
			'ptpTypeIDX'=>$ptpTypeIDX,
		);
		$dataPack=ClientPtpAccountMo::GetClientPtpAccountDataWithType($dataArr);
		$resultData = ['data'=>$dataPack,'result'=>'t'];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}

	public function ClientSearch()
	{
		if(!isset($_POST['cType'])||empty($_POST['cType'])){
			$errMsg='cType 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['name'])||empty($_POST['name'])){
			$errMsg='name 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$nameKeyword=trim($_POST['name']);
		// if(mb_strlen($nameKeyword, 'UTF-8')<2){
		if(mb_strlen($nameKeyword, 'UTF-8')<1){
			$errMsg='이름을 2글자 이상 입력해주세요. ['.$nameKeyword.']';
			$errOn=$this::errExport($errMsg);
		}
		//바이어라면 type 이 필요하고 전자화폐 계정이 있어야함
		if($_POST['cType']=='b' && (!isset($_POST['typeIDX'])||empty($_POST['typeIDX']))){
			$errMsg='구매자는 먼저 전자화폐 계정을 선택해야합니다.';
			$errOn=$this::errExport($errMsg);
		}

		if($_POST['cType']=='b'){//구매자를 찾아보자
			$typeIDX=$_POST['typeIDX'];
			$dataArr=array(
				'nameKeyword'=>$nameKeyword,
				'typeIDX'=>$typeIDX
			);
			$dataPack=ClientMo::GetClientListKeywordSearch_withPtpAccount($dataArr);
		}else{//판매자를 찾아보자
			$dataPack=ClientMo::GetClientListKeywordSearch($nameKeyword);
		}
		$resultData = ['data'=>$dataPack,'result'=>'t'];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}

	public function PtpFinalCheck()
	{
		/*
		여기부터 PtpFinalCheck 랑 PtpSubmit 이랑 같음
		*/
		if(!isset($_POST['ptpTypeIDX'])||empty($_POST['ptpTypeIDX'])||$_POST['ptpTypeIDX']*1<1){
			$errMsg='ptpTypeIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['BuyerIDX'])||empty($_POST['BuyerIDX'])||$_POST['BuyerIDX']*1<1){
			$errMsg='BuyerIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['BuyerPtpAccountIDX'])||empty($_POST['BuyerPtpAccountIDX'])||$_POST['BuyerPtpAccountIDX']*1<1){
			$errMsg='BuyerPtpAccountIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['SellerIDX'])||empty($_POST['SellerIDX'])||$_POST['SellerIDX']*1<1){
			$errMsg='SellerIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['Rate'])||empty($_POST['Rate'])||$_POST['Rate']*1<1){
			$errMsg='Rate 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['Amount'])||empty($_POST['Amount'])||$_POST['Amount']*1<1){
			$errMsg='Amount 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$ptpTypeIDX=$_POST['ptpTypeIDX']*1;

		$BuyerIDX=$_POST['BuyerIDX']*1;
		$BuyerPtpAccountIDX=$_POST['BuyerPtpAccountIDX']*1;

		$SellerIDX=$_POST['SellerIDX']*1;

		if($SellerIDX===$BuyerIDX){
			$errMsg='SellerIDX BuyerIDX 똑같습니다.';
			$errOn=$this::errExport($errMsg);
		}

		$Rate=$_POST['Rate']*1;
		$Amount=$_POST['Amount']*1;

		$dataArr=array(
			'clientIDX'=>$BuyerIDX,
			'ptpAccountIDX'=>$BuyerPtpAccountIDX,
			'typeIDX'=>$ptpTypeIDX
		);
		$getPtpBuyerCheck=ClientMo::getPtpBuyerCheck($dataArr);
		if(!isset($getPtpBuyerCheck['idx'])||empty($getPtpBuyerCheck['idx'])){
			$errMsg='getPtpBuyerCheck 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$BuyerTradeFeePer=$getPtpBuyerCheck['tradeFeePer'];
		$BuyerDefaultFeeKRW=$getPtpBuyerCheck['defaultFeeKRW'];
		$BuyerName=$getPtpBuyerCheck['name'];

		$getPtpSellerCheck=ClientMo::getPtpSellerCheck($SellerIDX);
		if(!isset($getPtpSellerCheck['idx'])||empty($getPtpSellerCheck['idx'])){
			$errMsg='getPtpSellerCheck 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$SellerTradeFeePer=$getPtpSellerCheck['tradeFeePer'];
		$SellerDefaultFeeKRW=$getPtpSellerCheck['defaultFeeKRW'];

		$totPrice=bcmul($Rate, $Amount,0);
		$SellerFee=floor(bcmul($totPrice, $SellerTradeFeePer)*0.01);
		if($SellerFee*1<$SellerDefaultFeeKRW*1){
			$SellerFee=$SellerDefaultFeeKRW;
		}
		$SellerPrice=bcsub($totPrice, $SellerFee, 0);
		$BuyerFee=floor(bcmul($totPrice, $BuyerTradeFeePer)*0.01);
		if($BuyerFee*1<$BuyerDefaultFeeKRW*1){
			$BuyerFee=$BuyerDefaultFeeKRW;
		}
		$BuyerPrice=bcadd($totPrice, $BuyerFee, 0);
		$BuyerMileage=ClientDetailMo::getMileage($BuyerIDX);
		/*
		여기까지 PtpFinalCheck 랑 PtpSubmit 이랑 같음
		*/

		if($BuyerMileage*1<$BuyerPrice*1){
			$BuyerDepositEx='구매자 '.$BuyerName.' 계정으로 '.number_format($BuyerPrice*1).'원 입금신청 예정';
		}else{
			$BuyerDepositEx='구매자 '.$BuyerName.' 계정 '.number_format($BuyerPrice*1).'원 차감 예정';
		}


		$resultData = [
			'getPtpBuyerCheck'=>$getPtpBuyerCheck,
			'getPtpSellerCheck'=>$getPtpSellerCheck,
			'totPrice'=>$totPrice,
			'SellerFee'=>$SellerFee,
			'SellerPrice'=>$SellerPrice,
			'BuyerFee'=>$BuyerFee,
			'BuyerPrice'=>$BuyerPrice,
			'BuyerMileage'=>$BuyerMileage,
			'BuyerDepositEx'=>$BuyerDepositEx,
			'Rate'=>$Rate,
			'Amount'=>$Amount,
			'result'=>'t'
		];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}
	public function PtpSubmit()
	{
		/*
		여기부터 PtpFinalCheck 랑 PtpSubmit 이랑 같음
		*/
		if(!isset($_POST['ptpTypeIDX'])||empty($_POST['ptpTypeIDX'])||$_POST['ptpTypeIDX']*1<1){
			$errMsg='ptpTypeIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['BuyerIDX'])||empty($_POST['BuyerIDX'])||$_POST['BuyerIDX']*1<1){
			$errMsg='BuyerIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['BuyerPtpAccountIDX'])||empty($_POST['BuyerPtpAccountIDX'])||$_POST['BuyerPtpAccountIDX']*1<1){
			$errMsg='BuyerPtpAccountIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['SellerIDX'])||empty($_POST['SellerIDX'])||$_POST['SellerIDX']*1<1){
			$errMsg='SellerIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['Rate'])||empty($_POST['Rate'])||$_POST['Rate']*1<1){
			$errMsg='Rate 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		if(!isset($_POST['Amount'])||empty($_POST['Amount'])||$_POST['Amount']*1<1){
			$errMsg='Amount 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$ptpTypeIDX=$_POST['ptpTypeIDX']*1;

		$BuyerIDX=$_POST['BuyerIDX']*1;
		$BuyerPtpAccountIDX=$_POST['BuyerPtpAccountIDX']*1;

		$SellerIDX=$_POST['SellerIDX']*1;

		if($SellerIDX===$BuyerIDX){
			$errMsg='SellerIDX BuyerIDX 똑같습니다.';
			$errOn=$this::errExport($errMsg);
		}

		$Rate=$_POST['Rate']*1;
		$Amount=$_POST['Amount']*1;

		$dataArr=array(
			'clientIDX'=>$BuyerIDX,
			'ptpAccountIDX'=>$BuyerPtpAccountIDX,
			'typeIDX'=>$ptpTypeIDX
		);
		$getPtpBuyerCheck=ClientMo::getPtpBuyerCheck($dataArr);
		if(!isset($getPtpBuyerCheck['idx'])||empty($getPtpBuyerCheck['idx'])){
			$errMsg='getPtpBuyerCheck 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$BuyerTradeFeePer=$getPtpBuyerCheck['tradeFeePer'];
		$BuyerDefaultFeeKRW=$getPtpBuyerCheck['defaultFeeKRW'];
		$BuyerGradeIDX=$getPtpBuyerCheck['gradeIDX'];
		$BuyerBankIDX=$getPtpBuyerCheck['clientBankIDX'];
		$BuyerName=$getPtpBuyerCheck['name'];

		$getPtpSellerCheck=ClientMo::getPtpSellerCheck($SellerIDX);
		if(!isset($getPtpSellerCheck['idx'])||empty($getPtpSellerCheck['idx'])){
			$errMsg='getPtpSellerCheck 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$SellerTradeFeePer=$getPtpSellerCheck['tradeFeePer'];
		$SellerDefaultFeeKRW=$getPtpSellerCheck['defaultFeeKRW'];

		$totPrice=bcmul($Rate, $Amount,0);
		$SellerFee=floor(bcmul($totPrice, $SellerTradeFeePer)*0.01);
		if($SellerFee*1<$SellerDefaultFeeKRW*1){
			$SellerFee=$SellerDefaultFeeKRW;
		}
		$SellerPrice=bcsub($totPrice, $SellerFee, 0);
		$BuyerFee=floor(bcmul($totPrice, $BuyerTradeFeePer)*0.01);
		if($BuyerFee*1<$BuyerDefaultFeeKRW*1){
			$BuyerFee=$BuyerDefaultFeeKRW;
		}
		$BuyerPrice=bcadd($totPrice, $BuyerFee, 0);
		$BuyerMileage=ClientDetailMo::getMileage($BuyerIDX);
		/*
		여기까지 PtpFinalCheck 랑 PtpSubmit 이랑 같음
		*/

		$ClientPeerToPeerStatusIDX=251102;
		$BuyerClientMileageDepositIDX=0;
		if($BuyerMileage*1<$BuyerPrice*1){//마일리지 입금신청 부터 해야함!!!!!!!!!
			$ClientPeerToPeerStatusIDX=251101;

			$depositSystemIDX=6;
			$systemInfo =SystemMo::getSystemStatusInfo_deposit($depositSystemIDX);
			if(!isset($systemInfo['idx'])||empty($systemInfo['idx'])){
				$errMsg='[시스템]필수값이 없습니다.systemIDX';
				$errOn=$this::errExport($errMsg);
			}
			if(!isset($systemInfo['status'])||empty($systemInfo['status'])){
				$errMsg='[시스템]필수값이 없습니다.systemStatus';
				$errOn=$this::errExport($errMsg);
			}
			$depositStatusIDX=$systemInfo['status'];


			$clientGradeIDX=$BuyerGradeIDX;
			$getEbuyBankInfo =ClientGradeMo::getEbuyBankInfo($clientGradeIDX);
			if(!isset($getEbuyBankInfo['idx'])||empty($getEbuyBankInfo['idx'])){
				$errMsg='필수값이 없습니다.ebuyBankIDX';
				$errOn=$this::errExport($errMsg);
			}
			if($getEbuyBankInfo['ebuyBankStatusIDX']!=323301){
				$aa=$getEbuyBankInfo['idx'];//임시
				$errMsg='이바이은행이 비활성된 상태'.$aa;
				$errOn=$this::errExport($errMsg);
			}
			// $ebuyBankIDX=$getEbuyBankInfo['idx'];
			$ebuyBankIDX=$getEbuyBankInfo['ebuyBankIDX'];
			$clientBankIDX=$BuyerBankIDX;
			$createTime= date('Y-m-d H:i:s');

			$db = static::getDB();
			$dbName= self::MainDBName;
			$insert =$db->prepare("INSERT INTO $dbName.ClientMileageDeposit
				(
					clientIDX,
					clientBankIDX,
					ebuyBankIDX,
					statusIDX,
					amount,
					createTime
				)
				VALUES
				(
					:clientIDX,
					:clientBankIDX,
					:ebuyBankIDX,
					:depositStatusIDX,
					:amount,
					:createTime
				)
			");
			$insert->bindValue(':clientIDX', $BuyerIDX);
			$insert->bindValue(':clientBankIDX', $clientBankIDX);
			$insert->bindValue(':ebuyBankIDX', $ebuyBankIDX);
			$insert->bindValue(':depositStatusIDX', $depositStatusIDX);
			$insert->bindValue(':amount', $BuyerPrice);
			$insert->bindValue(':createTime', $createTime);
			$insert->execute();
			$BuyerClientMileageDepositIDX=$db->lastInsertId();

			$logIDX=static::ClientLogInsert($depositStatusIDX,$BuyerClientMileageDepositIDX,$BuyerIDX);
			$ex='P2P거래 시작 구매자 자동입금신청 ('.$BuyerPrice.'원)';
			static::ClientLogExInsert($logIDX,0,0,$ex);


			//스태프에 알람 띄우고 싶은데 어떻게 하는지 모르겠어..
			// $appUri = DefinAll::DomainUri;
			// $goPage ='balanceRecharge';
			// $result=['result'=>'t', 'goPage'=>$goPage, 'amount'=>$BuyerPrice];
			// echo json_encode($result,JSON_UNESCAPED_UNICODE);

			// //스태프에게 알람을 보내자
			// $alarmParam=[
			//     'targetIDX'=>$insertIDX,
			//     'statusIDX'=>$depositStatusIDX,
			//     'param'=>['amount'=>$BuyerPrice]
			// ];
			// StaffAlarmCon::sendAlarmToStaff($alarmParam);
			// static::socketTable($insertIDX);
		}

		$nowTime=date("Y-m-d H:i:s");

		$db = static::getDB();
		$dbName= self::MainDBName;
		$insert=$db->prepare("INSERT INTO $dbName.ClientPeerToPeer
			(
				SellerClientIDX
				,SellerFeePer
				,SellerFee
				,BuyerClientIDX
				,BuyerClientPtpAccountIDX
				,BuyerClientMileageDepositIDX
				,BuyerFeePer
				,BuyerFee
				,amount
				,rate
				,createTime
				,statusIDX
			)
			VALUES
			(
				:SellerClientIDX
				,:SellerFeePer
				,:SellerFee
				,:BuyerClientIDX
				,:BuyerClientPtpAccountIDX
				,:BuyerClientMileageDepositIDX
				,:BuyerFeePer
				,:BuyerFee
				,:amount
				,:rate
				,:createTime
				,:statusIDX
			)
		");
		$insert->bindValue(':SellerClientIDX', $SellerIDX);
		$insert->bindValue(':SellerFeePer', $SellerTradeFeePer);
		$insert->bindValue(':SellerFee', $SellerFee);
		$insert->bindValue(':BuyerClientIDX', $BuyerIDX);
		$insert->bindValue(':BuyerClientPtpAccountIDX', $BuyerPtpAccountIDX);
		$insert->bindValue(':BuyerClientMileageDepositIDX', $BuyerClientMileageDepositIDX);
		$insert->bindValue(':BuyerFeePer', $BuyerTradeFeePer);
		$insert->bindValue(':BuyerFee', $BuyerFee);
		$insert->bindValue(':amount', $Amount);
		$insert->bindValue(':rate', $Rate);
		$insert->bindValue(':createTime', $nowTime);
		$insert->bindValue(':statusIDX', $ClientPeerToPeerStatusIDX);
		$insert->execute();
		$ptpInsertIDX = $db->lastInsertId();
		$logIDX=static::ClientLogInsert($ClientPeerToPeerStatusIDX,$ptpInsertIDX,$SellerIDX);
		$ex='P2P거래 시작 판매자 ('.$SellerPrice.'원)';
		static::ClientLogExInsert($logIDX,0,0,$ex);
		$logIDX=static::ClientLogInsert($ClientPeerToPeerStatusIDX,$ptpInsertIDX,$BuyerIDX);
		$ex='P2P거래 시작 구매자 ('.$BuyerPrice.'원)';
		static::ClientLogExInsert($logIDX,0,0,$ex);

		if($ClientPeerToPeerStatusIDX==251102){
			//구매자 말리지 차감
			$stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
				(clientIDX,statusIDX,targetIDX,amount,createTime)
				VALUES
				(:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
			");
			$stat1->bindValue(':clientIDX', $BuyerIDX);
			$stat1->bindValue(':statusIDX', 251201);
			$stat1->bindValue(':targetIDX', $ptpInsertIDX);
			$stat1->bindValue(':amount', $BuyerPrice);
			$stat1->bindValue(':createTime', $nowTime);
			$stat1->execute();
			$insertIDX = $db->lastInsertId();
			$logIDX=static::ClientLogInsert(251201,$insertIDX,$BuyerIDX);
			$ex='P2P거래 구매자 에스크로 ('.$BuyerPrice.'원)';
			static::ClientLogExInsert($logIDX,0,0,$ex);
			//구매자 말리지 차감 끝

			$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
				statusIDX = :changeStatus
				WHERE idx = :targetIDX
			");
			$update->bindValue(':changeStatus', 251201);
			$update->bindValue(':targetIDX', $ptpInsertIDX);
			$update->execute();

			$logIDX=static::ClientLogInsert(251201,$ptpInsertIDX,$SellerIDX);
			$ex='P2P거래 2단계 판매자';
			static::ClientLogExInsert($logIDX,0,0,$ex);
			$logIDX=static::ClientLogInsert(251201,$ptpInsertIDX,$BuyerIDX);
			$ex='P2P거래 2단계 구매자';
			static::ClientLogExInsert($logIDX,0,0,$ex);
		}elseif($ClientPeerToPeerStatusIDX==251101){//마일리지 입금신청 했군
            //구매자에게 알람을 쏴주자
            //이렇게 보내고 싶은데.. :: P2P거래 구매 입금 신청이 완료되었습니다. 잔액입금화면에서 입금신청 정보를 확인 후 송금해주세요.
            $alarmArr=[
                'clientIDX'=>$BuyerIDX,
                'statusIDX'=>$ClientPeerToPeerStatusIDX,
                'targetIDX'=>$ptpInsertIDX,
                'param'=>[]
            ];
            $AppMainIoUri= self::AppMainIoUri;
            $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
            static::sendCurl($alarmArr,$AppMainIoAddr);
		}

		/*
		푸쉬알람?? 하고 싶은데 어떻게 하는지 모르겠어..
		*/

		$resultData = [
			'ptpIDX'=>$ptpInsertIDX,
			'result'=>'t'
		];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}

	public function DetailFormLoad()
	{
		if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
			$errMsg='pageType 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$pageType=$_POST['pageType'];
		$GetPeerToPeerTypeData='';
		$GetDetail='';
		$GetPtpDetailStatusLog='';
		$targetIDX='new';
		if($pageType=='upd'){
			if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
				$errMsg='targetIDX 정보가 없습니다.';
				$errOn=$this::errExport($errMsg);
			}
			$targetIDX=$_POST['targetIDX'];
			$GetDetail=ClientPeerToPeerMo::GetDetail($targetIDX);
			$GetPtpDetailStatusLog=ClientLogMo::GetPtpDetailStatusLog($targetIDX);

		}else if($pageType=='ins'){
			$GetPeerToPeerTypeData=PeerToPeerTypeMo::GetPeerToPeerTypeData();
		}else if($pageType=='hand'){
			//
		}
		$renderData=[
			'targetIDX'=>$targetIDX,
			'pageType'=>$pageType,
			'GetDetail'=>$GetDetail,
			'GetPtpDetailStatusLog'=>$GetPtpDetailStatusLog,
			'GetPeerToPeerTypeData'=>$GetPeerToPeerTypeData,
		];
		View::renderTemplate('page/ptp/ptpDetail.html',$renderData);
	}

	public function BtnBeforeCheck()
	{
		if(!isset($_POST['type'])||empty($_POST['type'])){
			$errMsg='type 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$btnType=$_POST['type'];//cancel, complete, next

		if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
			$errMsg='targetIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$targetIDX=$_POST['targetIDX'];

		$issetPtp=ClientPeerToPeerMo::issetPtp($targetIDX);
		if(!isset($issetPtp['idx'])||empty($issetPtp['idx'])){
			$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모3한테 물어보세요.';
			$errOn=$this::errExport($errMsg);
		}
		$statusIDX=$issetPtp['statusIDX'];
		$status=$issetPtp['status'];
		if($statusIDX=='251401' || $statusIDX=='251402' || $statusIDX=='251403'){//종료건이니? 꺼지세요~
			$errMsg='이미 종료된 거래입니다. 새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모4한테 물어보세요. 종료 상태: '.$statusIDX.' : '.$status;
			$errOn=$this::errExport($errMsg);
		}
		// 숨고르기. 아래랑 똑같이 히헤히헤

		$BuyerPrice_format=number_format($issetPtp['BuyerPrice']);
		$SellerPrice_format=number_format($issetPtp['SellerPrice']);
		$BuyerName=$issetPtp['BuyerName'];
		$SellerName=$issetPtp['SellerName'];
		$typeName=$issetPtp['typeName'];
		$BuyerClientPtpAccount=$issetPtp['BuyerClientPtpAccount'];
		$amount_format=number_format($issetPtp['amount'],2);
		if($btnType=='next'){
			if($statusIDX!='251201'){//다음 단계로!는 현재 상태가 구매자 예치완료! 상태일때만 가능하기때문에 이외에는 다 병맛임
				$errMsg='"다음 단계로" 는 현재 상태가 구매자 예치완료(251201) 상태일때만 가능합니다. 새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모5한테 물어보세요. 현재 상태: '.$statusIDX.' : '.$status;
				$errOn=$this::errExport($errMsg);
			}

			$msg='판매자('.$SellerName.')님이<br />구매자('.$BuyerName.')님 '.$typeName.' 계정인 '.$BuyerClientPtpAccount.'으로<br />$'.$amount_format.'을 보냈다고 연락이 왔나요?';
		}elseif($btnType=='cancel'){
			if($statusIDX=='251101'){//시작 마일리지 입금신청 같이 됨
				$msg='거래를 취소할까요?<br /><br />구매자('.$BuyerName.')의 입금신청만 되어있는 상태입니다.<br />해당 입금신청이 완료 전이라면 입금신청이 취소됩니다.<br />구매자('.$BuyerName.')에게 돈을 보내지 마라고 하세요.<br /><br />현재 입금신청이 완료처리되어있다면?<br />(구매자('.$BuyerName.')가 돈을 보냈다는 말)<br /> 입금신청완료로 마일리지 증가, P2P거래로 마일리지 차감된 상태일 수 있습니다.<br /><br />1분뒤 상태가 바뀐걸 확인하고 다시 시도해주세요.';
			}elseif($statusIDX=='251102'){//시작 마일리지가 있나 봄. 이거라면 좀 이상한거쥐 왜냐면 현재상태가 이 상태로 머물 수 있는 순간은 없자너
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모2a한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}elseif($statusIDX=='251201' || $statusIDX=='251301'){//251201: 구매자 마일리지 차감 완료, 251301: 판매자 송금완
				$msg='거래를 취소할까요?<br /><br />구매자('.$BuyerName.')의 마일리지가 차감된 상태입니다.<br />진행하시면 구매자('.$BuyerName.')한테 '.$BuyerPrice_format.'원 마일리지를 줍니다!';
			}else{
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모6한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}
		}elseif($btnType=='complete'){
			if($statusIDX=='251101'){//시작 마일리지 입금신청 같이 됨 이거라면 좀 이상한거쥐 왜냐면 뷰에서 내가 버튼을 숨켜놨거던
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모8한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}elseif($statusIDX=='251102'){//시작 마일리지가 있나 봄. 이거라면 좀 이상한거쥐 왜냐면 현재상태가 이 상태로 머물 수 있는 순간은 없자너
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모2b한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}elseif($statusIDX=='251201' || $statusIDX=='251301'){//251201: 구매자 마일리지 차감 완료, 251301: 판매자 송금완
				$msg='거래를 완료할까요?<br /><br />판매자('.$SellerName.')에게 거래대금을 전달합니다.<br />판매자('.$SellerName.')의 마일리지가 '.$SellerPrice_format.'원 증가합니다.';
			}else{
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모6한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}
		}
		$resultData = [
			'targetIDX'=>$targetIDX,
			'statusIDX'=>$statusIDX,
			'msg'=>$msg,
			'type'=>$btnType,
			'data'=>$targetIDX,
			'result'=>'t'
		];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}

	public function BtnSubmit()
	{
		if(!isset($_POST['type'])||empty($_POST['type'])){
			$errMsg='type 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$btnType=$_POST['type'];//cancel, complete, next

		if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
			$errMsg='targetIDX 정보가 없습니다.(ee)';
			$errOn=$this::errExport($errMsg);
		}
		$targetIDX=$_POST['targetIDX'];

		if(!isset($_POST['isStatusIDX'])||empty($_POST['isStatusIDX'])){
			$errMsg='isStatusIDX 정보가 없습니다.';
			$errOn=$this::errExport($errMsg);
		}
		$isStatusIDX=$_POST['isStatusIDX'];

		$issetPtp=ClientPeerToPeerMo::issetPtp($targetIDX);
		if(!isset($issetPtp['idx'])||empty($issetPtp['idx'])){
			$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모3한테 물어보세요.';
			$errOn=$this::errExport($errMsg);
		}
		$statusIDX=$issetPtp['statusIDX'];
		$status=$issetPtp['status'];

		if($statusIDX=='251401' || $statusIDX=='251402' || $statusIDX=='251403'){//종료건이니? 꺼지세요~
			$errMsg='이미 종료된 거래입니다. 새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모4한테 물어보세요. 종료 상태: '.$statusIDX.' : '.$status;
			$errOn=$this::errExport($errMsg);
		}
		// 숨고르기. 위에랑 똑같이 히헤히헤

		if($statusIDX!=$isStatusIDX){//? 꺼지세요~
			$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모7한테 물어보세요. statusIDX:'.$statusIDX.' isStatusIDX:'.$isStatusIDX;
			$errOn=$this::errExport($errMsg);
		}

		$BuyerPrice=$issetPtp['BuyerPrice'];
		$BuyerPrice_format=number_format($BuyerPrice);
		$SellerPrice=$issetPtp['SellerPrice'];
		$SellerPrice_format=number_format($SellerPrice);
		$BuyerName=$issetPtp['BuyerName'];
		$SellerName=$issetPtp['SellerName'];
		$typeName=$issetPtp['typeName'];
		$BuyerClientPtpAccount=$issetPtp['BuyerClientPtpAccount'];
		$amount_format=number_format($issetPtp['amount'],2);

		$BuyerClientIDX=$issetPtp['BuyerClientIDX'];
		$SellerClientIDX=$issetPtp['SellerClientIDX'];

		$ex='err...';
		if($btnType=='next'){
			if($statusIDX!='251201'){//다음 단계로!는 현재 상태가 구매자 예치완료! 상태일때만 가능하기때문에 이외에는 다 병맛임
				$errMsg='"다음 단계로" 는 현재 상태가 구매자 예치완료(251201) 상태일때만 가능합니다. 새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모5한테 물어보세요. 현재 상태: '.$statusIDX.' : '.$status;
				$errOn=$this::errExport($errMsg);
			}
			$nextStatus=251301;
			$db = static::getDB();
			$dbName= self::MainDBName;
			$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
				statusIDX = :changeStatus
				WHERE idx = :targetIDX
			");
			$update->bindValue(':changeStatus', $nextStatus);
			$update->bindValue(':targetIDX', $targetIDX);
			$update->execute();

			$logIDX=static::ClientLogInsert($nextStatus,$targetIDX,$SellerClientIDX);
			$ex='P2P 다음 단계로!';
			static::ClientLogExInsert($logIDX,0,0,$ex);

			$logIDX=static::ClientLogInsert($nextStatus,$targetIDX,$BuyerClientIDX);
			$ex='P2P 다음 단계로!';
			static::ClientLogExInsert($logIDX,0,0,$ex);

		}elseif($btnType=='cancel'){
			if($statusIDX=='251102'){//시작 마일리지가 있나 봄. 이거라면 좀 이상한거쥐 왜냐면 현재상태가 이 상태로 머물 수 있는 순간은 없자너
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모12한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}
			if($statusIDX!='251101' && $statusIDX!='251201' && $statusIDX!='251301'){
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모16한테 물어보세요. statusIDX: '.$statusIDX;
				$errOn=$this::errExport($errMsg);
			}

			if($statusIDX=='251101'){//시작 마일리지 입금신청 같이 됨
				//입금신청 뭐시깽이인지 확인
				//입금신청이 완료되어있따? 그럼 1분뒤에 새로고침해보라고! 스테이터스아이디엑스가 바뀌어있을테!니!까!
				//입금신청이 완료나 취소가 아니라고?
				//취소해버려!!!!!!!!!
				$getBuyerDepositStatus=ClientPeerToPeerMo::getBuyerDepositStatus($targetIDX);
				if(!isset($getBuyerDepositStatus['idx'])||empty($getBuyerDepositStatus['idx'])){
					$errMsg='고객의 입금신청 내역을 찾을 수 없습니다. 김준모31한테 물어보세요.';
					$errOn=$this::errExport($errMsg);
				}
				$BuyerClientMileageDepositIDX=$getBuyerDepositStatus['BuyerClientMileageDepositIDX'];
				$BuyerClientMileageDepositStatusIDX=$getBuyerDepositStatus['BuyerClientMileageDepositStatusIDX'];
				$BuyerClientMileageDepositStatus=$getBuyerDepositStatus['BuyerClientMileageDepositStatus'];

				// 203102              신청 자동
				// 203212              종료 취소(스태프가)
				// 203213              종료 취소(유저가)
				// 203215              종료 취소(잡취소)

				// XXX 203101              신청 수동
				// XXX 203103              신청 가상계좌
				// XXX 203104              신청 수동(보류)

				// XXX 203211              종료 취소(기타)
				// XXX 203214              종료 취소(가상계좌실패)

				// XXX 203201              종료 수동으로 성공
				// XXX 203202              종료 자동으로 성공
				// XXX 203203              종료 가상계좌 성공
				if(
					$BuyerClientMileageDepositStatusIDX!='203102'
					&& $BuyerClientMileageDepositStatusIDX!='203212'
					&& $BuyerClientMileageDepositStatusIDX!='203213'
					&& $BuyerClientMileageDepositStatusIDX!='203215'
				){
					$errMsg='고객의 입금신청 내역이 완료전(자동), 취소(스태프가), 취소(유저가), 취소(자동취소) 인 경우만 가능합니다. 지금 입금신청 상태는 '.$BuyerClientMileageDepositStatus.' (code '.$BuyerClientMileageDepositStatusIDX.') 입니다.';
					$errOn=$this::errExport($errMsg);
				}

				if($BuyerClientMileageDepositStatusIDX=='203102'){//지금 입금신청이 신청 자동이래~~ 취소하자~~

					$cancelStatus=203212;
					$db = static::getDB();
					$dbName= self::MainDBName;
					$update=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
						statusIDX = :cancelStatus
						WHERE idx = :BuyerClientMileageDepositIDX
					");
					$update->bindValue(':cancelStatus', $cancelStatus);
					$update->bindValue(':BuyerClientMileageDepositIDX', $BuyerClientMileageDepositIDX);
					$update->execute();
					$logIDX=static::ClientLogInsert($cancelStatus,$BuyerClientMileageDepositIDX,$BuyerClientIDX);
					$ex='P2P 취소로 인해 입금신청 취소';
					static::ClientLogExInsert($logIDX,0,0,$ex);
				}

				$changeStatus=251403;
				$db = static::getDB();
				$dbName= self::MainDBName;
				$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
					statusIDX = :changeStatus
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $changeStatus);
				$update->bindValue(':targetIDX', $targetIDX);
				$update->execute();

				$logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$SellerClientIDX);
				$ex='P2P 취소';
				static::ClientLogExInsert($logIDX,0,0,$ex);

				$logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$BuyerClientIDX);
				$ex='P2P 취소(마일리지 증차감 없음)';
				static::ClientLogExInsert($logIDX,0,0,$ex);

			}elseif($statusIDX=='251201' || $statusIDX=='251301'){//251201: 구매자 마일리지 차감 완료, 251301: 판매자 송금완
				//구매자한테 말리지를 돌려줘!!!!!!!!!!

				$changeStatus=251402;
				$nowTime=date("Y-m-d H:i:s");
				$db = static::getDB();
				$dbName= self::MainDBName;
				$stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
					(clientIDX,statusIDX,targetIDX,amount,createTime)
					VALUES
					(:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
				");
				$stat1->bindValue(':clientIDX', $BuyerClientIDX);
				$stat1->bindValue(':statusIDX', $changeStatus);
				$stat1->bindValue(':targetIDX', $targetIDX);
				$stat1->bindValue(':amount', $BuyerPrice);
				$stat1->bindValue(':createTime', $nowTime);
				$stat1->execute();
				$insertIDX = $db->lastInsertId();
				$logIDX=static::ClientLogInsert($changeStatus,$insertIDX,$BuyerClientIDX);
				$ex='P2P거래 구매자 거래대금 반환 ('.$BuyerPrice.'원)';
				static::ClientLogExInsert($logIDX,0,0,$ex);

				$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
					statusIDX = :changeStatus
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $changeStatus);
				$update->bindValue(':targetIDX', $targetIDX);
				$update->execute();

				$logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$SellerClientIDX);
				$ex='P2P 취소';
				static::ClientLogExInsert($logIDX,0,0,$ex);

				$logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$BuyerClientIDX);
				$ex='P2P 취소(마일리지 반환)';
				static::ClientLogExInsert($logIDX,0,0,$ex);
			}
		}elseif($btnType=='complete'){
			if($statusIDX=='251101'){//시작 마일리지 입금신청 같이 됨 이거라면 좀 이상한거쥐 왜냐면 뷰에서 내가 버튼을 숨켜놨거던
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모8한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}elseif($statusIDX=='251102'){//시작 마일리지가 있나 봄. 이거라면 좀 이상한거쥐 왜냐면 현재상태가 이 상태로 머물 수 있는 순간은 없자너
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모2한테 물어보세요.';
				$errOn=$this::errExport($errMsg);
			}elseif($statusIDX!='251201' && $statusIDX!='251301'){//251201: 구매자 마일리지 차감 완료, 251301: 판매자 송금완
				$errMsg='새로고침 후 다시 시도해주세요. 동일 증상 지속 시 김준모61한테 물어보세요. statusIDX: '.$statusIDX;
				$errOn=$this::errExport($errMsg);
			}else{
			}

			if($statusIDX=='251201' || $statusIDX=='251301'){//251201: 구매자 마일리지 차감 완료, 251301: 판매자 송금완
				//판매자한테 말리지를 줘!!!!

				$changeStatus=251401;
				$nowTime=date("Y-m-d H:i:s");
				$db = static::getDB();
				$dbName= self::MainDBName;
				$stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
					(clientIDX,statusIDX,targetIDX,amount,createTime)
					VALUES
					(:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
				");
				$stat1->bindValue(':clientIDX', $SellerClientIDX);
				$stat1->bindValue(':statusIDX', $changeStatus);
				$stat1->bindValue(':targetIDX', $targetIDX);
				$stat1->bindValue(':amount', $SellerPrice);
				$stat1->bindValue(':createTime', $nowTime);
				$stat1->execute();
				$insertIDX = $db->lastInsertId();
				$logIDX=static::ClientLogInsert($changeStatus,$insertIDX,$SellerClientIDX);
				$ex='P2P 거래 완료 판매자 거래대금 지급 ('.$SellerPrice.'원)';
				static::ClientLogExInsert($logIDX,0,0,$ex);

				$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
					statusIDX = :changeStatus
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $changeStatus);
				$update->bindValue(':targetIDX', $targetIDX);
				$update->execute();

				// $logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$SellerClientIDX);
				// $ex='P2P 완료(거래대금 지급)';
				// static::ClientLogExInsert($logIDX,0,0,$ex);

				$logIDX=static::ClientLogInsert($changeStatus,$targetIDX,$BuyerClientIDX);
				$ex='P2P 거래 완료';
				static::ClientLogExInsert($logIDX,0,0,$ex);
			}
		}
		$resultData = [
			'msg'=>$ex,
			'result'=>'t'
		];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;

	}


































































    // public function AESEncrypt($str)
    // {
    //     $iv = "OeKkc7062o4hMCV9";
    //     $key = "jqjlnRQH4HMSwaMBU5G6SpYicmda6p4U";
    //     return base64_encode(openssl_encrypt($str, "AES-256-CBC", $key, 5,  $iv));
    // }
    // public function AESDecrypt($str1)
    // {
    //     $iv = "OeKkc7062o4hMCV9";
    //     $key = "jqjlnRQH4HMSwaMBU5G6SpYicmda6p4U";
    //     return openssl_decrypt(base64_decode($str1), "AES-256-CBC", $key, 5,  $iv);
    // }

	//피투피 크론임 잡으로 옮길 예정
	//20240222 민잡에 넣어놨음
	public function crontestttttttttttttttttttttttttttt()
	{
		$ex='없데요.';
		$PtpNextCheckJob=ClientPeerToPeerMo::PtpNextCheckJob();
		if(isset($PtpNextCheckJob['idx'])&&!empty($PtpNextCheckJob['idx'])){
			$ClientPeerToPeerIDX=$PtpNextCheckJob['idx'];
			/*
			ClientPeerToPeer 테이블에서 251101 이면서 AAA 를 가진 ClientMileageDeposit 테이블에 상태가 완료처리되었다!(203201, 2, 3)

			ClientMileage 테이블 251201 로 인서트 하면서
			구매자 마일리지 차감(구매자 예치금)

			ClientLog 테이블 인서트

			ClientPeerToPeer 테이블 251201 업데이트

			ClientLog 테이블 인서트
			*/

			$SellerClientIDX=$PtpNextCheckJob['SellerClientIDX'];
			$BuyerClientIDX=$PtpNextCheckJob['BuyerClientIDX'];
			$BuyerFee=$PtpNextCheckJob['BuyerFee'];
			$rate=$PtpNextCheckJob['rate'];
			$amount=$PtpNextCheckJob['amount'];
			$totPrice=bcmul($rate, $amount,0);
			$BuyerPrice=bcadd($totPrice, $BuyerFee, 0);

			$changeStatus=251201;
			$nowTime=date("Y-m-d H:i:s");
			$db = static::getDB();
			$dbName= self::MainDBName;
			$stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
				(clientIDX,statusIDX,targetIDX,amount,createTime)
				VALUES
				(:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
			");
			$stat1->bindValue(':clientIDX', $BuyerClientIDX);
			$stat1->bindValue(':statusIDX', $changeStatus);
			$stat1->bindValue(':targetIDX', $ClientPeerToPeerIDX);
			$stat1->bindValue(':amount', $BuyerPrice);
			$stat1->bindValue(':createTime', $nowTime);
			$stat1->execute();
			$insertIDX = $db->lastInsertId();
			$logIDX=static::ClientLogInsert($changeStatus,$insertIDX,$BuyerClientIDX);
			$ex='P2P거래 구매자 에스크로 ('.$BuyerPrice.'원)';
			static::ClientLogExInsert($logIDX,0,0,$ex);
			//구매자 말리지 차감 끝

			$update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
				statusIDX = :changeStatus
				WHERE idx = :targetIDX
			");
			$update->bindValue(':changeStatus', $changeStatus);
			$update->bindValue(':targetIDX', $ClientPeerToPeerIDX);
			$update->execute();

			$logIDX=static::ClientLogInsert($changeStatus,$ClientPeerToPeerIDX,$SellerClientIDX);
			$ex='P2P거래 2단계 판매자';
			static::ClientLogExInsert($logIDX,0,0,$ex);
			$logIDX=static::ClientLogInsert($changeStatus,$ClientPeerToPeerIDX,$BuyerClientIDX);
			$ex='P2P거래 2단계 구매자(마일리지 차감 완료)';
			static::ClientLogExInsert($logIDX,0,0,$ex);
		}
		$resultData = ['msg'=>$ex,'result'=>'t'];
		$result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
		echo $result;
	}






	//현금영수증 1분잡 1-1번작업자 크론임 잡으로 옮길 예정
	public function CashReceiptCron11()
	{
		/*
		현영발행하기
		스태프 서버에서 1분잡 1-1번작업자
			API 디비 ClientCashReceipt 테이블에 일할거리를 쌓음
				- 지불대행은 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
				- 피투피는 완료되었으나 ClientCashReceipt 테이블에 없는 친구들

		startDate, endDate 포스트받는건 테스트땜시고 잡으로 옮길땐 지워야지 ㅎ.. (=노잡)
		*/
		// $startDate=date('Y-m-d H:i:s', strtotime('-3 minutes'));
		// $endDate=date('Y-m-d H:i:s');

		// $startDate=date('Y-m-d H:i:s', strtotime('-15 day'));// <- 페이크페이크페이크페이크페이크
		// $endDate=date('Y-m-d H:i:s');

		// $startDate='2024-01-29 10:54:01';// //20240206 buneul@naver.com 최시영 2900불 짜리부터 발행해야함
		// $endDate='2024-01-31 23:59:59';

		// $startDate='2024-01-29 10:54:01';// //20240206 buneul@naver.com 최시영 2900불 짜리부터 발행해야함
		// $endDate='2024-02-29 12:59:59';

		// $dataArr=array(
		// 	'startDate'=>$startDate,
		// 	'endDate'=>$endDate,
		// );

		//지불대행은 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		//지불대행은 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		// $dataPack=XxxJunmoxxXMo::GetMarketDepositLogForCashReceiptJob($dataArr);//이건 모든 지불대행 데이터에 ci가 있을때만 가능, 그래서 탈락
		// foreach ($dataPack as $item) {
		// 	$MarketDepositLogIDX=$item['idx'];
		// 	$TRAN_REQ_ID='MD'.$MarketDepositLogIDX.'T'.date('ymdHis');
		// 	$ReceiptAmt=($item['clientFee'])*1;
		// 	$ci='';
		// 	$clientIDX=0;
		// 	$clientName='';
		// 	$clientTel='';
		// 	$clientEmail='';
		// 	$ReceiptTypeNo='';
		// 	$ClientCashReceiptSettingIDX=0;
		// 	$insStatusIDX=811101;//기본 발행하지않음
		// 	$insEX='알수없음';
		// 	if(isset($item['ci']) && !empty($item['ci']) && strlen($item['ci'])>4){
		// 		$ci=$item['ci'];
		// 		$GetClientInfo=XxxJunmoxxXMo::GetClientInfoByCiForCashReceiptJob($ci);
		// 		if(isset($GetClientInfo['idx']) && !empty($GetClientInfo['idx']) && $GetClientInfo['idx']*1>1){
		// 			$clientIDX=$GetClientInfo['idx'];
		// 			$clientName=$GetClientInfo['clientName'];
		// 			$clientTel=$GetClientInfo['clientTel'];
		// 			$clientEmail=$GetClientInfo['clientEmail'];
		// 			$ReceiptTypeNo=$GetClientInfo['ReceiptTypeNo'];
		// 			if(strlen($ReceiptTypeNo)!=11){//없을리는 없겠지만은..
		// 				$ReceiptTypeNo=$clientTel;
		// 			}
		// 			if(isset($GetClientInfo['ClientCashReceiptSettingIDX']) && !empty($GetClientInfo['ClientCashReceiptSettingIDX']) && $GetClientInfo['ClientCashReceiptSettingIDX']*1>1){
		// 				$ClientCashReceiptSettingIDX=$GetClientInfo['ClientCashReceiptSettingIDX'];
		// 			}
		// 			$ClientCashReceiptSettingStatusIDX=$GetClientInfo['ClientCashReceiptSettingStatusIDX'];
		// 			$ClientCashReceiptSettingCreateTime=$GetClientInfo['ClientCashReceiptSettingCreateTime'];
		// 			if( $ClientCashReceiptSettingStatusIDX=='229101' || ($ClientCashReceiptSettingStatusIDX=='229201' && $ReceiptAmt>99999) ){
		// 				if($ClientCashReceiptSettingStatusIDX=='229101'){//229101받겠다
		// 					if($ReceiptAmt<100){//99원 이하라고? 받지말어
		// 						$insEX='소액이라발행안하겠음 '.$ReceiptAmt.'원';//기본수수료가 3000원인데 이럴리가 없겠지..
		// 					}else{
		// 						$insStatusIDX=811201;
		// 						$insEX='발행대기';
		// 					}
		// 				}else{//229201 안받겠다고 했는데 십만원보다 크다고?
		// 					$insStatusIDX=811201;
		// 					$insEX='발행거부했지만 강제발행대기. 발행금액:'.$ReceiptAmt.'원 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 				}
		// 			}else{
		// 				if($ClientCashReceiptSettingStatusIDX=='229201'){//229101받겠다
		// 					$insEX='발행거부 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 				}else{// 없다고..?
		// 					$insEX='ClientCashReceiptSetting이 없는것 같다.';// 왜 없는데..?
		// 				}
		// 			}
		// 		}else{//clientIDX가 없다!!!!!!!!!!!!!!!!!!!!!!!!
		// 			$insEX='clientIDX가 없는것 같다.';// 왜 없는데..?
		// 		}
		// 	}else{//씨아이가 없다!!!!!!!!!!!!!!!!!!!!!!!!
		// 		$insEX='ci가 없는것 같다.';// 왜 없는데..? 구바이인가..?
		// 	}
		// 	$nowTime=date("Y-m-d H:i:s");
		// 	$db = static::GetApiDB();
		// 	$dbName= self::EbuyApiDBName;
		// 	$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceipt
		// 		(
		// 			targetStatusIDX,targetIDX,statusIDX,ReceiptAmt,ClientCashReceiptSettingIDX,ReceiptTypeNo,clientIDX,clientTel,clientName,clientEmail,TRAN_REQ_ID,createTime,resultTime,result_msg
		// 		)
		// 		VALUES
		// 		(
		// 			:targetStatusIDX,:targetIDX,:statusIDX,:ReceiptAmt,:ClientCashReceiptSettingIDX,:ReceiptTypeNo,:clientIDX,:clientTel,:clientName,:clientEmail,:TRAN_REQ_ID,:createTime,:resultTime,:result_msg
		// 		)
		// 	");
		// 	$insert->bindValue(':targetStatusIDX', 906101);
		// 	$insert->bindValue(':targetIDX', $MarketDepositLogIDX);
		// 	$insert->bindValue(':statusIDX', $insStatusIDX);
		// 	$insert->bindValue(':ReceiptAmt', $ReceiptAmt);
		// 	$insert->bindValue(':ClientCashReceiptSettingIDX', $ClientCashReceiptSettingIDX);
		// 	$insert->bindValue(':ReceiptTypeNo', $ReceiptTypeNo);
		// 	$insert->bindValue(':clientIDX', $clientIDX);
		// 	$insert->bindValue(':clientTel', $clientTel);
		// 	$insert->bindValue(':clientName', $clientName);
		// 	$insert->bindValue(':clientEmail', $clientEmail);
		// 	$insert->bindValue(':TRAN_REQ_ID', $TRAN_REQ_ID);
		// 	$insert->bindValue(':createTime', $nowTime);
		// 	$insert->bindValue(':resultTime', $nowTime);
		// 	$insert->bindValue(':result_msg', $insEX);
		// 	$insert->execute();
		// 	$ClientCashReceiptIDX=$db->lastInsertId();
		// 	if($clientIDX*1>1){
		// 		$logIDX=static::ClientLogInsert($insStatusIDX,$ClientCashReceiptIDX,$clientIDX);
		// 		static::ClientLogExInsert($logIDX,0,0,$insEX);
		// 	}
		// }


		// $startDate='2024-01-29 10:54:01';// //20240206 buneul@naver.com 최시영 2900불 짜리부터 발행해야함
		// $endDate='2024-01-31 23:59:59';

		// $startDate='2024-01-29 10:54:01';// //20240206 buneul@naver.com 최시영 2900불 짜리부터 발행해야함
		// $endDate='2024-02-09 23:59:59';

		// $dataArr=array(
		// 	'startDate'=>$startDate,
		// 	'endDate'=>$endDate,
		// );
		// $dataPack=XxxJunmoxxXMo::GetClientMileageIsApiDepositForCashReceiptJob($dataArr);
		// foreach ($dataPack as $item) {
		// 	$insStatusIDX=811101;//기본 발행하지않음

		// 	$ClientMileageIDX=$item['idx'];
		// 	$clientIDX=$item['clientIDX'];
		// 	$MarketDepositLogIDX=$item['targetIDX'];

		// 	$TRAN_REQ_ID='MD'.$MarketDepositLogIDX.'T'.date('ymdHis');

		// 	$clientName=$item['clientName'];
		// 	$clientTel=$item['clientTel'];
		// 	$clientEmail=$item['clientEmail'];

		// 	$ReceiptTypeNo=$item['ReceiptTypeNo'];
		// 	$ClientCashReceiptSettingIDX=$item['ClientCashReceiptSettingIDX'];
		// 	$ClientCashReceiptSettingStatusIDX=$item['ClientCashReceiptSettingStatusIDX'];
		// 	if($ClientCashReceiptSettingStatusIDX!='229101'&&$ClientCashReceiptSettingStatusIDX!='229201'){
		// 		$ClientCashReceiptSettingStatusIDX='229101';
		// 	}
		// 	$ClientCashReceiptSettingCreateTime=$item['ClientCashReceiptSettingCreateTime'];
		// 	if(strlen($ReceiptTypeNo)!=11){//없을리는 없겠지만은..
		// 		$ReceiptTypeNo=$clientTel;
		// 	}
		// 	$ReceiptAmt=0;
		// 	$db     = static::GetApiDB();
		// 	$dbName = self::EbuyApiDBName;
		// 	$query = $db->prepare("SELECT clientFee
		// 	FROM $dbName.MarketDepositLog
		// 	WHERE idx = '$MarketDepositLogIDX'
		// 	AND NOT EXISTS (
		// 		SELECT 1
		// 		FROM $dbName.ClientCashReceipt
		// 		WHERE targetIDX = '$MarketDepositLogIDX'
		// 		AND targetStatusIDX= '906101'
		// 	)");
		// 	$query->execute();
		// 	$sel=$query->fetch(PDO::FETCH_ASSOC);
		// 	if(isset($sel['clientFee'])){
		// 		$ReceiptAmt=$sel['clientFee'];

		// 		$insEX='알수없음';
		// 		if( $ClientCashReceiptSettingStatusIDX=='229101' || ($ClientCashReceiptSettingStatusIDX=='229201' && $ReceiptAmt>99999) ){
		// 			if($ClientCashReceiptSettingStatusIDX=='229101'){//229101 받겠다
		// 				if($ReceiptAmt<100){//99원 이하라고? 받지말어
		// 					$insEX='소액이라발행안하겠음 '.$ReceiptAmt.'원';//기본수수료가 3000원인데 이럴리가 없겠지..
		// 				}else{
		// 					$insStatusIDX=811201;
		// 					$insEX='발행대기';
		// 				}
		// 			}else{//229201 안받겠다고 했는데 십만원보다 크다고?
		// 				$insStatusIDX=811201;
		// 				$insEX='발행거부했지만 강제발행대기. 발행금액:'.$ReceiptAmt.'원 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}
		// 		}else{
		// 			if($ClientCashReceiptSettingStatusIDX=='229201'){//229101받겠다
		// 				$insEX='발행거부 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}else{// 없다고..?
		// 				$insEX='ClientCashReceiptSetting이 없는것 같다.';// 왜 없는데..?
		// 			}
		// 		}

		// 		$nowTime=date("Y-m-d H:i:s");
		// 		$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceipt
		// 			(
		// 				targetStatusIDX,targetIDX,statusIDX,ReceiptAmt,ClientCashReceiptSettingIDX,ReceiptTypeNo,clientIDX,clientTel,clientName,clientEmail,TRAN_REQ_ID,createTime,resultTime,result_msg
		// 			)
		// 			VALUES
		// 			(
		// 				:targetStatusIDX,:targetIDX,:statusIDX,:ReceiptAmt,:ClientCashReceiptSettingIDX,:ReceiptTypeNo,:clientIDX,:clientTel,:clientName,:clientEmail,:TRAN_REQ_ID,:createTime,:resultTime,:result_msg
		// 			)
		// 		");
		// 		$insert->bindValue(':targetStatusIDX', 906101);
		// 		$insert->bindValue(':targetIDX', $MarketDepositLogIDX);
		// 		$insert->bindValue(':statusIDX', $insStatusIDX);
		// 		$insert->bindValue(':ReceiptAmt', $ReceiptAmt);
		// 		$insert->bindValue(':ClientCashReceiptSettingIDX', $ClientCashReceiptSettingIDX);
		// 		$insert->bindValue(':ReceiptTypeNo', $ReceiptTypeNo);
		// 		$insert->bindValue(':clientIDX', $clientIDX);
		// 		$insert->bindValue(':clientTel', $clientTel);
		// 		$insert->bindValue(':clientName', $clientName);
		// 		$insert->bindValue(':clientEmail', $clientEmail);
		// 		$insert->bindValue(':TRAN_REQ_ID', $TRAN_REQ_ID);
		// 		$insert->bindValue(':createTime', $nowTime);
		// 		$insert->bindValue(':resultTime', $nowTime);
		// 		$insert->bindValue(':result_msg', $insEX);
		// 		$insert->execute();
		// 		$ClientCashReceiptIDX=$db->lastInsertId();
		// 		if($clientIDX*1>1){
		// 			$logIDX=static::ClientLogInsert($insStatusIDX,$ClientCashReceiptIDX,$clientIDX);
		// 			static::ClientLogExInsert($logIDX,0,0,$insEX);
		// 		}
		// 	}
		// }
		//지불대행은 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		//지불대행은 완료되었으나 ClientCashReceipt 테이블에 없는 친구들










		//////////////////////////////////////////////// 현금영수증 2분잡 1-2번작업자 크론임 잡으로 옮길 예정
		//////////////////////////////////////////////// 현금영수증 2분잡 1-2번작업자 크론임 잡으로 옮길 예정
		/*
		스태프 서버에서 2분잡 1-2번작업자
			API 디비 ClientCashReceipt 테이블에 811301(발행중)은 없고 811201(발행대기중) 은 있다??
				811201(발행대기중) 이 녀석 발사!!!!
					결과값 고대로 ClientCashReceiptLog 여기에 쌓아!!
					ClientCashReceipt 업데이트 쳐!!!

		*/

		// $encryptedLogibpw=$this::AESEncrypt('dlqkdl5565@');
		// $encryptedPw=$this::AESEncrypt('0925');
		// $db     = static::GetApiDB();
		// $dbName = self::EbuyApiDBName;
		// $query = $db->prepare("SELECT
		// idx
		// ,targetIDX
		// ,ReceiptAmt
		// ,clientIDX
		// ,clientTel
		// ,clientName
		// ,clientEmail
		// ,ReceiptTypeNo
		// ,TRAN_REQ_ID
		// FROM $dbName.ClientCashReceipt
		// WHERE statusIDX = 811201
		// AND NOT EXISTS (
		//     SELECT 1
		//     FROM $dbName.ClientCashReceipt
		//     WHERE statusIDX = 811301
		// )
		// LIMIT 1
		// ");
		// $query->execute();
		// $sel=$query->fetch(PDO::FETCH_ASSOC);
		// if(isset($sel['idx'])){
		// 	$ClientCashReceiptIDX=$sel['idx'];
		// 	$clientIDX=$sel['clientIDX'];

		// 	$nowTime=date("Y-m-d H:i:s");
		// 	$nextStatus=811301;
		// 	$insEX='현금영수증 대기중 -> 발행중 상태로 업데이트되었습니다.';
		// 	$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
		// 		statusIDX = :changeStatus
		// 		,resultTime = :tmpTime
		// 		,result_msg = :tmpMsg
		// 		WHERE idx = :targetIDX
		// 	");
		// 	$update->bindValue(':changeStatus', $nextStatus);
		// 	$update->bindValue(':tmpTime', $nowTime);
		// 	$update->bindValue(':tmpMsg', $insEX);
		// 	$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
		// 	$update->execute();
		// 	if($clientIDX*1>1){
		// 		$logIDX=static::ClientLogInsert($nextStatus,$ClientCashReceiptIDX,$clientIDX);
		// 		static::ClientLogExInsert($logIDX,0,0,$insEX);
		// 	}

		// 	$targetIDX=$sel['targetIDX'];
		// 	$ReceiptAmt=$sel['ReceiptAmt'];
		// 	$clientTel=$sel['clientTel'];
		// 	$clientName=$sel['clientName'];
		// 	$clientEmail=$sel['clientEmail'];
		// 	$ReceiptTypeNo=$sel['ReceiptTypeNo'];
		// 	$TRAN_REQ_ID=$sel['TRAN_REQ_ID'];

		// 	$ReceiptType='1';
		// 	$GoodsName='거래 수수료';
		// 	$ReceiptSubNum='1588800568';
		// 	$ReceiptSubCoNm='이바이컴퍼니';
		// 	$ReceiptSubBossNm='이바이컴퍼니';
		// 	$ReceiptSubTel='15225565';
		// 	$curl = curl_init();
		// 	curl_setopt_array($curl, array(
		// 		CURLOPT_URL => 'https://api.mydatahub.co.kr/kwichub/Cash/Issue',
		// 		CURLOPT_RETURNTRANSFER => true,
		// 		CURLOPT_ENCODING => 'UTF-8',
		// 		CURLOPT_MAXREDIRS => 10,
		// 		CURLOPT_TIMEOUT => 0,
		// 		CURLOPT_FOLLOWLOCATION => true,
		// 		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		// 		CURLOPT_CUSTOMREQUEST => 'POST',
		// 		CURLOPT_POSTFIELDS =>'{
		// 			"ReceiptType" : "'.$ReceiptType.'",
		// 			"GoodsName" : "'.$GoodsName.'",
		// 			"ReceiptSubNum" : "'.$ReceiptSubNum.'",
		// 			"ReceiptSubCoNm" : "'.$ReceiptSubCoNm.'",
		// 			"ReceiptSubBossNm" : "'.$ReceiptSubBossNm.'",
		// 			"ReceiptSubTel" : "'.$ReceiptSubTel.'",
		// 			"ReceiptTypeNo" : "'.$ReceiptTypeNo.'",
		// 			"ReceiptAmt" : "'.$ReceiptAmt.'",
		// 			"TRAN_REQ_ID" : "'.$TRAN_REQ_ID.'",
		// 			"BuyerEmail" : "'.$clientEmail.'",
		// 			"BuyerTel" : "'.$clientTel.'",
		// 			"BuyerName" : "'.$clientName.'"
		// 		}',
		// 		CURLOPT_HTTPHEADER => array(
		// 			'Authorization: Token ab75a3f450024a119835de4352bc1fc60b5fdbd6',
		// 			'Content-Type: application/json;charset=UTF-8'
		// 		)
		// 	));
		// 	$response = curl_exec($curl);
		// 	curl_close($curl);
		// 	$json=json_decode($response, true);
		// 	$erro=$json["errMsg"];
		// 	$receiveJsonData=json_encode($json);
		// 	if($erro=="success"){
		// 		// echo json_encode($json["data"]);

		// 		$responseData=$json["data"];

		// 		$res_TID=$responseData["TID"];
		// 		$res_ReceiptAmt=$responseData["ReceiptAmt"];
		// 		$res_GoodsName=$responseData["GoodsName"];
		// 		$res_ReceiptType=$responseData["ReceiptType"];
		// 		$res_ReceiptTypeNo=$responseData["ReceiptTypeNo"];
		// 		$res_TRAN_REQ_ID=$responseData["TRAN_REQ_ID"];
		// 		$res_ReceiptSupplyAmt=$responseData["ReceiptSupplyAmt"];
		// 		$res_ReceiptVAT=$responseData["ReceiptVAT"];
		// 		$res_ReceiptServiceAmt=$responseData["ReceiptServiceAmt"];
		// 		$res_ReceiptTaxFreeAmt=$responseData["ReceiptTaxFreeAmt"];
		// 		$res_ReceiptSubNum=$responseData["ReceiptSubNum"];
		// 		$res_ReceiptSubCoNm=$responseData["ReceiptSubCoNm"];
		// 		$res_ReceiptSubBossNm=$responseData["ReceiptSubBossNm"];
		// 		$res_ReceiptSubTel=$responseData["ReceiptSubTel"];
		// 		$res_BuyerName=$responseData["BuyerName"];
		// 		$res_BuyerEmail=$responseData["BuyerEmail"];
		// 		$res_BuyerTel=$responseData["BuyerTel"];
		// 		$res_ResultCode=$responseData["ResultCode"];
		// 		$res_ResultMsg=$responseData["ResultMsg"];
		// 		$res_ResultTID=$responseData["ResultTID"];
		// 		$res_ResultMoid=$responseData["ResultMoid"];
		// 		$res_AuthCode=$responseData["AuthCode"];
		// 		$res_AuthDate=$responseData["AuthDate"];
		// 		$res_status_code=$responseData["status_code"];

		// 		if($res_ResultCode=='7001'){//정상!
		// 			$nextStatus=811401;
		// 		}else{// 7001 외 실패
		// 			$nextStatus=811402;
		// 		}

		// 		$nowTime=date("Y-m-d H:i:s");
		// 		$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
		// 			statusIDX = :changeStatus
		// 			,resultTime = :resultTime
		// 			,result_TID = :result_TID
		// 			,result_code = :result_code
		// 			,result_msg = :result_msg
		// 			WHERE idx = :targetIDX
		// 		");
		// 		$update->bindValue(':changeStatus', $nextStatus);
		// 		$update->bindValue(':resultTime', $nowTime);
		// 		$update->bindValue(':result_TID', $res_ResultTID);
		// 		$update->bindValue(':result_code', $res_ResultCode);
		// 		$update->bindValue(':result_msg', $res_ResultMsg);
		// 		$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
		// 		$update->execute();
		// 		if($clientIDX*1>1){
		// 			$logIDX=static::ClientLogInsert($nextStatus,$ClientCashReceiptIDX,$clientIDX);
		// 			static::ClientLogExInsert($logIDX,0,0,$res_ResultMsg);
		// 		}
		// 	}else{
		// 		$nextStatus=811403;
		// 		$nowTime=date("Y-m-d H:i:s");
		// 		$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
		// 			statusIDX = :changeStatus
		// 			,resultTime = :resultTime
		// 			WHERE idx = :targetIDX
		// 		");
		// 		$update->bindValue(':changeStatus', $nextStatus);
		// 		$update->bindValue(':resultTime', $nowTime);
		// 		$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
		// 		$update->execute();
		// 		if($clientIDX*1>1){
		// 			$logIDX=static::ClientLogInsert($nextStatus,$ClientCashReceiptIDX,$clientIDX);
		// 			static::ClientLogExInsert($logIDX,0,0,$res_ResultMsg);
		// 		}
		// 	}
		// 	$nowTime=date("Y-m-d H:i:s");
		// 	$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceiptFullData
		// 		(
		// 			ClientCashReceiptIDX,
		// 			fullData,
		// 			createTime
		// 		)
		// 		VALUES
		// 		(
		// 			:ClientCashReceiptIDX,
		// 			:fullData,
		// 			:createTime
		// 		)
		// 	");
		// 	$insert->bindValue(':ClientCashReceiptIDX', $ClientCashReceiptIDX);
		// 	$insert->bindValue(':fullData', $receiveJsonData);
		// 	$insert->bindValue(':createTime', $nowTime);
		// 	$insert->execute();
		// }
		//////////////////////////////////////////////// 현금영수증 2분잡 1-2번작업자 크론임 잡으로 옮길 예정
		//////////////////////////////////////////////// 현금영수증 2분잡 1-2번작업자 크론임 잡으로 옮길 예정

























		//피투피는 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		//피투피는 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		// $dataPack=XxxJunmoxxXMo::GetClientPeerToPeerForCashReceiptJob($dataArr);
  //       foreach ($dataPack as $item) {
		// 	$ClientPeerToPeerIDX=$item['idx'];

		// 	$Buyer_ReceiptAmt=($item['BuyerFee'])*1;
		// 	$BuyerClientIDX=$item['BuyerClientIDX'];

		// 	$Seller_ReceiptAmt=($item['SellerFee'])*1;
		// 	$SellerClientIDX=$item['SellerClientIDX'];

		// 	//Buyer 할께요~
		// 	//Buyer 할께요~
		// 	//Buyer 할께요~
		// 	$clientIDX=0;
		// 	$clientName='';
		// 	$clientTel='';
		// 	$clientEmail='';
		// 	$ReceiptTypeNo='';
		// 	$ClientCashReceiptSettingIDX=0;

		// 	$insStatusIDX=811101;//기본 발행하지않음
		// 	$insEX='알수없음';

		// 	$TRAN_REQ_ID='PB'.$ClientPeerToPeerIDX.'T'.date('ymdHis');

		// 	$ReceiptAmt=$Buyer_ReceiptAmt;
		// 	$GetClientInfo=XxxJunmoxxXMo::GetClientInfoByClientIDXForCashReceiptJob($BuyerClientIDX);
		// 	if(isset($GetClientInfo['idx']) && !empty($GetClientInfo['idx']) && $GetClientInfo['idx']*1>1){
		// 		$clientIDX=$GetClientInfo['idx'];
		// 		$clientName=$GetClientInfo['clientName'];
		// 		$clientTel=$GetClientInfo['clientTel'];
		// 		$clientEmail=$GetClientInfo['clientEmail'];
		// 		$ReceiptTypeNo=$GetClientInfo['ReceiptTypeNo'];
		// 		if(strlen($ReceiptTypeNo)!=11){//없을리는 없겠지만은..
		// 			$ReceiptTypeNo=$clientTel;
		// 		}

		// 		if(isset($GetClientInfo['ClientCashReceiptSettingIDX']) && !empty($GetClientInfo['ClientCashReceiptSettingIDX']) && $GetClientInfo['ClientCashReceiptSettingIDX']*1>1){
		// 			$ClientCashReceiptSettingIDX=$GetClientInfo['ClientCashReceiptSettingIDX'];
		// 		}
		// 		$ClientCashReceiptSettingStatusIDX=$GetClientInfo['ClientCashReceiptSettingStatusIDX'];
		// 		$ClientCashReceiptSettingCreateTime=$GetClientInfo['ClientCashReceiptSettingCreateTime'];

		// 		if(
		// 			$ClientCashReceiptSettingStatusIDX=='229101'
		// 			|| ($ClientCashReceiptSettingStatusIDX=='229201' && $ReceiptAmt>99999)
		// 		){
		// 			if($ClientCashReceiptSettingStatusIDX=='229101'){//229101받겠다
		// 				if($ReceiptAmt<100){//99원 이하라고? 받지말어
		// 					$insEX='소액이라발행안하겠음 '.$ReceiptAmt.'원';//기본수수료가 3000원인데 이럴리가 없겠지..
		// 				}else{
		// 					$insStatusIDX=811201;
		// 					$insEX='발행대기';
		// 				}
		// 			}else{//229201 안받겠다고 했는데 십만원보다 크다고?
		// 				$insStatusIDX=811201;
		// 				$insEX='발행거부했지만 강제발행대기. 발행금액:'.$ReceiptAmt.'원 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}
		// 		}else{
		// 			if($ClientCashReceiptSettingStatusIDX=='229201'){//229101받겠다
		// 				$insEX='발행거부 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}else{// 없다고..?
		// 				$insEX='ClientCashReceiptSetting이 없는것 같다.';// 왜 없는데..?
		// 			}
		// 		}
		// 	}else{//clientIDX 가 없다!!!!!!!!!!!!!!!!!!!!!!!!
		// 		$insEX='Buyer ClientIDX 가 없는것 같다.';// 왜 없는데..?
		// 	}

		// 	$nowTime=date("Y-m-d H:i:s");
		// 	$db = static::GetApiDB();
		// 	$dbName= self::EbuyApiDBName;
		// 	$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceipt
		// 		(
		// 			targetStatusIDX
		// 			,targetIDX
		// 			,statusIDX
		// 			,ReceiptAmt
		// 			,ClientCashReceiptSettingIDX
		// 			,ReceiptTypeNo
		// 			,clientIDX
		// 			,clientTel
		// 			,clientName
		// 			,clientEmail
		// 			,TRAN_REQ_ID
		// 			,createTime
		// 			,resultTime
		// 			,result_msg
		// 		)
		// 		VALUES
		// 		(
		// 			:targetStatusIDX
		// 			,:targetIDX
		// 			,:statusIDX
		// 			,:ReceiptAmt
		// 			,:ClientCashReceiptSettingIDX
		// 			,:ReceiptTypeNo
		// 			,:clientIDX
		// 			,:clientTel
		// 			,:clientName
		// 			,:clientEmail
		// 			,:TRAN_REQ_ID
		// 			,:createTime
		// 			,:resultTime
		// 			,:result_msg
		// 		)
		// 	");
		// 	$insert->bindValue(':targetStatusIDX', 251401);
		// 	$insert->bindValue(':targetIDX', $ClientPeerToPeerIDX);
		// 	$insert->bindValue(':statusIDX', $insStatusIDX);
		// 	$insert->bindValue(':ReceiptAmt', $ReceiptAmt);
		// 	$insert->bindValue(':ClientCashReceiptSettingIDX', $ClientCashReceiptSettingIDX);
		// 	$insert->bindValue(':ReceiptTypeNo', $ReceiptTypeNo);
		// 	$insert->bindValue(':clientIDX', $clientIDX);
		// 	$insert->bindValue(':clientTel', $clientTel);
		// 	$insert->bindValue(':clientName', $clientName);
		// 	$insert->bindValue(':clientEmail', $clientEmail);
		// 	$insert->bindValue(':TRAN_REQ_ID', $TRAN_REQ_ID);
		// 	$insert->bindValue(':createTime', $nowTime);
		// 	$insert->bindValue(':resultTime', $nowTime);
		// 	$insert->bindValue(':result_msg', $insEX);
		// 	$insert->execute();
		// 	$ClientCashReceiptIDX=$db->lastInsertId();
		// 	$logIDX=static::ClientLogInsert($insStatusIDX,$ClientCashReceiptIDX,$clientIDX);
		// 	static::ClientLogExInsert($logIDX,0,0,$insEX);


		// 	//Seller 할께요~
		// 	//Seller 할께요~
		// 	//Seller 할께요~
		// 	$clientIDX=0;
		// 	$clientName='';
		// 	$clientTel='';
		// 	$clientEmail='';
		// 	$ReceiptTypeNo='';
		// 	$ClientCashReceiptSettingIDX=0;

		// 	$insStatusIDX=811101;//기본 발행하지않음
		// 	$insEX='알수없음';

		// 	$TRAN_REQ_ID='PS'.$ClientPeerToPeerIDX.'T'.date('ymdHis');

		// 	$ReceiptAmt=$Seller_ReceiptAmt;
		// 	$GetClientInfo=XxxJunmoxxXMo::GetClientInfoByClientIDXForCashReceiptJob($SellerClientIDX);
		// 	if(isset($GetClientInfo['idx']) && !empty($GetClientInfo['idx']) && $GetClientInfo['idx']*1>1){
		// 		$clientIDX=$GetClientInfo['idx'];
		// 		$clientName=$GetClientInfo['clientName'];
		// 		$clientTel=$GetClientInfo['clientTel'];
		// 		$clientEmail=$GetClientInfo['clientEmail'];
		// 		$ReceiptTypeNo=$GetClientInfo['ReceiptTypeNo'];
		// 		if(strlen($ReceiptTypeNo)!=11){//없을리는 없겠지만은..
		// 			$ReceiptTypeNo=$clientTel;
		// 		}

		// 		if(isset($GetClientInfo['ClientCashReceiptSettingIDX']) && !empty($GetClientInfo['ClientCashReceiptSettingIDX']) && $GetClientInfo['ClientCashReceiptSettingIDX']*1>1){
		// 			$ClientCashReceiptSettingIDX=$GetClientInfo['ClientCashReceiptSettingIDX'];
		// 		}
		// 		$ClientCashReceiptSettingStatusIDX=$GetClientInfo['ClientCashReceiptSettingStatusIDX'];
		// 		$ClientCashReceiptSettingCreateTime=$GetClientInfo['ClientCashReceiptSettingCreateTime'];

		// 		if(
		// 			$ClientCashReceiptSettingStatusIDX=='229101'
		// 			|| ($ClientCashReceiptSettingStatusIDX=='229201' && $ReceiptAmt>99999)
		// 		){
		// 			if($ClientCashReceiptSettingStatusIDX=='229101'){//229101받겠다
		// 				if($ReceiptAmt<100){//99원 이하라고? 받지말어
		// 					$insEX='소액이라발행안하겠음 '.$ReceiptAmt.'원';//기본수수료가 3000원인데 이럴리가 없겠지..
		// 				}else{
		// 					$insStatusIDX=811201;
		// 					$insEX='발행대기';
		// 				}
		// 			}else{//229201 안받겠다고 했는데 십만원보다 크다고?
		// 				$insStatusIDX=811201;
		// 				$insEX='발행거부했지만 강제발행대기. 발행금액:'.$ReceiptAmt.'원 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}
		// 		}else{
		// 			if($ClientCashReceiptSettingStatusIDX=='229201'){//229101받겠다
		// 				$insEX='발행거부 거부일시:'.$ClientCashReceiptSettingCreateTime;
		// 			}else{// 없다고..?
		// 				$insEX='ClientCashReceiptSetting이 없는것 같다.';// 왜 없는데..?
		// 			}
		// 		}
		// 	}else{//clientIDX 가 없다!!!!!!!!!!!!!!!!!!!!!!!!
		// 		$insEX='Seller ClientIDX 가 없는것 같다.';// 왜 없는데..?
		// 	}

		// 	$nowTime=date("Y-m-d H:i:s");
		// 	$db = static::GetApiDB();
		// 	$dbName= self::EbuyApiDBName;
		// 	$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceipt
		// 		(
		// 			targetStatusIDX
		// 			,targetIDX
		// 			,statusIDX
		// 			,ReceiptAmt
		// 			,ClientCashReceiptSettingIDX
		// 			,ReceiptTypeNo
		// 			,clientIDX
		// 			,clientTel
		// 			,clientName
		// 			,clientEmail
		// 			,TRAN_REQ_ID
		// 			,createTime
		// 			,resultTime
		// 			,result_msg
		// 		)
		// 		VALUES
		// 		(
		// 			:targetStatusIDX
		// 			,:targetIDX
		// 			,:statusIDX
		// 			,:ReceiptAmt
		// 			,:ClientCashReceiptSettingIDX
		// 			,:ReceiptTypeNo
		// 			,:clientIDX
		// 			,:clientTel
		// 			,:clientName
		// 			,:clientEmail
		// 			,:TRAN_REQ_ID
		// 			,:createTime
		// 			,:resultTime
		// 			,:result_msg
		// 		)
		// 	");
		// 	$insert->bindValue(':targetStatusIDX', 251401);
		// 	$insert->bindValue(':targetIDX', $ClientPeerToPeerIDX);
		// 	$insert->bindValue(':statusIDX', $insStatusIDX);
		// 	$insert->bindValue(':ReceiptAmt', $ReceiptAmt);
		// 	$insert->bindValue(':ClientCashReceiptSettingIDX', $ClientCashReceiptSettingIDX);
		// 	$insert->bindValue(':ReceiptTypeNo', $ReceiptTypeNo);
		// 	$insert->bindValue(':clientIDX', $clientIDX);
		// 	$insert->bindValue(':clientTel', $clientTel);
		// 	$insert->bindValue(':clientName', $clientName);
		// 	$insert->bindValue(':clientEmail', $clientEmail);
		// 	$insert->bindValue(':TRAN_REQ_ID', $TRAN_REQ_ID);
		// 	$insert->bindValue(':createTime', $nowTime);
		// 	$insert->bindValue(':resultTime', $nowTime);
		// 	$insert->bindValue(':result_msg', $insEX);
		// 	$insert->execute();
		// 	$ClientCashReceiptIDX=$db->lastInsertId();
		// 	$logIDX=static::ClientLogInsert($insStatusIDX,$ClientCashReceiptIDX,$clientIDX);
		// 	static::ClientLogExInsert($logIDX,0,0,$insEX);
  //       }
		//피투피는 완료되었으나 ClientCashReceipt 테이블에 없는 친구들
		//피투피는 완료되었으나 ClientCashReceipt 테이블에 없는 친구들


	}

	//현금영수증 2분잡 1-2번작업자 크론임 잡으로 옮길 예정
	public function CashReceiptCron12()
	{
/*
스태프 서버에서 2분잡 1-2번작업자
	API 디비 ClientCashReceipt 테이블에 811301(발행중)은 없고 811201(발행대기중) 은 있다??
		811201(발행대기중) 이 녀석 발사!!!!
			결과값 고대로 ClientCashReceiptLog 여기에 쌓아!!
			ClientCashReceipt 업데이트 쳐!!!

*/
	}

	//현금영수증 취소발행 2분잡 2-1번작업자 크론임 잡으로 옮길 예정
	public function CashReceiptCron21()
	{
/*
스태프 서버에서 2분잡 2-1번작업자
	API 디비 ClientCashCancelReceipt 테이블에 811801(발행취소중)은 없고 811701(발행취소대기) 은 있다??
		811701(발행취소대기) 이 녀석 발사!!!!
			결과값 고대로 ClientCashCancelReceiptLog 여기에 쌓아!!
			ClientCashCancelReceipt 업데이트 쳐!!!!
*/
	}

















	public function MarketDepositLog_junmo()
	{
		// $db     = static::GetApiDB();
		// $dbName = self::EbuyApiDBName;
		// $query = $db->prepare("SELECT
		// 		idx
		// 		,exchangeRate
		// 		,amount
		// 		,clientFeePer
		// 		,clientFee
		// 	FROM ebuyAPI.MarketDepositLog_junmo
		// 	WHERE mileage<1
		// 	AND statusIDX = 906101
		// 	AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
		// ");
		// $query->execute();
		// $sel=$query->fetchAll(PDO::FETCH_ASSOC);
		// foreach ($sel as $key) {
		// 	$idx=$key['idx'];
		// 	$exchangeRate=$key['exchangeRate'];
		// 	$amount=$key['amount'];
		// 	$clientFeePer=$key['clientFeePer'];
		// 	$clientFee=$key['clientFee'];
		// 	echo '<br />idx -------------------'.$idx;
		// 	echo '<br />exchangeRate ----------'.$exchangeRate;
		// 	echo '<br />amount ----------------'.$amount;
		// 	echo '<br />clientFeePer ----------'.$clientFeePer;
		// 	echo '<br />clientFee ------------'.$clientFee;
		// 	echo '<br /><br />';
		// }
		echo '뭐세요..?';
	}
}