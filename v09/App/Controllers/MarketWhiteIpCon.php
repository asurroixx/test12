<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketWhiteIpMo;
use App\Models\PortalLogMo;
use PDO;

/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketWhiteIpCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$marketPack=MarketMo::GetMarketAllListData();
		$renderData=['marketPack'=>$marketPack];
		View::renderTemplate('page/marketWhiteIp/marketWhiteIp.html',$renderData);
	}

	public function DataTableListLoad($data=null)
	{
		$dataPack=MarketWhiteIpMo::GetDatatableListData();
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	public function DetailFormLoad($data=null)
	{
		if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=MarketWhiteIpMo::GetDetailData($targetIDX);
            // $filePack=FilesMo::GetFileData($targetIDX);
        }else if($pageType=='ins'){
            $dataPack='';
            $targetIDX='';
            $filePack='';
        }
        $renderData=[
            //ins
            'pageType'=>$pageType,
            //
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            // 'filePack'=>$filePack,
        ];
        View::renderTemplate('page/marketWhiteIp/marketWhiteIpDetail.html',$renderData);
	}

    public function statusUpdate($data=null){
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['nowStatusIDX'])||empty($_POST['nowStatusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $targetIDX=$_POST['targetIDX'];
        $nowStatusIDX=$_POST['nowStatusIDX'];
        $statusIDX=$_POST['statusIDX'];
        $statusVal=$_POST['statusVal'];

        $issetData=MarketWhiteIpMo::GetTargetData($targetIDX);
        if(!isset($issetData['idx'])&&empty($issetData['idx'])){
            $errMsg='타겟 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $arr=['targetIDX'=>$targetIDX,'statusIDX'=>$nowStatusIDX];
        $getLogManager=PortalLogMo::GetMarketManagerInfoData($arr);
        if(!isset($getLogManager['idx'])&&empty($getLogManager['idx'])){
            $errMsg='타겟 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $managerEmail=$getLogManager['managerEmail'];
        $createTime = date("Y-m-d H:i:s");
        $cdateTime = new \DateTime($createTime);
        $cformattedDate = $cdateTime->format('M.d, Y, \a\t g:i:s A');
        $step1=0;

        $targetStatusIDX=$issetData['statusIDX'];
        $targetStatusVal=$issetData['statusVal'];
        $ipAddress=$issetData['ipAddress'];
        $marketName=$issetData['marketName'];

        if($statusIDX!=$targetStatusIDX){
            $step1=1;
            $ex='신청IP가 '.$targetStatusVal.'에서 '.$statusVal.'(으)로 변경됐습니다.';
            $logIDX=$this->PortalLogInsert($statusIDX,$targetIDX);
            $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.MarketWhiteIp SET
            statusIDX=:statusIDX
            WHERE idx=:idx
        ");
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':idx', $targetIDX);
        $stat1->execute();

        $marketIDX=$issetData['marketIDX'];

        // 이메일을 보내주자
        $emailParamVal = [
            'marketname'=>$marketName,
            'manageremail'=>$managerEmail,
            'time'=>$cformattedDate.' (UTC)',
            'ip'=>$ipAddress,
        ];
        $emailParam=[
            'targetIDX'=>$targetIDX,
            'statusIDX'=>$statusIDX,
            'marketIDX'=>$marketIDX,
            'param'=>$emailParamVal,
            'type'=>'group'
        ];
        StaffEmailCon::sendEmailToPortal($emailParam);

        $alarmParam=[
            'targetIDX'=>$targetIDX,
            'statusIDX'=>$statusIDX,
            'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
            'param'=>[]
        ];
        PortalAlarmCon::sendAlarmToPortal($alarmParam);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;



    }

}