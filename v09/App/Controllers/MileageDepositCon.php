<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\ClientMileageDepositMo;
use App\Models\ClientDetailMo;
use App\Models\StatusMo;
use App\Models\EbuyBankLogMo;
use App\Models\StaffMo;


use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MileageDepositCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$bankPack=EbuyBankMo::GetEbuyBankData();
        $statusPack=StatusMo::GetMileageDepositSorting();
		$renderData=['bankPack'=>$bankPack,'statusPack'=>$statusPack];
		View::renderTemplate('page/mileageDeposit/mileageDeposit.html',$renderData);
	}//렌더

    //MileageDepositCon 날짜별 리셋 데이터
    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=ClientMileageDepositMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //MileageDepositCon 솔팅별 리셋 데이터
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['startDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $columnsVal=$_POST['columnsVal'];
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'columnsVal'=>$columnsVal,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=ClientMileageDepositMo::GetTotalSortingData($dataArr);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageDeposit.html 해당 거래 디테일 로드
    public function MileageDepositDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $clientIDX=$_POST['clientIDX'];
        $dataPack=ClientMileageDepositMo::GetDepositDetail($targetIDX);
        $clientPack=ClientDetailMo::GetTotalClientMileageDeposit($clientIDX);
        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'clientIDX'=>$clientIDX,
            'dataPack'=>$dataPack,
            'clientPack'=>$clientPack,
            'getMileage'=>$getMileage,
        ];
        View::renderTemplate('page/mileageDeposit/mileageDepositDetail.html',$renderData);
    }

    //mileageDeposit.html 컨펌로드
    public function ConfirmFormLoad($data=null)
    {
        $dataPack=EbuyBankLogMo::GetEbuyBankLogData();
        $renderData=['dataPack'=>$dataPack];
        View::renderTemplate('page/mileageDeposit/mileageConfirm.html',$renderData);
    }

    //mileageDeposit.html 유저 공통 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

    //mileageDeposit.html 스크래핑 데이터테이블 리스트 로드
    public function scrapingDataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=EbuyBankLogMo::GetScrapingListLoad($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


	//mileageDeposit.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );

        $dataPack=ClientMileageDepositMo::GetClientMileageDepositData($dataArr);

        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageDeposit.html 수동신청으로 status 업데이트
    public function ChangeManualStatus()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['changeStatus'])||empty($_POST['changeStatus'])){
            $errMsg='changeStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $changeStatus=$_POST['changeStatus'];
        $dataPack=ClientMileageDepositMo::GetTargetDataPack($targetIDX);
        if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
            $statusIDX=$dataPack['statusIDX'];
        }
        if($statusIDX!='203101'&&$statusIDX!='203102'&&$statusIDX!='203103'&&$statusIDX!='203104'){
            $errMsg='변경할 수 있는 상태가 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($statusIDX=='203101'){
            $errMsg='이미 수동처리 된 입금건입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
            statusIDX='$changeStatus'
        WHERE idx='$targetIDX'
        ");
        $stat1->execute();


        $type = 'web';
        $staffIDX = '';
        $createTime=date("Y-m-d H:i:s");
        $ipAddress = $this->GetIPaddress();
        if(isset($_POST['type'])||!empty($_POST['type'])){
            $type = $_POST['type'];
            $staffIDX = $_POST['staffIDX'];
        }

        if($type == 'app'){
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                VALUES
                (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':statusIDX', $changeStatus);
            $stat2->bindValue(':targetIDX', $targetIDX);
            $stat2->bindValue(':clientIDX', 0);
            $stat2->bindValue(':loginIDX', $staffIDX);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', $ipAddress);
            $stat2->execute();
        }else{
            static::ClientLogInsert($changeStatus,$targetIDX,0);
        }


        // //소켓 데이터테이블
        // $getStaffList=StaffMo::GetStaffList();
        // $staffPack = [];
        // foreach ($getStaffList as $key) {
        //     $staffPack[] = $key['idx'];
        // }
        // $DomainUri=self::DomainUri;
        // $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
        // $uri = $DomainUri.':16001/statusUpdate';
        // self::curlSend($dataPack,$uri);
        // //소켓 데이터테이블
        $socketTable=$this->socketTable($targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageDeposit.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['valueIDX'])||empty($_POST['valueIDX'])){
            $errMsg='valueIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $dataArr='';
        if(isset($_POST['dataArr'])||!empty($_POST['dataArr'])){
            $dataArr=$_POST['dataArr'];
        }
        $targetIDX=$_POST['targetIDX'];
        if($dataArr!=''){
            $this->EbuyBankLogUpdate($dataArr,$targetIDX);
        }
        $dataPack=ClientMileageDepositMo::GetTargetDataPack($targetIDX);
        if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
        	$statusIDX=$dataPack['statusIDX'];
        	$amount=$dataPack['amount'];
        	$clientIDX=$dataPack['clientIDX'];
            $rn=$dataPack['rn'];
        }
        if($statusIDX!='203101'&&$statusIDX!='203102'&&$statusIDX!='203103'&&$statusIDX!='203104'){
        	$errMsg='변경할 수 있는 상태가 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }




        $valueIDX=$_POST['valueIDX'];
        $createTime=date("Y-m-d H:i:s");
        $ipAddress = $this->GetIPaddress();

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
            statusIDX='$valueIDX',
            completeTime='$createTime'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        if($valueIDX=='203201'||$valueIDX=='203202'||$valueIDX=='203203'){
        	$amount = str_replace(',', '', $amount); // 쉼표(,)를 제거 (결과: "85555")
			$amountAsInt = intval($amount); // 정수로 변환
	        $stat2=$db->prepare("INSERT INTO $dbName.ClientMileage
	            (clientIDX,statusIDX,targetIDX,amount,createTime)
	            VALUES
	            ('$clientIDX','$valueIDX','$targetIDX','$amountAsInt','$createTime')
	        ");
	        $stat2->execute();
        }

        $type = 'web';
        $staffIDX = '';
        if(isset($_POST['type'])||!empty($_POST['type'])){
            $type = $_POST['type'];
            $staffIDX = $_POST['staffIDX'];
        }
        if($type == 'app'){
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                VALUES
                (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':statusIDX', $valueIDX);
            $stat2->bindValue(':targetIDX', $targetIDX);
            $stat2->bindValue(':clientIDX', $clientIDX);
            $stat2->bindValue(':loginIDX', $staffIDX);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', $ipAddress);
            $stat2->execute();
        }else{
            static::ClientLogInsert($valueIDX,$targetIDX,$clientIDX);
        }
        // static::ClientLogInsert($valueIDX,$targetIDX,$clientIDX);
         //유저에게 알람을 쏴주자
        $alarmArr=[
            'clientIDX'=>$clientIDX,
            'statusIDX'=>$valueIDX,
            'targetIDX'=>$targetIDX,
            'param'=>['amount'=>$amount]
        ];


        $AppMainIoUri= self::AppMainIoUri;
        $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
        static::sendCurl($alarmArr,$AppMainIoAddr);

        //소켓 데이터테이블
        $socketTable=$this->socketTable($targetIDX);

        //앱서버 화면전환 소켓
        $scrParam=[
            'rn'=>$rn,
            'statusIDX'=>$valueIDX,
        ];
        $AppMainIoAddr =$AppMainIoUri.'/rechargeResultAlarm';
        static::sendCurl($scrParam,$AppMainIoAddr);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

        // $dataPack2=ClientMileageDepositMo::tmpTargetData($targetIDX);
        // if(isset($dataPack2['idx'])&&!empty($dataPack2['idx'])){
        //     $migrationIDX=$dataPack2['migrationIDX'];
        //     if($migrationIDX != 0){
        //         if($valueIDX=='203201'||$valueIDX=='203202'||$valueIDX=='203203'){
        //             $field = 'https://partners-api.ebuycompany.com/MagApi/depositComplete';
        //         }else if($valueIDX=='203211'||$valueIDX=='203212'||$valueIDX=='203213'||$valueIDX=='203214'||$valueIDX=='203215'){
        //             $field = 'https://partners-api.ebuycompany.com/MagApi/depositCancel';
        //         }
        //         //구바이에도 보내자
        //         $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount;
        //         // $nextStepUri='https://partners-api.ebuycompany.com/MagApi/'.$fieldName;

        //         $curl = curl_init();
        //         curl_setopt($curl, CURLOPT_REFERER, "$field");
        //         curl_setopt_array($curl, array(
        //             CURLOPT_URL => "$field",
        //             CURLOPT_RETURNTRANSFER => true,
        //             CURLOPT_ENCODING => "",
        //             CURLOPT_MAXREDIRS => 10,
        //             CURLOPT_TIMEOUT => 30,
        //             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //             CURLOPT_CUSTOMREQUEST => "POST",
        //             CURLOPT_POSTFIELDS => "$postFields",
        //             CURLOPT_HTTPHEADER => array(
        //                 "cache-control: no-cache",
        //                 "content-type: application/x-www-form-urlencoded"
        //             ),
        //         ));
        //         $response = curl_exec($curl);
        //         $err = curl_error($curl);
        //         curl_close($curl);
        //     }
        // }







    }

    //mileageDeposit.html 전체 승인 status 업데이트 1019 일단 주석 > 승인은 전체승인이 없을거같음
    // public function AllStatusUpdate()
    // {
    //     if(!isset($_POST['tmpData'])||empty($_POST['tmpData'])){
    //         $errMsg='tmpData 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $tmpData=$_POST['tmpData'];
    //     $completeCount=0;
    //     foreach ($tmpData as $key) {
    //         $targetIDX=$key['idx'];
    //         $valueIDX=$key['valueIDX'];
    //         $dataPack=ClientMileageDepositMo::GetTargetDataPack($targetIDX);
    //         if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
    //             $statusIDX=$dataPack['statusIDX'];
    //             $amount=$dataPack['amount'];
    //             $clientIDX=$dataPack['clientIDX'];
    //         }
    //         if($statusIDX!='203101'&&$statusIDX!='203102'&&$statusIDX!='203103'){
    //             $errMsg='변경할 수 있는 상태가 아닙니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }

    //         $db = static::getDB();
    //         $dbName= self::MainDBName;
    //         $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
    //             statusIDX='$valueIDX'
    //             WHERE idx='$targetIDX'
    //         ");
    //         $stat1->execute();

    //         if($valueIDX=='203201'||$valueIDX=='203202'||$valueIDX=='203203'){
    //             $amount = str_replace(',', '', $amount); // 쉼표(,)를 제거 (결과: "85555")
    //             $amountAsInt = intval($amount); // 정수로 변환
    //             $createTime=date("Y-m-d H:i:s");
    //             $stat2=$db->prepare("INSERT INTO $dbName.ClientMileage
    //                 (clientIDX,statusIDX,targetIDX,amount,createTime)
    //                 VALUES
    //                 ('$clientIDX','$valueIDX','$targetIDX','$amountAsInt','$createTime')
    //             ");
    //             $stat2->execute();
    //         }


    //         static::ClientLogInsert($valueIDX,$targetIDX,$clientIDX);

    //          //유저에게 알람을 쏴주자
    //         $alarmArr=[
    //             'clientIDX'=>$clientIDX,
    //             'statusIDX'=>$valueIDX,
    //             'targetIDX'=>$targetIDX,
    //             'param'=>['amount'=>$amount]
    //         ];
    //         static::appPushAlarm($alarmArr);
    //     }
    //     $getStaffList=StaffMo::GetStaffList();
    //     $staffPack = [];
    //     foreach ($getStaffList as $key) {
    //         $staffPack[] = $key['idx'];
    //     }
    //     $DomainUri=self::DomainUri;
    //     $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
    //     $uri = $DomainUri.':16001/statusUpdate';
    //     self::curlSend($dataPack,$uri);


    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //mileageDeposit.html 입금 은행 로그 업데이트
    public function EbuyBankLogUpdate($dataArr,$targetIDX)
    {
        $totalAmount=$dataArr[0]['totalAmount'];
        $depositPack=ClientMileageDepositMo::GetTargetDataPack($targetIDX);
        if(isset($depositPack['idx'])&&!empty($depositPack['idx'])){
            $depositTargetAmount=$depositPack['amount'];
            $clientName=$depositPack['clientName'];
        }
        if($totalAmount!=$depositTargetAmount){
            $errMsg='총 금액이 입금한 금액과 다릅니다.';
            $errOn=$this::errExport($errMsg);
        }

        foreach ($dataArr as $key) {
            $idx=$key['idx'];
            $amount=$key['amount'];
            $clientAccountHolder=$key['clientAccountHolder'];
            $tradeTime=$key['tradeTime'];
            // $totalAmount=$key['totalAmount'];
            $dataPack=EbuyBankLogMo::GetEbuyBankLogTargetData($idx);
            if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
                $targetAmount=$dataPack['amount'];
            }

            // if($clientName!=$clientAccountHolder){
            //     $errMsg='예금주 명이 상이합니다.';
            //     $errOn=$this::errExport($errMsg);
            // }

            /*금액*/

            if($amount!=$targetAmount){
                $errMsg='금액이 상이합니다.';
                $errOn=$this::errExport($errMsg);
            }

            /*금액*/
            $apiDb = static::GetApiDB();
            $apiDbName= self::EbuyApiDBName;
            $stat1=$apiDb->prepare("UPDATE $apiDbName.EbuyBankLog SET
                statusIDX=101102,
                targetIDX='$targetIDX'
                WHERE idx='$idx'
            ");
            $stat1->execute();
        }

    }

    //mileageDeposit.html 마지막 인서트된 데이터 가져오기
    public function GetTargetNodeData($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMileageDepositMo::GetTargetNodeData($targetIDX);
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageDeposit.html 해당 스크래핑 가져오기
    public function GetTargetScrapingData($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=EbuyBankLogMo::GetTargetScrapingData($targetIDX);
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}