<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMileageWithdrawalMo;
use App\Models\ClientDetailMo;
use App\Models\StatusMo;
use App\Models\StaffMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MileageWithdrawalCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$statusPack=StatusMo::GetMileageWithdrawalSorting();
		$renderData=['statusPack'=>$statusPack];
		View::renderTemplate('page/mileageWithdrawal/mileageWithdrawal.html',$renderData);
	}//렌더

    //mileageWithdrawalCon 날짜별 리셋 데이터
    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=ClientMileageWithdrawalMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageWithdrawalCon 솔팅별 리셋 데이터
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['startDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $columnsVal=$_POST['columnsVal'];
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'columnsVal'=>$columnsVal,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=ClientMileageWithdrawalMo::GetTotalSortingData($dataArr);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageWithdrawal.html 해당 거래 디테일 로드
    public function MileageWithdrawalDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $clientIDX=$_POST['clientIDX'];
        $dataPack=ClientMileageWithdrawalMo::GetWithdrawalDetail($targetIDX);
        $clientPack=ClientDetailMo::GetTotalClientMileageWithDrawal($clientIDX);
        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'clientIDX'=>$clientIDX,
            'dataPack'=>$dataPack,
            'clientPack'=>$clientPack,
            'getMileage'=>$getMileage,
        ];
        View::renderTemplate('page/mileageWithdrawal/mileageWithdrawalDetail.html',$renderData);
    }

    //mileageWithdrawal.html 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

	//mileageWithdrawal.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=ClientMileageWithdrawalMo::GetClientMileageWithdrawalData($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageWithdrawal.html 수동신청으로 status 업데이트
    public function ChangeManualStatus()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['changeStatus'])||empty($_POST['changeStatus'])){
            $errMsg='changeStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $changeStatus=$_POST['changeStatus'];
        $dataPack=ClientMileageWithdrawalMo::GetTargetDataPack($targetIDX);
        if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
            $statusIDX=$dataPack['statusIDX'];
        }
        if($statusIDX!='204102'){
            $errMsg='변경할 수 있는 상태가 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($statusIDX=='204101'){
            $errMsg='이미 수동처리 된 출금건입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientMileageWithdrawal SET
            statusIDX='$changeStatus'
        WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        // $stat2=$db->prepare("UPDATE $dbName.ClientMileage SET
        //     statusIDX='$changeStatus'
        // WHERE targetIDX='$targetIDX'
        // ");
        // $stat2->execute();


        static::ClientLogInsert($changeStatus,$targetIDX,0);
        //소켓 데이터테이블
        // $getStaffList=StaffMo::GetStaffList();
        // $staffPack = [];
        // foreach ($getStaffList as $key) {
        //     $staffPack[] = $key['idx'];
        // }
        // $DomainUri=self::DomainUri;
        // $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
        // $uri = $DomainUri.':16001/statusUpdate';
        // self::curlSend($dataPack,$uri);
        //소켓 데이터테이블
        $socketTable=$this->socketTable($targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //mileageWithdrawal.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['valueIDX'])||empty($_POST['valueIDX'])){
            $errMsg='valueIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMileageWithdrawalMo::GetTargetDataPack($targetIDX);
        if(!isset($dataPack['idx'])||empty($dataPack['idx'])){
            $errMsg='비정상적인 접근입니다.';
            $errOn=$this::errExport($errMsg);
        }

    	$statusIDX=$dataPack['statusIDX'];
    	$amount=$dataPack['amount'];
        $fee=$dataPack['fee'];
    	$clientIDX=$dataPack['clientIDX'];
        $totalAmount=$amount+$fee;
        if($statusIDX!='204101'&&$statusIDX!='204102'){
        	$errMsg='변경할 수 있는 상태가 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }
        $valueIDX=$_POST['valueIDX'];
        $createTime=date("Y-m-d H:i:s");

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientMileageWithdrawal SET
            statusIDX='$valueIDX',
            completeTime='$createTime'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();
        if($valueIDX=='204211'||$valueIDX=='204212'){
	        $stat2=$db->prepare("INSERT INTO $dbName.ClientMileage
	            (clientIDX,statusIDX,targetIDX,amount,createTime)
	            VALUES
	            ('$clientIDX','$valueIDX','$targetIDX','$totalAmount','$createTime')
	        ");
	        $stat2->execute();
        }
        static::ClientLogInsert($valueIDX,$targetIDX,$clientIDX);
        //유저에게 알람을 쏴주자

        $alarmArr=[
            'clientIDX'=>$clientIDX,
            'statusIDX'=>$valueIDX,
            'targetIDX'=>$targetIDX,
            'param'=>['amount'=>$amount]
        ];
        $AppMainIoUri= self::AppMainIoUri;
        $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
        static::sendCurl($alarmArr,$AppMainIoAddr);

        //소켓 데이터테이블
        // $getStaffList=StaffMo::GetStaffList();
        // $staffPack = [];
        // foreach ($getStaffList as $key) {
        //     $staffPack[] = $key['idx'];
        // }
        // $DomainUri=self::DomainUri;
        // $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
        // $uri = $DomainUri.':16001/statusUpdate';
        // self::curlSend($dataPack,$uri);
        //소켓 데이터테이블
        $socketTable=$this->socketTable($targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

        // $dataPack2=ClientMileageWithdrawalMo::tmpTargetData($targetIDX);
        // if(isset($dataPack2['idx'])&&!empty($dataPack2['idx'])){
        //     $migrationIDX=$dataPack2['migrationIDX'];
        //     if($migrationIDX != 0){
        //         if($valueIDX=='204201'||$valueIDX=='204202'){
        //             $field = 'https://partners-api.ebuycompany.com/MagApi/withdrawalComplete';
        //         }else if($valueIDX=='204211'||$valueIDX=='204212'){
        //             $field = 'https://partners-api.ebuycompany.com/MagApi/withdrawalCancel';
        //         }
        //         //구바이에도 보내자
        //         $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount.'&fee='.$fee;
        //         // $nextStepUri='https://partners-api.ebuycompany.com/MagApi/'.$fieldName;

        //         $curl = curl_init();
        //         curl_setopt($curl, CURLOPT_REFERER, "$field");
        //         curl_setopt_array($curl, array(
        //             CURLOPT_URL => "$field",
        //             CURLOPT_RETURNTRANSFER => true,
        //             CURLOPT_ENCODING => "",
        //             CURLOPT_MAXREDIRS => 10,
        //             CURLOPT_TIMEOUT => 30,
        //             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //             CURLOPT_CUSTOMREQUEST => "POST",
        //             CURLOPT_POSTFIELDS => "$postFields",
        //             CURLOPT_HTTPHEADER => array(
        //                 "cache-control: no-cache",
        //                 "content-type: application/x-www-form-urlencoded"
        //             ),
        //         ));
        //         $response = curl_exec($curl);
        //         $err = curl_error($curl);
        //         curl_close($curl);
        //     }
        // }



    }

    //mileageWithdrawal.html 전체 승인 status 업데이트
    public function AllStatusUpdate()
    {
        if(!isset($_POST['tmpData'])||empty($_POST['tmpData'])){
            $errMsg='tmpData 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $tmpData=$_POST['tmpData'];
        foreach ($tmpData as $key) {
            $targetIDX=$key['idx'];
            $valueIDX=$key['valueIDX'];

            $dataPack=ClientMileageWithdrawalMo::GetTargetDataPack($targetIDX);
            if(isset($dataPack['idx'])&&!empty($dataPack['idx'])){
                $statusIDX=$dataPack['statusIDX'];
                $amount=$dataPack['amount'];
                $clientIDX=$dataPack['clientIDX'];
                $fee=$dataPack['fee'];

                $totalAmount=$amount+$fee;
            }
            if($statusIDX != '204101' && $statusIDX != '204102'){
                $errMsg='변경할 수 있는 상태가 아닙니다.';
                $errOn=$this::errExport($errMsg);
            }

            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat1=$db->prepare("UPDATE $dbName.ClientMileageWithdrawal SET
                statusIDX='$valueIDX'
                WHERE idx='$targetIDX'
            ");
            $stat1->execute();

            if($valueIDX=='204211'||$valueIDX=='204212'){
                $totalAmount = str_replace(',', '', $totalAmount); // 쉼표(,)를 제거 (결과: "85555")
                $amountAsInt = intval($totalAmount); // 정수로 변환
                $createTime=date("Y-m-d H:i:s");
                $stat2=$db->prepare("INSERT INTO $dbName.ClientMileage
                    (clientIDX,statusIDX,targetIDX,amount,createTime)
                    VALUES
                    ('$clientIDX','$valueIDX','$targetIDX','$amountAsInt','$createTime')
                ");
                $stat2->execute();
            }


            static::ClientLogInsert($valueIDX,$targetIDX,$clientIDX);

             //유저에게 알람을 쏴주자
            $alarmArr=[
                'clientIDX'=>$clientIDX,
                'statusIDX'=>$valueIDX,
                'targetIDX'=>$targetIDX,
                'param'=>['amount'=>$amount]
            ];
            // static::appPushAlarm($alarmArr);

            $AppMainIoUri= self::AppMainIoUri;
            $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
            static::sendCurl($alarmArr,$AppMainIoAddr);
        }

        //소켓 데이터테이블
        // $getStaffList=StaffMo::GetStaffList();
        // $staffPack = [];
        // foreach ($getStaffList as $key) {
        //     $staffPack[] = $key['idx'];
        // }
        // $DomainUri=self::DomainUri;
        // $dataPack = ['staffIDXPack'=>$staffPack,'targetIDX'=>$targetIDX];
        // $uri = $DomainUri.':16001/statusUpdate';
        // self::curlSend($dataPack,$uri);
        //소켓 데이터테이블
        $socketTable=$this->socketTable($targetIDX);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //staffNode.js 마지막 인서트된 데이터 가져오기
    public function GetTargetNodeData($data=null)
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMileageWithdrawalMo::GetTargetNodeData($targetIDX);
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}