<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMo;
use App\Models\ClientGradeMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMarketListMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{
		$gradePack=ClientGradeMo::GetClientGradeList();
		$renderData=['gradePack'=>$gradePack,];
		View::renderTemplate('page/client/client.html',$renderData);
	}

    public function RenderTest($data=null)
    {
        $gradePack=ClientGradeMo::GetClientGradeList();
        $renderData=['gradePack'=>$gradePack,];
        View::renderTemplate('page/client/clientTest.html',$renderData);
    }

	//MileageDepositCon 솔팅별 리셋 데이터
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $columnsVal=$_POST['columnsVal'];
        $totalCountData=ClientMo::GetTotalSortingData($columnsVal);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	//client.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=ClientMo::GetDataTableListLoad();
        //탈퇴를 제외한 토탈 카운트
        $totalCount = ClientMo::GetTotalCount();
        $resultData = ['data'=>$dataPack,'result'=>'t','totalCount'=>$totalCount];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }






    //client.html 데이터테이블 리스트 로드
    public function DataTableListLoadTest()
    {
        $start=$_POST['start'];
        $draw=$_POST['draw'];
        $length=$_POST['length'];
        $orderIndex = $_POST['order'][0]['column'];
        $orderType = $_POST['order'][0]['dir'];
        $orderName = $_POST['columns'][$orderIndex]['data'];




        function mySortFunction(&$arrayToSort, $orderType='asc',$myExtraArgument1=null, $myExtraArgument2=null) {
            usort($arrayToSort, function($a, $b) use ($orderType,$myExtraArgument1, $myExtraArgument2) {
                if($orderType=="asc"){
                    $nameComparison = strcmp($a[$myExtraArgument1], $b[$myExtraArgument1]);
                }else{
                     $nameComparison = strcmp($b[$myExtraArgument1], $a[$myExtraArgument1]); //
                }

                // 이름이 같을 경우 생년월일로 비교
                // if ($nameComparison === 0) {
                //     return strcmp($a['birth'], $b['birth']);
                // }
                return $nameComparison;
            });
        }


        $dataPack=ClientMo::GetDataTableListLoadTest();
        mySortFunction($dataPack,$orderType,$orderName);



        $finalPack = [];
        $targetName = '상영';
        foreach ($dataPack as $item) {
            if (strpos($item['name'], $targetName) !== false) {
                $finalPack[] = $item;
            }
        }
        // $dataPack = mySortFunction($dataPack);
        // print_r($dataPack);
        // usort($dataPack, array($this, "sortingFunc"));



        $finalPackSlice = array_slice($finalPack, $start, $length);
        $finalPackTotal = count($finalPackSlice);
        $dataPackTotal = count($dataPack);
        $resultData = [
            'data'=>$finalPackSlice,
            'result'=>'t',
            'draw'=> $draw,
            'recordsTotal'=> $dataPackTotal,
            'recordsFiltered'=> $finalPackTotal
        ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html 유저 공통 디테일 로드
    public function ClientDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        static::ClientDetail($targetIDX);
    }

    //client.html 디테일 로드
    public function ClientRightDetailFormLoad()
    {

        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientMo::GetClientDetailData($targetIDX);
        $bankPack=ClientMo::GetTargetClientData($targetIDX);
        $gradePack=ClientGradeMo::GetClientGradeList();
        $getMileage=ClientDetailMo::getMileage($targetIDX);
        $cmlPack=ClientMarketListMo::GetClientMarketListData($targetIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'dataPack'=>$dataPack,
            'bankPack'=>$bankPack,
            'gradePack'=>$gradePack,
            'getMileage'=>$getMileage,
            'cmlPack'=>$cmlPack,
        ];
        View::renderTemplate('page/client/clientDetail.html',$renderData);
    }

    //client.html 클라이언트 업데이트
    public function ClientUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
            $errMsg='gradeName 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $gradeIDX=$_POST['gradeIDX'];
        $gradeName=$_POST['gradeName'];

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $issetGradeIDX=$issetStatusVal['gradeIDX'];
            $issetGradeName=$issetStatusVal['gradeName'];

            if($issetGradeIDX==$gradeIDX){
                $errMsg='이전 등급과 동일한 등급입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $step1=0;

        if($gradeName!=$issetGradeName){
            $step1=1;
            $ex='등급이 '.$issetGradeName.'에서 '.$gradeName.'로 변경됐습니다';
            $logIDX=$this->ClientLogInsert(225102,$targetIDX,$targetIDX);
            $logEx=$this->ClientLogExInsert($logIDX,$issetGradeIDX,$gradeIDX,$ex);
        }

        if($step1==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Client SET
            gradeIDX=:gradeIDX
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':gradeIDX', $gradeIDX);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();



        //클라이언트로그
        // $this->ClientLogInsert(225102,$targetIDX,0);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html 클라이언트 긴급 업데이트
    public function ClientEmergencyUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['emergency'])||empty($_POST['emergency'])){
            $errMsg='emergency 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $emergency=$_POST['emergency'];

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(!isset($issetStatusVal['idx'])){
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Client SET
            emergency=:emergency
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':emergency', $emergency);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        if($emergency == 'Y'){
            $ex='상태가 긴급으로 변경됐습니다';
        }else{
            $ex='상태가 정상으로 변경됐습니다';
        }
        $logIDX = $this->ClientLogInsert(226101,$targetIDX,$targetIDX);
        $logEx  = $this->ClientLogExInsert($logIDX,0,0,$ex);


        // 240207 je_add 채팅으로 이머전시 쏘기
        $chatIoParam = [
            'clientIDX' => $targetIDX,
            'emergency' => $emergency
        ];

        $ChatIoUri  = self::ChatIoUri;
        $chatSocket = $ChatIoUri.'/Emergency';
        $this::sendCurl($chatIoParam, $chatSocket);
        // 240207 je_add 채팅으로 이머전시 쏘기



        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html 클라이언트 출금금지 업데이트
    public function ClientWithdrawalBlockUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['withdrawalBlock'])||empty($_POST['withdrawalBlock'])){
            $errMsg='withdrawalBlock 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $withdrawalBlock=$_POST['withdrawalBlock'];

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(!isset($issetStatusVal['idx'])){
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Client SET
            withdrawalBlock=:withdrawalBlock
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':withdrawalBlock', $withdrawalBlock);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        if($withdrawalBlock == 'Y'){
            $ex='클라이언트 출금금지 상태가 금지로 변경됐습니다';
        }else{
            $ex='클라이언트 출금금지 상태가 정상으로 변경됐습니다';
        }
        $logIDX=$this->ClientLogInsert(226101,$targetIDX,$targetIDX);
        $logEx=$this->ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $loginEmail=GlobalsVariable::GetGlobals('loginEmail');
        $loginName=GlobalsVariable::GetGlobals('loginName');

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $statusIDX=$issetStatusVal['statusIDX'];
            if($statusIDX==226201){
                $errMsg='이미 탈퇴한 회원입니다.';
                $errOn=$this::errExport($errMsg);
            }
            if($statusIDX==226203){
                $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Client SET
            statusIDX=:statusVal
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':statusVal', $statusVal);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        //클라이언트로그
        // $this->ClientLogInsert($statusVal,$targetIDX,$targetIDX);
        $logIDX=static::ClientLogInsert($statusVal,$targetIDX,$targetIDX);
        $ex='';
        if($statusVal == 226101){
            $ex = '클라이언트 상태 스태프 차단에서 정상으로 변경 / 처리자 : '.$loginName.' ('.$loginEmail.')';
        }else{
            $ex = '클라이언트 상태 정상에서 스태프 차단으로 변경 / 처리자 : '.$loginName.' ('.$loginEmail.')';
        }
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html 해당 클라이언트 발란스 가져오기
    public function getBalance()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getBalance=ClientDetailMo::getMileage($targetIDX);
        $resultData = ['result'=>'t','getBalance'=>$getBalance];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //client.html 해당 클라이언트 발란스 업데이트 (정말 필요할때 해야함)
    public function ClientBalanceUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['amount'])||empty($_POST['amount'])){
            $errMsg='amount 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['exVal'])||empty($_POST['exVal'])){
            $errMsg='exVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!is_numeric($_POST['amount'])) {
            $errMsg = '발란스는 숫자만 입력 가능합니다.';
            $errOn = $this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $statusIDX=$_POST['statusIDX'];
        $amount=$_POST['amount'];
        $exVal=$_POST['exVal'];

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $targetStatusIDX=$issetStatusVal['statusIDX'];
            if($targetStatusIDX==226201){
                $errMsg='이미 탈퇴한 회원입니다.';
                $errOn=$this::errExport($errMsg);
            }

            if($targetStatusIDX==226202){
                $errMsg='스태프에 의해 차단된 회원입니다.';
                $errOn=$this::errExport($errMsg);
            }

            // if($targetStatusIDX==226203){
            //     $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
            //     $errOn=$this::errExport($errMsg);
            // }
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $getBalance=ClientDetailMo::getMileage($targetIDX);

        if($statusIDX==205201){
            if($getBalance < $amount){
                $errMsg='차감 발란스가 현재 발란스보다 높습니다.';
                $errOn=$this::errExport($errMsg);
            }
        }

        $createTime=date("Y-m-d H:i:s");

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
            (clientIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");

        $stat1->bindValue(':clientIDX', $targetIDX);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->bindValue(':amount', $amount);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $insertIDX = $db->lastInsertId();

        $ex=$exVal.' ('.$amount.'원)';


        $logIDX=static::ClientLogInsert($statusIDX,$targetIDX,$targetIDX);
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;

        $dataPack2=ClientMo::IssetClientData($targetIDX);
        if(isset($dataPack2['idx'])&&!empty($dataPack2['idx'])){
            $migrationIDX=$dataPack2['migrationIDX'];
            if($migrationIDX != 0){
                // 증감 205101
                // 차감 205201
                $field = 'https://partners-api.ebuycompany.com/MagApi/updateMileageByStaff';
                //구바이에도 보내자
                $sort = '';
                if($statusIDX == '205101'){
                    $sort = 'plus';
                }elseif($statusIDX == '205201'){
                    $sort = 'minus';
                }

                $loginEmail=GlobalsVariable::GetGlobals('loginEmail');
                $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount.'&sort='.$sort.'&staffEmail='.$loginEmail;

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_REFERER, "$field");
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "$field",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "$postFields",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "content-type: application/x-www-form-urlencoded"
                    ),
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
            }
        }
    }

    //client.html 해당 클라이언트 회원탈퇴
    public function ClientDel()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];

        $issetStatusVal=ClientMo::IssetClientData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $targetStatusIDX=$issetStatusVal['statusIDX'];
            if($targetStatusIDX==226201){
                $errMsg='이미 탈퇴한 회원입니다.';
                $errOn=$this::errExport($errMsg);
            }
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $getBalance=ClientDetailMo::getMileage($targetIDX);

        if($getBalance >= 2000){
            $errMsg='마일리지가 2000이상이면 회원탈퇴가 불가능합니다.';
            $errOn = static::errExport($errMsg);
        }

        $clientStatusIDX = 226201; //회원탈퇴
        $db = static::getDB();
        $dbName= self::MainDBName;
        $result = $db->prepare("UPDATE $dbName.Client SET
            statusIDX=:statusIDX,
            deviceIDX=:deviceIDX
        WHERE idx=:idx
        ");
        $result->bindValue(':statusIDX', $clientStatusIDX);
        $result->bindValue(':idx', $targetIDX);
        $result->bindValue(':deviceIDX', 0);
        $result->execute();
        $logIDX=static::clientLogInsert($clientStatusIDX,$targetIDX,$targetIDX);
        $ex = '스태프에서 클라이언트 탈퇴 처리 완료';
        static::ClientLogExInsert($logIDX,0,0,$ex);


        //클마리 이메일 다 지워주기
        $result2 = $db->prepare("UPDATE $dbName.ClientMarketList SET
            statusIDX=:statusIDX
        WHERE clientIDX=:clientIDX");
        $result2->bindValue(':statusIDX', 240201);
        $result2->bindValue(':clientIDX', $targetIDX);
        $result2->execute();


        $result3 = $db->prepare("UPDATE $dbName.ClientPtpAccount SET
            statusIDX=:statusIDX
        WHERE clientIDX=:clientIDX");
        $result3->bindValue(':statusIDX', 250201);
        $result3->bindValue(':clientIDX', $targetIDX);
        $result3->execute();


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}