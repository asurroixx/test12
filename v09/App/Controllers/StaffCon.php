<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffMo;
use App\Models\StatusMo;
use App\Models\StaffGradeMo;
use App\Models\StaffLogMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class StaffCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        // phpinfo();
		View::renderTemplate('page/staff/staff.html');
	}//렌더

	//staff.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $staffList=StaffMo::GetStaffList();
        $resultData = ['data'=>$staffList,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;


    }

    //staffDetail.html 디테일 로드
    public function staffDetailFormLoad()
    {

        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        // $deptPack=DeptStdMo::GetDatatableList();
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $staffDetail=StaffMo::GetStaffDetail($targetIDX);
            $statusPack=StatusMo::GetStatusTwoAndThree();
        }else if($pageType=='ins'){
            $staffDetail='';
            $targetIDX='';
            $statusPack='';
        }
        $gradePack=StaffGradeMo::GetStaffGradeList();
        $renderData=[
            'targetIDX'=>$targetIDX,
            'pageType'=>$pageType,
            'staffDetail'=>$staffDetail,
            'statusPack'=>$statusPack,
            'gradePack'=>$gradePack,
        ];
        View::renderTemplate('page/staff/staffDetail.html',$renderData);

    }

    //staffLoginHistory.html 디테일 로드
    public function loginHistoryRender()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $renderData=[
            'targetIDX'=>$targetIDX,
        ];
        View::renderTemplate('page/staff/staffLoginHistory.html',$renderData);

    }

    //staffLoginHistory.html 데이터테이블 리스트 로드
    public function StaffLoginHistoryListLoad()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $targetIDX=$_POST['targetIDX'];
        $Arr=array(
            "startDate"=>$startDate,
            "endDate"=>$endDate,
            "targetIDX"=>$targetIDX,
        );
        $staffList=StaffLogMo::getStaffLoginHistory($Arr);
        $resultData = ['data'=>$staffList,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //staff.html 스태프 업데이트
    public function StaffUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
            $errMsg='statusIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
            $errMsg='gradeName 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['appActiveStat'])||empty($_POST['appActiveStat'])){
            $errMsg='appActiveStat 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['appSettlementStat'])||empty($_POST['appSettlementStat'])){
            $errMsg='appSettlementStat 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }

        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];
        $statusIDX=$_POST['statusIDX'];
        $statusVal=$_POST['statusVal'];
        $gradeIDX=$_POST['gradeIDX'];
        $gradeName=$_POST['gradeName'];
        $appActiveStat=$_POST['appActiveStat'];
        $appSettlementStat=$_POST['appSettlementStat'];

        $step1=0;
        $step2=0;
        $step3=0;
        $step4=0;
        $step5=0;

        $issetStaffData=StaffMo::IssetStaffData($targetIDX);
        if(isset($issetStaffData['idx'])){
            $issetName=$issetStaffData['name'];
            $issetGradeIDX=$issetStaffData['gradeIDX'];
            $issetGradeName=$issetStaffData['gradeName'];
            $issetStatusIDX=$issetStaffData['statusIDX'];
            $issetStatusVal=$issetStaffData['statusVal'];
            $issetAppActiveStat=$issetStaffData['appActiveStat'];
            $issetAppSettlementStat=$issetStaffData['appSettlementStat'];
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        //등급이름
        if($name!=$issetName){
            $step1=1;
            $ex='이름이 '.$issetName.'에서 '.$name.'로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(301201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //등급이름
        if($gradeName!=$issetGradeName){
            $step2=1;
            $ex='등급이 '.$issetGradeName.'에서 '.$gradeName.'로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(301201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$issetGradeIDX,$gradeIDX,$ex);
        }
        //등급이름
        if($statusVal!=$issetStatusVal){
            $step3=1;
            $ex2='상태가 '.$issetStatusVal.'에서 '.$statusVal.'으로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(301201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,$issetStatusIDX,$statusIDX,$ex2);
        }

        //입금신청알림
        if($appActiveStat!=$issetAppActiveStat){
            $step4=1;
            $ex2='입금신청알림 상태가 '.$issetAppActiveStat.'에서 '.$appActiveStat.'으로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(301201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex2);
        }

        //정산신청알림
        if($appSettlementStat!=$issetAppSettlementStat){
            $step5=1;
            $ex2='정산신청알림 상태가 '.$issetAppSettlementStat.'에서 '.$appSettlementStat.'으로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(301201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex2);
        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0 && $step5==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $dataDbKey=self::dataDbKey;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Staff SET
            name=AES_ENCRYPT(:name, '$dataDbKey'),
            statusIDX=:statusIDX,
            gradeIDX=:gradeIDX,
            appActiveStat=:appActiveStat,
            appSettlementStat=:appSettlementStat
            WHERE idx=:targetIDX
        ");
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':statusIDX', $statusIDX);
        $stat1->bindValue(':gradeIDX', $gradeIDX);
        $stat1->bindValue(':appActiveStat', $appActiveStat);
        $stat1->bindValue(':appSettlementStat', $appSettlementStat);
        $stat1->bindValue(':targetIDX', $targetIDX);
        $stat1->execute();

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //staff.html 스태프 추가
    public function StaffInsert()
    {
        if(!isset($_POST['email'])||empty($_POST['email'])){
            $errMsg='email 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
            $errMsg='gradeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $email=$_POST['email']; $email=trim($email);
        $mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
            $errMsg='이메일 형식이 아닙니다.';
            $errOn=$this->errExport($errMsg);
        }
        $issetEmailChk=StaffMo::issetStaffEmail($email);
        if(isset($issetEmailChk['email'])){
            $resultData = ['result'=>'f','msg'=>'중복된 이메일입니다.'];
            $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
            echo $result;
        }
        $name=$_POST['name'];
        $gradeIDX=$_POST['gradeIDX'];
        $ipAddress=$this->GetIPaddress();
        $createTime=date("Y-m-d H:i:s");

        $dataDbKey=self::dataDbKey;
        $db = static::getDB();
        $dbName= self::MainDBName;

        $stat1=$db->prepare("INSERT INTO $dbName.Staff
            (email,name,ipAddress,createTime,statusIDX,gradeIDX)
            VALUES
            (AES_ENCRYPT(:email, '$dataDbKey'), AES_ENCRYPT(:name, '$dataDbKey'),:ipAddress,:createTime,:statusIDX,:gradeIDX)
        ");

        $stat1->bindValue(':email', $email);
        $stat1->bindValue(':name', $name);
        $stat1->bindValue(':ipAddress', $ipAddress);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->bindValue(':statusIDX', 302101);
        $stat1->bindValue(':gradeIDX', $gradeIDX);
        $stat1->execute();

        $targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(301101,$targetIDX);


        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}