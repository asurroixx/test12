<?php

namespace App\Controllers;

use \Core\View;

use App\Models\MarketMo;
use App\Models\MarketDepositLogMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMo;
use App\Models\ClientMileageMo;


class ApiDepositCon extends \Core\Controller
{
    //렌더
    public function Render($data=null)
    {
        $marketListData = MarketMo::GetMarketListData();
        $arr            = ['marketListData'=>$marketListData];

        View::renderTemplate('page/apiDeposit/apiDeposit.html',$arr);
    }

    //ApiDeposit.html 솔팅별 리셋 데이터 deposit 제대로 파악 후 작업
    public function ResetSortingCount($data=null){
        if(!isset($_POST['columnsVal'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['startDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])){
            $errMsg='columnsVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $columnsVal=$_POST['columnsVal'];
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'columnsVal'=>$columnsVal,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=MarketDepositLogMo::GetTotalSortingData($dataArr);
        $resultData = ['result'=>'t','data'=>$totalCountData];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function GetMarketDepositAllData()
    {
        $startDate = $_POST['startDate'] ?? '';
        $endDate   = $_POST['endDate'] ?? '';

        if(!$startDate) $this::errExport('startDate 정보가 없습니다.');
        if(!$endDate) $this::errExport('endDate 정보가 없습니다.');

        // 데이터배열들 들고오기
        $dataArr = ['startDate' => $startDate, 'endDate' => $endDate];
        $DepositDataPack   = MarketDepositLogMo::GetDepositData($dataArr); // API 리스트 가져오기

        $namePack = ClientMarketListMo::GetClientNamePack();

        // 이름넣기
        $result = [];
        foreach ($DepositDataPack as &$DepositList) {
            $matchingCode    = $DepositList['marketCode'];
            $marketUserEmail = $DepositList['marketUserEmail'];
            $name            = '-';
            $clientName      = '-';
            foreach ($namePack as $item) {
                    if ($item['code'] == $matchingCode && strtolower($item['referenceId']) == strtolower($marketUserEmail)) {
                    $name = $item['name'];
                    $clientName = $item['clientName'];
                }
            }
            $DepositList['name']       = $name;
            $DepositList['clientName'] = $clientName;

            $result[] = $DepositList;
        }

        $resultData = ['result'=>'t','data'=>$result];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }


    //오른쪽 디테일 창
    public function ApiDetailFormLoad()
    {
        $targetIDX       = $_POST['targetIDX'] ?? '';
        $marketUserEmail = $_POST['marketUserEmail'] ?? '';
        if(!$targetIDX) $this::errExport('targetIDX 정보가 없습니다.');
        if(!$marketUserEmail) $this::errExport('marketUserEmail 정보가 없습니다.');

        // 타겟 디테일 구하기
        $detailPack   = MarketDepositLogMo::GetTargetDepositData($targetIDX);
        if(!$detailPack) $this::errExport('디테일 정보가 없습니다.');

        // 에러 메시지
        $log               = $detailPack['log'] ?? '';
        $logDecode         = json_decode($log, true);
        $detailPack['msg'] = $logDecode['msg'] ?? '';

        // 클라이언트 정보 있으면 구하기
        $clientDataPack = ClientMarketListMo::GetClientIDX($marketUserEmail);
        $clientIDX=0;
        if($clientDataPack){
            $clientIDX = $clientDataPack['clientIDX'];

            $clientAllDataPack = ClientMo::GetTargetClientData($clientIDX);
            $getMileage        = ClientDetailMo::getMileage($clientIDX);
            $DepositCount      = ClientMileageMo::GetApiDepositCount($clientIDX);
        }


        $clientDetailPack = [
            'phone'            => $clientAllDataPack['phone'] ?? '-',
            'name'             => $clientAllDataPack['name'] ?? '-',
            'clientEmail'      => $clientAllDataPack['email'] ?? '-',
            'accountNumber'    => $clientAllDataPack['accountNumber'] ?? '-',
            'allAmount'        => $getMileage ?? '-',
            'DepositCount'     => $DepositCount['idxCount'] ?? '-',
            'latestCreateTime' => $DepositCount['createTime'] ?? '-'
        ];

        $renderData = [
            'data'   => $detailPack,
            'client' => $clientDetailPack,
            'clientIDX'=>$clientIDX,
        ];
        
        View::renderTemplate('page/apiDeposit/apiDepositDetail.html',$renderData);
    }

    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=MarketDepositLogMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function ClientDetailLoad($data=null){
        if(!isset($_POST['marketCode'])||empty($_POST['marketCode'])){
            $errMsg='marketCode 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketUserEmail'])||empty($_POST['marketUserEmail'])){
            $errMsg='marketUserEmail 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketCode=$_POST['marketCode'];
        $marketUserEmail=$_POST['marketUserEmail'];
        $dataArr=array(
            'marketCode'=>$marketCode,
            'marketUserEmail'=>$marketUserEmail,
        );
        $clientIDXData=ClientMarketListMo::GetClientIDXData($dataArr);

        if(isset($clientIDXData['idx'])&&!empty($clientIDXData['idx'])){
            $clientIDX = $clientIDXData['clientIDX'];
            static::ClientDetail($clientIDX);
        }
    }






}