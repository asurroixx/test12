<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EmailMsgMo;
use App\Models\ManagerMo;
use App\Models\MarketManagerMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class StaffEmailCon extends \Core\Controller
{

 
    public static function sendEmailToPortal($data=null)
    {
        $statusIDX = $data['statusIDX'];
        $targetIDX = $data['targetIDX'];
        $marketIDX = $data['marketIDX'];
        //type  1.all = 전체발송 2.group = 마켓단위 3.target = 개별발송
        $type = 'group';
        if(isset($data['type'])){
            $type=$data['type'];
        }
        $paramVal = $data["param"];

        $getAlarmMsg = EmailMsgMo::getEmailMsg($statusIDX);
        if(!isset($getAlarmMsg['idx'])){
            $errMsg='기준 템플릿내용이 존재하지 않습니다.';
            return false;
        }
        $content = $getAlarmMsg['con'];
        $param = $getAlarmMsg['param'];
        $title = $getAlarmMsg['title'];
        $paramArr = explode(",", $param);
        foreach ($paramArr as $key ) {
            if (isset($paramVal[$key])) {
                $fullKey="{{".$key."}}";
                $replaceKey = $paramVal[$key];
                $content = str_replace($fullKey, $replaceKey, $content);
            }
        }
        //staffalarm 테이블이 쌓기
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $db = static::GetDB();
        if($type == 'all'){//전체발송 ex)공지사항
            $emailList = ManagerMo::getAllManager();
        }else if($type == 'group'){//마켓단위 발송
            $paramArr =['marketIDX'=>$marketIDX, 'statusIDX'=>$statusIDX];
            $emailList=MarketManagerMo::getMarketManagerListForEmail($paramArr);
        }else if($type =='target'){
            $emailList=[];
            if(isset($data['idxPack'])){
                $idxPack = $data['idxPack'];
                foreach ($idxPack as $key) {
                    $thisEmail = $key['email'];
                    $thisIDX = $key['idx'];
                    $emailList[]=['idx'=>$thisIDX, 'emailSettingIDX'=>0 , 'email'=>$thisEmail];
                }
            }
        }else{
            $errMsg='type이 올바르지 않습니다.';
            return false;
        }

        $createTime=date('Y-m-d H:i:s');
        $query = "INSERT INTO $dbName.Email (email, title, statusIDX, targetIDX, con, createTime,sendStatusIDX) VALUES ";
        $bulkString = [];
        foreach ($emailList as $key) {
            $email = $key['idx'];
            $emailSettingIDX = $key['emailSettingIDX'];
            if($emailSettingIDX==0 || $emailSettingIDX==null || $emailSettingIDX==""){
                //알람받을거야
                $bulkString[] = "(
                AES_ENCRYPT(:email".$email.", :dataDbKey),
                :title,
                :statusIDX ,
                :targetIDX,
                :con,
                :createTime ,
                :sendStatusIDX)";
            }
        }
        if(empty($bulkString)){
            return false;
        }
        $query .= implode(", ", $bulkString);
        $result = $db->prepare($query);
        // 바인딩 변수 설정
        foreach ($emailList as $key) {
            $email = $key['idx'];
            $emailTxt = $key['email'];
            $emailSettingIDX = $key['emailSettingIDX'];
            $paramName = ":email".$email;
            if($emailSettingIDX==0 || $emailSettingIDX==null || $emailSettingIDX==""){
                $result->bindValue($paramName, $emailTxt);
            }
        }
        $result->bindValue(":title", $title);
        $result->bindValue(":statusIDX", $statusIDX);
        $result->bindValue(":targetIDX", $targetIDX);
        $result->bindValue(":con", $content);
        $result->bindValue(":createTime", $createTime);
        $result->bindValue(":sendStatusIDX", 150201);
        $result->bindValue(":dataDbKey", $dataDbKey);


        $result->execute();
        return true;
        //staffalarm 테이블이 쌓기
    }

   
}