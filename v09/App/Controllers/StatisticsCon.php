<?php
namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\XxxJunmoxxXMo;
use App\Models\MarketMo;

/**
 * Home controller
 *
 * PHP version 7.0
 */

class StatisticsCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $optionDateResult = [];
        $monthCountArr=[0,1,2,3,4];
        $monthCountArrCount=count($monthCountArr);
        $optionDate=[];
        for($i=0; $i <$monthCountArrCount; $i++){
            $agoMonth=$monthCountArr[$i];
            $standMonth = date("m", mktime(0, 0, 0, intval(date('m'))-$agoMonth, intval(date('d')), intval(date('Y'))));
            $startDate = date("Y-m-d", mktime(0, 0, 0, intval(date('m'))-$agoMonth, 1, intval(date('Y'))));//달 1일;
            $endDate = date("Y-m-d", mktime(0, 0, 0, intval(date('m'))+1-$agoMonth, 0, intval(date('Y'))));//달 말일
            $standMonth = ltrim($standMonth, '0');
            $optionDate[]=[
                'standMonth'=>$standMonth,
                'startDate'=>$startDate,
                'endDate'=>$endDate
            ];
            $optionDateResult = ['data'=>$optionDate,'result'=>'t'];
        }
        $renderData=[
            'optionDateResult'=>$optionDateResult,
        ];
        View::renderTemplate('page/statistics/statistics.html',$renderData);
	}

    public function Load()
    {
    	if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $startDate.=" 00:00:00";
        $utcStart = date('Y-m-d H:i:s',strtotime('-9 hours',strtotime($startDate)));


        $endDate=$_POST['endDate'];
        $endDate.=" 23:59:59";
        $utcEnd = date('Y-m-d H:i:s', strtotime('-9 hours', strtotime($endDate)));


        $dateArr=array(
            'startDate'=>$utcStart,
            'endDate'=>$utcEnd,
        );


        $pPack=XxxJunmoxxXMo::statisticsPartner($utcEnd);
        $cPack=XxxJunmoxxXMo::statisticsClient($utcEnd);
        $mdPack=XxxJunmoxxXMo::statisticsMileageDeposit($dateArr);
        $mwPack=XxxJunmoxxXMo::statisticsMileageWithdrawal($dateArr);
        $pdPack=XxxJunmoxxXMo::statisticsApiDeposit($dateArr);
        $pdfPack=XxxJunmoxxXMo::statisticsApiDepositFee($dateArr);
        $pwPack=XxxJunmoxxXMo::statisticsApiWithdrawal($dateArr);
        $bankScrapingPack=XxxJunmoxxXMo::statisticsApiBankScraping($utcEnd);
        $depositAvgRatePack=XxxJunmoxxXMo::statisticsApiDepositAvgRate($dateArr);
        $withdrawalAvgRatePack=XxxJunmoxxXMo::statisticsApiWithdrawalAvgRate($dateArr);

        foreach ($pdPack as $key => $item) {
            $marketCode=$item['marketCode'];
            $getMarket=MarketMo::GetMarketCode($marketCode);
            $name ='- ('.$marketCode.')';
            if(isset($getMarket['idx'])||!empty($getMarket['idx'])){
                $name =$getMarket['name'];
            }
            $pdPack[$key]['name'] = $name;
        }
        usort($pdPack, function ($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });


        foreach ($pwPack as $key => $item) {
            $marketCode=$item['marketCode'];
            $getMarket=MarketMo::GetMarketCode($marketCode);
            $name ='- ('.$marketCode.')';
            if(isset($getMarket['idx'])||!empty($getMarket['idx'])){
                $name =$getMarket['name'];
            }
            $pwPack[$key]['name'] = $name;
        }
        usort($pwPack, function ($a, $b) {
            return strcasecmp($a['name'], $b['name']);
        });

        $arr=[
        	'pPack'=>$pPack,
			'cPack'=>$cPack,
			'mdPack'=>$mdPack,
			'mwPack'=>$mwPack,
			'pdPack'=>$pdPack,
			'pdfPack'=>$pdfPack,
            'pwPack'=>$pwPack,
            'bankScrapingPack'=>$bankScrapingPack,
            'depositAvgRatePack'=>$depositAvgRatePack,
            'withdrawalAvgRatePack'=>$withdrawalAvgRatePack,
        ];

        $resultData = ['result'=>'t','data'=>$arr];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }

    public function daycalculForStatistics()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.1';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.1';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['type'])||empty($_POST['type'])){
            $errMsg=' 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $type=$_POST['type'];

        if($type=='mod'){
            $startDate = date("Y-m-d", strtotime($startDate . " -1 day"));
            $endDate = date("Y-m-d", strtotime($endDate . " -1 day"));
        }elseif($type=='pod'){
            $startDate = date("Y-m-d", strtotime($startDate . " +1 day"));
            $endDate = date("Y-m-d", strtotime($endDate . " +1 day"));
        }

        $dateArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );


        $resultData = ['result'=>'t','data'=>$dateArr];
        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }




}