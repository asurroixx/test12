<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketSettlementCryptoWalletMo;
use App\Models\MarketSettlementBankwireMo;
use App\Models\MarketSettlementKRWMo;
use App\Models\PortalLogMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketSettlementCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		// $marketIDX=28;
        $type = 'A';
        $dataPack = MarketMo::GetMarketSortingListData($type);
        if (isset($_GET['marketIDX']) && !empty($_GET['marketIDX'])) {
            $marketIDX = $_GET['marketIDX'];
        } else {
            // $_GET['marketIDX']가 설정되어 있지 않거나 비어있는 경우, 데이터의 첫 번째 마켓의 idx 가져오기
            $marketIDX = !empty($dataPack) ? $dataPack[0]['idx'] : null;
        }
		$renderData=['dataPack'=>$dataPack,'marketIDX'=>$marketIDX];
		View::renderTemplate('page/marketSettlement/marketSettlement.html',$renderData);
	}//렌더


    //marketSettlement.html 마켓 솔팅
    public function MarketSorting()
    {
        if(!isset($_POST['type'])||empty($_POST['type'])){
            $errMsg='type 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $type=$_POST['type'];

        $dataPack=MarketMo::GetMarketSortingListData($type);
        $marketIDX = !empty($dataPack) ? $dataPack[0]['idx'] : null;
        $resultData = ['data'=>$dataPack,'marketIDX'=>$marketIDX,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	//marketSettlement.html 해당 마켓정보
    public function GetMarketDetailData($data=null)
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketIDX=$_POST['marketIDX'];
        $walletDataPack=MarketSettlementCryptoWalletMo::GetSettlementCryptoWalletData($marketIDX);
        $bankwirePack=MarketSettlementBankwireMo::GetSettlementBankwireData($marketIDX);
        $KRWPack=MarketSettlementKRWMo::GetSettlementKRWData($marketIDX);
        $resultData = [
        	'walletDataPack'=>$walletDataPack,
	        'bankwirePack'=>$bankwirePack,
	        'KRWPack'=>$KRWPack,
	        'result'=>'t',
	    ];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketSettlement.html 지갑로그 데이터테이블
    public function WalletLogDataTableListLoad()
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketIDX=$_POST['marketIDX'];

        $dataPack=PortalLogMo::GetWalletLogData($marketIDX);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketSettlement.html bankwire 로그 데이터테이블
    public function BankwireLogDataTableListLoad()
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketIDX=$_POST['marketIDX'];

        $dataPack=PortalLogMo::GetBankwireLogData($marketIDX);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //marketSettlement.html krw 로그 데이터테이블
    public function KrwLogDataTableListLoad()
    {
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $marketIDX=$_POST['marketIDX'];

        $dataPack=PortalLogMo::GetKrwLogData($marketIDX);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}