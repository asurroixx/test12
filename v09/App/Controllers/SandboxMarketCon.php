<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\SandboxMo;

/**
 * Home controller
 *
 * PHP version 7.0
 */

class SandboxMarketCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        $dataPack=SandboxMo::MarketList();
        $renderData=['dataPack'=>$dataPack];
		View::renderTemplate('page/sandboxMarket/sandboxMarket.html',$renderData);
	}//렌더

    public function dataTableListLoad()
    {
        $dataPack=SandboxMo::DatatableMarket();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}