<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientGradeMo;
use App\Models\EbuyBankMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientGradeCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{

		View::renderTemplate('page/clientGrade/clientGrade.html');
	}//렌더

	//clientGrade.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=ClientGradeMo::GetClientGradeData();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //clientGradeDetail.html 디테일 로드
    public function ClientGradeDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $detailPack=ClientGradeMo::GetClientGradeDetail($targetIDX);
        }else if($pageType=='ins'){
            $targetIDX='';
			$detailPack='';
        }
        $ebuyBankPack=EbuyBankMo::GetEbuyBankStatusData();
        $renderData=[
            //pub
            'pageType'=>$pageType,
            //upd
            'targetIDX'=>$targetIDX,
            'detailPack'=>$detailPack,
            //ins
            'ebuyBankPack'=>$ebuyBankPack,
        ];
        View::renderTemplate('page/clientGrade/clientGradeDetail.html',$renderData);

    }

    //clientGrade.html 수수료 업데이트
    public function ClientGradeFeeUpdate()
    {
    	//empty를 걸면 0을 없는값으로 침
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['ebuyBankIDX'])||empty($_POST['ebuyBankIDX'])){
            $errMsg='ebuyBankIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['defaultFeeKRW'])){
            $errMsg='defaultFeeKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['tradeFeePer'])){
            $errMsg='tradeFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withFeeKRW'])){
            $errMsg='withFeeKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withMinKRW'])){
            $errMsg='withMinKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['tranFeePer'])){
            $errMsg='tranFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withFeePer'])){
            $errMsg='withFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['upto'])){
            $errMsg='upto 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $name=$_POST['name'];

        //일단보류
        // $issetNameData=ClientGradeMo::IssetGradeName($name);
        // if(isset($issetNameData['idx'])||!empty($issetNameData['idx'])){
        // 	$errMsg='동일한 등급 이름이 존재합니다.';
        //     $errOn=$this::errExport($errMsg,'n');
        // }
        $ebuyBankIDX=$_POST['ebuyBankIDX'];
		$defaultFeeKRW=$_POST['defaultFeeKRW'];
		$tradeFeePer=$_POST['tradeFeePer'];
		$withFeeKRW=$_POST['withFeeKRW'];
        $withMinKRW=$_POST['withMinKRW'];
		$tranFeePer=$_POST['tranFeePer'];
		$withFeePer=$_POST['withFeePer'];
		$upto=$_POST['upto'];
        $ebuyBankName=$_POST['ebuyBankName'];

        $issetStatusVal=ClientGradeMo::issetClientGradeData($targetIDX);
        if(isset($issetStatusVal['idx'])){
            $issetIDX=$issetStatusVal['idx'];
            $issetName=$issetStatusVal['name'];
            $issetEbuyBankIDX=$issetStatusVal['ebuyBankIDX'];
            $issetDefaultFeeKRW=$issetStatusVal['defaultFeeKRW'];
            $issetTradeFeePer=$issetStatusVal['tradeFeePer'];
            $issetTranFeePer=$issetStatusVal['tranFeePer'];
            $issetWithFeePer=$issetStatusVal['withFeePer'];
            $issetWithFeeKRW=$issetStatusVal['withFeeKRW'];
            $issetWithMinKRW=$issetStatusVal['withMinKRW'];
            $issetUpto=$issetStatusVal['upto'];
            $issetEbuyBankName=$issetStatusVal['ebuyBankName'];
        }else{
            $errMsg='해당 클라이언트 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $step1=0;
        $step2=0;
        $step3=0;
        $step4=0;
        $step5=0;
        $step6=0;
        $step7=0;
        $step8=0;
        $step9=0;

        //등급
        if($name!=$issetName){
            $step1=1;
            $ex='등급이 '.$issetName.'에서 '.$name.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //은행이름
        if($ebuyBankName!=$issetEbuyBankName){
            $step2=1;
            $ex='입금은행이 '.$issetEbuyBankName.'에서 '.$ebuyBankName.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //기본수수료
        if($defaultFeeKRW!=$issetDefaultFeeKRW){
            $step3=1;
            $ex='기본수수료가 '.$issetDefaultFeeKRW.'에서 '.$defaultFeeKRW.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //거래수수료
        if($tradeFeePer!=$issetTradeFeePer){
            $step4=1;
            $ex='거래수수료가 '.$issetTradeFeePer.'에서 '.$tradeFeePer.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //지불대행수수료
        if($tranFeePer!=$issetTranFeePer){
            $step5=1;
            $ex='지불대행수수료가 '.$issetTranFeePer.'에서 '.$tranFeePer.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //출금대행수수료
        if($withFeePer!=$issetWithFeePer){
            $step6=1;
            $ex='출금대행수수료가 '.$issetWithFeePer.'에서 '.$withFeePer.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //출금수수료
        if($withFeeKRW!=$issetWithFeeKRW){
            $step7=1;
            $ex='출금수수료가 '.$issetWithFeeKRW.'에서 '.$withFeeKRW.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //최소출금가능금액
        if($withMinKRW!=$issetWithMinKRW){
            $step8=1;
            $ex='최소출금가능금액이 '.$issetWithMinKRW.'에서 '.$withMinKRW.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }
        //등급상향조건
        if($upto!=$issetUpto){
            $step9=1;
            $ex='최소출금가능금액이 '.$issetUpto.'에서 '.$upto.'(으)로 변경됐습니다';
            $logIDX=$this->StaffLogInsert(325201,$targetIDX);
            $logEx=$this->StaffLogExInsert($logIDX,0,0,$ex);
        }

        if($step1==0 && $step2==0 && $step3==0 && $step4==0 && $step5==0 && $step6==0 && $step7==0 && $step8==0 && $step9==0){
            $errMsg='변경된 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


		$defaultFeeKRW = str_replace(',', '', $defaultFeeKRW); // 문자열 "1000"
		$defaultFeeKRWFloat = floatval($defaultFeeKRW); //숫자로 변환
		$tradeFeePerFloat = floatval($tradeFeePer); //숫자로 변환
		$withFeeKRW = str_replace(',', '', $withFeeKRW); // 문자열 "1000"
		$withFeeKRWFloat = floatval($withFeeKRW); //숫자로 변환
        $withMinKRW = str_replace(',', '', $withMinKRW); // 문자열 "1000"
        $withMinKRWFloat = floatval($withMinKRW); //숫자로 변환
		$tranFeePerFloat = floatval($tranFeePer); //숫자로 변환
		$withFeePerFloat = floatval($withFeePer); //숫자로 변환
		$uptoFloat = floatval($upto); //숫자로 변환

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientGrade SET
        	name='$name',
        	ebuyBankIDX='$ebuyBankIDX',
			defaultFeeKRW='$defaultFeeKRWFloat',
			tradeFeePer='$tradeFeePerFloat',
			withFeeKRW='$withFeeKRWFloat',
            withMinKRW='$withMinKRWFloat',
			tranFeePer='$tranFeePerFloat',
			withFeePer='$withFeePerFloat',
			upto='$uptoFloat'
	    	WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        // $this->StaffLogInsert(325201,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //clientGrade.html 등급 인서트
    public function ClientGradeInsert()
    {
    	//empty를 걸면 0을 없는값으로 침
        if(!isset($_POST['name'])||empty($_POST['name'])){
            $errMsg='name 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['ebuyBankIDX'])||empty($_POST['ebuyBankIDX'])){
            $errMsg='ebuyBankIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['defaultFeeKRW'])){
            $errMsg='defaultFeeKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['tradeFeePer'])){
            $errMsg='tradeFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withFeeKRW'])){
            $errMsg='withFeeKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withMinKRW'])){
            $errMsg='withMinKRW 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['tranFeePer'])){
            $errMsg='tranFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['withFeePer'])){
            $errMsg='withFeePer 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['upto'])){
            $errMsg='upto 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }

        $name=$_POST['name'];
        $issetNameData=ClientGradeMo::IssetGradeName($name);
        if(isset($issetNameData['idx'])||!empty($issetNameData['idx'])){
        	$errMsg='동일한 등급 이름이 존재합니다.';
            $errOn=$this::errExport($errMsg,'n');
        }

        $ebuyBankIDX=$_POST['ebuyBankIDX'];
		$defaultFeeKRW=$_POST['defaultFeeKRW'];
		$tradeFeePer=$_POST['tradeFeePer'];
		$withFeeKRW=$_POST['withFeeKRW'];
        $withMinKRW=$_POST['withMinKRW'];
		$tranFeePer=$_POST['tranFeePer'];
		$withFeePer=$_POST['withFeePer'];
		$upto=$_POST['upto'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.ClientGrade
            (name,ebuyBankIDX,defaultFeeKRW,tradeFeePer,tranFeePer,withFeePer,withFeeKRW,withMinKRW,upto,statusIDX)
            VALUES
            ('$name','$ebuyBankIDX','$defaultFeeKRW','$tradeFeePer','$tranFeePer','$withFeePer','$withFeeKRW',$withMinKRW,'$upto',325301)
        ");
        $stat1->execute();
        $targetIDX = $db->lastInsertId();
        $this->StaffLogInsert(325101,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //clientGrade.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
            $errMsg='statusVal 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $statusVal=$_POST['statusVal'];

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientGrade SET
            statusIDX='$statusVal'
            WHERE idx='$targetIDX'
        ");
        $stat1->execute();

        $this->StaffLogInsert($statusVal,$targetIDX);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

}