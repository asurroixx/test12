<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientMo;
use App\Models\ClientGradeMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMarketListMo;
use App\Models\StaffMemoMo;
use App\Models\ClientMileageMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ClientUnifyCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	//렌더
	public function Render($data=null)
	{

		View::renderTemplate('page/clientUnify/clientUnify.html');
	}

	//client.html 데이터테이블 리스트 로드
    public function DataTableListLoad()
    {
        $dataPack=ClientMo::clientUnifyTable();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function ClientUnifySelLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $getDetail = ClientMo::getUnifyDetail($targetIDX);
        if(isset($getDetail['idx'])){
            $idx = $getDetail['idx'];
            $birth = $getDetail['birth'];
            $name = $getDetail['name'];
            $phone = $getDetail['phone'];
            $gradeName = $getDetail['gradeName'];
            $email = $getDetail['email'];

            $paramArr = ['clientIDX'=>$targetIDX,'name'=>$name, 'birth'=>$birth];
            $getAbleArr = ClientMo::getUnifyAbleArr($paramArr);
            $renderData = [
                'idx'=>$idx,
                'birth'=>$birth,
                'name'=>$name,
                'phone'=>$phone,
                'gradeName'=>$gradeName,
                'ableUserArr'=>$getAbleArr,
                'targetIDX'=>$targetIDX,
                'email'=>$email
            ];
            View::renderTemplate('page/clientUnify/clientUnifyDetail.html',$renderData);
        }

    }


    public function ClientUnifyAction()
    {


        if(!isset($_POST['standardIDX'])||empty($_POST['standardIDX'])){
            $errMsg='standardIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        if(!isset($_POST['unifiedIDX'])||empty($_POST['unifiedIDX'])){
            $errMsg='unifiedIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $standardIDX=$_POST['standardIDX'];
        $unifiedIDX=$_POST['unifiedIDX'];

        $staffIDX=GlobalsVariable::GetGlobals('loginIDX');
        if($staffIDX==0 || $staffIDX==null || $staffIDX==""){
            $errMsg='로그인해주세요.';
            $errOn=$this::errExport($errMsg);
        }

        $getStandard = ClientMo::getUnifyDetail($standardIDX);
        if(!isset($getStandard['idx'])){
            $errMsg='통합기준계정이 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $standardCi = $getStandard['ci'];
        $standardBirth = $getStandard['birth'];
        $standardPhone = $getStandard['phone'];
        $standardName = $getStandard['name'];
        $standardBankIDX = $getStandard['bankIDX'];
        $standardAccountNumber = $getStandard['accountNumber'];
        $standardAccountHolder = $getStandard['accountHolder'];
        $standardEmail = $getStandard['email'];
        $standardBankCreateTime = $getStandard['bankCreateTime'];
        $standardGender = $getStandard['gender'];
        $standardGrade = $getStandard['gradeIDX'];
        $standardDeviceIDX = $getStandard['deviceIDX'];
        $standardCreateTime = $getStandard['createTime'];
        $standardMigrationIDX = $getStandard['migrationIDX'];
        $standardMigrationTermsTime = $getStandard['migrationTermsTime'];
        $standardEmergency = $getStandard['emergency'];
        $standardStatus = $getStandard['statusIDX'];

        $getUnified = ClientMo::getUnifyDetail($unifiedIDX);
        if(!isset($getUnified['idx'])){
            $errMsg='통합대상계정이 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $unifiedCi = $getUnified['ci'];
        $unifiedBirth = $getUnified['birth'];
        $unifiedName = $getUnified['name'];
        $unifiedGradeIDX = $getUnified['gradeIDX'];
        $unifiedMigrationIDX = $getUnified['migrationIDX'];
        $unifiedMigrationTermsTime = $getUnified['migrationTermsTime'];
        $unifiedStatusIDX = $getUnified['statusIDX'];


        if($standardStatus!=226101){
            $errMsg='통합기준계정이 정상상태(status = 226101)가 아니라 통합할수 없습니다';
            $errOn=$this::errExport($errMsg);
        }
        if($unifiedStatusIDX!=226203){
            $errMsg='통합대상계정이 웹가입상태(status = 226203)가 아니라 통합할수 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        if($standardCi=="" || $standardCi==null){
            $errMsg='통합기준계정의 CI가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if($unifiedCi!==""){
            if($standardCi!=$unifiedCi){
                $errMsg='두 계정의 CI가 달라 통합할 수 없습니다';
            }
        }
        // if($standardName!=$unifiedName){
        //     $errMsg='두 계정의 이름이 달라 통합할 수 없습니다';
        //     $errOn=$this::errExport($errMsg);
        // }

        if($standardBirth!=$unifiedBirth){
            $errMsg='두 계정의 생년월일이 달라 통합할 수 없습니다';
            $errOn=$this::errExport($errMsg);
        }

        if($standardMigrationIDX==0){
            if($unifiedMigrationIDX>0){
                $standardMigrationIDX=$unifiedMigrationIDX;
            }
        }

        //통합할 클마리
        $getClientMarketList = ClientMarketListMo::GetClientMarketListByIDX($standardIDX);

        $getUnifiedClientMarketList = ClientMarketListMo::GetClientMarketListByIDX($unifiedIDX);

        //통합할 클라이언트 메모.
        $paramArr = ['statusIDX'=>371101 , 'targetIDX'=>$standardIDX,'staffIDX'=>$staffIDX];
        $getClientMemo = StaffMemoMo::GetMemoList($paramArr);
        $paramArr = ['statusIDX'=>371101 , 'targetIDX'=>$unifiedIDX,'staffIDX'=>$staffIDX];
        $getClientUnifiedMemo = StaffMemoMo::GetMemoList($paramArr);


        //통합할 등급
        if($unifiedGradeIDX>$standardGrade){
            $standardGrade=$unifiedGradeIDX;
        }
        //통합할 마일리지
        $standardMileage = ClientMileageMo::getMileage($standardIDX);
        $unifiedMileage = ClientMileageMo::getMileage($unifiedIDX);
        $finalMileage = $standardMileage*1 + $unifiedMileage*1;

        $createTime=date("Y-m-d H:i:s");
        $db = static::getDB();
        $dbName= self::MainDBName;
        $dataDbKey = self::dataDbKey;


        $result = $db->prepare("INSERT INTO $dbName.Client
        ( ci,gradeIDX, name, birth, phone,email,statusIDX,deviceIDX,createTime,gender,migrationIDX )
        VALUES
        ( :ci, :gradeIDX,
            AES_ENCRYPT(:name, :dataDbKey),
            AES_ENCRYPT(:birth, :dataDbKey),
            AES_ENCRYPT(:phone, :dataDbKey),
            AES_ENCRYPT(:email, :dataDbKey),
        :statusIDX, :deviceIDX, :createTime,:gender,:migrationIDX )
        ");
        $result->bindValue(':ci', $standardCi);
        $result->bindValue(':gradeIDX', $standardGrade);
        $result->bindValue(':name', $standardName);
        $result->bindValue(':birth', $standardBirth);
        $result->bindValue(':phone', $standardPhone);
        $result->bindValue(':email', $standardEmail);
        $result->bindValue(':statusIDX', 226101);
        $result->bindValue(':deviceIDX', $standardDeviceIDX);
        $result->bindValue(':createTime', $standardCreateTime);
        $result->bindValue(':gender', $standardGender);
        $result->bindValue(':migrationIDX', $standardMigrationIDX);
        $result->bindValue(':dataDbKey', $dataDbKey);
        $result->execute();
        $clientIDX=$db->lastInsertId();

        $logIDX = static::ClientLogInsert(226301,$clientIDX,$clientIDX);
        $beforeVal = $standardIDX;
        $afterVal = $clientIDX;
        $txt = $standardIDX."와".$unifiedIDX."를 합쳐".$clientIDX."로 바꿨습니다.(idx)";
        static::ClientLogExInsert($logIDX,$beforeVal,$afterVal,$txt);



        //합산마일리지 등록
        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
            (clientIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");

        $stat1->bindValue(':clientIDX', $clientIDX);
        $stat1->bindValue(':statusIDX', 206101);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->bindValue(':amount', $finalMileage);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();

        //은행등록
        $result1 = $db->prepare("INSERT INTO $dbName.ClientBank
        ( clientIDX ,bankIDX, accountNumber, accountHolder, createTime )
        VALUES
        ( :clientIDX,:bankIDX,
            AES_ENCRYPT(:accountNumber, :dataDbKey),
            AES_ENCRYPT(:accountHolder, :dataDbKey),
            :createTime )
        ");
        $result1->bindValue(':clientIDX', $clientIDX);
        $result1->bindValue(':bankIDX', $standardBankIDX);
        $result1->bindValue(':accountNumber', $standardAccountNumber);
        $result1->bindValue(':accountHolder', $standardAccountHolder);
        $result1->bindValue(':createTime', $standardBankCreateTime);
        $result1->bindValue(':dataDbKey', $dataDbKey);

        $result1->execute();
        $clientBankIDX=$db->lastInsertId();

        //메모등록
        foreach ($getClientMemo as $key ) {
            $memo = $key['memo'];
            $memo = '[통합전] '.$memo;
            $ip =$key['ip'];
            $memoCreateTime =$key['createTime'];
            $staffIDX=$key['staffIDX'];

            $ipAddress = static::GetIPaddress();
            $insert=$db->prepare("INSERT INTO $dbName.StaffMemo
                (statusIDX,staffIDX,targetIDX,memo,ip,createTime)
                VALUES
                (:statusIDX,:staffIDX,:targetIDX,:memoVal,:ipAddress,:createTime)
            ");
            $insert->bindValue(':statusIDX', 371101);
            $insert->bindValue(':staffIDX', $staffIDX);
            $insert->bindValue(':targetIDX', $clientIDX);
            $insert->bindValue(':memoVal', $memo);
            $insert->bindValue(':ipAddress', $ipAddress);
            $insert->bindValue(':createTime', $memoCreateTime);
            $insert->execute();
        }
        foreach ($getClientUnifiedMemo as $key ) {
            $memo = $key['memo'];
            $memo = '[통합전] '.$memo;
            $ip =$key['ip'];
            $memoCreateTime =$key['createTime'];
            $staffIDX=$key['staffIDX'];

            $ipAddress = static::GetIPaddress();
            $insert=$db->prepare("INSERT INTO $dbName.StaffMemo
                (statusIDX,staffIDX,targetIDX,memo,ip,createTime)
                VALUES
                (:statusIDX,:staffIDX,:targetIDX,:memoVal,:ipAddress,:createTime)
            ");
            $insert->bindValue(':statusIDX', 371101);
            $insert->bindValue(':staffIDX', $staffIDX);
            $insert->bindValue(':targetIDX', $clientIDX);
            $insert->bindValue(':memoVal', $memo);
            $insert->bindValue(':ipAddress', $ipAddress);
            $insert->bindValue(':createTime', $memoCreateTime);
            $insert->execute();
        }


        //계정탈퇴
        $update =$db->prepare("UPDATE $dbName.Client SET
            deviceIDX = 0,
            statusIDX = 226201
            WHERE idx = :idx
        ");
        $update->bindValue(':idx', $standardIDX);
        $update->execute();

        $logIDX=static::clientLogInsert(226201,$standardIDX,$standardIDX);
        $ex = '스태프의 계정통합에 의한 회원탈퇴처리(idx='.$clientIDX.'로 승계)';
        static::ClientLogExInsert($logIDX,0,0,$ex);

        $update =$db->prepare("UPDATE $dbName.Client SET
            deviceIDX = 0,
            statusIDX = 226201
            WHERE idx = :idx
        ");
        $update->bindValue(':idx', $unifiedIDX);
        $update->execute();

        $logIDX=static::clientLogInsert(226201,$unifiedIDX,$unifiedIDX);
        $ex = '스태프의 계정통합에 의한 회원탈퇴처리(idx='.$clientIDX.'로 승계)';
        static::ClientLogExInsert($logIDX,0,0,$ex);
        //계정탈퇴


        //기존계정 마일리지 차감
        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
            (clientIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");
        $stat1->bindValue(':clientIDX', $standardIDX);
        $stat1->bindValue(':statusIDX', 205201);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->bindValue(':amount', $standardMileage);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $ClientMileageIDX=$db->lastInsertId();

        $logIDX=static::clientLogInsert(205201,$ClientMileageIDX,$standardIDX);
        $ex = '스태프의 계정통합에 의한 마일리지차감';
        static::ClientLogExInsert($logIDX,$standardMileage,0,$ex);

        $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
            (clientIDX,statusIDX,targetIDX,amount,createTime)
            VALUES
            (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
        ");
        $stat1->bindValue(':clientIDX', $unifiedIDX);
        $stat1->bindValue(':statusIDX', 205201);
        $stat1->bindValue(':targetIDX', 0);
        $stat1->bindValue(':amount', $unifiedMileage);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();
        $ClientMileageIDX=$db->lastInsertId();

        $logIDX=static::clientLogInsert(205201,$ClientMileageIDX,$unifiedIDX);
        $ex = '스태프의 계정통합에 의한 마일리지차감';
        static::ClientLogExInsert($logIDX,$unifiedMileage,0,$ex);
        //기존계정 마일리지 차감


        //기존계정 클마리 비활성
        $clientMarketListDelStatus = 240201;
        $stat1=$db->prepare("UPDATE $dbName.ClientMarketList SET
            statusIDX=:statusIDX
            WHERE clientIDX=:clientIDX
        ");
        $stat1->bindValue(':statusIDX', $clientMarketListDelStatus);
        $stat1->bindValue(':clientIDX', $standardIDX);
        $stat1->execute();

        $stat1=$db->prepare("UPDATE $dbName.ClientMarketList SET
            statusIDX=:statusIDX
            WHERE clientIDX=:clientIDX
        ");
        $stat1->bindValue(':statusIDX', $clientMarketListDelStatus);
        $stat1->bindValue(':clientIDX', $unifiedIDX);
        $stat1->execute();
        //기존계정 클마리 비활성



        //클마리 인서트 (구 신 다 하기로 했음)
        $clientMarketListInsStatus = 240101;
        foreach ($getClientMarketList as $key ) {
            $clientMarketListIDX = $key['idx'];
            $marketIDX = $key['marketIDX'];
            $referenceId = $key['referenceId'];
            $ip = $key['ip'];
            $CMLcreateTime = $key['createTime'];
            $note = $key['note'];
            // 마켓에 유저 인써트
            $insert = $db->prepare("INSERT INTO $dbName.ClientMarketList
                (marketIDX, clientIDX, referenceId, statusIDX, ip, createTime)
            VALUES
                (:marketIDX, :clientIDX, AES_ENCRYPT(:referenceId,:dataDbKey), :statusIDX, :ip, :createTime)
            ");
            $insert->bindValue(':marketIDX', $marketIDX);
            $insert->bindValue(':clientIDX', $clientIDX);
            $insert->bindValue(':referenceId', $referenceId);
            $insert->bindValue(':statusIDX', $clientMarketListInsStatus);
            $insert->bindValue(':ip', $ip);
            $insert->bindValue(':createTime', $CMLcreateTime);
            $insert->bindValue(':dataDbKey', $dataDbKey);
            $insert->execute();
            $targetIDX=$db->lastInsertId();
            $logIDX=static::clientLogInsert($clientMarketListInsStatus,$targetIDX,$clientIDX);
            $ex='이메일 : '.$referenceId.' | 마켓 : '.$marketIDX.'(계정통합으로 인한 이전)';
            static::ClientLogExInsert($logIDX,0,0,$ex);
        }
        foreach ($getUnifiedClientMarketList as $key ) {
            $clientMarketListIDX = $key['idx'];
            $marketIDX = $key['marketIDX'];
            $referenceId = $key['referenceId'];
            $ip = $key['ip'];
            $CMLcreateTime = $key['createTime'];
            $note = $key['note'];
            // 마켓에 유저 인써트
            $insert = $db->prepare("INSERT INTO $dbName.ClientMarketList
                (marketIDX, clientIDX, referenceId, statusIDX, ip, createTime)
            VALUES
                (:marketIDX, :clientIDX, AES_ENCRYPT(:referenceId,:dataDbKey), :statusIDX, :ip, :createTime)
            ");
            $insert->bindValue(':marketIDX', $marketIDX);
            $insert->bindValue(':clientIDX', $clientIDX);
            $insert->bindValue(':referenceId', $referenceId);
            $insert->bindValue(':statusIDX', $clientMarketListInsStatus);
            $insert->bindValue(':ip', $ip);
            $insert->bindValue(':createTime', $CMLcreateTime);
            $insert->bindValue(':dataDbKey', $dataDbKey);
            $insert->execute();
            $targetIDX=$db->lastInsertId();
            $logIDX=static::clientLogInsert($clientMarketListInsStatus,$targetIDX,$clientIDX);
            $ex='이메일 : '.$referenceId.' | 마켓 : '.$marketIDX.'(계정통합으로 인한 이전)';
            static::ClientLogExInsert($logIDX,0,0,$ex);
        }

         //클마리 인서트


        if($unifiedMigrationIDX>0){
            $nextStepUri='https://partners-api.ebuycompany.com/MagApi/member_drop';
            $postFields='migrationIDX='.$unifiedMigrationIDX.'&uniqueKey=통합으로인한 삭제요청';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_REFERER, "https://partners-api.ebuycompany.com/MagApi/member_drop");
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://partners-api.ebuycompany.com/MagApi/member_drop",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "$postFields",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
        }


        $resultData = ['result'=>'t','msg'=>'통합이 완료되었습니다.'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;


    }

    // //client.html 디테일 로드
    // public function ClientRightDetailFormLoad()
    // {

    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg,'n');
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $dataPack=ClientMo::GetClientDetailData($targetIDX);
    //     $bankPack=ClientMo::GetTargetClientData($targetIDX);
    //     $gradePack=ClientGradeMo::GetClientGradeList();
    //     $getMileage=ClientDetailMo::getMileage($targetIDX);
    //     $renderData=[
    //         'targetIDX'=>$targetIDX,
    //         'dataPack'=>$dataPack,
    //         'bankPack'=>$bankPack,
    //         'gradePack'=>$gradePack,
    //         'getMileage'=>$getMileage
    //     ];
    //     View::renderTemplate('page/client/clientDetail.html',$renderData);
    // }

    // //client.html 클라이언트 업데이트
    // public function ClientUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
    //         $errMsg='gradeIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['gradeName'])||empty($_POST['gradeName'])){
    //         $errMsg='gradeName 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $gradeIDX=$_POST['gradeIDX'];
    //     $gradeName=$_POST['gradeName'];

    //     $issetStatusVal=ClientMo::IssetClientData($targetIDX);
    //     if(isset($issetStatusVal['idx'])){
    //         $issetGradeIDX=$issetStatusVal['gradeIDX'];
    //         $issetGradeName=$issetStatusVal['gradeName'];

    //         if($issetGradeIDX==$gradeIDX){
    //             $errMsg='이전 등급과 동일한 등급입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }else{
    //         $errMsg='해당 클라이언트 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $step1=0;

    //     if($gradeName!=$issetGradeName){
    //         $step1=1;
    //         $ex='등급이 '.$issetGradeName.'에서 '.$gradeName.'로 변경됐습니다';
    //         $logIDX=$this->ClientLogInsert(225102,$targetIDX,0);
    //         $logEx=$this->ClientLogExInsert($logIDX,$issetGradeIDX,$gradeIDX,$ex);
    //     }

    //     if($step1==0){
    //         $errMsg='변경된 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.Client SET
    //         gradeIDX=:gradeIDX
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':gradeIDX', $gradeIDX);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();



    //     //클라이언트로그
    //     // $this->ClientLogInsert(225102,$targetIDX,0);


    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // //client.html 클라이언트 업데이트
    // public function ClientEmergencyUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['emergency'])||empty($_POST['emergency'])){
    //         $errMsg='emergency 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $emergency=$_POST['emergency'];

    //     $issetStatusVal=ClientMo::IssetClientData($targetIDX);
    //     if(!isset($issetStatusVal['idx'])){
    //         $errMsg='해당 클라이언트 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.Client SET
    //         emergency=:emergency
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':emergency', $emergency);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     if($emergency == 'Y'){
    //         $ex='상태가 긴급으로 변경됐습니다';
    //     }else{
    //         $ex='상태가 정상으로 변경됐습니다';
    //     }
    //     $logIDX=$this->ClientLogInsert(226101,$targetIDX,$targetIDX);
    //     $logEx=$this->ClientLogExInsert($logIDX,0,0,$ex);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // //client.html status 업데이트
    // public function StatusUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
    //         $errMsg='statusVal 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusVal=$_POST['statusVal'];

    //     $issetStatusVal=ClientMo::IssetClientData($targetIDX);
    //     if(isset($issetStatusVal['idx'])){
    //         $statusIDX=$issetStatusVal['statusIDX'];
    //         if($statusIDX==226201){
    //             $errMsg='이미 탈퇴한 회원입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //         if($statusIDX==226203){
    //             $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }else{
    //         $errMsg='해당 클라이언트 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }


    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.Client SET
    //         statusIDX=:statusVal
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':statusVal', $statusVal);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     //클라이언트로그
    //     $this->ClientLogInsert($statusVal,$targetIDX,0);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // //client.html 해당 클라이언트 발란스 가져오기
    // public function getBalance()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $getBalance=ClientDetailMo::getMileage($targetIDX);
    //     $resultData = ['result'=>'t','getBalance'=>$getBalance];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // //client.html 해당 클라이언트 발란스 업데이트 (정말 필요할때 해야함)
    // public function ClientBalanceUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
    //         $errMsg='statusIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['amount'])||empty($_POST['amount'])){
    //         $errMsg='amount 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['exVal'])||empty($_POST['exVal'])){
    //         $errMsg='exVal 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!is_numeric($_POST['amount'])) {
    //         $errMsg = '발란스는 숫자만 입력 가능합니다.';
    //         $errOn = $this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusIDX=$_POST['statusIDX'];
    //     $amount=$_POST['amount'];
    //     $exVal=$_POST['exVal'];

    //     $issetStatusVal=ClientMo::IssetClientData($targetIDX);
    //     if(isset($issetStatusVal['idx'])){
    //         $targetStatusIDX=$issetStatusVal['statusIDX'];
    //         if($targetStatusIDX==226201){
    //             $errMsg='이미 탈퇴한 회원입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }

    //         if($targetStatusIDX==226202){
    //             $errMsg='스태프에 의해 차단된 회원입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }

    //         // if($targetStatusIDX==226203){
    //         //     $errMsg='웹버전 회원이지만 앱 미가입 회원입니다.';
    //         //     $errOn=$this::errExport($errMsg);
    //         // }
    //     }else{
    //         $errMsg='해당 클라이언트 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $getBalance=ClientDetailMo::getMileage($targetIDX);

    //     if($statusIDX==205201){
    //         if($getBalance < $amount){
    //             $errMsg='차감 발란스가 현재 발란스보다 높습니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }

    //     $createTime=date("Y-m-d H:i:s");

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
    //         (clientIDX,statusIDX,targetIDX,amount,createTime)
    //         VALUES
    //         (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
    //     ");

    //     $stat1->bindValue(':clientIDX', $targetIDX);
    //     $stat1->bindValue(':statusIDX', $statusIDX);
    //     $stat1->bindValue(':targetIDX', 0);
    //     $stat1->bindValue(':amount', $amount);
    //     $stat1->bindValue(':createTime', $createTime);
    //     $stat1->execute();
    //     $insertIDX = $db->lastInsertId();

    //     $ex=$exVal.' ('.$amount.'원)';


    //     $logIDX=static::ClientLogInsert($statusIDX,$targetIDX,$targetIDX);
    //     static::ClientLogExInsert($logIDX,0,0,$ex);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    // //client.html 해당 클라이언트 회원탈퇴
    // public function ClientDel()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];

    //     $issetStatusVal=ClientMo::IssetClientData($targetIDX);
    //     if(isset($issetStatusVal['idx'])){
    //         $targetStatusIDX=$issetStatusVal['statusIDX'];
    //         if($targetStatusIDX==226201){
    //             $errMsg='이미 탈퇴한 회원입니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }else{
    //         $errMsg='해당 클라이언트 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $getBalance=ClientDetailMo::getMileage($targetIDX);

    //     if($getBalance >= 2000){
    //         $errMsg='마일리지가 2000이상이면 회원탈퇴가 불가능합니다.';
    //         $errOn = static::errExport($errMsg);
    //     }

    //     $clientStatusIDX = 226201; //회원탈퇴
    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $result = $db->prepare("UPDATE $dbName.Client SET
    //         statusIDX=:statusIDX,
    //         deviceIDX=:deviceIDX
    //     WHERE idx=:idx
    //     ");
    //     $result->bindValue(':statusIDX', $clientStatusIDX);
    //     $result->bindValue(':idx', $targetIDX);
    //     $result->bindValue(':deviceIDX', 0);
    //     $result->execute();
    //     $logIDX=static::clientLogInsert($clientStatusIDX,$targetIDX,$targetIDX);
    //     $ex = '스태프에서 클라이언트 탈퇴 처리 완료';
    //     static::ClientLogExInsert($logIDX,0,0,$ex);


    //     //클마리 이메일 다 지워주기
    //     $result2 = $db->prepare("UPDATE $dbName.ClientMarketList SET
    //         statusIDX=:statusIDX
    //     WHERE clientIDX=:clientIDX");
    //     $result2->bindValue(':statusIDX', 240201);
    //     $result2->bindValue(':clientIDX', $targetIDX);
    //     $result2->execute();


    //     $result3 = $db->prepare("UPDATE $dbName.ClientPtpAccount SET
    //         statusIDX=:statusIDX
    //     WHERE clientIDX=:clientIDX");
    //     $result3->bindValue(':statusIDX', 250201);
    //     $result3->bindValue(':clientIDX', $targetIDX);
    //     $result3->execute();


    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

}