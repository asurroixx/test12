<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StaffMo;
use App\Models\StaffLoginDumpMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class LoginCon extends \Core\Controller
{
	/**
	 * Show the index page
	 *
	 * @return void
	 */
	public function Render($data=null)
	{
		$renderData=['data'=>$data];
		View::renderTemplate('page/login/login.html',$renderData);
	}

	//스태프 암호화 이메일 저장
	public function StaffEmailInsertData($data=null)
	{

		if (!isset($_POST['email'])||empty($_POST['email'])) {
			$errMsg='이메일을 입력해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		$email = htmlspecialchars($_POST['email']);
		$email = trim($email);
		$mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
        	$errMsg='이메일 형식이 아닙니다.';
            $errOn=$this->errExport($errMsg);
        }
        if (!isset($_POST['name'])||empty($_POST['name'])) {
			$errMsg='이름을 입력해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		$name=$_POST['name'];

        $createTime= date('Y-m-d H:i:s');
		$ipAddress=$this->GetIPaddress();

		$db = static::getDB();
		$dbName=self::MainDBName;
		$dataDbKey=self::dataDbKey;

		$query = $db->prepare("INSERT INTO $dbName.Staff
			(email,name,ipAddress,createTime,statusIDX,gradeIDX)
            VALUES
            (AES_ENCRYPT(:email,'$dataDbKey'),AES_ENCRYPT(:name,'$dataDbKey'),:ipAddress,:createTime,:statusIDX,:gradeIDX)
        ");
        $query->bindValue(':email', $email);
        $query->bindValue(':name', $name);
        $query->bindValue(':ipAddress', $ipAddress);
        $query->bindValue(':createTime', $createTime);
        $query->bindValue(':statusIDX', 302101);
        $query->bindValue(':gradeIDX', 1);
        $query->execute();


        $data = ['result'=>'t'];
        $result=json_encode($data,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

	
	//인증메일 보내기
	public function LoginProcess($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffLoginDump'];
		if (!isset($_POST['email'])||empty($_POST['email'])) {
			$errMsg='이메일을 입력해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		$email = htmlspecialchars($_POST['email']);
		$email = trim($email);
		$mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
        	$errMsg='이메일 형식이 아닙니다.';
            $errOn=$this->errExport($errMsg);
        }
        $emailExist= StaffMo::issetEmail($email);
		if(!isset($emailExist['idx'])){
			$errMsg='이메일이 존재하지 않습니다.';
        	$errOn=$this->errExport($errMsg);
		}
		$adminIDX=$emailExist['idx'];
        $createTime= date('Y-m-d H:i:s');
		$SignUpHistoryRandoms=$this->MakeRandomString(5,'SignUpHistoryRandoms');
		$ipAddress=$this->GetIPaddress();
		$db = static::getDB();

		/*-----로그인 덤프에 추가합니다.-----*/
		$stat2=$db->prepare("INSERT INTO $MainDBName.$BasicTable (staffIDX, code,createTime, ipAddress) VALUES ('$adminIDX', '$SignUpHistoryRandoms', '$createTime', '$ipAddress')");
	    $stat2->execute();

	    $data = ['result'=>'t','tempCode'=>$SignUpHistoryRandoms];
        $result=json_encode($data,JSON_UNESCAPED_UNICODE);
        echo $result;
	    /*-----실제 메일을 보낼때 사용합니다-----*/
    }


    public function EmailSend($data=null)
	{
		 /*-----실제 메일을 보낼때 사용합니다-----*/
		if (!isset($_POST['email'])||empty($_POST['email'])) {
			$errMsg='Please enter your email.';
            $errOn=static::errExport($errMsg);
		}
		$email = htmlspecialchars($_POST['email']);
		$email = trim($email);
		$mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
        	$errMsg='Email format is incorrect.';
            $errOn=static::errExport($errMsg);
        }

		$getCodeSel=StaffLoginDumpMo::getCode($email);
		if(!isset($getCodeSel['code'])){
			$errMsg='Unauthorized Access Detected.';
            $errOn=static::errExport($errMsg);
		}
		$code=$getCodeSel['code'];
		$title='EBUY';
        $emailBody = '<span style="font-size:16px;">Dear '.$email.',</br> We need to be sure it is you trying to log in to your EBUY account. Here is your authentication code:</span><span style="font-size:30px; font-weight:bold">'.$code.'</span>';
        static::emailSendForm($email,$emailBody,$title);


     //    $paramVal=['email'=>$email, 'code'=>$code];
	    // $getAlarmMsg = EmailMsgMo::getEmailMsg(404101);
     //    if(!isset($getAlarmMsg['idx'])){
     //        $errMsg='Unauthorized Access Detected.';
     //        return false;
     //    }
     //    $emailTitle = $getAlarmMsg['title'];
     //    $content = $getAlarmMsg['con'];
     //    $param = $getAlarmMsg['param'];
     //    $paramArr = explode(",", $param);
     //    foreach ($paramArr as $key ) {
     //        if (isset($paramVal[$key])) {
     //            $fullKey="{{".$key."}}";
     //            $replaceKey = $paramVal[$key];
     //            $content = str_replace($fullKey, $replaceKey, $content);
     //        }
     //    }
	}




    
	public function LoginAct($data=null)
	{	
		$ServerName=self::ServerName;
		$MainDBName=self::MainDBName;
		$pageTokenKey=self::PageTokenKey;
        $RouterPageDuplicateLogin=self::RouterPageDuplicateLogin;
		$DomainUri=self::DomainUri;

		if(!isset($_POST['email'])||empty($_POST['email'])){
			$errMsg='이메일을 입력해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		if(!isset($_POST['code'])||empty($_POST['code'])){
			$errMsg='코드를 입력해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		$code = $_POST['code'];
		$email = $_POST['email'];
		$email = htmlspecialchars($email); $email = trim($email);
		$code = htmlspecialchars($code); $code = trim($code);
		$ipAddress=$this->GetIPaddress();

		$paramArr = ['email' => $email,'ipAddress'=>$ipAddress,'code'=>$code];
		$adminInfo=StaffMo::GetLoginCodeChk($paramArr);
		if(!isset($adminInfo['idx'])){
			$errMsg='코드가 맞지않습니다. 다시 시도해주세요.';
            $errOn=$this->errExport($errMsg);
		}
		$loginIDX=$adminInfo['idx'];
		$statusIDX=$adminInfo['statusIDX'];
		if($statusIDX==302201){
			$errMsg='비활성된 계정입니다.';
            $errOn=$this->errExport($errMsg);
		}
        $date=date("Y-m-d H:i:s");

		//쿠키 및 세션 생성
		$CookieMake=$this->CookieMake($loginIDX);
		//글로벌 쿠키 생성
		$globalCookie=$this->GlobalCookieMake($loginIDX);

		$MakeIDX=$CookieMake['idx'];
		$MakeToken=$CookieMake['token'];
		$MakeIpAddress=$CookieMake['ipAddress'];

        $encryptStringArr=['idx'=>$MakeIDX,'token'=>$MakeToken,'ipAddress'=>$MakeIpAddress];
        $encryptString=json_encode($encryptStringArr,JSON_UNESCAPED_UNICODE);
        $encryptFunc=$this->encrypt($encryptString,$pageTokenKey);//암호화한 세션
        $tokenFunc=$this->encrypt($MakeToken,$pageTokenKey);//암호화한 토큰
        $db = static::getDB();
        $BasicTable=self::BasicTable['PageToken'];
        if($RouterPageDuplicateLogin=='N'){
	        $stat1=$db->prepare("
	        	DELETE FROM $MainDBName.$BasicTable WHERE loginIDX='$MakeIDX' AND subUrl='$ServerName'
	        ");
	        $stat1->execute();
        }
        $db = static::getDB();
        $stat2=$db->prepare("INSERT INTO $MainDBName.$BasicTable
            (session,token,subUrl,ipAddress,createTime,loginIDX)
            VALUES
            ('$encryptFunc','$tokenFunc','$ServerName','$MakeIpAddress','$date','$MakeIDX')
        ");
        $stat2->execute();

        //Core > View.php 전역변수에 전달
        View::$PageToken=$tokenFunc;

		$redirectUri = $DomainUri."/dashboard?token=".$tokenFunc."";
		$result=['result'=>'t','redirectUri'=>$redirectUri];
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
	}

	public function CookieMake($data=null)
	{
		$loginKey        = self::LoginKey;
		$MainDBName      = self::MainDBName;
		$CookieDomainUri = self::CookieDomainUri;

		$loginIDX        = $data;
		$ipAddress       = $this->GetIPaddress();
		$randomToken     = $this->MakeRandomString(32);

		$createTime = date('Y-m-d H:i:s');
		$db         = static::GetDB();
		$BasicTable = self::BasicTable['Staff'];

		$stat=$db->prepare("UPDATE $MainDBName.$BasicTable SET
        	loginToken='$randomToken',
        	ipAddress='$ipAddress'
			WHERE idx='$loginIDX'
    	");
    	$stat->execute();
    	//암호화
		$encryptArr    = ['idx'=>$loginIDX,'token'=>$randomToken,'ipAddress'=>$ipAddress,'date'=>$createTime];
		$encryptString = json_encode($encryptArr,JSON_UNESCAPED_UNICODE);
		$encrypt       = $this->encrypt($encryptString,$loginKey);
		$encryptSecond = $this->encrypt($encryptString,$loginKey);

		//세션과 쿠키
		if (session_status() == PHP_SESSION_NONE) {
		  session_start();
		}
		$_SESSION['pSession'] = $encrypt;
		setcookie("pCookie", $encryptSecond, time() + (7200 * 120), "/", $CookieDomainUri);
		
		// 240103 챗서버와 공유할 쿠키 //////////
		$CookieChatDomainUri = self::CookieChatDomainUri;
		setcookie("chatCookie", $encryptSecond, time() + (7200 * 120), "/", '.ebuycompany.com');
		//////////////////////////////////////////////////

		//로그인 히스토리 글로벌변수 생성전이라 로그인만 함수 안씀
		$stat2=$db->prepare("INSERT INTO ebuy.StaffLog
			(statusIDX,staffIDX,createTime,ip)
			VALUES
			(304101,'$loginIDX','$createTime','$ipAddress')
		");
		$stat2->execute();
		return ['result'=>'t','idx'=>$loginIDX,'token'=>$randomToken,'ipAddress'=>$ipAddress];
	}

	public function GlobalCookieMake($data=null)
	{

		$loginKey=self::LoginKey;
		$CookieDomainUri=self::CookieDomainUri;

		$resultIDX=$data;
		$globalValues = StaffMo::GetGlobalVal($resultIDX);
		if(isset($globalValues['idx'])&&!empty($globalValues['idx'])){
			$globalIDX=$globalValues['idx'];
			$globalEmail=$globalValues['email'];
			$globalName=$globalValues['name'];
			$globalGrade=$globalValues['gradeIDX'];
			$date=date("Y-m-d H:i:s");
			$globalValPackArr=array(
				'globalIDX'=>$globalIDX,
				'globalEmail'=>$globalEmail,
				'globalName'=>$globalName,
				'globalGrade'=>$globalGrade,
				'date'=>$date,
			);
			$globalValPack=json_encode($globalValPackArr,JSON_UNESCAPED_UNICODE);
			$globalVal = $this->encrypt($globalValPack,$loginKey);
    		setcookie("pGlobalCookie", $globalVal, time() + (7200 * 120), "/", $CookieDomainUri);
		}
	}

	public function LoginExtend($data=null)
	{
		if(!isset($_SESSION)){session_start();}
    	$session=$_SESSION['pSession'];
    	$loginKey=self::LoginKey;
        $sessionDecrypt = $this->decrypt($session,$loginKey);
        $sessionArr=json_decode($sessionDecrypt,true);
        $loginIDX=$sessionArr['idx'];
		$CookieMake=$this->CookieMake($loginIDX);
		$globalCookie=$this->GlobalCookieMake($loginIDX);
	}

	public function LoginTimeLoad($data=null)
	{
    	$newCookieMin= GlobalsVariable::GetGlobals('min');
    	$newCookieSec= GlobalsVariable::GetGlobals('sec');
    	$result=['result'=>'t','min'=>$newCookieMin,'sec'=>$newCookieSec];
		echo json_encode($result,JSON_UNESCAPED_UNICODE);
	}

	public function LogoutAct($data=null)
	{	
		$ServerName=self::ServerName;
		$CookieDomainUri=self::CookieDomainUri;
		$MainDBName=self::MainDBName;
		$RouterPageDuplicateLogin=self::RouterPageDuplicateLogin;

		
    	$loginIDX= GlobalsVariable::GetGlobals('loginIDX');
		$createTime=date("Y-m-d H:i:s");

		$fromData = $_POST['data']??'';
		$db = static::getDB();
		$dbName = self::MainDBName;
		$stat1 = $db->prepare("INSERT INTO $dbName.StaffLogout
			(createTime,staffIDX,fromData,ex)
			VALUES
			(:createTime,:staffIDX,:fromData,:ex)
		");
		$stat1->bindValue(':createTime', $createTime);
		$stat1->bindValue(':staffIDX', $loginIDX);
		$stat1->bindValue(':fromData', $fromData);
		$stat1->bindValue(':ex', '');
		$stat1->execute();
		

		if(isset($loginIDX)&&!empty($loginIDX)){
			if (session_status() == PHP_SESSION_NONE) {
			    session_start();
			}
			$db = static::getDB();
			$BasicTable=self::BasicTable['PageToken'];
			if($RouterPageDuplicateLogin=='N'){
		        $stat1=$db->prepare("
		        	DELETE FROM $MainDBName.$BasicTable WHERE loginIDX='$loginIDX' AND subUrl='$ServerName'
		        ");
		        $stat1->execute();
			}
			$ipAddress=$this->GetIPaddress();
	        $stat2=$db->prepare("INSERT INTO ebuy.StaffLog
				(statusIDX,staffIDX,createTime,ip)
				VALUES
				(304102,'$loginIDX','$createTime','$ipAddress')
			");
			$stat2->execute();
			session_unset();
			session_destroy();
			setcookie("pGlobalCookie", NULL, time() - (86400 * 999), "/", $CookieDomainUri);
			setcookie("pCookie", NULL, time() - (86400 * 999), "/", $CookieDomainUri);
			// 240109 챗서버와 공유할 쿠키 //////////
			$CookieChatDomainUri = self::CookieChatDomainUri;
			setcookie("chatCookie", NULL, time() - (86400 * 999), "/", '.ebuycompany.com');
		}
	}



	public function LoginActsdvsvdsdvsdv($data=null)
	{	

		$email="asurroixx@gmail.com";
        $bodycontent="acascavavs";
        $bodycontitle="azczxczxczxczxczxc";
        $result=$this->emailSendFormAttach($email,$bodycontent,$bodycontitle);
        echo "ascscscscscscscscsc";
	}

}
?>