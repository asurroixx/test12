<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ClientPtpAccountMo;
use App\Models\ClientDetailMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class PtpClientAccountCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
        View::renderTemplate('page/ptpClientAccount/ptpClientAccount.html');
	}//렌더

	//marketManager.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $dataPack=ClientPtpAccountMo::GetDatatableList();
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }





    //tmpInsert임시에요
    public function tmp2Insert()
    {
        $errMsg='이건 준모가 만든 버전이고!!!! 장배버전을 쓰란 말이야!!!!';
        $errOn=$this::errExport($errMsg);

        if(!isset($_POST['chamID'])||empty($_POST['chamID'])){
            $errMsg='chamID 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
            $errMsg='marketIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }


        $marketIDX=$_POST['marketIDX'];
        $clientIDX=$_POST['clientIDX'];
        $referenceId=$_POST['chamID'];
        // $statusIDX=$_POST['statusIDX'] 240101;
        $createTime=date("Y-m-d H:i:s");

        $dataDbKey=self::dataDbKey;

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.ClientMarketList
        (
        marketIDX,
        clientIDX,
        referenceId,
        statusIDX,
        createTime
        )
        VALUES
        (
        :marketIDX,
        :clientIDX,
        AES_ENCRYPT(:referenceId, :dataDbKey),
        :statusIDX,
        :createTime
        )
        ");
        $stat1->bindValue(':marketIDX', $marketIDX);
        $stat1->bindValue(':clientIDX', $clientIDX);
        $stat1->bindValue(':referenceId', $referenceId);
        $stat1->bindValue(':dataDbKey', $dataDbKey);
        $stat1->bindValue(':statusIDX', 240101);
        $stat1->bindValue(':createTime', $createTime);
        $stat1->execute();

        $resultData = ['result'=>'t','msg'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

















    //tmpInsert임시에요
    public function tmpInsert()
    {
        $dataDbKey=self::dataDbKey;
        if(!isset($_POST['PeerToPeerTypeIDX'])||empty($_POST['PeerToPeerTypeIDX'])){
            $errMsg='PeerToPeerTypeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['clientIDX'])||empty($_POST['clientIDX'])){
            $errMsg='clientIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['account'])||empty($_POST['account'])){
            $errMsg='account 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $PeerToPeerTypeIDX=$_POST['PeerToPeerTypeIDX'];
        $clientIDX=$_POST['clientIDX'];
        $account=$_POST['account'];
        $account=trim($account);
        $mailCheck = filter_var($account, FILTER_VALIDATE_EMAIL);
        if($mailCheck===false){
            $errMsg='이메일 형식이 아닙니다.';
            $errOn=$this::errExport($errMsg);
        }

        $ipAddress = $this->GetIPaddress();
        $createTime=date("Y-m-d H:i:s");

        $tmpArr=['account'=>$account,'PeerToPeerTypeIDX'=>$PeerToPeerTypeIDX];
        $issetAccountData=ClientPtpAccountMo::tmpIssetAccount($tmpArr);
        if(isset($issetAccountData['idx'])){
            $errMsg='이미 등록된 account 입니다.';
            $errOn=$this::errExport($errMsg);
        }

        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat2=$db->prepare("INSERT INTO $dbName.ClientPtpAccount
            (clientIDX,account,PeerToPeerTypeIDX,statusIDX,createTime)
            VALUES
            (:clientIDX,AES_ENCRYPT(:account, :dataDbKey),:PeerToPeerTypeIDX,:statusIDX,:createTime)
        ");
        $stat2->bindValue(':clientIDX', $clientIDX);
        $stat2->bindValue(':account', $account);
        $stat2->bindValue(':dataDbKey', $dataDbKey);
        $stat2->bindValue(':PeerToPeerTypeIDX', $PeerToPeerTypeIDX);
        $stat2->bindValue(':statusIDX', 250101);
        $stat2->bindValue(':createTime', $createTime);
        $stat2->execute();
        $targetIDX = $db->lastInsertId();

        $resultData = ['result'=>'t','msg'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function ptpClientAccountDetail()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $targetIDX=$_POST['targetIDX'];
        $dataPack=ClientPtpAccountMo::GetClientPtpAccountDetailData($targetIDX);
        if(!isset($dataPack['idx'])){
            $errMsg='클라이언트 정보가 없어요.';
            $errOn=$this::errExport($errMsg);
        }
        $clientIDX=$dataPack['idx'];
        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $renderData=[
            'dataPack'=>$dataPack,
            'getMileage'=>$getMileage
        ];
        View::renderTemplate('page/ptpClientAccount/ptpClientAccountDetail.html',$renderData);
    }

















    public function junmoTest()
    {
        // // $startDate='2024-01-29 18:31:39';
        // // $endDate=date('Y-m-d H:i:s');
        // $db = static::GetDB();
        // $dbName= self::MainDBName;

        // $query = $db->prepare("SELECT
        //     A.idx
        //     ,A.clientIDX
        //     ,A.targetIDX
        //     ,A.createTime
        // FROM $dbName.ClientMileage AS A
        // WHERE A.statusIDX=:SucStatusIDX
        // -- AND A.createTime BETWEEN :startDate AND :endDate
        // ");
        // // $query->bindValue(':startDate', $startDate);
        // // $query->bindValue(':endDate', $endDate);
        // $query->bindValue(':SucStatusIDX',906101);
        // $query->execute();
        // $result=$query->fetchAll(PDO::FETCH_ASSOC);
        // foreach ($result as $item) {
        //     $clientIDX=$item['clientIDX'];
        //     $targetIDX=$item['targetIDX'];
        //     $createTime=$item['createTime'];

        //     $dbApi     = static::GetApiDB();
        //     $query = $dbApi->prepare("SELECT
        //             amount
        //         FROM ebuyAPI.MarketDepositLog
        //         WHERE idx='$targetIDX'
        //         AND statusIDX = 906101
        //     ");

        //     $query->execute();
        //     $sel=$query->fetch(PDO::FETCH_ASSOC);
        //     if(isset($sel['amount'])){
        //         $amount=$sel['amount'];
        //         $db = static::GetDB();
        //         $dbName= self::MainDBName;
        //         $stat1=$db->prepare("INSERT INTO $dbName.ClientAllAmount
        //             (clientIDX,amount,statusIDX,targetIDX,createTime)
        //             VALUES
        //             (:clientIDX,:amount,:statusIDX,:targetIDX,:createTime)
        //         ");
        //         $stat1->bindValue(':clientIDX', $clientIDX);
        //         $stat1->bindValue(':amount', $amount);
        //         $stat1->bindValue(':statusIDX', 906101);
        //         $stat1->bindValue(':targetIDX', $targetIDX);
        //         $stat1->bindValue(':createTime', $createTime);
        //         $stat1->execute();
        //     }else{
        //         echo $clientIDX;
        //         echo $targetIDX;
        //         echo $createTime;
        //     }
        // }


                    // $db = static::GetDB();
                    // $dbName= self::MainDBName;
                    // $clientIDX=116591;
                    // $que11 = $db->prepare("SELECT
                    //         B.gradeIDX,
                    //         ROUND(SUM(A.amount), 2) AS totAmount,
                    //         IFNULL(D.upto, 0) AS nextUpto
                    //     FROM $dbName.ClientAllAmount AS A
                    //     LEFT JOIN $dbName.Client AS B ON B.idx = A.clientIDX
                    //     LEFT JOIN $dbName.ClientGrade AS D ON D.idx = B.gradeIDX + 1
                    //     WHERE B.gradeIDX != 3
                    //     AND A.clientIDX = :clientIDX
                    //     GROUP BY A.clientIDX
                    //     -- HAVING ROUND(SUM(A.amount), 2) >= IFNULL(D.upto, 0)
                    //     HAVING ROUND(SUM(A.amount), 2) >= IFNULL(nextUpto, 0)

                    // ");
                    // $que11->bindValue(':clientIDX', $clientIDX);
                    // $que11->execute();
                    // $sel11=$que11->fetch(PDO::FETCH_ASSOC);
                    // if(isset($sel11['gradeIDX'])){
                    //     $gradeIDX=$sel11['gradeIDX'];
                    //     echo $gradeIDX;
                    // }





                    // $db = static::GetDB();
                    // $dbName= self::MainDBName;
                    // $dataDbKey=self::dataDbKey;
                    // $query = $db->prepare("
                    // SELECT
                    // A.clientIDX
                    // ,B.gradeIDX
                    // ,B.migrationIDX
                    // ,ROUND(SUM(A.amount), 2) AS totAmount
                    // ,IFNULL(D.upto, 0) AS nextUpto
                    // FROM $dbName.ClientAllAmount AS A
                    // LEFT JOIN $dbName.Client AS B ON B.idx = A.clientIDX
                    // LEFT JOIN $dbName.ClientGrade AS D ON D.idx = B.gradeIDX+1
                    // WHERE B.gradeIDX != 3
                    // GROUP BY A.clientIDX
                    // HAVING ROUND(SUM(A.amount), 2) >= IFNULL(nextUpto, 0)
                    // ");
                    // $query->execute();
                    // $result=$query->fetchAll(PDO::FETCH_ASSOC);
                    // foreach ($result as $item) {
                    //     $gradeIDX=$item['gradeIDX'];
                    //     $clientIDX=$item['clientIDX'];
                    //     $nowTime=date("Y-m-d H:i:s");
                    //     $nextGradeIDX=$gradeIDX+1;
                    //     $ex='(잡) 등급이 '.$gradeIDX.'에서 '.$nextGradeIDX.'로 변경됐습니다';
                    //     $que12=$db->prepare("INSERT INTO $dbName.ClientLog
                    //         (statusIDX,targetIDX,clientIDX,createTime)
                    //         VALUES
                    //         (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                    //     ");
                    //     $que12->bindValue(':updateStatusIDX', 225101);
                    //     $que12->bindValue(':targetIDX', $clientIDX);
                    //     $que12->bindValue(':clientIDX', $clientIDX);
                    //     $que12->bindValue(':createTime', $nowTime);
                    //     $que12->execute();
                    //     $ClientLogIDX=$db->lastInsertId();
                    //     $que13=$db->prepare("INSERT INTO $dbName.ClientLogEx
                    //         (logIDX,ex)
                    //         VALUES
                    //         (:logIDX,:ex)
                    //     ");
                    //     $que13->bindValue(':logIDX', $ClientLogIDX);
                    //     $que13->bindValue(':ex', $ex);
                    //     $que13->execute();

                    //     $que14=$db->prepare("UPDATE $dbName.Client SET
                    //     gradeIDX=:gradeIDX
                    //     WHERE idx=:targetIDX
                    //     ");
                    //     $que14->bindValue(':gradeIDX', $nextGradeIDX);
                    //     $que14->bindValue(':targetIDX', $clientIDX);
                    //     $que14->execute();
                    // }


                    // $db = static::GetDB();
                    // $dbName= self::MainDBName;
                    // $query = $db->prepare("
                    // SELECT
                    // A.gradeIDX
                    // ,A.migrationIDX
                    // FROM $dbName.Client AS A
                    // WHERE A.migrationIDX IS NOT NULL
                    // AND A.migrationIDX>1
                    // AND A.gradeIDX!=1
                    // ");
                    // $query->execute();
                    // $result=$query->fetchAll(PDO::FETCH_ASSOC);
                    // foreach ($result as $item) {
                    //     $migrationIDX=$item['migrationIDX'];
                    //     $gradeIDX=$item['gradeIDX'];
                    //     if($gradeIDX==1){
                    //         $GuLevel=2;
                    //         $GuLevelName='SILVER VIP';
                    //     }elseif($gradeIDX==2){
                    //         $GuLevel=3;
                    //         $GuLevelName='GOLD VIP';
                    //     }elseif($gradeIDX==3){
                    //         $GuLevel=6;
                    //         $GuLevelName='PLATINUM VIP';
                    //     }

                    //     $oldDb = static::GetOldDB();
                    //     $oldDbName= self::EbuyOldDBName;
                    //     $query = $oldDb->prepare("SELECT
                    //         M_LEVEL
                    //         ,M_LEVELNAME
                    //         ,user_name
                    //         FROM $oldDbName.eb_member
                    //         WHERE member_srl='$migrationIDX'
                    //         AND M_LEVEL!='$GuLevel'
                    //         AND (M_LEVEL='2' OR M_LEVEL='3' OR M_LEVEL='6')
                    //     ");
                    //     $query->execute();
                    //     $sel11=$query->fetch(PDO::FETCH_ASSOC);
                    //     if(isset($sel11['user_name'])){
                    //         $user_name=$sel11['user_name'];
                    //         $M_LEVELNAME=$sel11['M_LEVELNAME'];
                    //         echo $user_name.'--구:'.$M_LEVELNAME.'--뉴:'.$gradeIDX.'-----';

                    //         $stat1=$oldDb->prepare("UPDATE $oldDbName.eb_member SET
                    //             M_LEVEL=:M_LEVEL
                    //             ,M_LEVELNAME=:M_LEVELNAME
                    //             WHERE member_srl=:migrationIDX
                    //         ");
                    //         $stat1->bindValue(':M_LEVEL', $GuLevel);
                    //         $stat1->bindValue(':M_LEVELNAME', $GuLevelName);
                    //         $stat1->bindValue(':migrationIDX', $migrationIDX);
                    //         $stat1->execute();
                    //     }
                    // }
    }
























    // //marketManagerGrade.html status 업데이트
    // public function StatusUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusVal'])||empty($_POST['statusVal'])){
    //         $errMsg='statusVal 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusVal=$_POST['statusVal'];

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
    //         statusIDX=:statusVal
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':statusVal', $statusVal);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     //포탈로그
    //     $this->PortalLogInsert($statusVal,$targetIDX);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //marketManagerGrade.html 삭제버튼 status 업데이트
    // public function DeleteStatusUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $statusVal=402202;


    //     $getDetail = ClientPtpAccountMo::GetGradeDetail($targetIDX);
    //     if(!isset($getDetail['idx'])){
    //         $errMsg='존재하지 않는 IDX.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $targetEmail = $getDetail['email'];
    //     $targetMarketIDX =  $getDetail['marketIDX'];


    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
    //         statusIDX=:statusVal
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':statusVal', $statusVal);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     //포탈로그
    //     $this->PortalLogInsert($statusVal,$targetIDX);

    //     //샌드박스에도 매니저를 삭제시켜주자.
    //     $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($targetMarketIDX);
    //     if(isset($marketIDXInSandboxSel['idx'])){
    //         $sandboxMarketIDX=$marketIDXInSandboxSel['idx'];
    //         $paramArr = ['marketIDX'=>$sandboxMarketIDX, 'email'=>$targetEmail];

    //         $getMMIDX = ClientPtpAccountMo::getMarketManagerIDXInSandbox($paramArr);
    //         if(isset($getMMIDX['idx'])){
    //             $sandboxMMIDX = $getMMIDX['idx'];
    //             $dbsand = static::GetSandboxDB();
    //             $dbNameSand= self::EbuySandboxDBName;
    //             $query = $dbsand->prepare("UPDATE $dbNameSand.MarketManager SET statusIDX=:statusIDX WHERE idx=:idx");
    //             $query->bindValue(':statusIDX', 402202);
    //             $query->bindValue(':idx', $sandboxMMIDX);
    //             $query->execute();
    //         }
    //     }
    //     //샌드박스에도 매니저를 삭제시켜주자.

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //marketManagerGrade.html 마켓 매니저 인서트
    // public function MarketManagerInsert()
    // {

    //     $dataDbKey=self::dataDbKey;
    //     if(!isset($_POST['email'])||empty($_POST['email'])){
    //         $errMsg='email 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['name'])||empty($_POST['name'])){
    //         $errMsg='name 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
    //         $errMsg='marketIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     //마켓 선택을 안함
    //     $marketIDX=$_POST['marketIDX'];
    //     if($marketIDX=='none'){
    //         $errMsg='마켓을 선택해주세요.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
    //         $errMsg='gradeIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['statusIDX'])||empty($_POST['statusIDX'])){
    //         $errMsg='statusIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['position'])||empty($_POST['position'])){
    //         $errMsg='position 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     //이메일 형식
    //     $email=$_POST['email']; $email=trim($email);
    //     $mailCheck = filter_var($email, FILTER_VALIDATE_EMAIL);
    //     if($mailCheck===false){
    //         $errMsg='이메일 형식이 아닙니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $name=$_POST['name'];
    //     $gradeIDX=$_POST['gradeIDX'];
    //     $statusIDX=$_POST['statusIDX'];
    //     $position=$_POST['position'];

    //     //해당 마켓에 등급이 Super가 있으면 Super 등급으로는 인서트 불가
    //     if($gradeIDX==1){
    //         $issetSuperGrade=ClientPtpAccountMo::issetManagerGradeData($marketIDX);
    //         $superGradeCount=$issetSuperGrade['count'];
    //         if($superGradeCount > 0){
    //             $errMsg=$issetSuperGrade['name'].'마켓의 Super 등급이 존재합니다.';
    //             $errOn=$this::errExport($errMsg);
    //         }
    //     }

    //     $ipAddress = $this->GetIPaddress();
    //     $createTime=date("Y-m-d H:i:s");
    //     $issetEmailData=ManagerMo::IssetManagerEmail($email);
    //     //이메일 조사 후 매니저 테이블에 이메일 없으면 인서트 / 있으면 그해당 idx 가져오기 (managerIDX)
    //     if(isset($issetEmailData['idx'])){
    //         $managerIDX=$issetEmailData['idx'];
    //     }else{
    //         $db = static::getDB();
    //         $dbName= self::MainDBName;
    //         $stat1=$db->prepare("INSERT INTO $dbName.Manager
    //             (email,ipAddress,createTime)
    //             VALUES
    //             (AES_ENCRYPT(:email, '$dataDbKey'),:ipAddress,:createTime)
    //         ");
    //         $stat1->bindValue(':email', $email);
    //         $stat1->bindValue(':ipAddress', $ipAddress);
    //         $stat1->bindValue(':createTime', $createTime);
    //         $stat1->execute();
    //         $managerIDX = $db->lastInsertId();
    //     }

    //     //해당 마켓에 마켓 매니저가 있다면 인서트 불가
    //     $managerArr=['marketIDX'=>$marketIDX,'managerIDX'=>$managerIDX];
    //     $issetManagerIDX=ClientPtpAccountMo::issetManagerData($managerArr);
    //     if(isset($issetManagerIDX['idx'])){
    //         $errMsg='이미 등록된 마켓 매니저 입니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     //유효성 끝나면 마켓매니저 테이블에 인서트
    //     $db2 = static::getDB();
    //     $dbName= self::MainDBName;
    //     $stat2=$db2->prepare("INSERT INTO $dbName.MarketManager
    //         (marketIDX,managerIDX,gradeIDX,statusIDX,position,name,createTime)
    //         VALUES
    //         (:marketIDX,:managerIDX,:gradeIDX,:statusIDX,:position,AES_ENCRYPT(:name, '$dataDbKey'),:createTime)
    //     ");

    //     $stat2->bindValue(':marketIDX', $marketIDX);
    //     $stat2->bindValue(':managerIDX', $managerIDX);
    //     $stat2->bindValue(':gradeIDX', $gradeIDX);
    //     $stat2->bindValue(':statusIDX', $statusIDX);
    //     $stat2->bindValue(':position', $position);
    //     $stat2->bindValue(':name', $name);
    //     $stat2->bindValue(':createTime', $createTime);

    //     $stat2->execute();
    //     $targetIDX = $db2->lastInsertId();

    //     //포탈로그
    //     $this->PortalLogInsert(401101,$targetIDX);



    //      //샌드박스에도 매니저를 만들어주자.
    //     $dbsand = static::GetSandboxDB();
    //     $dbNameSand= self::EbuySandboxDBName;
    //     $managerDupleChkInSand = ManagerMo::issetEmailInSand($email);
    //     $marketIDXInSandboxSel = MarketMo::getSandboxMarketIDX($marketIDX);
    //     if(isset($marketIDXInSandboxSel['idx'])){
    //         $sandboxMarketIDX = $marketIDXInSandboxSel['idx'];
    //         if(!isset($managerDupleChkInSand['idx'])){
    //             $query=$dbsand->prepare("INSERT INTO $dbNameSand.Manager (email,ipAddress, createTime)
    //                 VALUES (AES_ENCRYPT(:email, '$dataDbKey'), :ipAddress, :createTime)
    //             ");
    //             $query->bindValue(':email', $email);
    //             $query->bindValue(':ipAddress', $ipAddress);
    //             $query->bindValue(':createTime', $createTime);
    //             $query->execute();
    //             $managerIDX=$dbsand->lastInsertId();
    //         }else{
    //             $managerIDX=$managerDupleChkInSand['idx'];
    //         }

    //         $query=$dbsand->prepare("INSERT INTO $dbNameSand.MarketManager (marketIDX,managerIDX,gradeIDX, statusIDX,position,name,createTime)
    //             VALUES (:marketIDX, :managerIDX, :gradeIDX, :statusIDX,:position,AES_ENCRYPT(:name, '$dataDbKey'),:createTime)
    //         ");
    //         $query->bindValue(':marketIDX', $sandboxMarketIDX);
    //         $query->bindValue(':managerIDX', $managerIDX);
    //         $query->bindValue(':gradeIDX', $gradeIDX);
    //         $query->bindValue(':statusIDX', 402101);
    //         $query->bindValue(':position', $position);
    //         $query->bindValue(':name', $name);
    //         $query->bindValue(':createTime', $createTime);
    //         $query->execute();
    //     }
    //     //샌드박스에도 매니저를 만들어주자.

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //marketManagerGrade.html 마켓 매니저 업데이트
    // public function MarketManagerUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['name'])||empty($_POST['name'])){
    //         $errMsg='name 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
    //         $errMsg='gradeIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['position'])||empty($_POST['position'])){
    //         $errMsg='position 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
    //         $errMsg='marketIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $name=$_POST['name'];
    //     $gradeIDX=$_POST['gradeIDX'];
    //     $position=$_POST['position'];

    //     $marketIDX=$_POST['marketIDX'];
    //     //해당 마켓에 등급이 Super가 있으면 Super 등급으로는 업데이트 불가

    //     $issetSuperGrade=ClientPtpAccountMo::issetSuperManagerGradeData($marketIDX);
    //     //있으면 일단 이 마켓엔 슈퍼등급이 있음
    //     if(isset($issetSuperGrade['idx'])){
    //         $superIDX=$issetSuperGrade['idx'];
    //         $superGradeIDX=$issetSuperGrade['gradeIDX'];
    //         $issetMarketName=$issetSuperGrade['name'];

    //         if($superIDX!=$targetIDX){
    //             //슈퍼등급이 아닌데 넘어온값이 슈퍼등급이야? 그럼 안돼
    //             if($gradeIDX==$superGradeIDX){
    //                 $errMsg=$issetMarketName.'마켓의 Super 등급이 존재합니다.';
    //                 $errOn=$this::errExport($errMsg);
    //             }
    //         }

    //     }



    //     //업데이트가 가능한 상태인가
    //     $targetData=ClientPtpAccountMo::GetTargetUpdateData($targetIDX);
    //     if(!isset($targetData['idx'])){
    //         $errMsg='타겟이 없습니다';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $targetGradeIDX = $targetData['gradeIDX'];
    //     $targetPosition = $targetData['position'];
    //     $targetName = $targetData['name'];
    //     $targetStatusIDX=$targetData['statusIDX'];

    //     if($targetStatusIDX==402202){
    //         $errMsg='이미 삭제된 이메일 입니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }

    //     $step1=0;
    //     $step2=0;
    //     $step3=0;

    //     if($gradeIDX!=$targetGradeIDX){
    //         $step1=1;
    //         $ex='등급이 '.$targetGradeIDX.'에서 '.$gradeIDX.'(으)로 변경됐습니다.';
    //         $logIDX=$this->PortalLogInsert(401201,$targetIDX);
    //         $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
    //     }

    //     if($position!=$targetPosition){
    //         $step2=1;
    //         $ex='포지션이 '.$targetPosition.'에서 '.$position.'(으)로 변경됐습니다.';
    //         $logIDX=$this->PortalLogInsert(401201,$targetIDX);
    //         $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
    //     }

    //     if($name!=$targetName){
    //         $step3=1;
    //         $ex='매니저이름이 '.$targetName.'에서 '.$name.'(으)로 변경됐습니다.';
    //         $logIDX=$this->PortalLogInsert(401201,$targetIDX);
    //         $logEx=$this->PortalLogExInsert($logIDX,0,0,$ex);
    //     }

    //     if($step1==0 && $step2==0 && $step3==0){
    //         $errMsg='변경된 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }


    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     $dataDbKey=self::dataDbKey;
    //     $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
    //         gradeIDX=:gradeIDX,
    //         position=:position,
    //         name=AES_ENCRYPT(:name, '$dataDbKey')
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':gradeIDX', $gradeIDX);
    //     $stat1->bindValue(':position', $position);
    //     $stat1->bindValue(':name', $name);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();

    //     //포탈로그
    //     // $logIDX=$this->PortalLogInsert(401201,$targetIDX);
    //     //포탈로그ex
    //     // $this->PortalLogExInsert($logIDX,$ex);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

    //marketManagerGrade.html 마켓 매니저 슈퍼등급 업데이트
    // public function MarketManagerGradeUpdate()
    // {
    //     if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
    //         $errMsg='targetIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['marketIDX'])||empty($_POST['marketIDX'])){
    //         $errMsg='marketIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['gradeIDX'])||empty($_POST['gradeIDX'])){
    //         $errMsg='gradeIDX 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     if(!isset($_POST['email'])||empty($_POST['email'])){
    //         $errMsg='email 정보가 없습니다.';
    //         $errOn=$this::errExport($errMsg);
    //     }
    //     $targetIDX=$_POST['targetIDX'];
    //     $marketIDX=$_POST['marketIDX'];
    //     $gradeIDX=$_POST['gradeIDX'];
    //     $email=$_POST['email'];
    //     $issetSuperGrade=ClientPtpAccountMo::issetSuperManagerGradeData($marketIDX);
    //     $superIDX=$issetSuperGrade['idx'];
    //     $superEmail=$issetSuperGrade['email'];

    //     $db = static::getDB();
    //     $dbName= self::MainDBName;
    //     //슈퍼등급 업데이트
    //     $stat1=$db->prepare("UPDATE $dbName.MarketManager SET
    //         gradeIDX=:gradeIDX
    //         WHERE idx=:targetIDX
    //     ");
    //     $stat1->bindValue(':gradeIDX', 1);
    //     $stat1->bindValue(':targetIDX', $targetIDX);
    //     $stat1->execute();


    //     $ex2='슈펴등급이 '.$superEmail.'에서 '.$email.'(으)로 변경됐습니다.';
    //     $logIDX2=$this->PortalLogInsert(401201,$targetIDX);
    //     $this->PortalLogExInsert($logIDX2,$superIDX,$targetIDX,$ex2);

    //     //원래 슈퍼등급이였던 매니저는 하이 클래스로 변경
    //     $stat2=$db->prepare("UPDATE $dbName.MarketManager SET
    //         gradeIDX=:gradeIDX
    //         WHERE idx=:superIDX
    //     ");
    //     $stat2->bindValue(':gradeIDX', 2);
    //     $stat2->bindValue(':superIDX', $superIDX);
    //     $stat2->execute();

    //     $ex='슈퍼등급이였던 '.$superEmail.' 등급이 High Class로 변경됐습니다.';
    //     $logIDX=$this->PortalLogInsert(401201,$superIDX);
    //     $this->PortalLogExInsert($logIDX,0,0,$ex);

    //     $resultData = ['result'=>'t'];
    //     $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
    //     echo $result;
    // }

}