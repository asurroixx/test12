<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\MarketMo;
use App\Models\MarketWithdrawalLogMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMo;
use App\Models\ClientMileageMo;





use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ApiWithdrawalCon extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */

    //렌더
    public function Render($data=null)
    {
        $marketListData=MarketMo::GetMarketListData();
        $arr = ['marketListData'=>$marketListData];
        View::renderTemplate('page/apiWithdrawal/apiWithdrawal.html',$arr);
    }

    public function GetMarketWithdrawalAllData()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'] ?? '';
        $endDate=$_POST['endDate'] ?? '';
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $withdrawalDataPack=MarketWithdrawalLogMo::GetWithdrawalData($dataArr);
        $getMarketDataPack=MarketMo::GetMarketAllListData(); //마켓정보 가져오기
        $getClientDataPack=ClientMo::GetClientAllListData(); //클라이언트정보 가져오기

        unset($getClientDataPack[0]); 
        unset($getMarketDataPack[0]);

        $marketNclientDataPack = array_merge($getMarketDataPack, $getClientDataPack);

        $result = [];

        foreach ($withdrawalDataPack as $withdrawalList) {
            $matchingCode = $withdrawalList['marketCode'];
            $marketUserEmail = $withdrawalList['marketUserEmail'];

            $name = '';
            $clientName = '';

            foreach ($marketNclientDataPack as $item) {
                if(isset($item['code'])){
                    if ($item['code'] === $matchingCode) {
                        $name = $item['name'];
                    }
                }else if(isset($item['email'])){
                    if ($item['email'] === $marketUserEmail) {
                        $clientName = $item['clientName'];
                    }
                }
            }

            $withdrawalList['name'] = $name;
            $withdrawalList['clientName'] = $clientName;
            
            $result[] = $withdrawalList;
        }

        $resultData = ['result'=>'t','data'=>$result];

        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }


    //오른쪽 디테일 창
    public function ApuWithdrowalDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketUserEmail'])||empty($_POST['marketUserEmail'])){
            $errMsg='marketUserEmail 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketUserEmail=$_POST['marketUserEmail'];


        $clientDataPack=ClientMarketListMo::GetClientIDX($marketUserEmail);
        $marketIDX = $clientDataPack['marketIDX'];
        $clientIDX = $clientDataPack['clientIDX'];
      
        $getTargetWithdrawalDataPack=MarketWithdrawalLogMo::GetTargetWithdrawalData($targetIDX);

        $createTime = $getTargetWithdrawalDataPack['createTime'];
        $marketCode = $getTargetWithdrawalDataPack['marketCode'];
        $amount = $getTargetWithdrawalDataPack['amount'];
        $invoiceID = $getTargetWithdrawalDataPack['invoiceID'];
        $statusIDX = $getTargetWithdrawalDataPack['statusIDX'];
        $log = $getTargetWithdrawalDataPack['log'];
        $logDecode = json_decode($log, true);
        if(isset($logDecode['msg'])){
           $parseLog = $logDecode['msg'];
        }else{
            $parseLog = '';
        }

        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $clientAllDataPack=ClientMo::GetTargetClientData($clientIDX);
        $withdrawalCount=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['idxCount'];
        $latestCreateTime=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['createTime'];
     

        $phone = $clientAllDataPack['phone'];
        $name = $clientAllDataPack['name'];
        $createTime = $clientAllDataPack['createTime'];
        $accountNumber = $clientAllDataPack['accountNumber'];

        $renderData= [
            // 'marketIDX'=>$marketIDX,

            'targetIDX'=>$targetIDX,
            'clientIDX'=>$clientIDX,
            'createTime'=>$createTime,
            'marketCode'=>$marketCode,
            'requestAmount'=>$amount,
            'invoiceID'=>$invoiceID,
            'statusIDX'=>$statusIDX,
            'log'=>$parseLog,
            'allAmount'=>$getMileage,
            'phone'=>$phone,
            'name'=>$name,
            'latestCreateTime'=>$latestCreateTime,
            'withdrawalCount'=>$withdrawalCount,
            'createTime'=>$createTime,
            'accountNumber'=>$accountNumber,
            'clientEmail'=>$marketUserEmail
        ];

        View::renderTemplate('page/apiWithdrawal/apiWithdrawalDetail.html',$renderData);
    }

    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=MarketWithdrawalLogMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
}