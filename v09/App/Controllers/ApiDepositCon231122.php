<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\EbuyBankMo;
use App\Models\MarketMo;
use App\Models\MarketDepositLogMo;
use App\Models\ClientMarketListMo;
use App\Models\ClientDetailMo;
use App\Models\ClientMo;
use App\Models\ClientMileageMo;





use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */


class ApiDepositCon extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */

    //렌더
    public function Render($data=null)
    {
        $marketListData=MarketMo::GetMarketListData();
        $arr = ['marketListData'=>$marketListData];
        View::renderTemplate('page/apiDeposit/apiDeposit.html',$arr);
    }

    public function GetMarketDepositAllData()
    {
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'] ?? '';
        $endDate=$_POST['endDate'] ?? '';
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $DepositDataPack=MarketDepositLogMo::GetDepositData($dataArr);
        $getMarketDataPack=MarketMo::GetMarketAllListData(); //마켓정보 가져오기
        $getClientDataPack=ClientMo::GetClientAllListData(); //클라이언트정보 가져오기

        unset($getClientDataPack[0]); 
        unset($getMarketDataPack[0]);

        $marketNclientDataPack = array_merge($getMarketDataPack, $getClientDataPack);

        $result = [];

        foreach ($DepositDataPack as $DepositList) {
            $matchingCode = $DepositList['marketCode'];
            $marketUserEmail = $DepositList['marketUserEmail'];

            $name = '';
            $clientName = '';

            foreach ($marketNclientDataPack as $item) {
                if(isset($item['code'])){
                    if ($item['code'] === $matchingCode) {
                        $name = $item['name'];
                    }
                }else if(isset($item['email'])){
                    if ($item['email'] === $marketUserEmail) {
                        $clientName = $item['clientName'];
                    }
                }
            }

            $DepositList['name'] = $name;
            $DepositList['clientName'] = $clientName;
            
            $result[] = $DepositList;
        }

        $resultData = ['result'=>'t','data'=>$result];

        echo json_encode($resultData,JSON_UNESCAPED_UNICODE);
    }


    //오른쪽 디테일 창
    public function ApuWithdrowalDetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['marketUserEmail'])||empty($_POST['marketUserEmail'])){
            $errMsg='marketUserEmail 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $marketUserEmail=$_POST['marketUserEmail'];


        $clientDataPack=ClientMarketListMo::GetClientIDX($marketUserEmail);
        if(!$clientDataPack){
            $errMsg='이메일 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }

        $marketIDX = $clientDataPack['marketIDX'];
        $clientIDX = $clientDataPack['clientIDX'];
      
        $getTargetDepositDataPack=MarketDepositLogMo::GetTargetDepositData($targetIDX);

        $createTime = $getTargetDepositDataPack['createTime'];
        $marketCode = $getTargetDepositDataPack['marketCode'];
        $amount = $getTargetDepositDataPack['amount'];
        $invoiceID = $getTargetDepositDataPack['invoiceID'];
        $statusIDX = $getTargetDepositDataPack['statusIDX'];
        $log = $getTargetDepositDataPack['log'];
        $logDecode = json_decode($log, true);
        if(isset($logDecode['msg'])){
           $parseLog = $logDecode['msg'];
        }else{
            $parseLog = '';
        }

        $getMileage=ClientDetailMo::getMileage($clientIDX);
        $clientAllDataPack=ClientMo::GetTargetClientData($clientIDX);
        $DepositCount=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['idxCount'];
        $latestCreateTime=ClientMileageMo::GetApiWithdrawalCount($clientIDX)['createTime'];
     

        $phone = $clientAllDataPack['phone'];
        $name = $clientAllDataPack['name'];
        $createTime = $clientAllDataPack['createTime'];
        $accountNumber = $clientAllDataPack['accountNumber'];

        $renderData= [
            // 'marketIDX'=>$marketIDX,

            'targetIDX'=>$targetIDX,
            'clientIDX'=>$clientIDX,
            'createTime'=>$createTime,
            'marketCode'=>$marketCode,
            'requestAmount'=>$amount,
            'invoiceID'=>$invoiceID,
            'statusIDX'=>$statusIDX,
            'log'=>$parseLog,
            'allAmount'=>$getMileage,
            'phone'=>$phone,
            'name'=>$name,
            'latestCreateTime'=>$latestCreateTime,
            'DepositCount'=>$DepositCount,
            'createTime'=>$createTime,
            'accountNumber'=>$accountNumber,
            'clientEmail'=>$marketUserEmail
        ];

        View::renderTemplate('page/apiDeposit/apiDepositDetail.html',$renderData);
    }

    public function ResetDateCount($data=null){
        if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $totalCountData=MarketDepositLogMo::GetTotalCountData($dataArr);
        $resultData = ['data'=>$totalCountData,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }
}