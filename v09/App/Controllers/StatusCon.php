<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\StatusMo;
use App\Models\MdSubUrlMo;
use PDO;
// use App\Models\OmcMenu_M;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class StatusCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */
	//렌더
	public function Render($data=null)
	{

		View::renderTemplate('page/status/status.html');
	}

	//status.html 데이터테이블 리스트 로드
    public function dataTableListLoad()
    {
        $staffList=StatusMo::GetStatusData();
        $resultData = ['data'=>$staffList,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //statusDetail.html 디테일 로드
    public function StatusDetailFormLoad()
    {
        if(!isset($_POST['pageType'])||empty($_POST['pageType'])){
            $errMsg='pageType 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $pageType=$_POST['pageType'];
        if($pageType=='upd'){
            if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
                $errMsg='targetIDX 정보가 없습니다.';
                $errOn=$this::errExport($errMsg,'n');
            }
            $targetIDX=$_POST['targetIDX'];
            $dataPack=StatusMo::GetStatusDetail($targetIDX);
        }else if($pageType=='ins'){
            $dataPack='';
            $targetIDX='';
        }
        $renderData=[
            'targetIDX'=>$targetIDX,
            'pageType'=>$pageType,
            'dataPack'=>$dataPack,
            // 'gradePack'=>$gradePack,
            // 'deptPack'=>$deptPack,
        ];
        View::renderTemplate('page/status/statusDetail.html',$renderData);

    }

    //status.html status 추가
    public function StatusInsert()
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['memo'])||empty($_POST['memo'])){
            $errMsg='memo 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $idx=$_POST['idx'];
        $issetIDXData=StatusMo::GetIssetIDXData($idx);
        if(isset($issetIDXData['idx'])){
        	$errMsg='이미 등록된 idx 입니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $memo=$_POST['memo'];

        //메인db
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("INSERT INTO $dbName.Status
            (idx,memo)
            VALUES
            (:idx,:memo)
        ");
        $stat1->bindValue(':idx', $idx);
        $stat1->bindValue(':memo', $memo);
        $stat1->execute();
        $this->StaffLogInsert(324101,$idx);
        //메인db

        //메인api db
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("INSERT INTO $apiDbName.Status
            (idx,memo)
            VALUES
            (:idx,:memo)
        ");
        $stat2->bindValue(':idx', $idx);
        $stat2->bindValue(':memo', $memo);
        $stat2->execute();
        //메인api db

        //샌드박스db
        $sandboxDb = static::GetSandboxDB();
        $sandboxDbName= self::EbuySandboxDBName;
        $stat3=$sandboxDb->prepare("INSERT INTO $sandboxDbName.Status
            (idx,memo)
            VALUES
            (:idx,:memo)
        ");
        $stat3->bindValue(':idx', $idx);
        $stat3->bindValue(':memo', $memo);
        $stat3->execute();
        //샌드박스db

        //샌드박스 api db
        $EbuySandboxApiDBName= self::EbuySandboxApiDBName;
        $stat4=$sandboxDb->prepare("INSERT INTO $EbuySandboxApiDBName.Status
            (idx,memo)
            VALUES
            (:idx,:memo)
        ");
        $stat4->bindValue(':idx', $idx);
        $stat4->bindValue(':memo', $memo);
        $stat4->execute();
        //샌드박스 api db

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    //status.html status 업데이트
    public function StatusUpdate()
    {
        if(!isset($_POST['idx'])||empty($_POST['idx'])){
            $errMsg='idx 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        if(!isset($_POST['memo'])||empty($_POST['memo'])){
            $errMsg='memo 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $idx=$_POST['idx'];
        $memo=$_POST['memo'];

        //메인db
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.Status SET
            memo=:memo
            WHERE idx=:idx
        ");
        $stat1->bindValue(':idx', $idx);
        $stat1->bindValue(':memo', $memo);
        $stat1->execute();
        $this->StaffLogInsert(324201,$idx);
        //메인db

        //메인api db
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $stat2=$apiDb->prepare("UPDATE $apiDbName.Status SET
            memo=:memo
            WHERE idx=:idx
        ");
        $stat2->bindValue(':idx', $idx);
        $stat2->bindValue(':memo', $memo);
        $stat2->execute();
        //메인api db

        //샌드박스db
        $sandboxDb = static::GetSandboxDB();
        $sandboxDbName= self::EbuySandboxDBName;
        $stat3=$sandboxDb->prepare("UPDATE $sandboxDbName.Status SET
            memo=:memo
            WHERE idx=:idx
        ");
        $stat3->bindValue(':idx', $idx);
        $stat3->bindValue(':memo', $memo);
        $stat3->execute();
        //샌드박스db

        //샌드박스 api db
        $EbuySandboxApiDBName= self::EbuySandboxApiDBName;
        $stat4=$sandboxDb->prepare("UPDATE $EbuySandboxApiDBName.Status SET
            memo=:memo
            WHERE idx=:idx
        ");
        $stat4->bindValue(':idx', $idx);
        $stat4->bindValue(':memo', $memo);
        $stat4->execute();
        //샌드박스 api db

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }


}