<?php

namespace App\Controllers;

use \Core\View;
use \Core\GlobalsVariable;
use App\Models\MarketMo;
use App\Models\MarketKeyMo;
use PDO;

/**
 * Home controller
 *
 * PHP version 7.0
 */

class MarketKeyCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		$marketPack=MarketMo::GetMarketAllListData();
		$renderData=['marketPack'=>$marketPack];
		View::renderTemplate('page/marketKey/marketKey.html',$renderData);
	}

	public function DataTableListLoad($data=null)
	{
		$dataPack=MarketKeyMo::DataTableListLoad();
        $resultData = ['result'=>'t','data'=>$dataPack];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}

	public function MarketKeyInsert($data=null)
	{
		if(!isset($_POST['marketCode'])||empty($_POST['marketCode'])){
            $errMsg='marketCode 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }

        $marketCode=$_POST['marketCode'];
        $issetKeyData=MarketKeyMo::MarketkApiKeyCountByMarketKey($marketCode);
        $marketData=MarketMo::GetMarketCode($marketCode);
        if(!isset($marketData['idx'])||empty($marketData['idx'])){
            $errMsg='market 정보가 없습니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        $marketName=$marketData['name'];
        $marketIDX=$marketData['idx'];
        if($issetKeyData['count']*1>0){
        	$errMsg='이미 최초등록을 한 마켓입니다.';
            $errOn=$this::errExport($errMsg,'n');
        }
        //240121 여기는 최초등록만 가능해서 상태말고 해당 마켓코드에 마켓키가 있는지만 조사하면 될거같아여 제이

        // $issetKeyData=MarketKeyMo::GetTargetMarketKey($marketCode);
        // foreach ($issetKeyData as $key) {
    	// 	$keyIDX=$key['idx'];
    	//     $statusIDX=$key['statusIDX'];
    	//     if($statusIDX==429101){
    	//     	$errMsg='정상인 마켓키가 존재합니다.';
    	//         $errOn=$this::errExport($errMsg,'n');
    	//     }elseif($statusIDX==429201){
    	//     	$errMsg='삭제 대기중인 마켓키가 존재합니다.';
    	//         $errOn=$this::errExport($errMsg,'n');
    	//     }
        // }

    	function ApiKeyString($length)  {
    	    $characters= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    	    $string_generated = "";
    	    $nmr_loops = $length;
    	    while ($nmr_loops--){
    	        $string_generated .= $characters[mt_rand(0, strlen($characters) - 1)];
    	    }
    	    $issetString=MarketKeyMo::MarketkApiKeyChkByMarketKey($string_generated);
    	    if(isset($issetString['idx'])){//혹시나 중복되면 다시만들어라
    	        return GenerateString($length);
    	    }else{
    	        return $string_generated;
    	    }//(db조사해서 자기자신과 중복이 안나올때까지 계속해서 함수를 탐)
    	}

    	$marketKey=ApiKeyString(64);
    	$ip = static::GetIPaddress();
    	$insertStatusIDX = 429101;
    	$managerIDX=0;//임시
    	$createTime=date("Y-m-d H:i:s");

    	$apiDb = static::GetApiDB();
    	$apiDbName= self::EbuyApiDBName;
    	$stat1=$apiDb->prepare("INSERT INTO $apiDbName.MarketKey
    	    (marketCode,marketKey,ip,statusIDX,managerIDX,createTime)
    	    VALUES
    	    (:marketCode,:marketKey,:ip,:statusIDX,:managerIDX,:createTime)
    	");

    	$stat1->bindValue(':marketCode', $marketCode);
    	$stat1->bindValue(':marketKey', $marketKey);
    	$stat1->bindValue(':ip', $ip);
    	$stat1->bindValue(':statusIDX', $insertStatusIDX);
    	$stat1->bindValue(':managerIDX', $managerIDX);
    	$stat1->bindValue(':createTime', $createTime);
    	$stat1->execute();
    	$joinIDX=$apiDb->lastInsertId();


    	$stringLength = strlen($marketKey);
        $emailApi = str_repeat('*', $stringLength - 10) . substr($marketKey, -10);
    	$emailParamVal = [
            'marketname'=>$marketName,
            'api'=>$emailApi
        ];
    	$emailParam=[
            'targetIDX'=>$joinIDX,
            'statusIDX'=>$insertStatusIDX,
            'marketIDX'=>$marketIDX, //모든 마켓에게 다 보내려면 0으로 보내주세요
            'param'=>$emailParamVal
        ];
        StaffEmailCon::sendEmailToPortal($emailParam);

        $resultData = ['result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
	}



}