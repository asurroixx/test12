<?php

namespace App\Controllers;

use PDO;
use \Core\View;
use \Core\GlobalsVariable;
use App\Models\ManagerLoginHistoryMo;
use App\Models\MarketManagerMo;
/**
 * Home controller
 *
 * PHP version 7.0
 */

class ManagerLoginHistoryCon extends \Core\Controller
{

	/**
	 * Show the index page
	 *
	 * @return void
	 */

	public function Render($data=null)
	{
		View::renderTemplate('page/managerLoginHistory/managerLoginHistory.html');
	}//렌더

	public function dataTableListLoad()
    {
    	if(!isset($_POST['startDate'])||empty($_POST['startDate'])){
            $errMsg='startDate 정보가 없습니다.';
            $errOn=static::errExport($errMsg);
        }
        if(!isset($_POST['endDate'])||empty($_POST['endDate'])){
            $errMsg='endDate 정보가 없습니다.';
            $errOn=static::errExport($errMsg);
        }
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];

        $dataArr=array(
            'startDate'  => $startDate,
            'endDate'    => $endDate
        );
        $dataPack=ManagerLoginHistoryMo::GetDatatableList($dataArr);
        $resultData = ['data'=>$dataPack,'result'=>'t'];
        $result=json_encode($resultData,JSON_UNESCAPED_UNICODE);
        echo $result;
    }

    public function DetailFormLoad()
    {
        if(!isset($_POST['targetIDX'])||empty($_POST['targetIDX'])){
            $errMsg='targetIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['managerIDX'])||empty($_POST['managerIDX'])){
            $errMsg='managerIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        if(!isset($_POST['email'])||empty($_POST['email'])){
            $errMsg='email 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $targetIDX=$_POST['targetIDX'];
        $managerIDX=$_POST['managerIDX'];
        $email=$_POST['email'];
        $dataPack=MarketManagerMo::GetDetail($managerIDX);
        $renderData=[
            'targetIDX'=>$targetIDX,
            'email'=>$email,
            'dataPack'=>$dataPack,
        ];
        View::renderTemplate('page/managerLoginHistory/managerLoginHistoryDetail.html',$renderData);
    }

}