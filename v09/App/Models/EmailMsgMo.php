<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class EmailMsgMo extends \Core\Model
{
	//ClientGradeCon 데이터테이블
	public static function getEmailMsg($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$statusIDX = $data;
		$query = $db->query("SELECT
			idx,
			title,
			con,
			param
		FROM $dbName.EmailMsg
		WHERE statusIDX = '$statusIDX'
		");
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
    
}