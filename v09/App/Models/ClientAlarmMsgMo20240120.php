<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientAlarmMsgMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//ClientGradeCon 데이터테이블
	public static function getClientAlarmMsg($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$statusIDX = $data;
		$db = static::GetDB();
		$query = $db->query("SELECT
			idx,
			title,
			con,
			param
		FROM $dbName.ClientAlarmMsg
		WHERE statusIDX = '$statusIDX'
		");
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
    
}