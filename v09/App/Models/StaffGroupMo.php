<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffGroupMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//StaffGradeCon 스태프 등급별 퍼미션 가져오기
	public static function GetPermissionData($data=null)
	{
		$targetIDX=$data;
		$db = static::GetDB();
		$CategoryList = $db->query("SELECT
		idx,
		permission
		FROM ebuy.StaffGroup
		WHERE gradeIDX='$targetIDX'
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//GlobalsVariable page이름으로 idx 가져오기
	public static function getMenuIDX($data=null)
    {
        $url = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM ebuy.StaffMenu
        WHERE url='$url'
        AND idx NOT IN (SELECT momIDX FROM ebuy.StaffMenu)
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //GlobalsVariable 로그인한 idx의 등급과 page로 퍼미션 가져오기
	public static function issetGroupPermission($data=null)
    {
        $menuIDX = $data['menuIDX'];
        $loginIDX = $data['loginIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        B.idx,
        B.permission
        FROM ebuy.Staff AS A
        LEFT JOIN ebuy.StaffGroup AS B
        ON A.gradeIDX = B.gradeIDX
        WHERE B.menuIDX = '$menuIDX'
        AND B.gradeIDX IN (SELECT gradeIDX FROM ebuy.Staff WHERE idx = '$loginIDX')
        ");

        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    
}