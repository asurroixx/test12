<?php

namespace App\Models;

use PDO;

class ClientPeerToPeerMo extends \Core\Model
{
    public static function GetClientPeerToPeerData($data=null)
    {

        // -- 251101  1단계: 시작 마일리지 입금신청 같이 됨
        // -- 251102  1단계: 시작 마일리지가 있나 봄
        // -- 251201  2단계: 구매자 마일리지 차감 완료 / 판매자 전자화폐 송금중 << 구매자 ClientMileage 차감 코드
        // -- 251301  3단계: 판매자 송금완 / 구매자 전자화폐 지갑 확인중(생략가능)
        // -- 251401  끝: 종료 구매자 확인완 / 피투피거래종료(완료) << 판매자 ClientMileage 증가 하는 코드
        // -- 251402  끝: 거래취소 << 구매자가 예치한 마일리지 반환함 << 구매자 ClientMileage 증가 하는 코드
        // -- 251403  끝: 거래취소 << 구매자 마일리지 예치하기 전에 취소하는것 마일리지 증차감 없음
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx
        -- ,IFNULL(DATE_ADD(A.completeTime, INTERVAL 9 HOUR), '-') AS completeTime
        ,IFNULL(A.completeTime, '-') AS completeTime
        -- ,DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime
        ,A.createTime

        ,A.amount
        ,A.rate

        ,A.BuyerClientIDX
        ,A.SellerClientIDX

        ,AES_DECRYPT(B.account,:dataDbKey) AS BuyerClientPtpAccount
        ,B.PeerToPeerTypeIDX

        ,(SELECT name AS typeName FROM $dbName.PeerToPeerType WHERE B.PeerToPeerTypeIDX=idx) AS typeName


        ,AES_DECRYPT(Buyer.name,:dataDbKey) AS BuyerName
        ,AES_DECRYPT(Buyer.email,:dataDbKey) AS BuyerClientEmail

        ,AES_DECRYPT(Seller.name,:dataDbKey) AS SellerName
        ,AES_DECRYPT(Seller.email,:dataDbKey) AS SellerClientEmail

        ,A.statusIDX
        -- ,(SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status
        ,CASE A.statusIDX
            WHEN 251401 THEN '성공'
            WHEN 251402 THEN '취소'
            WHEN 251403 THEN '취소'
            ELSE (SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx)
        END AS status

        ,CAST(A.amount * A.rate AS SIGNED) AS totalPrice
        ,CAST(A.amount * A.rate AS SIGNED) + A.BuyerFee AS BuyerPrice
        ,CAST(A.amount * A.rate AS SIGNED) - A.SellerFee AS SellerPrice

        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientPtpAccount AS B ON A.BuyerClientPtpAccountIDX=B.idx

        INNER JOIN $dbName.Client AS Buyer ON A.BuyerClientIDX=Buyer.idx

        INNER JOIN $dbName.Client AS Seller ON A.SellerClientIDX=Seller.idx

        WHERE (A.createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function GetDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx
        -- ,IFNULL(DATE_ADD(A.completeTime, INTERVAL 9 HOUR), '-') AS completeTime
        ,IFNULL(A.completeTime, '-') AS completeTime
        -- ,DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime
        ,A.createTime

        ,A.amount
        ,A.rate

        ,A.BuyerClientIDX
        ,A.SellerClientIDX

        ,AES_DECRYPT(B.account,:dataDbKey) AS BuyerClientPtpAccount
        ,B.PeerToPeerTypeIDX

        ,(SELECT name AS typeName FROM $dbName.PeerToPeerType WHERE B.PeerToPeerTypeIDX=idx) AS typeName


        ,AES_DECRYPT(Buyer.name,:dataDbKey) AS BuyerName
        ,AES_DECRYPT(Buyer.phone,:dataDbKey) AS BuyerPhone
        ,AES_DECRYPT(Buyer.email,:dataDbKey) AS BuyerClientEmail
        ,BuyerGrade.name AS BuyerGradeName
        ,A.BuyerFeePer
        ,A.BuyerFee

        ,AES_DECRYPT(Seller.name,:dataDbKey) AS SellerName
        ,AES_DECRYPT(Seller.phone,:dataDbKey) AS SellerPhone
        ,AES_DECRYPT(Seller.email,:dataDbKey) AS SellerClientEmail
        ,SellerGrade.name AS SellerGradeName
        ,A.SellerFeePer
        ,A.SellerFee

        ,A.statusIDX
        ,(SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status


        ,CAST(A.amount * A.rate AS SIGNED) AS totalPrice
        ,CAST(A.amount * A.rate AS SIGNED) + A.BuyerFee AS BuyerPrice
        ,CAST(A.amount * A.rate AS SIGNED) - A.SellerFee AS SellerPrice
        ,Buyer.idx AS clientIDX
        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientPtpAccount AS B ON A.BuyerClientPtpAccountIDX=B.idx

        INNER JOIN $dbName.Client AS Buyer ON A.BuyerClientIDX=Buyer.idx
        INNER JOIN $dbName.ClientGrade AS BuyerGrade ON Buyer.gradeIDX=BuyerGrade.idx

        INNER JOIN $dbName.Client AS Seller ON A.SellerClientIDX=Seller.idx
        INNER JOIN $dbName.ClientGrade AS SellerGrade ON Seller.gradeIDX=SellerGrade.idx

        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function issetPtp($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx
        ,A.statusIDX
        ,(SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status

        ,A.BuyerClientIDX
        ,A.SellerClientIDX

        ,AES_DECRYPT(B.account,:dataDbKey) AS BuyerClientPtpAccount
        ,(SELECT name AS typeName FROM $dbName.PeerToPeerType WHERE B.PeerToPeerTypeIDX=idx) AS typeName

        ,A.amount
        ,A.rate

        ,AES_DECRYPT(Buyer.name,:dataDbKey) AS BuyerName
        ,AES_DECRYPT(Seller.name,:dataDbKey) AS SellerName

        ,CAST(A.amount * A.rate AS SIGNED) AS totalPrice
        ,CAST(A.amount * A.rate AS SIGNED) + A.BuyerFee AS BuyerPrice
        ,CAST(A.amount * A.rate AS SIGNED) - A.SellerFee AS SellerPrice

        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientPtpAccount AS B ON A.BuyerClientPtpAccountIDX=B.idx

        INNER JOIN $dbName.Client AS Buyer ON A.BuyerClientIDX=Buyer.idx

        INNER JOIN $dbName.Client AS Seller ON A.SellerClientIDX=Seller.idx

        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function getBuyerDepositStatus($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        A.idx
        ,A.statusIDX

        ,A.BuyerClientMileageDepositIDX

        ,B.statusIDX AS BuyerClientMileageDepositStatusIDX
        ,(SELECT memo AS status FROM $dbName.Status WHERE B.statusIDX=idx) AS BuyerClientMileageDepositStatus

        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientMileageDeposit AS B ON A.BuyerClientMileageDepositIDX=B.idx

        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    //20240222 이것은 구매자의 입금신청이 완료되었나~ 안되었나~ 찾는 친구에요 민잡에서 쓰이고 있죠.
    public static function PtpNextCheckJob($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        A.idx
        ,A.BuyerClientIDX
        ,A.BuyerFee
        ,A.SellerClientIDX
        ,A.rate
        ,A.amount

        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientMileageDeposit AS B ON A.BuyerClientMileageDepositIDX=B.idx

        WHERE A.statusIDX=:targetStatusIDX

        AND (
            B.statusIDX=:DepositSuc1StatusIDX
            OR B.statusIDX=:DepositSuc2StatusIDX
            OR B.statusIDX=:DepositSuc3StatusIDX
        )

        LIMIT 1
        ");
        $query->bindValue(':targetStatusIDX', 251101);
        $query->bindValue(':DepositSuc1StatusIDX', 203201);
        $query->bindValue(':DepositSuc2StatusIDX', 203202);
        $query->bindValue(':DepositSuc3StatusIDX', 203203);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}