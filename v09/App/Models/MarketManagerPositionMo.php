<?php

namespace App\Models;

use PDO;

class MarketManagerPositionMo extends \Core\Model
{

    //MarketManagerCon 파트너 기본 리스트
    public static function GetPositionListData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        name
        FROM $dbName.MarketManagerPosition
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



}