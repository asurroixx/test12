<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientBankMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */


	//ClientCon 데이터테이블
	public static function GetDataTableListLoad($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		A.createTime,
		AES_DECRYPT(A.accountNumber,:dataDbKey) AS accountNumber,
		AES_DECRYPT(A.accountHolder,:dataDbKey) AS accountHolder,
		B.name AS bankName,
		AES_DECRYPT(C.name,:dataDbKey) AS name,
		AES_DECRYPT(C.birth,:dataDbKey) AS birth,
		-- AES_DECRYPT(C.phone,:dataDbKey) AS phone,
		CONCAT(
		    SUBSTRING(AES_DECRYPT(C.phone,:dataDbKey), 1, 3), '-', -- 국번
		    SUBSTRING(AES_DECRYPT(C.phone,:dataDbKey), 4, 4), '-', -- 가운데 번호
		    SUBSTRING(AES_DECRYPT(C.phone,:dataDbKey), 8, 4)       -- 끝 번호
		) AS phone,
		CASE
			WHEN C.statusIDX = '226201' THEN '탈퇴'
           	WHEN (A.createTime) IN (
               	SELECT MAX(A.createTime)
		    	FROM $dbName.ClientBank AS A
		    	GROUP BY A.clientIDX
           ) THEN '활성'
           ELSE '비활성'
        END AS status,
        C.idx AS clientIDX
		FROM $dbName.ClientBank AS A
        INNER JOIN $dbName.Bank AS B ON(A.bankIDX = B.idx)
		LEFT JOIN $dbName.Client AS C ON (A.clientIDX = C.idx)
        ORDER BY A.createTime DESC
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}






}



