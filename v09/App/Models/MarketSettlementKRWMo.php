<?php

namespace App\Models;

use PDO;

class MarketSettlementKRWMo extends \Core\Model
{

    //MarketSettlementCon 해당 마켓 KRW 정보
    public static function GetSettlementKRWData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.accountNumber,
        A.beneficiaryName,
        A.createTime,
        B.name
        FROM $dbName.MarketSettlementKRW AS A
        JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
        WHERE A.marketIDX='$marketIDX' AND A.statusIDX=427101
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketSettlementRequestCon 정산신청시에 해당정보
    public static function GetMarketSettlementInfo($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.accountNumber,
        A.beneficiaryName,
        A.createTime,
        B.name
        FROM $dbName.MarketSettlementKRW AS A
        JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
        WHERE A.idx='$idx'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}