<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMenuMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//MenuCon 메뉴 숨김 status 조사
	public static function CheeseEyeAction($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM $MainDBName.$BasicTable
        WHERE idx='$targetIDX' ORDER BY seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 컨펌창 열때 정보 불러오기
	public static function MenuConfirmData($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		name,
		url
		FROM $MainDBName.$BasicTable
        WHERE idx='$targetIDX' ORDER BY seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 삭제 전 2차메뉴가 있는지 조사
	public static function MenuDeleteCheck($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM $MainDBName.$BasicTable
        WHERE momIDX='$targetIDX'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//StaffGradeCon , MenuCon , NavCon 권한에 따른 1차메뉴
	public static function GetGradeMenuList($data=null)
	{
		$targetIDX=$data;
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$db = static::GetDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.url,
		A.seq,
		A.status,
		A.memo,
		B.permission
		FROM $MainDBName.$BasicTable AS A
		LEFT JOIN ebuy.StaffGroup AS B ON A.idx=B.menuIDX
        WHERE A.momIDX=0 AND B.gradeIDX='$targetIDX' ORDER BY A.seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StaffGradeCon , MenuCon , NavCon 권한에 따른 2차메뉴
	public static function GetChildGradeMenuList($data=null)
	{
		$menuIDX=$data['menuIDX'];
		$gradeIDX=$data['gradeIDX'];
		$loginIDX=$data['loginIDX'];
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.url,
		A.seq,
		A.status,
		A.memo,
		B.permission,
		COALESCE(C.count, 0) AS count
		FROM ebuy.StaffMenu AS A
		LEFT JOIN ebuy.StaffGroup AS B ON A.idx=B.menuIDX
		LEFT JOIN (
			SELECT menuIDX, COUNT(idx) AS count
			FROM ebuy.StaffAlarm
			WHERE staffIDX = '$loginIDX' AND viewStatusIDX = 305201
			GROUP BY menuIDX
		) AS C ON A.idx=C.menuIDX
        WHERE A.momIDX='$menuIDX' AND B.gradeIDX='$gradeIDX'
        GROUP BY A.idx
		ORDER BY A.seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StaffGradeCon 등급 추가할때 모든 메뉴 idx 가져올때
	public static function GetAllMenuIDXData($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenu'];
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx
		FROM $MainDBName.$BasicTable
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}



	// //NavCon 2차메뉴 리스트 원본
	// public static function GetChildMenuListByPermission($data=null)
	// {
	// 	$MainDBName=self::MainDBName;
	// 	$menuIDX=$data['menuIDX'];
	// 	$gradeIDX=$data['gradeIDX'];
	// 	$loginIDX=$data['loginIDX'];
	// 	$db = static::getDB();
	// 	$CategoryList = $db->query("SELECT
	// 	A.idx,
	// 	A.momIDX,
	// 	A.name,
	// 	A.url,
	// 	A.seq,
	// 	A.status,
	// 	A.memo
	// 	B.permission
	// 	COALESCE(C.count, 0) AS count
	// 	FROM $MainDBName.StaffMenu AS A
	// 	INNER JOIN $MainDBName.AdminGroup AS B ON A.idx = B.MenuIDX
	// 	LEFT JOIN (
	// 		SELECT menuIDX, COUNT(idx) AS count
	// 		FROM $MainDBName.StaffAlarm
	// 		WHERE staffIDX = '$loginIDX' AND status = 2
	// 		GROUP BY menuIDX
	// 	) AS C ON A.idx=C.menuIDX
	// 	WHERE  A.momIDX='$menuIDX' AND B.MdGradeIDX='$gradeIDX'
	// 	GROUP BY A.idx
	// 	ORDER BY A.seq
	// 	");
	// 	$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
	// 	return $result;
	// }






    
}