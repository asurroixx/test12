<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffLoginDumpMo extends \Core\Model
{
    //StaffCon 로그인히스토리
    public static function getCode($data=null)
    {
        $db = static::getDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $email = $data;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        A.code
        FROM $dbName.StaffLoginDump AS A
        INNER JOIN $dbName.Staff AS B ON(A.staffIDX = B.idx)
        WHERE B.email = AES_ENCRYPT(:email,:dataDbKey)
        ORDER BY A.createTime DESC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':email', $email);
        $Sel->execute();
        $Sel=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Sel;
    }
}
