<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffGradeMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//StaffGradeCon 스태프 등급 데이터테이블 , StaffCon 등급 리스트
	public static function GetStaffGradeList($data=null)
	{
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		idx,
		name
		FROM $dbName.StaffGrade
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StaffGradeCon 스태프 등급 디테일
	public static function GetStaffGradeDetail($data=null)
	{
		$idx=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		idx,
		name
		FROM $dbName.StaffGrade
		WHERE idx='$idx'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//StaffGradeCon 모든정보 유효성 idx기준
    public static function issetStaffGradeData($data=null)
    {

        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name
        FROM ebuy.StaffGrade
        WHERE idx='$targetIDX'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    
}