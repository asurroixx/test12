<?php

namespace App\Models;

use PDO;

class MarketWithdrawalLogMo extends \Core\Model
{
    public static function GetWithdrawalData($data=null)
    {
        $startDate  = $data['startDate'] ?? '1970-01-01';
        $endDate    = $data['endDate'] ?? date('Y-m-d');
        $startDate .= ' 00:00:00';
        $endDate   .= ' 23:59:59';

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT 
            COALESCE(( -- 908102(위드드로우 성공)의 idx 가져오기, 없으면 에러콜백 아닌 MAX(idx)
                SELECT idx
                FROM $dbName.MarketWithdrawalLog
                WHERE invoiceIDunique = A.invoiceIDunique
                AND marketCode = A.marketCode
                AND statusIDX = '908102'
            ), ( -- 성공의 IDX 없으면 에러 콜백 IDX 아닌것중 제일 최근
                SELECT MAX(idx)
                FROM $dbName.MarketWithdrawalLog
                WHERE invoiceIDunique = A.invoiceIDunique
                AND marketCode = A.marketCode
                AND statusIDX NOT LIKE '9083%' -- 에러 콜백
            )) AS idx,
            ROW_NUMBER() OVER (ORDER BY A.createTime,A.idx ASC) AS no,
            ci,
            marketCode,
            amount,
            invoiceID,
            marketUserEmail,
            MIN(createTime) AS createTime,
            MAX(mileage) AS mileage,
            MAX(afterMileage) AS afterMileage,
            MAX(afterMileage) - MAX(mileage) AS beforeMileage,
            COALESCE( -- 콜백타임이 있다면 콜백, 없으면 성공의 타임
                MAX(CASE WHEN statusIDX = '908102' THEN createTime END),
                MAX(CASE WHEN statusIDX = '908103' THEN createTime END), 
            '-') AS appTime,
            CASE
                WHEN EXISTS ( -- 성공과 콜백이 있으면 000 없으면 조건 아닌애들중 맥스
                    SELECT 1
                    FROM $dbName.MarketWithdrawalLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND (statusIDX = '908102' OR statusIDX = '908103')
                    ) THEN '000'
                ELSE MAX(CASE WHEN statusIDX NOT IN ('908102', '908103', '908301') THEN statusIDX END)
            END AS errorCode,
            CASE
                WHEN MAX(statusIDX) LIKE '9082%' THEN 'Cancelled' -- 실패
                WHEN MAX(statusIDX) LIKE '9083%' THEN 'Cancelled' -- 실패 콜백
                WHEN MAX(statusIDX) = '908101' THEN 'In Progress' -- 진행중
                WHEN MAX(statusIDX) IN ('908102', '908103') THEN 'Completed' -- 성공,성공콜백
                ELSE MAX(statusIDX)
            END AS status,
            CASE
                WHEN orderLocation = 2 THEN 'app'
                WHEN orderLocation = 3 THEN 'web'
                ELSE '-'
            END AS orderLocation
            FROM $dbName.MarketWithdrawalLog AS A
            WHERE (createTime BETWEEN '$startDate' AND '$endDate')
            GROUP BY invoiceIDunique, marketCode
        ");
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetTargetWithdrawalData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
            idx,
            ci,
            marketCode,
            amount,
            MAX(mileage) AS mileage,
            MAX(afterMileage) AS afterMileage,
            MAX(afterMileage) - MAX(mileage) AS beforeMileage,
            invoiceID,
            invoiceIDunique,
            log,
            marketFeePer,
            IF(MAX(balance) = 0, 0, amount - MAX(balance)) AS marketFeeCash,
            clientFeePer,
            marketUserEmail,
            ex1,
            ex2,
            ex3,
            CASE
                WHEN statusIDX = '908101' THEN '진행중'
                WHEN statusIDX LIKE '9081%' THEN '성공'
                WHEN statusIDX LIKE '9082%' THEN '실패'
                ELSE statusIDX
            END AS status,
            CASE
                WHEN statusIDX IN ('908102','908103') THEN '000'
                ELSE statusIDX
            END AS errorCode,
            CONCAT( -- 디파짓 신청한 UTC 시간
            COALESCE(
                CONCAT('UTC : ',
                    (
                        SELECT createTime
                        FROM $dbName.MarketWithdrawalLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                        AND statusIDX = 908101
                        ORDER BY createTime DESC
                        LIMIT 1
                        )
                    ), (
                    SELECT CONCAT('UTC : ', MIN(createTime))
                    FROM $dbName.MarketWithdrawalLog
                    WHERE marketCode = A.marketCode
                    AND invoiceIDunique = A.invoiceIDunique
                    ), '-'
                    )
            ) AS utcCreateTime,
            CONCAT(
                COALESCE(
                    CONCAT('UTC : ',
                        (
                            SELECT createTime
                            FROM $dbName.MarketWithdrawalLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 908102
                            ORDER BY createTime DESC
                            LIMIT 1
                            )
                        ), '-'
                    )
                ) AS utcAppTime,
            CONCAT(
                COALESCE(
                    CONCAT('UTC : ',
                        (
                            SELECT createTime
                            FROM $dbName.MarketWithdrawalLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 908103
                            ORDER BY createTime DESC
                            LIMIT 1
                            )
                        ), '-'
                    )
                ) AS utcCallBackTime,
            COALESCE( -- 디파짓 신청한 KST 시간
                CONCAT('KST : ', DATE_ADD(
                    COALESCE(
                        (
                            SELECT createTime
                            FROM $dbName.MarketWithdrawalLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 908101
                            ORDER BY createTime DESC
                            LIMIT 1
                        ), (
                            SELECT MIN(createTime)
                            FROM $dbName.MarketWithdrawalLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                        )
                    ), INTERVAL 9 HOUR)
                ), '-') AS kstCreateTime,
            COALESCE(
                CONCAT('KST : ', DATE_ADD(
                    (
                        SELECT createTime
                        FROM $dbName.MarketWithdrawalLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                        AND statusIDX = 908102
                        ORDER BY createTime DESC
                        LIMIT 1
                        ), INTERVAL 9 HOUR)
                ), '-') AS kstAppTime,
            COALESCE(
                CONCAT('KST : ', DATE_ADD(
                    (
                        SELECT createTime
                        FROM $dbName.MarketWithdrawalLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                        AND statusIDX = 908103
                        ORDER BY createTime DESC
                        LIMIT 1
                        ), INTERVAL 9 HOUR)
                ), '-') AS kstCallBackTime
        FROM $dbName.MarketWithdrawalLog AS A
        WHERE idx = '$targetIDX'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $Sel = $db->prepare("SELECT
            marketCode,
            invoiceID,
            COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = '908101' THEN idx END) AS successTotalCount,
            COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX LIKE '9082%' THEN idx END) AS failTotalCount,
            IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = '908101' THEN amount ELSE 0 END), 0) AS successAmount
            
        FROM $dbName.MarketWithdrawalLog
        WHERE createTime BETWEEN '$startDate' AND '$endDate'


        /*성공 및 실패건만 카운트 하기 (신청건은 카운트 미포함)*/
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetLogList($data=null)
    {
        $invoiceID  = $data['invoiceID'];
        $marketCode = $data['marketCode'];
        $whereType  = $data['whereType'];
        $whereArr   = $data['whereArr'];

        $whereQuery='';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
                    $endRange = $whereArr['end'].'999';
                    $whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.createTime,
        A.log,
        B.memo
        FROM $dbName.MarketWithdrawalLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        WHERE A.invoiceIDunique = '$invoiceID'
        AND A.marketCode = '$marketCode'
        $whereQuery
        ORDER BY A.createTime DESC, A.statusIDX ASC;
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        
        // 로그에서 데이터뿌릴때 ' 를 " 로 변경
        foreach ($result as &$row) {
            $row['log'] = str_replace("'", '"', $row['log']);
        }

        return $result;
    }

}   
