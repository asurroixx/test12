<?php

namespace App\Models;

use PDO;

class MarketManagerMo extends \Core\Model
{

    //MarketManagerCon 정보 불러오기
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 402101 THEN '정상'
                WHEN 402201 THEN '비활성'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        AES_DECRYPT(A.name,:dataDbKey) AS marketManagerName,
        A.marketIDX,
        A.createTime,
        A.position,
        A.gradeIDX,
        B.code,
        B.name AS marketName,
        AES_DECRYPT(C.email,:dataDbKey) AS email,
        D.name AS gradeName,
        E.name AS parterName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        JOIN $dbName.Manager AS C ON A.managerIDX=C.idx
        JOIN $dbName.MarketManagerGrade AS D ON A.gradeIDX=D.idx
        JOIN $dbName.Partner AS E ON B.partnerIDX=E.idx
        WHERE A.statusIDX IN (402101 , 402201)
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    //MarketManagerCon 디테일
    public static function GetGradeDetail($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 402101 THEN '정상'
                WHEN 402201 THEN '비활성'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.marketIDX,
        A.managerIDX,
        A.position,
        AES_DECRYPT(A.name,:dataDbKey) AS marketManagerName,
        A.createTime,
        B.code,
        B.name AS marketName,
        AES_DECRYPT(C.email,:dataDbKey) AS email,
        D.name AS gradeName,
        E.name AS parterName,
        E.code AS parterCode
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        JOIN $dbName.Manager AS C ON A.managerIDX=C.idx
        JOIN $dbName.MarketManagerGrade AS D ON A.gradeIDX=D.idx
        JOIN $dbName.Partner AS E ON B.partnerIDX=E.idx
        WHERE A.idx='$idx' AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 슈퍼등급이 있는지 조사
    public static function issetManagerGradeData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        COUNT(A.idx) AS count,
        B.name
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE A.marketIDX=:marketIDX AND A.gradeIDX=1 AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':marketIDX', $marketIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 슈퍼등급이 있는지 조사 2 (업데이트 시) , 슈퍼등급 양도해줄때 마켓에 등록된 슈퍼등급 idx 조사
    public static function issetSuperManagerGradeData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.gradeIDX,
        B.name,
        AES_DECRYPT(C.email,:dataDbKey) AS email,
        D.name AS gradeName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        JOIN $dbName.Manager AS C ON A.managerIDX=C.idx
        JOIN $dbName.MarketManagerGrade AS D ON A.gradeIDX=D.idx
        WHERE A.marketIDX=:marketIDX AND A.gradeIDX=1 AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':marketIDX', $marketIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 동일한 마켓에 동일한 이메일이 있는지 조사
    public static function issetManagerData($data=null)
    {
        $marketIDX=$data['marketIDX'];
        $managerIDX=$data['managerIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        idx
        FROM $dbName.MarketManager
        WHERE marketIDX='$marketIDX' AND managerIDX='$managerIDX' AND statusIDX IN (402101 , 402201)
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 업데이트 상태가 가능한 데이터 조사
    public static function GetTargetUpdateData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.gradeIDX,
        A.position,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        B.name AS gradeName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.MarketManagerGrade AS B ON A.gradeIDX = B.idx
        WHERE A.idx='$targetIDX' AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 마켓에 등록된 매니저 리스트
    public static function GetMarketManagerList($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.marketIDX,
        A.gradeIDX,
        A.position,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(B.email,:dataDbKey) AS email,
        C.name AS gradeName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Manager AS B ON A.managerIDX=B.idx
        JOIN $dbName.MarketManagerGrade AS C ON A.gradeIDX=C.idx
        WHERE A.marketIDX=:marketIDX AND A.gradeIDX!=1 AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':marketIDX', $marketIDX);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getMarketManagerListForAlarm($data=null)
    {
        $statusIDX=$data['statusIDX'];
        $marketIDX=$data['marketIDX'];
        if($marketIDX>0){
            $query='WHERE A.marketIDX = '.$marketIDX;
        }else{
            $query='';
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        B.idx AS alarmSettingIDX
        FROM $dbName.MarketManager AS A
        LEFT JOIN $dbName.PortalAlarmSetting AS B ON (A.idx=B.marketManagerIDX AND B.statusIDX='$statusIDX')
        ".$query);
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //샌드박스 알람 받을 매니저IDX
    public static function getMarketManagerListForSandboxAlarm($data=null)
    {

        // 마켓idx = 26 , statusIDX=340101
        $statusIDX=$data['statusIDX'];
        $marketIDX=$data['marketIDX'];
        if($marketIDX>0){
            $query='WHERE C.targetMarketIDX = '.$marketIDX;
        }else{
            $query='';
        }
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $Sel = $db->query("SELECT
        A.idx,
        B.idx AS alarmSettingIDX
        FROM $dbName.MarketManager AS A
        LEFT JOIN $dbName.PortalAlarmSetting AS B ON (A.idx=B.marketManagerIDX AND B.statusIDX='$statusIDX')
        JOIN $dbName.Market AS C ON A.marketIDX=C.idx
        ".$query);
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function getMarketManagerListForEmail($data=null)
    {

        $statusIDX=$data['statusIDX'];
        $marketIDX=$data['marketIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        B.idx AS emailSettingIDX,
        AES_DECRYPT(C.email,:dataDbKey) AS email
        FROM $dbName.MarketManager AS A
        LEFT JOIN $dbName.PortalEmailSetting AS B ON (A.idx=B.marketManagerIDX AND B.statusIDX=:statusIDX)
        INNER JOIN $dbName.Manager AS C ON (A.managerIDX=C.idx)
        WHERE A.statusIDX = 402101 AND A.marketIDX = :marketIDX");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':statusIDX', $statusIDX);
        $Sel->bindValue(':marketIDX', $marketIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



    //MarketManagerEmailCon 해당 마켓의 모든 마켓매니저 리스트
    public static function GetMarketManagerListLoad($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(B.email,:dataDbKey) AS email
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Manager AS B ON A.managerIDX=B.idx
        WHERE A.marketIDX='$targetIDX' AND A.statusIDX=402101
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }




    ///////샌드박스
     public static function getMarketManagerIDXInSandbox($data=null)
    {
        $dbsand = static::GetSandboxDB();
        $dbNameSand= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $email=$data['email'];
        $marketIDX=$data['marketIDX'];
        $query = $dbsand->prepare("SELECT
            A.idx
            FROM $dbNameSand.MarketManager AS A
            INNER JOIN $dbNameSand.Manager AS B ON A.managerIDX = B.idx
            WHERE (A.statusIDX=402101 OR A.statusIDX=402201) AND AES_DECRYPT(B.email,:dataDbKey)=:email AND A.marketIDX =:marketIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':email', $email);
        $query->bindValue(':marketIDX', $marketIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    //ManagerLoginHistoryCon
    public static function GetDetail($data=null)
    {
        $managerIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        CASE WHEN A.statusIDX = 402101 THEN '활성'
             WHEN A.statusIDX = 402201 THEN '비활성'
             WHEN A.statusIDX = 402202 THEN '삭제'
             ELSE 'unknown'
        END AS statusName,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        C.name AS marketName,
        C.code AS marketCode,
        D.name AS parterName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Manager AS B ON A.managerIDX=B.idx
        JOIN $dbName.Market AS C ON A.marketIDX=C.idx
        JOIN $dbName.Partner AS D ON C.partnerIDX=D.idx
        WHERE A.managerIDX=:managerIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':managerIDX', $managerIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


      //QnaCon 슈퍼등급idx 가져오기
    public static function getSuperManager($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx ,
        B.name AS marketName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE A.marketIDX=:marketIDX AND A.gradeIDX=1 AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':marketIDX', $marketIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}