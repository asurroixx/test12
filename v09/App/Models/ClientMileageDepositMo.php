<?php

namespace App\Models;

use PDO;

class ClientMileageDepositMo extends \Core\Model
{



    //MileageDepositCon 날짜별 데이터 리셋
    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (203201, 203202, 203203) THEN idx END) AS successTotalCount,
        COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (203211, 203212, 203213, 203214, 203215) THEN idx END) AS failTotalCount,
        IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (203201, 203202, 203203) THEN amount ELSE 0 END), 0) AS successAmount,
        IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (203211, 203212, 203213, 203214, 203215) THEN amount ELSE 0 END), 0) AS failureAmount
        FROM ebuy.ClientMileageDeposit
        WHERE createTime BETWEEN '$startDate' AND '$endDate';
        /*성공 및 실패건만 카운트 하기 (신청건은 카운트 미포함)*/
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 솔팅별 데이터 리셋
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data['columnsVal'];


        $firstString = $columnsVal[0];
        $secondString=$columnsVal[1];
        $thirdString=$columnsVal[2];

        $firstArray = explode('|', $firstString);
        $secondArray = explode('|', $secondString);
        $thirdArray = explode('|', $thirdString);

        $firstQuery = 'AND C.name IN (';
        $secondQuery = 'AND A.statusIDX IN (';
        $thirdQuery = 'AND (A.amount ';

        // if(isset($firstString)&&!empty($firstString)){
        //     $secondQuery = 'AND A.statusIDX IN (';
        //     $thirdQuery = 'AND (A.amount ';
        // }

        // if(isset($secondString)&&!empty($secondString)){
        //     $thirdQuery = 'AND (A.amount ';
        // }

        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {

                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $value;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($secondArray)) {
            $count = count($secondArray);
            foreach ($secondArray as $index => $key) {
                $statusIDX='';
                switch ($key) {
                    case '수동신청':
                        $statusIDX='203101';
                    break;
                    case '자동신청':
                        $statusIDX='203102';
                    break;
                    case '가상계좌신청':
                        $statusIDX='203103';
                    break;
                    case '수동신청(보류)':
                        $statusIDX='203104';
                    break;
                    case '수동완료':
                        $statusIDX='203201';
                    break;
                    case '자동완료':
                        $statusIDX='203202';
                    break;
                    case '가상계좌완료':
                        $statusIDX='203203';
                    break;
                    case '기타취소':
                        $statusIDX='203211';
                    break;
                    case '스태프취소':
                        $statusIDX='203212';
                    break;
                    case '유저취소':
                        $statusIDX='203213';
                    break;
                    case '가상계좌실패':
                        $statusIDX='203214';
                    break;
                    case '크론취소':
                        $statusIDX='203215';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $secondQuery .= $statusIDX;
                if ($index < $count - 1) {
                    $secondQuery .= ',';
                }
            }
            $secondQuery .= ')';
            if($key==''){
                $secondQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($thirdArray)) {
            $count = count($thirdArray);
            foreach ($thirdArray as $index => $key) {
                $betVal='';
                switch ($key) {
                    case 'A':
                        $betVal='BETWEEN 1 AND 1000000';
                    break;
                    case 'B':
                        $betVal='BETWEEN 1000001 AND 5000000';
                    break;
                    case 'C':
                        $betVal='BETWEEN 5000001 AND 10000000';
                    break;
                    case 'D':
                        $betVal='>= 10000001';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $thirdQuery .= $betVal;
                if ($index < $count - 1) {
                    $thirdQuery .= ') OR (A.amount ';
                }
            }
            $thirdQuery .= ')';
            if($key==''){
                $thirdQuery = ''; // 빈 문자열로 설정
            }
        }

        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }


        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
            IFNULL(SUM(CASE WHEN A.statusIDX IN (203201, 203202, 203203) THEN 1 ELSE 0 END), 0) AS successTotalCount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (203211, 203212, 203213, 203214, 203215) THEN 1 ELSE 0 END), 0) AS failTotalCount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (203201, 203202, 203203) THEN A.amount ELSE 0 END), 0) AS successAmount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (203211, 203212, 203213, 203214, 203215) THEN A.amount ELSE 0 END), 0) AS failureAmount
        FROM
            $dbName.ClientMileageDeposit AS A
        JOIN
            $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
        JOIN
            $dbName.Bank AS C ON B.bankIDX = C.idx
        WHERE A.createTime BETWEEN '$startDate' AND '$endDate'
        ".$firstQuery."".$secondQuery."".$thirdQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    //MileageDepositCon 데이터테이블
    public static function GetClientMileageDepositData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        CASE A.statusIDX
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS status,
        CAST(A.amount AS CHAR) AS amount,
        A.createTime,
        C.idx AS clientIDX,
        C.statusIDX AS clientStatus,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        E.name AS ebuyBankName,
        F.name AS bankName,
        IFNULL((SELECT CONCAT(AES_DECRYPT(name,:dataDbKey),' (',AES_DECRYPT(email,:dataDbKey),')') FROM $dbName.Staff WHERE idx=G.StaffIDX),'-') AS staffEmail,
        /*CASE
            WHEN A.statusIDX = 203202 THEN
                (SELECT IFNULL(createTime, '-') FROM $dbName.ClientLog
                WHERE statusIDX = 203202 AND targetIDX = A.idx AND clientIDX=C.idx)
            ELSE IFNULL(G.createTime, '-')
        END AS updateTime,*/
        A.completeTime AS updateTime,
        CASE
            WHEN A.amount BETWEEN 1 AND 1000000 THEN 'A'
            WHEN A.amount BETWEEN 1000001 AND 5000000 THEN 'B'
            WHEN A.amount BETWEEN 5000001 AND 10000000 THEN 'C'
            WHEN A.amount >= 10000001 THEN 'D'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS amountGrade
        FROM $dbName.ClientMileageDeposit AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.EbuyBank AS D ON A.ebuyBankIDX=D.idx
        LEFT JOIN $dbName.Bank AS E ON D.bankIDX=E.idx
        LEFT JOIN $dbName.Bank AS F ON B.bankIDX=F.idx
        LEFT JOIN $dbName.ClientLog AS G
            ON ( A.statusIDX = 203201 AND G.statusIDX = 203201 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203212 AND G.statusIDX = 203212 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203101 AND G.statusIDX = 203101 AND  A.idx = G.targetIDX )
        WHERE (A.createTime BETWEEN :startDate AND :endDate)
        GROUP BY A.idx
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':startDate', $startDate);
        $Sel->bindValue(':endDate', $endDate);
        $Sel->execute();

        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 해당 idx 정보 가져오기
    public static function GetDepositDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        (SELECT CASE idx
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        FORMAT(A.amount, 0) AS amount,
        A.createTime,
        C.idx AS clientIDX,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS clientBankNumber,
        AES_DECRYPT(B.accountHolder,:dataDbKey) AS clientBankAccountHolder,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        D.accountNumber AS ebuyBankNumber,
        D.accountHolder AS ebuyBankAccountHolder,
        E.name AS ebuyBankName,
        F.name AS clientBankName,
        IFNULL((SELECT CONCAT(AES_DECRYPT(name,:dataDbKey),' (',AES_DECRYPT(email,:dataDbKey),')') FROM $dbName.Staff WHERE idx=G.StaffIDX),'-') AS staffEmail,
        IFNULL(G.createTime,'-') AS updateTime,
        H.name AS gradeName
        FROM $dbName.ClientMileageDeposit AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.EbuyBank AS D ON A.ebuyBankIDX=D.idx
        LEFT JOIN $dbName.Bank AS E ON D.bankIDX=E.idx
        LEFT JOIN $dbName.Bank AS F ON B.bankIDX=F.idx
        LEFT JOIN $dbName.ClientLog AS G
            ON ( A.statusIDX = 203201 AND G.statusIDX = 203201 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203212 AND G.statusIDX = 203212 AND  A.idx = G.targetIDX )
        JOIN $dbName.ClientGrade AS H ON C.gradeIDX=H.idx
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 해당 idx 정보 가져오기
    public static function GetTargetDataPack($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.amount,
        C.idx AS clientIDX,
        AES_DECRYPT(C.name,:dataDbKey) AS clientName,
        D.roomName AS rn
        FROM $dbName.ClientMileageDeposit AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.ClientDevice AS D ON C.deviceIDX = D.idx
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



    //minJob 자동 입금신청건 가져오기
    public static function MinJobAutoMileageDepositList($data=null)
    {
        $closeTime=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            idx,
            clientIDX,
            amount
        FROM $dbName.ClientMileageDeposit
        WHERE createTime <= DATE_SUB(NOW(), INTERVAL $closeTime MINUTE) AND statusIDX = 203102
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //secJob 자동 입금신청건 매칭하기
    public static function MinJobAutoMileageDepositMatching($data=null)
    {
        $depositAmount=$data['depositAmount'];
        $ebuyBankIDX=$data['ebuyBankIDX'];
        $clientBankIDX=$data['clientBankIDX'];
        $clientAccountHolder=$data['clientAccountHolder'];
        $tradeTime=$data['tradeTime'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
            A.idx,
            A.migrationIDX,
            A.clientIDX,
            FORMAT(A.amount, 0) AS amount,
            E.roomName AS rn
        FROM $dbName.ClientMileageDeposit AS A
        INNER JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        INNER JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
        INNER JOIN $dbName.Client AS D ON A.clientIDX = D.idx
        LEFT JOIN $dbName.ClientDevice AS E ON D.deviceIDX = E.idx
        WHERE 
            A.createTime <= :tradeTime AND
            A.amount = :depositAmount AND
            A.ebuyBankIDX = :ebuyBankIDX AND
            AES_DECRYPT(B.accountHolder,:dataDbKey) = :clientAccountHolder AND
            A.statusIDX = 203102 AND
            (
                (C.idx = 6 AND 5 = :clientBankIDX)
                OR (C.idx <> 6 AND C.idx = :clientBankIDX)
            )
            /*C.idx = :clientBankIDX*/
        ");
        $Sel->bindValue(':tradeTime', $tradeTime);
        $Sel->bindValue(':depositAmount', $depositAmount);
        $Sel->bindValue(':ebuyBankIDX', $ebuyBankIDX);
        $Sel->bindValue(':clientBankIDX', $clientBankIDX);
        $Sel->bindValue(':clientAccountHolder', $clientAccountHolder);
        $Sel->bindValue(':dataDbKey', $dataDbKey);

        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    // public static function MinJobAutoMileageDepositMatching($data=null)
    // {
    //     $depositAmount=$data['depositAmount'];
    //     $ebuyBankIDX=$data['ebuyBankIDX'];
    //     $clientBankIDX=$data['clientBankIDX'];
    //     $clientAccountHolder=$data['clientAccountHolder'];
    //     $tradeTime=$data['tradeTime'];
    //     $db = static::GetDB();
    //     $dbName= self::MainDBName;
    //     $dataDbKey=self::dataDbKey;

    //     $Sel = $db->prepare("SELECT
    //         A.idx,
    //         A.clientIDX,
    //         FORMAT(A.amount, 0) AS amount,
    //         E.roomName AS rn
    //     FROM $dbName.ClientMileageDeposit AS A
    //     INNER JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
    //     INNER JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
    //     INNER JOIN $dbName.Client AS D ON A.clientIDX = D.idx
    //     WHERE 
    //         A.createTime <= :tradeTime AND
    //         A.amount = :depositAmount AND
    //         A.ebuyBankIDX = :ebuyBankIDX AND
    //         A.statusIDX = 203102 AND
    //         AES_DECRYPT(B.accountHolder,:dataDbKey) = :clientAccountHolder AND
    //         C.idx = :clientBankIDX
    //     ");
    //     $Sel->bindValue(':tradeTime', $tradeTime);
    //     $Sel->bindValue(':depositAmount', $depositAmount);
    //     $Sel->bindValue(':ebuyBankIDX', $ebuyBankIDX);
    //     $Sel->bindValue(':clientBankIDX', $clientBankIDX);
    //     $Sel->bindValue(':clientAccountHolder', $clientAccountHolder);
    //     $Sel->bindValue(':dataDbKey', $dataDbKey);
    //     $Sel->execute();
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }



    //MileageDepositCon 해당 idx 정보 가져오기 (노드)
    public static function GetTargetNodeData($data=null)
    {

        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        (SELECT CASE idx
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        FORMAT(A.amount, 0) AS amount,
        A.createTime,
        B.clientIDX,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        E.name AS ebuyBankName,
        F.name AS bankName,
        IFNULL((SELECT AES_DECRYPT(email,:dataDbKey) FROM $dbName.Staff WHERE idx=G.StaffIDX),'-') AS staffEmail,
        IFNULL(G.createTime,'-') AS updateTime,
        CASE
            WHEN A.amount BETWEEN 1 AND 1000000 THEN 'A'
            WHEN A.amount BETWEEN 1000001 AND 5000000 THEN 'B'
            WHEN A.amount BETWEEN 5000001 AND 10000000 THEN 'C'
            WHEN A.amount >= 10000001 THEN 'D'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS amountGrade
        FROM $dbName.ClientMileageDeposit AS A
        JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        JOIN $dbName.Client AS C ON B.clientIDX=C.idx
        JOIN $dbName.EbuyBank AS D ON A.ebuyBankIDX=D.idx
        JOIN $dbName.Bank AS E ON D.bankIDX=E.idx
        JOIN $dbName.Bank AS F ON B.bankIDX=F.idx
        LEFT JOIN $dbName.ClientLog AS G
            ON ( A.statusIDX = 203201 AND G.statusIDX = 203201 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203212 AND G.statusIDX = 203212 AND  A.idx = G.targetIDX )
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //minJob 수동신청보류 5분 지난 입금건들 조사
    public static function ClientMileageDepositAutoCancelList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
            A.idx,
            A.statusIDX,
            A.clientIDX,
            A.createTime,
            A.clientBankIDX,
            A.ebuyBankIDX,
            A.amount,
            C.roomName AS rn,
            AES_DECRYPT(D.accountHolder,:dataDbKey) AS accountHolder
        FROM $dbName.ClientMileageDeposit AS A
        LEFT JOIN $dbName.Client AS B ON A.clientIDX = B.idx
        LEFT JOIN $dbName.ClientDevice AS C ON B.deviceIDX = C.idx
        LEFT JOIN $dbName.ClientBank AS D ON A.clientBankIDX = D.idx
        WHERE A.completeTime <= DATE_SUB(NOW(), INTERVAL 5 MINUTE) AND A.statusIDX = 203104
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //minJob 입금신청후 15분 지난데이터들 가져오기
    public static function ClientMileageDepositStaffAlarmList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
            A.idx,
            A.statusIDX,
            A.clientIDX,
            A.createTime,
            A.clientBankIDX,
            A.ebuyBankIDX,
            A.amount,
            AES_DECRYPT(B.name,:dataDbKey) AS name
        FROM $dbName.ClientMileageDeposit AS A
        JOIN $dbName.Client AS B ON A.clientIDX = B.idx
        WHERE A.createTime <= DATE_SUB(NOW(), INTERVAL 20 MINUTE) AND A.statusIDX IN (203101,203102,203104) AND A.appStaffAlarm = 'N'
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 해당 idx 정보 가져오기
    public static function tmpTargetData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.amount,
        B.idx AS clientIDX,
        B.migrationIDX
        FROM $dbName.ClientMileageDeposit AS A
        JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageBlock 2번이상 유저취소 또는 크론취소 된 데이터 (데이터테이블)
    public static function GetUserAndCronCancelList($data=null)
    {
        $createTime = date('Y-m-d H:i:s');
        $twentyFourHoursAgo = date('Y-m-d H:i:s', strtotime('-24 hours', strtotime($createTime)));
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        B.idx AS clientIDX,
        AES_DECRYPT(B.name,:dataDbKey) AS name,
        AES_DECRYPT(B.birth,:dataDbKey) AS birth,
        AES_DECRYPT(B.phone,:dataDbKey) AS phone
        FROM $dbName.ClientMileageDeposit AS A
        JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        WHERE A.statusIDX IN (203213 , 203215) AND A.createTime BETWEEN :twentyFourHoursAgo AND :createTime AND B.statusIDX = 226101
        GROUP BY B.idx HAVING COUNT(B.idx) > 2
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':twentyFourHoursAgo', $twentyFourHoursAgo);
        $Sel->bindValue(':createTime', $createTime);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageBlock 2번이상 유저취소 또는 크론취소 된 데이터
    public static function GetTargetUserAndCronCancelData($data=null)
    {
        $targetIDX = $data;
        $createTime = date('Y-m-d H:i:s');
        $twentyFourHoursAgo = date('Y-m-d H:i:s', strtotime('-24 hours', strtotime($createTime)));
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        idx,
        DATE_ADD(createTime, INTERVAL 9 HOUR) AS createTime,
        CASE statusIDX
            WHEN 203213 THEN '유저취소'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS status,
        amount
        FROM $dbName.ClientMileageDeposit
        WHERE statusIDX IN (203213 , 203215) AND clientIDX = :targetIDX AND createTime BETWEEN :twentyFourHoursAgo AND :createTime
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->bindValue(':createTime', $createTime);
        $Sel->bindValue(':twentyFourHoursAgo', $twentyFourHoursAgo);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }




}