<?php

namespace App\Models;

use PDO;

class ClientDeviceMo extends \Core\Model
{

    //ClientDeviceCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.deviceNumber,
        CASE A.deviceType
            WHEN 'I' THEN '아이폰'
            WHEN 'A' THEN '안드로이드'
            ELSE '' END
        AS deviceType,
        A.createTime,
        A.pushToken,
        A.roomName,
        IFNULL(A.termsTime,'-') AS termsTime,
        B.idx AS clientIDX,
        IFNULL(AES_DECRYPT(B.name, :dataDbKey) , '-') AS name
        FROM $dbName.ClientDevice AS A
        LEFT JOIN $dbName.Client AS B ON A.idx = B.deviceIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDeviceCon 디테일
    // public static function GetFaqDetailData($data=null)
    // {
    //     $targetIDX=$data;
    //     $db = static::GetDB();
    //     $dbName= self::MainDBName;
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.faqCategoryIDX,
    //     A.title,
    //     A.content,
    //     (SELECT
    //         CASE idx
    //             WHEN 314401 THEN '활성'
    //             WHEN 314501 THEN '비활성'
    //             ELSE '' END
    //         AS status
    //     FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
    //     A.createTime,
    //     B.name AS CateName
    //     FROM $dbName.ClientFaq AS A
    //     JOIN $dbName.ClientFaqCategory AS B ON A.faqCategoryIDX=B.idx
    //     WHERE A.idx='$targetIDX'
    //     ");
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }






}