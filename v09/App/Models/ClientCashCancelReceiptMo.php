<?php

namespace App\Models;

use PDO;

class ClientCashCancelReceiptMo extends \Core\Model
{


    public static function GetDataTableListLoad($data=null)
    {
        $startDate   = $data['startDate'] ?? '1970-01-01';
        $endDate     = $data['endDate'] ?? date('Y-m-d');
        $startDate  .= ' 00:00:00';
        $endDate    .= ' 23:59:59';
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            ROW_NUMBER() OVER (ORDER BY A.createTime,A.idx ASC) AS no,
            A.idx,
            A.CancelAmt,
            A.createTime,
            CASE
                WHEN A.statusIDX IN (811701, 811801) THEN '-'
                ELSE A.resultTime
            END AS resultTime,
            CASE A.statusIDX
                WHEN 811701 THEN '2'
                WHEN 811801 THEN '2'
                WHEN 811901 THEN '3'
                WHEN 811902 THEN '1'
                WHEN 811903 THEN '1'
                ELSE '-'
            END AS onlyOrderable,
            CASE A.statusIDX
                WHEN  811701 THEN '취소대기'
                WHEN  811801 THEN '취소중'
                WHEN  811901 THEN '취소완료'
                WHEN  811902 THEN '취소실패'
                WHEN  811903 THEN '취소실패(통신에러)'
                ELSE '-'
            END AS status,
            CASE
                WHEN B.targetStatusIDX = 906101 THEN C.createTime
                WHEN B.targetStatusIDX = 251401 THEN '미적용'
                ELSE '-'
            END AS originReg,
            B.clientIDX,
            B.clientName,
            B.ReceiptTypeNo,
            CASE B.targetStatusIDX
                WHEN  906101 THEN '지불대행'
                WHEN  251401 THEN 'P2P'
                ELSE '-'
            END AS type
        FROM $dbName.ClientCashCancelReceipt AS A
        JOIN $dbName.ClientCashReceipt AS B ON B.idx= A.ClientCashReceiptIDX
        LEFT JOIN $dbName.MarketDepositLog AS C ON C.idx = B.targetIDX AND B.targetStatusIDX = 906101
        WHERE (A.createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetDetail($data=null)
    {
        $idx=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            A.idx,
            A.CancelAmt,
            A.CancelMsg,
            A.createTime,
            A.statusIDX,
            A.statusIDX,
            A.result_msg,
            COALESCE(A.resultTime, '-') AS resultTime,
            CASE A.statusIDX
                WHEN  811701 THEN '취소대기'
                WHEN  811801 THEN '취소중'
                WHEN  811901 THEN '취소완료'
                WHEN  811902 THEN '취소실패'
                WHEN  811903 THEN '취소실패(통신에러)'
                ELSE '-'
            END AS status,
            CASE A.result_code
                WHEN  2001 THEN '성공'
                ELSE A.result_code
            END AS code,
            B.idx AS reIDX,
            B.targetIDX,
            B.targetStatusIDX,
            B.clientIDX,
            B.clientTel,
            B.clientName,
            B.clientEmail,
            B.ReceiptTypeNo,
            B.TRAN_REQ_ID,
            COALESCE(B.result_TID, 0) AS result_TID,
            CASE B.targetStatusIDX
                WHEN  906101 THEN '지불대행'
                WHEN  251401 THEN 'P2P'
                ELSE '-'
            END AS type
        FROM $dbName.ClientCashCancelReceipt AS A
        JOIN $dbName.ClientCashReceipt AS B ON B.idx= A.ClientCashReceiptIDX
        WHERE A.idx=:idx
        ");
        $query->bindValue(':idx', $idx);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetFulldata($data=null)
    {
        $idx=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            A.idx
            ,A.createTime
            ,A.fullData
        FROM $dbName.ClientCashCancelReceiptFullData AS A
        WHERE A.ClientCashCancelReceiptIDX=:ClientCashCancelReceiptIDX
        ");
        $query->bindValue(':ClientCashCancelReceiptIDX', $idx);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }





}