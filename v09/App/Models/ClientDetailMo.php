<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientDetailMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//ClientDetailCon 해당 clientIDX 정보 가져오기 (마일리지 입금내역)
	public static function GetTotalClientMileageDeposit($data=null)
	{
		$targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(A.idx) AS totalDeposit,
        SUM(CASE WHEN A.statusIDX = 203201 THEN 1 ELSE 0 END) AS manualDeposit,
        SUM(CASE WHEN A.statusIDX = 203202 THEN 1 ELSE 0 END) AS autoDeposit,
        SUM(CASE WHEN A.statusIDX = 203203 THEN 1 ELSE 0 END) AS accountDeposit,
        IFNULL(MAX(B.createTime),'-') AS recentTime,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        LEFT JOIN $dbName.ClientMileageDeposit AS B ON A.targetIDX=B.idx
        WHERE A.clientIDX=:targetIDX AND A.statusIDX IN (203201, 203202, 203203)
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
	}

	//ClientDetailCon 해당 clientIDX 정보 가져오기 (마일리지 출금내역) [db 깨끗한 상태로 한번 확인해볼것]
    public static function GetTotalClientMileageWithDrawal($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        /*
            204101,204102의 총 카운트에서 (4) 204211와 204212가 있는 데이터들을 (1) 조사 한다음
            출금이 취소된 데이터를 빼준 다음 카운트롤 출력해줌 (0이상일때)
        */
        COUNT(A.idx) AS withCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204101 AND clientIDX=:targetIDX) AS manualWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204102 AND clientIDX=:targetIDX) AS autoWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE clientIDX=:targetIDX AND statusIDX IN (204211, 204212)) AS minusCount,
        IFNULL(MAX(A.createTime),'-') AS recentTime,
        /*
            204101,204102의 총 amount에서 (1,000,000)구한 후
            출금이 취소된 204211,204212의 총 amount (50,000)를 빼준 amount (950,000)의 데이터를 출력해줌(0이상일때)
        */
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS tmpTotalAmount,
        (SELECT IFNULL(FORMAT(SUM(amount),0),'0') FROM $dbName.ClientMileage WHERE clientIDX=:targetIDX AND statusIDX IN (204211, 204212)) AS minusSUMAmount,

        /*
            avgWithAmount는 총 출금액(출금이 취소된 데이터를 안 뺀 금액)에서 총카운트(출금이 취소 안된 총 횟수)평균만 계산함
        */
        -- IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        (SELECT IFNULL(MAX(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX=:targetIDX AND statusIDX NOT IN (204211, 204212)) AS tmpMaxWithAmount,
        (SELECT IFNULL(MIN(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX=:targetIDX AND statusIDX NOT IN (204211, 204212)) AS tmpMinWithAmount

        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX=:targetIDX AND A.statusIDX IN (204101, 204102);
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);

        $withCount=$result['withCount'];
        $manualWithCount=$result['manualWithCount'];
        $autoWithCount=$result['autoWithCount'];
        $minusCount=$result['minusCount'];

        $totalWithdrawal=0;
        $manualWithdrawal=0;
        $autoWithdrawal=0;

        //총 출금횟수
        if($withCount>0){
            $totalWithdrawal=$withCount-$minusCount;
            if($totalWithdrawal < 0) {
                $totalWithdrawal = 0;
            }
        }
        //수동출금수
        if($manualWithCount>0){
            $manualWithdrawal=$manualWithCount-$minusCount;
            if($manualWithdrawal < 0) {
                $manualWithdrawal = 0;
            }
        }
        //자동출금수
        if($autoWithCount>0){
            $autoWithdrawal=$autoWithCount-$minusCount;
            if($autoWithdrawal < 0) {
                $autoWithdrawal = 0;
            }
        }


        // --------------------------
        $tmpTotalAmount = intval(str_replace(',', '', $result['tmpTotalAmount']));
        $minusSUMAmount = intval(str_replace(',', '', $result['minusSUMAmount']));

        $tmpMaxWithAmount = intval(str_replace(',', '', $result['tmpMaxWithAmount']));
        $tmpMinWithAmount = intval(str_replace(',', '', $result['tmpMinWithAmount']));

        $totalAmount=0;
        $totalAvgWithAmount=0;
        $maxWithAmount=0;
        $minWithAmount=0;

        //총 출금액
        if($tmpTotalAmount>0){
            $totalAmountNumber=$tmpTotalAmount-$minusSUMAmount;
            $totalAmount = number_format($totalAmountNumber);
        }

        //출금 평균금액
        if($totalWithdrawal>0){
            $totalAvgWithAmountNumber=$totalAmountNumber/$totalWithdrawal;
            $totalAvgWithAmount = number_format($totalAvgWithAmountNumber);
        }
        //최대 출금액
        if($tmpMaxWithAmount>0){
            $maxWithAmountNumber=$tmpMaxWithAmount;
            $maxWithAmount = number_format($maxWithAmountNumber);
        }
        //최소 출금액
        if($tmpMinWithAmount>0){
            $minWithAmountNumber=$tmpMinWithAmount;
            $minWithAmount = number_format($minWithAmountNumber);
        }



        $result['totalWithdrawal'] = $totalWithdrawal;
        $result['manualWithdrawal'] = $manualWithdrawal;
        $result['autoWithdrawal'] = $autoWithdrawal;
        $result['totalAmount'] = $totalAmount;
        $result['avgWithAmount'] = $totalAvgWithAmount;
        $result['maxWithAmount'] = $maxWithAmount;
        $result['minWithAmount'] = $minWithAmount;



        return $result;
    }

    //MileageWithdrawalCon clientIDX 총 마일리지
    public static function getMileage($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            COALESCE((
                SUM(
                    /*203201, 203202, 203203, 204211, 204212, 205101, 908102, 251402, 251401 앱*/
                    CASE
                        WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN A.amount
                        ELSE 0
                    END
                ) - SUM(
                    /*204101, 204102, 205201, 906101, 251201 앱*/
                    CASE
                        WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN A.amount
                        ELSE 0
                    END
                )
            ), 0) AS mileage
            FROM $dbName.ClientMileage AS A WHERE A.clientIDX='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        if(isset($result['mileage']) && $result['mileage'] !=null){
            $mileage=$result['mileage'];
            //number_format 은 안돼.. 20240116 jm
            // $resultMileage=number_format($mileage);
            // return $resultMileage;
            return $mileage;
        }else{ return 0;}
    }

    //ClientDetailCon 데이터테이블 (마일리지 입금내역)
    public static function GetClientMileageDepositDataTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        CASE A.statusIDX
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS '상태',
        FORMAT(A.amount, 0) AS '금액',
        A.createTime AS '요청시간',
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS '계좌번호',
        F.name AS '예금주',
        IFNULL((SELECT AES_DECRYPT(email, :dataDbKey) FROM $dbName.Staff WHERE idx=G.StaffIDX),'-') AS '승인스태프',
        IFNULL(G.createTime,'-') AS '완료시간'
        FROM $dbName.ClientMileageDeposit AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.EbuyBank AS D ON A.ebuyBankIDX=D.idx
        LEFT JOIN $dbName.Bank AS E ON D.bankIDX=E.idx
        LEFT JOIN $dbName.Bank AS F ON B.bankIDX=F.idx
        LEFT JOIN $dbName.ClientLog AS G
            ON ( A.statusIDX = 203201 AND G.statusIDX = 203201 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203212 AND G.statusIDX = 203212 AND  A.idx = G.targetIDX )
        WHERE A.clientIDX=:clientIDX ORDER BY A.createTime DESC

        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetialCon 데이터테이블 (마일리지 출금내역)
    public static function GetClientMileageWithdrawalDataTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        CASE A.statusIDX
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            WHEN 204201 THEN '수동완료'
            WHEN 204202 THEN '자동완료'
            WHEN 204211 THEN '기타취소'
            WHEN 204212 THEN '스태프취소'
            ELSE '' END
        AS '상태',
        FORMAT(A.amount, 0) AS '실출금액',
        FORMAT(A.fee, '0') AS '수수료',
        A.createTime AS '요청시간',
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS '계좌번호',
        D.name AS '예금주',
        IFNULL((SELECT AES_DECRYPT(email, :dataDbKey) FROM $dbName.Staff WHERE idx=E.StaffIDX),'-') AS '승인스태프',
        IFNULL(E.createTime,'-') AS '완료시간'
        FROM $dbName.ClientMileageWithdrawal AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Bank AS D ON B.bankIDX=D.idx
        LEFT JOIN $dbName.ClientLog AS E
            ON ( A.statusIDX = 204201 AND E.statusIDX = 204201 AND  A.idx = E.targetIDX )
            OR ( A.statusIDX = 204212 AND E.statusIDX = 204212 AND  A.idx = E.targetIDX )
        WHERE A.clientIDX=:clientIDX ORDER BY A.createTime DESC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 마일리지 변동내역 데이터테이블
    public static function GetClientMileageVariationHistoryTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        // 세션 변수 초기화
        $db->exec("SET @resultAmount := 0;");
        $Sel = $db->query("SELECT
        CASE
            WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212) THEN '마일리지 증감'
            WHEN A.statusIDX IN (205101) THEN '스태프 증감'
            WHEN A.statusIDX IN (908102) THEN '출금대행 증감'
            WHEN A.statusIDX IN (251402, 251401) THEN 'P2P거래 증감'
            WHEN A.statusIDX IN (204101, 204102) THEN '마일리지 차감'
            WHEN A.statusIDX IN (205201) THEN '스태프 차감'
            WHEN A.statusIDX IN (906101) THEN '지불대행 차감'
            WHEN A.statusIDX IN (251201) THEN 'P2P거래 차감'
            WHEN A.statusIDX IN (206101) THEN '시스템 증감'
            ELSE ''
        END AS '상태',
        A.createTime AS '요청시간',
        CASE
            WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN IFNULL(A.amount, 0)
            ELSE NULL
        END AS '증감금액',
        CASE
            WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN IFNULL(A.amount, 0)
            ELSE NULL
        END AS '차감금액',
        @resultAmount := @resultAmount + (
            CASE
                WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN IFNULL(A.amount, 0)
                WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN -IFNULL(A.amount, 0)
                ELSE 0
            END
        ) AS '남은금액'
        FROM ebuy.ClientMileage AS A
        WHERE A.clientIDX='$clientIDX'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 클라이언트 기본정보
    public static function GetClientDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.ci,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.birth,:dataDbKey) AS birth,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        A.gradeIDX,
        CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE '' END
        AS status,
        A.createTime,
        B.name AS gradeName,
        B.defaultFeeKRW,
        B.tradeFeePer,
        B.tranFeePer,
        B.withFeePer,
        B.withFeeKRW,
        B.withMinKRW,
        B.upto,
        C.deviceNumber,
        CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType
        FROM $dbName.Client AS A
        LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
        LEFT JOIN $dbName.ClientBank AS D ON A.idx=D.clientIDX
        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 클라이언트 idx 등록은행
    public static function GetTargetClientData($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        IFNULL(B.createTime,'-') AS createTime,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        AES_DECRYPT(B.accountHolder,:dataDbKey) AS accountHolder,
        C.name AS bankName,
        C.code AS bankCode
        FROM $dbName.Client AS A
        JOIN $dbName.ClientBank AS B ON A.idx = B.clientIDX
        JOIN $dbName.Bank AS C ON B.BankIDX = C.idx
        WHERE A.idx = :clientIDX
        ORDER BY B.createTime DESC
        LIMIT 1;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 클라이언트 이메일
    public static function GetTargetClientEmailLog($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        IFNULL(B.createTime,'-') AS createTime
        FROM $dbName.Client AS A
        JOIN $dbName.ClientLog AS B ON A.idx = B.clientIDX
        WHERE A.idx = :clientIDX AND B.statusIDX=228101
        ORDER BY B.createTime DESC
        LIMIT 1;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetClientLogList($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        A.createTime AS '시간',
        B.memo AS '내용',
        IFNULL(C.ex,'-') AS 'Ex'
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.ClientLogEx AS C ON A.idx=C.logIDX
        WHERE A.clientIDX = :targetIDX
        ORDER BY A.createTime DESC;
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 clientIDX 정보 가져오기 (지불대행)
    public static function GetTotalClientDeposit($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(A.idx) AS totalDeposit,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX=:targetIDX AND A.statusIDX = 906101
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 client 지불대행 테이블
    public static function GetClientDepositDataTable($data=null)
    {
        $referenceIdArr = $data;
        $query = '';

        // 배열이 비어있지 않다면 IN 절 생성
        if (!empty($referenceIdArr)) {
            $query = ' IN (';

            // 배열을 순회하며 각 이메일을 쿼리에 추가
            foreach ($referenceIdArr as $key) {
                $referenceId = $key['referenceId'];
                $query .= "'" . $referenceId . "',";
            }

            // 뒤의 콤마(,) 제거
            $query = rtrim($query, ',');
            $query .= ')';
        } else {
            // 배열이 비어있다면 빈 문자열로 설정
            $query = '';
        }

        $mainDb = static::GetDB();
        $mainDbName= self::MainDBName;

        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->prepare("SELECT
            CASE A.statusIDX
                WHEN 906101 THEN '지불대행 완료'
                ELSE '' END
            AS '상태',
            MAX(A.createTime) AS '시간',
            A.marketCode AS '마켓코드',
            A.amount AS '지불대행금액',
            A.exchangeRate AS '환율',
            A.clientFee AS '유저수수료',
            A.amount * A.exchangeRate + A.clientFee AS '금액', -- 추가된 부분
            A.invoiceID AS 'InvoiceID',
            A.marketUserEmail AS '지불대행이메일'
        FROM $dbName.MarketDepositLog AS A
        WHERE marketUserEmail ".$query." AND A.statusIDX = 906101
        GROUP BY A.invoiceID, A.idx, A.statusIDX, A.marketCode, A.amount, A.marketUserEmail
        ORDER BY CreateTime DESC;
        ");
        $Sel->execute();
        $result = $Sel->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $key => $row) {
            $marketCode=$row['마켓코드'];
            $Sel2 = $mainDb->prepare("SELECT
            name
            FROM $mainDbName.Market
            WHERE code = :marketCode
            ");
            $Sel2->bindValue(':marketCode', $marketCode);
            $Sel2->execute();
            $mainDbResult=$Sel2->fetch(PDO::FETCH_ASSOC);

            if (isset($mainDbResult['name'])) {
                $marketName = $mainDbResult['name'];
            } else {
                $marketName = null;
            }

            $result[$key]['마켓이름'] = $marketName;
        }


        return $result;
    }

    //ClientDetailCon 해당 clientIDX 정보 가져오기 (출금대행)
    public static function GetTotalClientWithdrawal($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(A.idx) AS totalDeposit,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX=:targetIDX AND A.statusIDX = 908102
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 client 출금대행 테이블
    public static function GetClientWithdrawalDataTable($data=null)
    {
        $referenceIdArr = $data;
        $query = '';

        // 배열이 비어있지 않다면 IN 절 생성
        if (!empty($referenceIdArr)) {
            $query = ' IN (';

            // 배열을 순회하며 각 이메일을 쿼리에 추가
            foreach ($referenceIdArr as $key) {
                $referenceId = $key['referenceId'];
                $query .= "'" . $referenceId . "',";
            }

            // 뒤의 콤마(,) 제거
            $query = rtrim($query, ',');
            $query .= ')';
        } else {
            // 배열이 비어있다면 빈 문자열로 설정
            $query = '';
        }

        $mainDb = static::GetDB();
        $mainDbName= self::MainDBName;

        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->prepare("SELECT
            CASE A.statusIDX
                WHEN 908102 THEN '출금대행 완료'
                ELSE '' END
            AS '상태',
            MAX(A.createTime) AS '시간',
            A.marketCode AS '마켓코드',
            A.amount AS '출금대행금액',
            A.exchangeRate AS '환율',
            A.clientFee AS '유저수수료',
            A.amount * A.exchangeRate + A.clientFee AS '금액', -- 추가된 부분
            A.invoiceID AS 'InvoiceID',
            A.marketUserEmail AS '지불대행이메일'
        FROM $dbName.MarketWithdrawalLog AS A
        WHERE marketUserEmail ".$query." AND A.statusIDX = 908102
        GROUP BY A.invoiceID, A.idx, A.statusIDX, A.marketCode, A.amount, A.marketUserEmail
        ORDER BY CreateTime DESC;
        ");
        $Sel->execute();
        $result = $Sel->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $key => $row) {
            $marketCode=$row['마켓코드'];
            $Sel2 = $mainDb->prepare("SELECT
            name
            FROM $mainDbName.Market
            WHERE code = :marketCode
            ");
            $Sel2->bindValue(':marketCode', $marketCode);
            $Sel2->execute();
            $mainDbResult=$Sel2->fetch(PDO::FETCH_ASSOC);

            if (isset($mainDbResult['name'])) {
                $marketName = $mainDbResult['name'];
            } else {
                $marketName = null;
            }

            $result[$key]['마켓이름'] = $marketName;
        }
        return $result;
    }

    //ClientDetailCon P2P 지불대행 계정 불러오기
    public static function GetClientPtpAccountList($data = null)
    {
        $clientIDX  = $data;
        $dbName = self::MainDBName;
        $dataDbKey = self::dataDbKey;
        $db     = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.account,:dataDbKey) AS account,
        A.statusIDX,
        DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime,
        B.name AS typeName
        FROM $dbName.ClientPtpAccount AS A
        JOIN $dbName.PeerToPeerType AS B ON A.PeerToPeerTypeIDX = B.idx
        WHERE A.clientIDX = :clientIDX AND A.statusIDX=250101 ORDER BY A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 구매내역 데이터테이블
    public static function GetClientPeerToPeerData($data=null)
    {

        // -- 251101  1단계: 시작 마일리지 입금신청 같이 됨
        // -- 251102  1단계: 시작 마일리지가 있나 봄
        // -- 251201  2단계: 구매자 마일리지 차감 완료 / 판매자 전자화폐 송금중 << 구매자 ClientMileage 차감 코드
        // -- 251301  3단계: 판매자 송금완 / 구매자 전자화폐 지갑 확인중(생략가능)
        // -- 251401  끝: 종료 구매자 확인완 / 피투피거래종료(완료) << 판매자 ClientMileage 증가 하는 코드
        // -- 251402  끝: 거래취소 << 구매자가 예치한 마일리지 반환함 << 구매자 ClientMileage 증가 하는 코드
        // -- 251403  끝: 거래취소 << 구매자 마일리지 예치하기 전에 취소하는것 마일리지 증차감 없음

        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        (SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS '상태'
        ,A.createTime AS '신청일시'
        ,IFNULL(A.completeTime, '-') AS '종료일시'
        ,A.amount AS '수량'
        ,A.rate AS '단가'
        ,AES_DECRYPT(Buyer.email,:dataDbKey) AS '구매자계정이메일'

        ,(SELECT name AS typeName FROM $dbName.PeerToPeerType WHERE B.PeerToPeerTypeIDX=idx) AS '전자화폐'


        ,AES_DECRYPT(Buyer.name,:dataDbKey) AS '구매자'
        ,AES_DECRYPT(B.account,:dataDbKey) AS '구매자전자화폐계정'

        ,AES_DECRYPT(Seller.name,:dataDbKey) AS '판매자'
        ,AES_DECRYPT(Seller.email,:dataDbKey) AS '판매자전자화폐계정'



        ,CAST(A.amount * A.rate AS SIGNED) AS '합계금액'
        ,CAST(A.amount * A.rate AS SIGNED) + A.BuyerFee AS '구매금액'
        ,CAST(A.amount * A.rate AS SIGNED) - A.SellerFee AS '판매금액'

        FROM $dbName.ClientPeerToPeer AS A

        INNER JOIN $dbName.ClientPtpAccount AS B ON A.BuyerClientPtpAccountIDX=B.idx

        INNER JOIN $dbName.Client AS Buyer ON A.BuyerClientIDX=Buyer.idx

        INNER JOIN $dbName.Client AS Seller ON A.SellerClientIDX=Seller.idx

        WHERE (A.BuyerClientIDX = :clientIDX OR A.SellerClientIDX = :clientIDX) ORDER BY A.createTime DESC
        ");
        $query->bindValue(':clientIDX', $clientIDX);
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



}