<?php

namespace App\Models;

use PDO;

class MarketMo extends \Core\Model
{

    //마켓 table용 Data 전체 불러오기 , MarketManagerEmailCon , MarketWhiteIpCon , MarketKeyCon
    public static function GetMarketAllListData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        createTime
        FROM $dbName.Market
        ORDER BY name ASC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



    //MarketCon 데이터테이블
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 412101 THEN '정상'
                WHEN 412201 THEN '차단'
                WHEN 412202 THEN '계약만료'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        (SELECT
            CASE idx
                WHEN 413101 THEN '지불대행정상'
                WHEN 413201 THEN '임시차단'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.depositStatusIDX=idx) AS depositStatus,
        (SELECT
            CASE idx
                WHEN 414101 THEN '출금대행정상'
                WHEN 414201 THEN '임시차단'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.withdrawalStatusIDX=idx) AS withdrawalStatus,
        A.code,
        A.name,
        A.createTime,
        IFNULL(AES_DECRYPT(C.email, :dataDbKey),'-') AS email,
        D.name AS partnerName,
        (
            SELECT
            COALESCE((
                SUM(
                    CASE
                        /*20240122 421101 마이그레이션 구 피센터 발란스 이전으로 인한 증감*/
                        WHEN statusIDX IN (418201,417211,419101,906101,420101,421101) THEN amount
                        ELSE 0
                    END
                ) - SUM(
                    CASE
                        WHEN statusIDX IN (417101, 417102, 417103,419201,908102,420201) THEN amount
                        ELSE 0
                    END
                )
            ), 0) AS balance
            FROM $dbName.MarketBalance WHERE marketIDX = A.idx
        ) AS balance
        FROM $dbName.Market AS A
        LEFT JOIN $dbName.MarketManager AS B ON A.idx=B.marketIDX AND B.gradeIDX=1
        LEFT JOIN $dbName.Manager AS C ON B.managerIDX=C.idx
        JOIN $dbName.Partner AS D ON A.partnerIDX=D.idx
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketCon 디테일
    public static function GetMarketDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 412101 THEN '정상'
                WHEN 412201 THEN '차단'
                WHEN 412202 THEN '계약만료'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        (SELECT
            CASE idx
                WHEN 413101 THEN '지불대행정상'
                WHEN 413201 THEN '임시차단'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.depositStatusIDX=idx) AS depositStatus,
        (SELECT
            CASE idx
                WHEN 414101 THEN '출금대행정상'
                WHEN 414201 THEN '임시차단'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.withdrawalStatusIDX=idx) AS withdrawalStatus,
        A.marketDepositFeePer,
        A.marketWithdrawalFeePer,
        A.marketSettleBankwireFeePer,
        A.marketSettleCryptoFeePer,
        A.marketTopupBankwireFeePer,
        A.marketTopupCryptoFeePer,
        A.marketMinusLimitAmount,
        A.marketDepositMinAmount,
        A.marketDepositMaxAmount,
        A.code,
        A.name,
        A.createTime,
        A.phoneNumber,
        A.hostUrl,
        A.serviceUrl,
        IFNULL(AES_DECRYPT(C.email, :dataDbKey),'-') AS email,
        D.name AS partnerName,
        D.code AS partnerCode
        FROM $dbName.Market AS A
        LEFT JOIN $dbName.MarketManager AS B ON A.idx=B.marketIDX AND B.gradeIDX=1
        LEFT JOIN $dbName.Manager AS C ON B.managerIDX=C.idx
        JOIN $dbName.Partner AS D ON A.partnerIDX=D.idx
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketCon 업데이트 상태가 가능한 데이터 조사
    public static function GetTargetUpdateData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        idx,
        statusIDX,
        phoneNumber,
        marketDepositFeePer,
        marketWithdrawalFeePer,
        marketSettleBankwireFeePer,
        marketSettleCryptoFeePer,
        marketTopupBankwireFeePer,
        marketTopupCryptoFeePer,
        marketMinusLimitAmount,
        marketDepositMinAmount,
        marketDepositMaxAmount,
        hostUrl,
        serviceUrl,
        (SELECT
            CASE idx
                WHEN 412101 THEN '정상'
                WHEN 412201 THEN '차단'
                WHEN 412202 THEN '계약만료'
                ELSE '' END
            AS status FROM $dbName.Status WHERE statusIDX=idx) AS statusVal
        FROM $dbName.Market
        WHERE idx='$targetIDX'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerCon 마켓 리스트
    public static function GetMarketListData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.code,
        A.name,
        B.name AS partnerName
        FROM $dbName.Market AS A
        JOIN $dbName.Partner AS B ON A.partnerIDX=B.idx
        ORDER BY A.name ASC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketSettlementCon 마켓 리스트 솔팅용
    public static function GetMarketSortingListData($data=null)
    {
        $type = $data;
        $query = '';
        if($type == 'A'){
            $query = 'ORDER BY A.name ASC';
        }elseif($type == 'U'){
            $query = 'ORDER BY maxCreateTime DESC';
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.code,
        A.name,
        COUNT(B.idx) AS count,
        MAX(B.createTime) AS maxCreateTime
        FROM ebuy.Market AS A
        LEFT JOIN ebuy.MarketSettlementCryptoWallet AS B ON A.idx = B.marketIDX AND B.statusIDX = 425101
        GROUP BY A.idx
        ".$query."
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketCon 해당 마켓 idx statusIDX 조사
    public static function MarketStatusData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        name,
        code,/*발란스용*/
        statusIDX,
        depositStatusIDX,
        withdrawalStatusIDX
        FROM $dbName.Market
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketCon 코드 받아오기
    //231228 DayJob에서도 쓰고있어용
    public static function GetMarketCode($data=null)
    {
        $code=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name
        FROM $dbName.Market
        WHERE code='$code'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientMarketListCon 해당 마켓정보 가져오기
    public static function GetMarketIDXData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name
        FROM $dbName.Market
        WHERE idx='$marketIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }





    ///샌드박스 마켓IDX 가져오기

    public static function getSandboxMarketIDX($data=null)
    {
        $marketIDX=$data;
        $dbsand = static::GetSandboxDB();
        $dbNameSand= self::EbuySandboxDBName;

        $Sel = $dbsand->query("SELECT
        idx
        FROM $dbNameSand.Market
        WHERE targetMarketIDX='$marketIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}