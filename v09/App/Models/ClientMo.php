<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */


	public static function GetClientAllListData($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		AES_DECRYPT(name,:dataDbKey) AS clientName,
		AES_DECRYPT(birth,:dataDbKey) AS birth,
		AES_DECRYPT(phone,:dataDbKey) AS phone,
		AES_DECRYPT(email,:dataDbKey) AS email,
		gradeIDX
		FROM $dbName.Client
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientGradeCon 데이터테이블
	public static function getClientList($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$statusIDX = $data;
		$query = $db->query("SELECT
		idx
		FROM $dbName.Client
		");
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MileageDepositCon 솔팅별 데이터 리셋
    // public static function GetTotalSortingData($data=null)
    // {
    //     $columnsVal=$data;
    //     $firstString = $columnsVal[0];
    //     $secondString = $columnsVal[1];

    //     $firstArray = explode('|', $firstString);
    //     $secondArray = explode('|', $secondString);

    //     $firstQuery = 'AND B.name IN (';
    //     $secondQuery = 'AND C.deviceType IN (';

    //     // if(isset($firstString)&&!empty($firstString)){
    //     //     $secondQuery = 'AND C.deviceType IN (';
    //     // }

    //     if (!empty($firstArray)) {
    //         $count = count($firstArray);
    //         foreach ($firstArray as $index => $key) {

    //             $value = "'" . $key . "'"; // 작은 따옴표 추가
    //             $firstQuery .= $value;
    //             if ($index < $count - 1) {
    //                 $firstQuery .= ',';
    //             }
    //         }
    //         $firstQuery .= ')';
    //         if($key==''){
    //             $firstQuery = ''; // 빈 문자열로 설정
    //         }
    //     }

    //     if (!empty($secondArray)) {
    //         $count = count($secondArray);
    //         foreach ($secondArray as $index => $key) {
    //             $deviceType='';
    //             switch ($key) {
    //                 case '안드로이드':
    //                     $deviceType='A';
    //                 break;
    //                 case '아이폰':
    //                     $deviceType='I';
    //                 break;
    //             }
    //             $value = "'" . $deviceType . "'"; // 작은 따옴표 추가
    //             $secondQuery .= $value;
    //             if ($index < $count - 1) {
    //                 $secondQuery .= ',';
    //             }
    //         }
    //         $secondQuery .= ')';
    //         if($key==''){
    //             $secondQuery = ''; // 빈 문자열로 설정
    //         }
    //     }

    //     $db = static::GetDB();
    //     $dbName= self::MainDBName;
    //     $Sel = $db->prepare("SELECT
    //         COUNT(A.idx) AS count
    //     FROM
    //     $dbName.Client AS A
    //     JOIN $dbName.ClientGrade AS B ON A.gradeIDX = B.idx
    //     JOIN $dbName.ClientDevice AS C ON A.deviceIDX = C.idx
    //     ".$firstQuery."".$secondQuery."
    //     ");
    //     $Sel->execute();
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    //ClientCon 탈퇴를 제외한 토탈카운트
    public static function GetTotalCount($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $statusIDX = $data;
        $query = $db->query("SELECT
        COUNT(idx) AS total
        FROM $dbName.Client
        WHERE statusIDX <> 226201
        ");
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

	//ClientCon 데이터테이블
	public static function GetDataTableListLoad($data=null)
	{
        $db = static::GetDB();
        // $date = date("Y-m-d H:i:s", strtotime("+9 hours"));

		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.birth,:dataDbKey) AS birth,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
        CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226102 THEN '본인인증완료'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE A.statusIDX END
        AS status,
		A.createTime,
        CASE A.migrationIDX
            WHEN 0 THEN '신규'
            ELSE A.migrationIDX END
        AS migrationIDX,
        A.emergency,
        A.withdrawalBlock,
		B.name AS gradeName,
		C.deviceNumber,
		CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType,
        COUNT(D.idx) AS count,
        CASE WHEN  MAX(D.createTime)  >= DATE_SUB(NOW(), INTERVAL 24 HOUR) THEN MAX(D.createTime) ELSE NULL END AS memoTime
		FROM $dbName.Client AS A
		LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
		LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
        LEFT JOIN $dbName.StaffMemo AS D ON A.idx=D.targetIDX AND D.statusIDX=371101
        /*ORDER BY A.createTime DESC*/
        GROUP BY A.idx
        ORDER BY
        A.createTime DESC
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

    //ClientCon 데이터테이블
    public static function GetDataTableListLoadTest($data=null)
    {
        $db = static::GetDB();
        // $date = date("Y-m-d H:i:s", strtotime("+9 hours"));

        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.birth,:dataDbKey) AS birth,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226102 THEN '본인인증완료'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE A.statusIDX END
        AS status,
        A.createTime,
        CASE A.migrationIDX
            WHEN 0 THEN '신규'
            ELSE A.migrationIDX END
        AS migrationIDX,
        A.emergency,
        A.withdrawalBlock,
        B.name AS gradeName,
        C.deviceNumber,
        CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType,
        COUNT(D.idx) AS count,
        CASE WHEN  MAX(D.createTime)  >= DATE_SUB(NOW(), INTERVAL 24 HOUR) THEN MAX(D.createTime) ELSE NULL END AS memoTime
        FROM $dbName.Client AS A
        LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
        LEFT JOIN $dbName.StaffMemo AS D ON A.idx=D.targetIDX AND D.statusIDX=371101
        /*ORDER BY A.createTime DESC*/
        GROUP BY A.idx
        ORDER BY
        A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	//ClientCon 디테일 (오른쪽)
	public static function GetClientDetailData($data=null)
	{
		$targetIDX=$data;
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		A.ci,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.birth,:dataDbKey) AS birth,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		A.gradeIDX,
		CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226102 THEN '본인인증완료'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE A.statusIDX END
        AS status,
		A.createTime,
        A.emergency,
        A.withdrawalBlock,
		B.name AS gradeName,
		B.defaultFeeKRW,
		B.tradeFeePer,
		B.tranFeePer,
		B.withFeePer,
		B.withFeeKRW,
		B.withMinKRW,
		C.deviceNumber,
		CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType
		FROM $dbName.Client AS A
		LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
		LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
		LEFT JOIN $dbName.ClientBank AS D ON A.idx=D.clientIDX
		WHERE A.idx=:targetIDX
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientCon 해당 클라이언트 idx statusIDX 조사
    public static function IssetClientData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.gradeIDX,
        A.migrationIDX,
        B.name AS gradeName
        FROM $dbName.Client AS A
        JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


	// public static function getClientForPushAlarm($data=null)
	// {
 //        $db = static::GetDB();
	// 	$dbName= self::MainDBName;

	// 	$queryTxt='';
	// 	$clientIDX = $data['clientIDX'];
	// 	$statusIDX = $data['statusIDX'];

	// 	if($clientIDX=='all'){
	// 	}else{
	// 		$queryTxt='WHERE A.idx='.$clientIDX;
	// 	}
	// 	$query = $db->query("SELECT
	// 		A.idx,
	// 		B.deviceType,
	// 		B.pushToken,
	// 		C.statusIDX
	// 	FROM $dbName.Client AS A
	// 	INNER JOIN $dbName.ClientDevice AS B ON A.deviceIDX = B.idx
	// 	LEFT JOIN $dbName.ClientAlarmSetting AS C ON A.idx = C.clientIDX AND C.statusIDX = '$statusIDX'
	// 	".$queryTxt);
	// 	$result=$query->fetchAll(PDO::FETCH_ASSOC);
	// 	return $result;
	// }

	//ClientCon 해당 클라이언트 idx 등록은행
	public static function GetTargetClientData($data=null)
	{

		$clientIDX = $data;

        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		B.createTime,
		AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
		AES_DECRYPT(B.accountHolder,:dataDbKey) AS accountHolder,
		C.name AS bankName,
		C.code AS bankCode
		FROM $dbName.Client AS A
		JOIN $dbName.ClientBank AS B ON A.idx = B.clientIDX
		JOIN $dbName.Bank AS C ON B.BankIDX = C.idx
		WHERE A.idx = :clientIDX
		ORDER BY B.createTime DESC
		LIMIT 1;
		");
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}


	//20240112 jm p2p
	public static function GetClientListKeywordSearch($data=null)
	{
		$nameKeyword = $data;

        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		B.name AS gradeName

		FROM $dbName.Client AS A
        JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
		WHERE AES_DECRYPT(A.name,:dataDbKey) LIKE :keywordWithWildcards
		AND A.statusIDX=:statusIDX

		");
		$query->bindValue(':dataDbKey', $dataDbKey);
							$keywordWithWildcards = '%' . $nameKeyword . '%';
		$query->bindValue(':keywordWithWildcards', $keywordWithWildcards);
		$query->bindValue(':statusIDX', 226101);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//20240112 jm p2p
    public static function GetClientListKeywordSearch_withPtpAccount($data=null)
    {

        $nameKeyword = $data['nameKeyword'];
        $typeIDX = $data['typeIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        B.name AS gradeName

        FROM $dbName.Client AS A

        INNER JOIN $dbName.ClientPtpAccount AS X ON A.idx=X.clientIDX

        INNER JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx

        WHERE AES_DECRYPT(A.name,:dataDbKey) LIKE :keywordWithWildcards
        AND X.PeerToPeerTypeIDX=:typeIDX
        AND X.statusIDX=:XstatusIDX
		AND A.statusIDX=:statusIDX
		GROUP BY A.idx;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
                            $keywordWithWildcards = '%' . $nameKeyword . '%';
        $query->bindValue(':keywordWithWildcards', $keywordWithWildcards);
        $query->bindValue(':typeIDX', $typeIDX);
        $query->bindValue(':statusIDX', 226101);
        $query->bindValue(':XstatusIDX', 250101);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	//20240115 jm p2p
    public static function getPtpBuyerCheck($data=null)
    {
        $clientIDX = $data['clientIDX'];
        $ptpAccountIDX = $data['ptpAccountIDX'];
        $typeIDX = $data['typeIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.gradeIDX,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,

        C.idx AS clientBankIDX,

        B.name AS gradeName,
        B.tradeFeePer,
        B.defaultFeeKRW,

        Y.name AS ptpTypeName,
        Y.idx AS ptpTypeIDX,

        X.idx AS ptpAccountIDX,
        AES_DECRYPT(X.account,:dataDbKey) AS account

        FROM $dbName.Client AS A

        INNER JOIN $dbName.ClientPtpAccount AS X ON A.idx=X.clientIDX

        JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        INNER JOIN $dbName.ClientBank AS C ON A.idx = C.clientIDX
        JOIN $dbName.PeerToPeerType AS Y ON X.PeerToPeerTypeIDX=Y.idx

        WHERE A.idx=:clientIDX
        AND X.idx=:ptpAccountIDX
        AND X.PeerToPeerTypeIDX=:typeIDX
        AND X.statusIDX=:XstatusIDX
		AND A.statusIDX=:statusIDX
        ORDER BY C.createTime DESC LIMIT 1
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->bindValue(':ptpAccountIDX', $ptpAccountIDX);
        $query->bindValue(':typeIDX', $typeIDX);
        $query->bindValue(':statusIDX', 226101);
        $query->bindValue(':XstatusIDX', 250101);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

	//20240115 jm p2p
    public static function getPtpSellerCheck($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        B.name AS gradeName,
        B.tradeFeePer,
        B.defaultFeeKRW

        FROM $dbName.Client AS A

        JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx

        WHERE A.idx=:clientIDX
		AND A.statusIDX=:statusIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->bindValue(':statusIDX', 226101);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }






    //ClientCon 데이터테이블
    public static function clientUnifyTable($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
         CONCAT(
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 1, 3), '-',
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 4, 4), '-',
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 8, 4)
        ) AS phone,
        CONCAT(
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 1, 4), '-',
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 5, 2), '-',
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 7, 2)
        ) AS birth,
        A.createTime,
        CASE A.migrationIDX
            WHEN 0 THEN '신규'
            ELSE '기존'
        END migrationIDX,
        B.name AS gradeName
        FROM $dbName.Client AS A
        INNER JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        WHERE A.statusIDX = 226101
        ORDER BY
        A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

       public static function getUnifyAbleClient($data=null)
    {

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
         CONCAT(
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 1, 3), '-',
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 4, 4), '-',
            SUBSTRING(AES_DECRYPT(A.phone, :dataDbKey), 8, 4)
        ) AS phone,
        CONCAT(
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 1, 4), '-',
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 5, 2), '-',
            SUBSTRING(AES_DECRYPT(A.birth, :dataDbKey), 7, 2)
        ) AS birth,
        A.createTime,
        CASE A.migrationIDX
            WHEN 0 THEN '신규'
            ELSE '기존'
        END migrationIDX,
        B.name AS gradeName
        FROM $dbName.Client AS A
        INNER JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        WHERE A.statusIDX = 226101
        AND (A.createTime) IN (
            SELECT MAX(A.createTime)
            FROM $dbName.ClientBank AS A
            GROUP BY A.clientIDX
        ORDER BY
        A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public static function getUnifyDetail($data=null)
    {
        $idx = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            A.idx,
            A.ci,
            A.gender,
            A.gradeIDX,
            A.deviceIDX,
            A.createTime,
            A.migrationIDX,
            A.migrationTermsTime,
            A.emergency,
            A.statusIDX,
            AES_DECRYPT(A.name,:dataDbKey) AS name,
            AES_DECRYPT(A.birth,:dataDbKey) AS birth,
            AES_DECRYPT(A.phone,:dataDbKey) AS phone,
            AES_DECRYPT(A.email,:dataDbKey) AS email,
            B.name AS gradeName,
            C.bankIDX AS bankIDX,
            AES_DECRYPT(C.accountNumber,:dataDbKey) AS accountNumber,
            AES_DECRYPT(C.accountHolder,:dataDbKey) AS accountHolder,
            C.createTime AS bankCreateTime
        FROM $dbName.Client AS A
        INNER JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        INNER JOIN $dbName.ClientBank AS C ON A.idx=C.clientIDX
        WHERE A.idx = '$idx'
        ORDER BY C.createTime DESC
        LIMIT 1;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //20240206 개명들어왔따 이름 풀께;;;
    public static function getUnifyAbleArr($data=null)
    {
        $clientIDX = $data['clientIDX'];
        $name = $data['name'];
        $birth = $data['birth'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            A.idx,
            A.migrationIDX,
            AES_DECRYPT(A.name,:dataDbKey) AS name,
            AES_DECRYPT(A.phone,:dataDbKey) AS phone,
            AES_DECRYPT(A.birth,:dataDbKey) AS birth,
            AES_DECRYPT(A.email,:dataDbKey) AS email
            FROM $dbName.Client AS A
            WHERE AES_DECRYPT(A.birth,:dataDbKey) = :birth
            -- AND AES_DECRYPT(A.name,:dataDbKey) = :name
            AND A.idx != :idx
            AND A.statusIDX = 226203
        ");
        // $query->bindValue(':name', $name);
        $query->bindValue(':birth', $birth);
        $query->bindValue(':idx', $clientIDX);

        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientMarketListCon 이름+번호로 clientIDX 조사
    public static function GetIssetClientIDXData($data=null)
    {
        $name = $data['name'];
        $phone = $data['phone'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            A.idx,
            A.statusIDX,
            A.migrationIDX
            FROM $dbName.Client AS A
            WHERE AES_DECRYPT(A.name,:dataDbKey) = :name
            AND AES_DECRYPT(A.phone,:dataDbKey) = :phone
            AND A.statusIDX = 226101
        ");
        $query->bindValue(':name', $name);
        $query->bindValue(':phone', $phone);
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}



