<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMemoMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    public static function GetMemoList($data=null)
    {
        $statusIDX=$data['statusIDX'];
        $targetIDX=$data['targetIDX'];
        $staffIDX=$data['staffIDX'];
        $dbName=self::MainDBName;
        $db = static::GetDB();
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
            A.idx,
            A.memo,
            A.staffIDX,
            A.ip,
            CASE 
                WHEN A.staffIDX = :staffIDX THEN 't'
                ELSE 'f'
            END AS writerType,
            DATE_FORMAT(A.createTime, '%Y-%m-%d %H:%i') AS createTime,
            AES_DECRYPT(B.name, :dataDbKey) AS staffName,
            AES_DECRYPT(B.email, :dataDbKey) AS staffEmail
        FROM $dbName.StaffMemo AS A
        INNER JOIN $dbName.Staff AS B ON A.staffIDX=B.idx
        WHERE A.targetIDX=:targetIDX AND A.statusIDX=:statusIDX
        ORDER BY A.createTime DESC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':staffIDX', $staffIDX);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->bindValue(':statusIDX', $statusIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    
}
