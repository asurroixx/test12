<?php

namespace App\Models;

use PDO;

class ClientAuthLogMo extends \Core\Model
{
    // all Log 전체 불러오기
    public static function GetDataTableListLoad($data=null)
    {
        $startDate  = $data['startDate'] ?? '1970-01-01';
        $endDate    = $data['endDate'] ?? date('Y-m-d');
        $startDate .= ' 00:00:00';
        $endDate   .= ' 23:59:59';





        // 시작일과 종료일을 DateTime 객체로 변환
        $startDateObj = new \DateTime($startDate, new \DateTimeZone('Asia/Seoul'));
        $endDateObj = new \DateTime($endDate, new \DateTimeZone('Asia/Seoul'));

        // 시작일과 종료일의 시간대를 UTC로 변경
        $startDateObj->setTimezone(new \DateTimeZone('UTC'));
        $endDateObj->setTimezone(new \DateTimeZone('UTC'));

        // UTC로 변환된 날짜를 문자열로 다시 포맷
        $startDate = $startDateObj->format('Y-m-d H:i:s');
        $endDate = $endDateObj->format('Y-m-d H:i:s');

        
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
        idx,
        CASE
            WHEN statusIDX = 804101 THEN '기관조사성공(1단계)'
            WHEN statusIDX = 804201 THEN '기관조사실패(1단계)'
            WHEN statusIDX = 805101 THEN '토큰발행성공(2단계)'
            WHEN statusIDX = 805201 THEN '토큰발행실패(2단계)'
            WHEN statusIDX = 806101 THEN '유저요청성공(3단계)'
            WHEN statusIDX = 806201 THEN '유저요청실패(3단계)'
            WHEN statusIDX = 807101 THEN '유저결과성공(최종)'
            WHEN statusIDX = 807201 THEN '유저결과실패(최종)'
        ELSE ''
        END AS statusName,
        statusIDX,
        resultCode,
        oacxCode,
        provider,
        CASE
        WHEN LENGTH(clientMessage) > 30 THEN CONCAT(SUBSTRING(clientMessage, 1, 30), '...')
        ELSE clientMessage
        END AS clientMessage,
        name,
        birth,
        phone,
        ip,
        createTime
        FROM $dbName.ClientAuthLog
        WHERE (createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }


     public static function GetDetailLoad($data=null)
    {
        $targetIDX = $data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
        idx,
        CASE
            WHEN statusIDX = 804101 THEN '기관조사성공(1단계)'
            WHEN statusIDX = 804201 THEN '기관조사실패(1단계)'
            WHEN statusIDX = 805101 THEN '토큰발행성공(2단계)'
            WHEN statusIDX = 805201 THEN '토큰발행실패(2단계)'
            WHEN statusIDX = 806101 THEN '유저요청성공(3단계)'
            WHEN statusIDX = 806201 THEN '유저요청실패(3단계)'
            WHEN statusIDX = 807101 THEN '유저결과성공(최종)'
            WHEN statusIDX = 807201 THEN '유저결과실패(최종)'
        ELSE ''
        END AS statusName,
        statusIDX,
        uniqueKey,
        resultCode,
        oacxCode,
        provider,
        clientMessage,
        name,
        birth,
        phone,
        ip,
        fullData,
        CONVERT_TZ(createTime, '+00:00', '+09:00') AS kscTime,
        createTime AS utcTime
        FROM $dbName.ClientAuthLog
        WHERE idx = :targetIDX
        ");
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $results = $query->fetch(PDO::FETCH_ASSOC);
        return $results;
    }

  


}

