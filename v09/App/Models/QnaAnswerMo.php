<?php

namespace App\Models;

use PDO;

class QnaAnswerMo extends \Core\Model
{

    //QnaCon 대화내용 불러오기
    public static function GetContentData($data=null)
    {
        $targetIDX = $data;
        $db     = static::GetDB();
        $dbName = self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query  = $db->prepare("SELECT
        A.idx,
        A.content,
        A.staffContent,
        A.statusIDX,
        A.createTime,
        AES_DECRYPT(D.email, :dataDbKey) AS marketManagerEmail,
        AES_DECRYPT(E.email, :dataDbKey) AS staffEmail,
        F.serverName AS srcFileName,
        F.idx AS fileIDX,
        F.ext
        FROM $dbName.QnaAnswer AS A
        JOIN $dbName.Qna AS B ON A.qnaIDX = B.idx
        JOIN $dbName.MarketManager AS C ON B.marketManagerIDX = C.idx
        JOIN $dbName.Manager AS D ON C.managerIDX = D.idx
        LEFT JOIN $dbName.Staff AS E ON A.staffIDX = E.idx
        LEFT JOIN $dbName.Files AS F ON F.targetIDX = A.idx AND (F.statusIDX=441102 OR F.statusIDX=341102)
        WHERE A.qnaIDX=:targetIDX
        ORDER BY A.idx
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}