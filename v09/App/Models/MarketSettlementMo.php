<?php

namespace App\Models;

use PDO;

class MarketSettlementMo extends \Core\Model
{
    //MarketSettlementRequestCon 데이터테이블
    public static function GetMarketSettlementData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.marketIDX,
        A.requestStatusIDX,
        A.statusIDX,
        A.methodIDX,
        A.createTime,
        CASE A.requestStatusIDX
            WHEN 417101 THEN 'Crypto'
            WHEN 417102 THEN 'Bankwire'
            WHEN 417103 THEN 'KRW'
            ELSE '' END
        AS method,
        A.fee,
        A.feePer,
        A.amount,
        '' AS nonFeeAmount,
        CASE
            WHEN A.statusIDX BETWEEN 417101 AND 417103 THEN '신청'
            WHEN A.statusIDX = 417301 THEN '확인'
            WHEN A.statusIDX = 417201 THEN '완료'
            WHEN A.statusIDX = 417211 THEN '취소-이바이'
            /*WHEN A.statusIDX = 417212 THEN '취소-마켓' 20240118 안됨*/
            -- 추가 솔팅에 대한 조건을 계속해서 추가하세요.
            ELSE 'Unknown'
        END AS status,
        IFNULL(A.expectedTime, '-') AS expectedTime,
        IFNULL(A.completeTime, '-') AS completeTime,
        A.hash,
        CONCAT(B.name, '(', B.code ,')') AS marketInfo
        FROM $dbName.MarketSettlement AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function GetDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.marketIDX,
        A.methodIDX,
        CASE A.statusIDX
            WHEN 417101 THEN '신청'
            WHEN 417102 THEN '신청'
            WHEN 417103 THEN '신청'
            WHEN 417301 THEN '확인'
            WHEN 417201 THEN '완료'
            WHEN 417211 THEN '취소-이바이'
            /*WHEN 417212 THEN '취소-마켓' 20240118 안됨*/

            ELSE '' END
        AS statusName,
        A.requestStatusIDX,
        A.statusIDX,
        A.createTime,
        CASE A.requestStatusIDX
            WHEN 417101 THEN 'Crypto'
            WHEN 417102 THEN 'Bankwire'
            WHEN 417103 THEN 'KRW'
            ELSE '' END
        AS method,
        A.fee,
        A.feePer,
        A.amount,
        A.amount - A.fee AS nonFeeAmount,
        IFNULL(A.expectedTime, '-') AS expectedTime,
        IFNULL(A.completeTime, '-') AS completeTime,
        A.hash,
        A.note,
        B.name AS marketName,
        CONCAT(B.name, '(', B.code ,')') AS marketInfo,
        AES_DECRYPT(C.name, :dataDbKey) AS marketManager,
        C.position,
        AES_DECRYPT(D.email, :dataDbKey) AS managerEmail
        /*CONCAT(AES_DECRYPT(C.name,:dataDbKey), '(', C.position ,')') AS marketManager*/ /*CONCAT으로 하니까 복호화한 이름이 깨짐*/
        FROM $dbName.MarketSettlement AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        INNER JOIN $dbName.MarketManager AS C ON A.marketManagerIDX=C.idx
        INNER JOIN $dbName.Manager AS D ON C.managerIDX=D.idx
        WHERE A.idx =:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}