<?php

namespace App\Models;

use PDO;

class MarketManagerGradeMo extends \Core\Model
{

    //MarketManagerGradeCon , MarketManagerCon 정보 불러오기
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        ROW_NUMBER() OVER (ORDER BY idx ASC) AS no,
        name
        FROM $dbName.MarketManagerGrade
        ");
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    //MarketManagerGradeCon 디테일
    public static function GetGradeDetail($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
            idx,
            name
            FROM $dbName.MarketManagerGrade
            WHERE idx='$idx'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerGradeCon 이름 유효성 검사
    public static function issetNameData($data=null)
    {
        $name=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
            name
            FROM $dbName.MarketManagerGrade
            WHERE name='$name'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketManagerGradeCon 디테일 데이터 유효성
    public static function issetMarketManagerGradeData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
        idx,
        name
        FROM $dbName.MarketManagerGrade
        WHERE idx='$targetIDX'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}