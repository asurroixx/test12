<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientLogMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */



    public static function GetLogList($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $whereType=$data['whereType'];
        $whereArr=$data['whereArr'];
        $whereQuery='';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                	$arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
            		$endRange = $whereArr['end'].'999';
            		$whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        A.staffIDX,
        B.memo,
        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,
        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS clientEmail,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS clientName,
        E.ex
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.Client AS D ON A.clientIDX=D.idx
        LEFT JOIN $dbName.ClientLogEx AS E ON A.idx=E.logIDX
        WHERE A.targetIDX =$targetIDX$whereQuery
        ORDER BY A.createTime DESC;
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    
}