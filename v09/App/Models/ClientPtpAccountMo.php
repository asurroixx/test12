<?php

namespace App\Models;

use PDO;

class ClientPtpAccountMo extends \Core\Model
{

    //MarketManagerCon 정보 불러오기
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        -- (SELECT
        --     CASE idx
        --         WHEN 250101 THEN '정상'
        --         WHEN 250201 THEN '삭제클라이언트'
        --         WHEN 250211 THEN '삭제스태프'
        --         ELSE '' END
        --     AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,

        A.statusIDX,

        -- CASE A.statusIDX
        --     WHEN 250101 THEN '정상'
        --     WHEN 250201 THEN '삭제클라이언트'
        --     WHEN 250211 THEN '삭제스태프'
        --     ELSE '' END
        -- AS status,
        CASE
            WHEN B.statusIDX = 226201 THEN '회원탈퇴'
            WHEN A.statusIDX = 250101 THEN '정상'
            WHEN A.statusIDX = 250201 THEN '삭제클라이언트'
            WHEN A.statusIDX = 250211 THEN '삭제스태프'
            ELSE '??' END
        AS status,

        AES_DECRYPT(B.name,:dataDbKey) AS clientName,
        AES_DECRYPT(B.email,:dataDbKey) AS clientEmail,
        AES_DECRYPT(A.account,:dataDbKey) AS account,
        C.name AS typeName,
        A.createTime
        FROM $dbName.ClientPtpAccount AS A
        JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        JOIN $dbName.PeerToPeerType AS C ON A.PeerToPeerTypeIDX=C.idx
        -- WHERE A.statusIDX IN (402101 , 402201)
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    public static function tmpIssetAccount($data=null)
    {
        $account=$data['account'];
        $PeerToPeerTypeIDX=$data['PeerToPeerTypeIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        idx
        FROM $dbName.ClientPtpAccount
        WHERE AES_DECRYPT(account,:dataDbKey)=:account
        AND PeerToPeerTypeIDX=:PeerToPeerTypeIDX AND statusIDX='250101'
        ");

        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':account', $account);
        $query->bindValue(':PeerToPeerTypeIDX', $PeerToPeerTypeIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function GetClientPtpAccountDetailData($data=null)
    {
        $targetIDX=$data;//전자화폐 계정idx임
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.birth,:dataDbKey) AS birth,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        A.gradeIDX,

        -- (SELECT
        --     CASE idx
        --         WHEN 226101 THEN '정상'
        --         WHEN 226201 THEN '탈퇴'
        --         WHEN 226202 THEN '스태프차단'
        --         WHEN 226203 THEN '앱미가입'
        --         ELSE '' END
        --     AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,



        A.statusIDX,
        CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE '' END
        AS status,

        A.createTime,
        -- DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime,

        B.name AS gradeName,
        B.defaultFeeKRW,
        B.tradeFeePer,

        C.deviceNumber,

        CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType

        ,X.idx AS cpa_idx
        ,X.createTime AS cpa_createTime
        -- ,DATE_ADD(X.createTime, INTERVAL 9 HOUR) AS cpa_createTime
        ,X.ip AS cpa_ip
        ,X.statusIDX AS cpa_statusIDX
        ,CASE X.statusIDX
            WHEN 250101 THEN '정상'
            WHEN 250201 THEN '삭제클라이언트'
            WHEN 250211 THEN '삭제스태프'
            ELSE '' END
        AS cpa_status
        ,AES_DECRYPT(X.account,:dataDbKey) AS cpa_account
        ,X.PeerToPeerTypeIDX AS cpa_PeerToPeerTypeIDX

        ,D.name AS typeName

        FROM $dbName.ClientPtpAccount AS X

        LEFT JOIN $dbName.Client AS A ON X.clientIDX=A.idx

        LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx

        JOIN $dbName.PeerToPeerType AS D ON X.PeerToPeerTypeIDX=D.idx

        WHERE X.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetClientPtpAccountDataWithType($data=null)
    {
        $clientIDX=$data['clientIDX'];
        $ptpTypeIDX=$data['ptpTypeIDX'];

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        X.idx
        ,DATE_ADD(X.createTime, INTERVAL 9 HOUR) AS createTime
        -- ,X.createTime
        ,AES_DECRYPT(X.account,:dataDbKey) AS account

        FROM $dbName.ClientPtpAccount AS X

        WHERE X.PeerToPeerTypeIDX=:ptpTypeIDX
        AND X.clientIDX=:clientIDX
        AND X.statusIDX=250101
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':ptpTypeIDX', $ptpTypeIDX);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }











}