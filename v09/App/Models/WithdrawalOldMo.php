<?php

namespace App\Models;

use PDO;

class WithdrawalOldMo extends \Core\Model
{
    public static function GetDatatableList($data=null)
    {

        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT 
            idx,
            ROW_NUMBER() OVER (ORDER BY createTime ASC) AS no,
            name,
            code,
            refereceId,
            amount,
            invoiceId,
            createTime,
            CASE status
                WHEN 1 THEN '잡확인(매칭X)'
                WHEN 3 THEN '매칭완료'
                WHEN 4 THEN '비정상'
                ELSE status END
            AS status,
            oldIDX,
            memo
            FROM $dbName.WithdrawalOld
            WHERE (createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetTargetData($data=null)
    {
        $idx = $data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
            idx,
            oldIDX
            FROM $dbName.WithdrawalOld
            WHERE oldIDX = :idx
        ");
        $query->bindValue('idx', $idx);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetIssetNewDatatableList($data=null)
    {
        $marketCode = $data['marketCode'];
        $refereceId = $data['refereceId'];
        $amount = $data['amount'];
        $invoiceId = $data['invoiceId'];

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
            idx,
            marketCode,
            marketUserEmail,
            amount,
            invoiceID,
            createTime
            FROM $dbName.MarketWithdrawalLog
            WHERE statusIDX = 908102
            AND marketCode = :marketCode
            AND marketUserEmail = :refereceId
            AND amount = :amount
            AND invoiceID = :invoiceId
        ");
        $query->bindValue('marketCode', $marketCode);
        $query->bindValue('refereceId', $refereceId);
        $query->bindValue('amount', $amount);
        $query->bindValue('invoiceId', $invoiceId);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}   
