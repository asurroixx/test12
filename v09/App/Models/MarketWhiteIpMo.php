<?php

namespace App\Models;

use PDO;

class MarketWhiteIpMo extends \Core\Model
{

    //MarketWhiteIpCon 데이터테이블
    public static function GetDatatableListData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            A.idx,
            (
                SELECT
                    CASE idx
                        WHEN 428101 THEN 'IP추가신청'
                        WHEN 428102 THEN 'IP삭제신청'
                        WHEN 428201 THEN 'IP신청취소'
                        WHEN 351101 THEN '승인'
                        WHEN 351102 THEN '삭제'
                        WHEN 351201 THEN '반려'
                        ELSE '' END 
                AS status
                FROM $dbName.Status WHERE A.statusIDX = idx
            ) AS status,
            A.ipAddress,
            A.statusIDX,
            A.createTime,
            B.code AS marketCode,
            B.name AS marketName
        FROM $dbName.MarketWhiteIp AS A
        JOIN $dbName.Market AS B ON A.marketIDX = B.idx
        ORDER BY
            CASE 
                WHEN A.statusIDX LIKE '4281%' THEN 0 
                ELSE 1 
            END,
            A.createTime DESC;
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketWhiteIpCon 디테일
    public static function GetDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 428101 THEN 'IP추가신청'
                WHEN 428102 THEN 'IP삭제신청'
                WHEN 428201 THEN 'IP신청취소'
                WHEN 351101 THEN '승인'
                WHEN 351102 THEN '삭제'
                WHEN 351201 THEN '반려'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.ipAddress,
        A.createTime,
        B.idx AS marketIDX,
        B.code AS marketCode,
        B.name AS marketName
        FROM $dbName.MarketWhiteIp AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketWhiteIpCon 업데이트 전 조사
    public static function GetTargetData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        (SELECT
            CASE idx
                WHEN 428101 THEN 'IP추가신청'
                WHEN 428102 THEN 'IP삭제신청'
                WHEN 428201 THEN 'IP신청취소'
                WHEN 351101 THEN '승인'
                WHEN 351102 THEN '삭제'
                WHEN 351201 THEN '반려'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS statusVal,
        A.ipAddress,
        A.createTime,
        B.idx AS marketIDX,
        B.name AS marketName
        FROM $dbName.MarketWhiteIp AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}