<?php

namespace App\Models;

use PDO;

class ClientMileageWithdrawalMo extends \Core\Model
{

    //MileageWithdrawalCon 날짜별 데이터 리셋
    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (204201, 204202) THEN idx END) AS successTotalCount,
        COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (204211, 204212) THEN idx END) AS failTotalCount,
        IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (204201, 204202) THEN amount ELSE 0 END), 0) AS successAmount,
        IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (204211, 204212) THEN amount ELSE 0 END), 0) AS failureAmount
        FROM $dbName.ClientMileageWithdrawal
        WHERE createTime BETWEEN '$startDate' AND '$endDate'
        /*성공 및 실패건만 카운트 하기 (신청건은 카운트 미포함)*/
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 솔팅별 데이터 리셋
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data['columnsVal'];

        $firstString=$columnsVal[0];
        $secondString=$columnsVal[1];

        $firstArray = explode('|', $firstString);
        $secondArray = explode('|', $secondString);

        $firstQuery = 'AND A.statusIDX IN (';
        $secondQuery = 'AND (A.amount ';
        // if(isset($firstString)&&!empty($firstString)){
        //     $secondQuery = 'AND (A.amount ';
        // }

        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {
                $statusIDX='';
                switch ($key) {
                    case '수동신청':
                        $statusIDX='204101';
                    break;
                    case '자동신청':
                        $statusIDX='204102';
                    break;
                    case '수동완료':
                        $statusIDX='204201';
                    break;
                    case '자동완료':
                        $statusIDX='204202';
                    break;
                    case '기타취소':
                        $statusIDX='203211';
                    break;
                    case '스태프취소':
                        $statusIDX='204212';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $statusIDX;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($secondArray)) {
            $count = count($secondArray);
            foreach ($secondArray as $index => $key) {
                $betVal='';
                switch ($key) {
                    case 'A':
                        $betVal='BETWEEN 1 AND 1000000';
                    break;
                    case 'B':
                        $betVal='BETWEEN 1000001 AND 5000000';
                    break;
                    case 'C':
                        $betVal='BETWEEN 5000001 AND 10000000';
                    break;
                    case 'D':
                        $betVal='>= 10000001';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $secondQuery .= $betVal;
                if ($index < $count - 1) {
                    $secondQuery .= ') OR (A.amount ';
                }
            }
            $secondQuery .= ')';
            if($key==''){
                $secondQuery = ''; // 빈 문자열로 설정
            }
        }

        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }


        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
            IFNULL(SUM(CASE WHEN A.statusIDX IN (204201, 204202) THEN 1 ELSE 0 END), 0) AS successTotalCount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (204211, 204212) THEN 1 ELSE 0 END), 0) AS failTotalCount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (204201, 204202) THEN A.amount ELSE 0 END), 0) AS successAmount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (204211, 204212) THEN A.amount ELSE 0 END), 0) AS failureAmount
        FROM
            $dbName.ClientMileageWithdrawal AS A
        WHERE A.createTime BETWEEN '$startDate' AND '$endDate'
        ".$firstQuery."".$secondQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    //MileageWithdrawalCon 데이터테이블
    public static function GetClientMileageWithdrawalData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        CASE A.statusIDX
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            WHEN 204201 THEN '수동완료'
            WHEN 204202 THEN '자동완료'
            WHEN 204211 THEN '기타취소'
            WHEN 204212 THEN '스태프취소'
            ELSE '' END
        AS status,
        CAST(A.amount AS CHAR) AS amount,
        FORMAT(A.fee, 0) AS fee,
        A.createTime,
        C.idx AS clientIDX,
        C.statusIDX AS clientStatus,
        C.withdrawalBlock,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        D.name AS bankName,
        IFNULL((SELECT CONCAT(AES_DECRYPT(name,:dataDbKey),' (',AES_DECRYPT(email,:dataDbKey),')') FROM $dbName.Staff WHERE idx=E.StaffIDX),'-') AS staffEmail,
        CASE
            WHEN A.statusIDX = 204202 THEN
                (SELECT IFNULL(createTime, '-') FROM $dbName.ClientLog
                WHERE statusIDX = 204202 AND targetIDX = A.idx AND clientIDX=C.idx)
            ELSE IFNULL(E.createTime,'-')
        END AS updateTime,
        CASE
            WHEN A.amount BETWEEN 1 AND 1000000 THEN 'A'
            WHEN A.amount BETWEEN 1000001 AND 5000000 THEN 'B'
            WHEN A.amount BETWEEN 5000001 AND 10000000 THEN 'C'
            WHEN A.amount >= 10000001 THEN 'D'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS amountGrade
        FROM $dbName.ClientMileageWithdrawal AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.Bank AS D ON B.bankIDX=D.idx
        LEFT JOIN $dbName.ClientLog AS E
            ON ( A.statusIDX = 204201 AND E.statusIDX = 204201 AND  A.idx = E.targetIDX )
            OR ( A.statusIDX = 204212 AND E.statusIDX = 204212 AND  A.idx = E.targetIDX )
        WHERE (A.createTime BETWEEN :startDate AND :endDate)
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':startDate', $startDate);
        $Sel->bindValue(':endDate', $endDate);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 해당 거래 디테일
    public static function GetWithdrawalDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        CASE A.statusIDX
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            WHEN 204201 THEN '수동완료'
            WHEN 204202 THEN '자동완료'
            WHEN 204211 THEN '기타취소'
            WHEN 204212 THEN '스태프취소'
            ELSE '' END
        AS status,
        FORMAT(A.amount, 0) AS amount,
        FORMAT(A.fee, 0) AS fee,
        A.createTime,
        C.idx AS clientIDX,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS clientBankNumber,
        AES_DECRYPT(B.accountHolder,:dataDbKey) AS clientBankAccountHolder,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        D.name AS bankName,
        IFNULL((SELECT CONCAT(AES_DECRYPT(name,:dataDbKey),' (',AES_DECRYPT(email,:dataDbKey),')') FROM $dbName.Staff WHERE idx=E.StaffIDX),'-') AS staffEmail,
        IFNULL(E.createTime,'-') AS updateTime,
        F.name AS gradeName
        FROM $dbName.ClientMileageWithdrawal AS A
        LEFT JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        LEFT JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.Bank AS D ON B.bankIDX=D.idx
        LEFT JOIN $dbName.ClientLog AS E
            ON ( A.statusIDX = 204201 AND E.statusIDX = 204201 AND  A.idx = E.targetIDX )
            OR ( A.statusIDX = 204212 AND E.statusIDX = 204212 AND  A.idx = E.targetIDX )
        JOIN $dbName.ClientGrade AS F ON C.gradeIDX=F.idx
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 해당 idx 정보 가져오기
    public static function GetTargetDataPack($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.amount,
        A.fee,
        C.idx AS clientIDX
        FROM $dbName.ClientMileageWithdrawal AS A
        JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        JOIN $dbName.Client AS C ON B.clientIDX=C.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 해당 idx 정보 가져오기 (노드)
    public static function GetTargetNodeData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        B.clientIDX,
        A.createTime,
        C.withdrawalBlock,
        AES_DECRYPT(C.name,:dataDbKey) AS name,
        AES_DECRYPT(C.email,:dataDbKey) AS clientEmail,
        D.name AS bankName,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        FORMAT(A.fee, 0) AS fee,
        CAST(A.amount AS CHAR) AS amount,
        CASE A.statusIDX
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            ELSE '' END
        AS status,
        IFNULL((SELECT AES_DECRYPT(email,:dataDbKey) FROM $dbName.Staff WHERE idx=E.StaffIDX),'-') AS staffEmail,
        CASE
            WHEN A.statusIDX = 204202 THEN
                (SELECT IFNULL(createTime, '-') FROM $dbName.ClientLog
                WHERE statusIDX = 204202 AND targetIDX = A.idx AND clientIDX=C.idx)
            ELSE IFNULL(E.createTime,'-')
        END AS updateTime,
        CASE
            WHEN A.amount BETWEEN 1 AND 1000000 THEN 'A'
            WHEN A.amount BETWEEN 1000001 AND 5000000 THEN 'B'
            WHEN A.amount BETWEEN 5000001 AND 10000000 THEN 'C'
            WHEN A.amount >= 10000001 THEN 'D'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS amountGrade
        FROM $dbName.ClientMileageWithdrawal AS A
        JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        JOIN $dbName.Client AS C ON B.clientIDX=C.idx
        JOIN $dbName.Bank AS D ON B.bankIDX=D.idx
        LEFT JOIN $dbName.ClientLog AS E
            ON ( A.statusIDX = 204201 AND E.statusIDX = 204201 AND  A.idx = E.targetIDX )
            OR ( A.statusIDX = 204212 AND E.statusIDX = 204212 AND  A.idx = E.targetIDX )
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 해당 idx 정보 가져오기
    public static function tmpTargetData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.amount,
        A.fee,
        B.idx AS clientIDX,
        B.migrationIDX
        FROM $dbName.ClientMileageWithdrawal AS A
        JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }





}