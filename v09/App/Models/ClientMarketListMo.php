<?php

namespace App\Models;

use PDO;

class ClientMarketListMo extends \Core\Model
{

    //clientMarketListCon 데이터테이블
    public static function GetDataTableData($data=null)
    {
        $startDate   = $data['startDate'] ?? '1970-01-01';
        $endDate     = $data['endDate'] ?? date('Y-m-d');
        $startDate  .= ' 00:00:00';
        $endDate    .= ' 23:59:59';
        $dataDbKey = self::dataDbKey;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        AES_DECRYPT(A.referenceId, '$dataDbKey') AS referenceId,
        A.createTime,
        A.statusIDX,
        A.note,
        CASE
            WHEN A.statusIDX = 240101 THEN '정상'
            WHEN A.statusIDX = 240102 THEN '임시'
            WHEN A.statusIDX = 240201 THEN '비활성'
            -- 추가 솔팅에 대한 조건을 계속해서 추가하세요.
            ELSE '-'
        END AS status,
        B.name,
        B.code,
        C.idx AS clientIDX,
        AES_DECRYPT(C.name, '$dataDbKey') AS clientName
        FROM $dbName.ClientMarketList AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        INNER JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientMarketListCon 디테일
    public static function GetDetailData($data=null)
    {
        $targetIDX = $data;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->query("SELECT
        A.idx,
        A.marketIDX,
        A.clientIDX,
        A.statusIDX,
        AES_DECRYPT(A.referenceId, '$dataDbKey') AS referenceId,
        A.createTime,
        CASE
            WHEN A.statusIDX = 240101 THEN '정상'
            WHEN A.statusIDX = 240102 THEN '임시'
            WHEN A.statusIDX = 240201 THEN '비활성'
            -- 추가 솔팅에 대한 조건을 계속해서 추가하세요.
            ELSE '-'
        END AS status,
        A.note,
        A.migrationIDX,
        B.name AS marketName,
        B.code AS marketCode,
        AES_DECRYPT(C.name, '$dataDbKey') AS clientName,
        AES_DECRYPT(C.birth, '$dataDbKey') AS clientBirth,
        AES_DECRYPT(C.phone, '$dataDbKey') AS clientPhone,
        AES_DECRYPT(C.email, '$dataDbKey') AS clientEmail
        FROM $dbName.ClientMarketList AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        INNER JOIN $dbName.Client AS C ON A.clientIDX = C.idx
        WHERE A.idx ='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientMarketListCon 해당 이메일에 대한 히스토리
    public static function GetClientRefereceIDCheck($data=null)
    {
        $marketIDX = $data['marketIDX'];
        $referenceId = $data['referenceId'];


        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->query("SELECT
        DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS '시간',
        B.code AS '마켓코드',
        B.name AS '마켓이름',
        C.idx AS clientIDX,
        AES_DECRYPT(C.name, '$dataDbKey') AS '고객명',
        AES_DECRYPT(C.phone, '$dataDbKey') AS '휴대폰번호'
        FROM $dbName.ClientMarketList AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        INNER JOIN $dbName.Client AS C ON A.clientIDX = C.idx
        WHERE A.marketIDX ='$marketIDX' AND AES_DECRYPT(A.referenceId, '$dataDbKey') = '$referenceId' AND A.statusIDX = 240201
        ORDER BY A.createTime DESC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetDetail($data=null)
    {
        $targetIDX = $data;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $dataDbKey = self::dataDbKey;



        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        AES_DECRYPT(A.referenceId, '$dataDbKey') AS referenceId,
        A.createTime,
        CONCAT('UTC : ', A.createTime) AS utcCreateTime,
        CONCAT('KST : ', DATE_ADD(A.createTime, INTERVAL 9 HOUR)) AS kstCreateTime,
        A.statusIDX,
        A.note,
        CASE
            WHEN A.statusIDX = 240101 THEN 'Active'
            WHEN A.statusIDX = 240102 THEN 'Temporary'
            WHEN A.statusIDX = 240201 THEN 'Inactive'
            -- 추가 솔팅에 대한 조건을 계속해서 추가하세요.
            ELSE '-'
        END AS status,
        B.name AS clientName,
        B.email AS clientEmail
        FROM $dbName.ClientMarketList AS A
        INNER JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        WHERE A.idx ='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //client IDX 받아오기
    public static function GetClientIDX($data=null)
    {
        $clientEmail=$data;
        $db = static::GetDB();
        $dataDbKey = self::dataDbKey;

        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        marketIDX,
        clientIDX
        FROM $dbName.ClientMarketList
        WHERE AES_DECRYPT(referenceId, '$dataDbKey')='$clientEmail'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetClientEmail($data=null)
    {
        $idx=$data;
        $dataDbKey = self::dataDbKey;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        marketIDX,
        clientIDX,
        AES_DECRYPT(referenceId, '$dataDbKey') AS referenceId
        FROM $dbName.ClientMarketList
        WHERE clientIDX='$idx'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetClientNamePack($data=null)
    {
        $dataDbKey = self::dataDbKey;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $Sel       = $db->query("SELECT
        A.clientIDX,
        AES_DECRYPT(A.referenceId, '$dataDbKey') AS referenceId,
        B.code,
        B.name,
        AES_DECRYPT(C.name, '$dataDbKey') AS clientName
        FROM $dbName.ClientMarketList AS A
        JOIN $dbName.Market AS B ON B.idx = A.marketIDX
        JOIN $dbName.Client AS C ON C.idx = A.clientIDX
        WHERE A.statusIDX IN ('240101','240102')
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetArrClientNamePack($data=null)
    {
        $emailArr = $data;

        $emailList = implode(',', array_map(function ($email) {
            return "'$email'";
        }, $emailArr));

        $dataDbKey = self::dataDbKey;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $Sel       = $db->query("SELECT
        A.clientIDX,
        AES_DECRYPT(A.referenceId, '$dataDbKey') AS referenceId,
        B.code,
        B.name,
        AES_DECRYPT(C.name, '$dataDbKey') AS clientName
        FROM $dbName.ClientMarketList AS A
        JOIN $dbName.Market AS B ON B.idx = A.marketIDX
        JOIN $dbName.Client AS C ON C.idx = A.clientIDX
        WHERE A.statusIDX IN ('240101','240102')
        AND AES_DECRYPT(A.referenceId, '$dataDbKey') IN ($emailList)
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    //ClientMarketListCon , 하나더 쓰이는데 찾아야함 ㅠ
    public static function GetClientIDXData($data=null)
    {
        $marketCode = $data['marketCode'];
        $marketUserEmail = $data['marketUserEmail'];
        $dataDbKey = self::dataDbKey;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $Sel       = $db->prepare("SELECT
        A.idx,
        A.clientIDX
        FROM $dbName.ClientMarketList AS A
        LEFT JOIN $dbName.Market AS B ON B.idx = A.marketIDX
        WHERE B.code = :marketCode AND AES_DECRYPT(A.referenceId, :dataDbKey) = :marketUserEmail AND A.statusIDX = 240101
        ");
        $Sel->bindValue(':marketCode', $marketCode);
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':marketUserEmail', $marketUserEmail);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function GetClientMarketListByIDX($data=null)
    {
        $clientIDX = $data;
        $dataDbKey = self::dataDbKey;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $Sel       = $db->prepare("SELECT
            A.idx,
            A.marketIDX,
            AES_DECRYPT(A.referenceId, :dataDbKey) AS referenceId,
            A.ip,
            A.createTime,
            A.note,
            A.migrationIDX
        FROM $dbName.ClientMarketList AS A
        WHERE A.clientIDX = :clientIDX AND  A.statusIDX = 240101
        ");
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientCon 등록된 클라이언트 마켓 리스트 (clientIDX 기준)
    //240213 채팅에서도 쓰고있어여 수정시 채팅도 수정해야함 
    public static function GetClientMarketListData($data=null)
    {
        $clientIDX = $data;
        $dataDbKey = self::dataDbKey;
        $db        = static::GetDB();
        $dbName    = self::MainDBName;
        $Sel       = $db->prepare("SELECT
            A.idx,
            AES_DECRYPT(A.referenceId, :dataDbKey) AS referenceId,
            A.createTime,
            B.code AS marketCode,
            B.name AS marketName
        FROM $dbName.ClientMarketList AS A
        JOIN $dbName.Market AS B ON A.marketIDX = B.idx
        WHERE A.clientIDX = :clientIDX AND  A.statusIDX = 240101
        ");
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}