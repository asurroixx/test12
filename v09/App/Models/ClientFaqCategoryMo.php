<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientFaqCategoryMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//FaqCon 1차 카테고리
	public static function GetCategoryList($data=null)
	{
		$dbName=self::MainDBName;
		$db = static::GetDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.seq,
		A.statusIDX
		FROM $dbName.ClientFaqCategory AS A
        WHERE A.momIDX=0 ORDER BY A.seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//FaqCon 2차 카테고리
	public static function GetChildCategoryList($data=null)
	{
		$menuIDX=$data;
		$dbName=self::MainDBName;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.seq,
		A.statusIDX
		FROM $dbName.ClientFaqCategory AS A
        WHERE A.momIDX='$menuIDX'
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//QnaCon 추가할때 이름 조사
	public static function GetIssetNameData($data=null)
	{
		$name=$data;
		$dbName=self::MainDBName;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.name
		FROM $dbName.ClientFaqCategory AS A
        WHERE A.name='$name'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//QnaCon 메뉴 숨김 status 조사
	public static function CheeseEyeAction($data=null)
	{
		$targetIDX=$data;
		$dbName=self::MainDBName;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		statusIDX
		FROM $dbName.ClientFaqCategory
        WHERE idx='$targetIDX' ORDER BY seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//QnaCon 컨펌창 열때 정보 불러오기 , 디테일 카테고리 변경할때 idx있는지 조사
	public static function GetTargetData($data=null)
	{
		$targetIDX=$data;
		$dbName=self::MainDBName;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		name
		FROM $dbName.ClientFaqCategory
        WHERE idx='$targetIDX'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//QnaCon 삭제 전 2차메뉴가 있는지 조사
	public static function CategoryDeleteCheck($data=null)
	{
		$targetIDX=$data;
		$dbName=self::MainDBName;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		statusIDX
		FROM $dbName.ClientFaqCategory
        WHERE momIDX='$targetIDX'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	// QnaCon 해당 카테고리 idx
    public static function GetCategoryDataForIDX($data=null)
    {
        $idx=$data;
        $db = static::getDB();
		$dbName=self::MainDBName;
        $sel = $db->query("SELECT
        A.idx,
        A.momIDX
        FROM $dbName.ClientFaqCategory AS A
        WHERE A.idx='$idx'
        ");
        $result=$sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

	// QnaCon 디테일 카테고리 불러오기
    public static function GetCategoryListForParentIDX($data=null)
    {
    	$tmpParentIDX=$data['parentIDX'];
        $tmpType=$data['type'];//내가 몇차인지: 2차리스트가 필요하면 2
        $tmpQuery='';
        if($tmpType=='1'){//1차 리스트주세요
            $tmpQuery=' A.momIDX="0"';
        }elseif($tmpType=='2'){//2차 리스트주세요
            $tmpQuery=' A.momIDX="'.$tmpParentIDX.'"';
        }
        $db = static::getDB();
		$dbName=self::MainDBName;
        $sel = $db->query("SELECT
        A.idx,
        A.name
        FROM $dbName.ClientFaqCategory AS A
        WHERE ".$tmpQuery."
        ORDER BY A.seq
        ");
        $result=$sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    
}