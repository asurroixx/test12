<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StatusMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    //StatusCon 기본정보
    public static function GetStatusData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        memo
        FROM $dbName.Status
        ORDER BY idx
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StatusCon 디테일
    public static function GetStatusDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        memo
        FROM $dbName.Status
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //StatusCon idx 유효성
    public static function GetIssetIDXData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx
        FROM $dbName.Status
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //staffCon status 활성 비활성 가져올때
    public static function GetStatusTwoAndThree($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        memo
        FROM $dbName.Status
        WHERE idx IN (302101,302201)
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 솔팅 status 가져올때
    public static function GetMileageDepositSorting($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        CASE idx
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS memo
        FROM $dbName.Status
        WHERE idx IN (203101,203102,203103,203104,203201,203202,203203,203211,203212,203213,203214,203215)
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageWithdrawalCon 솔팅 status 가져올때
    public static function GetMileageWithdrawalSorting($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        CASE idx
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            WHEN 204201 THEN '수동완료'
            WHEN 204202 THEN '자동완료'
            WHEN 204211 THEN '기타취소'
            WHEN 204212 THEN '스태프취소'
            ELSE '' END
        AS memo
        FROM $dbName.Status
        WHERE idx IN (204101,204102,204201,204202,204211,204212)
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



}
