<?php

namespace App\Models;

use PDO;

class MileageHistoryMgErrorLogMo extends \Core\Model
{

    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.createTime,
        A.statusIDX,
        CASE A.statusIDX
            WHEN '908102' THEN '출금대행'
            WHEN '906101' THEN '지불대행'
            WHEN '203201,203202' THEN '마일리지 입금'
            WHEN '204101,204102' THEN '마일리지 출금'
            WHEN '205101' THEN '스태프에 의한 증감'
            WHEN '205201' THEN '스태프에 의한 차감'
            ELSE ''
        END AS content,
        A.targetIDX,
        A.amount,
        B.statusIDX AS clientStausIDX,
        CASE B.migrationIDX
            WHEN 0 THEN '신규'
            ELSE B.migrationIDX END
        AS migrationIDX,
        AES_DECRYPT(B.name,:dataDbKey) AS name
        FROM $dbName.MileageHistoryMgErrorLog AS A
        INNER JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }




    public static function HundredCheck($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;



        $query = $db->prepare("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.createTime,
        A.statusIDX,
        CASE A.statusIDX
            WHEN '908102' THEN '출금대행'
            WHEN '906101' THEN '지불대행'
            WHEN '203201,203202' THEN '마일리지 입금'
            WHEN '204101,204102' THEN '마일리지 출금'
            WHEN '205101' THEN '스태프에 의한 증감'
            WHEN '205201' THEN '스태프에 의한 차감'
            ELSE ''
        END AS content,
        A.targetIDX,
        A.amount,
        B.statusIDX AS clientStausIDX,
        CASE B.migrationIDX
            WHEN 0 THEN '신규'
            ELSE B.migrationIDX END
        AS migrationIDX,
        AES_DECRYPT(B.name,:dataDbKey) AS name
        FROM $dbName.MileageHistoryMgErrorLog AS A
        INNER JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
