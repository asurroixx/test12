<?php

namespace App\Models;

use PDO;

class StaffAlarmMo extends \Core\Model
{

    //NavCon
    public static function GetResetCount($data=null)
    {
        $staffIDX=$data;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            A.menuIDX,
            COUNT(A.idx) AS count,
            B.momIDX
            FROM $dbName.StaffAlarm AS A
            LEFT JOIN $dbName.StaffMenu AS B ON A.menuIDX=B.idx
            WHERE A.staffIDX = '$staffIDX' AND A.viewStatusIDX = 305201
            GROUP BY A.menuIDX
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }

    //NavCon
    public static function GetTotalCount($data=null)
    {
        $staffIDX=$data;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            COUNT(A.idx) AS count,
            B.momIDX
            FROM $dbName.StaffAlarm AS A
            LEFT JOIN $dbName.StaffMenu AS B ON A.menuIDX=B.idx
            WHERE A.staffIDX = '$staffIDX' AND A.viewStatusIDX = 305201
            GROUP BY B.momIDX
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }


}