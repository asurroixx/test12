<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientGradeMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//ClientGradeCon 데이터테이블
	public static function GetClientGradeData($data=null)
	{
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		A.idx,
		A.name,
		FORMAT(A.defaultFeeKRW, 0) AS defaultFeeKRW,
		A.tradeFeePer,
		A.tranFeePer,
		A.withFeePer,
		FORMAT(A.withFeeKRW, 0) AS withFeeKRW,
		FORMAT(A.withMinKRW, 0) AS withMinKRW,
		FORMAT(A.upto, 0) AS upto,
        (SELECT
        	CASE idx
                WHEN 325301 THEN '정상'
                WHEN 325401 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        C.name AS bankName
		FROM $dbName.ClientGrade AS A
		JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
		JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientGradeCon 스태프 등급 디테일
	public static function GetClientGradeDetail($data=null)
	{
		$targetIDX=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		A.idx,
		A.name,
		A.ebuyBankIDX,
		A.defaultFeeKRW,
		A.tradeFeePer,
		A.tranFeePer,
		A.withFeePer,
		A.withFeeKRW,
		A.withMinKRW,
		A.upto,
        (SELECT idx FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        C.name AS bankName
		FROM $dbName.ClientGrade AS A
		JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
		JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
		WHERE A.idx='$targetIDX'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientGradeCon 동일한 등급 이름 조사
	public static function IssetGradeName($data=null)
	{
		$name=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		idx,
		name
		FROM $dbName.ClientGrade
		WHERE name='$name'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientCon 등급 리스트
	public static function GetClientGradeList($data=null)
	{
		$db = static::GetDB();
        $dbName= self::MainDBName;
		$CategoryList = $db->query("SELECT
		idx,
		name
		FROM $dbName.ClientGrade
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientGradeCon 모든정보 유효성 idx기준
    public static function issetClientGradeData($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.ebuyBankIDX,
        A.defaultFeeKRW,
        A.tradeFeePer,
        A.tranFeePer,
        A.withFeePer,
        A.withFeeKRW,
        A.withMinKRW,
        A.upto,
        C.name AS ebuyBankName
        FROM $dbName.ClientGrade AS A
        JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX=B.idx
        JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
        WHERE A.idx='$targetIDX'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //20240115 ptp 자동 입금신청때 APP 서버에서 훔쳐옴
    public static function getEbuyBankInfo($data=null)
    {
        $dbName= self::MainDBName;
        $gradeIDX=$data;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
            A.idx,
            A.ebuyBankIDX,
            A.name AS gradeName,
            B.accountNumber AS ebuyAccountNumber,
            B.accountHolder AS ebuyAccountHolder,
            B.statusIDX AS ebuyBankStatusIDX,
            C.name AS ebuyBankName
            FROM $dbName.ClientGrade AS A
            INNER JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
            INNER JOIN $dbName.Bank AS C ON B.bankIDX = C.idx
            WHERE A.idx='$gradeIDX'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}