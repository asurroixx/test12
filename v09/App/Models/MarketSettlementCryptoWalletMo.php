<?php

namespace App\Models;

use PDO;

class MarketSettlementCryptoWalletMo extends \Core\Model
{

    //marketSettlementCon 해당 마켓 지갑주소
    public static function GetSettlementCryptoWalletData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.walletAddr,
        A.createTime,
        B.name AS coinName,
        B.code AS coinCode,
        C.name AS mainnetName,
        C.code AS mainnetCode
        FROM $dbName.MarketSettlementCryptoWallet AS A
        JOIN $dbName.MainnetCoin AS B ON A.mainnetCoinIDX=B.idx
        JOIN $dbName.Mainnet AS C ON B.mainnetIDX=C.idx
        WHERE A.marketIDX='$marketIDX' AND A.statusIDX=425101
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketSettlementRequestCon 정산신청시에 해당정보
    public static function GetMarketSettlementInfo($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.walletAddr,
        A.createTime,
        B.name AS coinName,
        B.code AS coinCode,
        C.name AS mainnetName,
        C.code AS mainnetCode
        FROM $dbName.MarketSettlementCryptoWallet AS A
        JOIN $dbName.MainnetCoin AS B ON A.mainnetCoinIDX=B.idx
        JOIN $dbName.Mainnet AS C ON B.mainnetIDX=C.idx
        WHERE A.idx='$idx'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketSettlementTableCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.walletAddr,
        A.createTime,
        B.name AS coinName,
        B.code AS coinCode,
        C.name AS mainnetName,
        C.code AS mainnetCode,
        D.name AS marketName
        FROM $dbName.MarketSettlementCryptoWallet AS A
        JOIN $dbName.MainnetCoin AS B ON A.mainnetCoinIDX=B.idx
        JOIN $dbName.Mainnet AS C ON B.mainnetIDX=C.idx
        JOIN $dbName.Market AS D ON A.marketIDX = D.idx
        WHERE A.statusIDX = 425101
        ");
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketSettlementTableCon 디테일
    public static function GetDetailLoad($data=null)
    {
        $targetIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.walletAddr,
        A.createTime,
        A.memo,
        B.name AS coinName,
        B.code AS coinCode,
        C.name AS mainnetName,
        C.code AS mainnetCode,
        D.idx AS marketIDX,
        D.name AS marketName,
        D.code AS marketCode,
        AES_DECRYPT(E.name,:dataDbKey) AS marketManagerName,
        AES_DECRYPT(F.email,:dataDbKey) AS marketManagerEmail
        FROM $dbName.MarketSettlementCryptoWallet AS A
        JOIN $dbName.MainnetCoin AS B ON A.mainnetCoinIDX=B.idx
        JOIN $dbName.Mainnet AS C ON B.mainnetIDX=C.idx
        JOIN $dbName.Market AS D ON A.marketIDX = D.idx
        JOIN $dbName.MarketManager AS E ON A.marketManagerIDX = E.idx
        JOIN $dbName.Manager AS F ON E.managerIDX = F.idx
        WHERE A.idx = :targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}