<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffLogMo extends \Core\Model
{
    //StaffCon 로그인히스토리
    public static function getStaffLoginHistory($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $targetIDX=$data['targetIDX'];
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        createTime,
        ip
        FROM $dbName.StaffLog
        WHERE staffIDX='$targetIDX' AND (createTime BETWEEN '$startDate' AND '$endDate')
        AND statusIDX = 304101
        ORDER BY createTime DESC
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function GetLogList($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $whereType=$data['whereType'];
        $whereArr=$data['whereArr'];
        $whereQuery='';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
                    $endRange = $whereArr['end'].'999';
                    $whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        -- DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime,
        A.staffIDX,
        B.memo,
        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,
        D.ex
        FROM $dbName.StaffLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.StaffLogEx AS D ON A.idx=D.logIDX
        WHERE A.targetIDX =$targetIDX$whereQuery
        ORDER BY A.createTime DESC;
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StaffLogCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.createTime,
        B.memo,
        C.ex,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS staffName,
        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS staffEmail
        FROM $dbName.StaffLog AS A
        LEFT JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.StaffLogEx AS C ON A.idx = C.logIDX
        INNER JOIN $dbName.Staff AS D ON A.staffIDX = D.idx
        WHERE A.createTime BETWEEN :startDate AND :endDate;
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':startDate', $startDate);
        $Sel->bindValue(':endDate', $endDate);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StaffLogCon 디테일
    public static function GetLogDetailData($data=null)
    {
        $targetIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.createTime,
        A.staffIDX,
        B.memo,
        IFNULL(C.ex,'-') AS ex,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS staffName,
        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS staffEmail,
        D.clientIDX,
        D.appActiveStat,
        D.appSettlementStat,
        E.name AS gradeName
        FROM $dbName.StaffLog AS A
        LEFT JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.StaffLogEx AS C ON A.idx = C.logIDX
        INNER JOIN $dbName.Staff AS D ON A.staffIDX = D.idx
        INNER JOIN $dbName.StaffGrade AS E ON D.gradeIDX = E.idx
        WHERE A.idx = :targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}
