<?php

namespace App\Models;

use PDO;

class EbuyBankLogMo extends \Core\Model
{
    //MileageDepositCon 입금 로그 정보
    public static function GetEbuyBankLogData($data=null)
    {
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        idx,
        format(depositAmount, 0) AS depositAmount,
        clientBank,
        clientAccountHolder,
        DATE_ADD(tradeTime, INTERVAL 9 HOUR) AS tradeTime
        FROM $dbName.EbuyBankLog
        WHERE targetIDX='0' AND tradeTime > SUBDATE(NOW(), INTERVAL 7 DAY)
        AND clientAccountHolder NOT REGEXP '[^[:alnum:][:space:]]�' /*문자,숫자,공백 제외한 나머지 글자는 제외*/
        ORDER BY tradeTime DESC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 타겟정보
    public static function GetEbuyBankLogTargetData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        idx,
        format(depositAmount, 0) AS amount,
        clientAccountHolder,
        tradeTime
        FROM $dbName.EbuyBankLog
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    //자동신청건 매칭
    public static function MinJobEbuyBankLogList($data=null)
    {
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        idx,
        depositAmount,
        clientBankIDX,
        clientBank,
        ebuyBankIDX,
        clientAccountHolder,
        tradeTime
        FROM $dbName.EbuyBankLog
        WHERE statusIDX=101101
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //매칭건 가져오기
    public static function GetTargetMinJobEbuyBankLog($data=null)
    {
        $accountHolder = $data['accountHolder'];
        $createTime = $data['createTime'];



        $dataDbKey=self::dataDbKey;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->prepare("SELECT
            idx,
            depositAmount,
            clientBankIDX,
            ebuyBankIDX,
            clientAccountHolder,
            tradeTime
        FROM $dbName.EbuyBankLog
        WHERE
            clientAccountHolder LIKE CONCAT('%', :accountHolder, '%') AND
            tradeTime > :createTime
        ");
        $Sel->bindValue(':accountHolder', $accountHolder);
        $Sel->bindValue(':createTime', $createTime);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 날짜별 데이터 리셋
    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->prepare("SELECT
        COUNT(CASE WHEN tradeTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (101101) THEN idx END) AS beforeCount,
        COUNT(CASE WHEN tradeTime BETWEEN '$startDate' AND '$endDate' AND statusIDX IN (101102) THEN idx END) AS afterCount,
        IFNULL(SUM(CASE WHEN tradeTime BETWEEN '$startDate' AND '$endDate' THEN withdrawalAmount ELSE 0 END), 0) AS withdrawalAmount,
        IFNULL(SUM(CASE WHEN tradeTime BETWEEN '$startDate' AND '$endDate' THEN depositAmount ELSE 0 END), 0) AS depositAmount
        FROM $dbName.EbuyBankLog
        WHERE tradeTime BETWEEN '$startDate' AND '$endDate';
        /*이바이 계좌내역 금액들은 매칭 전/후 상관없이 입/출금 금액만 보여줘도 될듯함*/
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 솔팅별 데이터 리셋
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data;

        $firstString = $columnsVal[0];
        $secondString=$columnsVal[1];
        $thirdString=$columnsVal[2];

        $firstArray = explode('|', $firstString);
        $secondArray = explode('|', $secondString);
        $thirdArray = explode('|', $thirdString);

        $firstQuery = 'WHERE C.name IN (';
        $secondQuery = 'WHERE (A.depositAmount ';
        $thirdQuery = 'WHERE (A.withdrawalAmount ';

        if(isset($firstString)&&!empty($firstString)){
            $secondQuery = 'AND (A.depositAmount';
            $thirdQuery = 'AND (A.withdrawalAmount ';
        }

        if(isset($secondString)&&!empty($secondString)){
            $thirdQuery = 'AND (A.withdrawalAmount ';
        }

        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {

                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $value;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($secondArray)) {
            $count = count($secondArray);
            foreach ($secondArray as $index => $key) {
                $betVal='';
                switch ($key) {
                    case 'A':
                        $betVal='BETWEEN 1 AND 1000000';
                    break;
                    case 'B':
                        $betVal='BETWEEN 1000001 AND 5000000';
                    break;
                    case 'C':
                        $betVal='BETWEEN 5000001 AND 10000000';
                    break;
                    case 'D':
                        $betVal='>= 10000001';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $secondQuery .= $betVal;
                if ($index < $count - 1) {
                    $secondQuery .= ') OR (A.depositAmount ';
                }
            }
            $secondQuery .= ')';
            if($key==''){
                $secondQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($thirdArray)) {
            $count = count($thirdArray);
            foreach ($thirdArray as $index => $key) {
                $betVal='';
                switch ($key) {
                    case 'Z':
                        $betVal='BETWEEN 1 AND 1000000';
                    break;
                    case 'Y':
                        $betVal='BETWEEN 1000001 AND 5000000';
                    break;
                    case 'X':
                        $betVal='BETWEEN 5000001 AND 10000000';
                    break;
                    case 'W':
                        $betVal='>= 10000001';
                    break;
                }
                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $thirdQuery .= $betVal;
                if ($index < $count - 1) {
                    $thirdQuery .= ') OR (A.withdrawalAmount ';
                }
            }
            $thirdQuery .= ')';
            if($key==''){
                $thirdQuery = ''; // 빈 문자열로 설정
            }
        }

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->prepare("SELECT
            IFNULL(SUM(CASE WHEN A.statusIDX IN (101101) THEN 1 ELSE 0 END), 0) AS beforeCount,
            IFNULL(SUM(CASE WHEN A.statusIDX IN (101102) THEN 1 ELSE 0 END), 0) AS afterCount,
            IFNULL(SUM(A.depositAmount), 0) AS depositAmount,
            IFNULL(SUM(A.withdrawalAmount), 0) AS withdrawalAmount
        FROM $dbName.EbuyBankLog AS A
        JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
        JOIN $dbName.Bank AS C ON B.bankIDX = C.idx
        ".$firstQuery."".$secondQuery."".$thirdQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 101101 THEN '매칭전'
                WHEN 101102 THEN '매칭후'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.withdrawalAmount,
        A.depositAmount,
        A.afterAmount,
        A.clientBank,
        A.clientAccountHolder,
        A.ex,
        A.tradeTime,
        CASE
            WHEN A.depositAmount BETWEEN 1 AND 1000000 THEN 'A'
            WHEN A.depositAmount BETWEEN 1000001 AND 5000000 THEN 'B'
            WHEN A.depositAmount BETWEEN 5000001 AND 10000000 THEN 'C'
            WHEN A.depositAmount >= 10000001 THEN 'D'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS depositAmountGrade,
        CASE
            WHEN A.withdrawalAmount BETWEEN 1 AND 1000000 THEN 'Z'
            WHEN A.withdrawalAmount BETWEEN 1000001 AND 5000000 THEN 'Y'
            WHEN A.withdrawalAmount BETWEEN 5000001 AND 10000000 THEN 'X'
            WHEN A.withdrawalAmount >= 10000001 THEN 'W'
            -- 추가 등급에 대한 조건을 계속해서 추가하세요.
            ELSE '해당없음'
        END AS withdrawalAmountGrade,
        C.name AS ebuyBankName
        FROM $dbName.EbuyBankLog AS A
        LEFT JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
        INNER JOIN $dbName.Bank AS C ON B.bankIDX = C.idx
        WHERE (A.tradeTime BETWEEN '$startDate' AND '$endDate')

        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageCon 디테일
    public static function GetMileageDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        A.idx,
        (SELECT
            CASE idx
                WHEN 101101 THEN '매칭전'
                WHEN 101102 THEN '매칭후'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.withdrawalAmount,
        A.depositAmount,
        A.afterAmount,
        A.clientBank,
        A.clientAccountHolder,
        A.ex,
        A.tradeTime,
        B.accountNumber AS ebuyAccountNumber,
        B.accountHolder AS ebuyAccountHolder,
        C.name AS ebuyBankName
        FROM $dbName.EbuyBankLog AS A
        JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX=B.idx
        JOIN $dbName.Bank AS C ON B.bankIDX=C.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 데이터테이블
    public static function GetScrapingListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        A.idx,
        CONCAT(A.clientAccountHolder, ' / ', A.clientBank, ' / ', FORMAT(A.depositAmount, 0) , ' / ', C.name) AS scraping,
        A.tradeTime,
        A.targetIDX
        FROM $dbName.EbuyBankLog AS A
        INNER JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
        INNER JOIN $dbName.Bank AS C ON B.bankIDX = C.idx
        WHERE (A.tradeTime BETWEEN '$startDate' AND '$endDate') AND A.depositAmount <> 0 AND A.statusIDX = 101102 /*이미 데이터가 처리 된것들만 가져오기 (아니면 중첩됨)*/

        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MileageDepositCon 해당 스크래핑값 가져오기
    public static function GetTargetScrapingData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        A.idx,
        CONCAT(A.clientAccountHolder, ' / ', A.clientBank, ' / ', FORMAT(A.depositAmount, 0), ' / ', C.name) AS scraping,
        A.tradeTime,
        A.targetIDX
        FROM $dbName.EbuyBankLog AS A
        INNER JOIN $dbName.EbuyBank AS B ON A.ebuyBankIDX = B.idx
        INNER JOIN $dbName.Bank AS C ON B.bankIDX = C.idx
        WHERE A.idx='$targetIDX'

        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}