<?php

namespace App\Models;

use PDO;

class AccountAuthLogMo extends \Core\Model
{
    //ClientAccountAuthCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->prepare("SELECT
        A.idx,
        CASE A.statusIDX
            WHEN 801101 THEN 'Step1_성공'
            WHEN 801201 THEN 'Step1_실패'
            WHEN 802101 THEN 'Step2_성공'
            WHEN 802201 THEN 'Step2_실패'
            ELSE '' END
        AS status,
        A.resultCode,
        A.msg,
        A.accountHolder,
        A.accountNumber,
        A.createTime,
        B.name AS bankName
        FROM $dbName.AccountAuthLog AS A
        INNER JOIN $dbName.Bank AS B ON A.bankCode = B.code
        ");
        // $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}