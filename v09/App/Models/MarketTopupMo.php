<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MarketTopupMo extends \Core\Model
{

    public static function GetMarketTopupData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.marketIDX,
        A.requestStatusIDX,
        A.statusIDX,
        A.methodIDX,
        A.createTime,
        CASE A.requestStatusIDX
            WHEN 418101 THEN 'Crypto'
            WHEN 418102 THEN 'Bankwire'
            WHEN 418103 THEN 'KRW'
            ELSE '' END
        AS method,
        A.fee,
        A.feePer,
        A.amount,
        '' AS nonFeeAmount,
        CASE
            WHEN A.statusIDX BETWEEN 418101 AND 418103 THEN '신청'
            WHEN A.statusIDX = 418301 THEN '확인'
            WHEN A.statusIDX = 418201 THEN '완료'
            WHEN A.statusIDX = 418211 THEN '취소-이바이'
            WHEN A.statusIDX = 418212 THEN '취소-마켓'
            -- 추가 솔팅에 대한 조건을 계속해서 추가하세요.
            ELSE 'Unknown'
        END AS status,
        IFNULL(A.completeTime, '-') AS completeTime,
        A.hash,
        A.note,
        CONCAT(B.name, '(', B.code ,')') AS marketInfo
        FROM $dbName.MarketTopup AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

     public static function GetDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.marketIDX,
        A.methodIDX,
        CASE
            WHEN A.statusIDX BETWEEN 418101 AND 418103 THEN '신청'
            WHEN A.statusIDX = 418301 THEN '확인'
            WHEN A.statusIDX = 418201 THEN '완료'
            WHEN A.statusIDX = 418211 THEN '취소-이바이'
            WHEN A.statusIDX = 418212 THEN '취소-마켓'
            ELSE '' END
        AS statusName,
        A.requestStatusIDX,
        A.statusIDX,
        A.createTime,
        CASE A.requestStatusIDX
            WHEN 418101 THEN 'Crypto'
            WHEN 418102 THEN 'Bankwire'
            WHEN 418103 THEN 'KRW'
            ELSE '' END
        AS method,
        A.feePer,
        FORMAT(A.amount, 0) AS amount,
        FORMAT(A.fee, 0) AS fee,
        FORMAT(A.amount - A.fee, 0) AS nonFeeAmount,
        IFNULL(A.completeTime, '-') AS completeTime,
        A.hash,
        A.note,
        CONCAT(B.name, '(', B.code ,')') AS marketInfo,
        B.name AS marketName,
        AES_DECRYPT(C.name, :dataDbKey) AS marketManager,
        C.position,
        AES_DECRYPT(D.email, :dataDbKey) AS managerEmail
        /*CONCAT(AES_DECRYPT(C.name,:dataDbKey), '(', C.position ,')') AS marketManager*/ /*CONCAT으로 하니까 복호화한 이름이 깨짐*/
        FROM $dbName.MarketTopup AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        INNER JOIN $dbName.MarketManager AS C ON A.marketManagerIDX=C.idx
        INNER JOIN $dbName.Manager AS D ON C.managerIDX=D.idx
        WHERE A.idx =:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }




}
