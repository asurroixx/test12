<?php

namespace App\Models;

use PDO;

class FaqMo extends \Core\Model
{

    //FaqCon 데이터테이블
    public static function DataTableListLoad($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.title,
        A.content,
        (SELECT
            CASE idx
                WHEN 343401 THEN '활성'
                WHEN 343501 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime,
        AES_DECRYPT(B.email,:dataDbKey) AS staffEmail
        FROM $dbName.Faq AS A
        LEFT JOIN $dbName.Staff AS B ON A.staffIDX=B.idx
        WHERE A.statusIDX IN (343401,343501)
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //FaqCon 디테일
    public static function GetFaqDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.faqCategoryIDX,
        A.title,
        A.content,
        (SELECT
            CASE idx
                WHEN 343401 THEN '활성'
                WHEN 343501 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime,
        B.name AS CateName
        FROM $dbName.Faq AS A
        JOIN $dbName.FaqCategory AS B ON A.faqCategoryIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //FaqCon 해당 질문 조사
    public static function GetTargetIssetData($data=null)
    {
        $targetIDX=$data;
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        A.faqCategoryIDX,
        A.title,
        A.statusIDX,
        A.createTime,
        B.name AS categoryName
        FROM $dbName.Faq AS A
        JOIN $dbName.FaqCategory AS B ON A.faqCategoryIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }




}