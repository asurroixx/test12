<?php

namespace App\Models;

use PDO;

class JobMo extends \Core\Model
{

    //환율정보 불러오기
    public static function ExchangeRate($data=null)
    {
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->query("SELECT
        standKRW
        FROM $dbName.ExchangeRate
        ORDER BY idx DESC LIMIT 1
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}