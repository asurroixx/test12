<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class EbuyOperatingTimeMo extends \Core\Model
{
	//CalendarCon 데이터테이블
    public static function getOperateTime($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        SUBSTRING_INDEX(openTime, ':', 2) AS openTime,
        SUBSTRING_INDEX(closeTime, ':', 2) AS closeTime
        FROM $dbName.EbuyOperatingTime
        WHERE idx=1
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



    
}