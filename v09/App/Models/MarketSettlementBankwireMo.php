<?php

namespace App\Models;

use PDO;

class MarketSettlementBankwireMo extends \Core\Model
{

    //MarketSettlementCon 해당 마켓 bankwire 정보
    public static function GetSettlementBankwireData($data=null)
    {
        $marketIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        receivingCountry,
        bankName,
        bankAddress,
        swiftCode,
        accountNumber,
        beneficiaryName,
        beneficiaryAddress,
        createTime
        FROM $dbName.MarketSettlementBankwire
        WHERE marketIDX='$marketIDX' AND statusIDX=426101
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } 

     //MarketSettlementRequestCon 정산신청시에 해당정보
    public static function GetMarketSettlementInfo($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        receivingCountry,
        bankName,
        bankAddress,
        swiftCode,
        accountNumber,
        beneficiaryName,
        beneficiaryAddress,
        createTime
        FROM $dbName.MarketSettlementBankwire
        WHERE A.idx='$idx'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



}