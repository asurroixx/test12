<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class EbuyCryptoWalletMo extends \Core\Model
{

    public static function getEbuyTopupCrytoWalletForEmail($data=null)
    {
        $idx=$data;
        $MainDBName=self::MainDBName;
        $db = static::GetDB();
        $idx=$data;
        $query = $db->prepare("SELECT
            A.idx,
            A.walletAddr,
            B.code AS coinCode,
            C.code AS mainnetCode
            FROM $MainDBName.EbuyCryptoWallet AS A
            INNER JOIN $MainDBName.MainnetCoin AS B ON A.mainnetCoinIDX = B.idx
            INNER JOIN $MainDBName.Mainnet AS C ON B.mainnetIDX = C.idx
            WHERE A.idx = :idx
        ");
        $query->bindValue(':idx', $idx);

        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}
