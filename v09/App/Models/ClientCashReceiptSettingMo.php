<?php

namespace App\Models;

use PDO;

class ClientCashReceiptSettingMo extends \Core\Model
{




    // public static function GetDataTableListLoad($data=null)
    // {
    //     $db = static::GetDB();
    //     $dbName= self::MainDBName;
    //     $dataDbKey=self::dataDbKey;

    //     $query = $db->prepare("SELECT
    //         A.idx,
    //         A.clientIDX,
    //         A.statusIDX,
    //         A.phoneNumber AS cashNum,
    //         A.createTime AS createTime,
    //         CASE A.statusIDX
    //             WHEN  229101 THEN '신청'
    //             WHEN  229201 THEN '미신청'
    //             ELSE '-'
    //         END AS status,
    //         AES_DECRYPT(B.name, :dataDbKey) AS name,
    //         AES_DECRYPT(B.phone, :dataDbKey) AS phone,
    //         AES_DECRYPT(B.birth, :dataDbKey) AS birth,
    //         IF(B.migrationIDX = 0, '신규', CONCAT('기존(', B.migrationIDX, ')')) AS migrationIDX
    //     FROM $dbName.ClientCashReceiptSetting AS A
    //     JOIN $dbName.Client AS B ON B.idx = A.clientIDX
    //     WHERE (A.clientIDX, A.createTime) IN (
    //             SELECT
    //             clientIDX,
    //             MAX(createTime) AS maxCreateTime
    //         FROM $dbName.ClientCashReceiptSetting
    //         GROUP BY clientIDX)
    //     ");
    //     $query->bindValue(':dataDbKey', $dataDbKey);
    //     $query->execute();
    //     $result=$query->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }


    public static function GetDataTableListLoad($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            A.idx,
            A.clientIDX,
            CASE A.statusIDX
                WHEN  229101 THEN '신청'
                WHEN  229201 THEN '미신청'
                ELSE '-'
            END AS status,
            IFNULL(AES_DECRYPT(A.phoneNumber, :dataDbKey), '-') AS cashNum,
            A.createTime,
            AES_DECRYPT(B.name, :dataDbKey) AS name,
            AES_DECRYPT(B.phone, :dataDbKey) AS phone,
            AES_DECRYPT(B.birth, :dataDbKey) AS birth,
            IF(B.migrationIDX = 0, '신규', CONCAT('기존(', B.migrationIDX, ')')) AS migrationIDX

        FROM ebuy.ClientCashReceiptSetting AS A
        -- JOIN $dbName.Client AS B ON B.idx = A.clientIDX AND B.statusIDX=226101
        JOIN $dbName.Client AS B ON B.idx = A.clientIDX
        WHERE (A.clientIDX, A.createTime) IN (
                SELECT
                clientIDX,
                MAX(createTime) AS maxCreateTime
            FROM $dbName.ClientCashReceiptSetting
            GROUP BY clientIDX)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }







}