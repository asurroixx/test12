<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    //LoginCon 이메일 유효성 검사
    public static function issetEmail($data=null)
    {
        $MainDBName=self::MainDBName;
        $BasicTable=self::BasicTable['Staff'];
        $dataDbKey=self::dataDbKey;
        $email=$data;
        $email=trim($email);
        $db = static::GetDB();

        $query = $db->prepare("SELECT
        idx,
        AES_DECRYPT(email, :dataDbKey) AS email
        FROM $MainDBName.$BasicTable
        WHERE AES_DECRYPT(email, :dataDbKey) = :email
        ");

        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':email', $email);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //LoginCon 로그인 코드 검사
    public static function GetLoginCodeChk($data=null)
    {
        $MainDBName=self::MainDBName;
        $BasicTableAdmin=self::BasicTable['Staff'];
        $BasicTableLoginDump=self::BasicTable['StaffLoginDump'];
        $dataDbKey=self::dataDbKey;

        $email=$data['email'];
        $code=$data['code'];
        $ipAddress=$data['ipAddress'];

        $db = static::GetDB();
        $GetDump = $db->prepare("SELECT
        A.code,
        B.idx,
        AES_DECRYPT(B.email, :dataDbKey) AS email,
        B.statusIDX
        FROM $MainDBName.$BasicTableLoginDump AS A
        JOIN $MainDBName.$BasicTableAdmin AS B ON A.staffIDX = B.idx
        WHERE AES_DECRYPT(B.email, :dataDbKey)=:email AND A.code=:code AND A.ipAddress=:ipAddress
        ORDER BY A.idx DESC LIMIT 1
        ");
        $GetDump->bindValue(':dataDbKey', $dataDbKey);
        $GetDump->bindValue(':email', $email);
        $GetDump->bindValue(':code', $code);
        $GetDump->bindValue(':ipAddress', $ipAddress);
        $GetDump->execute();
        $row = $GetDump->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //LoginCon 글로벌변수 생성시
    public static function GetGlobalVal($data=null)
    {
        $MainDBName=self::MainDBName;
        $BasicTable=self::BasicTable['Staff'];
        $dataDbKey=self::dataDbKey;

        $idx=$data;
        $db = static::getDB();
        $globalVal = $db->prepare("SELECT
        idx,
        AES_DECRYPT(email, :dataDbKey) AS email,
        AES_DECRYPT(name, :dataDbKey) AS name,
        gradeIDX
        FROM $MainDBName.$BasicTable
        WHERE idx=:idx
        ");
        $globalVal->bindValue(':dataDbKey', $dataDbKey);
        $globalVal->bindValue(':idx', $idx);
        $globalVal->execute();
        $result=$globalVal->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //StaffCon 데이터테이블
    public static function GetStaffList($data=null)
    {
        // $startDate=$data['startDate'];
        // $endDate=$data['endDate'];
        // if($startDate==""){
        //     $startDate='1970-01-01 00:00:00';
        // }else{
        //     $startDate.=" 00:00:00";
        // }
        // if($endDate==""){
        //     $endDate=date('Y-m-d 23:59:59');
        // }else{
        //     $endDate.=" 23:59:59";
        // }
        $dataDbKey=self::dataDbKey;


        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.email, :dataDbKey) AS email,
        AES_DECRYPT(A.name, :dataDbKey) AS name,
        B.memo AS statusMemo,
        C.name AS gradeName,
        (SELECT createTime FROM ebuy.StaffLog WHERE A.idx=staffIDX AND statusIDX=304101 GROUP BY createTime DESC LIMIT 1) AS createTime
        FROM ebuy.Staff AS A
        JOIN ebuy.Status AS B ON A.statusIDX=B.idx
        JOIN ebuy.StaffGrade AS C ON A.gradeIDX=C.idx
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StaffCon 디테일
    public static function GetStaffDetail($data=null)
    {
        $dataDbKey=self::dataDbKey;

        $idx=$data;
        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.email, :dataDbKey) AS email,
        AES_DECRYPT(A.name, :dataDbKey) AS name,
        A.statusIDX,
        A.gradeIDX,
        A.appActiveStat,
        A.appSettlementStat,
        B.memo AS statusMemo,
        C.name AS gradeName,
        (SELECT DATE_ADD(createTime, INTERVAL 9 HOUR) FROM ebuy.StaffLog WHERE A.idx=staffIDX AND statusIDX=304101 GROUP BY createTime DESC LIMIT 1) AS createTime
        FROM ebuy.Staff AS A
        JOIN ebuy.Status AS B ON A.statusIDX=B.idx
        JOIN ebuy.StaffGrade AS C ON A.gradeIDX=C.idx
        WHERE A.idx='$idx'
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':idx', $idx);
        $Sel->execute();
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StaffCon 모든정보 유효성 idx기준
    public static function issetStaffData($data=null)
    {
        $dataDbKey=self::dataDbKey;

        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        A.idx,
        A.loginToken,
        AES_DECRYPT(A.email, :dataDbKey) AS email,
        AES_DECRYPT(A.name, :dataDbKey) AS name,
        A.gradeIDX,
        A.statusIDX,
        A.appActiveStat,
        A.appSettlementStat,
        B.name AS gradeName,
        C.memo AS statusVal
        FROM ebuy.Staff AS A
        JOIN ebuy.StaffGrade AS B ON A.gradeIDX=B.idx
        JOIN ebuy.Status AS C ON A.statusIDX=C.idx
        WHERE A.idx=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StaffCon 이메일 유효성
    public static function issetStaffEmail($data=null)
    {
        $dataDbKey=self::dataDbKey;
        $email=$data;
        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        AES_DECRYPT(email, :dataDbKey) AS email
        FROM ebuy.Staff
        WHERE email=:email
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':email', $email);
        $Sel->execute();
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StaffGradeCon 등급별 스태프 목록
    public static function GetStaffGradeListData($data=null)
    {
        $dataDbKey=self::dataDbKey;
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        idx,
        AES_DECRYPT(email, :dataDbKey) AS email,
        AES_DECRYPT(name, :dataDbKey) AS name,
        statusIDX,
        gradeIDX
        FROM ebuy.Staff
        WHERE gradeIDX=:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //MinJob 앱 알람을 허용한 스태프
    public static function GetAppStaffAlarmList($data=null)
    {
        $dataDbKey=self::dataDbKey;
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->prepare("SELECT
        idx,
        /*AES_DECRYPT(email, :dataDbKey) AS email,
        AES_DECRYPT(name, :dataDbKey) AS name,*/
        statusIDX,
        gradeIDX,
        clientIDX
        FROM ebuy.Staff
        WHERE clientIDX > 0 AND appActiveStat = 'Y'
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }


}
