<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	
	public static function GetClientAllListData($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		AES_DECRYPT(name,:dataDbKey) AS clientName,
		AES_DECRYPT(birth,:dataDbKey) AS birth,
		AES_DECRYPT(phone,:dataDbKey) AS phone,
		AES_DECRYPT(email,:dataDbKey) AS email,
		gradeIDX
		FROM $dbName.Client
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientGradeCon 데이터테이블
	public static function getClientList($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$statusIDX = $data;
		$query = $db->query("SELECT
		idx
		FROM $dbName.Client
		");
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MileageDepositCon 솔팅별 데이터 리셋
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data;
        $firstString = $columnsVal[0];
        $secondString = $columnsVal[1];

        $firstArray = explode('|', $firstString);
        $secondArray = explode('|', $secondString);

        $firstQuery = 'WHERE B.name IN (';
        $secondQuery = 'WHERE C.deviceType IN (';

        if(isset($firstString)&&!empty($firstString)){
            $secondQuery = 'AND C.deviceType IN (';
        }

        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {

                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $value;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        if (!empty($secondArray)) {
            $count = count($secondArray);
            foreach ($secondArray as $index => $key) {
                $deviceType='';
                switch ($key) {
                    case '안드로이드':
                        $deviceType='A';
                    break;
                    case '아이폰':
                        $deviceType='I';
                    break;
                }
                $value = "'" . $deviceType . "'"; // 작은 따옴표 추가
                $secondQuery .= $value;
                if ($index < $count - 1) {
                    $secondQuery .= ',';
                }
            }
            $secondQuery .= ')';
            if($key==''){
                $secondQuery = ''; // 빈 문자열로 설정
            }
        }

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
            COUNT(A.idx) AS count
        FROM
        $dbName.Client AS A
        JOIN $dbName.ClientGrade AS B ON A.gradeIDX = B.idx
        JOIN $dbName.ClientDevice AS C ON A.deviceIDX = C.idx
        ".$firstQuery."".$secondQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

	//ClientCon 데이터테이블
	public static function GetDataTableListLoad($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.birth,:dataDbKey) AS birth,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		(SELECT
            CASE idx
                WHEN 226101 THEN '정상'
                WHEN 226201 THEN '탈퇴'
                WHEN 226202 THEN '스태프차단'
                WHEN 226203 THEN '앱미가입'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
		A.createTime,
		B.name AS gradeName,
		C.deviceNumber,
		CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType
		FROM $dbName.Client AS A
		JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
		JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientCon 디테일 (오른쪽)
	public static function GetClientDetailData($data=null)
	{
		$targetIDX=$data;
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		A.ci,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.birth,:dataDbKey) AS birth,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		A.gradeIDX,
		(SELECT
            CASE idx
                WHEN 226101 THEN '정상'
                WHEN 226201 THEN '탈퇴'
                WHEN 226202 THEN '스태프차단'
                WHEN 226203 THEN '앱미가입'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
		A.createTime,
		B.name AS gradeName,
		B.defaultFeeKRW,
		B.tradeFeePer,
		B.tranFeePer,
		B.withFeePer,
		B.withFeeKRW,
		B.withMinKRW,
		C.deviceNumber,
		CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType
		FROM $dbName.Client AS A
		LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
		LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
		LEFT JOIN $dbName.ClientBank AS D ON A.idx=D.clientIDX
		WHERE A.idx=:targetIDX
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientCon 해당 클라이언트 idx statusIDX 조사
    public static function IssetClientData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.gradeIDX,
        B.name AS gradeName
        FROM $dbName.Client AS A
        JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


	public static function getClientForPushAlarm($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;

		$queryTxt='';
		$clientIDX = $data['clientIDX'];
		$statusIDX = $data['statusIDX'];

		if($clientIDX=='all'){
		}else{
			$queryTxt='WHERE A.idx='.$clientIDX;
		}
		$query = $db->query("SELECT
			A.idx,
			B.deviceType,
			B.pushToken,
			C.statusIDX
		FROM $dbName.Client AS A
		INNER JOIN $dbName.ClientDevice AS B ON A.deviceIDX = B.idx
		LEFT JOIN $dbName.ClientAlarmSetting AS C ON A.idx = C.clientIDX AND C.statusIDX = '$statusIDX'
		".$queryTxt);
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//ClientCon 해당 클라이언트 idx 등록은행
	public static function GetTargetClientData($data=null)
	{

		$clientIDX = $data;

        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		A.idx,
		AES_DECRYPT(A.name,:dataDbKey) AS name,
		AES_DECRYPT(A.phone,:dataDbKey) AS phone,
		AES_DECRYPT(A.email,:dataDbKey) AS email,
		B.createTime,
		AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
		AES_DECRYPT(B.accountHolder,:dataDbKey) AS accountHolder,
		C.name AS bankName,
		C.code AS bankCode
		FROM $dbName.Client AS A
		JOIN $dbName.ClientBank AS B ON A.idx = B.clientIDX
		JOIN $dbName.Bank AS C ON B.BankIDX = C.idx
		WHERE A.idx = :clientIDX
		ORDER BY B.createTime DESC
		LIMIT 1;
		");
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}



    
}