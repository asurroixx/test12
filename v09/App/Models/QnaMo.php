<?php

namespace App\Models;

use PDO;

class QnaMo extends \Core\Model
{

    //QnaCon 날짜별 데이터 리셋
    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM $dbName.Qna
        WHERE createTime BETWEEN '$startDate' AND '$endDate';
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //QnaCon 솔팅별 데이터 리셋
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data;
        $firstString = $columnsVal[0];
        // $secondString = $columnsVal[1];

        $firstArray = explode('|', $firstString);
        // $secondArray = explode('|', $secondString);

        $firstQuery = 'WHERE C.name IN (';
        // $secondQuery = 'WHERE C.deviceType IN (';

        // if(isset($firstString)&&!empty($firstString)){
            // $secondQuery = 'AND C.deviceType IN (';
        // }

        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {

                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $value;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        // if (!empty($secondArray)) {
        //     $count = count($secondArray);
        //     foreach ($secondArray as $index => $key) {
        //         $deviceType='';
        //         switch ($key) {
        //             case '안드로이드':
        //                 $deviceType='A';
        //             break;
        //             case '아이폰':
        //                 $deviceType='I';
        //             break;
        //         }
        //         $value = "'" . $deviceType . "'"; // 작은 따옴표 추가
        //         $secondQuery .= $value;
        //         if ($index < $count - 1) {
        //             $secondQuery .= ',';
        //         }
        //     }
        //     $secondQuery .= ')';
        //     if($key==''){
        //         $secondQuery = ''; // 빈 문자열로 설정
        //     }
        // }

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
        COUNT(A.idx) AS count
        FROM $dbName.Qna AS A
        JOIN $dbName.MarketManager AS B ON A.marketManagerIDX=B.idx
        JOIN $dbName.Market AS C ON B.marketIDX=C.idx
        ".$firstQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //QnaCon 데이터테이블
    public static function DataTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.ticketCode,
        A.title,
        (SELECT
            CASE idx
                WHEN 440101 THEN '접수'
                WHEN 440102 THEN '재문의'
                WHEN 440201 THEN '취소'
                WHEN 440301 THEN '종료'
                WHEN 340101 THEN '답변완료'
                WHEN 340201 THEN '스태프문의'
                WHEN 140301 THEN '자동종료'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime,
        CASE
            WHEN A.statusIDX = 440101 OR A.statusIDX = 440102 THEN
                A.expectedTime
            ELSE
                '-'
            END
        AS expectedTime,
        (SELECT createTime FROM $dbName.QnaAnswer WHERE A.idx=qnaIDX ORDER BY createTime DESC LIMIT 1) AS updateTime,
        C.name AS marketName,
        D.name AS categoryName,
        (SELECT
            CASE
                WHEN D.momIDX != 0 THEN
                    (SELECT name FROM $dbName.QnaCategory WHERE idx = D.momIDX)
                ELSE
                    ''
            END
        ) AS momName
        FROM $dbName.Qna AS A
        LEFT JOIN $dbName.MarketManager AS B ON A.marketManagerIDX=B.idx
        JOIN $dbName.Market AS C ON B.marketIDX=C.idx
        JOIN $dbName.QnaCategory AS D ON A.qnaCategoryIDX=D.idx
        WHERE (A.createTime BETWEEN :startDate AND :endDate) OR A.title = 'Sandbox QnA'
        ");
        $Sel->bindValue(':startDate', $startDate);
        $Sel->bindValue(':endDate', $endDate);
        $Sel->execute();
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    //QnaCon 해당 질문 정보
    public static function GetDetailData($data=null)
    {
        $targetIDX=$data;
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        A.qnaCategoryIDX,
        A.marketManagerIDX,
        A.ticketCode,
        A.title,
        (SELECT
            CASE idx
                WHEN 440101 THEN '접수'
                WHEN 440102 THEN '재문의'
                WHEN 440201 THEN '취소'
                WHEN 440301 THEN '종료'
                WHEN 340101 THEN '답변완료'
                WHEN 340201 THEN '스태프문의'
                WHEN 140301 THEN '자동종료'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime,
        A.expectedTime,
        AES_DECRYPT(B.name, :dataDbKey) AS managerName,
        AES_DECRYPT(C.email, :dataDbKey) AS email,
        D.createTime AS lastTime,
        E.name AS categoryName,
        (SELECT
            CASE
                WHEN E.momIDX != 0 THEN
                    (SELECT name FROM $dbName.QnaCategory WHERE idx = E.momIDX)
                ELSE
                    ''
            END
        ) AS momName,
        F.name AS marketName
        FROM $dbName.Qna AS A
        JOIN $dbName.MarketManager AS B ON A.marketManagerIDX=B.idx
        JOIN $dbName.Manager AS C ON B.managerIDX=C.idx
        JOIN $dbName.QnaAnswer AS D ON A.idx=D.qnaIDX
        JOIN $dbName.QnaCategory AS E ON A.qnaCategoryIDX=E.idx
        JOIN $dbName.Market AS F ON B.marketIDX=F.idx
        WHERE A.idx=:targetIDX ORDER BY D.createTime DESC LIMIT 1
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //QnaCon 해당 질문 조사
    public static function GetTargetIssetData($data=null)
    {
        $targetIDX=$data;
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        A.qnaCategoryIDX,
        A.marketManagerIDX,
        A.ticketCode,
        A.title,
        A.statusIDX,
        A.createTime,
        B.marketIDX,
        AES_DECRYPT(C.email, :dataDbKey) AS email,
        D.name AS categoryName,
        E.name AS marketName
        FROM $dbName.Qna AS A
        JOIN $dbName.MarketManager AS B ON A.marketManagerIDX=B.idx
        JOIN $dbName.Manager AS C ON B.managerIDX=C.idx
        JOIN $dbName.QnaCategory AS D ON A.qnaCategoryIDX=D.idx
        JOIN $dbName.Market AS E ON B.marketIDX=E.idx
        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //QnaCon 해당 마켓매니저 이전 질문
    public static function GetBeforeData($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $marketManagerIDX=$data['marketManagerIDX'];
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx,
        A.ticketCode,
        A.title,
        (SELECT
            CASE idx
                WHEN 440101 THEN '접수'
                WHEN 440102 THEN '재문의'
                WHEN 440201 THEN '취소'
                WHEN 440301 THEN '종료'
                WHEN 340101 THEN '답변완료'
                WHEN 340201 THEN '스태프문의'
                WHEN 140301 THEN '자동종료'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime
        FROM $dbName.Qna AS A
        WHERE A.marketManagerIDX='$marketManagerIDX' AND A.idx <> '$targetIDX'
        ORDER BY A.createTime DESC
        ");
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
     //QnaCon 중복되는 티켓코드 있는지 조사
    public static function ticketCodeDuplicateChk($data=null)
    {
        $ticketCode=$data;
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        idx,
        ticketCode
        FROM $dbName.Qna
        WHERE ticketCode='$ticketCode'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}