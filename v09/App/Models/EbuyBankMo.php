<?php

namespace App\Models;

use PDO;

class EbuyBankMo extends \Core\Model
{
    //EbuyBankCon 데이터테이블 , Mileage 메뉴들 솔팅
    public static function GetEbuyBankData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.accountNumber,
        A.accountHolder,
        A.createTIme,
        B.code,
        B.name,
        (SELECT
            CASE idx
                WHEN 323301 THEN '정상'
                WHEN 323401 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status
        FROM $dbName.EbuyBank AS A
        JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //EbuyBankCon 디테일 로드
    public static function GetEbuyBankDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        A.accountNumber,
        A.accountHolder,
        A.createTIme,
        B.code,
        B.name,
        (SELECT
            CASE idx
                WHEN 323301 THEN '정상'
                WHEN 323401 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status
        FROM $dbName.EbuyBank AS A
        JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientGradeCon 등급 추가할때 활성화 된 은행만 가져오기
    public static function GetEbuyBankStatusData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        B.name,
        (SELECT idx FROM $dbName.Status WHERE A.statusIDX=idx) AS status
        FROM $dbName.EbuyBank AS A
        JOIN $dbName.Bank AS B ON A.bankIDX=B.idx
        WHERE A.statusIDX=323301
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //EbuyBankCon 모든정보 유효성 idx기준
    public static function issetEbuyBankData($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        bankIDX,
        accountNumber,
        accountHolder,
        statusIDX
        FROM $dbName.EbuyBank
        WHERE idx='$targetIDX'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }




}