<?php

namespace App\Models;

use PDO;

class PeerToPeerTypeMo extends \Core\Model
{
    public static function GetPeerToPeerTypeData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        -- (SELECT
        --     CASE idx
        --         WHEN 331301 THEN '정상'
        --         WHEN 331401 THEN '비활성'
        --         ELSE '' END
        --     AS status
        -- FROM $dbName.Status WHERE statusIDX=idx) AS status,
        statusIDX,
        CASE statusIDX
            WHEN 331301 THEN '정상'
            WHEN 331401 THEN '비활성'
            ELSE '' END
        AS status,
        name,
        price
        FROM $dbName.PeerToPeerType
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

     //PtpTypeCon 타겟조사
    public static function PeerToPeerTypeStatusData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        statusIDX,
        name,
        price
        FROM $dbName.PeerToPeerType
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}



