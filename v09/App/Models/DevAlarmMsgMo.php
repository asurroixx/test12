<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class DevAlarmMsgMo extends \Core\Model
{
	//ClientGradeCon 데이터테이블
	public static function getDevAlarmMsg($data=null)
	{
        $dbName= self::EbuySandboxDBName;
        $db = static::GetSandboxDB();
		$statusIDX = $data;
		$query = $db->query("SELECT
			idx,
			title,
			con,
			param
		FROM $dbName.PortalAlarmMsg
		WHERE statusIDX = '$statusIDX'
		");
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

}