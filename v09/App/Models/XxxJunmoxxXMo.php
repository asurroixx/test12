<?php

namespace App\Models;

use PDO;

class XxxJunmoxxXMo extends \Core\Model
{


	//내가 꼭 정리할께.. jm

	//이거 뭥미
	public static function GetClientInfoByCiForCashReceiptJob($data=null)
	{
		$ci=$data;
		$db = static::GetDB();
		$dbName= self::MainDBName;
		$dataDbKey=self::dataDbKey;

		$query = $db->prepare("SELECT
			A.idx
			,AES_DECRYPT(A.name,:dataDbKey) AS clientName
			,AES_DECRYPT(A.phone,:dataDbKey) AS clientTel
			,AES_DECRYPT(A.email,:dataDbKey) AS clientEmail

			,AES_DECRYPT(B.phoneNumber,:dataDbKey) AS ReceiptTypeNo
			,B.idx AS ClientCashReceiptSettingIDX
			,B.statusIDX AS ClientCashReceiptSettingStatusIDX
			,B.createTime AS ClientCashReceiptSettingCreateTime
		FROM $dbName.Client AS A
		INNER JOIN $dbName.ClientCashReceiptSetting AS B ON B.clientIDX = A.idx
		WHERE A.ci=:ci
		ORDER BY B.createTime DESC LIMIT 1
		");
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->bindValue(':ci', $ci);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//이거 뭥미
	public static function GetClientInfoByClientIDXForCashReceiptJob($data=null)
	{
		$clientIDX=$data;
		$db = static::GetDB();
		$dbName= self::MainDBName;
		$dataDbKey=self::dataDbKey;

		$query = $db->prepare("SELECT
			A.idx
			,AES_DECRYPT(A.name,:dataDbKey) AS clientName
			,AES_DECRYPT(A.phone,:dataDbKey) AS clientTel
			,AES_DECRYPT(A.email,:dataDbKey) AS clientEmail

			,AES_DECRYPT(B.phoneNumber,:dataDbKey) AS ReceiptTypeNo
			,B.idx AS ClientCashReceiptSettingIDX
			,B.statusIDX AS ClientCashReceiptSettingStatusIDX
			,B.createTime AS ClientCashReceiptSettingCreateTime
		FROM $dbName.Client AS A
		INNER JOIN $dbName.ClientCashReceiptSetting AS B ON B.clientIDX = A.idx
		WHERE A.idx=:clientIDX
		ORDER BY B.createTime DESC LIMIT 1
		");
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->bindValue(':clientIDX', $clientIDX);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//민잡에서 찐으로 쓰고 있음
	public static function GetClientMileageIsApiDepositForCashReceiptJob($data=null)
	{
		// $startDate=$data['startDate'];
		// $endDate=$data['endDate'];
		// if($startDate==""){
		//     $startDate=date('Y-m-d H:i:s', strtotime('-3 minutes'));
		// }
		// if($endDate==""){
		//     $endDate=date('Y-m-d H:i:s');
		// }
		// if(strtotime($startDate)>strtotime($endDate)){
		//     $endDate = date('Y-m-d H:i:s', strtotime('+3 minutes', strtotime($startDate)));
		// }
		// $db = static::GetDB();
		// $dbName= self::MainDBName;

		// $query = $db->prepare("SELECT
		//     A.idx
		//     ,A.clientIDX
		//     ,A.targetIDX

		// FROM $dbName.ClientMileage AS A
		// WHERE A.createTime BETWEEN :startDate AND :endDate
		// AND A.statusIDX=:SucStatusIDX
		// AND A.clientIDX NOT IN (116267,116253,116141,116020,116018,99408,99406,99404,99392,99391,99390,99389,4,116247,116243,113814,116230,116168,2,113825,116201,8,116291,116286,116285,116169,116145,116022,116019,99407,99405,99403,1,113824,115808,3,101017,116207,110691,101636,114999,11,7,100690,9,113823,6,16534,14696,102947,99564,99421,99455)
		// ");
		// $query->bindValue(':startDate', $startDate);
		// $query->bindValue(':endDate', $endDate);
		// $query->bindValue(':SucStatusIDX',906101);
		// $query->execute();
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		// return $result;

		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		if($startDate==""){
			$startDate=date('Y-m-d H:i:s', strtotime('-3 minutes'));
		}
		if($endDate==""){
			$endDate=date('Y-m-d H:i:s');
		}
		if(strtotime($startDate)>strtotime($endDate)){
			$endDate = date('Y-m-d H:i:s', strtotime('+3 minutes', strtotime($startDate)));
		}
		$db = static::GetDB();
		$dbName= self::MainDBName;
		$dataDbKey=self::dataDbKey;

		$query = $db->prepare("SELECT
			A.idx
			,A.clientIDX
			,A.targetIDX

			,A.statusIDX

			,AES_DECRYPT(B.name,:dataDbKey) AS clientName
			,AES_DECRYPT(B.phone,:dataDbKey) AS clientTel
			,AES_DECRYPT(B.email,:dataDbKey) AS clientEmail

			,IFNULL(AES_DECRYPT(C.phoneNumber,:dataDbKey), 0) AS ReceiptTypeNo
			,IFNULL(C.idx, 0) AS ClientCashReceiptSettingIDX
			,IFNULL(C.statusIDX, 0) AS ClientCashReceiptSettingStatusIDX
			,IFNULL(C.createTime, 0) AS ClientCashReceiptSettingCreateTime
		FROM $dbName.ClientMileage AS A
		INNER JOIN $dbName.Client AS B ON B.idx = A.clientIDX
		LEFT JOIN (
			SELECT
				clientIDX,
				MAX(createTime) AS maxCreateTime
			FROM $dbName.ClientCashReceiptSetting
			GROUP BY clientIDX
		) AS maxCC ON maxCC.clientIDX = A.clientIDX
		LEFT JOIN $dbName.ClientCashReceiptSetting AS C
			ON C.clientIDX = maxCC.clientIDX
			AND C.createTime = maxCC.maxCreateTime
		WHERE A.createTime BETWEEN :startDate AND :endDate
		AND (A.statusIDX=:SucStatusIDX OR A.statusIDX=:PtpSucStatusIDX)
		AND A.clientIDX NOT IN (116207, 110691, 101636, 3, 101017, 116267, 116452, 116453, 116454, 116455, 116456, 116253, 116141, 116020, 116018, 99408, 99406, 99404, 99392, 99391, 99390, 99389, 4, 116247, 116243, 113814, 116230, 116168, 2, 113825, 8, 116291, 116286, 116285, 116169, 116145, 116022, 116019, 99407, 99405, 99403, 1, 113824, 9, 113823, 6, 16534, 14696, 11, 114999, 7, 99564, 102947, 99421, 100690)
		");
		//20240216 현영테스트때문에 박제이 제외 116201
		//20240223 115808 준모 제외
		//20240223 251401 피투피완료도 추가함~.~ 여기서 잡히는 애는 셀러임 바이어는 민잡에서 따로 구할거임
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->bindValue(':SucStatusIDX',906101);
		$query->bindValue(':PtpSucStatusIDX',251401);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}




















































	// 240201 통계
	/*
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	*/
	//파트너
	public static function statisticsPartner($data=null)
	{
		$endDate=$data;
		// $marketCode = $data;
		$dbName= self::MainDBName;
		$db = static::GetDB();
		$query = $db->prepare("SELECT
			COALESCE((
				SUM(
					CASE
						WHEN A.statusIDX IN (418201,417211,419101,906101,420101,421101) THEN A.amount
						ELSE 0
					END
				) - SUM(
					CASE
						WHEN A.statusIDX IN (417101, 417102, 417103,419201,908102,420201) THEN A.amount
						ELSE 0
					END
				)
			), 0) AS balance,
			B.name
			FROM $dbName.MarketBalance AS A
			INNER JOIN $dbName.Market AS B ON A.marketIDX = B.idx
			WHERE A.createTime <= :endDate AND B.idx NOT IN (1,53,54,55,56,58,60,61)
			GROUP BY A.marketIDX
			ORDER BY B.name ASC;
		");
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//회원
	public static function statisticsClient($data=null)
	{
		$endDate=$data;
		// $marketCode = $data;
		$dbName= self::MainDBName;
		$db = static::GetDB();
		$query = $db->prepare("SELECT
			COUNT(DISTINCT A.clientIDX) AS total,
			COALESCE((
				SUM(
					CASE
						WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN A.amount
						ELSE 0
					END
				) - SUM(
					CASE
						WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN A.amount
						ELSE 0
					END
				)
			), 0) AS mileage
			FROM $dbName.ClientMileage AS A
			WHERE A.createTime <= :endDate
			AND A.clientIDX NOT IN (116207, 110691, 101636, 115808, 3, 101017, 116267, 116452, 116453, 116454, 116455, 116456, 116253, 116141, 116020, 116018, 99408, 99406, 99404, 99392, 99391, 99390, 99389, 4, 116247, 116243, 113814, 116230, 116168, 2, 113825, 116201, 8, 116291, 116286, 116285, 116169, 116145, 116022, 116019, 99407, 99405, 99403, 1, 113824, 9, 113823, 6, 16534, 14696, 11, 114999, 7, 99564, 102947, 99421, 100690)
		");
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//입금신청
	public static function statisticsMileageDeposit($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$dbName= self::MainDBName;
		$db = static::GetDB();
		$query = $db->prepare("SELECT
			COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX IN (203201,203202) THEN idx END) AS totalCount,
			IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX IN (203201,203202) THEN amount ELSE 0 END), 0) AS totalAmount,
			COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 203201 THEN idx END) AS manualCount,
			IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 203201 THEN amount ELSE 0 END), 0) AS manualAmount,
			COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 203202 THEN idx END) AS autoCount,
			IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 203202 THEN amount ELSE 0 END), 0) AS autoAmount
			FROM $dbName.ClientMileageDeposit
			WHERE createTime BETWEEN :startDate AND :endDate
			AND clientIDX NOT IN (116207, 110691, 101636, 115808, 3, 101017, 116267, 116452, 116453, 116454, 116455, 116456, 116253, 116141, 116020, 116018, 99408, 99406, 99404, 99392, 99391, 99390, 99389, 4, 116247, 116243, 113814, 116230, 116168, 2, 113825, 116201, 8, 116291, 116286, 116285, 116169, 116145, 116022, 116019, 99407, 99405, 99403, 1, 113824, 9, 113823, 6, 16534, 14696, 11, 114999, 7, 99564, 102947, 99421, 100690)
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//출금신청
	public static function statisticsMileageWithdrawal($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$dbName= self::MainDBName;
		$db = static::GetDB();
		$query = $db->prepare("SELECT
				COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 THEN idx END) AS count,
				IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 THEN amount ELSE 0 END), 0) AS nonFeeAmount,
				COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 AND fee != 0 THEN idx END) AS feeCount,
				IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 THEN fee ELSE 0 END), 0) AS fee,
				COALESCE(
					SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 THEN amount ELSE 0 END) +
					SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 204201 THEN fee ELSE 0 END),
					0
				) AS amount
			FROM ebuy.ClientMileageWithdrawal
			WHERE createTime BETWEEN :startDate AND :endDate
			AND statusIDX = 204201
			AND clientIDX NOT IN (116207, 110691, 101636, 115808, 3, 101017, 116267, 116452, 116453, 116454, 116455, 116456, 116253, 116141, 116020, 116018, 99408, 99406, 99404, 99392, 99391, 99390, 99389, 4, 116247, 116243, 113814, 116230, 116168, 2, 113825, 116201, 8, 116291, 116286, 116285, 116169, 116145, 116022, 116019, 99407, 99405, 99403, 1, 113824, 9, 113823, 6, 16534, 14696, 11, 114999, 7, 99564, 102947, 99421, 100690)
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	/*

	테스트건 메모:
	[지불대행]
	Tradeview Markets
	KST : 2024-02-29 00:59:53.230
	$1,000
	invoice ID: 1709135989-oJV

	CapitalXtend
	KST : 2024-02-07 18:00:12.470
	$50
	invoice ID: 70b1e5ce-7580-4046-acd0-189d594afc22

	[출금대행]
	CapitalXtend
	KST : 2024-02-07 18:39:04.860
	$40
	invoice ID: b66115e9-2f39-4cb9-b45f-b437c18b7e5e

	*/
	//지불대행
	public static function statisticsApiDeposit($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("SELECT
			marketCode,
			COUNT(idx) AS count,
			IFNULL(SUM(balance), 0) AS nonfeeamount,
			IFNULL(SUM(amount), 0) AS amount,
			COALESCE( SUM(amount) - SUM(balance), 0 ) AS feeAmount,
			IFNULL(SUM(clientFee), 0) AS mileageFee,
			IFNULL(SUM(mileage), 0) AS nonfeemileage,
			COALESCE( SUM(mileage) - SUM(clientFee), 0 ) AS mileage
			FROM ebuyAPI.MarketDepositLog
			WHERE createTime BETWEEN :startDate AND :endDate
			AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
			AND statusIDX = 906101
			AND (
				invoiceID!='1709135989-oJV'
				AND invoiceID!='70b1e5ce-7580-4046-acd0-189d594afc22'
			)
			GROUP BY marketCode
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	//지불대행수수료
	public static function statisticsApiDepositFee($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("
			SELECT
			clientFeePer,
			COUNT(idx) AS count,
			IFNULL(SUM(balance), 0) AS nonfeeamount,
			IFNULL(SUM(amount), 0) AS amount,
			COALESCE( SUM(amount) - SUM(balance), 0 ) AS feeAmount,
			IFNULL(SUM(clientFee), 0) AS mileageFee,
			IFNULL(SUM(mileage), 0) AS nonfeemileage,
			COALESCE( SUM(mileage) - SUM(clientFee), 0 ) AS mileage
			FROM ebuyAPI.MarketDepositLog
			WHERE createTime BETWEEN :startDate AND :endDate
			AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
			AND statusIDX = 906101
			AND (
				invoiceID!='1709135989-oJV'
				AND invoiceID!='70b1e5ce-7580-4046-acd0-189d594afc22'
			)
			GROUP BY clientFeePer
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//출금대행
	public static function statisticsApiWithdrawal($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("
			SELECT
			marketCode,
			COUNT(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 908102 THEN idx END) AS count,
			IFNULL(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = 908102 THEN amount ELSE 0 END), 0) AS amount,
			IFNULL(SUM(CASE WHEN createTime BETWEEN :startDate AND :endDate AND statusIDX = 908102 THEN mileage ELSE 0 END), 0) AS mileage
			FROM ebuyAPI.MarketWithdrawalLog
			WHERE createTime BETWEEN :startDate AND :endDate
			AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
			AND statusIDX = 908102
			AND (
				invoiceID!='JUNMO-MAN'
				AND invoiceID!='b66115e9-2f39-4cb9-b45f-b437c18b7e5e'
			)
			GROUP BY marketCode
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	// 240201 통계끝


	public static function statisticsApiDepositAvgRate($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("
			SELECT
			SUM(amount * exchangeRate) / SUM(amount) AS avg_exchange_rate
			FROM ebuyAPI.MarketDepositLog
			WHERE createTime BETWEEN :startDate AND :endDate AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
			AND statusIDX = 906101
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	public static function statisticsApiWithdrawalAvgRate($data=null)
	{
		$startDate=$data['startDate'];
		$endDate=$data['endDate'];
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("
			SELECT
			SUM(amount * exchangeRate) / SUM(amount) AS avg_exchange_rate
			FROM ebuyAPI.MarketWithdrawalLog
			WHERE createTime BETWEEN :startDate AND :endDate AND marketCode NOT IN ('m4x26bx','3pswiaa','gn8nom3','a036qsl','x6l2qdh','b3kdads','demo1234','zgqvgqg')
			AND statusIDX = 908102
		");
		$query->bindValue(':startDate', $startDate);
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	public static function statisticsApiBankScraping($data=null)
	{
		$endDate=$data;
		// $marketCode = $data;
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("
			SELECT afterAmount, ebuyBankIDX
			FROM (
				SELECT afterAmount, ebuyBankIDX,
					    ROW_NUMBER() OVER (PARTITION BY ebuyBankIDX ORDER BY tradeTime DESC) as row_num
				FROM $dbName.EbuyBankLog
				WHERE tradeTime <= :endDate
			) AS ranked
			WHERE row_num = 1
		");
		$query->bindValue(':endDate', $endDate);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	/*
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통동통통통통통통통통통통통통통계계계계계계계계계계계계계계제계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	통통통통통통통통통통통통통통통통통통통통통통통통통통통계계계계계계계계계계계계계계계계계계계계계계계계계계계
	*/
	// 240205 통계끝




















































	//개발자 > 마일리지히스토리매칭에러로그 에서 뉴바이 마일리지순으로 보고싶어가지고 한거
	public static function GetDataTableListLoad($data=null)
	{
		/*
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		*/
		// $db = static::GetDB();
		// $dbName= self::MainDBName;
		// $dataDbKey=self::dataDbKey;
		// $query = $db->prepare("SELECT
		// A.idx,
		// AES_DECRYPT(A.name,:dataDbKey) AS name,
		// AES_DECRYPT(A.email,:dataDbKey) AS email,

		// (
		// 	SELECT
		// 	COALESCE((
		// 		SUM(
		// 			/*203201, 203202, 203203, 204211, 204212, 205101, 908102, 251402, 251401 앱*/
		// 			CASE
		// 				WHEN statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN amount
		// 				ELSE 0
		// 			END
		// 		) - SUM(
		// 			/*204101, 204102, 205201, 906101, 251201 앱*/
		// 			CASE
		// 				WHEN statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN amount
		// 				ELSE 0
		// 			END
		// 		)
		// 	), 0) AS phone
		// 	FROM $dbName.ClientMileage WHERE clientIDX=A.idx
		// ) AS phone,

		// '' AS birth,

		// '' AS status,
		// '' AS createTime,
		// '' AS migrationIDX,
		// '' AS emergency,
		// '' AS gradeName,
		// '' AS deviceNumber,
		// '' AS deviceType
		// FROM $dbName.Client AS A
		// ORDER BY A.createTime DESC
		// ");
		// $query->bindValue(':dataDbKey', $dataDbKey);
		// $query->execute();
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		// return $result;
		/*
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		뉴바이 마일리지순으로 보고싶어가지고 한거
		*/











// 		$db = static::GetDB();
// 		$dbName= self::MainDBName;
// 		$dataDbKey=self::dataDbKey;
// 		$query = $db->prepare("
// 		SELECT
// 		A.clientIDX AS idx
// 		,'' AS createTime
// 		,IFNULL(AES_DECRYPT(B.name,:dataDbKey), 0) AS name
// 		,'' AS birth
// 		,ROUND(SUM(A.amount), 2) - IFNULL(D.upto, 0) AS phone
// 		,IFNULL(B.gradeIDX, 0) AS email
// 		,IFNULL(C.name, 0) AS gradeName
// 		,IFNULL(C.upto, 0) AS deviceType
// 		,ROUND(SUM(A.amount), 2) AS deviceNumber
// 		,IFNULL(D.upto, 0) AS status
// 		,'' AS emergency
// 		,'' AS migrationIDX
// 		FROM $dbName.ClientAllAmount AS A
// 		LEFT JOIN $dbName.Client AS B ON B.idx = A.clientIDX
// 		LEFT JOIN $dbName.ClientGrade AS C ON C.idx = B.gradeIDX
// 		LEFT JOIN $dbName.ClientGrade AS D ON D.idx = B.gradeIDX+1
// WHERE B.gradeIDX != 3
// GROUP BY A.clientIDX
// HAVING deviceNumber > status
// 		");
// 		$query->bindValue(':dataDbKey', $dataDbKey);
// 		$query->execute();
// 		$result=$query->fetchAll(PDO::FETCH_ASSOC);
// 		return $result;











  //       $oldDb = static::GetOldDB();
  //       $oldDbName= self::EbuyOldDBName;
		// $query = $oldDb->prepare("
		// SELECT
  //           C.member_srl AS idx,
  //           A.APH_NAME AS name,
  //           B.cellphone AS phone,
  //           MAX(A.APH_SUC_P_ADM_REG) AS createTime,
		// 	'' AS birth,
		// 	'' AS email,
		// 	'' AS gradeName,
		// 	'' AS deviceType,
		// 	'' AS deviceNumber,
		// 	'' AS status,
		// 	'' AS emergency,
		// 	'' AS migrationIDX
  //       FROM ebuy.AA_AGENT_PARTNER_HIS AS A
  //       LEFT JOIN ebuy.AA_AUTH_CALL AS B ON A.APH_SRL = B.srl
  //       INNER JOIN ebuy.eb_member AS C ON A.APH_SRL = C.member_srl
  //       GROUP BY A.APH_SRL
  //       ORDER BY MAX(A.APH_SUC_P_ADM_REG) DESC
		// ");
		// $query->execute();
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		// return $result;


















  //       $db = static::GetDB();
		// $dbName= self::MainDBName;
  //       $dataDbKey=self::dataDbKey;
		// $query = $db->prepare("SELECT
		// AES_DECRYPT(name,:dataDbKey) AS name,
		// AES_DECRYPT(phone,:dataDbKey) AS phone,
		// '' AS idx,
		// '' AS createTime,
		// '' AS birth,
		// '' AS email,
		// '' AS gradeName,
		// '' AS deviceType,
		// '' AS deviceNumber,
		// '' AS status,
		// '' AS emergency,
		// '' AS migrationIDX
		// FROM $dbName.Client
		// WHERE statusIDX='226101'
		// ");
  //       $query->bindValue(':dataDbKey', $dataDbKey);
  //       $query->execute();
		// $result=$query->fetchAll(PDO::FETCH_ASSOC);
		// return $result;















		$db = static::GetDB();
		$dbName= self::MainDBName;
		$dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
		'' AS idx,
		AES_DECRYPT(A.phone,:dataDbKey) AS name,
		AES_DECRYPT(A.name,:dataDbKey) AS email,
		'' AS phone,

		'' AS birth,

		'' AS status,
		'' AS createTime,
		'' AS migrationIDX,
		'' AS emergency,
		'' AS gradeName,
		'' AS deviceNumber,
		'' AS deviceType
		FROM $dbName.Client AS A
		WHERE A.statusIDX!='226201'
		AND A.statusIDX!='226202'
		GROUP BY phone
		");
		$query->bindValue(':dataDbKey', $dataDbKey);
		$query->execute();
		$result=$query->fetchAll(PDO::FETCH_ASSOC);
		return $result;










	}









	public static function ClientGubuyTotalApiDepositFuckYa($data=null)
	{
	}
}