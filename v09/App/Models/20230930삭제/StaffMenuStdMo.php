<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMenuStdMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//MenuCon 스태프 페이지 메뉴 리스트
	public static function GetMenuList($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$db = static::GetDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		name,
		url,
		seq,
		status,
		memo
		FROM $MainDBName.$BasicTable
        WHERE momIDX=0 ORDER BY seq 
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 스태프 페이지 2차 메뉴 리스트
	public static function GetChildMenuList($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$staffIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		name,
		url,
		seq,
		status,
		memo
		FROM $MainDBName.$BasicTable
        WHERE momIDX='$staffIDX' ORDER BY seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 메뉴 숨김 status 조사
	public static function CheeseEyeAction($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM $MainDBName.$BasicTable
        WHERE idx='$targetIDX' ORDER BY seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 컨펌창 열때 정보 불러오기
	public static function MenuConfirmData($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		name,
		url
		FROM $MainDBName.$BasicTable
        WHERE idx='$targetIDX' ORDER BY seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//MenuCon 삭제 전 2차메뉴가 있는지 조사
	public static function MenuDeleteCheck($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$targetIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM $MainDBName.$BasicTable
        WHERE momIDX='$targetIDX'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	// //StStaffGroupCon 권한에 따른 메뉴
	// public static function GetStaffMenuListByGroup($data=null)
	// {
	// 	$db = static::getDB();
	// 	$CategoryList = $db->query("SELECT
	// 		idx
	// 		,momIDX
	// 		,name
	// 		,IF (STATUS = '3', '활성', '비활성') AS status
	// 		FROM $MainDBName.MenuStd AS A
	// 		ORDER BY
	// 		CASE WHEN momIDX = 0 THEN seq ELSE
	// 		(SELECT seq FROM $MainDBName.MenuStd WHERE idx = A.momIDX ORDER BY seq) END,
	// 		CASE WHEN momIDX != 0 THEN seq END
	// 		");
	// 	$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
	// 	return $result;
	// }


	//NavCon 1차메뉴 리스트
	public static function GetMenuListByPermission($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$gradeIDX = $data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.url,
		A.seq,
		A.status,
		A.memo
		/*B.permission,
		B.MdGradeIDX*/
		FROM $MainDBName.$BasicTable AS A
		/*LEFT JOIN $MainDBName.AdminGroup AS B ON A.idx = B.MenuIDX*/
      	WHERE A.momIDX=0 AND A.status=3/*AND B.MdGradeIDX = '$gradeIDX' OR B.MdGradeIDX IS NULL*/
      	ORDER BY A.seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	//NavCon 2차메뉴 리스트
	public static function GetChildMenuListByPermission($data=null)
	{
		$MainDBName=self::MainDBName;
		$BasicTable=self::BasicTable['StaffMenuStd'];
		$menuIDX=$data['menuIDX'];
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		name,
		url,
		seq,
		status,
		memo
		FROM $MainDBName.$BasicTable
		WHERE  momIDX='$menuIDX'  AND status=3
		GROUP BY idx
		ORDER BY seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	// //NavCon 2차메뉴 리스트 원본
	// public static function GetChildMenuListByPermission($data=null)
	// {
	// 	$MainDBName=self::MainDBName;
	// 	$menuIDX=$data['menuIDX'];
	// 	$gradeIDX=$data['gradeIDX'];
	// 	$loginIDX=$data['loginIDX'];
	// 	$db = static::getDB();
	// 	$CategoryList = $db->query("SELECT
	// 	A.idx,
	// 	A.momIDX,
	// 	A.name,
	// 	A.url,
	// 	A.seq,
	// 	A.status,
	// 	A.memo
	// 	B.permission
	// 	COALESCE(C.count, 0) AS count
	// 	FROM $MainDBName.StaffMenuStd AS A
	// 	INNER JOIN $MainDBName.AdminGroup AS B ON A.idx = B.MenuIDX
	// 	LEFT JOIN (
	// 		SELECT menuIDX, COUNT(idx) AS count
	// 		FROM $MainDBName.StaffAlarm
	// 		WHERE staffIDX = '$loginIDX' AND status = 2
	// 		GROUP BY menuIDX
	// 	) AS C ON A.idx=C.menuIDX
	// 	WHERE  A.momIDX='$menuIDX' AND B.MdGradeIDX='$gradeIDX'
	// 	GROUP BY A.idx
	// 	ORDER BY A.seq
	// 	");
	// 	$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
	// 	return $result;
	// }






    
}