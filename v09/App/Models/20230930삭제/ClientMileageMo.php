<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientMileageMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//MileageDepositCon , ClientDetailCon 해당 clientIDX 정보 가져오기
	public static function GetTotalClientMileageDeposit($data=null)
	{
		$targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(A.idx) AS totalDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203201 AND clientIDX='$targetIDX') AS manualDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203202 AND clientIDX='$targetIDX') AS autoDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203203 AND clientIDX='$targetIDX') AS accountDeposit,
        IFNULL(MAX(A.createTime),'-') AS recentTime,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX IN (203201, 203202, 203203)
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
	}

	//MileageWithdrawalCon , ClientDetailCon 해당 clientIDX 정보 가져오기
    public static function GetTotalClientMileageWithDrawal($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        /*
            204101,204102의 총 카운트에서 (4) 204211와 204212가 있는 데이터들을 (1) 조사 한다음
            출금이 취소된 데이터를 빼준 다음 카운트롤 출력해줌 (0이상일때)
        */
        COUNT(A.idx) AS withCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204101 AND clientIDX='$targetIDX') AS manualWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204102 AND clientIDX='$targetIDX') AS autoWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE clientIDX='$targetIDX' AND statusIDX IN (204211, 204212)) AS minusCount,
        IFNULL(MAX(A.createTime),'-') AS recentTime,
        /*
            204101,204102의 총 amount에서 (1,000,000)구한 후
            출금이 취소된 204211,204212의 총 amount (50,000)를 빼준 amount (950,000)의 데이터를 출력해줌(0이상일때)
        */
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS tmpTotalAmount,
        (SELECT IFNULL(FORMAT(SUM(amount),0),'0') FROM $dbName.ClientMileage WHERE clientIDX='$targetIDX' AND statusIDX IN (204211, 204212)) AS minusSUMAmount,

        /*
            avgWithAmount는 총 출금액(출금이 취소된 데이터를 안 뺀 금액)에서 총카운트(출금이 취소 안된 총 횟수)평균만 계산함
        */
        -- IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        (SELECT IFNULL(MAX(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX='$targetIDX' AND statusIDX NOT IN (204211, 204212)) AS tmpMaxWithAmount,
        (SELECT IFNULL(MIN(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX='$targetIDX' AND statusIDX NOT IN (204211, 204212)) AS tmpMinWithAmount

        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX IN (204101, 204102);
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);

        $withCount=$result['withCount'];
        $manualWithCount=$result['manualWithCount'];
        $autoWithCount=$result['autoWithCount'];
        $minusCount=$result['minusCount'];

        $totalWithdrawal=0;
        $manualWithdrawal=0;
        $autoWithdrawal=0;
        //총 출금횟수
        if($withCount>0){
            $totalWithdrawal=$withCount-$minusCount;
        }
        //수동출금수
        if($manualWithCount>0){
            $manualWithdrawal=$manualWithCount-$minusCount;
        }
        //자동출금수
        if($autoWithCount>0){
            $autoWithdrawal=$autoWithCount-$minusCount;
        }


        // --------------------------
        $tmpTotalAmount = intval(str_replace(',', '', $result['tmpTotalAmount']));
        $minusSUMAmount = intval(str_replace(',', '', $result['minusSUMAmount']));

        $tmpMaxWithAmount = intval(str_replace(',', '', $result['tmpMaxWithAmount']));
        $tmpMinWithAmount = intval(str_replace(',', '', $result['tmpMinWithAmount']));

        $totalAmount=0;
        $totalAvgWithAmount=0;
        $maxWithAmount=0;
        $minWithAmount=0;

        //총 출금액
        if($tmpTotalAmount>0){
            $totalAmountNumber=$tmpTotalAmount-$minusSUMAmount;
            $totalAmount = number_format($totalAmountNumber);
        }

        //출금 평균금액
        if($totalWithdrawal>0){
            $totalAvgWithAmountNumber=$totalAmountNumber/$totalWithdrawal;
            $totalAvgWithAmount = number_format($totalAvgWithAmountNumber);
        }
        //최대 출금액
        if($tmpMaxWithAmount>0){
            $maxWithAmountNumber=$tmpMaxWithAmount;
            $maxWithAmount = number_format($maxWithAmountNumber);
        }
        //최소 출금액
        if($tmpMinWithAmount>0){
            $minWithAmountNumber=$tmpMinWithAmount;
            $minWithAmount = number_format($minWithAmountNumber);
        }



        $result['totalWithdrawal'] = $totalWithdrawal;
        $result['manualWithdrawal'] = $manualWithdrawal;
        $result['autoWithdrawal'] = $autoWithdrawal;
        $result['totalAmount'] = $totalAmount;
        $result['avgWithAmount'] = $totalAvgWithAmount;
        $result['maxWithAmount'] = $maxWithAmount;
        $result['minWithAmount'] = $minWithAmount;



        return $result;
    }

    //MileageWithdrawalCon , ClientDetailCon 해당 clientIDX 총 마일리지
    public static function getMileage($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            COALESCE((
                SUM(
                    CASE
                        WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101) THEN A.amount
                        ELSE 0
                    END
                ) - SUM(
                    CASE
                        WHEN A.statusIDX IN (204101, 204102, 205201) THEN A.amount
                        ELSE 0
                    END
                )
            ), 0) AS mileage
            FROM $dbName.ClientMileage AS A WHERE A.clientIDX='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        if(isset($result['mileage']) && $result['mileage'] !=null){
            $mileage=$result['mileage'];
            $resultMileage=number_format($mileage);
            return $resultMileage;
        }else{ return 0;}
    }


    
}