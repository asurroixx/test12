<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class FilesMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */


    //NoticeCon 해당 파일 기본 정보
    public static function GetFileData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->query("SELECT
        idx,
        serverName,
        orignName,
        ext,
        targetIDX,
        statusIDX,
        createTime
        FROM $dbName.Files
        WHERE targetIDX='$targetIDX' AND statusIDX IN (342401,342201)
        ");
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //AppNoticeCon 해당 파일 기본 정보
    public static function GetAppNoticeFileData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->query("SELECT
        idx,
        serverName,
        orignName,
        ext,
        targetIDX,
        statusIDX,
        createTime
        FROM $dbName.Files
        WHERE targetIDX='$targetIDX' AND statusIDX IN (313401,313201)
        ");
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}