<?php

namespace App\Models;

use PDO;

class BankMo extends \Core\Model
{
    //BankCon 데이터테이블 , EbuyBankCon 리스트 , PartnerCon 디테일 랜더시
    public static function GetBankData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        (SELECT
            CASE idx
                WHEN 329301 THEN '정상'
                WHEN 329401 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE statusIDX=idx) AS status,
        name,
        code,
        img
        FROM $dbName.Bank
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

     //BankCon 해당 마켓 idx statusIDX 조사
    public static function BankStatusData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        statusIDX
        FROM $dbName.Bank
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

}