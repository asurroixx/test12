<?php

namespace App\Models;

use PDO;

class ManagerMo extends \Core\Model
{

    //MarketManagerCon 동일한 이메일 있는지 조사
    public static function IssetManagerEmail($data=null)
    {
        $email=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        idx
        FROM $dbName.Manager
        WHERE AES_DECRYPT(email,:dataDbKey)=:email
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':email', $email);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public static function getAllManager($data=null)
    {
        $email=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        idx,
        AES_DECRYPT(email,:dataDbKey) AS email,
        null as emailSettingIDX
        FROM $dbName.Manager
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    //이메일 유효성 검사 샌드박스용
    public static function issetEmailInSand($data=null)
    {
        $EbuySandboxDBName=self::EbuySandboxDBName;
        $email=$data;
        $email=trim($email);
        $dbsand = static::GetSandboxDB();
        $dataDbKey=self::dataDbKey;
        $Sel = $dbsand->prepare("SELECT
        idx,
        AES_DECRYPT(email,:dataDbKey) AS email
        FROM $EbuySandboxDBName.Manager
        WHERE AES_DECRYPT(email,:dataDbKey)=:email
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':email', $email);
        $Sel->execute();
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }


}