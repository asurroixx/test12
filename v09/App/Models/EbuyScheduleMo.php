<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class EbuyScheduleMo extends \Core\Model
{

	//CalendarCon 데이터테이블
    public static function DataTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01';
        }else{
            $startDate;
        }
        if($endDate==""){
            $endDate=date('Y-m-d');
        }else{
            $endDate;
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        `date`,
        MONTH(`date`) AS month,
        DAY(`date`) AS day,
        YEAR(`date`) AS year,
        DATE_FORMAT(`date`, '%a') AS dayOfWeek,
        SUBSTRING_INDEX(openTime, ':', 2) AS openTime,
        SUBSTRING_INDEX(closeTime, ':', 2) AS closeTime,
        statusIDX,
        '' AS saveBtn,
        '' AS statusName,
        memo
        FROM $dbName.EbuySchedule
        WHERE `date` BETWEEN '$startDate' AND '$endDate';
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

	//CalendarCon 디테일
	public static function GetDetailData($data=null)
	{
		$targetIDX = $data;
        $db = static::GetDB();
		$dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$query = $db->prepare("SELECT
        A.idx,
        A.holiday,
        A.memo,
        (SELECT
            CASE idx
                WHEN 330401 THEN '공식휴일'
                WHEN 330402 THEN '이바이휴일'
                WHEN 330501 THEN '비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        CASE
	        WHEN A.staffIDX = 0 THEN '외부API'
	        ELSE AES_DECRYPT(B.email, :dataDbKey)
	    END AS staffEmail,
	    A.createTime
        FROM $dbName.Calendar AS A
        LEFT JOIN $dbName.Staff AS B ON A.staffIDX=B.idx
        WHERE A.idx=:targetIDX
		");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//CalendarCon 해당데이터
	public static function GetUpdateTargetData($data=null)
	{
		$targetIDX = $data;
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$query = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.openTime,
        A.closeTime,
        A.memo
        FROM $dbName.EbuySchedule AS A
        WHERE A.idx='$targetIDX'
		");
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

    //해당 날짜 조사
    public static function GetResponseDate($data=null)
    {
        $expectedTime = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.date,
        A.openTime,
        A.closeTime
        FROM $dbName.EbuySchedule AS A
        WHERE A.date='$expectedTime'
        ");
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }




    
}