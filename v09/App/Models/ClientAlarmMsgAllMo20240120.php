<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientAlarmMsgAllMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function getClientAlarmMsg($data=null)
	{
        $db = static::GetDB();
		$dbName= self::MainDBName;
		$targetIDX = $data['targetIDX'];
		$statusIDX = $data['statusIDX'];

 		$query = $db->query("SELECT
			idx,
			title,
			con
		FROM $dbName.ClientAlarmMsgAll
		WHERE statusIDX = '$statusIDX' AND idx='$targetIDX'
		");
		$result=$query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
    
}