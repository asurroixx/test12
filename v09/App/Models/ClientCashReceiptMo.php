<?php

namespace App\Models;

use PDO;

class ClientCashReceiptMo extends \Core\Model
{


    public static function GetDataTableListLoad($data=null)
    {
        $startDate   = $data['startDate'] ?? '1970-01-01';
        $endDate     = $data['endDate'] ?? date('Y-m-d');
        $startDate  .= ' 00:00:00';
        $endDate    .= ' 23:59:59';

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            ROW_NUMBER() OVER (ORDER BY A.createTime,A.idx ASC) AS no
            ,A.idx
            ,A.clientIDX
            ,A.clientName
            ,A.ReceiptTypeNo
            ,A.ReceiptAmt
            ,A.createTime


            ,CASE A.statusIDX
                WHEN 811101 THEN '3'
                WHEN 811201 THEN '2'
                WHEN 811301 THEN '2'
                WHEN 811401 THEN '3'
                WHEN 811402 THEN '1'
                WHEN 811403 THEN '1'
                WHEN 812101 THEN '3'
                ELSE '-'
            END AS onlyOrderable

            ,CASE A.statusIDX
                WHEN 811101 THEN '미발행'
                WHEN 811201 THEN '발행대기'
                WHEN 811301 THEN '발행중'
                WHEN 811401 THEN '발행완료'
                WHEN 811402 THEN '발행실패'
                WHEN 811403 THEN '발행실패(통신에러)'
                WHEN 812101 THEN '발행완료(취소신청)'
                ELSE '-'
            END AS status

            ,CASE A.targetStatusIDX
                WHEN 906101 THEN '지불대행'
                WHEN 251401 THEN 'P2P'
                ELSE '-'
            END AS type

            ,CASE
                WHEN A.statusIDX IN (811101, 811201, 811301) THEN '-'
                ELSE A.resultTime
            END AS resultTime

            ,CASE
                WHEN A.targetStatusIDX = 906101 THEN B.createTime
                -- WHEN A.targetStatusIDX = 251401 THEN '미적용'
                WHEN A.targetStatusIDX = 251401 THEN A.createTime
                ELSE '-'
            END AS originReg

        FROM $dbName.ClientCashReceipt AS A
        LEFT JOIN $dbName.MarketDepositLog AS B ON B.idx = A.targetIDX AND A.targetStatusIDX = 906101
        -- LEFT JOIN C ON C.idx = A.targetIDX AND A.targetStatusIDX = 906999
        WHERE (A.createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public static function GetFulldata($data=null)
    {
        $idx=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            A.idx
            ,A.createTime
            ,A.fullData


        FROM $dbName.ClientCashReceiptFullData AS A
        WHERE A.ClientCashReceiptIDX=:ClientCashReceiptIDX
        ");
        $query->bindValue(':ClientCashReceiptIDX', $idx);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetDetail($data=null)
    {
        $idx=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            idx,
            targetIDX,
            targetStatusIDX,
            clientIDX,
            clientTel,
            clientName,
            clientEmail,
            ReceiptTypeNo,
            TRAN_REQ_ID,
            createTime,
            ReceiptAmt,
            statusIDX,
            COALESCE(resultTime, '-') AS resultTime,
            COALESCE(result_TID, 0) AS result_TID,
            result_msg,
            CASE result_code
                WHEN  7001 THEN '성공'
                ELSE result_code
            END AS code,
            CASE statusIDX
                WHEN  811101 THEN '미발행'
                WHEN  811201 THEN '발행대기'
                WHEN  811301 THEN '발행중'
                WHEN  811401 THEN '발행완료'
                WHEN  811402 THEN '발행실패'
                WHEN  811403 THEN '발행실패(통신에러)'
                WHEN  812101 THEN '발행완료(취소신청)'
                ELSE '-'
            END AS status,
            CASE targetStatusIDX
                WHEN  906101 THEN '지불대행'
                WHEN  251401 THEN 'P2P'
                ELSE '-'
            END AS type
        FROM $dbName.ClientCashReceipt
        WHERE idx=:idx
        ");
        $query->bindValue(':idx', $idx);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetData($data=null)
    {
        $idx=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            idx,
            statusIDX,
            ReceiptAmt,
            result_TID,
            clientIDX
        FROM $dbName.ClientCashReceipt
        WHERE idx=:idx
        ");
        $query->bindValue(':idx', $idx);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }







}