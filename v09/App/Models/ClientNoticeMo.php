<?php

namespace App\Models;

use PDO;

class ClientNoticeMo extends \Core\Model
{

    //AppNoticeCon 데이터테이블
    public static function DataTableListLoad($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.title,
        A.content,
        (SELECT
            CASE idx
                WHEN 313401 THEN '일반'
                WHEN 313402 THEN '긴급'
                WHEN 313403 THEN '비활성'
                WHEN 313404 THEN '이벤트 및 혜택'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        A.createTime,
        A.popupStatusIDX,
        AES_DECRYPT(B.email,:dataDbKey) AS staffEmail
        FROM $dbName.ClientNotice AS A
        JOIN $dbName.Staff AS B ON A.staffIDX=B.idx
        WHERE A.statusIDX IN (313401,313402,313403,313404)
        ORDER BY
            CASE A.statusIDX
                WHEN 313402 THEN 1 -- 긴급이 제일 먼저 나오도록
                ELSE 2 END, A.createTime DESC;
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //AppNoticeCon 디테일
    public static function GetNoticeDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        title,
        content,
        (SELECT
            CASE idx
                WHEN 313401 THEN '일반'
                WHEN 313402 THEN '긴급'
                WHEN 313403 THEN '비활성'
                WHEN 313404 THEN '이벤트 및 혜택'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE statusIDX=idx) AS status,
        mailingStatusIDX,
        popupStatusIDX,
        createTime
        FROM $dbName.ClientNotice
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //AppNoticeCon 해당 타겟 데이터
    public static function GetTargetData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        title,
        content,
        statusIDX,
        (SELECT
            CASE idx
                WHEN 313401 THEN '일반'
                WHEN 313402 THEN '긴급'
                WHEN 313403 THEN '비활성'
                WHEN 313404 THEN '이벤트 및 혜택'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE statusIDX=idx) AS statusVal,
        mailingStatusIDX,
        popupStatusIDX,
        (SELECT
            CASE idx
                WHEN 313601 THEN '팝업 활성'
                WHEN 313601 THEN '팝업 비활성'
                ELSE '' END
            AS status
        FROM $dbName.Status WHERE statusIDX=idx) AS popupStatusVal,
        createTime
        FROM $dbName.ClientNotice
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //AppNoticeCon 인서트,업데이트 시 팝업 갯수 조사
    public static function GetPopupCount($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(idx) AS popupCount
        FROM $dbName.ClientNotice
        WHERE popupStatusIDX=313601 AND idx <> '$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }





    public static function getClientAlarmMsg($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            idx,
            title,
            pushAlarmContent
        FROM $dbName.ClientNotice
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }






}