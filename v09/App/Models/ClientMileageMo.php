<?php

namespace App\Models;

use PDO;

class ClientMileageMo extends \Core\Model
{

    //MileageHistoryCon 데이터테이블
    public static function GetDatatableList($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.createTime,
        A.statusIDX,
        A.targetIDX,
        CASE
            WHEN A.statusIDX IN(203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN '증감'
            WHEN A.statusIDX IN(204101, 204102, 205201, 906101, 251201) THEN '차감'
            ELSE '-'
        END AS type,
        CASE
            WHEN A.statusIDX = 203201 THEN '마일리지 입금(수동)'
            WHEN A.statusIDX = 203202 THEN '마일리지 입금(자동)'
            WHEN A.statusIDX = 203203 THEN '마일리지 입금(가상계좌)'
            WHEN A.statusIDX = 204211 THEN '마일리지 출금 취소(기타)'
            WHEN A.statusIDX = 204212 THEN '마일리지 출금 취소(스태프)'
            WHEN A.statusIDX = 205101 THEN '스태프에 의한 증감'
            WHEN A.statusIDX = 908102 THEN '출금대행'
            WHEN A.statusIDX = 251402 THEN 'P2P 취소'
            WHEN A.statusIDX = 251401 THEN 'P2P 완료'
            WHEN A.statusIDX = 206101 THEN '시스템에 의한 증감'
            WHEN A.statusIDX = 204101 THEN '마일리지 출금(수동)'
            WHEN A.statusIDX = 204102 THEN '마일리지 출금(자동)'
            WHEN A.statusIDX = 205201 THEN '스태프에 의한 차감'
            WHEN A.statusIDX = 906101 THEN '지불대행'
            WHEN A.statusIDX = 251201 THEN 'P2P 구매자 차감'
            ELSE '-'
        END AS content,
        A.clientIDX,
        A.amount,
        B.statusIDX AS clientStausIDX,
        CASE B.migrationIDX
            WHEN 0 THEN '신규'
            ELSE B.migrationIDX END
        AS migrationIDX,
        AES_DECRYPT(B.name,:dataDbKey) AS name
        FROM $dbName.ClientMileage AS A
        INNER JOIN $dbName.Client AS B ON A.clientIDX=B.idx
        WHERE A.createTime BETWEEN :startDate AND :endDate;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }




    //PartnerCon 데이터테이블
    public static function GetApiWithdrawalCount($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(idx) as idxCount,
        MAX(createTime) as createTime
        FROM $dbName.ClientMileage
        WHERE clientIDX = '$clientIDX'
        AND statusIDX LIKE '908%'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //PartnerCon 데이터테이블
    public static function GetApiDepositCount($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(idx) as idxCount,
        MAX(createTime) as createTime
        FROM $dbName.ClientMileage
        WHERE clientIDX = '$clientIDX'
        AND statusIDX LIKE '906%'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //20240116 jm getMileage는 ClientDetailMo.php 에서 함
    public static function getMileage($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            COALESCE((
                SUM(
                    /*203201, 203202, 203203, 204211, 204212, 205101, 908102, 251402, 251401 앱*/
                    CASE
                        WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401,206101) THEN A.amount
                        ELSE 0
                    END
                ) - SUM(
                    /*204101, 204102, 205201, 906101, 251201 앱*/
                    CASE
                        WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN A.amount
                        ELSE 0
                    END
                )
            ), 0) AS mileage
            FROM $dbName.ClientMileage AS A WHERE A.clientIDX='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        if(isset($result['mileage']) && $result['mileage'] !=null){
            $mileage=$result['mileage'];
            return $mileage;
        }else{
            return 0;
        }
    }
}
