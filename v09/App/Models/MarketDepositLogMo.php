<?php

namespace App\Models;

use PDO;

class MarketDepositLogMo extends \Core\Model
{
    public static function GetDepositData($data=null)
    {
        $startDate  = $data['startDate'] ?? '1970-01-01';
        $endDate    = $data['endDate'] ?? date('Y-m-d');
        $startDate .= ' 00:00:00';
        $endDate   .= ' 23:59:59';


        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;

        $query = $db->prepare("SELECT
            COALESCE(( -- 디파짓 성공의 IDX
                SELECT idx
                FROM $dbName.MarketDepositLog
                WHERE invoiceIDunique = A.invoiceIDunique
                AND marketCode = A.marketCode
                AND statusIDX = '906101'
            ), ( -- 성공의 IDX 없으면 에러 콜백 IDX 아닌것중 제일 최근
                SELECT MAX(idx)
                FROM $dbName.MarketDepositLog
                WHERE invoiceIDunique = A.invoiceIDunique
                AND marketCode = A.marketCode
                AND statusIDX NOT LIKE '9044%' -- 에러 콜백
                AND statusIDX NOT LIKE '9063%' -- 에러 콜백
            )) AS idx,
            ROW_NUMBER() OVER (ORDER BY A.createTime,A.idx ASC) AS no,
            marketCode,
            amount,
            invoiceID,
            marketUserEmail,
            MIN(createTime) AS createTime,
            MAX(mileage) AS mileage,
            MAX(afterMileage) AS afterMileage,
            MAX(mileage) + MAX(afterMileage) AS beforeMileage,
            CASE
                WHEN EXISTS ( -- 이 주문에 성공 스테이터스가있으면 000
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9061%'
                ) THEN '000'
                WHEN EXISTS ( -- 실패 스테이터스 있으면 실패 statusIDX
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9062%'
                ) THEN MAX(CASE WHEN statusIDX LIKE '9062%' THEN statusIDX END)
                WHEN EXISTS ( -- 취소 스테이터스 있으면 취소 statusIDX
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9043%'
                ) THEN MAX(CASE WHEN statusIDX LIKE '9043%' THEN statusIDX END)
                WHEN EXISTS ( -- 위 조건들 없다면 디파짓진행 statusIDX
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9051%'
                ) THEN MAX(CASE WHEN statusIDX LIKE '9051%' THEN statusIDX END)
                WHEN EXISTS ( -- 위 조건들 없다면 디파짓진행 statusIDX
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX = '904101'
                ) THEN '904101'
                ELSE MAX(CASE WHEN statusIDX NOT LIKE '9044%' THEN statusIDX END) -- 다 없으면 오류 statusIDX
            END AS errorCode,
            CASE
                WHEN EXISTS ( -- 성공 스테이터스가있으면 성공
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9061%'
                ) THEN 'Completed'
                WHEN EXISTS ( -- 성공 없는데 실패 스테이터스가있으면 실패
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9062%'
                ) THEN 'Cancelled'
                WHEN EXISTS ( -- 성공 없는데 실패 스테이터스가있으면 실패
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9043%'
                ) THEN 'Cancelled'
                WHEN EXISTS ( -- 실패, 성공 없는데 진행 스테이터스가있으면 진행
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9041%'
                ) THEN 'In Progress'
                WHEN EXISTS ( -- 진행 없는데 오류 스테이터스가있으면 실패
                    SELECT 1
                    FROM $dbName.MarketDepositLog
                    WHERE invoiceIDunique = A.invoiceIDunique
                    AND marketCode = A.marketCode
                    AND statusIDX LIKE '9042%'
                ) THEN 'Cancelled'
                ELSE MAX(statusIDX)
            END AS status,
            COALESCE(MAX(CASE WHEN statusIDX = 906102 THEN createTime END), '-') AS appTime,
            CASE
                WHEN MAX(orderLocation) = 2 THEN 'app'
                WHEN MAX(orderLocation) = 3 THEN 'web'
                ELSE '-'
            END AS orderLocation
            FROM $dbName.MarketDepositLog AS A
            WHERE (createTime BETWEEN '$startDate' AND '$endDate')
            GROUP BY invoiceIDunique, marketCode
            HAVING invoiceIDunique <> ''
        ");

        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ApiDepositCon 솔팅별 데이터 리셋 deposit 제대로 파악 후 작업
    public static function GetTotalSortingData($data=null)
    {
        $columnsVal=$data['columnsVal'];
        $firstString = $columnsVal[0];

        $firstArray = explode('|', $firstString);

        $firstQuery = 'AND A.marketCode IN (';


        if (!empty($firstArray)) {
            $count = count($firstArray);
            foreach ($firstArray as $index => $key) {

                $value = "'" . $key . "'"; // 작은 따옴표 추가
                $firstQuery .= $value;
                if ($index < $count - 1) {
                    $firstQuery .= ',';
                }
            }
            $firstQuery .= ')';
            if($key==''){
                $firstQuery = ''; // 빈 문자열로 설정
            }
        }

        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }


        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->prepare("SELECT
            IFNULL(SUM(CASE WHEN A.statusIDX LIKE '9061%' THEN 1 ELSE 0 END), 0) AS successTotalCount,
            IFNULL(SUM(CASE
                WHEN A.statusIDX LIKE '9062%' THEN 1
                WHEN A.statusIDX LIKE '9043%' THEN 1
                WHEN A.statusIDX LIKE '9042%' THEN 1
            ELSE 0 END), 0) AS failTotalCount,
            IFNULL(SUM(CASE WHEN A.statusIDX LIKE '9061%' THEN A.amount ELSE 0 END), 0) AS successAmount,
            IFNULL(SUM(CASE
                WHEN A.statusIDX LIKE '9062%' THEN A.amount
                WHEN A.statusIDX LIKE '9043%' THEN A.amount
                WHEN A.statusIDX LIKE '9042%' THEN A.amount
            ELSE 0 END), 0) AS failureAmount
        FROM $dbName.MarketDepositLog AS A
        WHERE A.createTime BETWEEN '$startDate' AND '$endDate'
        ".$firstQuery."
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetTargetDepositData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
            idx,
            ci,
            marketCode,
            marketUserEmail,
            amount,
            invoiceID,
            invoiceIDunique,
            statusIDX,
            ex1,
            ex2,
            ex3,
            log,
            marketFeePer,
            IF(MAX(balance) = 0, 0, amount - MAX(balance)) AS marketFeeCash,
            clientFeePer,
            clientFee,
            exchangeRate,
            MAX(mileage) AS mileage,
            MAX(afterMileage) AS afterMileage,
            MAX(mileage) + MAX(afterMileage) AS beforeMileage,
            CASE
                WHEN statusIDX LIKE '9061%' THEN '성공'
                WHEN statusIDX LIKE '9062%' THEN '실패'
                WHEN statusIDX LIKE '9043%' THEN '실패'
                WHEN statusIDX LIKE '9051%' THEN '진행중'
                WHEN statusIDX LIKE '9041%' THEN '진행중'
                WHEN statusIDX LIKE '9042%' THEN '실패'
                ELSE statusIDX
            END AS status,
            CASE
                WHEN statusIDX LIKE '9061%' THEN '000'
                ELSE statusIDX
            END AS errorCode,
            CONCAT( -- 디파짓 신청한 UTC 시간
                COALESCE(
                    CONCAT('UTC : ',
                        (
                            SELECT createTime
                            FROM $dbName.MarketDepositLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 904101
                            ORDER BY createTime DESC
                            LIMIT 1
                        )
                    ), (
                        SELECT CONCAT('UTC : ', MIN(createTime))
                        FROM $dbName.MarketDepositLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                    ), '-'
                )
            ) AS utcCreateTime,
            COALESCE( -- 디파짓 신청한 KST 시간
                CONCAT('KST : ', DATE_ADD(
                    COALESCE(
                        (
                            SELECT createTime
                            FROM $dbName.MarketDepositLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 904101
                            ORDER BY createTime DESC
                            LIMIT 1
                        ), (
                            SELECT MIN(createTime)
                            FROM $dbName.MarketDepositLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                        )
                    ), INTERVAL 9 HOUR)
                ), '-') AS kstCreateTime,
            CONCAT( -- 디파짓 성공한 UTC 시간
                COALESCE(
                    CONCAT('UTC : ',
                        (
                            SELECT createTime
                            FROM $dbName.MarketDepositLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 906101
                            ORDER BY createTime DESC
                            LIMIT 1
                        )
                    ), '-'
                )
            ) AS utcAppTime,
            COALESCE( -- 디파짓 성공한 KST 시간
                CONCAT('KST : ', DATE_ADD(
                    (
                        SELECT createTime
                        FROM $dbName.MarketDepositLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                        AND statusIDX = 906101
                        ORDER BY createTime DESC
                        LIMIT 1
                    ), INTERVAL 9 HOUR)
            ), '-') AS kstAppTime,

            CONCAT( -- 디파짓 콜백 성공한 UTC 시간
                COALESCE(
                    CONCAT('UTC : ',
                        (
                            SELECT createTime
                            FROM $dbName.MarketDepositLog
                            WHERE marketCode = A.marketCode
                            AND invoiceIDunique = A.invoiceIDunique
                            AND statusIDX = 906102
                            ORDER BY createTime DESC
                            LIMIT 1
                        )
                    ), '-'
                )
            ) AS utcCallBackTime,
            COALESCE( -- 디파짓 콜백 성공한 KST 시간
                CONCAT('KST : ', DATE_ADD(
                    (
                        SELECT createTime
                        FROM $dbName.MarketDepositLog
                        WHERE marketCode = A.marketCode
                        AND invoiceIDunique = A.invoiceIDunique
                        AND statusIDX = 906102
                        ORDER BY createTime DESC
                        LIMIT 1
                    ), INTERVAL 9 HOUR)
            ), '-') AS kstCallBackTime
        FROM $dbName.MarketDepositLog AS A
        WHERE idx = '$targetIDX'

        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetTotalCountData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $Sel = $db->prepare("SELECT
            marketCode,
            invoiceID,
            MAX(statusIDX) as statusIDX,
            COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = '906101' THEN idx END) AS successTotalCount,
            COUNT(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND (statusIDX LIKE '9042%' OR statusIDX LIKE '9062%') THEN idx END) AS failTotalCount,
            IFNULL(ROUND(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = '906101' THEN amount ELSE 0 END), 2), 0) AS successAmount,
            IFNULL(ROUND(SUM(CASE WHEN createTime BETWEEN '$startDate' AND '$endDate' AND statusIDX = '906101' THEN clientFee ELSE 0 END), 2), 0) AS successClientFee
            FROM $dbName.MarketDepositLog
            WHERE createTime BETWEEN '$startDate' AND '$endDate'
            ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetLogList($data=null)
    {
        $invoiceID  = $data['invoiceID'];
        $marketCode = $data['marketCode'];
        $whereType  = $data['whereType'];
        $whereArr   = $data['whereArr'];

        $whereQuery = '';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
                    $endRange = $whereArr['end'].'999';
                    $whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;

        $Sel = $db->query("SELECT
        A.idx,
        A.statusIDX,
        A.createTime,
        A.log,
        B.memo
        FROM $dbName.MarketDepositLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        WHERE A.invoiceIDunique = '$invoiceID'
        AND A.marketCode = '$marketCode'
        $whereQuery
        ORDER BY A.createTime DESC;
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);

        // 로그에서 데이터뿌릴때 ' 를 " 로 변경
        foreach ($result as &$row) {
            $row['log'] = str_replace("'", '"', $row['log']);
        }

        return $result;
    }

    public static function GetCompleteLog()
    {
        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;

        $query = $db->prepare("SELECT
            idx,
            marketCode,
            amount,
            invoiceID,
            marketUserEmail,
            mileage,
            afterMileage,
            mileage + afterMileage AS beforeMileage,
            statusIDX,
            createTime,
            CASE
                WHEN orderLocation = 2 THEN 'app'
                WHEN orderLocation = 3 THEN 'web'
                ELSE '-'
            END AS orderLocation
            FROM ebuyAPI.MarketDepositLog
            WHERE statusIDX = 906101
            GROUP BY invoiceIDunique, marketCode
				ORDER BY idx DESC
        ");

        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
