<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MarketManagerPermissionMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */


    public static function GetPermissionList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query(" SELECT
            IF(A.abled = 'Y', 'O', 'X') AS permission,
            CONCAT(
                CASE WHEN B.momIDX = 0 THEN B.content ELSE (SELECT content FROM $dbName.MarketManagerPermissionLabel WHERE idx = B.momIDX) END
            ) AS title,
            B.content AS content,
            C.name AS gradeName
        FROM $dbName.MarketManagerPermission AS A
        INNER JOIN $dbName.MarketManagerPermissionLabel AS B ON A.permissionLabelIDX = B.idx
        INNER JOIN $dbName.MarketManagerGrade AS C ON A.gradeIDX = C.idx
        WHERE B.momIDX != 0
        ORDER BY
        CASE WHEN B.momIDX = 0 THEN B.seq ELSE (SELECT seq FROM $dbName.MarketManagerPermissionLabel WHERE idx = B.momIDX) END, B.seq , C.idx;
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetPermissionForEditMomList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query(" SELECT
            idx AS momIDX,
            seq AS momSeq,
            content AS momContent
        FROM $dbName.MarketManagerPermissionLabel
        WHERE momIDX = 0
        ORDER BY seq
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetPermissionForEditChildList($data=null)
    {
        $momIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query(" SELECT
            idx AS childIDX,
            seq AS childSeq,
            content AS childContent
        FROM $dbName.MarketManagerPermissionLabel
        WHERE momIDX = '$momIDX'
        ORDER BY seq
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //등급별권한불러오기
    public static function GetGradePermissionList($data=null)
    {
        $pageType=$data['pageType'];
        $gradeIDX=$data['gradeIDX'];
        $query='';
        $select='';
        if($pageType=='upd'){
            $select='C.name AS gradeName';
            $query=' AND A.gradeIDX='.$gradeIDX;
        }else if($pageType=='ins'){
            $select='MIN(B.seq) AS min_idx';
            $query=' GROUP BY B.content';
        };
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query(" SELECT
            A.abled,
            CONCAT(
                CASE WHEN B.momIDX = 0 THEN B.content ELSE (SELECT content FROM $dbName.MarketManagerPermissionLabel WHERE idx = B.momIDX) END,  ' : ',
                B.content
            ) AS content,
            B.idx,
            $select
        FROM $dbName.MarketManagerPermission AS A
        INNER JOIN $dbName.MarketManagerPermissionLabel AS B ON A.permissionLabelIDX = B.idx
        INNER JOIN $dbName.MarketManagerGrade AS C ON A.gradeIDX = C.idx
        WHERE B.momIDX != 0 $query
        ORDER BY
        CASE WHEN B.momIDX = 0 THEN B.seq ELSE (SELECT seq FROM $dbName.MarketManagerPermissionLabel WHERE idx = B.momIDX) END, B.seq
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}
