<?php

namespace App\Models;

use PDO;

class SystemMo extends \Core\Model
{



    //정보 불러오기
    public static function GetSystemData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        memo,
        status,
        value
        FROM $dbName.System
        ");
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    public static function GetSystemStatusInfo($data=null)
    {
        $idx=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
            idx,
            status,
            value
            FROM $dbName.System
            WHERE idx='$idx'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //20240115 ptp 입금신청할때 APP서버에서 훔ㅊ쳐왓음..
    public static function getSystemStatusInfo_deposit($data=null)
    {
        $dbName= self::MainDBName;
        $idx=$data;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
            idx,
            CASE
                WHEN idx = 6 AND status = 2 THEN 203101 -- 입금신청수동
                WHEN idx = 6 AND status = 3 THEN 203102 -- 입금신청자동
                WHEN idx = 6 AND status = 4 THEN 203103 -- 입금신청가상계좌
                WHEN idx = 7 AND status = 2 THEN 204101 -- 출금신청수동
                WHEN idx = 7 AND status = 3 THEN 204102 -- 출금신청자동
                ELSE status
            END AS status,
            value
            FROM $dbName.System
            WHERE idx='$idx'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}