<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MarketBalanceMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
        public static function getBalance($data=null)
    {
        //20231011 417212 정산을 마켓매니저가 취소하는건데~~~ 없애자~~~
        $marketCode = $data;
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
            COALESCE((
                SUM(
                    CASE
                        /*20240122 421101 마이그레이션 구 피센터 발란스 이전으로 인한 증감*/
                        WHEN A.statusIDX IN (418201,417211,419101,906101,420101,421101) THEN A.amount
                        ELSE 0
                    END
                ) - SUM(
                    CASE
                        WHEN A.statusIDX IN (417101, 417102, 417103,419201,908102,420201) THEN A.amount
                        ELSE 0
                    END
                )
            ), 0) AS balance
            FROM $dbName.MarketBalance AS A
            INNER JOIN $dbName.Market AS B ON A.marketIDX = B.idx
            WHERE B.code='$marketCode'
        ");
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        if(isset($result['balance']) && $result['balance'] !=null){
            return $result['balance'];
        }else{ return 0;}
    }

    //MarketBalanceHistoryCon 데이터테이블
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.createTime,
        CASE
            WHEN A.statusIDX IN(417211,418201,419101,906101,420101,421101) THEN '증감'
            WHEN A.statusIDX IN(417101,417102,417103,419201,908102,420201) THEN '차감'
            ELSE '-'
        END AS type,
        CASE
            WHEN A.statusIDX IN(417101,417102,417103) THEN '정산신청'
            WHEN A.statusIDX = 417211 THEN '정산 실패(스태프)'
            WHEN A.statusIDX = 418201 THEN '역정산 입금완료'
            WHEN A.statusIDX = 419101 THEN '스태프 발란스 증감'
            WHEN A.statusIDX = 419201 THEN '스태프 발란스 차감'
            WHEN A.statusIDX = 906101 THEN '지불대행 성공'
            WHEN A.statusIDX = 908102 THEN '출금대행 성공'
            WHEN A.statusIDX = 420101 THEN '포탈 Lv5 발란스 이전으로 증감'
            WHEN A.statusIDX = 420201 THEN '포탈 Lv5 발란스 이전으로 차감'
            WHEN A.statusIDX = 421101 THEN '시스템에 의한 증감'
            ELSE '-'
        END AS content,
        A.amount,
        C.name AS partnerName,
        C.code AS partnerCode,
        B.name AS marketName,
        B.code AS marketCode,
        E.ex
        FROM $dbName.MarketBalance AS A
        INNER JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        LEFT JOIN $dbName.Partner AS C ON B.partnerIDX = C.idx
        LEFT JOIN $dbName.PortalLog AS D ON A.idx = D.targetIDX AND D.statusIDX = 419102
        LEFT JOIN $dbName.PortalLogEx AS E ON D.idx = E.logIDX
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    




    // public static function getBalanceHistory($data=null)
    // {
    //     //20231011 417212 정산을 마켓매니저가 취소하는건데~~~ 없애자~~~
    //     $marketCode = $data;
    //     $dbName= self::MainDBName;
    //     $db = static::GetDB();
    //     $query = $db->prepare("SELECT
    //         A.idx,
    //         A.amount,
    //         A.createTime,
    //         -- afterbalance 구하기
    //         SUM(CASE
    //             WHEN A.statusIDX IN (418201,417211) THEN A.amount
    //             WHEN A.statusIDX IN (417101, 417102, 417103) THEN -A.amount
    //             ELSE 0
    //         END) OVER (ORDER BY A.createTime ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS afterBalance
    //         -- afterbalance 구하기


    //         FROM $dbName.MarketBalance AS A
    //         INNER JOIN $dbName.Market AS B ON A.marketIDX = B.idx
    //         WHERE B.code='$marketCode'
    //         ORDER BY A.createTime DESC
    //     ");
    //     $query->execute();
    //     $result=$query->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }



}
