<?php

namespace App\Models;

use PDO;

class ClientAllAmountMo extends \Core\Model
{

    //ClientDeviceCon 데이터테이블
    public static function ClientAllAmountData($data=null)
    {
        $clientIDX  = $data;
        $dbName = self::MainDBName;
        $dataDbKey = self::dataDbKey;
        $db     = static::GetDB();
        $query = $db->prepare("SELECT
        A.idx
        ,A.amount
        ,A.statusIDX
        ,A.createTime
        FROM $dbName.ClientAllAmount AS A
        WHERE A.clientIDX = :clientIDX
        ORDER BY A.createTime DESC
        ");
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    public static function ClientAllAmountSumData($data=null)
    {
        $clientIDX  = $data;
        $dbName = self::MainDBName;
        $dataDbKey = self::dataDbKey;
        $db     = static::GetDB();
        $query = $db->prepare("SELECT
        SUM(A.amount) AS AllAmountSum
        FROM $dbName.ClientAllAmount AS A
        WHERE A.clientIDX = :clientIDX
        GROUP BY A.clientIDX
        ");
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }







}