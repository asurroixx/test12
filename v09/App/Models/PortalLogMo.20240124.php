<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class PortalLogMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//MarketSettlementCon wallet 로그 정보
	public static function GetWalletLogData($data=null)
	{
		$marketIDX=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$Sel = $db->prepare("SELECT
		A.idx,
		(SELECT
            CASE idx
                WHEN 425101 THEN '지갑등록'
                WHEN 425201 THEN '지갑수정'
                WHEN 425301 THEN '지갑삭제'
                WHEN 417101 THEN 'crypto 정산신청'
                WHEN 417201 THEN '정산완료'
                WHEN 417211 THEN '정산스태프취소'
                WHEN 417212 THEN '정산마켓매니저취소'
                WHEN 418101 THEN 'crypto 역정산신청'
                WHEN 418101 THEN '역정산완료'
                WHEN 418111 THEN '역정산스태프취소'
                WHEN 418112 THEN '역정산마켓매니저취소'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
		A.createTime,
		IFNULL(B.ex,'-') AS ex,
		C.walletAddr,
        AES_DECRYPT(E.email, :dataDbKey) AS email
		FROM $dbName.PortalLog AS A
		LEFT JOIN $dbName.PortalLogEx AS B ON A.idx=B.logIDX
		LEFT JOIN $dbName.MarketSettlementCryptoWallet AS C ON A.targetIDX=C.idx AND A.statusIDX IN (425101, 425201, 425301)
        LEFT JOIN $dbName.MarketManager AS D ON A.marketManagerIDX = D.idx
        JOIN $dbName.Manager AS E ON D.managerIDX=E.idx
		WHERE C.marketIDX=:marketIDX AND A.statusIDX IN (425101,425201,425301,417101,417201,417211,417212,418101,418101,418111,418112)
		");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':marketIDX', $marketIDX);
        $Sel->execute();
		$result=$Sel->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MarketSettlementCon bankwire 로그 정보
	public static function GetBankwireLogData($data=null)
	{
		$marketIDX=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$Sel = $db->prepare("SELECT
		A.idx,
		(SELECT
            CASE idx
                WHEN 426101 THEN 'bankwire 등록'
                WHEN 426201 THEN 'bankwire 수정'
                WHEN 417102 THEN 'bankwire 정산신청'
                WHEN 417201 THEN '정산완료'
                WHEN 417211 THEN '정산스태프취소'
                WHEN 417212 THEN '정산마켓매니저취소'
                WHEN 418102 THEN 'bankwire 역정산신청'
                WHEN 418101 THEN '역정산완료'
                WHEN 418111 THEN '역정산스태프취소'
                WHEN 418112 THEN '역정산마켓매니저취소'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
		A.createTime,
		IFNULL(B.ex,'-') AS ex,
		C.bankName,
        AES_DECRYPT(E.email, :dataDbKey) AS email
		FROM $dbName.PortalLog AS A
		LEFT JOIN $dbName.PortalLogEx AS B ON A.idx=B.logIDX
		LEFT JOIN $dbName.MarketSettlementBankwire AS C ON A.targetIDX=C.idx AND A.statusIDX IN (426101, 426201)
        LEFT JOIN $dbName.MarketManager AS D ON A.marketManagerIDX = D.idx
        JOIN $dbName.Manager AS E ON D.managerIDX=E.idx
		WHERE C.marketIDX=:marketIDX AND A.statusIDX IN (426101,426201,417102,417201,417211,417212,418102,418101,418111,418112)
		");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':marketIDX', $marketIDX);
        $Sel->execute();
		$result=$Sel->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//MarketSettlementCon krw 로그 정보
	public static function GetKrwLogData($data=null)
	{
		$marketIDX=$data;
		$db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
		$Sel = $db->prepare("SELECT
		A.idx,
		(SELECT
            CASE idx
                WHEN 427101 THEN 'KRW account 등록'
                WHEN 427201 THEN 'KRW account 수정'
                WHEN 417103 THEN 'KRW 정산신청'
                WHEN 417201 THEN '정산완료'
                WHEN 417211 THEN '정산스태프취소'
                WHEN 417212 THEN '정산마켓매니저취소'
                WHEN 418103 THEN 'KRW 역정산신청'
                WHEN 418101 THEN '역정산완료'
                WHEN 418111 THEN '역정산스태프취소'
                WHEN 418112 THEN '역정산마켓매니저취소'
                ELSE '' END
            AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
		A.createTime,
		IFNULL(B.ex,'-') AS ex,
		C.beneficiaryName,
        AES_DECRYPT(E.email, :dataDbKey) AS email
		FROM $dbName.PortalLog AS A
		LEFT JOIN $dbName.PortalLogEx AS B ON A.idx=B.logIDX
		LEFT JOIN $dbName.MarketSettlementKRW AS C ON A.targetIDX=C.idx AND A.statusIDX IN (427101, 427201)
        LEFT JOIN $dbName.MarketManager AS D ON A.marketManagerIDX = D.idx
        JOIN $dbName.Manager AS E ON D.managerIDX=E.idx
		WHERE C.marketIDX=:marketIDX AND A.statusIDX IN (427101,427201,417102,417201,417211,417212,418102,418101,418111,418112)
		");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':marketIDX', $marketIDX);
        $Sel->execute();
		$result=$Sel->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	public static function GetCompleteStaffInfoData($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $statusIDX=$data['statusIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(B.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(B.name, :dataDbKey) AS staffName
        FROM $dbName.PortalLog AS A
        INNER JOIN $dbName.Staff AS B ON A.staffIDX=B.idx
        WHERE A.statusIDX =:statusIDX AND A.targetIDX =:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':statusIDX', $statusIDX);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetMarketManagerInfoData($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $statusIDX=$data['statusIDX'];
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(C.email, :dataDbKey) AS managerEmail
        FROM $dbName.PortalLog AS A
        INNER JOIN $dbName.MarketManager AS B ON A.marketManagerIDX=B.idx
        INNER JOIN $dbName.Manager AS C ON B.managerIDX=C.idx
        WHERE A.statusIDX =:statusIDX AND A.targetIDX =:targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':statusIDX', $statusIDX);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetLogList($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $whereType=$data['whereType'];
        $whereArr=$data['whereArr'];
        $whereQuery='';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                	$arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
            		$endRange = $whereArr['end'].'999';
            		$whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        -- DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime,
        A.marketManagerIDX,
        A.staffIDX,
        B.memo,
        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,
        AES_DECRYPT(E.email, :dataDbKey) AS mangerEmail,
        AES_DECRYPT(D.name, :dataDbKey) AS mangerName,
        F.ex
        FROM $dbName.PortalLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.MarketManager AS D ON A.marketManagerIDX=D.idx
        LEFT JOIN $dbName.Manager AS E ON D.managerIDX=E.idx
        LEFT JOIN $dbName.PortalLogEx AS F ON A.idx=F.logIDX
        WHERE A.targetIDX =$targetIDX$whereQuery
        ORDER BY A.createTime DESC;
        ");

        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    
}