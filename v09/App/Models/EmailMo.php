<?php

namespace App\Models;

use PDO;

class EmailMo extends \Core\Model
{

    //QnaCon 날짜별 데이터 리셋
    public static function getReadyToSendEmail($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->prepare("SELECT
            idx,
            email,
            con,
            targetIDX,
            statusIDX
        FROM $dbName.Email
        WHERE A.sendStatusIDX = 150201");
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }




}