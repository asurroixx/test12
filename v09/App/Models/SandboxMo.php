<?php

namespace App\Models;

use PDO;

class SandboxMo extends \Core\Model
{
    //SandboxMarketCon 마켓리스트
    public static function MarketList($data=null)
    {
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        createTime
        FROM $dbName.Market
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //SandboxMarketCon 데이터테이블
    public static function DatatableMarket($data=null)
    {
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.code,
        A.name,
        A.createTime,
        B.name AS partnerName,
        B.code AS partnerCode
        FROM $dbName.Market AS A
        JOIN $dbName.Partner AS B ON A.partnerIDX=B.idx
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //SandboxMarketManagerCon 데이터테이블
    public static function DatatableMarketManager($data=null)
    {
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
        A.createTime,
        AES_DECRYPT(A.name,:dataDbKey) AS marketManagerName,
        A.position,
        A.gradeIDX,
        B.code,
        B.name AS marketName,
        AES_DECRYPT(C.email,:dataDbKey) AS email,
        D.name AS gradeName,
        E.name AS parterName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        JOIN $dbName.Manager AS C ON A.managerIDX=C.idx
        JOIN $dbName.MarketManagerGrade AS D ON A.gradeIDX=D.idx
        JOIN $dbName.Partner AS E ON B.partnerIDX=E.idx
        WHERE A.statusIDX IN (402101 , 402201)
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    //SandboxMarketManagerCon 디테일
    public static function DetailMarketManager($data=null)
    {
        $idx=$data;
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.marketIDX,
        A.managerIDX,
        A.position,
        AES_DECRYPT(A.name,:dataDbKey) AS marketManagerName,
        A.createTime,
        B.code,
        B.name AS marketName,
        AES_DECRYPT(C.email,:dataDbKey) AS email,
        E.name AS parterName,
        E.code AS parterCode
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Market AS B ON A.marketIDX=B.idx
        JOIN $dbName.Manager AS C ON A.managerIDX=C.idx
        JOIN $dbName.MarketManagerGrade AS D ON A.gradeIDX=D.idx
        JOIN $dbName.Partner AS E ON B.partnerIDX=E.idx
        WHERE A.idx='$idx' AND A.statusIDX IN (402101 , 402201)
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //SandboxMarketManagerCon 디테일 매니저 로그인 히스토리
    public static function GetLoginHisList($data=null)
    {
        $loginIDX=$data;
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $query = $db->prepare("SELECT
            ROW_NUMBER() OVER (ORDER BY createTime ASC) AS no,
            IFNULL(createTime, '-') AS loginTime,
            ipAddress
        FROM $dbName.ManagerLoginHistory
        WHERE loginIDX= :loginIDX
        ORDER BY createTime DESC
        ");
        $query->bindValue(':loginIDX', $loginIDX);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //SandboxMarketManagerCon 해당 매니저 최근로그인
    public static function GetRecentLoginTime($data=null)
    {
        $loginIDX=$data;
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $query = $db->prepare("SELECT
            IFNULL(MAX(createTime), '-') AS recentLoginTime
        FROM $dbName.ManagerLoginHistory
        WHERE loginIDX= :loginIDX
        ");
        $query->bindValue(':loginIDX', $loginIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }



    //SandboxManagerLoginHistoryCon 데이터테이블
    public static function DatatableManagerLoginHistory($data=null)
    {
        $startDate   = $data['startDate'] ?? '1970-01-01';
        $endDate     = $data['endDate'] ?? date('Y-m-d');
        $startDate  .= ' 00:00:00';
        $endDate    .= ' 23:59:59';
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
            A.idx,
            ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
            IFNULL(A.createTime, '-') AS loginTime,
            A.ipAddress,
            B.idx AS managerIDX,
            AES_DECRYPT(B.email,:dataDbKey) AS email
        FROM $dbName.ManagerLoginHistory AS A
        JOIN $dbName.Manager AS B ON A.loginIDX=B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ORDER BY A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //SandboxManagerLoginHistoryCon 디테일
    public static function DetailManagerLoginHistory($data=null)
    {
        $managerIDX=$data;
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        CASE WHEN A.statusIDX = 402101 THEN '활성'
             WHEN A.statusIDX = 402201 THEN '비활성'
             WHEN A.statusIDX = 402202 THEN '삭제'
             ELSE 'unknown'
        END AS statusName,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        C.name AS marketName,
        C.code AS marketCode,
        D.name AS parterName
        FROM $dbName.MarketManager AS A
        JOIN $dbName.Manager AS B ON A.managerIDX=B.idx
        JOIN $dbName.Market AS C ON A.marketIDX=C.idx
        JOIN $dbName.Partner AS D ON C.partnerIDX=D.idx
        WHERE A.managerIDX=:managerIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':managerIDX', $managerIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



    public static function GetSandboxMarketAllApiData($data=null)
    {
        $startDate  = $data['startDate'] ?? '1970-01-01';
        $endDate    = $data['endDate'] ?? date('Y-m-d');
        $startDate .= ' 00:00:00';
        $endDate   .= ' 23:59:59';

        // $db = static::GetApiDB();
        // $dbName= self::EbuyApiDBName;
        $db = static::GetSandboxDB();
        $dbName= self::EbuySandboxDBName;
        $query = $db->prepare("SELECT
        idx,
        log,
        marketCode,
        ip,
        action,
        type,
        createTime
        FROM ebuyAPI.AllApiLog
        WHERE (createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        // 로그에서 데이터뿌릴때 ' 를 " 로 변경
        foreach ($results as &$row) {
            $row['log'] = str_replace("'", '"', $row['log']);
        }

        return $results;
    }
}