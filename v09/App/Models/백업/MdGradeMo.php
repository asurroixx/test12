<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MdGradeMo extends \Core\Model
{
    public static function getGradeList($data=null)
    {
        $subUrlIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,name,subUrlIDX
        FROM sendipay.MdGrade
        WHERE subUrlIDX='$subUrlIDX'");
        $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Grade;
    }

    public static function GetStaffNameList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,name
        FROM sendipay.MdGrade
        WHERE subUrlIDX='9'");
        $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Grade;
    }//스태프관리 솔팅시작
}
