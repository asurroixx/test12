<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class WithdrawalMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
    //StWithdrawalCon 총 카운트
    public static function GetTotalCount($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM sendipay.Withdrawal
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        $totalCount=$globalVal['count'];
        return $totalCount;
    }

    //StWithdrawalCon 데이터테이블
    public static function GetWithdrawalList($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        IFNULL(A.contractOrderID,'-') AS contractOrderID,
        A.amount,
        A.fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        IFNULL(A.contractUserID,'-') AS contractUserID,
        B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(D.name,'-') AS contractName
        FROM sendipay.Withdrawal AS A
        JOIN sendipay.WithdrawalTime AS B ON A.idx = B.WithdrawalIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        WHERE (B.createTime BETWEEN '$startDate' AND '$endDate')
        GROUP BY A.idx
        "
        );
        $withdrawal=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $withdrawal;
    }

    ////StWithdrawalCon 디테일
    public static function get_withdrawalForm($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        IFNULL(A.contractOrderID,'-') AS contractOrderID,
        IFNULL(A.customOrderID1,'-') AS customOrderID1,
        IFNULL(A.customOrderID2,'-') AS customOrderID2,
        A.amount,
        A.fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        IFNULL(A.contractUserID,'-') AS contractUserID,
        A.dealResultIDX,
        A.staffEx,
         B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(C.code,'-') AS dealCode,
        IFNULL(C.name,'-') AS dealCodeName,
        IFNULL(D.name,'-') AS contractName
        FROM sendipay.Withdrawal AS A
        JOIN sendipay.WithdrawalTime AS B ON A.idx = B.withdrawalIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        WHERE A.idx='$targetIDX'
        ");
        $withdrawalForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $withdrawalForm;
    }

    // //솔팅
    // public static function WithdrawalListLoad($data=null)
    // {
    //     $startDate=$data['startDate'];
    //     $endDate=$data['endDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.status,
    //     A.amount
    //     FROM sendipay.Withdrawal AS A
    //     LEFT JOIN sendipay.WithdrawalTime AS B
    //     ON(A.idx=B.withdrawalIDX)
    //     LEFT JOIN sendipay.Contract AS C
    //     ON(A.contractIDX=C.idx)
    //     WHERE (B.createTime BETWEEN '$startDate' AND '$endDate')
    //     ");
    //     $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }

    // public static function get_withdrawalByInvoiceId($data=null)
    // {
    //     $contractIDX=$data['contractIDX'];
    //     $contractOrderID=$data['invoice_id'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.status
    //     FROM sendipay.Withdrawal AS A
    //     WHERE A.contractOrderID='$contractOrderID' AND contractIDX='$contractIDX'
    //     ");
    //     $withdrawalForm=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $withdrawalForm;
    // }
    // public static function GetDashboardListLoad($data=null)
    // {
    //     $contractCode=$data['contractCode'];

    //     $startDate=$data['startDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     $endDate=$data['endDate'];
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.contractUserID,
    //     A.amount,
    //     C.completeTime,
    //     COUNT(A.idx) AS idxCount
    //     FROM sendipay.Withdrawal AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     LEFT JOIN sendipay.WithdrawalTime AS C
    //     ON (A.idx = C.withdrawalIDX)
    //     WHERE (C.completeTime BETWEEN '$startDate' AND '$endDate')
    //     AND B.code='$contractCode'
    //     AND A.status=4
    //     GROUP BY A.idx
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    // public static function GetContractTotalAmount($data=null)
    // {
    //     $contractCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.amount
    //     FROM sendipay.Withdrawal AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     WHERE B.code='$contractCode' AND A.status=4
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }
}