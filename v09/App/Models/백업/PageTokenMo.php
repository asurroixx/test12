<?php

namespace App\Models;

use PDO;

class PageTokenMo extends \Core\Model
{

    //Router,GlobalVariable 로그인한 스태프 마지막 토큰값
    public static function GetChkToken($data=null)
    {
        $sessionIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        token
        FROM sendipay.PageToken
        WHERE staffIDX='$sessionIDX' ORDER BY idx DESC LIMIT 1
        ");
        $langCate=$Sel->fetch(PDO::FETCH_ASSOC);
        return $langCate;
    }

    //Router,GlobalVariable get으로 받은 토큰값
    public static function GetTokenData($data=null)
    {
        $token=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        token,
        ipAddress,
        loginIDX
        FROM sendipay.PageToken
        WHERE token='$token'
        ");
        $langCate=$Sel->fetch(PDO::FETCH_ASSOC);
        return $langCate;
    }


}