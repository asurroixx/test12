<?php

namespace App\Models;

use PDO;

class QnaQuestionToAdminMo extends \Core\Model
{

    public static function GetAdminQnaList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.ticketCode,
        A.status,
        A.createTime,
        A.updateTime,
        B.idx AS targetIDX,
        B.title,
        C.questionIDX,
        D.email AS staffEmail
        FROM sendipay.QnaQuestionToAdmin AS A
        LEFT JOIN sendipay.QnaQuestion AS B
        ON(A.ticketCode=B.ticketCode)
        LEFT JOIN sendipay.QnaAnswer AS C
        ON(B.idx=C.questionIDX)
        LEFT JOIN sendipay.Staff AS D
        ON(A.staffIDX=D.idx)
        WHERE C.type=4 OR C.type=5 GROUP BY C.questionIDX
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    public static function issetTicketCodeData($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        ticketCode
        FROM sendipay.QnaQuestionToAdmin
        WHERE ticketCode='$ticketCode'
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    



}