<?php

namespace App\Models;

use PDO;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class SettlementMo extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    //StSettlementCon 총 카운트
    public static function GetTotalCount($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM sendipay.Settlement
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        $totalCount=$globalVal['count'];
        return $totalCount;
    }

    //StSettlementCon 데이터테이블
    public static function GetTableDataSel($data=null)
    {   
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];


        if($startDate==""){ 
            $startDate='1970-01-01 00:00:00'; 
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){ 
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }

        $db = static::getDB();
        $documentSel = $db->query("SELECT 
        A.idx,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        CASE A.type
            WHEN 2 THEN 'Tether'
            WHEN 3 THEN 'Ether'
            WHEN 4 THEN 'USDC'
            ELSE '' END
        AS type,
        A.fee,
        A.decreaseBalance,
        A.receiveBalance,
        A.receiveAmount,
        A.createTime,
        CASE A.completeTime
            WHEN A.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE A.completeTime END
        AS completeTime,
        B.name AS contractName,
        B.code,
        C.email AS managerEmail
        FROM sendipay.Settlement AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        LEFT JOIN sendipay.Manager AS C ON A.managerIDX = C.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $customerFetch=$documentSel->fetchAll(PDO::FETCH_ASSOC);
        return $customerFetch;
    }

    //StSettlementCon 디테일
    public static function get_settlementForm($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.invoiceID,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        CASE A.type
            WHEN 2 THEN 'Tether'
            WHEN 3 THEN 'Ether'
            WHEN 4 THEN 'USDC'
            ELSE '' END
        AS type,
        A.fee,
        A.decreaseBalance,
        A.receiveBalance,
        A.receiveAmount,
        A.createTime,
        CASE A.completeTime
            WHEN A.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE A.completeTime END
        AS completeTime,
        A.staffEx,
        A.ipAddress,
        B.name AS contractName,
        IFNULL(C.code,'-') AS dealCode,
        IFNULL(C.name,'-') AS dealCodeName,
        D.email AS managerEmail,
        E.walletAddr
        FROM sendipay.Settlement AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX = B.idx
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Manager AS D ON A.managerIDX = D.idx
        LEFT JOIN sendipay.ContractWallet AS E ON A.walletIDX=E.idx
        WHERE A.idx='$targetIDX'
        ");
        $withdrawalForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $withdrawalForm;
    }

    public static function GetSettlementInfoByStatus($data=null)
    {   
        $contractCode = $data;
        $db = static::getDB();
        $documentSel = $db->query("SELECT 
        A.idx
        FROM sendipay.Settlement AS A
        LEFT JOIN sendipay.Contract AS B
        ON(A.contractIDX= B.idx)
        WHERE B.code = '$contractCode' AND (A.status = 2 OR  A.status = 3)
        ");
        $customerFetch=$documentSel->fetch(PDO::FETCH_ASSOC);
        return $customerFetch;
        
    }



    // public static function GetDashboardListLoad($data=null)
    // {
    //     $contractCode=$data['contractCode'];

    //     $startDate=$data['startDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     $endDate=$data['endDate'];
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.receiveBalance,
    //     COUNT(A.idx) AS idxCount
    //     FROM sendipay.Settlement AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     WHERE (A.completeTime BETWEEN '$startDate' AND '$endDate') AND B.code='$contractCode' AND A.status=4 GROUP BY A.idx
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    // public static function GetContractWalletUsed($data=null)
    // {
    //     $contractCode=$data['contractCode'];
    //     $targetIDX=$data['targetIDX'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.walletIDX
    //     FROM sendipay.Settlement AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON(A.contractIDX=B.idx)
    //     WHERE B.code='$contractCode' AND A.walletIDX='$targetIDX'
    //     ");
    //     $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    // public static function GetContractTotalAmount($data=null)
    // {
    //     $contractCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.decreaseBalance
    //     FROM sendipay.Settlement AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     WHERE B.code='$contractCode' AND A.status=4
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    // public static function get_settlementByInvoiceId($data=null)
    // {
    //     $contractIDX=$data['contractIDX'];
    //     $invoiceID=$data['invoice_id'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.invoiceID
    //     FROM sendipay.Settlement AS A
    //     WHERE A.invoiceID='$invoiceID' AND A.contractIDX='$contractIDX'
    //     ");
    //     $withdrawalForm=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $withdrawalForm;
    // }
    
}
