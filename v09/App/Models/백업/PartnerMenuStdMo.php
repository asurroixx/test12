<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class PartnerMenuStdMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function GetPartnerMenuList($data=null)
	{
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		langCode,
		url,
		seq,
		status,
		memo,
		subTitleLangCode

		FROM sendipay.PartnerMenuStd
        WHERE momIDX=0 ORDER BY seq 
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function GetPartnerChildMenuList($data=null)
	{
		$partnerIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		langCode,
		url,
		seq,
		status,
		memo,
		subTitleLangCode
		FROM sendipay.PartnerMenuStd
        WHERE momIDX='$partnerIDX' ORDER BY seq 
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function CheeseEyeAction($data=null)
	{
		$EditItemId=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM sendipay.PartnerMenuStd
        WHERE idx='$EditItemId' ORDER BY seq 
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function PartnerMenuConfirmData($data=null)
	{
		$partnerIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.langCode,
		A.url,
		C.value AS krName
		FROM sendipay.PartnerMenuStd AS A
		LEFT JOIN sendipay.MdLang AS B
		ON (A.langCode = B.code)
		LEFT JOIN sendipay.LangPack AS C
		ON (B.idx = C.langIDX AND C.cateIDX='2')
        WHERE A.idx='$partnerIDX' ORDER BY A.seq
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function PartnerMenuDeleteCheck($data=null)
	{
		$EditItemId=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM sendipay.PartnerMenuStd
        WHERE momIDX='$EditItemId'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	

	
    
}