<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class PortalMenuStdMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
    public static function get_PortalMenuLoad($data=null)
    {   
        $db = static::getDB();
        $Sel = $db->query("SELECT 
        url,
        langCode,
        subTitleLangCode
        FROM sendipay.PortalMenuStd 
        WHERE status=3 ORDER BY seq
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }
}