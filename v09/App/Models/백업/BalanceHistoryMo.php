<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class BalanceHistoryMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

    //StBalanceHistoryCon 총 카운트
    public static function GetTotalCount($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM sendipay.BalanceHistory
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        $totalCount=$globalVal['count'];
        return $totalCount;
    }

    //StBalanceHistoryCon 데이터테이블
    public static function GetStaffBalanceHistoryListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];

        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        format(A.beforeBalance, 2) AS beforeBalance,
        A.amount,
        format(A.afterBalance, 2) AS afterBalance,
        CASE A.dealType
            WHEN 2 THEN '증감결제'
            WHEN 3 THEN '차감출금'
            WHEN 4 THEN '차감정산'
            WHEN 5 THEN '증감정산취소'
            ELSE '' END
        AS dealType,
        A.dealTargetIDX,
        A.createTime,
        B.name AS contractName
        FROM sendipay.BalanceHistory AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX = B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ORDER BY A.createTime DESC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StBalanceHistoryCon 디테일
    public static function GetBalanceHistoryDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        format(A.beforeBalance, 2) AS beforeBalance,
        A.amount,
        format(A.afterBalance, 2) AS afterBalance,
        CASE A.dealType
            WHEN 2 THEN '증감결제'
            WHEN 3 THEN '차감출금'
            WHEN 4 THEN '차감정산'
            WHEN 5 THEN '증감정산취소'
            ELSE '' END
        AS dealType,
        A.dealTargetIDX,
        A.createTime,
        B.code,
        B.name AS contractName,
        B.homepage
        FROM sendipay.BalanceHistory AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $transactionForm;
    }

	public static function GetBalanceHistoryListLoad($data=null)
	{

        $contractCode=$data['contractCode'];
        $beforeDate=$data['beforeDate'];
        $afterDate=$data['afterDate'];

        if($beforeDate==""){ 
            $beforeDate='1970-01-01 00:00:00'; 
        }else{
            $beforeDate.=" 00:00:00";
        }
        if($afterDate==""){ 
            $afterDate=date('Y-m-d 23:59:59');
        }else{
            $afterDate.=" 23:59:59";
        }

        $query=" AND A.createTime BETWEEN '".$beforeDate."' AND '".$afterDate."' ";
        
		$db = static::getDB();
		$Sel = $db->query("SELECT
		A.idx,
        A.contractIDX,
        A.beforeBalance,
        A.amount,
        A.afterBalance,
        A.dealType,
        A.dealTargetIDX,
        A.createTime,
        B.name AS contractName,
        C.contractOrderID AS transactionID,
        D.contractOrderID AS WithdrawalID,
        E.invoiceID AS settlementID
		FROM sendipay.BalanceHistory AS A
        LEFT JOIN sendipay.Contract AS B
        ON (A.contractIDX = B.idx)
        LEFT JOIN sendipay.Transaction AS C
        ON (A.dealTargetIDX = C.idx)
        LEFT JOIN sendipay.Withdrawal AS D
        ON (A.dealTargetIDX = D.idx)
        LEFT JOIN sendipay.Settlement AS E
        ON (A.dealTargetIDX = E.idx)
        WHERE B.code = '$contractCode'
        ".$query."ORDER BY A.createTime DESC");
		$result=$Sel->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

    //솔팅
    public static function SortingListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        dealType,
        amount
        FROM sendipay.BalanceHistory
        WHERE (createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    public static function GetBalanceHistoryDetailListLoad($data=null)
    {

        $targetIDX=$data['targetIDX'];
        $contractCode=$data['contractCode'];
        $dealType=$data['dealType'];
        $thisDealType='';
        if($dealType=='transaction'){
            $thisDealType='C.dealResultIDX';
        }elseif($dealType=='withdrawal'){
            $thisDealType='D.dealResultIDX';
        }else{
            $thisDealType='E.dealResultIDX';
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        A.beforeBalance,
        A.amount,
        A.afterBalance,
        A.dealType,
        A.dealTargetIDX,
        A.createTime,
        B.name AS contractName,
        C.contractOrderID AS transactionID,
        C.customOrderID1 AS TrsEx1,
        C.customOrderID2 AS TrsEx2,
        D.contractOrderID AS WithdrawalID,
        D.customOrderID1 AS WidEx1,
        D.customOrderID2 AS WidEx2,
        E.invoiceID AS settlementID,
        F.code AS dealCode,
        F.name AS dealName
        FROM sendipay.BalanceHistory AS A
        LEFT JOIN sendipay.Contract AS B
        ON (A.contractIDX = B.idx)
        LEFT JOIN sendipay.Transaction AS C
        ON (A.dealTargetIDX = C.idx)
        LEFT JOIN sendipay.Withdrawal AS D
        ON (A.dealTargetIDX = D.idx)
        LEFT JOIN sendipay.Settlement AS E
        ON (A.dealTargetIDX = E.idx)
        LEFT JOIN sendipay.MdDealResult AS F
        ON ($thisDealType=F.idx)
        WHERE B.code = '$contractCode' AND A.idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function get_settlementByInvoiceId($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx
        FROM sendipay.BalanceHistory AS A
        WHERE A.idx='$idx'
        ");
        $withdrawalForm=$Sel->fetch(PDO::FETCH_ASSOC);
        return $withdrawalForm;
    }

}