<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class PartnerAlarmMo extends \Core\Model
{
    public static function PartnerAlarmLoad($data=null)
    {
        $directorIDX = $data['directorIDX'];
        $type= $data['type'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        con
        FROM sendipay.PartnerAlarm 
        WHERE directorIDX = '$directorIDX' AND type='$type'
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    public static function PartnerListAlarmLoad($data=null)
    {
        $directorIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.type,
        A.status,
        A.contractIDX,
        A.createTime,
        A.con,
        B.name,
        B.status AS partnerStatus
        FROM sendipay.PartnerAlarm AS A
        LEFT JOIN sendipay.Contract AS B
        ON(A.contractIDX=B.idx)
        WHERE A.directorIDX = '$directorIDX' AND A.type=2 ORDER BY A.createTime DESC
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    public static function alarmStatusInfoLoad($data=null)
    {
        $pCode = $data;
        $db = static::getDB();
        $SelAlarmInfo = $db->query("SELECT
        A.idx,
        B.idx as alarmIDX,
        B.type,
        B.directorIDX,
        B.contractIDX,
        B.status,
        B.con
        FROM sendipay.Contract AS A
        LEFT JOIN sendipay.PartnerAlarm AS B
        ON(A.idx = B.contractIDX )
        WHERE A.code = '$pCode'
        ");
        $returnData=$SelAlarmInfo->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

}
