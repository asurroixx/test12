<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ManagerMo extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function GetPoGlobalValue($data=null)
    {
        $resultIDX=$data;
        $db = static::getDB();
        $GetDump = $db->prepare("
        SELECT
        idx,
        email,
        loginToken,
        ipAddress,
        name
        FROM sendipay.Manager WHERE idx = '$resultIDX'
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
        
    }
   
   
}
