<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractRegiContractRenegoMo extends \Core\Model
{
    //StContractCon 재협상 요구 내용 가져오기
    public static function getContractRenegoInfo($data=null)
    {
        $contractIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        status,
        reqCompany,
        reqHomepage,
        reqContractCategoryIDX,
        reqBusinessLicense,
        reqIp,
        reqPortalEmail,
        directorComment
        FROM sendipay.ContractRegiContractRenego
        WHERE contractIDX = '$contractIDX' ORDER BY renegoTime DESC
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }
}
