<?php

namespace App\Models;
use PDO;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractManagerMo extends \Core\Model
{
	// !!manager도 여기서
	/**
	 * Get all the users as an associative array
	 * @return array
	 */

///////////////////////////////////////ContractmanagerTable/////////////////////////////////////



    public static function GetContractUriCheck($data=null)
    {   
        $contractIDX=$data['contractIDX'];
        $managerIDX=$data['managerIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.gradeIDX,
        A.managerIDX,
        B.idx AS contractIDX,
        B.name AS contractName,
        B.balance AS contractBalance,
        B.status AS contractStatus
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Contract AS B
        ON A.contractIDX = B.idx
        WHERE A.managerIDX='$managerIDX' AND A.contractIDX='$contractIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
	public static function GetManagerList($data=null)
    {
        $contractCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.position,
        A.gradeIDX,
        A.name AS managerName,
        B.name AS gradeName,
        C.email
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.MdGrade AS B
        ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.Manager AS C
        ON A.managerIDX = C.idx
        LEFT JOIN sendipay.Contract AS D
        ON A.contractIDX = D.idx
        WHERE D.code = '$contractCode'
        ");
        $Member=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Member;
    }
    public static function GetManagerInfoByIDX($data=null)
    {
        $contractManagerIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.position,
        A.createTime,
        A.gradeIDX,
        A.name AS managerName,
        B.name AS gradeName,
        C.email,
        C.idx AS managerIDX
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.MdGrade AS B
        ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.Manager AS C
        ON A.managerIDX = C.idx
        WHERE A.idx = '$contractManagerIDX'
        ");
        $Member=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Member;
    }

	public static function get_managerEmailExistence($data=null)
    {
    	$emailVal=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,email
        FROM sendipay.Manager
        WHERE email = '$emailVal'
        ");
        $Email=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Email;
    }

    public static function ManagerEmailExistence($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        B.email
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Manager AS B
        ON(A.managerIDX=B.idx)
        WHERE A.contractIDX = '$idx'
        ");
        $Email=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Email;
    }

	public static function get_contractmanagerExistence($data=null)
    {
    	$emailVal=$data['emailVal'];
    	$contractCode=$data['contractCode'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.email
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Manager AS B
        ON(A.managerIDX=B.idx)
        LEFT JOIN sendipay.Contract AS C
        ON(A.contractIDX=C.idx)
        WHERE B.email = '$emailVal' AND C.code = '$contractCode'
        ");
        $ContractManager=$Sel->fetch(PDO::FETCH_ASSOC);
        return $ContractManager;
    }

    //mypage
    public static function get_contractList($data=null)
    {
        $managerIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.position,
        A.gradeIDX,
        B.code,
        B.name AS contractName,
        B.homepage,
        B.transactionFee,
        B.withdrawalFee,
        B.settlemenTetherFee,
        B.settlementUSDCoinFee,
        B.balance,
        C.name AS gradeName
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Contract AS B
        ON A.contractIDX = B.idx
        LEFT JOIN sendipay.MdGrade AS C
        ON A.gradeIDX = C.idx
        LEFT JOIN sendipay.Manager AS D
        ON (A.managerIDX = D.idx)
        WHERE A.managerIDX = '$managerIDX'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetContractManagerList($data=null)
    {       
        $contractCode=$data['contractCode'];
        $keyword=$data['keyword'];
        $keywordStat='n';
        if(isset($data['keywordStat'])){
            $keywordStat=$data['keywordStat'];
        }

        $contractIDXQuery='';

        if($keywordStat==='y'){
            $contractIDXQuery=' WHERE C.name LIKE "%'.$keyword.'%"';
        }else{
            if($contractCode!=='all'){
                $contractIDXQuery=' WHERE (C.code="'.$contractCode.'")';
            }
        }

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        A.managerIDX,
        A.gradeIDX,
        A.position,
        A.createIDX,
        A.createTime,
        D.email,
        B.name AS gradeName,
        C.name AS contractName,
        C.code
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.MdGrade AS B
        ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.Contract AS C
        ON A.contractIDX = C.idx
        LEFT JOIN sendipay.Manager AS D
        ON (A.managerIDX = D.idx)
        "
        .$contractIDXQuery);
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }



    public static function GetManagerLoginHistory($data=null)
    {       
        $managerIDX=$data['managerIDX'];
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){ 
            $startDate='1970-01-01 00:00:00'; 
        }else{
            $startDate.=" 00:00:00";
        }
        $endDate=$data['endDate'];
        if($endDate==""){ 
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $dateSearchQuery=" AND ( createTime BETWEEN '".$startDate."' AND '".$endDate."' )";
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        createTime,
        ipAddress
        FROM sendipay.ManagerLoginHistory 
        WHERE managerIDX='$managerIDX'
        ".$dateSearchQuery);
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }


    

    public static function issetContractManagerEmail($data=null)
    {       
        $managerIDX=$data['managerIDX'];
        $contractIDX=$data['contractIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Manager AS B
        ON (A.managerIDX = B.idx)
        WHERE A.managerIDX='$managerIDX' AND A.contractIDX='$contractIDX'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function issetContractManagerGrade($data=null)
    {      
        $gradeIDX=$data['gradeIDX'];
        $contractIDX=$data['contractIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        gradeIDX,
        contractIDX
        FROM sendipay.ContractManager
        WHERE contractIDX='$contractIDX' AND gradeIDX=1
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function issetContractManagerIDX($data=null)
    {      
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        A.gradeIDX,
        B.idx AS managerIDX
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Manager AS B
        ON A.managerIDX=B.idx
        WHERE A.idx='$idx'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function GetContractManagerEmail($data=null)
    {      
        $gbLoginEmail=$data['gbLoginEmail'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.email,
        A.contractIDX,
        A.gradeIDX
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Manager AS B
        ON (A.managerIDX = B.idx)
        WHERE B.email='$gbLoginEmail'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function getContractGrade($data=null)
    {      
        $managerIDX=$data['managerIDX'];
        $contractCode=$data['contractCode'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.gradeIDX
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Contract AS B
        ON (A.contractIDX = B.idx)
        WHERE A.managerIDX='$managerIDX' AND B.code='$contractCode'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getSuperManagerInContract($data=null)
    {      
        $contractCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        C.email
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.Contract AS B
        ON (A.contractIDX = B.idx)
        LEFT JOIN sendipay.Manager AS C
        ON (A.managerIDx = C.idx)
        WHERE  B.code='$contractCode' AND A.gradeIDX = 1
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetManagerListData($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        gradeIDX
        FROM sendipay.ContractManager
        WHERE (createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
///////////////////////////////////////ContractmanagerTable/////////////////////////////////////

///////////////////////////////////////managerTable //////////////////////////////////////
    public static function issetManagerEmail($data=null)
    {       
        $email=$data;
        $email=trim($email);
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.Manager
        WHERE email='$email'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function GetLogin($data=null)
    {
        $email=$data['email'];
        $ipAddress=$data['ipAddress'];
        $code=$data['code'];
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        B.code
        FROM sendipay.Manager AS A
        LEFT JOIN sendipay.ManagerLoginDump AS B
        ON A.idx = B.managerIDX 
        WHERE A.email='$email' AND B.ipAddress ='$ipAddress' AND B.code='$code'
        ORDER BY B.idx DESC LIMIT 1;
        ");
        $GetDump->execute();
        $count = $GetDump->rowCount();
        $row = $GetDump->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

     public static function GetPoGlobalValue($data=null)
    {
        $resultIDX=$data;
        $db = static::getDB();
        $GetDump = $db->prepare("
        SELECT
        idx,
        email,
        loginToken,
        ipAddress
        FROM sendipay.Manager WHERE idx = '$resultIDX'
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
        
    }

    public static function GetManagerAllInfo($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        contractIDX,
        managerIDX,
        gradeIDX,
        position,
        createIDX,
        createTime
        FROM sendipay.ContractManager
        WHERE idx='$targetIDX'
        ");
        $Member=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Member;
    }
///////////////////////////////////managerTable /////////////////////////////////



/*------------------------------202306 진짜사용-----------------------------*/

    //StManagerCon 전체카운드
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx
        FROM sendipay.ContractManager
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StManagerCon 스태프 매니저관리 데이터테이블
    public static function GetContractManagerListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.createTime,
        CASE B.name
            WHEN 'Manager' THEN 'Normal Manager'
            WHEN 'Super Manager' THEN 'Super Manager'
            WHEN 'Staff' THEN 'Staff'
            WHEN 'Observer' THEN 'Observer'
            ELSE '' END
        AS gradeName,
        IFNULL(C.name,'-') AS contractName,
        IFNULL(C.code,'-') AS code,
        D.email
        FROM sendipay.ContractManager AS A
        JOIN sendipay.MdGrade AS B ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.Contract AS C ON A.contractIDX = C.idx
        JOIN sendipay.Manager AS D ON A.managerIDX=D.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StManagerCon 디테일 정보
    public static function GetContractManagerDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.gradeIDX,
        A.position,
        A.createTime,
        B.name AS gradeName,
        IFNULL(C.serviceName,'-') AS serviceName,
        IFNULL(C.name,'-') AS contractName,
        IFNULL(C.code,'-') AS code,
        IFNULL(C.homepage,'-') AS homepage,
        D.email
        FROM sendipay.ContractManager AS A
        LEFT JOIN sendipay.MdGrade AS B ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.Contract AS C ON A.contractIDX = C.idx
        LEFT JOIN sendipay.Manager AS D ON A.managerIDX = D.idx
        WHERE A.idx='$targetIDX'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }
}