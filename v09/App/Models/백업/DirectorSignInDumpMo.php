<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class DirectorSignInDumpMo extends \Core\Model
{
    public static function GetSignInfo($data=null)
    {
        $code=$data['code'];
        $email=$data['email'];
        $ipAddress=$data['ipAddress'];
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        code,
        idx,
        email
        FROM sendipay.DirectorSignInDump
        WHERE email='$email' AND code='$code' AND ipAddress='$ipAddress'
        ORDER BY createTime DESC LIMIT 1
        ");
        $GetDump->execute();
        $row = $GetDump->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
}
