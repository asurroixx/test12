<?php

namespace App\Models;

use PDO;

class FaqCategoryMo extends \Core\Model
{

    public static function FaqCategoryList($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];

        if($langCateCode=="ALL"){
            $langCateQuery='';
        }else{
            $langCateQuery=' AND B.code="'.$langCateCode.'"';
        }
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.grandMomIDX,
        A.momIDX,
        A.name,
        A.seq,
        A.subUrlIDX,
        A.langCateIDX,
        A.mdLangIDX,
        A.status
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdLangCategory AS B
        ON (A.langCateIDX = B.idx)
        LEFT JOIN sendipay.MdSubUrl AS C
        ON (A.subUrlIDX = C.idx)
        WHERE A.grandMomIDX=0 AND A.momIDX=0 AND C.subUrl='$subUrlCode'
        ".$langCateQuery."
        ORDER BY A.seq");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetCategoryChildMenuList($data=null)
    {
        $adminIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        grandMomIDX,
        momIDX,
        name,
        seq,
        subUrlIDX,
        langCateIDX,
        mdLangIDX,
        status
        FROM sendipay.FaqCategory
        WHERE grandMomIDX='$adminIDX' AND momIDX=0 ORDER BY seq
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetCategoryChildrenMenuList($data=null)
    {
        $adminIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        grandMomIDX,
        momIDX,
        name,
        seq,
        subUrlIDX,
        langCateIDX,
        mdLangIDX,
        status
        FROM sendipay.FaqCategory
        WHERE momIDX='$adminIDX' ORDER BY seq
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function CheeseEyeAction($data=null)
    {
        $EditItemId=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        momIDX,
        status
        FROM sendipay.FaqCategory
        WHERE idx='$EditItemId' ORDER BY seq
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // public static function CategoryLangConfirmData($data=null)
    // {
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     B.idx,
    //     B.name
    //     FROM sendipay.FaqCategory AS A
    //     LEFT JOIN sendipay.MdLangCategory AS B
    //     ON(A.langCateIDX=B.idx)
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    public static function CategoryMenuConfirmData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        name
        FROM sendipay.FaqCategory
        WHERE idx='$idx' ORDER BY seq
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function CategoryConfirmDelete($data=null)
    {
        $EditItemId=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        grandMomIDX,
        status
        FROM sendipay.FaqCategory
        WHERE grandMomIDX='$EditItemId'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function issetCategoryNameData($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $name=$data['name'];
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.name
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B
        ON(A.subUrlIDX=B.idx)
        WHERE A.name='$name' AND B.subUrl='$subUrlCode'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function categoryGroupData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.Faq AS B
        ON(A.idx=B.faqCategoryIDX)
        WHERE B.idx='$idx'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function categoryGrandMomData($data=null)
    {
        $cateIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX,
        B.name AS grandMomName,
        C.name AS momName
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.FaqCategory AS B
        ON(A.grandMomIDX=B.idx)
        LEFT JOIN sendipay.FaqCategory AS C
        ON(A.momIDX=C.idx)
        WHERE A.idx='$cateIDX'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function categoryMomData($data=null)
    {
        $cateMomIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        name
        FROM sendipay.FaqCategory
        WHERE idx='$cateMomIDX'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public static function QnaCategoryChildChange($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX,
        B.name AS grandMomName,
        C.name AS momName
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.FaqCategory AS B
        ON(A.grandMomIDX=B.idx)
        LEFT JOIN sendipay.FaqCategory AS C
        ON(A.momIDX=C.idx)
        WHERE A.idx='$targetIDX'
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetQnaCategoryList($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        if($langCateCode=="ALL"){
            $langCateQuery='';
        }else{
            $langCateQuery=' AND C.code="'.$langCateCode.'"';
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B
        ON(A.subUrlIDX=B.idx)
        LEFT JOIN sendipay.MdLangCategory AS C
        ON(A.langCateIDX=C.idx)
        WHERE B.subUrl='$subUrlCode' AND A.status=3
        ".$langCateQuery."
        ORDER BY A.seq ASC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    public static function GetMomCategoryData($data=null)
    {
        $idx=$data;

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX,
        A.langCateIDX,
        B.name AS langCateName
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdLangCategory AS B
        ON(A.langCateIDX=B.idx)
        WHERE A.grandMomIDX='$idx' ORDER BY A.seq ASC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    public static function GetMomAndCategoryData($data=null)
    {
        $childCategoryIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX,
        A.langCateIDX,
        B.name AS langCateName
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdLangCategory AS B
        ON(A.langCateIDX=B.idx)
        WHERE A.momIDX='$childCategoryIDX' ORDER BY A.seq ASC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }







    public static function GetCategoryDataForIDX($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $sel = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX
        FROM sendipay.FaqCategory AS A
        WHERE A.idx='$idx'
        ");
        $result=$sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetCategoryLangCateIDX($data=null)
    {
        $langCateCode=$data;
        $db = static::getDB();
        $sel = $db->query("SELECT
        B.idx
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdLangCategory AS B
        ON(A.langCateIDX=B.idx)
        WHERE B.Code='$langCateCode'
        ");
        $result=$sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetCategoryListForParentIDX($data=null)
    {
        //내부모idx를 줄테니 형제들을 주시오
        $tmpParentIDX=$data['parentIDX'];//1차가 필요할땐 부모idx가 아니라 subUrlIDX 임
        $tmpType=$data['type'];//내가 몇차인지: 2차리스트가 필요하면 2, 3차리스트가 필요하면 3
        $tmpQuery='';
        if($tmpType=='1'){//1차 리스트주세요
            $tmpQuery=' grandMomIDX="0" AND subUrlIDX="'.$tmpParentIDX.'"';
        }elseif($tmpType=='2'){//2차 리스트주세요
            $tmpQuery=' grandMomIDX="'.$tmpParentIDX.'" AND momIDX=0';
        }else{//3차 리스트주세요
            $tmpQuery=' momIDX="'.$tmpParentIDX.'"';
        }
        $db = static::getDB();
        $sel = $db->query("SELECT
        idx,
        name,
        status
        FROM sendipay.FaqCategory
        WHERE status=3
        AND ".$tmpQuery."
        ORDER BY seq
        ");
        $result=$sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}