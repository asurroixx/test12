<?php

namespace App\Models;

use PDO;

class ContractApiKeyMo extends \Core\Model
{
    public static function GetContractApiKey($data=null)
    {
        $pCode = $data;
        $db = static::getDB();
        $selData = $db->query("SELECT
        B.idx,
        B.apiKey,
        B.status
        FROM sendipay.Contract AS A
        LEFT JOIN sendipay.ContractApiKey AS B
        ON(A.idx = B.contractIDX)
        WHERE A.code = '$pCode'
        ");
        $returnData=$selData->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;

    }

    //StContractDetailCon 같은 apikey 조사
    public static function apiKeyDuplicateChk($data=null)
    {
        $apiKey = $data;
        $db = static::getDB();
        $selData = $db->query("SELECT
        idx,
        apiKey,
        status
        FROM sendipay.ContractApiKey
        WHERE apiKey = '$apiKey'
        ");
        $returnData=$selData->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }
}