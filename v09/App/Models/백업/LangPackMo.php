<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class LangPackMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
	//langPack, langCateogory 둘다씀

	public static function GetLangPackData($data=null)
	{
		$langPageIDX = $data['langPageIDX'];
		$subUrlCode=$data['subUrlCode'];
		$langPageIDXQuery='';
		if($langPageIDX!=='all'){
            $langPageIDXQuery=' AND (B.langPageIDX='.$langPageIDX.')';
        }

		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx,
		A.langIDX,
		A.cateIDX,
		A.value,
		B.code AS langCode,
		C.code AS langCateCode,
		D.name AS langPageName
		FROM sendipay.LangPack as A 
		LEFT JOIN sendipay.MdLang as B
		ON (A.langIDX = B.idx)
		LEFT JOIN sendipay.MdLangCategory as C
		ON (A.cateIDX = C.idx)
		LEFT JOIN sendipay.MdLangPage as D
		ON (B.langPageIDX = D.idx)
		LEFT JOIN sendipay.MdSubUrl as E
		ON (D.subUrlIDX = E.idx)
		WHERE E.subUrl='$subUrlCode'
		".$langPageIDXQuery);

		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}

	public static function GetLangPackDataByLangIDX($data=null)
	{
		$langIDX = $data['langIDX'];
		$subUrl = $data['subUrlCode'];
		$db = static::getDB();
		$langPackSel = $db->query("SELECT
		A.idx,
		A.cateIDX,
		A.value,
		B.langPageIDX,
		B.code AS langCode,
		C.code AS langCateCode
		FROM sendipay.LangPack as A 
		LEFT JOIN sendipay.MdLang as B
		ON (A.langIDX = B.idx)
		LEFT JOIN sendipay.MdLangCategory as C
		ON (A.cateIDX = C.idx)
		WHERE A.langIDX = '$langIDX' AND $subUrl=3
		");

		$LangFetch=$langPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}


	public static function excelMakeLangPackData($data=null)
	{
		// $langCateIDX=$data['langCateIDX'];
		// $subUrlCode = $data['subUrlCode'];
		// $pageIDX = $data['pageIDX'];
		// if($langCateIDX=="all"){
        //     $thisLangQuery='';
        // }else{
        //     $thisLangQuery=' AND A.cateIDX='.$langCateIDX;
        // }
        // $thisPageQuery=' AND C.idx='.$pageIDX;


		$subUrlCode = $data['subUrlCode'];
		$standardLangCateIDX = $data['standardLangCateIDX'];
		$translateLangCateIDX = $data['translateLangCateIDX'];
		$pageIDX = $data['pageIDX'];
        $thisPageQuery=' AND C.idx='.$pageIDX;

		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
			B.value AS standardValue,
			C.value AS translateValue,
			A.code,
			D.name AS langPageName
		FROM sendipay.MdLang as A
		LEFT JOIN sendipay.LangPack as B
		ON (A.idx = B.langIDX AND B.cateIDX = '$standardLangCateIDX')
		LEFT JOIN sendipay.LangPack as C
		ON (A.idx = C.langIDX AND C.cateIDX = '$translateLangCateIDX')
		LEFT JOIN sendipay.MdLangPage as D
		ON (A.langPageIDX = D.idx)
		LEFT JOIN sendipay.MdSubUrl as E
		ON (D.subUrlIDX = E.idx)
		WHERE E.subUrl = '$subUrlCode' AND D.idx='$pageIDX'
		");

		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}



	// public static function GetLangPackDataVersionTwo($data=null)
	// {
	// 	// $langCateIDX=$data['langCateIDX'];
	// 	// $subUrlCode = $data['subUrlCode'];
	// 	// $pageIDX = $data['pageIDX'];
	// 	// if($langCateIDX=="all"){
 //        //     $thisLangQuery='';
 //        // }else{
 //        //     $thisLangQuery=' AND A.cateIDX='.$langCateIDX;
 //        // }
 //        // $thisPageQuery=' AND C.idx='.$pageIDX;


	// 	$subUrlCode = $data['subUrlCode'];
	// 	$thisLangIDX = $data['thisLangIDX'];
	// 	$pageIDX = $data['pageIDX'];

	// 	$thisLangQuery=' AND A.cateIDX='.$thisLangIDX;
 //        $thisPageQuery=' AND C.idx='.$pageIDX;



	// 	$db = static::getDB();
	// 	$MdLangPackSel = $db->query("SELECT
	// 	A.idx,
	// 	A.langIDX,
	// 	A.cateIDX,
	// 	A.value,
	// 	B.code,
	// 	C.name AS langPageName
	// 	FROM sendipay.LangPack as A
	// 	LEFT JOIN sendipay.MdLang as B
	// 	ON (A.langIDX = B.idx)
	// 	LEFT JOIN sendipay.MdLangPage as C
	// 	ON (B.langPageIDX = C.idx)
	// 	LEFT JOIN sendipay.MdSubUrl as D
	// 	ON (C.subUrlIDX = D.idx)
	// 	WHERE D.subUrl = '$subUrlCode'
	// 	".$thisLangQuery.$thisPageQuery);

	// 	$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $LangFetch;
	// }

	public static function GetLangPackDataVersionThree($data=null)
	{
		$langCateIDX=$data['langCateIDX'];
		$langPageIDX=$data['langPageIDX'];
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx,
		A.langIDX,
		A.cateIDX,
		A.value,
		B.code
		FROM sendipay.LangPack as A 
		LEFT JOIN sendipay.MdLang as B
		ON (A.langIDX = B.idx)
		WHERE B.langPageIDX = '$langPageIDX' AND A.cateIDX='$langCateIDX'
		");
		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}

	

	public static function chkLangCodeIsset($data=null)
	{
		$langCode=$data['langCode'];
		$langPageIDX=$data['langPageIDX'];

				
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		idx
		FROM sendipay.MdLang 
		WHERE langPageIDX = '$langPageIDX' AND code='$langCode'
		");
		$langFetch=$MdLangPackSel->fetch(PDO::FETCH_ASSOC);
		return $langFetch;
	}

	public static function get_langCate($data=null)
    {
        $subUrl=$data;
        if($subUrl=='partnerApplication'){
        	$subUrl='partner';
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name,
        code
        FROM sendipay.MdLangCategory WHERE $subUrl=3
        ");
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }

    public static function GetMdLangCategoryData($data=null)
	{
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		idx,
		name,
		code
		FROM sendipay.MdLangCategory
		");

		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}

	
	public static function getCateIDXByCateCode($data=null)
    {
        $langCateCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name
        FROM sendipay.MdLangCategory WHERE code='$langCateCode'
        ");
        $langCate=$Sel->fetch(PDO::FETCH_ASSOC);
        return $langCate;
    }

    public static function getExcelCodeExistence($data=null)
    {
        $langCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        code
        FROM sendipay.MdLang WHERE code='$langCode'
        ");
        $langCodeChk=$Sel->fetch(PDO::FETCH_ASSOC);
        return $langCodeChk;
    }

    public static function langChk($data=null)
    {
        $code=$data['code'];
        $subUrlCode=$data['subUrlCode'];
        $db = static::getDB();
        $Sel = $db->query("SELECT 
        idx
    	FROM sendipay.MdLangCategory
	    WHERE code = '$code' AND $subUrlCode = 3
	    ");
        $chkRowCount=$Sel->fetch(PDO::FETCH_ASSOC);
        return $chkRowCount;
       
    }


    public static function getMdLangIDX($data=null)
    {
        $langCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.MdLang WHERE code='$langCode'
        ");
        $langCodeReturn=$Sel->fetch(PDO::FETCH_ASSOC);
        return $langCodeReturn;
    }

    public static function getMdLangUniqueBysubUrlIDX($data=null)
    {
        $langPageIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.MdLang WHERE langPageIDX='$langPageIDX'
        ");
        $langCodeReturn=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCodeReturn;
    }

    public static function pageAndCodeChk($data=null)
    {
        $code=$data['code'];
        $page=$data['page'];
        $db = static::getDB();
        $Sel = $db->query("SELECT 
        A.idx,
        B.idx AS langPageIDX 
    	FROM sendipay.MdLang as A
    	LEFT JOIN sendipay.MdLangPage as B
		ON (A.langPageIDX = B.idx)
	    WHERE A.code = '$code' AND B.name = '$page'
	    ");
        $chkRowCount=$Sel->fetch(PDO::FETCH_ASSOC);
        return $chkRowCount;
    }

    public static function codeUniqueChk($data=null)
	{
		$code = $data;
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		idx
		FROM sendipay.MdLang
		WHERE  code = '$code'
		");
		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}


}