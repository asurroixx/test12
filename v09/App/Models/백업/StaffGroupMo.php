<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffGroupMo extends \Core\Model
{
    //StStaffGroupCon 데이터테이블
    public static function getStaffGroupList($data=null)
    {       
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        CASE A.status
            WHEN 2 THEN '비활성화'
            WHEN 3 THEN '활성화'
            ELSE '' END
        AS status,
        A.createTime,
        A.updateTime,
        A.name,
        B.name AS staffName
        FROM sendipay.MdGrade AS A
        JOIN sendipay.Staff AS B ON A.StaffIDX = B.IDX
        WHERE A.subUrlIDX = '9'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StStaffGroupCon 등급에 따른 정보
    public static function getStaffList($data=null)
    {   
        $gradeIDX = $data;
        
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name,
        email
        FROM sendipay.Staff
        WHERE gradeIDX = '$gradeIDX'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StStaffGroupCon status 정보
    public static function getGroupStatus($data=null)
    {
        $idx = $data;

        $db = static::getDB();
        $Sel = $db->query("SELECT
        status
        FROM sendipay.MdGrade
        WHERE idx = '$idx'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;

    }

    //StStaffGroupCon 해당 권한
    public static function getPermission($data=null)
    {
        $idx = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        StaffMenuIDX,
        permission
        FROM sendipay.StaffGroup
        WHERE MdGradeIDX='$idx'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //StStaffGroupCon 그룹이름 검사
    public static function issetGroupName($data=null)
    {
        $name = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.MdGrade
        WHERE name='$name'
        AND subUrlIDX='9'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getMenuIDX($data=null)
    {
        $url = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
            idx
            FROM sendipay.StaffMenuStd
            WHERE url='$url'
            AND idx NOT IN (SELECT momIDX FROM sendipay.StaffMenuStd)
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function issetGroupPermission($data=null)
    {
        $menuIDX = $data['menuIDX'];
        $staffIDX = $data['staffIDX'];

        $db = static::getDB();
        $Sel = $db->query("SELECT
            B.idx,
            B.permission
            FROM sendipay.Staff AS A
            LEFT JOIN sendipay.StaffGroup AS B
            ON A.gradeIDX = B.MdGradeIDX
            WHERE B.StaffMenuIDX = '$menuIDX'
            AND B.MdGradeIDX IN (SELECT gradeIDX FROM sendipay.Staff WHERE idx = '$staffIDX')
        ");

        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}
