<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractStatusHistoryMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */


     

    public static function getContractStatusHistory($data=null)
    {   
        $contractIDX=$data['contractIDX'];
        $afterStatus=$data['afterStatus'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        createTime
        FROM sendipay.ContractStatusHistory WHERE afterStatus='$afterStatus' AND contractIDX='$contractIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getContractApplicationTime($data=null)
    {   
        $contractIDX = $data;
        // $contractIDX=$data['contractIDX'];
        // $afterStatus=$data['afterStatus'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        createTime
        FROM sendipay.ContractStatusHistory WHERE afterStatus=2 OR afterStatus=12 AND contractIDX='$contractIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    /*----------------------202306--------------------------*/

    //StContractCon 페이지 랜더시 로그
    public static function GetLogHistoryAndQnaDetail($data=null)
    {
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.createTime,
        A.txt
        FROM sendipay.ContractStatusHistory AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        WHERE B.code='$code'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}