<?php

namespace App\Models;

use PDO;

class QnaAnswerMo extends \Core\Model
{
    
    // public static function GetAnswerDetail($data=null)
    // {
    //     $targetIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.questionIDX,
    //     A.type,
    //     A.contents,
    //     A.createTime,
    //     A.useIDX
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.questionIDX = B.idx)
    //     WHERE A.questionIDX='$targetIDX'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetAnswerSearchData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.type,
    //     A.contents,
    //     A.createTime,
    //     A.useIDX,
    //     A.questionIDX,
    //     B.idx AS QuIDX,
    //     B.subUrlIDX,
    //     B.customerIDX,
    //     B.ticketCode,
    //     B.answerStatus,
    //     C.idx AS staffIDX,
    //     C.email AS staffEmail,
    //     C.name AS staffName,
    //     D.idx AS directorIDX,
    //     D.email AS directorEmail,
    //     D.name AS directorName,
    //     E.idx AS adminIDX,
    //     E.name AS adminName,
    //     E.email AS adminEmail,
    //     G.email AS managerEmail
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.QnaQuestion AS B
    //     ON(A.questionIDX = B.idx)
    //     LEFT JOIN sendipay.Staff AS C
    //     ON(A.useIDX=C.idx)
    //     LEFT JOIN sendipay.Director AS D
    //     ON(A.useIDX=D.idx)
    //     LEFT JOIN sendipay.Admin AS E
    //     ON(A.useIDX=E.idx)
    //     LEFT JOIN sendipay.ContractManager AS F
    //     ON(A.useIDX=F.idx)
    //     LEFT JOIN sendipay.Manager AS G
    //     ON(F.managerIDX=G.idx)
    //     WHERE B.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetBeforeTypeData($data=null)
    // {
    //     $questionIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     type
    //     FROM sendipay.QnaAnswer
    //     WHERE questionIDX='$questionIDX' ORDER BY createTime DESC LIMIT 2
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetAnswerTypeData($data=null)
    // {
    //     $thisIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     type,
    //     createTime
    //     FROM sendipay.QnaAnswer
    //     WHERE questionIDX='$thisIDX' ORDER BY createTime DESC
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function answerStatusDataSearch($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.type,
    //     B.status,
    //     B.answerStatus
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.QnaQuestion AS B
    //     ON(A.questionIDX=B.idx)
    //     WHERE B.ticketCode='$ticketCode' ORDER BY A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function answerCountDataSearch($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.type,
    //     A.createTime,
    //     B.status
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.QnaQuestion AS B
    //     ON(A.questionIDX=B.idx)
    //     WHERE B.ticketCode='$ticketCode' AND (A.type=2 OR A.type=3)
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }



    // public static function GetUseIDXData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.useIDX
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.QnaQuestion AS B
    //     ON(A.questionIDX=B.idx)
    //     LEFT JOIN sendipay.Staff AS C
    //     ON(A.useIDX=C.idx)
    //     WHERE B.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetNodeTargetIDXData($data=null)
    // {
    //     $targetIDX=$data['targetIDX'];
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.useIDX
    //     FROM sendipay.QnaAnswer AS A
    //     LEFT JOIN sendipay.ContractManager AS B
    //     ON(A.useIDX = B.idx)
    //     LEFT JOIN sendipay.QnaQuestion AS C
    //     ON(A.questionIDX = C.idx)
    //     WHERE A.questionIDX='$targetIDX' AND A.type=2 AND C.subUrlIDX='$subUrlIDX'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    

    // public static function GetQnaCategoryList($data=null)
    // {
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     if($langCateIDX=="all"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND langCateIDX='.$langCateIDX;
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     name
    //     FROM sendipay.QnaCategory
    //     WHERE subUrlIDX='$subUrlIDX'
    //     ".$langCateQuery."
    //     ORDER BY seq ASC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }
    // public static function GetQnaList($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     if($categoryIDX=="all"){
    //         $thisQuery='';
    //     }else{
    //         $thisQuery=' AND A.qnaCategoryIDX='.$categoryIDX;
    //     }
    //     if($langCateIDX=="all"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND B.langCateIDX='.$langCateIDX;
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.langCateIDX,
    //     A.questionIDX,
    //     A.title,
    //     A.question,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.staffIDX,
    //     B.name AS qnaCategoryName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     WHERE B.subUrlIDX='$subUrlIDX'
    //     ".$thisQuery
    //     .$langCateQuery.
    //     "
    //     ORDER BY B.seq ASC 
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetAdminQnaList($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.status,
    //     B.name
    //     FROM sendipay.Qna AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     WHERE A.adminCategoryIDX
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetQnaDetail($data=null)
    // {
    //     $idx=$data;
        
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.langCateIDX,
    //     A.questionIDX,
    //     A.title,
    //     A.question,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.staffIDX,
    //     B.idx AS categoryIDX,
    //     B.name AS categoryName,
    //     C.name AS subUrlName,
    //     C.idx AS subUrlIDX
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS C
    //     ON(A.subUrlIDX=C.idx)
    //     WHERE A.idx='$idx'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }
    // public static function issetQnaListByQnaCateIDX($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.Qna 
    //     WHERE qnaCategoryIDX='$idx'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }





    /*---------------------20230601 JB-------------------------*/

    // QnaCon 데이터테이블 제목,카테고리 작업
    public static function answerStaffCountDataSearch($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.type,
        A.createTime,
        COUNT(A.questionIDX) AS QidxCount,
        B.status,
        C.momIDX,
        D.value,
        E.value AS secondValue
        FROM sendipay.QnaAnswer AS A
        LEFT JOIN sendipay.QnaQuestion AS B ON A.questionIDX=B.idx
        LEFT JOIN sendipay.QnaCategory AS C ON B.qnaCategoryIDX=C.idx
        LEFT JOIN sendipay.SystemLang AS D ON C.idx=D.targetIDX AND D.type=2 AND D.langCategoryIDX=2
        LEFT JOIN sendipay.SystemLang AS E ON C.momIDX=E.targetIDX AND E.type=2 AND E.langCategoryIDX=2
        WHERE B.ticketCode='$ticketCode' AND (A.type=2 OR A.type=3)
        ORDER BY A.createTime DESC
        ");
        // $count = $Sel->rowCount();
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaCon dev 데이터테이블 제목,카테고리 작업
    public static function DevAnswerStaffCountDataSearch($data=null)
    {
        $ticketCode=$data;
        $devDb = static::getDevDB();
        $Sel = $devDb->query("SELECT
        A.type,
        A.createTime,
        COUNT(A.questionIDX) AS QidxCount,
        B.status,
        C.momIDX,
        D.value,
        E.value AS secondValue
        FROM sendipay.QnaAnswer AS A
        LEFT JOIN sendipay.QnaQuestion AS B ON A.questionIDX=B.idx
        LEFT JOIN sendipay.QnaCategory AS C ON B.qnaCategoryIDX=C.idx
        LEFT JOIN sendipay.SystemLang AS D ON C.idx=D.targetIDX AND D.type=2 AND D.langCategoryIDX=2
        LEFT JOIN sendipay.SystemLang AS E ON C.momIDX=E.targetIDX AND E.type=2 AND E.langCategoryIDX=2
        WHERE B.ticketCode='$ticketCode' AND (A.type=2 OR A.type=3)
        ORDER BY A.createTime DESC
        ");
        // $count = $Sel->rowCount();
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon 질문,답변,시스템 정보(디테일 가운데 영역)
    public static function GetAnswerData($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.type,
        A.contents,
        A.createTime,
        A.staffIDX,
        CASE
            WHEN B.subUrlIDX = 0 THEN (SELECT email FROM sendipay.Director WHERE idx = B.customerIDX)
            -- WHEN B.subUrlIDX = 1 THEN (SELECT name FROM sendipay.Director WHERE idx = B.customerIDX)
            WHEN B.subUrlIDX = 2 THEN (SELECT email FROM sendipay.Director WHERE idx = B.customerIDX)
            WHEN B.subUrlIDX = 3 THEN (SELECT name FROM sendipay.ContractManager WHERE idx = B.customerIDX)
            -- WHEN B.subUrlIDX = 4 THEN (SELECT name FROM sendipay.Director WHERE idx = B.customerIDX)
            WHEN B.subUrlIDX = 5 THEN (SELECT nickName FROM sendipay.Member WHERE idx = B.customerIDX)
            -- WHEN B.subUrlIDX = 6 THEN (SELECT nickName FROM sendipay.Member WHERE idx = A.customerIDX)
        END AS customerName,
        C.email AS staffEmail,
        C.name AS staffName,
        D.value AS systemLang
        FROM sendipay.QnaAnswer AS A
        JOIN sendipay.QnaQuestion AS B ON A.questionIDX = B.idx
        LEFT JOIN sendipay.Staff AS C ON A.staffIDX=C.idx/*staffIDX정보가 너무없음 데이터 초기화하면 INNER JOIN 변경*/
        LEFT JOIN sendipay.SystemLang AS D ON A.systemIDX=D.targetIDX AND D.type=3 AND D.langCategoryIDX=2
        WHERE B.ticketCode='$ticketCode'
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon dev 질문,답변,시스템 정보(디테일 가운데 영역)
    public static function DevGetAnswerData($data=null)
    {
        $ticketCode=$data;
        $devDb = static::getDevDB();
        $Sel = $devDb->query("SELECT
        A.idx,
        A.type,
        A.contents,
        A.createTime,
        A.staffIDX,
        A.staffEmail,
        '' AS staffName,
        (SELECT email FROM sendipay.Director WHERE idx = B.customerIDX) AS customerName,
        D.value AS systemLang
        FROM sendipay.QnaAnswer AS A
        JOIN sendipay.QnaQuestion AS B ON A.questionIDX = B.idx
        LEFT JOIN sendipay.SystemLang AS D ON A.systemIDX=D.targetIDX AND D.type=3 AND D.langCategoryIDX=2
        WHERE B.ticketCode='$ticketCode'
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon 해당 질문의 questionIDX
    public static function GetTargetquestionIDXData($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $subUrlIDX=$data['subUrlIDX'];
        if($subUrlIDX==6){
            $db = static::getDevDB();
        }else{
            $db = static::getDB();
        }
        $Sel = $db->query("SELECT
        questionIDX
        FROM sendipay.QnaAnswer
        WHERE questionIDX='$targetIDX'
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon dev 해당 질문의 questionIDX
    // public static function DevGetTargetquestionIDXData($data=null)
    // {
    //     $targetIDX=$data;
    //     $devDb = static::getDevDB();
    //     $Sel = $devDb->query("SELECT
    //     questionIDX
    //     FROM sendipay.QnaAnswer
    //     WHERE questionIDX='$targetIDX'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }



}