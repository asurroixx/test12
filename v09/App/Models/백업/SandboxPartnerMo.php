<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class SandboxPartnerMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
	// public static function GetPartnerDataLoad($data=null)
	// {
 //        $idx=$data;
 //        $db = static::getDB();
 //        $partnerData = $db->query("SELECT
 //        A.idx,
 //        A.name,
 //        A.homepage,
 //        A.manager,
 //        A.email,
 //        A.status,
 //        A.balance,
 //        A.transactionFee,
 //        A.transactionMinAmount,
 //        A.transactionMaxAmount,
 //        A.withdrawMinAmount,
 //        A.withdrawMaxAmount,
 //        A.settlementMinAmount,
 //        A.settlementMaxAmount,
 //        A.settlementTetherFee,
 //        A.settlementEtherFee,
 //        A.withdrawalFee,
 //        B.partnerIDX
 //        FROM sendipay.Partner AS A
 //        LEFT JOIN sendipay.PartnerWallet AS B
 //        ON A.idx = B.partnerIDX
 //        WHERE A.idx = '$idx'
 //        ");
 //        $result=$partnerData->fetch(PDO::FETCH_ASSOC);
 //        return $result;
	// }

 //    public static function GetPartnerInfoForm($data=null)
 //    {   
 //        $thisIDX=$data;
 //        $db = static::getDB();
 //        $Sel = $db->query("SELECT
 //        idx,
 //        code,
 //        homepage,
 //        manager,
 //        email
 //        FROM sendipay.Partner WHERE idx='$thisIDX'
 //        ");
 //        $partnerForm=$Sel->fetch(PDO::FETCH_ASSOC);
 //        return $partnerForm;
 //    }

 //    public static function GetPartnerWalletLoad($data=null)
 //    {
 //        $idx=$data;
 //        $db = static::getDB();
 //        $partnerData = $db->query("SELECT
 //        idx,
 //        partnerIDX,
 //        coinType,
 //        walletAddr,
 //        managerIDX,
 //        createTime,
 //        memo
 //        FROM sendipay.PartnerWallet WHERE partnerIDX = '$idx'
 //        ");
 //        $result=$partnerData->fetchAll(PDO::FETCH_ASSOC);
 //        return $result;
 //    }

    public static function GetSandboxPartnerCode($data=null)
    {   
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        directorIDX,
        name
        FROM sendipay.SandboxPartner WHERE code='$code'
        ");
        $partnerForm=$Sel->fetch(PDO::FETCH_ASSOC);
        return $partnerForm;
    }

    // public static function GetPartnerWalletForm($data=null)
    // {   
    //     $thisIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.partnerIDX,
    //     A.coinType,
    //     A.walletAddr,
    //     A.managerIDX,
    //     A.createTime,
    //     A.memo,
    //     B.name AS managerName
    //     FROM sendipay.PartnerWallet AS A
    //     LEFT JOIN sendipay.Manager AS B
    //     ON A.managerIDX = B.idx
    //     WHERE A.idx='$thisIDX'
    //     ");
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetPartnerWalletAddrConfirm($data=null)
    // {   
    //     $walletAddr=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     partnerIDX,
    //     coinType,
    //     walletAddr,
    //     managerIDX,
    //     createTime,
    //     memo
    //     FROM sendipay.PartnerWallet WHERE walletAddr='$walletAddr'
    //     ");
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }


    // public static function GetPartnerWalletByCoinType($data=null)
    // {   
    //     $partnerIDX=$data['partnerIDX'];
    //     $coinType=$data['coinType'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,       
    //     walletAddr,
    //     memo
    //     FROM sendipay.PartnerWallet WHERE partnerIDX='$partnerIDX' AND coinType='$coinType'
    //     ");
    //     $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function get_partnerList($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,name,status
    //     FROM sendipay.Partner");
    //     $Partner=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $Partner;
    // }

    // public static function GetPartnerSelDataLoad($data=null)
    // {   
    //     $thisIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     code,
    //     name,
    //     homepage,
    //     manager,
    //     email
    //     FROM sendipay.Partner
    //     ");
    //     $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    public static function getPartnerByDirectorIDX($data=null)
    {   
        $directorIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        homepage,
        manager,
        email,
        createTime,
        status
        FROM sendipay.SandboxPartner
        WHERE directorIDX='$directorIDX'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // public static function GetPartnerWalletForm($data=null)
    // {   
    //     $thisIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     partnerIDX,
    //     coinType,
    //     walletAddr,
    //     managerIDX,
    //     createTime,
    //     memo
    //     FROM sendipay.PartnerWallet WHERE idx='$thisIDX'
    //     ");
    //     $result=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

	// public static function GetFaqList($data=null)
 //    {
 //    	$categoryIDX=$data['categoryIDX'];
 //        $subUrlIDX=$data['subUrlIDX'];
 //        if($categoryIDX=="all"){
 //            $thisQuery='';
 //        }else{
 //            $thisQuery=' AND A.faqCategoryIDX='.$categoryIDX;
 //        }

 //        $db = static::getDB();
 //        $faqList = $db->query("SELECT
 //        A.idx,
 //        A.faqCategoryIDX,
 //        A.title,
 //        A.answer,
 //        A.adminIDX,
 //        A.subUrlIDX,
 //        A.langCateIDX,
 //        B.name,
 //        C.name AS langCateName
 //        FROM sendipay.Faq AS A
 //        LEFT JOIN sendipay.FaqCategory AS B
 //        ON(A.faqCategoryIDX = B.idx)
 //        LEFT JOIN sendipay.MdLangCategory AS C
 //        ON(A.langCateIDX = C.idx)
 //        WHERE A.subUrlIDX = '$subUrlIDX'
 //        ".$thisQuery);
 //        $result=$faqList->fetchAll(PDO::FETCH_ASSOC);
 //        return $result;
 //    }

 //    public static function GetFaqDetailList($data=null)
 //    {
 //        $thisIDX=$data['idx'];
 //        $langCateIDX=$data['langCateIDX'];
 //        $db = static::getDB();
 //        $faqData = $db->query("SELECT
 //        A.idx,
 //        A.faqCategoryIDX,
 //        A.title,
 //        A.answer,
 //        A.adminIDX,
 //        A.subUrlIDX,
 //        A.langCateIDX,
 //        B.name
 //        FROM sendipay.Faq AS A
 //        LEFT JOIN sendipay.FaqCategory AS B
 //        ON A.faqCategoryIDX = B.idx
 //        WHERE A.idx = '$thisIDX' AND A.langCateIDX = '$langCateIDX'
 //        ");
 //        $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
 //        return $faqListData;
 //    }

 //    public static function FaqConfirm($data=null)
 //    {
 //        $thisIDX=$data;
 //        $db = static::getDB();
 //        $faqData = $db->query("SELECT
 //        idx,
 //        name
 //        FROM sendipay.FaqCategory 
 //        ");
 //        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
 //        return $faqListData;
 //    }

    
}