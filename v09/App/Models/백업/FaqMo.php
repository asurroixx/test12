<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class FaqMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function GetFaqCategoryList($data=null)
	{
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        if($langCateCode=="ALL"){
            $langCateQuery='';
        }else{
            $langCateQuery=' AND C.code="'.$langCateCode.'"';
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.name,
        A.grandMomIDX,
        A.momIDX
        FROM sendipay.FaqCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B
        ON(A.subUrlIDX=B.idx)
        LEFT JOIN sendipay.MdLangCategory AS C
        ON(A.langCateIDX=C.idx)
        WHERE B.subUrl='$subUrlCode' AND A.status=3
        ".$langCateQuery."
        ORDER BY A.seq ASC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
	}




	public static function GetFaqList($data=null)
    {
    	$categoryIDX=$data['categoryIDX'];
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        
        if($categoryIDX=="all"){
            $thisQuery='';
        }else{
            // $thisQuery=' AND A.faqCategoryIDX='.$categoryIDX;
            $thisQuery=' AND (B.idx='.$categoryIDX.' OR B.momIDX='.$categoryIDX.' OR B.grandMomIDX='.$categoryIDX.')';
        }
        if($langCateCode=="ALL"){
            $thisLangCateQuery='';
        }else{
            // $thisLangCateQuery=' AND B.langCateIDX=C.idx';
            $thisLangCateQuery=' AND C.code="'.$langCateCode.'"';
        }
        $db = static::getDB();
        $faqList = $db->query("SELECT
        A.idx,
        A.faqCategoryIDX,
        A.title,
        A.answer,
        A.staffIDX,
        A.subUrlIDX,
        A.langCateIDX,
        A.createTime,
        B.name AS categoryName,
        C.name AS langCateName,
        E.name AS staffName
        FROM sendipay.Faq AS A
        LEFT JOIN sendipay.FaqCategory AS B
        ON(A.faqCategoryIDX = B.idx)
        LEFT JOIN sendipay.MdLangCategory AS C
        ON(B.langCateIDX = C.idx)
        LEFT JOIN sendipay.MdSubUrl AS D
        ON(A.subUrlIDX = D.idx)
        LEFT JOIN sendipay.Staff AS E
        ON(A.staffIDX = E.idx)
        WHERE D.subUrl = '$subUrlCode'
        ".$thisQuery.$thisLangCateQuery);
        $result=$faqList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetPortalFaqList($data=null)
    {
        $categoryIDX=$data['categoryIDX'];
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        
        if($categoryIDX=="all"){
            $thisQuery='';
        }else{
            $thisQuery=' AND A.faqCategoryIDX='.$categoryIDX;
        }
        if($langCateCode=="ALL"){
            $thisLangCateQuery='';
        }else{
            $thisLangCateQuery=' AND B.langCateIDX=C.idx';
        }
        $db = static::getDB();
        $faqList = $db->query("SELECT
        A.idx,
        A.title,
        A.answer,
        B.name AS categoryName,
        D.name AS staffName
        FROM sendipay.Faq AS A
        LEFT JOIN sendipay.FaqCategory AS B
        ON(A.faqCategoryIDX = B.idx)
        LEFT JOIN sendipay.MdSubUrl AS C
        ON(A.subUrlIDX = C.idx)
        LEFT JOIN sendipay.Staff AS D
        ON(A.staffIDX = D.idx)
        WHERE C.subUrl = '$subUrlCode'
        ".$thisQuery.$thisLangCateQuery);
        $result=$faqList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetFaqDetailList($data=null)
    {
        // $thisIDX=$data;
        $thisIDX=$data['idx'];
        $subUrlCode=$data['subUrlCode'];
        $db = static::getDB();
        $faqData = $db->query("SELECT
        A.idx,
        A.faqCategoryIDX,
        A.title,
        A.answer,
        A.subUrlIDX,
        A.createTime,
        D.code,
        B.name,
        C.subUrl,
        E.name AS staffName
        FROM sendipay.Faq AS A
        LEFT JOIN sendipay.FaqCategory AS B
        ON A.faqCategoryIDX = B.idx
        LEFT JOIN sendipay.MdSubUrl AS C
        ON A.subUrlIDX = C.idx
        LEFT JOIN sendipay.MdLangCategory AS D
        ON A.langCateIDX = D.idx
        LEFT JOIN sendipay.Staff AS E
        ON A.staffIDX = E.idx
        WHERE A.idx = '$thisIDX' 
        ");
        $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function GetFaqSubUrlList($data=null)
    {
        // $subUrlIDX=$data;
        // $db = static::getDB();
        // $faqData = $db->query("SELECT
        // A.idx,
        // A.subUrlIDX,
        // B.idx AS cateIDX,
        // B.subUrlIDX AS cateSubUrlIDX,
        // B.name
        // FROM sendipay.Faq AS A
        // LEFT JOIN sendipay.FaqCategory AS B
        // ON A.subUrlIDX = B.subUrlIDX
        // WHERE A.subUrlIDX = '$subUrlIDX'
        // ");
        // $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
        // return $faqListData;
    }

    public static function FaqConfirm($data=null)
    {

        $subUrlIDX=$data['subUrlIDX'];
        $langCateIDX=$data['langCateIDX'];
        if($langCateIDX=="all"){
            $thisQuery='';
        }else{
            $thisQuery=' AND subUrlIDX='.$subUrlIDX;
        }
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        subUrlIDX,
        langCateIDX,
        name
        FROM sendipay.FaqCategory WHERE langCateIDX = '$langCateIDX'
        ".$thisQuery);
        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function GetFaqCategoryConfirmList($data=null)
    {
        $thisIDX=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        name,
        langCateIDX
        FROM sendipay.FaqCategory WHERE idx='$thisIDX'
        ");
        $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function issetCategoryName($data=null)
    {
        $inputVal=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        name
        FROM sendipay.FaqCategory WHERE name='$inputVal'
        ");
        $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    // public static function GetLangCateIDX($data=null)
    // {
    //     $langCateCode=$data;
    //     if($langCateCode=="all"){
    //         $thisQuery='';
    //     }else{
    //         $thisQuery=' AND B.code='.$langCateCode;
    //     }   
    //     $db = static::getDB();
    //     $faqData = $db->query("SELECT
    //     B.idx,
    //     B.name
    //     FROM sendipay.FaqCategory AS A
    //     LEFT JOIN sendipay.MdLangCategory AS B
    //     ON(A.langCateIDX=B.idx)
    //     ".$thisQuery);
    //     $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
    //     return $faqListData;
    // }

    public static function GetSubUrlIDXConfirmList($data=null)
    {
        $langCateIDX=$data['langCateIDX'];
        $subUrlIDX=$data['subUrlIDX'];
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        name,
        subUrlIDX,
        langCateIDX
        FROM sendipay.FaqCategory
        WHERE langCateIDX='$langCateIDX' AND subUrlIDX='$subUrlIDX' 
        ORDER BY seq");
        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function FaqDeleteCheck($data=null)
    {
        $thisIDX=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        faqCategoryIDX
        FROM sendipay.Faq
        WHERE faqCategoryIDX='$thisIDX'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    
}