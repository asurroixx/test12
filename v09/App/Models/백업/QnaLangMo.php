<?php

namespace App\Models;

use PDO;

class QnaLangMo extends \Core\Model
{

    public static function get_langCate($data=null)
    {
        $langCateIDX=$data;

        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        value
        FROM sendipay.QnaLang WHERE langCategoryIDX='$langCateIDX' AND type=2
        ");
        $langCate=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $langCate;
    }


}