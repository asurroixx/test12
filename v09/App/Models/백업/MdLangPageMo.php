<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MdLangPageMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function getLangPageIDX($data=null)
	{
		$subUrlCode = $data['subUrlCode'];
		$name = $data['name'];
		if($subUrlCode=='partnerApplication'){
			$subUrlCode='partner';
		}
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx,
		A.name
		FROM sendipay.MdLangPage as A 
		LEFT JOIN sendipay.MdSubUrl as B
		ON (A.subUrlIDX = B.idx)
		WHERE B.subUrl = '$subUrlCode' AND A.name = '$name'
		");
		$LangFetch=$MdLangPackSel->fetch(PDO::FETCH_ASSOC);
		return $LangFetch;
	}


	
	public static function getLangPageList($data=null)
	{
		$subUrlCode = $data;
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx,
		A.name
		FROM sendipay.MdLangPage as A 
		LEFT JOIN sendipay.MdSubUrl as B
		ON (A.subUrlIDX = B.idx)
		WHERE B.subUrl = '$subUrlCode'
		");
		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}


	public static function getLangPageListInFileMaker($data=null)
	{
		$subUrlCode = $data['subUrlCode'];
		$langPageIDX = $data['langPageIDX'];
		if($langPageIDX=='all'){
			$langPageIDXQuery='';
		}else{
			$langPageIDXQuery= ' AND A.idx='.$langPageIDX;
		}
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx,
		A.name
		FROM sendipay.MdLangPage as A
		LEFT JOIN sendipay.MdSubUrl as B
		ON (A.subUrlIDX = B.idx)
		WHERE B.subUrl = '$subUrlCode'
		".$langPageIDXQuery);
		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}

	public static function langPageUniqueChk($data=null)
	{
		$subUrlCode = $data['subUrlCode'];
		$name = $data['name'];
		$db = static::getDB();
		$MdLangPackSel = $db->query("SELECT
		A.idx
		FROM sendipay.MdLangPage as A 
		LEFT JOIN sendipay.MdSubUrl as B
		ON (A.subUrlIDX = B.idx)
		WHERE B.subUrl = '$subUrlCode' AND A.name = '$name'
		");
		$LangFetch=$MdLangPackSel->fetchAll(PDO::FETCH_ASSOC);
		return $LangFetch;
	}


}