<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ManagerLoginHistoryMo extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function loginHistoryData($data=null)
    {
        $loginIDX=$data;
        $db = static::getDB();
        $GetData = $db->prepare("
        SELECT
        idx,
        createTime,
        ipAddress
        FROM sendipay.ManagerLoginHistory WHERE managerIDX = '$loginIDX'
        ");
        $GetData->execute();
        $GetDataLoad=$GetData->fetchAll(PDO::FETCH_ASSOC);
        return $GetDataLoad;
        
    }

    public static function GetManagerLoginHistory($data=null)
    {       
        $loginIDX=$data['loginIDX'];
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){ 
            $startDate='1970-01-01 00:00:00'; 
        }else{
            $startDate.=" 00:00:00";
        }
        $endDate=$data['endDate'];
        if($endDate==""){ 
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $dateSearchQuery=" AND ( createTime BETWEEN '".$startDate."' AND '".$endDate."' )";
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        createTime,
        ipAddress
        FROM sendipay.ManagerLoginHistory 
        WHERE managerIDX='$loginIDX'
        ".$dateSearchQuery);
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }
   
   
}
