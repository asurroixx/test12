<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MdLangCategoryMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	// QnaCon 카테고리 추가시 정보불러오기
	public static function GetData($data=null)
	{
		$code = $data;
		$db = static::getDB();
		$idxSel = $db->query("SELECT
			idx,
			code
			FROM sendipay.MdLangCategory
			ORDER BY idx

		");
		$idx=$idxSel->fetchAll(PDO::FETCH_ASSOC);
		return $idx;
	}

	// QnaDetailCon langCateIDX 구하기
	public static function GetLangCateIDX($data=null)
	{
		$code = $data;
		$db = static::getDB();
		$idxSel = $db->query("SELECT
			idx,
			code
			FROM sendipay.MdLangCategory
			WHERE code='$code'

		");
		$idx=$idxSel->fetch(PDO::FETCH_ASSOC);
		return $idx;
	}

}