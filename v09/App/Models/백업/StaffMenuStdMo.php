<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMenuStdMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//StStaffMenuCon 스태프 페이지 메뉴 리스트
	public static function GetStaffMenuList($data=null)
	{
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		name,
		url,
		seq,
		status,
		memo
		FROM sendipay.StaffMenuStd
        WHERE momIDX=0 ORDER BY seq 
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StStaffMenuCon 스태프 페이지 2차 메뉴 리스트
	public static function GetStaffChildMenuList($data=null)
	{
		$staffIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		name,
		url,
		seq,
		status,
		memo
		FROM sendipay.StaffMenuStd
        WHERE momIDX='$staffIDX' ORDER BY seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StStaffGroupCon 권한에 따른 메뉴
	public static function GetStaffMenuListByGroup($data=null)
	{
		$db = static::getDB();
		$CategoryList = $db->query("SELECT 
			idx
			,momIDX
			,name
			,IF (STATUS = '3', '활성', '비활성') AS status
			FROM sendipay.StaffMenuStd AS A
			ORDER BY
			CASE WHEN momIDX = 0 THEN seq ELSE
			(SELECT seq FROM sendipay.StaffMenuStd WHERE idx = A.momIDX ORDER BY seq) END,
			CASE WHEN momIDX != 0 THEN seq END
			");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//코어컨트롤러에서 쓰이는듯?
	public static function GetStaffMenuListByPermission($data=null)
	{
		$MdGradeIDX = $data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.url,
		A.seq,
		A.status,
		A.memo,
		B.permission,
		B.MdGradeIDX
		FROM sendipay.StaffMenuStd AS A
		LEFT JOIN sendipay.StaffGroup AS B ON A.idx = B.StaffMenuIDX
      	WHERE A.momIDX=0 AND B.MdGradeIDX = '$MdGradeIDX' OR B.MdGradeIDX IS NULL
      	ORDER BY seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	//코어컨트롤러에서 쓰이는듯?
	public static function GetStaffChildMenuListByPermission($data=null)
	{
		$menuIDX=$data['menuIDX'];
		$gradeIDX=$data['gradeIDX'];
		$loginIDX=$data['loginIDX'];
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		A.idx,
		A.momIDX,
		A.name,
		A.url,
		A.seq,
		A.status,
		A.memo,
		B.permission,
		COALESCE(C.count, 0) AS count
		FROM sendipay.StaffMenuStd AS A
		INNER JOIN sendipay.StaffGroup AS B ON A.idx = B.StaffMenuIDX
		LEFT JOIN (
			SELECT menuIDX, COUNT(idx) AS count
			FROM sendipay.StaffAlarm
			WHERE staffIDX = '$loginIDX' AND status = 2
			GROUP BY menuIDX
		) AS C ON A.idx=C.menuIDX
		WHERE  A.momIDX='$menuIDX' AND B.MdGradeIDX='$gradeIDX'
		GROUP BY A.idx
		ORDER BY A.seq
		");
		$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	//StStaffMenuCon 메뉴 숨김 status 조사
	public static function CheeseEyeAction($data=null)
	{
		$EditItemId=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM sendipay.StaffMenuStd
        WHERE idx='$EditItemId' ORDER BY seq 
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//StStaffMenuCon 컨펌창 열때 정보 불러오기
	public static function StaffMenuConfirmData($data=null)
	{
		$staffIDX=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		name,
		url
		FROM sendipay.StaffMenuStd
        WHERE idx='$staffIDX' ORDER BY seq 
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//StStaffMenuCon 삭제 전 2차메뉴가 있는지 조사
	public static function StaffMenuDeleteCheck($data=null)
	{
		$EditItemId=$data;
		$db = static::getDB();
		$CategoryList = $db->query("SELECT
		idx,
		momIDX,
		status
		FROM sendipay.StaffMenuStd
        WHERE momIDX='$EditItemId'
		");
		$result=$CategoryList->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	//StNavCon 에서 쓰임
	public static function getGradeIDx($data=null)
	{
		$idx=$data;
		$db = static::getDB();
		$Select = $db->query("SELECT
		gradeIDX
		FROM sendipay.Staff
        WHERE idx='$idx'
		");
		$result=$Select->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	

	// public static function GetStaffMenuListTest($data=null)
	// {
	// 	$db = static::getDB();
	// 	$CategoryList = $db->query("SELECT
	// 		idx,
	// 		momIDX,
	// 		name,
	// 		url,
	// 		seq,
	// 		status,
	// 		memo
	// 		FROM sendipay.StaffMenuStd
	// 		WHERE momIDX=0 ORDER BY seq
	// 		");
	// 	$result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
	// 	return $result;
	// }
    
}