<?php

namespace App\Models;

use PDO;

class QnaCategoryMo extends \Core\Model
{
    //QnaCon 랜더시 솔팅 카테고리 리스트
    public static function getCateList($data=null)
    {
        $cateType=$data['type'];
        $subUrlCode=$data['subUrlCode'];
        $cateQuery='';

        if($cateType=="child"){
            $cateQuery=' AND A.momIDX!=0';
        }else if($cateType=="mom"){
            $cateQuery=' AND A.momIDX=0';
        }

        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.momIDX,
        A.seq,
        A.status,
        C.langCategoryIDX,
        C.value
        FROM sendipay.QnaCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B ON A.subUrlIDX=B.idx
        LEFT JOIN sendipay.SystemLang AS C ON A.idx=C.targetIDX AND C.type=2
        WHERE B.subUrl='$subUrlCode' AND A.status=3 ".$cateQuery."
        ORDER BY A.seq
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //카테고리관리01 QnaCon 카테고리 관리 리스트 로드
    public static function QnaCategoryList($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $langCateIDX = $data['langCateIDX'];
        $langCateIDXQuery='';
        if($langCateIDX!=='all'){
            $langCateIDXQuery=' AND C.langCategoryIDX='.$langCateIDX.'';
        }
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.momIDX,
        A.seq,
        A.subUrlIDX,
        A.status,
        C.idx AS langPackIDX,
        C.value
        FROM sendipay.QnaCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B ON A.subUrlIDX=B.idx
        LEFT JOIN sendipay.SystemLang AS C ON A.idx=C.targetIDX AND C.type=2
        WHERE A.momIDX=0 AND B.subUrl='$subUrlCode'".$langCateIDXQuery."
        ORDER BY A.seq");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //카테고리관리02 QnaCon 카테고리 관리 리스트 로드
    public static function GetCategoryChildMenuList($data=null)
    {
        $idx=$data['idx'];
        $langCateIDX = $data['langCateIDX'];
        $langCateIDXQuery='';
        if($langCateIDX!=='all'){
            $langCateIDXQuery=' AND C.langCategoryIDX='.$langCateIDX.'';
        }
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        A.idx,
        A.momIDX,
        A.seq,
        A.subUrlIDX,
        A.status,
        C.idx AS langPackIDX,
        C.value
        FROM sendipay.QnaCategory AS A
        LEFT JOIN sendipay.MdSubUrl AS B ON A.subUrlIDX=B.idx
        LEFT JOIN sendipay.SystemLang AS C ON A.idx=C.targetIDX AND C.type=2
        WHERE A.momIDX='$idx' ".$langCateIDXQuery." ORDER BY A.seq
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaCon 숨김처리할때 status 조사
    public static function CheeseEyeAction($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        status
        FROM sendipay.QnaCategory
        WHERE idx='$idx'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaCon 업데이트전 해당 idx가 가지고 있는 SystemLang value값들과 idx들 가져오기
    public static function GetCategoryLangData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        B.idx,
        B.langCategoryIDX,
        B.value,
        C.code
        FROM sendipay.QnaCategory AS A
        JOIN sendipay.SystemLang AS B ON A.idx=B.targetIDX AND B.type=2
        JOIN sendipay.MdLangCategory AS C ON B.langCategoryIDX=C.idx
        WHERE A.idx='$idx'
        ORDER BY C.idx
        ");
        $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaCon 카테고리 삭제전 1차 카테고리에 등록되있는 2차 카테고리 조사
    public static function issetGrandMomIDXData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $CategoryList = $db->query("SELECT
        idx,
        momIDX,
        status
        FROM sendipay.QnaCategory
        WHERE momIDX='$idx'
        ");
        $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaDetailCon 해당 카테고리 idx
    public static function GetCategoryDataForIDX($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $sel = $db->query("SELECT
        A.idx,
        A.momIDX
        FROM sendipay.QnaCategory AS A
        WHERE A.idx='$idx'
        ");
        $result=$sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaDetailCon 언어에 맞는 카테고리 불러오기
    public static function GetCategoryListForParentIDX($data=null)
    {
        //내부모idx를 줄테니 형제들을 주시오
        $tmpParentIDX=$data['parentIDX'];//1차가 필요할땐 부모idx가 아니라 subUrlIDX 임
        $tmpType=$data['type'];//내가 몇차인지: 2차리스트가 필요하면 2, 3차리스트가 필요하면 3
        // $langCateIDX=$data['langCateIDX'];//어떤 언어로 문의가 들어왔는지
        $tmpQuery='';
        if($tmpType=='1'){//1차 리스트주세요
            $tmpQuery=' A.momIDX="0" AND A.subUrlIDX="'.$tmpParentIDX.'"';
        }elseif($tmpType=='2'){//2차 리스트주세요
            $tmpQuery=' A.momIDX="'.$tmpParentIDX.'"';
        }
        // else{//3차 리스트주세요
        //     $tmpQuery=' A.momIDX="'.$tmpParentIDX.'"';
        // }
        $db = static::getDB();
        $sel = $db->query("SELECT
        A.idx,
        A.status,
        B.value
        FROM sendipay.QnaCategory AS A
        JOIN sendipay.SystemLang AS B ON A.idx=B.targetIDX AND B.type=2
        WHERE B.langCategoryIDX=2
        AND ".$tmpQuery."
        ORDER BY A.seq
        ");
        $result=$sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // //카테고리관리03 = 2차까지만 하기로함
    // public static function GetCategoryChildrenMenuList($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.subUrlIDX,
    //     A.status,
    //     B.idx AS langPackIDX,
    //     B.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.SystemLang AS B ON A.idx=B.targetIDX AND B.type=2
    //     WHERE A.momIDX='$idx' AND A.status=3 ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }


    //------------------------------------------------------



    /*20230605 기존 QnaCategoryMo에 있던 데이터들 문제없으면 10일 최종 삭제예정*/

    // public static function getCateList($data=null)
    // {
    //     $cateType=$data['type'];
    //     $subUrlCode=$data['subUrlCode'];
    //     $cateQuery='';

    //     if($cateType=="child"){
    //         $cateQuery=' AND A.grandMomIDX!=0';
    //     }else if($cateType=="mom"){
    //         $cateQuery=' AND A.grandMomIDX=0';
    //     }

    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.langCateIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.status,
    //     B.idx AS langIDX,
    //     C.value AS name,
    //     D.idx AS langCateIDX
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     LEFT JOIN sendipay.MdLangPage AS E
    //     ON (B.langPageIDX = E.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS F
    //     ON (A.subUrlIDX = F.idx)
    //     WHERE A.momIDX=0 AND F.subUrl='$subUrlCode' AND A.status=3".$cateQuery."
    //     ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // //카테고리관리01
    // public static function QnaCategoryList($data=null)
    // {
    //     $subUrlCode=$data['subUrlCode'];
    //     $langPageIDX = $data['langPageIDX'];
    //     $langCateIDX = $data['langCateIDX'];
    //     $langPageIDXQuery='';
    //     $langCateIDXQuery='';
    //     if($langPageIDX!=='all'){
    //         $langPageIDXQuery=' AND (B.langPageIDX='.$langPageIDX.')';
    //     }
    //     if($langCateIDX!=='all'){
    //         $langCateIDXQuery=' AND (D.idx='.$langCateIDX.')';
    //     }
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.mdLangIDX,
    //     A.subUrlIDX,
    //     A.status,
    //     B.idx AS langIDX,
    //     B.code,
    //     C.value AS name,
    //     C.idx AS langPackIDX,
    //     D.idx AS langCateIDX
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     LEFT JOIN sendipay.MdLangPage AS E
    //     ON (B.langPageIDX = E.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS F
    //     ON (A.subUrlIDX = F.idx)
    //     WHERE A.grandMomIDX=0 AND A.momIDX=0 AND A.status=3 AND F.subUrl='$subUrlCode' ".$langPageIDXQuery."".$langCateIDXQuery."
    //     ORDER BY A.seq");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }
    // //카테고리관리02
    // public static function GetCategoryChildMenuList($data=null)
    // {
    //     $adminIDX=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.subUrlIDX,
    //     A.mdLangIDX,
    //     A.status,
    //     C.idx AS langPackIDX,
    //     C.value AS name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     WHERE A.grandMomIDX='$adminIDX' AND A.momIDX=0 AND A.status=3 AND D.idx=2 ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }
    // //카테고리관리03
    // public static function GetCategoryChildrenMenuList($data=null)
    // {
    //     $adminIDX=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.subUrlIDX,
    //     A.mdLangIDX,
    //     A.status,
    //     C.idx AS langPackIDX,
    //     C.value AS name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     WHERE A.momIDX='$adminIDX' AND D.idx=2 AND A.status=3 ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetCate($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     $langCateIDXQuery='';
    //     if($langCateIDX!=='all'){
    //         $langCateIDXQuery=' AND (D.idx='.$langCateIDX.')';
    //     }
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.subUrlIDX,
    //     A.mdLangIDX,
    //     A.status,
    //     B.code,
    //     C.idx AS langPackIDX,
    //     C.value AS name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     WHERE A.idx='$categoryIDX' ".$langCateIDXQuery." ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }


    // public static function CheeseEyeAction($data=null)
    // {
    //     $EditItemId=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     idx,
    //     momIDX,
    //     status
    //     FROM sendipay.QnaCategory
    //     WHERE idx='$EditItemId' ORDER BY seq
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function PartnerGetCategoryChildMenuList($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     A.seq,
    //     A.subUrlIDX,
    //     A.mdLangIDX,
    //     A.status,
    //     C.idx AS langPackIDX,
    //     C.value AS name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     WHERE A.grandMomIDX='$categoryIDX' AND A.status=3 AND A.momIDX=0 AND D.idx='$langCateIDX' ORDER BY A.seq
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // // public static function CategoryLangConfirmData($data=null)
    // // {
    // //     $db = static::getDB();
    // //     $CategoryList = $db->query("SELECT
    // //     B.idx,
    // //     B.name
    // //     FROM sendipay.QnaCategory AS A
    // //     LEFT JOIN sendipay.MdLangCategory AS B
    // //     ON(A.langCateIDX=B.idx)
    // //     ");
    // //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    // //     return $result;
    // // }

    // public static function CategoryMenuConfirmData($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     idx,
    //     name
    //     FROM sendipay.QnaCategory
    //     WHERE idx='$idx' ORDER BY seq
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function CategoryConfirmDelete($data=null)
    // {
    //     $EditItemId=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     idx,
    //     grandMomIDX,
    //     status
    //     FROM sendipay.QnaCategory
    //     WHERE grandMomIDX='$EditItemId'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function issetCategoryNameData($data=null)
    // {
    //     $subUrlCode=$data['subUrlCode'];
    //     $name=$data['name'];
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdSubUrl AS B
    //     ON(A.subUrlIDX=B.idx)
    //     WHERE A.name='$name' AND B.subUrl='$subUrlCode'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function categoryGroupData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.QnaQuestion AS B
    //     ON(A.idx=B.qnaCategoryIDX)
    //     WHERE B.ticketCode='$ticketCode'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function categoryGrandMomData($data=null)
    // {
    //     $cateIDX=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     B.name AS grandMomName,
    //     C.name AS momName,
    //     D.value AS name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.grandMomIDX=B.idx)
    //     LEFT JOIN sendipay.QnaCategory AS C
    //     ON(A.momIDX=C.idx)
    //     LEFT JOIN sendipay.LangPack AS D
    //     ON(A.mdLangIDX=D.langIDX AND D.cateIDX=1)

    //     WHERE A.idx='$cateIDX'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // //20230302

    // public static function PartnerCategoryGroupData($data=null)
    // {
    //     $qnaCategoryIDX=$data['qnaCategoryIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     B.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX)
    //     WHERE A.idx='$qnaCategoryIDX' AND B.cateIDX='$langCateIDX'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }
    // //qnaCategoryIDX에서 grandMomIDX가 물고있는 idx가 0이면 1차 / 아니면 2차

    // public static function PartnerCategoryGrandMomData($data=null)
    // {
    //     $grandMomIDX=$data['grandMomIDX'];
    //     $langCateIDX=$data['langCateIDX'];
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     B.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX)
    //     WHERE A.idx='$grandMomIDX' AND B.cateIDX='$langCateIDX'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // //20230302

    // public static function categoryMomData($data=null)
    // {
    //     $cateMomIDX=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     idx,
    //     name
    //     FROM sendipay.QnaCategory
    //     WHERE idx='$cateMomIDX'
    //     ");
    //     $result=$CategoryList->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }
    // public static function QnaCategoryChildChange($data=null)
    // {
    //     $targetIDX=$data;
    //     $db = static::getDB();
    //     $CategoryList = $db->query("SELECT
    //     A.idx,
    //     A.name,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     B.name AS grandMomName,
    //     C.name AS momName
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.grandMomIDX=B.idx)
    //     LEFT JOIN sendipay.QnaCategory AS C
    //     ON(A.momIDX=C.idx)
    //     WHERE A.idx='$targetIDX'
    //     ");
    //     $result=$CategoryList->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetQnaCategoryList($data=null)
    // {
    //     $subUrlCode=$data['subUrlCode'];
    //     $langCateCode=$data['langCateCode'];
    //     if($langCateCode=="ALL"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND D.code="'.$langCateCode.'"';
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX,
    //     D.name
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.mdLangIDX = B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON (C.cateIDX = D.idx)
    //     LEFT JOIN sendipay.MdLangPage AS E
    //     ON (B.langPageIDX = E.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS F
    //     ON (A.subUrlIDX = F.idx)
    //     WHERE F.subUrl='$subUrlCode' AND A.status=3 AND A.grandMomIDX=0 AND A.momIDX=0
    //     ".$langCateQuery."
    //     ORDER BY A.seq ASC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetApplicationQnaCategory($data=null)
    // {
    //     $langCateIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     C.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON(A.mdLangIDX=B.idx)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX)
    //     WHERE A.subUrlIDX=0 AND C.cateIDX='$langCateIDX'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }//파트너계약문의 카테고리 임시

    // public static function GetMomCategoryData($data=null)
    // {
    //     $idx=$data;

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     B.value,
    //     A.grandMomIDX,
    //     A.momIDX
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX AND B.cateIDX = 1)
    //     WHERE A.grandMomIDX='$idx' ORDER BY A.seq ASC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetMomAndCategoryData($data=null)
    // {
    //     $childCategoryIDX=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX AND B.cateIDX = 1)
    //     WHERE A.momIDX='$childCategoryIDX' AND A.status=3 ORDER BY A.seq ASC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }







    // public static function GetCategoryDataForIDX($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $sel = $db->query("SELECT
    //     A.idx,
    //     A.grandMomIDX,
    //     A.momIDX
    //     FROM sendipay.QnaCategory AS A
    //     WHERE A.idx='$idx'
    //     ");
    //     $result=$sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetCategoryLangCateIDX($data=null)
    // {
    //     $langCateCode=$data;
    //     $db = static::getDB();
    //     $sel = $db->query("SELECT
    //     B.idx
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.MdLangCategory AS B
    //     ON(A.langCateIDX=B.idx)
    //     WHERE B.Code='$langCateCode'
    //     ");
    //     $result=$sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetContractCategoryIDX($data=null)
    // {
    //     $langCateIDX=$data;
    //     $db = static::getDB();
    //     $sel = $db->query("SELECT
    //     A.idx,
    //     B.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX)
    //     WHERE A.idx=280 AND B.cateIDX='$langCateIDX'
    //     ");
    //     $result=$sel->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }//파트너 서비스명 입력 후 question,answer 테이블 인서트시 카테고리 찾기(임시로일단 하드코딩)

    // public static function GetCategoryListForParentIDX($data=null)
    // {
    //     //내부모idx를 줄테니 형제들을 주시오
    //     $tmpParentIDX=$data['parentIDX'];//1차가 필요할땐 부모idx가 아니라 subUrlIDX 임
    //     $tmpType=$data['type'];//내가 몇차인지: 2차리스트가 필요하면 2, 3차리스트가 필요하면 3
    //     $langCateIDX=$data['langCateIDX'];//어떤 언어로 문의가 들어왔는지
    //     $tmpQuery='';
    //     if($tmpType=='1'){//1차 리스트주세요
    //         $tmpQuery=' A.grandMomIDX="0" AND A.subUrlIDX="'.$tmpParentIDX.'"';
    //     }elseif($tmpType=='2'){//2차 리스트주세요
    //         $tmpQuery=' A.grandMomIDX="'.$tmpParentIDX.'" AND A.momIDX=0';
    //     }else{//3차 리스트주세요
    //         $tmpQuery=' A.momIDX="'.$tmpParentIDX.'"';
    //     }
    //     $db = static::getDB();
    //     $sel = $db->query("SELECT
    //     A.idx,
    //     A.status,
    //     B.value
    //     FROM sendipay.QnaCategory AS A
    //     LEFT JOIN sendipay.LangPack AS B
    //     ON(A.mdLangIDX=B.langIDX)
    //     WHERE A.status=3 AND B.cateIDX='$langCateIDX'
    //     AND ".$tmpQuery."
    //     ORDER BY A.seq
    //     ");
    //     $result=$sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }


}