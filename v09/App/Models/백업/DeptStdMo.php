<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class DeptStdMo extends \Core\Model
{
    //QnaDetailCon 랜더시 dept정보 , DeptCon 데이터테이블
    public static function GetDatatableList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name,
        CASE status
            WHEN 2 THEN '비활성화'
            WHEN 3 THEN '활성화'
            ELSE '' END
        AS status
        FROM sendipay.DeptStd
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }
}
