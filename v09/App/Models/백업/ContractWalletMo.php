<?php

namespace App\Models;
use PDO;
/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractWalletMo extends \Core\Model
{
	// !!manager도 여기서
	/**
	 * Get all the users as an associative array
	 * @return array
	 */

///////////////////////////////////////partnermanagerTable/////////////////////////////////////
    public static function GetContractWallet($data=null)
    {   
        $contractIDX=$data['contractIDX'];
        $managerIDX=$data['managerIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.gradeIDX,
        A.managerIDX,
        B.idx AS contractIDX,
        B.name AS contractName,
        B.balance AS contractBalance
        FROM sendipay.PartnerManager AS A
        LEFT JOIN sendipay.Partner AS B
        ON A.contractIDX = B.idx
        WHERE A.managerIDX='$managerIDX' AND A.contractIDX='$contractIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
   
}