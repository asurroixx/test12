<?php

namespace App\Models;

use PDO;

class MdSubUrlMo extends \Core\Model
{
    public static function get_subUrlCate($data=null)
    {
        $subUrlIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        subUrl
        FROM sendipay.MdSubUrl WHERE idx='$subUrlIDX'
        ");
        $subUrlCate=$Sel->fetch(PDO::FETCH_ASSOC);
        return $subUrlCate;
    }

    public static function SubUrlLoad($data=null)
    {
        $thisIDX=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        name
        FROM sendipay.MdSubUrl 
        ");
        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function SubUrlLoadBySupport($data=null)
    {
        $thisColName=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        name,
        subUrl
        FROM sendipay.MdSubUrl 
        WHERE $thisColName=3
        ");
        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
        return $faqListData;
    }
    public static function GetMdSubUrlIDXData($data=null)
    {
        $subUrlIDX=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx,
        subUrl,
        name
        FROM sendipay.MdSubUrl 
        ");
        $faqListData=$faqData->fetchAll(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function QnaCategorySearchCode($data=null)
    {
        $subUrlCode=$data;
        $db = static::getDB();
        $faqData = $db->query("SELECT
        idx
        FROM sendipay.MdSubUrl WHERE subUrl='$subUrlCode'
        ");
        $faqListData=$faqData->fetch(PDO::FETCH_ASSOC);
        return $faqListData;
    }

    public static function QnaTableCode($data=null)
    {
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.MdSubUrl WHERE subUrl='$code'
        ");
        $subUrlCate=$Sel->fetch(PDO::FETCH_ASSOC);
        if($code=='partnerApplication'){
            $subUrlIDX=0;
        }else{
            $subUrlIDX=$subUrlCate['idx'];
        }
        return $subUrlIDX;
    }

    /*-----------------202306 진짜사용-------------------*/

    // QnaCon 랜더시 코드에 맞는 idx
    public static function get_subUrlDetailByCode($data=null)
    {
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        name
        FROM sendipay.MdSubUrl WHERE subUrl='$code'
        ");
        $subUrlCate=$Sel->fetch(PDO::FETCH_ASSOC);
        return $subUrlCate;
    }

}