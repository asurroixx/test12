<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class PortalAlarmMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
    public static function get_MyAlarmNotRead($data=null)
    {   
        $contractManagerIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT 
        idx,
        type,
        count(case when type=3 then 1 end) as withdrawalCount,
        count(case when type=2 then 1 end) as transactionCount,
        count(case when type=4 then 1 end) as qnaCount
        FROM sendipay.PotalAlarm 
        WHERE status=2 AND contractManagerIDX='$contractManagerIDX'
        GROUP BY type
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }
}