<?php

namespace App\Models;

use PDO;

class QnaQuestionMo extends \Core\Model
{

    // public static function get_myQnaList($data=null)
    // {
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $customerIDX=$data['customerIDX'];
    //     // $langCateIDX=$data['langCateIDX'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.createTime,
    //     A.status,
    //     A.ticketCode,
    //     A.customerIDX,
    //     A.qnaCategoryIDX,
    //     B.grandMomIDX
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX = B.idx)
    //     WHERE A.subUrlIDX='$subUrlIDX'
    //     AND A.customerIDX='$customerIDX' AND B.subUrlIDX='$subUrlIDX'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }
    // //grandMomIDX가 물고있는 idx가 1차임
    // //mdLangIDX가 언어에 물고있는 value 값(2차란 소리)

    // public static function get_PortalMyQnaList($data=null)//포탈 리스트
    // {
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $contractIDX=$data['contractIDX'];
    //     $contractManagerIDX=$data['contractManagerIDX'];
       
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.createTime,
    //     A.status,
    //     A.ticketCode,
    //     A.customerIDX,
    //     C.name AS managerName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX = B.idx)
    //     LEFT JOIN sendipay.ContractManager AS C
    //     ON(A.customerIDX=C.contractIDX)
    //     LEFT JOIN sendipay.MdLang AS D
    //     ON(B.mdLangIDX=D.idx)
        
    //     WHERE A.subUrlIDX='$subUrlIDX'
    //     AND C.contractIDX='$contractIDX' AND C.idx='$contractManagerIDX'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetAdminQnaList($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.status,
    //     A.ticketCode,
    //     B.name
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.QnaAnswer AS C
    //     ON(A.idx=C.questionIDX)
    //     WHERE C.type=4 OR C.type=5 GROUP BY C.questionIDX
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }



    // //파트너문의
    // public static function GetQnaList($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $subUrlCode=$data['subUrlCode'];
    //     $langCateCode=$data['langCateCode'];
    //     if($categoryIDX=="all"){
    //         $thisQuery='';
    //     }else{
    //         // $thisQuery=' AND A.qnaCategoryIDX='.$categoryIDX;
    //         $thisQuery=' AND (B.idx='.$categoryIDX.' OR B.momIDX='.$categoryIDX.' OR B.grandMomIDX='.$categoryIDX.')';
    //     }
    //     if($langCateCode=="ALL"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND D.code="'.$langCateCode.'"';
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.langCateIDX,
    //     A.title,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.customerIDX,
    //     B.idx AS categoryIDX,
    //     B.name AS qnaCategoryName,
    //     B.momIDX,
    //     C.email AS directorEmail,
    //     D.code,
    //     F.name AS managerEmail,
    //     G.email AS staffEmail,
    //     G.name AS staffName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.Director AS C
    //     ON(A.customerIDX=C.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON(A.langCateIDX=D.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS E
    //     ON(A.subUrlIDX=E.idx)
    //     LEFT JOIN sendipay.Partner AS F
    //     ON(A.customerIDX=F.idx)
    //     LEFT JOIN sendipay.Staff AS G
    //     ON(A.staffIDX=G.idx)

    //     WHERE E.subUrl='$subUrlCode'
    //     ".$thisQuery
    //     .$langCateQuery.
    //     "
    //     ORDER BY A.status ASC , A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // //230313 포탈문의
    // public static function GetBBQnaList($data=null)
    // {
    //     $subUrlCode=$data['subUrlCode'];
    //     $startDate=$data['startDate'];
    //     $endDate=$data['endDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.langCateIDX,
    //     A.title,
    //     A.status,
    //     A.answerStatus,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.customerIDX,
    //     B.idx AS categoryIDX,
    //     B.grandMomIDX,
    //     C.code,
    //     E.name AS partnerName,
    //     F.email AS staffEmail,
    //     F.name AS staffName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS C
    //     ON(A.langCateIDX=C.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS D
    //     ON(A.subUrlIDX=D.idx)
    //     LEFT JOIN sendipay.Partner AS E
    //     ON(A.customerIDX=E.idx)
    //     LEFT JOIN sendipay.Staff AS F
    //     ON(A.staffIDX=F.idx)
    //     WHERE
    //     (A.createTime BETWEEN '$startDate' AND '$endDate')
    //     AND D.subUrl='$subUrlCode' AND B.subUrlIDX!=0
    //     ORDER BY A.status ASC , A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // //230221 파트너문의
    // public static function getAAQnaList($data=null)
    // {
    //     $subUrlCode=$data['subUrlCode'];
    //     $startDate=$data['startDate'];
    //     $endDate=$data['endDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.langCateIDX,
    //     A.title,
    //     A.status,
    //     A.answerStatus,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.customerIDX,
    //     B.idx AS categoryIDX,
    //     B.name AS qnaCategoryName,
    //     B.langCateIDX,
    //     B.grandMomIDX,
    //     C.email AS directorEmail,
    //     D.code,
    //     F.name AS managerEmail,
    //     G.email AS staffEmail,
    //     G.name AS staffName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.Director AS C
    //     ON(A.customerIDX=C.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON(A.langCateIDX=D.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS E
    //     ON(A.subUrlIDX=E.idx)
    //     LEFT JOIN sendipay.Partner AS F
    //     ON(A.customerIDX=F.idx)
    //     LEFT JOIN sendipay.Staff AS G
    //     ON(A.staffIDX=G.idx)
    //     WHERE
    //     (A.createTime BETWEEN '$startDate' AND '$endDate')
    //     AND E.subUrl='$subUrlCode' AND B.subUrlIDX!=0
    //     ORDER BY A.status ASC , A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // //회원문의
    // public static function GetMemberQnaList($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $subUrlCode=$data['subUrlCode'];
    //     $langCateCode=$data['langCateCode'];
    //     if($categoryIDX=="all"){
    //         $thisQuery='';
    //     }else{
    //         // $thisQuery=' AND A.qnaCategoryIDX='.$categoryIDX;
    //         $thisQuery=' AND (B.idx='.$categoryIDX.' OR B.momIDX='.$categoryIDX.' OR B.grandMomIDX='.$categoryIDX.')';
    //     }
    //     if($langCateCode=="ALL"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND D.code="'.$langCateCode.'"';
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     B.name AS qnaCategoryName,
    //     C.nickName,
    //     D.code,
    //     F.name AS staffName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.Member AS C
    //     ON(A.customerIDX=C.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON(A.langCateIDX=D.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS E
    //     ON(A.subUrlIDX=E.idx)
    //     LEFT JOIN sendipay.Staff AS F
    //     ON(A.staffIDX=F.idx)
    //     WHERE E.subUrl='$subUrlCode'
    //     ".$thisQuery
    //     .$langCateQuery.
    //     "
    //     ORDER BY A.status ASC , A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }


    // //파트너신청문의 테이블
    // public static function GetApplicationQnaList($data=null)
    // {
    //     $categoryIDX=$data['categoryIDX'];
    //     $subUrlCode=$data['subUrlCode'];
    //     $langCateCode=$data['langCateCode'];
    //     if($categoryIDX=="all"){
    //         $thisQuery=' AND (B.idx=280) ';
    //         //계약서 문의 카테고리만 받으면됨
    //         //임시 하드코딩
    //     }else{
    //         // $thisQuery=' AND A.qnaCategoryIDX='.$categoryIDX;
    //         $thisQuery=' AND (B.idx='.$categoryIDX.' OR B.momIDX='.$categoryIDX.' OR B.grandMomIDX='.$categoryIDX.')';
    //     }
    //     if($langCateCode=="ALL"){
    //         $langCateQuery='';
    //     }else{
    //         $langCateQuery=' AND D.code="'.$langCateCode.'"';
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.title,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     C.email AS directorEmail,
    //     D.code,
    //     F.email AS staffEmail,
    //     F.name AS staffName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.Director AS C
    //     ON(A.customerIDX=C.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON(A.langCateIDX=D.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS E
    //     ON(A.subUrlIDX=E.idx)
    //     LEFT JOIN sendipay.Staff AS F
    //     ON(A.staffIDX=F.idx)
    //     WHERE E.subUrl='$subUrlCode'
    //     ".$thisQuery
    //     .$langCateQuery.
    //     "
    //     ORDER BY A.status ASC , A.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetQnaDetail($data=null)
    // {
    //     $ticketCode=$data;

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.subUrlIDX,
    //     A.customerIDX,
    //     A.langCateIDX,
    //     A.title,
    //     A.status,
    //     A.createTime,
    //     A.ticketCode,
    //     A.qnaCategoryIDX,
    //     A.returnEmail,
    //     A.csCount,
    //     A.itCount,
    //     B.idx AS categoryIDX,
    //     B.momIDX,
    //     C.name AS subUrlName,
    //     C.idx AS MdSubUrlIDX,
    //     C.subUrl AS subUrlCode,
    //     D.questionIDX,
    //     D.contents AS contents,
    //     D.createTime AS answerTime,
    //     D.type,
    //     D.useIDX,
    //     E.email AS staffEmail
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaCategory AS B
    //     ON(A.qnaCategoryIDX=B.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS C
    //     ON(A.subUrlIDX=C.idx)
    //     LEFT JOIN sendipay.QnaAnswer AS D
    //     ON(A.idx=D.questionIDX)
    //     LEFT JOIN sendipay.Staff AS E
    //     ON(A.staffIDX=E.idx)
    //     WHERE A.ticketCode='$ticketCode' ORDER BY D.createTime DESC
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function issetQnaListByQnaCateIDX($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.QnaQuestion
    //     WHERE qnaCategoryIDX='$idx'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetReplyTicketCodeSearchData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.customerIDX
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.QnaAnswer AS B
    //     ON(A.idx=B.questionIDX)
    //     WHERE A.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetTicketCodeData($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     ticketCode
    //     FROM sendipay.QnaQuestion
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetUserInfoDataLoad($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.subUrlIDX,
    //     A.ticketCode,
    //     A.customerIDX,
    //     B.email,
    //     C.name AS subUrlName,
    //     D.name AS langCateName,
    //     F.email AS memberEmail
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.Director AS B
    //     ON(A.customerIDX=B.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS C
    //     ON(A.subUrlIDX=C.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS D
    //     ON(A.langCateIDX=D.idx)
    //     LEFT JOIN sendipay.PartnerManager AS E
    //     ON(A.customerIDX=E.partnerIDX)
    //     LEFT JOIN sendipay.Manager AS F
    //     ON(E.managerIDX=F.idx)
    //     WHERE A.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }//추후 삭제예정

    // public static function GetMemberInfoDataLoad($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.subUrlIDX,
    //     A.customerIDX,
    //     B.name AS subUrlName,
    //     C.name AS langCateName,
    //     D.nickName,
    //     D.walletAddr
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.MdSubUrl AS B
    //     ON(A.subUrlIDX=B.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS C
    //     ON(A.langCateIDX=C.idx)
    //     LEFT JOIN sendipay.Member AS D
    //     ON(A.customerIDX=D.idx)
    //     WHERE A.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }//멤버(앱)에 대한 유저정보

    // public static function GetContractInfoDataLoad($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.subUrlIDX,
    //     A.customerIDX,
    //     B.name AS subUrlName,
    //     C.name AS langCateName,
    //     D.idx AS directorIDX,
    //     D.email AS directorEmail,
    //     D.name AS directorName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.MdSubUrl AS B
    //     ON(A.subUrlIDX=B.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS C
    //     ON(A.langCateIDX=C.idx)
    //     LEFT JOIN sendipay.Director AS D
    //     ON(A.customerIDX=D.idx)
    //     WHERE A.ticketCode='$ticketCode'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }//파트너문의에 대한 유저정보 20230215
    // // 파트너정보를 어떻게 뿌릴지 생각해야함

    // public static function GetContractManagerInfoDataLoad($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     A.subUrlIDX,
    //     A.customerIDX,
    //     B.name AS subUrlName,
    //     C.name AS langCateName,
    //     D.idx AS managerIDX,
    //     E.name,
    //     E.email,
    //     F.serviceName,
    //     F.name AS contractName,
    //     F.homepage
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.MdSubUrl AS B
    //     ON(A.subUrlIDX=B.idx)
    //     LEFT JOIN sendipay.MdLangCategory AS C
    //     ON(A.langCateIDX=C.idx)
    //     LEFT JOIN sendipay.ContractManager AS D
    //     ON(A.customerIDX=D.idx)
    //     LEFT JOIN sendipay.Manager AS E
    //     ON(D.managerIDX=E.idx)
    //     LEFT JOIN sendipay.Contract AS F
    //     ON(D.contractIDX=F.idx)
    //     WHERE A.ticketCode = '$ticketCode'
    //     ");
    //     $result=$partnerData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }//포탈 매니저에 대한 정보 , 일단 manager로 보여주고 추후 partnerManager로 수정해야하면 수정해야함

    // public static function GetDirectorEmailData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     B.idx,
    //     B.partnerIDX,
    //     B.email AS directorEmail,
    //     B.name AS directorName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.Director AS B
    //     ON(A.customerIDX=B.idx)
    //     WHERE A.ticketCode = '$ticketCode'
    //     ");
    //     $result=$partnerData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }//파트너 디렉터에 대한 정보

    // public static function GetBeforeTicketCodeData($data=null)
    // {
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $customerIDX=$data['customerIDX'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     ticketCode,
    //     title
    //     FROM sendipay.QnaQuestion
    //     WHERE subUrlIDX='$subUrlIDX' AND customerIDX='$customerIDX'
    //     ");
    //     $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }

    // public static function GetUserQuestionStatusData($data=null)
    // {
    //     $subUrlIDX=$data['subUrlIDX'];
    //     $paGbLoginIDX=$data['paGbLoginIDX'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     subUrlIDX,
    //     status,
    //     customerIDX
    //     FROM sendipay.QnaQuestion
    //     WHERE subUrlIDX='$subUrlIDX' AND customerIDX='$paGbLoginIDX'
    //     ");
    //     $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $qnaList;
    // }


    // public static function AnswerMemberData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     B.nickName
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.Member AS B
    //     ON(A.customerIDX=B.idx)
    //     WHERE A.ticketCode = '$ticketCode'
    //     ");
    //     $result=$partnerData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function chargeStaffData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     A.staffIDX,
    //     B.email
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.Staff AS B
    //     ON(A.staffIDX=B.idx)
    //     WHERE ticketCode = '$ticketCode'
    //     ");
    //     $result=$partnerData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    // public static function GetBeforeStatusData($data=null)
    // {
    //     $ticketCode=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     A.idx,
    //     A.status,
    //     A.returnEmail,
    //     B.email AS chargeEmail
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.Staff AS B
    //     ON(A.staffIDX=B.idx)
    //     WHERE ticketCode = '$ticketCode'
    //     ");
    //     $result=$partnerData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }//스태프가 답변 다는 도중 status 3이나 4일때 유효성
    // //스태프 페이지 파트너 신청 답변달때 idx,returnEmail 값찾을때

    // public static function QnaLangAndCategoryCount($data=null)
    // {
    //     $subUrlCode=$data;
    //     // $langCateCode=$data['langCateCode'];
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     A.idx,
    //     A.langCateIDX,
    //     A.qnaCategoryIDX
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.MdLangCategory AS B
    //     ON(A.langCateIDX=B.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS C
    //     ON(A.subUrlIDX=C.idx)
    //     WHERE C.subUrl='$subUrlCode'
    //     ");
    //     $result=$partnerData->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }//스태프가 답변 다는 도중 status 3이나 4일때 유효성

    // public static function ApplicationQnaLangAndCategoryCount($data=null)
    // {
    //     $subUrlCode=$data;
    //     // $langCateCode=$data['langCateCode'];
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT
    //     A.idx,
    //     A.langCateIDX,
    //     A.qnaCategoryIDX
    //     FROM sendipay.QnaQuestion AS A
    //     LEFT JOIN sendipay.MdLangCategory AS B
    //     ON(A.langCateIDX=B.idx)
    //     LEFT JOIN sendipay.MdSubUrl AS C
    //     ON(A.subUrlIDX=C.idx)
    //     WHERE C.subUrl='$subUrlCode' AND (A.qnaCategoryIDX=103 OR A.qnaCategoryIDX=133 OR A.qnaCategoryIDX=162 OR A.qnaCategoryIDX=190)
    //     ");
    //     //카테고리idx들 임시
    //     $result=$partnerData->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }//파트너신청문의 카운트(기존 파트너문의는 파트너에 대한 모든 질문이 들어오기 때문에 모든 질문 카운트가 필요하지만 파트너계약관리는 계약서 신청건 외에는 필요가없음)



    // public static function GetPartnerQnaCount($data=null)
    // {
    //     $PaGbLoginIDX=$data;
    //     $db = static::getDB();
    //     $partnerData = $db->query("SELECT EXISTS (SELECT
    //         idx
    //         FROM sendipay.QnaQuestion
    //         WHERE customerIDX = '$PaGbLoginIDX' AND subUrlIDX = 2)AS CNT
    //     ");

    //     $result=$partnerData->fetchAll(PDO::FETCH_ASSOC);
    //     $count = $result[0]['CNT'];
    //     return $count;
    // }


/*-----------------------------20230531 JB 진짜사용--------------------------------------*/

    // QnaCon 해당 카테고리에 등록되있는 질문idx들 조사
    public static function issetQnaDataByCateIDX($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.QnaQuestion
        WHERE qnaCategoryIDX='$idx'
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaCon 공통 데이터테이블
    public static function GetDatatable($data=null)
    {
        $subUrlIDX=$data['subUrlIDX'];
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        $subUrlCode=$data['subUrlCode'];

        $codeQuery='';
        if($subUrlCode=='partnerApplication'){
            $codeQuery=' AND (B.idx=282 OR B.momIDX=282)';
        }
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        CASE
            WHEN A.subUrlIDX = 0 THEN (SELECT email FROM sendipay.Director WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 1 THEN (SELECT name FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 2 THEN (SELECT email FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 3 THEN (SELECT email FROM sendipay.Manager WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 4 THEN (SELECT name FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 5 THEN (SELECT nickName FROM sendipay.Member WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 6 THEN (SELECT nickName FROM sendipay.Member WHERE idx = A.customerIDX)
        END AS customerName,
        A.title,
        CASE A.status
            WHEN 2 THEN 'Open'
            WHEN 3 THEN 'Close'
            WHEN 4 THEN 'Cancel'
            ELSE '' END
        AS status,
        CASE A.answerStatus
            WHEN 2 THEN '답변대기중'
            WHEN 3 THEN '답변완료'
            ELSE '' END
        AS answerStatus,
        A.createTime,
        A.ticketCode,
        C.value AS qnaCategoryName,
        IFNULL(D.name,'-') AS staffName
        FROM sendipay.QnaQuestion AS A
        JOIN sendipay.QnaCategory AS B ON A.qnaCategoryIDX=B.idx
        JOIN sendipay.SystemLang AS C ON B.idx=C.targetIDX AND C.type=2 AND C.langCategoryIDX=2
        LEFT JOIN sendipay.Staff AS D ON A.staffIDX=D.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        AND A.subUrlIDX='$subUrlIDX' ".$codeQuery." AND (A.status=2 or A.status=3)
        ORDER BY A.status ASC , A.createTime DESC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    //QnaCon dev서버 데이터테이블
    public static function GetDevQnaList($data=null)
    {
        $devDb = static::getDevDB();
        $DevSel = $devDb->query("SELECT
        A.idx,
        A.title,
        CASE A.status
            WHEN 2 THEN 'Open'
            WHEN 3 THEN 'Close'
            ELSE '' END
        AS status,
        CASE A.answerStatus
            WHEN 2 THEN '답변대기중'
            WHEN 3 THEN '답변완료'
            ELSE '' END
        AS answerStatus,
        A.createTime,
        A.ticketCode,
        (SELECT email FROM sendipay.Director WHERE idx=A.customerIDX) AS customerName,
        (SELECT createTime FROM sendipay.QnaAnswer WHERE questionIDX=A.idx GROUP BY createTime DESC LIMIT 1) AS updateTime,
        C.value AS qnaCategoryName,
        IFNULL(D.name,'-') AS staffName
        FROM sendipay.QnaQuestion AS A
        JOIN sendipay.QnaCategory AS B ON A.qnaCategoryIDX=B.idx
        JOIN sendipay.SystemLang AS C ON B.idx=C.targetIDX AND C.type=2 AND C.langCategoryIDX=2
        LEFT JOIN sendipay.Staff AS D ON A.staffIDX=D.idx
        WHERE (A.status=2 or A.status=3)
        ");
        $result=$DevSel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    // QnaDetailCon 디테일 랜더
    public static function GetQnaDetailData($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.subUrlIDX,
        CASE
            WHEN A.subUrlIDX = 0 THEN (SELECT idx FROM sendipay.Director WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 1 THEN (SELECT idx FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 2 THEN (SELECT idx FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 3 THEN (SELECT idx FROM sendipay.ContractManager WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 4 THEN (SELECT idx FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 5 THEN (SELECT idx FROM sendipay.Member WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 6 THEN (SELECT idx FROM sendipay.Member WHERE idx = A.customerIDX)
        END AS customerIDX,
        CASE
            WHEN A.subUrlIDX = 0 THEN (SELECT email FROM sendipay.Director WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 1 THEN (SELECT name FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 2 THEN (SELECT email FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 3 THEN (SELECT name FROM sendipay.ContractManager WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 4 THEN (SELECT name FROM sendipay.Director WHERE idx = A.customerIDX)
            WHEN A.subUrlIDX = 5 THEN (SELECT nickName FROM sendipay.Member WHERE idx = A.customerIDX)
            -- WHEN A.subUrlIDX = 6 THEN (SELECT nickName FROM sendipay.Member WHERE idx = A.customerIDX)
        END AS customerName,
        A.title,
        CASE A.status
            WHEN 2 THEN 'Open'
            WHEN 3 THEN 'Close'
            WHEN 4 THEN 'Cancel'
            ELSE '' END
        AS status,
        CASE A.answerStatus
            WHEN 2 THEN '답변대기중'
            WHEN 3 THEN '답변완료'
            ELSE '' END
        AS answerStatus,
        A.createTime,
        A.qnaCategoryIDX,
        A.ticketCode,
        A.returnEmail,
        B.idx AS categoryIDX,
        -- C.value AS cateValue, /*질문이 어떤 언어로 들어오냐에 따라 카테고리가 달라짐*/
        IFNULL(D.subUrl,'partnerApplication') AS subUrlCode,
        IFNULL(D.name,'계약문의') AS subUrlName,
        IFNULL(E.email,'담당자가 지정되지 않았습니다.') AS staffEmail
        FROM sendipay.QnaQuestion AS A
        LEFT JOIN sendipay.QnaCategory AS B ON A.qnaCategoryIDX=B.idx
        -- JOIN sendipay.SystemLang AS C ON B.idx=C.targetIDX AND C.type=2 AND C.langCategoryIDX=2
        LEFT JOIN sendipay.MdSubUrl AS D ON A.subUrlIDX=D.idx
        LEFT JOIN sendipay.Staff AS E ON A.staffIDX=E.idx
        WHERE A.ticketCode='$ticketCode'
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon dev 디테일 랜더
    public static function DevGetQnaDetailData($data=null)
    {
        $ticketCode=$data;
        $devDb = static::getDevDB();
        $Sel = $devDb->query("SELECT
        A.idx,
        A.subUrlIDX,
        A.title,
        CASE A.status
            WHEN 2 THEN 'Open'
            WHEN 3 THEN 'Close'
            ELSE '' END
        AS status,
        CASE A.answerStatus
            WHEN 2 THEN '답변대기중'
            WHEN 3 THEN '답변완료'
            ELSE '' END
        AS answerStatus,
        A.createTime,
        A.qnaCategoryIDX,
        A.ticketCode,
        A.returnEmail,
        B.idx AS categoryIDX,
        C.value,
        D.idx AS customerIDX,
        D.email AS customerName,
        '개발자센터' AS subUrlName
        FROM sendipay.QnaQuestion AS A
        JOIN sendipay.QnaCategory AS B ON A.qnaCategoryIDX=B.idx
        JOIN sendipay.SystemLang AS C ON B.idx=C.targetIDX AND C.type=2 AND C.langCategoryIDX=2
        JOIN sendipay.Director AS D ON A.customerIDX=D.idx
        WHERE A.ticketCode='$ticketCode'
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    // QnaDetailCon 이전질문 데이터
    public static function GetBeforeData($data=null)
    {
        $subUrlIDX=$data['subUrlIDX'];
        $customerIDX=$data['customerIDX'];
        if($subUrlIDX==6){
            $db = static::getDevDB();
        }else{
            $db = static::getDB();
        }
        $Sel = $db->query("SELECT
        ticketCode,
        title
        FROM sendipay.QnaQuestion
        WHERE subUrlIDX='$subUrlIDX' AND customerIDX='$customerIDX' AND (status=2 or status=3)
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    /*QnaDetailCon 담당스태프 변경시 스태프 조사,
    QnaDetailCon 유저메모 해당 customerIDX 찾기,
    QnaDetailCon 스태프가 답변 다는 도중 status 3이나 4일때 유효성*/
    public static function GetTargetData($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $partnerData = $db->query("SELECT
        A.customerIDX,
        A.staffIDX,
        A.status,
        B.email
        FROM sendipay.QnaQuestion AS A
        LEFT JOIN sendipay.Staff AS B ON A.staffIDX=B.idx
        WHERE A.ticketCode = '$ticketCode'
        ");
        $result=$partnerData->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    /*dev들!!!!!!!!
    QnaDetailCon 담당스태프 변경시 스태프 조사,
    QnaDetailCon 유저메모 해당 customerIDX 찾기,
    QnaDetailCon 스태프가 답변 다는 도중 status 3이나 4일때 유효성*/
    public static function DevGetTargetData($data=null)
    {
        $ticketCode=$data;
        $devDb = static::getDevDB();
        $partnerData = $devDb->query("SELECT
        A.customerIDX,
        A.staffIDX,
        A.status,
        B.staffEmail AS email
        FROM sendipay.QnaQuestion AS A
        LEFT JOIN sendipay.QnaAnswer AS B ON A.idx=B.questionIDX
        WHERE A.ticketCode = '$ticketCode'
        ");
        $result=$partnerData->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //StMemberCon staff member 테이블 디테일 질문내역
    public static function GetUserQuestionData($data=null)
    {
        $subUrlIDX=$data['subUrlIDX'];
        $customerIDX=$data['customerIDX'];
        $db = static::getDB();
        $partnerData = $db->query("SELECT
        idx,
        ticketCode,
        title,
        createTime
        FROM sendipay.QnaQuestion
        WHERE subUrlIDX='$subUrlIDX' AND customerIDX='$customerIDX'
        ");
        $result=$partnerData->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }










































}