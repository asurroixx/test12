<?php

namespace App\Models;

use PDO;

class NoticeMo extends \Core\Model
{
    public static function get_noticeList($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        // $startDate=$data['startDate'];
        // $endDate=$data['endDate'];

        if($langCateCode=="ALL"){
            $langCateQuery='';
        }else{
            $langCateQuery=' AND C.code="'.$langCateCode.'"';
        }

        // if($startDate==""){ 
        //     $startDate='1970-01-01 00:00:00'; 
        // }else{
        //     $startDate.=" 00:00:00";
        // }
        // if($endDate==""){ 
        //     $endDate=date('Y-m-d 23:59:59');
        // }else{
        //     $endDate.=" 23:59:59";
        // }

        // AND (A.createTime BETWEEN '$startDate' AND '$endDate') 

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.title,
        A.contents,
        A.createTime,
        A.staffIDX,
        A.subUrlIDX,
        B.name AS staffName,
        C.name AS langCateName
        FROM sendipay.Notice AS A
        LEFT JOIN sendipay.Staff AS B
        ON (A.staffIDX = B.idx)
        LEFT JOIN sendipay.MdLangCategory AS C
        ON (A.langCateIDX = C.idx)
        LEFT JOIN sendipay.MdSubUrl AS D
        ON (A.subUrlIDX = D.idx)
        WHERE D.subUrl='$subUrlCode' 
        ".$langCateQuery);
        $noticeList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $noticeList;
    }

    public static function get_noticeListDetail($data=null)
    {
        $targetIDX=$data['idx']*1;
        $subUrlCode=$data['subUrlCode'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.subUrlIDX,
        A.createTime,
        A.title,
        A.contents,
        A.staffIDX,
        B.name AS staffName,
        C.name AS subUrlName,
        D.code AS langCateCode
        FROM sendipay.Notice AS A
        LEFT JOIN sendipay.Staff AS B
        ON (A.staffIDX = B.idx)
        LEFT JOIN sendipay.MdSubUrl AS C
        ON (A.subUrlIDX = C.idx)
        LEFT JOIN sendipay.MdLangCategory AS D
        ON (A.langCateIDX = D.idx)
        WHERE A.idx='$targetIDX' AND C.subUrl='$subUrlCode' 
        ");
        $NoticeDetail=$Sel->fetch(PDO::FETCH_ASSOC);
        return $NoticeDetail;
    }

    public static function GetSubUrlNameData($data=null)
    {
        $subUrlCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        B.name
        FROM sendipay.Notice AS A
        LEFT JOIN sendipay.MdSubUrl AS B
        ON (A.subUrlIDX = B.idx)
        WHERE B.subUrl='$subUrlCode'
        ");
        $NoticeDetail=$Sel->fetch(PDO::FETCH_ASSOC);
        return $NoticeDetail;
    }

    public static function GetDashboardNotice($data=null)
    {
        $subUrlCode=$data['subUrlCode'];
        $langCateCode=$data['langCateCode'];
        if($langCateCode=="ALL"){
            $langCateQuery='';
        }else{
            $langCateQuery=' AND B.code="'.$langCateCode.'"';
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.title,
        A.createTime,
        C.name AS staffName
        FROM sendipay.Notice AS A
        LEFT JOIN sendipay.MdSubUrl AS B
        ON (A.subUrlIDX = B.idx)
        LEFT JOIN sendipay.Staff AS C
        ON (A.staffIDX=C.idx)
        WHERE B.subUrl='$subUrlCode' ORDER BY A.createTime DESC LIMIT 5
        ".$langCateQuery);
        $NoticeDetail=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $NoticeDetail;
    }
}