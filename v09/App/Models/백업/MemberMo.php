<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MemberMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */



    /*확인해봐야할것 202306*/
    
    // public static function GetMemberDetail($data=null)
    // {
    //     $sendType=$data['sendType'];
    //     $contentType=$data['alarmType'];

    //     switch ($contentType) {
    //         case 1: $contentTypeName = 'pushTransaction'; break;
    //         case 2: $contentTypeName = 'pushPayment'; break;
    //         case 3: $contentTypeName = 'pushNotice'; break;
    //         default: break;
    //     }
    //     $whereTxt='';
    //     if($sendType == 'specified'){ //개별
    //         $sendUserIdx=$data['sendUserIdx'];
    //         $whereTxt = 'idx = '.$sendUserIdx.' AND '.$contentTypeName.' = 3';
    //     }elseif($sendType == 'allowed'){ //허용된 전체
    //         $whereTxt = ''.$contentTypeName.' = 3';
    //     }elseif($sendType == 'Regardless'){ //완전 전체
    //         // $whereTxt = 'loginToken != "" AND deviceNumber!=""';
    //     }

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     reg,
    //     walletAddr,
    //     nickName,
    //     pushTransaction,
    //     pushPayment,
    //     pushNotice,
    //     walletStatus
    //     FROM sendipay.Member
    //     WHERE $whereTxt
    //     ");
    //     $sendUserSel=$Sel->fetchAll(PDO::FETCH_ASSOC);
        
    //     return $sendUserSel;
    // }

    // public static function GetDeviceNum($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     email,
    //     nickName
    //     FROM sendipay.Member
    //     WHERE idx = '$idx'
    //     ");
    //     $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }

    // // public static function GetMemberDeviceNumber($data=null)
    // // {
    // //     $idx=$data;
    // //     $db = static::getDB();
    // //     $Sel = $db->query("SELECT
    // //     idx
    // //     FROM sendipay.Member
    // //     WHERE idx = '$idx'
    // //     ");
    // //     $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
    // //     return $returnData;
    // // }

    // public static function GetMemberStaffUpdate($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     walletStatus,
    //     pushTransaction,
    //     pushPayment,
    //     pushNotice
    //     FROM sendipay.Member
    //     WHERE idx = '$idx'
    //     ");
    //     $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }

    /*--------------------------------진짜사용202306-------------------------------*/

    //StMemberCon 총 카운트
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx
        FROM sendipay.Member
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StMemberCon 스태프 멤버 데이터테이블
    public static function GetMemberTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        A.reg,
        CASE A.walletStatus
            WHEN 2 THEN '잠금'
            WHEN 3 THEN '정상'
            ELSE '' END
        AS walletStatus,
        A.walletAddr,
        A.email,
        A.nickName,
        A.country,
        CASE B.deviceType
            WHEN 'I' THEN '아이폰'
            WHEN 'A' THEN '안드로이드'
            ELSE '' END
        AS deviceType
        FROM sendipay.Member AS A
        JOIN sendipay.MemberDevice AS B ON A.idx=B.memberIDX
        WHERE (A.reg BETWEEN '$startDate' AND '$endDate')
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StMemberCon 스태프 멤버 디테일 정보
    public static function GetMemberDetailIsStaff($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.walletAddr,
        A.nickName,
        A.email,
        A.country,
        A.reg,
        A.termReg,
        A.termEndReg,
        CASE A.pushPayment WHEN 2 THEN '거부' WHEN 3 THEN '승인' ELSE '' END AS pushPayment,
        CASE A.pushTransaction WHEN 2 THEN '거부' WHEN 3 THEN '승인' ELSE '' END AS pushTransaction,
        CASE A.pushNotice WHEN 2 THEN '거부' WHEN 3 THEN '승인' ELSE '' END AS pushNotice,
        CASE A.walletStatus WHEN 2 THEN '잠금' WHEN 3 THEN '정상' ELSE '' END AS walletStatus,
        CASE A.consentStatus WHEN 2 THEN '거부' WHEN 3 THEN '승인' ELSE '' END AS consentStatus,
        CASE B.deviceType WHEN 'I' THEN '아이폰' WHEN 'A' THEN '안드로이드' ELSE '' END AS deviceType,
        CASE B.authStatus WHEN 2 THEN '거부' WHEN 3 THEN '승인' ELSE '' END AS authStatus,
        B.reg AS firstLoginTime
        FROM sendipay.Member AS A
        JOIN sendipay.MemberDevice AS B ON A.idx=B.memberIDX
        WHERE A.idx='$targetIDX'
        ");
        $sendUserSel=$Sel->fetch(PDO::FETCH_ASSOC);
        return $sendUserSel;
    }

    //StMemberCon 회원관리 디테일 로그인 히스토리
    public static function GetMemberLoginHistoryData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.reg AS loginTime
        FROM sendipay.Member AS A
        JOIN sendipay.MemberLoginHistory AS B ON A.idx=B.memberIDX
        WHERE A.idx='$idx' ORDER BY B.reg DESC LIMIT 10
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StMemberCon 회원관리 디테일 송금 히스토리
    public static function GetMemberTransferHistoryData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.coin,
        B.createTime
        FROM sendipay.Member AS A
        JOIN sendipay.Transfer AS B ON A.idx=B.memberIDX
        WHERE A.idx='$idx' ORDER BY B.createTime DESC LIMIT 10
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StMemberCon 회원관리 디테일 결제 히스토리
    public static function GetMemberTransactionHistoryData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.contractOrderID,
        C.completeTime
        FROM sendipay.Member AS A
        LEFT JOIN sendipay.Transaction AS B ON A.idx=B.memberIDX
        JOIN sendipay.TransactionTime AS C ON B.idx=C.transactionIDX
        WHERE A.idx='$idx' ORDER BY C.completeTime DESC LIMIT 10
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StMemberCon 회원관리 디테일 출금 히스토리
    public static function GetMemberWithdrawalHistoryData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        B.contractOrderID,
        C.completeTime
        FROM sendipay.Member AS A
        LEFT JOIN sendipay.Withdrawal AS B ON A.idx=B.memberIDX
        JOIN sendipay.WithdrawalTime AS C ON B.idx=C.withdrawalIDX
        WHERE A.idx='$idx' AND B.status=4 ORDER BY C.completeTime DESC LIMIT 10
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

}
