<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractCategoryStdMo extends \Core\Model
{
    public static function getSectorsList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        langCode,
        transactionMinFee,
        transactionMaxFee,
        settlementTetherMinFee,
        settlementTetherMaxFee,
        settlementEtherMinFee,
        settlementEtherMaxFee,
        withdrawalMinFee,
        withdrawalMaxFee
        FROM sendipay.ContractCategoryStd WHERE status=3
        ");
        $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Grade;
    }

    // public static function getSectorsInStaff($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     C.value        
    //     FROM sendipay.ContractCategoryStd AS A
    //     LEFT JOIN sendipay.MdLang AS B
    //     ON (A.langCode = B.code)
    //     LEFT JOIN sendipay.LangPack AS C
    //     ON (B.idx = C.langIDX AND C.cateIDX=1)
    //     WHERE A.status=3
    //     ");
    //     $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $Grade;
    // }

    // 20221202 staff contractStepOne에서 언어팩으로 지정된 업종 가져와야해서 contractMo에 다시 작업함 / 혹시나 다른곳에서 쓰일 수 있으니 주석처리

    public static function getAllCategoryList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.langCode,
        A.transactionMinFee,
        A.transactionMaxFee,
        A.settlementTetherMinFee,
        A.settlementTetherMaxFee,
        A.settlementEtherMinFee,
        A.settlementEtherMaxFee,
        A.withdrawalMinFee,
        A.withdrawalMaxFee,
        A.status,
        C.value AS langKorean
        FROM sendipay.ContractCategoryStd AS A
        LEFT JOIN sendipay.MdLang AS B
        ON (A.langCode = B.code)
        LEFT JOIN sendipay.LangPack AS C
        ON (B.idx = C.langIDX AND C.cateIDX=2)
        ");
        $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Grade;
    }

    public static function ContractCategoryList($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.langCode,
        A.transactionMinFee,
        A.transactionMaxFee,
        A.settlementTetherMinFee,
        A.settlementTetherMaxFee,
        A.settlementUSDCoinMinFee,
        A.settlementUSDCoinMaxFee,
        -- A.settlementEtherMinFee,
        -- A.settlementEtherMaxFee,
        A.withdrawalMinFee,
        A.withdrawalMaxFee,
        A.status,
        C.cateIDX,
        C.value
        FROM sendipay.ContractCategoryStd AS A
        LEFT JOIN sendipay.MdLang AS B
        ON (A.langCode = B.code)
        LEFT JOIN sendipay.LangPack AS C
        ON (B.idx = C.langIDX) WHERE A.status=3 GROUP BY A.idx
        ");
        $Grade=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $Grade;
    }

    public static function getCategoryDetail($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.langCode,
        FORMAT(A.transactionMinFee,1) AS transactionMinFee,
        FORMAT(A.transactionMaxFee,1) AS transactionMaxFee,
        FORMAT(A.settlementTetherMinFee,1) AS settlementTetherMinFee,
        FORMAT(A.settlementTetherMaxFee,1) AS settlementTetherMaxFee,
        FORMAT(A.settlementUSDCoinMinFee,1) AS settlementUSDCoinMinFee,
        FORMAT(A.settlementUSDCoinMaxFee,1) AS settlementUSDCoinMaxFee,
        FORMAT(A.settlementEtherMinFee,1) AS settlementEtherMinFee,
        FORMAT(A.settlementEtherMaxFee,1) AS settlementEtherMaxFee,
        FORMAT(A.withdrawalMinFee,1) AS withdrawalMinFee,
        FORMAT(A.withdrawalMaxFee,1) AS withdrawalMaxFee,
        A.status,
        C.value AS langKorean
        FROM sendipay.ContractCategoryStd AS A
        LEFT JOIN sendipay.MdLang AS B
        ON (A.langCode = B.code)
        LEFT JOIN sendipay.LangPack AS C
        ON (B.idx = C.langIDX AND C.cateIDX=2)
        WHERE A.idx='$idx'
        ");
        $Grade=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Grade;
    }

    public static function categoryNameDupliCofirm($data=null)
    {
        $idx=$data['idx'];
        $langCode=$data['langCode'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx
        FROM sendipay.ContractCategoryStd
        WHERE idx!='$idx' AND langCode='$langCode'
        ");
        $Grade=$Sel->fetch(PDO::FETCH_ASSOC);
        return $Grade;
    }
}
