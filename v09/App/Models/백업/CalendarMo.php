<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class CalendarMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
//Contract table , ContractWallet, ContractCategory , ContractWhiteIp 같이 사용

    public static function GetCalendar($data=null)
    {
        $startDate=$data["startDate"];
        $endDate=$data["endDate"];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.type,
        A.memo,
        A.createTIme,
        A.createIDX,
        A.date,
        A.staffIDX,
        B.name
        FROM sendipay.Calendar AS A
        LEFT JOIN sendipay.Staff AS B
        ON(A.staffIDX = B.idx)
        WHERE date BETWEEN date('$startDate') AND date('$endDate')
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetDetail($data=null)
    {
        $idx = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.date,
        A.type,
        A.memo,
        A.createTIme,
        A.createIDX,
        A.staffIDX,
        B.name
        FROM sendipay.Calendar AS A
        LEFT JOIN sendipay.Staff AS B
        ON(A.staffIDX = B.idx)
        WHERE A.idx='$idx'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // public static function GetContractData($data=null)
    // {
    //     $code=$data;
    //     $db = static::getDB();
    //     $selData = $db->query("SELECT
    //     A.idx,
    //     A.directorIDX,
    //     A.contractCategoryIDX,
    //     A.contractCategoryEx,
    //     A.code AS contractCode,
    //     A.name AS contractName,
    //     A.homepage,
    //     A.status,
    //     A.license,
    //     B.name AS managerName,
    //     C.email,
    //     D.status AS regiStatus,
    //     D.reqCompany,
    //     D.reqHomepage,
    //     D.reqContractCategoryIDX,
    //     D.reqBusinessLicense,
    //     D.reqLicense,
    //     D.reqIp,
    //     D.reqPortalEmail,
    //     D.requestValue,
    //     E.whiteIp,
    //     F.rejectReason
    //     FROM sendipay.Contract AS A
    //     LEFT JOIN sendipay.ContractManager AS B
    //     ON A.idx=B.contractIDX
    //     LEFT JOIN sendipay.Manager AS C
    //     ON B.managerIDX=C.idx
    //     LEFT JOIN sendipay.ContractRegiAddReq AS D
    //     ON A.idx=D.contractIDX
    //     LEFT JOIN sendipay.ContractWhiteIp AS E
    //     ON A.idx=E.contractIDX
    //     LEFT JOIN sendipay.ContractRegiReject AS F
    //     ON A.code=F.code
    //     WHERE A.code = '$code' ORDER BY D.idx DESC
    //     ");
    //     $result=$selData->fetch(PDO::FETCH_ASSOC);
    //     return $result;
    // }

}