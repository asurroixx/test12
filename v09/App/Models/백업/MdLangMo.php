<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MdLangMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function GetLangIDX($data=null)
	{
		$code = $data;
		$db = static::getDB();
		$idxSel = $db->query("SELECT
			idx
			FROM sendipay.MdLang
			WHERE code='$code'
		");
		$idx=$idxSel->fetch(PDO::FETCH_ASSOC);
		return $idx;
	}

}