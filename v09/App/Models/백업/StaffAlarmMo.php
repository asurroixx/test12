<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffAlarmMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
    public static function GetMyAlarmNotRead($data=null)
    {   
        $loginIDX=$data['loginIDX'];
        $nowPage=$data['nowPage'];
        $db = static::getDB();
        $Sel = $db->query("SELECT 
        A.idx,
        A.type,
        count(case when A.type=2 then 1 end) as qnaCount,
        count(case when A.type=3 then 1 end) as aaa,
        count(case when A.type=4 then 1 end) as bbb,
        B.url AS secondMenuName,
        C.url AS menuName
        FROM sendipay.StaffAlarm AS A
        JOIN sendipay.StaffMenuStd AS B ON A.menuIDX=B.idx
        JOIN sendipay.StaffMenuStd AS C ON B.momIDX=C.idx
        WHERE A.status=2 AND A.staffIDX='$loginIDX' AND B.url='$nowPage'
        GROUP BY A.type
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }


    public static function getMyAlarmAllCnt($data=null)
    {
        $loginIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        count(A.idx) AS allCnt
        FROM sendipay.StaffAlarm AS A
        WHERE A.staffIDX='$loginIDX'
        ");
        $menuFetch=$Sel->fetch(PDO::FETCH_ASSOC);
        return $menuFetch;
    }


    public static function getMyAlarmList($data=null)
    {
        $listPage=$data['listPage'];
        $loginIDX=$data['loginIDX'];

        $viewStart = $listPage * 10;
        $viewEnd = 10;

        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.menuIDX,
        A.status,
        A.createTime,
        A.con,
         CASE
            WHEN A.type = 2 THEN 'QNA'
            WHEN A.type = 3 THEN '서비스계약관련'
        END AS typeName
        FROM sendipay.StaffAlarm AS A
        WHERE A.staffIDX='$loginIDX'
        ORDER BY A.createTime DESC
        LIMIT $viewStart,$viewEnd
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }

    public static function GetResetCount($data=null)
    {
        $staffIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
            A.menuIDX,
            COUNT(A.idx) AS count,
            B.momIDX
            FROM sendipay.StaffAlarm AS A
            LEFT JOIN sendipay.StaffMenuStd AS B ON A.menuIDX=B.idx
            WHERE A.staffIDX = '$staffIDX' AND A.status = 2
            GROUP BY A.menuIDX
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }

    public static function GetTotalCount($data=null)
    {
        $staffIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
            COUNT(A.idx) AS count,
            B.momIDX
            FROM sendipay.StaffAlarm AS A
            LEFT JOIN sendipay.StaffMenuStd AS B ON A.menuIDX=B.idx
            WHERE A.staffIDX = '$staffIDX' AND A.status = 2
            GROUP BY B.momIDX
        ");
        $menuFetch=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $menuFetch;
    }

}