<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class TransferMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    
    //StTransferCon 전체 유저 카운트
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx,
        status,
        coin
        FROM sendipay.Transfer
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StTransferCon 데이터테이블
    public static function GetTransferTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];

        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        CASE A.status
            WHEN 2 THEN 'Pending'
            WHEN 3 THEN 'Success'
            WHEN 4 THEN 'Fail'
            ELSE '' END
        AS status,
        A.toAddr,
        A.coin,
        A.amount,
        format(A.fee, 1) AS fee,
        A.timestampReg,
        A.createTime,
        B.walletAddr,
        B.nickName
        FROM sendipay.Transfer AS A
        JOIN sendipay.Member AS B ON A.memberIDX=B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StTransferCon 디테일
    public static function GetDetailData($data=null)
    {
        $targetIDX=$data;

        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        CASE A.status
            WHEN 2 THEN 'Pending'
            WHEN 3 THEN 'Success'
            WHEN 4 THEN 'Fail'
            ELSE '' END
        AS status,
        A.toAddr,
        A.coin,
        A.hash,
        A.amount,
        format(A.fee, 2) AS fee,
        A.timestampReg,
        A.createTime,
        B.walletAddr,
        B.nickName
        FROM sendipay.Transfer AS A
        JOIN sendipay.Member AS B ON A.memberIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
    }

}
