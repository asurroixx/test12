<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class FilesMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
	//StContractCon 파일정보
	public static function GetDocumentFiles($data=null)
	{
        $targetIDX=$data;
		$db = static::getDB();
		$filesList = $db->query("SELECT
		idx,
		serverName,
        orignName,
        ext,
        targetIDX,
        type
		FROM sendipay.Files 
        WHERE targetIDX='$targetIDX' AND type=4
        ");
		$result=$filesList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function GetContractProof($data=null)
	{
        $contractCode=$data;
		$db = static::getDB();
		$filesList = $db->query("SELECT
		A.idx,
		A.serverName,
        A.orignName,
        A.ext
		FROM sendipay.Files AS A 
		LEFT JOIN sendipay.Contract AS B
		ON(A.targetIDX = B.idx)
        WHERE B.code='$contractCode' AND A.type=4
        ");
		$result=$filesList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	/*--------------진짜사용 202306 -----------------------*/

	//QnaDetailCon 해당 targetIDX의 파일가져오기
	public static function GetQuestionFileData($data=null)
	{
        $targetIDX=$data['targetIDX'];
        $type=$data['type'];
		$db = static::getDB();
		$filesList = $db->query("SELECT
		idx,
		serverName,
        orignName,
        ext,
        targetIDX,
        type
		FROM sendipay.Files
        WHERE targetIDX='$targetIDX' AND type='$type'
        ");
		$result=$filesList->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}