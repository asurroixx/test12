<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class MemberLoginHistoryMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    
    public static function GetMemberLoginHistoryListLoad($data=null)
    {
        $partnerCode=$data;
        $partnerQuery='';
        if($partnerCode=='all'){
            $partnerQuery='';
        }else{
            $partnerQuery=' AND ';
        }
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        A.memberIDX,
        A.deviceNumber,
        A.deviceType,
        A.reg,
        B.nickName,
        B.walletAddr
        FROM sendipay.MemberLoginHistory AS A
        LEFT JOIN sendipay.Member AS B
        ON A.memberIDX=B.idx
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    public static function GetTransferDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.memberIDX,
        A.status,
        A.toAddr,
        A.coin,
        A.hash,
        A.amount,
        A.fee,
        A.timestampReg,
        A.createTime,
        B.walletAddr,
        B.nickName
        FROM sendipay.Transfer AS A
        LEFT JOIN sendipay.Member AS B
        ON A.memberIDX=B.idx
        WHERE A.idx='$targetIDX'
        ");
        $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $transactionForm;
    }

}
