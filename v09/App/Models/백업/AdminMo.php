<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class AdminMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    //LoginCon 이메일 유효성 검사
    public static function issetEmail($data=null)
    {
        $DBTable=self::DBTable;
        $BasicTable=self::BasicTable['Admin'];
        $email=$data;
        $email=trim($email);
        $db = static::GetDB();
        $Sel = $db->query("SELECT
        idx,
        email
        FROM $DBTable.$BasicTable
        WHERE email='$email'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //LoginCon 로그인 코드 검사
    public static function GetLoginCodeChk($data=null)
    {
        $DBTable=self::DBTable;
        $BasicTableAdmin=self::BasicTable['Admin'];
        $BasicTableLoginDump=self::BasicTable['LoginDump'];
        $code=$data['code'];
        $email=$data['email'];
        $ipAddress=$data['ipAddress'];
        $db = static::GetDB();
        $GetDump = $db->prepare("SELECT
        A.code,
        B.idx,
        B.email
        FROM $DBTable.$BasicTableLoginDump AS A
        JOIN $DBTable.$BasicTableAdmin AS B ON A.adminIDX = B.idx
        WHERE B.email='$email' AND A.code='$code' AND A.ipAddress='$ipAddress'
        ORDER BY A.idx DESC LIMIT 1
        ");
        $GetDump->execute();
        $row = $GetDump->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    //LoginCon 글로벌변수 생성시
    public static function GetGlobalVal($data=null)
    {
        $DBTable=self::DBTable;
        $BasicTable=self::BasicTable['Admin'];
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email,
        name
        FROM $DBTable.$BasicTable
        WHERE idx='$idx'
        ");
        $globalVal=$Sel->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
    }


}
