<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractWhiteIpMo extends \Core\Model
{

    //StContractDetailCon 해당 계약 ip주소 검사
    public static function getIpCountSel($data=null)
    {
        $contractIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        whiteIp
        FROM sendipay.ContractWhiteIp
        WHERE contractIDX = '$contractIDX'
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StContractWhiteIpCon 전체카운드
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx
        FROM sendipay.ContractWhiteIp
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    // StContractWhiteIpCon 데이터테이블
    public static function GetWhiteIpDataTable($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        CASE A.status
            WHEN 2 THEN '신청'
            WHEN 3 THEN '정상'
            WHEN 4 THEN '삭제신청'
            WHEN 6 THEN '반려'
            WHEN 7 THEN '중지'
            ELSE '' END
        AS status,
        A.whiteIp,
        A.createTime,
        B.serviceName,
        B.code,
        B.name AS contractName,
        C.email AS directorEmail
        FROM sendipay.ContractWhiteIp AS A
        JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        JOIN sendipay.Director AS C ON B.directorIDX=C.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    // StContractWhiteIpCon 디테일 , ip 신청 왔을때 정보확인
    public static function GetWhiteIpDetailData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        CASE A.status
            WHEN 2 THEN '신청'
            WHEN 3 THEN '정상'
            WHEN 4 THEN '삭제신청'
            WHEN 6 THEN '반려'
            WHEN 7 THEN '중지'
            ELSE '' END
        AS status,
        A.whiteIp,
        A.createTime,
        B.directorIDX,
        B.serviceName,
        B.name AS contractName,
        C.email AS directorEmail
        FROM sendipay.ContractWhiteIp AS A
        JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        JOIN sendipay.Director AS C ON B.directorIDX=C.idx
        WHERE A.idx='$idx'
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }


    //?
    // public static function chkRegistIPSel($data=null)
    // {
    //     $contractIDX = $data['contractIDX'];
    //     $ipInputBoxVal = $data['ipInputBoxVal'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.ContractWhiteIp
    //     WHERE contractIDX = '$contractIDX' AND whiteIp = '$ipInputBoxVal'
    //     ");
    //     $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }
}
