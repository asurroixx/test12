<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class DashboardMo extends \Core\Model
{

	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */
	// public static function get_mdInterviewPurpose($data=null)
	// {
	// 	$db = static::getDB();
	// 	$MdInterviewPurposeSel = $db->query("SELECT
	// 	idx,
	// 	name
	// 	FROM joon.MdInterviewPurpose WHERE status=3 ORDER BY seq");
	// 	$CustomerBirthFetch=$MdInterviewPurposeSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $CustomerBirthFetch;


	// }

	// public static function get_InterviewDatatable($data=null)
	// {

	// 	$startDate=$data['startDate'];
	// 	$endDate=$data['endDate'];
	// 	$gbLoginHospitalIDX=$data['gbLoginHospitalIDX'];

	// 	$db = static::getDB();
	// 	//$stmt = $db->query('SELECT idx,ageIDX,category,order,status FROM joon.ChartCategory');
	// 	$customerSel = $db->query("SELECT
	// 	A.idx AS interviewIDX,
	// 	A.createTime,
	// 	A.hospitalIDX,
	// 	B.idx AS customerIDX,
	// 	B.name,
	// 	B.birth,
	// 	B.gender,
	// 	A.purpose,
	// 	B.phone
	// 	FROM joon.Interview AS A
	// 	LEFT JOIN joon.CustomerInfo AS B
	// 	ON (A.customerIDX = B.idx)
	// 	WHERE ( A.createTime  BETWEEN '$startDate' AND '$endDate') AND A.status!=2 AND A.hospitalIDX = '$gbLoginHospitalIDX'
	// 	");
	// 	$customerFetch=$customerSel->fetchAll(PDO::FETCH_ASSOC);
	// 	// $customerSel->execute();
	// 	return $customerFetch;
	// 	// return $stmt->fetchAll(PDO::FETCH_ASSOC);


	// }


	// public static function get_InterviewQuestionBySeq($data=null)
	// {
	// 	// interviewProcessCon::answerDelAndPreQLoad() 에서도 씀

	// 	$depth=1;
	// 	$seq=$data;

	// 	$db = static::getDB();
	// 	$InterviewQuestionSel = $db->query("SELECT
	// 	A.idx,
	// 	A.question,
	// 	A.siblingQIDX,
	// 	A.choiceType,
	// 	A.titleStatus,
	// 	A.seq,
	// 	B.category,
	// 	A.publishType,
	// 	A.activeCondition,
	// 	A.layoutSeq,
	// 	A.tempNum
	// 	FROM joon.InterviewQuestion AS A
	// 	LEFT JOIN joon.MdInterviewCategory AS B
	// 	ON (A.categoryIDX = B.idx)
	// 	WHERE A.depth='$depth' AND A.seq='$seq' AND A.type=4");
	// 	$InterviewQuestionFetch=$InterviewQuestionSel->fetch(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;
	// }

	// public static function get_InterviewQuestionByIDX($data=null)
	// {
	// 	// interviewCon::getQuestionArr()
	// 	$idx=$data;
	// 	$db = static::getDB();
	// 	$InterviewQuestionSel = $db->query("SELECT
	// 	A.idx,
	// 	A.question,
	// 	A.siblingQIDX,
	// 	A.choiceType,
	// 	A.titleStatus,
	// 	A.seq,
	// 	B.category,
	// 	A.publishType,
	// 	A.layoutSeq,
	// 	A.nextQW,
	// 	A.nextQM,
	// 	A.nextQC,
	// 	A.nextQP,
	// 	A.activeCondition,
	// 	A.titlePublish
	// 	FROM joon.InterviewQuestion AS A
	// 	LEFT JOIN joon.MdInterviewCategory AS B
	// 	ON (A.categoryIDX = B.idx)
	// 	WHERE A.idx='$idx' AND A.type=4");
	// 	$InterviewQuestionFetch=$InterviewQuestionSel->fetch(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;


	// }

	// public static function get_InterviewAllQuestion($data=null)
	// {
	// 	$depth=1;
	// 	$seq=$data;

	// 	$db = static::getDB();
	// 	$InterviewQuestionSel = $db->query("SELECT
	// 	A.idx,
	// 	A.question,
	// 	A.siblingQIDX,
	// 	A.choiceType,
	// 	A.titleStatus,
	// 	A.seq,
	// 	B.category,
	// 	A.publishType,
	// 	A.activeCondition
	// 	FROM joon.InterviewQuestion AS A
	// 	LEFT JOIN joon.MdInterviewCategory AS B
	// 	ON (A.categoryIDX = B.idx)
	// 	WHERE A.depth='$depth' AND A.type=4 ORDER BY A.seq");
	// 	$InterviewQuestionFetch=$InterviewQuestionSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;
	// }

	//  public static function get_InterviewChoice($data=null)
	// {
	// 	$qIDX=$data;
	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx,
	// 	choice,
	// 	subQIDX,
	// 	publishDefalt,
	// 	choicePublish,
	// 	nextCW,
	// 	nextCM,
	// 	nextCC,
	// 	nextCP
	// 	FROM joon.InterviewChoice WHERE qIDX='$qIDX' ORDER BY seq");
	// 	$InterviewChoiceFetch=$InterviewChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;


	// }
	// public static function get_InterviewAnswer($data=null)
	// {
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx,
	// 	cIDX
	// 	FROM joon.InterviewAnswer WHERE interviewIDX='$interviewIDX' AND qIDX='$qIDX'");
	// 	$count = $InterviewChoiceSel->rowCount();
	// 	$returnData=$InterviewChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	$returnData = array(
	// 		'rowCount' => $count,
	// 		'data'=>$returnData
	// 	);
	// 	return $returnData;
	// }

	// public static function issetInterviewAnswerStat($data=null)
	// {
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx,
	// 	cIDX
	// 	FROM joon.InterviewAnswer WHERE interviewIDX='$interviewIDX' AND qIDX='$qIDX'");
	// 	$count = $InterviewChoiceSel->rowCount();
	// 	$returnData=$InterviewChoiceSel->fetch(PDO::FETCH_ASSOC);
	// 	$returnData = array(
	// 		'rowCount' => $count,
	// 		'data'=>$returnData
	// 	);
	// 	return $returnData;
	// }

	// public static function issetInterviewAnswerStatByChoiceIDX($data=null)
	// {
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	$cIDX=$data['cIDX']*1;
	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx
	// 	FROM joon.InterviewAnswer WHERE interviewIDX='$interviewIDX'  AND cIDX='$cIDX' AND status=3");
	// 	$count = $InterviewChoiceSel->rowCount();
	// 	$returnData=$InterviewChoiceSel->fetch(PDO::FETCH_ASSOC);
	// 	$returnData = array(
	// 		'rowCount' => $count,
	// 		'data'=>$returnData
	// 	);
	// 	return $returnData;
	// }


	// public static function searchInterviewIDX($data=null)
	// {
	// 	$interviewIDX=$data*1;
	// 	$db = static::getDB();
	// 	$treatSel = $db->query("SELECT
	// 	idx,
	// 	customerIDX,
	// 	createTime
	// 	FROM joon.Interview WHERE idx='$interviewIDX'");
	// 	$returnData=$treatSel->fetch(PDO::FETCH_ASSOC);
	// 	return $returnData;
	// }



	// public static function confrimInterview($data=null)
	// {	
	// 	//해당 인터뷰IDX와 customerIDX를 가진 상태가 미완성인 문진이 맞는가?
	// 	// InterviewProcessCon::routingDo()에서 사용
	// 	$interviewIDX=$data['interviewIDX'];
	// 	$customerIDX=$data['customerIDX'];
	// 	$db = static::getDB();
	// 	$interviewSel = $db->query("SELECT
	// 	idx
	// 	FROM joon.Interview WHERE idx='$interviewIDX' AND customerIDX='$customerIDX' AND status=2");
	// 	$count = $interviewSel->rowCount();
	// 	$returnData = array(
	// 		'rowCount' => $count
	// 	);
	// 	return $returnData;
	// }


	//   public static function get_InterviewQuestionTxt($data=null)
	// {

	// 	$db = static::getDB();
	// 	$InterviewQuestionSel = $db->query("SELECT
	// 	idx,
	// 	categoryIDX,
	// 	titleStatus,
	// 	question,
	// 	depth,
	// 	choiceType,
	// 	siblingQIDX,
	// 	`type`,
	// 	seq,
	// 	nextQW,
	// 	nextQM,
	// 	nextQC,
	// 	nextQP
	// 	FROM joon.InterviewQuestion");
	// 	$InterviewQuestionFetch=$InterviewQuestionSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;


	// }


	//   public static function get_totalQustionNumber($data=null)
	// {

	// 	$db = static::getDB();
	// 	$InterviewQuestionSel = $db->query("SELECT
	// 	seq
	// 	FROM joon.InterviewQuestion WHERE `type`=4 ORDER BY seq DESC");
	// 	$InterviewQuestionFetch=$InterviewQuestionSel->fetch(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;


	// }

	// public static function get_InterviewChoiceTxt($data=null)
	// {

	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx,
	// 	qIDX,
	// 	choice,
	// 	seq,
	// 	subQIDX,
	// 	skipQIDX
	// 	FROM joon.InterviewChoice ");
	// 	$InterviewChoiceFetch=$InterviewChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;


	// }

	// public static function get_InterviewHis($data=null)
	// {
	// 	//회원IDX를 주면 문진내역을 뽑아줍니다.
	// 	// DetailsViewCon::interviewHisLoad
	// 	$customerIDX=$data;
	// 	$db = static::getDB();
	// 	$InterviewChoiceSel = $db->query("SELECT
	// 	idx,
	// 	date_format(createTime,'%Y-%m-%d') AS createTime
	// 	FROM joon.Interview 
	// 	WHERE status!=2 AND customerIDX='$customerIDX'");
	// 	$InterviewChoiceFetch=$InterviewChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;



	// }






	//  public static function get_doctorQuestionTxt($data=null)
	// {

	// 	$db = static::getDB();
	// 	$DoctorQuestionSel = $db->query("SELECT
	// 	idx,
	// 	categoryIDX,
	// 	titleStatus,
	// 	question,
	// 	depth,
	// 	choiceType,
	// 	publishType,
	// 	siblingQIDX,
	// 	`type`,
	// 	seq,
	// 	nextQW,
	// 	nextQM,
	// 	nextQC,
	// 	nextQP
	// 	FROM joon.InterviewQuestion WHERE `type`=5 ");
	// 	$InterviewQuestionFetch=$DoctorQuestionSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewQuestionFetch;


	// }

	// public static function QCustomerTargetConfirm($data=null)
	// {
	// 	// treatCon::routingDetailDo()

	// 	$qIDX=$data;
	// 	$db = static::getDB();
	// 	$interviewSel = $db->query("SELECT
	// 	nextQW,
	// 	nextQM,
	// 	nextQC,
	// 	nextQP
	// 	FROM joon.InterviewQuestion WHERE idx='$qIDX'");
	// 	$InterviewChoiceFetch=$interviewSel->fetch(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;
	// }
	

	// public static function get_doctorChoiceTxt($data=null)
	// {

	// 	$qIDX=$data['qIDX']*1;
	// 	$db = static::getDB();
	// 	$DoctorChoiceSel = $db->query("SELECT
	// 	idx,
	// 	qIDX,
	// 	choice,
	// 	seq,
	// 	subQIDX,
	// 	skipQIDX
	// 	FROM joon.InterviewChoice WHERE qIDX='$qIDX' ORDER BY seq");
	// 	$InterviewChoiceFetch=$DoctorChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;


	// }

	// public static function get_interviewData($data=null)
	// {
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	$db = static::getDB();
	// 	$interviewSel = $db->query("SELECT
	// 	idx,
	// 	interviewNum,
	// 	customerIDX,
	// 	hospitalIDX,
	// 	createTime,
	// 	updateTime,
	// 	purpose,
	// 	status
	// 	FROM joon.Interview WHERE idx='$interviewIDX'");
	// 	$InterviewChoiceFetch=$interviewSel->fetch(PDO::FETCH_ASSOC);
	// 	return $InterviewChoiceFetch;
	// }



	// // public static function get_interviewAnswerData($data=null)
	// // {
	// //     $cIDX=$data['cIDX']*1;
	// //     $qIDX=$data['qIDX']*1;
	// //     $interviewIDX=$data['interviewIDX']*1;
	// //     $db = static::getDB();
	// //     $interviewAnswerSel = $db->query("SELECT
	// //     idx,
	// //     interviewIDX,
	// //     qIDX,
	// //     cIDX,
	// //     createTime,
	// //     updateTime
	// //     FROM joon.InterviewAnswer WHERE interviewIDX='$interviewIDX' AND cIDX='$cIDX'");
	// //     $InterviewAnswerFetch=$interviewAnswerSel->fetchAll(PDO::FETCH_ASSOC);
	// //     return $InterviewAnswerFetch;
	// // }

	//  public static function get_interviewAnswerChoice($data=null)
	// {
	// 	// $interviewIDX=$data['interviewIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$cIDX=$data['cIDX']*1;
	// 	$db = static::getDB();
	// 	$interviewAnswerChoiceSel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX' OR cIDX='$cIDX'");
	// 	$InterviewAnswerChoiceFetch=$interviewAnswerChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	$count = $interviewAnswerChoiceSel->rowCount();
	// 	$returnData = array(
	// 		'rowCount' => $count,
	// 		'answerData'=>$InterviewAnswerChoiceFetch
	// 	);
	// 	return $returnData;
	// }


	// public static function get_interviewAnswerQuesChoice($data=null)
	// {
	// 	$qIDX=$data['qIDX']*1;
	// 	$db = static::getDB();
	// 	$interviewAnswerQuesChoiceSel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX'");
	// 	$InterviewAnswerQuesChoiceFetch=$interviewAnswerQuesChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	$count = $interviewAnswerQuesChoiceSel->rowCount();
	// 	 if($count>0){
	// 		return true;
	// 	}
	// }

	// public static function get_interviewTreatQAnsChoice($data=null)
	// {
	// 	$qIDX=$data['qIDX']*1;
	// 	$cIDX=$data['cIDX']*1;
	// 	$db = static::getDB();
	// 	$interviewTreatQAnsChoiceSel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX' OR cIDX='$cIDX'");
	// 	$InterviewTreatQAnsChoiceFetch=$interviewTreatQAnsChoiceSel->fetchAll(PDO::FETCH_ASSOC);
	// 	$count = $interviewTreatQAnsChoiceSel->rowCount();
	// 	$returnData = array(
	// 		'rowCount' => $count,
	// 		'answerData'=>$InterviewTreatQAnsChoiceFetch
	// 	);
	// 	return $returnData;
	// }


	// public static function get_interviewAnswerChoiceUpdate($data=null)
	// {
	// 	$cIDX=$data['cIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	$db = static::getDB();
	// 	$interviewAnswerUpdateSel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX'");
	// 	$InterviewAnswerFetch=$interviewAnswerUpdateSel->fetchAll(PDO::FETCH_ASSOC);
	// 	$count = $interviewAnswerUpdateSel->rowCount();
	// 	if($count>0){
	// 		return true;
	// 	}
	// }

	// public static function get_interviewQuestionAnsUpdate($data=null)
	// {
	// 	$cIDX=$data['cIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	if($cIDX==$qIDX){
	// 	   $db = static::getDB();
	// 		$interviewQuestionAnsUpdSel = $db->query("SELECT
	// 		idx,
	// 		interviewIDX,
	// 		qIDX,
	// 		cIDX,
	// 		createTime,
	// 		updateTime
	// 		FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX' AND cIDX='$cIDX' ");
	// 		$InterviewAnswerFetch=$interviewQuestionAnsUpdSel->fetchAll(PDO::FETCH_ASSOC);
	// 		$count = $interviewQuestionAnsUpdSel->rowCount();
	// 		if($count>0){
	// 			return true;
	// 		}
	// 	}

	// }

	// public static function get_interviewTreatQAnsUpdate($data=null)
	// {
	// 	$cIDX=$data['cIDX']*1;
	// 	$qIDX=$data['qIDX']*1;
	// 	$interviewIDX=$data['interviewIDX']*1;
	// 	if($cIDX==$qIDX){
	// 	   $db = static::getDB();
	// 		$interviewTreatQAnsUpdSel = $db->query("SELECT
	// 		idx,
	// 		interviewIDX,
	// 		qIDX,
	// 		cIDX,
	// 		createTime,
	// 		updateTime
	// 		FROM joon.InterviewAnswer WHERE interviewIDX=21 AND qIDX='$qIDX' AND cIDX='$cIDX' ");
	// 		$InterviewTreatQAnsFetch=$interviewTreatQAnsUpdSel->fetchAll(PDO::FETCH_ASSOC);
	// 		$count = $interviewTreatQAnsUpdSel->rowCount();
	// 		if($count>0){
	// 			return true;
	// 		}
	// 	}

	// }



















	// public static function get_InterviewChoiceUseCIDX($data=null)
	// {
	// 	$postCIDX=$data['cIDX']*1;
	// 	$db = static::getDB();
	// 	$sel = $db->query("SELECT
	// 	idx,
	// 	choice,
	// 	subQIDX,
	// 	publishDefalt,
	// 	seq
	// 	FROM joon.InterviewChoice WHERE cIDX='$postCIDX'");
	// 	$tmpResult=$sel->fetchAll(PDO::FETCH_ASSOC);
	// 	$tmpCount = $sel->rowCount();
	// 	$returnData = array(
	// 		'count' => $tmpCount,
	// 		'data'=>$tmpResult
	// 	);
	// 	return $returnData;
	// }
	// public static function get_InterviewChoiceUseQIDX($data=null)
	// {
	// 	$postQIDX=$data['qIDX']*1;
	// 	$db = static::getDB();
	// 	$sel = $db->query("SELECT
	// 	idx,
	// 	choice,
	// 	subQIDX,
	// 	publishDefalt,
	// 	seq
	// 	FROM joon.InterviewChoice WHERE qIDX='$postQIDX'");
	// 	$tmpResult=$sel->fetchAll(PDO::FETCH_ASSOC);
	// 	$tmpCount = $sel->rowCount();
	// 	$returnData = array(
	// 		'count' => $tmpCount,
	// 		'data'=>$tmpResult
	// 	);
	// 	return $returnData;
	// }
	// public static function get_InterviewAnswerUseQIDX($data=null)
	// {
	// 	$postQIDX=$data['qIDX']*1;
	// 	$postInterviewIDX=$data['interviewIDX']*1;
	// 	$db = static::getDB();
	// 	$sel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX='$postInterviewIDX' AND qIDX='$postQIDX'");
	// 	$tmpResult=$sel->fetchAll(PDO::FETCH_ASSOC);
	// 	$tmpCount = $sel->rowCount();
	// 	$returnData = array(
	// 		'count' => $tmpCount,
	// 		'data'=>$tmpResult
	// 	);
	// 	return $returnData;
	// }



	// public static function get_InterviewAnswerUseCIDX($data=null)
	// {
	// 	$postCIDX=$data['cIDX']*1;
	// 	$postInterviewIDX=$data['interviewIDX']*1;
	// 	$db = static::getDB();
	// 	$sel = $db->query("SELECT
	// 	idx,
	// 	interviewIDX,
	// 	qIDX,
	// 	cIDX,
	// 	createTime,
	// 	updateTime
	// 	FROM joon.InterviewAnswer WHERE interviewIDX='$postInterviewIDX' AND cIDX='$postCIDX'");
	// 	$tmpResult=$sel->fetchAll(PDO::FETCH_ASSOC);
	// 	$tmpCount = $sel->rowCount();
	// 	$returnData = array(
	// 		'count' => $tmpCount,
	// 		'data'=>$tmpResult
	// 	);
	// 	return $returnData;
	// }
}