<?php 
namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class BearerTokenMo extends \Core\Model
{
	public static function ApiKeyCheck($data=null)
	{
		$apiKey = $data;
		$db = static::getDB();
		$apiKeyChek = $db->query("SELECT
			idx
			,apiKey
			FROM sendipay.ApiKey
			WHERE apiKey='$apiKey'
			");

		$result = $apiKeyChek->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public static function TokenCheck($data=null)
	{
		$token = $data;
		$db = static::getDB();
		$tokenChek = $db->query("SELECT
			token
			FROM sendipay.BearerTokenDump
			WHERE token = '$token'
			AND status = 0
			AND createTime >= SUBDATE(NOW(), INTERVAL 1 MINUTE)
			");
		$result = $tokenChek->fetch(PDO::FETCH_ASSOC);
		return $result;
	}




	// 내부 사용
	
	public static function CookieCheck($data=null)
	{
		$cookieData = $data;
		$db = static::getDB();
		$cookieChek = $db->query("SELECT
			encryptValue
			FROM sendipay.CookieTokenDump
			WHERE decryptValue = '$cookieData'
			GROUP BY '$cookieData'
			HAVING COUNT(decryptValue) > 10
			");
		$result = $cookieChek->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
}

 ?>