<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function GetLogin($data=null)
    {
        $code=$data['code'];
        $email=$data['email'];
        $ipAddress=$data['ipAddress'];
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.code,
        B.idx,
        B.email
        FROM sendipay.StaffLoginDump AS A
        LEFT JOIN sendipay.Staff AS B
        ON (A.staffIDX = B.idx)
        WHERE B.email='$email' AND A.code='$code' AND A.ipAddress='$ipAddress'
        ORDER BY A.idx DESC LIMIT 1
        ");
        $GetDump->execute();
        $count = $GetDump->rowCount();
        $row = $GetDump->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public static function GetLoginInfo($data=null)
    {
        $staffIDX=$data;
        $db = static::getDB();
        $getInfo = $db->query("SELECT
        A.email,
        A.name,
        A.birth,
        A.qnaAlarmSet,
        A.contractAlarmSet,
        A.settlementAlarmSet,
        B.name AS gradeName, 
        C.name AS deptName
        FROM sendipay.Staff AS A
        JOIN sendipay.MdGrade AS B
        ON (A.gradeIDX = B.idx)
        JOIN sendipay.DeptStd AS C
        ON (A.deptIDX = C.idx)
        WHERE A.idx='$staffIDX'
        ");
        $staffInfo = $getInfo->fetch(PDO::FETCH_ASSOC);
        return $staffInfo;
    }


    //StLoginCon 글로벌변수 생성시
    public static function GetGlobalVal($data=null)
    {
        // $idx=$data;
        // $db = static::getDB();
        // $GetDump = $db->prepare("SELECT
        // idx,
        // email,
        // name
        // FROM sendipay.Staff
        // WHERE idx='$idx'
        // ");
        // $GetDump->execute();
        // $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        // return $globalVal;
        
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.email,
        A.name,
        A.qnaAlarmSet,
        A.contractAlarmSet,
        A.settlementAlarmSet,
        B.idx AS gradeIDX,
        B.name AS gradeName
        FROM sendipay.Staff AS A
        LEFT JOIN sendipay.MdGrade AS B
        ON (A.gradeIDX = B.idx)
        WHERE A.idx='$idx'
        ");
        $globalVal=$Sel->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //랜더할때 카운트
    // public static function RenderStaffList($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.email,
    //     A.name,
    //     A.createTime,
    //     A.gradeIDX,
    //     B.idx AS MdGradeIDX,
    //     B.name AS gradeName
    //     FROM sendipay.Staff AS A
    //     LEFT JOIN sendipay.MdGrade AS B
    //     ON (A.gradeIDX = B.idx)
    //     WHERE B.subUrlIDX='9'
    //     ");
    //     $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $adminList;
    // }



    //솔팅카운트
    // public static function GetGradeIDX($data=null)
    // {
    //     $idx=$data['idx'];
    //     $startDate=$data['startDate'];
    //     $endDate=$data['endDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.Staff
    //     WHERE gradeIDX='$idx' AND (createTime BETWEEN '$startDate' AND '$endDate')
    //     ");
    //     $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $adminList;
    // }





    // public static function issetStaffIDX($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     gradeIDX
    //     FROM sendipay.Staff
    //     WHERE idx='$idx'
    //     ");
    //     $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $adminList;
    // }

    // public static function issetStaffGrade($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx,
    //     gradeIDX
    //     FROM sendipay.Staff
    //     WHERE idx='$idx' AND gradeIDX=7
    //     ");
    //     $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $adminList;
    // }


    /*-----------------202306 진짜사용---------------------*/
    /*
    QnaDetailCon 스태프 수,담당스태프 로드
    QnaDetailCon nodeStaff.js 스태프idx 받아올때
    */
    public static function GetStaffCount($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.email,
        A.name,
        B.name AS gradeName
        FROM sendipay.Staff AS A
        LEFT JOIN sendipay.MdGrade AS B ON A.gradeIDX = B.idx
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //QnaDetailCon 해당 deptIDX 스태프 불러오기
    public static function GetDeptIDXData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email,
        name
        FROM sendipay.Staff
        WHERE deptIDX='$idx'
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //QnaDetailCon 자신을 제외한 스태프idx , StPostitCon 랜더시 스태프 정보
    public static function issetGlobalIDXData($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email
        FROM sendipay.Staff
        WHERE idx <> '$idx'
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StStaffCon 총 카운트
    public static function GetTotalCount($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM sendipay.Staff
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        $totalCount=$globalVal['count'];
        return $totalCount;
    }

    //StStaffCon 데이터테이블
    public static function getStaffList($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.email,
        A.name,
        A.createTime,
        B.name AS gradeName,
        C.name AS deptName
        FROM sendipay.Staff AS A
        LEFT JOIN sendipay.MdGrade AS B ON A.gradeIDX = B.idx
        LEFT JOIN sendipay.DeptStd AS C ON A.deptIDX=C.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StStaffCon 디테일
    public static function getStaffDetail($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.email,
        A.name,
        A.deptIDX,
        B.idx AS gradeIDX,
        B.name AS gradeName,
        C.name AS deptName
        FROM sendipay.Staff AS A
        LEFT JOIN sendipay.MdGrade AS B ON A.gradeIDX = B.idx
        JOIN sendipay.DeptStd AS C ON A.deptIDX=C.idx
        WHERE A.idx='$idx'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StStaffCon 로그인히스토리
    public static function getStaffLoginHistory($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $targetIDX=$data['targetIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        createTime,
        ipAddress
        FROM sendipay.StaffLoginHistory
        WHERE staffIDX='$targetIDX' AND (createTime BETWEEN '$startDate' AND '$endDate')
        ORDER BY createTime DESC
        ");
        $adminList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $adminList;
    }

    //StStaffCon 이메일 유효성 검사
    public static function issetStaffEmail($data=null)
    {
        $email=$data;
        $email=trim($email);
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email
        FROM sendipay.Staff
        WHERE email='$email'
        ");
        $adminList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $adminList;
    }

    public static function GetBirthday($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email,
        name,
        birth
        FROM sendipay.Staff
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}
