<?php

namespace App\Models;

use PDO;

class CustomerMemoMo extends \Core\Model
{

    public static function GetuseIDXSearchData($data=null)
    {
        $customerIDX=$data['customerIDX'];
        $subUrlIDX=$data['subUrlIDX'];
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.useIDX,
        A.contents
        FROM sendipay.CustomerMemo AS A
        LEFT JOIN sendipay.QnaQuestion AS B
        ON(A.subUrlIDX=B.subUrlIDX)
        WHERE B.subUrlIDX='$subUrlIDX' AND A.useIDX='$customerIDX' ORDER BY A.createTime DESC
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    public static function GetDataListLoad($data=null)
    {
        $subUrlIDX=$data['subUrlIDX'];
        $userIDX=$data['userIDX'];
        if($subUrlIDX==0){
            $subUrlIDX=2;
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.subUrlIDX,
        A.useIDX,
        A.contents,
        A.writer,
        A.createTime,
        B.name AS staffName
        FROM sendipay.CustomerMemo AS A
        LEFT JOIN sendipay.Staff AS B
        ON(A.writer=B.email)
        WHERE A.useIDX='$userIDX'
        AND A.subUrlIDX='$subUrlIDX' ORDER BY A.createTime DESC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
        
    }


    /*-------------------------20230602 JB 진짜사용---------------------------------*/

    //QnaDetailCon , StMemberCon 유저메모 데이터
    public static function GetDataLoad($data=null)
    {
        $subUrlIDX=$data['subUrlIDX'];
        $customerIDX=$data['customerIDX'];
        if($subUrlIDX==0){
            $subUrlIDX=2;
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.subUrlIDX,
        A.useIDX,
        A.contents,
        A.writer,
        A.createTime,
        (SELECT name FROM sendipay.Staff WHERE email= A.writer) AS staffName
        FROM sendipay.CustomerMemo AS A
        WHERE A.useIDX='$customerIDX'
        AND A.subUrlIDX='$subUrlIDX' ORDER BY A.createTime DESC
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;

    }






}