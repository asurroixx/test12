<?php

namespace App\Models;

use PDO;

class TicketMemoMo extends \Core\Model
{

    //QnaCon 해당 티켓에 스태프 안읽음 카운트 , QnaDetailCon 랜더시 티켓메모정보
    public static function GetTicketMemoData($data=null)
    {
        $ticketCode=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.ticketCode,
        A.staffIDX,
        A.contents,
        A.createTime,
        B.email AS staffEmail,
        B.name AS staffName,
        COUNT(C.idx) AS count
        FROM sendipay.TicketMemo AS A
        LEFT JOIN sendipay.Staff AS B ON A.staffIDX=B.idx
        LEFT JOIN sendipay.TicketMemoView AS C ON A.idx=C.ticketMemoIDX AND C.status=2
        WHERE A.ticketCode='$ticketCode'
        GROUP BY A.idx
        ");
        $qnaList=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $qnaList;
    }






}