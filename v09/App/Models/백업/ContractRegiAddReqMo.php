<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractRegiAddReqMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	public static function GetRegiAddReqData($data=null)
	{
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.reqCompany,
        A.reqHomepage,
        A.reqPartnerCategoryIDX,
        A.reqBusinessLicense,
        A.reqLicense,
        A.reqPortalEmail
        FROM sendipay.ContractRegiAddReq AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        WHERE B.code='$code'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //StContractDetailCon 계약서 전송전 ContractRegiAddReq status3 조사
    public static function GetStatusUpdateData($data=null)
    {
        $code=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.status
        FROM sendipay.ContractRegiAddReq AS A
        LEFT JOIN sendipay.Contract AS B ON A.contractIDX=B.idx
        WHERE B.code='$code'
        ORDER BY A.idx DESC
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    
}