<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class StaffPostitMo extends \Core\Model
{
    // StPostitCon 해당 로그인한 스태프의 정보
    public static function GetData($data=null)
    {
        $loginIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.createTime,
        A.con,
        A.status,
        B.email AS fromEmail,
        C.email AS toEmail
        FROM sendipay.StaffPostit AS A
        JOIN sendipay.Staff AS B ON A.fromIDX=B.idx
        JOIN sendipay.Staff AS C ON A.toIDX=C.idx
        WHERE A.toIDX = '$loginIDX' ORDER BY A.createTime DESC
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;

    }

    // StPostitCon 해당 스태프의 안읽은 쪽지 카운트
    public static function GetStatusData($data=null)
    {
        $loginIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        COUNT(idx) AS count
        FROM sendipay.StaffPostit
        WHERE toIDX = '$loginIDX' AND status=2
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;

    }
}
