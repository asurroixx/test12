<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractEditReqMo extends \Core\Model
{

    //StContractEditReqCon 전체카운드
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx
        FROM sendipay.ContractEditReq
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StContractEditReqCon 데이터테이블
    public static function ContractEditReqListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.createTime,
        CASE
            WHEN A.status = 2 THEN '신청'
            WHEN A.status = 3 THEN '승인'
            WHEN A.status = 4 THEN '반려'
        END AS status,
        CASE
            WHEN A.type = 2 THEN 'APIKEY 변경'
            WHEN A.type = 3 THEN 'IP추가'
            WHEN A.type = 4 THEN 'IP삭제'
        END AS type,
        IFNULL(A.beforeValue,'-') AS beforeValue,
        IFNULL(A.afterValue,'-') AS afterValue,
        B.name AS contractName,
        C.email AS directorEmail
        FROM sendipay.ContractEditReq AS A
        JOIN sendipay.Contract AS B ON A.contractIDX = B.idx
        JOIN sendipay.Director AS C ON A.directorIDX = C.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StContractEditReqCon 디테일
    public static function getContractEditReqDetail($data=null)
    {
        $idx=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        CASE
            WHEN A.status = 2 THEN '신청'
            WHEN A.status = 3 THEN '승인'
            WHEN A.status = 4 THEN '반려'
        END AS status,
        CASE
            WHEN A.type = 2 THEN 'APIKEY 변경'
            WHEN A.type = 3 THEN 'IP추가'
            WHEN A.type = 4 THEN 'IP삭제'
        END AS type,
        A.createTime,
        IFNULL(A.beforeValue,'-') AS beforeValue,
        IFNULL(A.afterValue,'-') AS afterValue,
        A.ex,
        B.name AS contractName,
        C.email AS directorEmail,
        IFNULL(D.email,'-') AS staffEmail,
        D.name AS staffName
        FROM sendipay.ContractEditReq AS A
        JOIN sendipay.Contract AS B ON A.contractIDX = B.idx
        JOIN sendipay.Director AS C ON A.directorIDX = C.idx
        LEFT JOIN sendipay.Staff AS D ON A.staffIDX = D.idx
        WHERE A.idx='$idx'
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }

    //StContractEditReqCon ip 추가삭제요청 왔을때 정보확인
    public static function GetContractEditData($data=null)
    {
        $thisIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        contractIDX,
        directorIDX,
        status,
        type,
        afterValue,
        beforeValue
        FROM sendipay.ContractEditReq
        WHERE idx='$thisIDX'
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }

    // public static function GetContractEditReqtDetailRenderdData($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.contractIDX,
    //     A.status,
    //     A.type,
    //     A.createTime,
    //     A.updateTime,
    //     A.beforeValue,
    //     A.afterValue,
    //     A.rejectedMsg,
    //     B.name AS contractName,
    //     C.email AS directorEmail,
    //     D.name AS staffName
    //     FROM sendipay.ContractEditReq AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON(A.contractIDX = B.idx)
    //     LEFT JOIN sendipay.Director AS C
    //     ON(A.directorIDX = C.idx)
    //     LEFT JOIN sendipay.Staff AS D
    //     ON(A.staffIDX = D.idx)
    //     WHERE A.idx='$idx'
    //     ");
    //     $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }

    // public static function getContractIpStatus($data=null)
    // {
    //     $contractCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.status
    //     FROM sendipay.ContractEditReq AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON(A.contractIDX = B.idx)
    //     WHERE B.code='$contractCode'
    //     ");
    //     $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $returnData;
    // }


    //파트너 코드를줄게 관련된 요청사항을 줘 > 어디서쓰이는지 확인
    public static function GetContractEditInfo($data=null)
    {
        $pCode = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractIDX,
        A.directorIDX,
        A.status,
        A.type,
        A.beforeValue,
        A.afterValue,
        B.idx
        FROM sendipay.ContractEditReq AS A
        LEFT JOIN sendipay.Contract AS B
        ON(A.contractIDX = B.idx)
        WHERE B.code = '$pCode' AND A.type != 6
        ");
        $returnData=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $returnData;
    }
    
}
