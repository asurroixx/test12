<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class DirectorMo extends \Core\Model
{
    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    // public static function GetLogin($data=null)
    // {
    //     $code=$data['code'];
    //     $email=$data['email'];
    //     $ipAddress=$data['ipAddress'];
    //     $db = static::getDB();
    //     $GetDump = $db->prepare("SELECT
    //     A.code,
    //     B.idx,
    //     B.email
    //     FROM sendipay.DirectorLoginDump AS A
    //     LEFT JOIN sendipay.Director AS B
    //     ON (A.directorIDX = B.idx)
    //     WHERE B.email='$email' AND A.code='$code' AND A.ipAddress='$ipAddress'
    //     ORDER BY A.idx DESC LIMIT 1
    //     ");
    //     $GetDump->execute();
    //     $count = $GetDump->rowCount();
    //     $row = $GetDump->fetch(PDO::FETCH_ASSOC);
    //     return $row;
    // }



    // public static function GetGlobalVal($data=null)
    // {
    //     $idx=$data;
    //     $db = static::getDB();
    //     $GetDump = $db->prepare("SELECT
    //     idx,
    //     email,
    //     name,
    //     gradeIDX
    //     FROM sendipay.Director
    //     WHERE idx='$idx'
    //     ");
    //     $GetDump->execute();
    //     $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
    //     return $globalVal;
    // }



    // public static function GetContractSearchList($data=null)
    // {
    //     $contractCode=$data;
    //     $thisQuery='';
    //     if($contractCode=='all'){
    //         $thisQuery='';
    //     }else{
    //         $thisQuery=' WHERE B.code="'.$contractCode.'"';
    //     }
    //     $db = static::getDB();
    //     $GetDump = $db->prepare("SELECT
    //     B.idx AS contractIDX,
    //     B.name AS contractName,
    //     B.code
    //     FROM sendipay.Director AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON(A.contractIDX=B.idx)
    //     ".$thisQuery."GROUP BY A.contractIDX");
    //     $GetDump->execute();
    //     $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
    //     return $globalVal;
    // }



    // public static function issetStaffEmail($data=null)
    // {
    //     $email=$data;
    //     $email=trim($email);
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.Director
    //     WHERE email='$email'
    //     ");
    //     $directorList=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $directorList;
    // }

    /*--------------------------202306 진짜사용-----------------------------*/

    //StDirectorCon 전체 카운트
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx
        FROM sendipay.Director
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StDirectorCon 스태프페이지 디렉터 데이터테이블
    public static function GetDirectorDataLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        idx,
        email,
        IFNULL(name,'-') AS name,
        createTime,
        IFNULL(ipAddress,'-') AS ipAddress,
        CASE status
            WHEN 2 THEN '잠금'
            WHEN 3 THEN '정상'
            ELSE '' END
        AS status
        FROM sendipay.Director
        WHERE (createTime BETWEEN '$startDate' AND '$endDate')
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StDirectorCon 스태프페이지 디렉터 디테일
    public static function GetDirectorDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx,
        email,
        IFNULL(name,'-') AS name,
        createTime,
        IFNULL(ipAddress,'-') AS ipAddress,
        CASE status
            WHEN 2 THEN '잠금'
            WHEN 3 THEN '정상'
            ELSE '' END
        AS status
        FROM sendipay.Director
        WHERE idx='$targetIDX'
        ");
        $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);
        return $transactionForm;
    }

    //StContractDetailCon 업데이트 디렉터 목록
    public static function GetUpdateDirectorData($data=null)
    {
        $db = static::getDB();
        $Sel = $db->query("SELECT
        idx AS 'No',
        email AS '이메일',
        IFNULL(name,'-') AS '이름'
        FROM sendipay.Director
        WHERE status=3
        ");
        $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $transactionForm;
    }

}
