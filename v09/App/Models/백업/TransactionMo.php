<?php

namespace App\Models;

use PDO;

class TransactionMo extends \Core\Model
{

    // //StTransactionCon 총 카운트
    // public static function GetTotalCount($data=null)
    // {
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     idx
    //     FROM sendipay.Transaction
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;
    // }


    // public static function SearchContractIDXData($data=null)
    // {
    //     $inputVal=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.contractIDX,
    //     A.contractOrderID,
    //     A.customOrderID1,
    //     A.customOrderID2,
    //     A.amount,
    //     A.fee,
    //     A.status,
    //     A.contractUserID,
    //     A.dealResultIDX,
    //     A.adminEx,
    //     A.staffEx,
    //     A.contractEx,
    //     B.name AS contractName
    //     FROM sendipay.Transaction AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON A.contractIDX = B.idx
    //     WHERE B.name LIKE '%$inputVal%'
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);

    //     return $transactionForm;
    // }


    // public static function get_transactionByInvoiceId($data=null)
    // {
    //     $contractIDX=$data['contractIDX'];
    //     $contractOrderID=$data['invoice_id'];
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.status
    //     FROM sendipay.Transaction AS A
    //     LEFT JOIN sendipay.TransactionTime AS B
    //     ON (A.idx = B.transactionIDX)
    //     WHERE A.contractOrderID='$contractOrderID' AND A.contractIDX='$contractIDX'
    //     ");
    //     $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);
    //     return $transactionForm;
    // }

    // public static function GetDashboardListLoad($data=null)
    // {
    //     $contractCode=$data['contractCode'];

    //     $startDate=$data['startDate'];
    //     if($startDate==""){
    //         $startDate='1970-01-01 00:00:00';
    //     }else{
    //         $startDate.=" 00:00:00";
    //     }
    //     $endDate=$data['endDate'];
    //     if($endDate==""){
    //         $endDate=date('Y-m-d 23:59:59');
    //     }else{
    //         $endDate.=" 23:59:59";
    //     }

    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.contractOrderID,
    //     A.amount,
    //     C.completeTime,
    //     COUNT(A.idx) AS idxCount
    //     FROM sendipay.Transaction AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     LEFT JOIN sendipay.TransactionTime AS C
    //     ON (A.idx = C.transactionIDX)
    //     WHERE (C.completeTime BETWEEN '$startDate' AND '$endDate')
    //     AND B.code='$contractCode'
    //     AND A.status=4
    //     GROUP BY A.idx
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    // public static function GetContractTotalAmount($data=null)
    // {
    //     $contractCode=$data;
    //     $db = static::getDB();
    //     $Sel = $db->query("SELECT
    //     A.idx,
    //     A.amount
    //     FROM sendipay.Transaction AS A
    //     LEFT JOIN sendipay.Contract AS B
    //     ON (A.contractIDX = B.idx)
    //     WHERE B.code='$contractCode' AND A.status=4
    //     ");
    //     $transactionForm=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $transactionForm;

    //     //
    // }

    /*--------------------------------202306 진짜사용-----------------------------------------*/

    //StAppTransactionCon 전체 유저 카운트
    public static function GetNotiUser($data=null)
    {
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        COUNT(idx) AS count
        FROM sendipay.Transaction
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetch(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StAppTransactionCon 데이터테이블
    public static function GetTransactionList($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];

        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $GetDump = $db->prepare("SELECT
        A.idx,
        A.contractOrderID,
        format(A.amount, 2) AS amount,
        format(A.fee, 2) AS fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        A.contractUserID,
        A.txhash,
        B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(C.code,'-') AS dealCode,
        D.name AS contractName,
        E.nickName AS memberName
        FROM sendipay.Transaction AS A
        JOIN sendipay.TransactionTime AS B ON A.idx = B.transactionIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        LEFT JOIN sendipay.Member AS E ON A.memberIDX=E.idx
        WHERE (B.createTime BETWEEN '$startDate' AND '$endDate')
        GROUP BY A.idx
        ");
        $GetDump->execute();
        $globalVal=$GetDump->fetchAll(PDO::FETCH_ASSOC);
        return $globalVal;
    }

    //StAppTransactionCon 디테일 로드
    public static function GetTransactionDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.contractOrderID,
        A.customOrderID1,
        A.customOrderID2,
        A.amount,
        A.fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        A.contractUserID,
        A.dealResultIDX,
        A.staffEx,
        B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(C.code,'-') AS dealCode,
        IFNULL(C.name,'-') AS dealCodeName,
        D.name AS contractName,
        E.nickName AS memberName
        FROM sendipay.Transaction AS A
        LEFT JOIN sendipay.TransactionTime AS B ON A.idx = B.transactionIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        LEFT JOIN sendipay.Member AS E ON A.memberIDX=E.idx
        WHERE A.idx='$targetIDX'
        ");
        $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $transactionForm;
    }

    //StTransactionCon 데이터테이블
    public static function Get_transactionList($data=null)
    {
        //admin과 portal 둘다씀 수정시 주의! > 어드민없음
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        $endDate=$data['endDate'];
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        IFNULL(A.contractOrderID,'-') AS contractOrderID,
        A.amount,
        A.fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        IFNULL(A.contractUserID,'-') AS contractUserID,
        B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(D.name,'-') AS contractName
        FROM sendipay.Transaction AS A
        JOIN sendipay.TransactionTime AS B ON A.idx = B.transactionIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        WHERE (B.createTime BETWEEN '$startDate' AND '$endDate')
        GROUP BY A.idx
        ");
        $transaction=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $transaction;
    }

    //StTransactionCon 디테일
    public static function get_transactionForm($data=null)
    {
        $targetIDX=$data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        IFNULL(A.contractOrderID,'-') AS contractOrderID,
        IFNULL(A.customOrderID1,'-') AS customOrderID1,
        IFNULL(A.customOrderID2,'-') AS customOrderID2,
        A.amount,
        A.fee,
        CASE A.status
            WHEN 2 THEN 'Request'
            WHEN 3 THEN 'Pending'
            WHEN 4 THEN 'Complete'
            WHEN 5 THEN 'Fail'
            ELSE '' END
        AS status,
        IFNULL(A.contractUserID,'-') AS contractUserID,
        A.dealResultIDX,
        A.staffEx,
        B.createTime,
        CASE B.completeTime
            WHEN B.completeTime='0000-00-00 00:00:00' THEN '-'
            ELSE B.completeTime END
        AS completeTime,
        IFNULL(C.code,'-') AS dealCode,
        IFNULL(C.name,'-') AS dealCodeName,
        IFNULL(D.name,'-') AS contractName
        FROM sendipay.Transaction AS A
        JOIN sendipay.TransactionTime AS B ON A.idx = B.transactionIDX
        LEFT JOIN sendipay.MdDealResult AS C ON A.dealResultIDX = C.idx
        LEFT JOIN sendipay.Contract AS D ON A.contractIDX = D.idx
        WHERE A.idx='$targetIDX'
        ");
        $transactionForm=$Sel->fetch(PDO::FETCH_ASSOC);

        return $transactionForm;
    }

}