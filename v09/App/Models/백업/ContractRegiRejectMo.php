<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ContractRegiRejectMo extends \Core\Model
{
    //StContractCon 반려정보 가져오기
    public static function getContractRegiRejectInfo($data=null)
    {
        $contractCode = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        A.idx,
        A.staffIDX,
        A.rejectTime,
        A.rejectReason,
        B.name AS rejectStaffName
        FROM sendipay.ContractRegiReject AS A
        LEFT JOIN sendipay.Staff AS B 
        ON(A.staffIDX = B.idx)
        WHERE A.code = '$contractCode'
        ");
        $returnData=$Sel->fetch(PDO::FETCH_ASSOC);
        return $returnData;
    }
}
