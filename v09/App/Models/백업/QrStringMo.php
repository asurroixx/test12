<?php

namespace App\Models;

use PDO;

class QrStringMo extends \Core\Model
{
    public static function QrIdxCodeSel($data=null) 
    {//idx에 맞는 원래 32스트링 값을 가져오기
        $thisIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        qrString
        FROM sendipay.QrString 
        WHERE idx='$thisIDX'
        ");
        $qnaList=$Sel->fetch(PDO::FETCH_ASSOC);
        return $qnaList;
    }

    public static function QrRederInfoFind($data=null) //QR로 찍어 나온 복호화한 랜덤 스트링을 대조해서 찾는과정
    {
        $randomString = $data;
        $db = static::getDB();
        $randomStringDBSel = $db->query("SELECT
        code
        FROM sendipay.QrString 
        WHERE qrString='$randomString'
        ");
        $code=$randomStringDBSel->fetch(PDO::FETCH_ASSOC);
        return $code;
    }

    //20221123
    public static function GetQrNumberSearch($data=null) //qrNumber가 겹치는지 확인
    {
        $string_generated = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        qrNumber
        FROM sendipay.QrString 
        WHERE qrNumber='$string_generated'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function QrRemadeCodeSel($data=null)//60초 지났을때 qr이미지 다시만드는 과정
    {
        $thisIDX = $data;
        $db = static::getDB();
        $Sel = $db->query("SELECT
        qrFakeString,
        qrNumber
        FROM sendipay.QrString 
        WHERE idx='$thisIDX'
        ");
        $reCode=$Sel->fetch(PDO::FETCH_ASSOC);
        return $reCode;
    }
}