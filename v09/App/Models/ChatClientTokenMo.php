<?php

namespace App\Models;

use PDO;

class ChatClientTokenMo extends \Core\Model
{


    public static function IssetTargetData($data=null)
    {


        $token=$data['token'];
        $idx=$data['idx'];
        $dbName= self::MainDBName;
        $db = static::GetDB();
        $query = $db->prepare("SELECT
        idx,
        token,
        createTime,
        statusIDX
        FROM $dbName.ChatClientToken
        WHERE loginIDX = :idx AND token = :token ORDER BY createTime DESC LIMIT 1
        ");
        $query->bindValue(':idx', $idx);
        $query->bindValue(':token', $token);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }




}