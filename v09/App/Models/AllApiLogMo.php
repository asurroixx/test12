<?php

namespace App\Models;

use PDO;

class AllApiLogMo extends \Core\Model
{
    // all Log 전체 불러오기
    public static function GetMarketAllApiData($data=null)
    {
        $startDate  = $data['startDate'] ?? '1970-01-01';
        $endDate    = $data['endDate'] ?? date('Y-m-d');
        $startDate .= ' 00:00:00';
        $endDate   .= ' 23:59:59';

        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $query = $db->prepare("SELECT
        idx,
        log,
        marketCode,
        ip,
        action,
        type,
        createTime
        FROM $dbName.AllApiLog
        WHERE (createTime BETWEEN :startDate AND :endDate)
        ");
        $query->bindValue(':startDate', $startDate);
        $query->bindValue(':endDate', $endDate);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_ASSOC);

        // 로그에서 데이터뿌릴때 ' 를 " 로 변경
        foreach ($results as &$row) {
            $row['log'] = str_replace("'", '"', $row['log']);
        }

        return $results;
    }


}

