<?php

namespace App\Models;

use PDO;

class ManagerLoginHistoryMo extends \Core\Model
{

    //MarketManagerCon 해당 매니저 최근로그인
    public static function GetRecentLoginTime($data=null)
    {
        $loginIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
            IFNULL(MAX(createTime), '-') AS recentLoginTime
        FROM $dbName.ManagerLoginHistory
        WHERE loginIDX= :loginIDX
        ");
        $query->bindValue(':loginIDX', $loginIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //매니저 로그인 히스토리 내역
    public static function GetLoginHisList($data=null)
    {
        $loginIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $query = $db->prepare("SELECT
            ROW_NUMBER() OVER (ORDER BY createTime ASC) AS no,
            IFNULL(createTime, '-') AS loginTime,
            ipAddress
        FROM $dbName.ManagerLoginHistory
        WHERE loginIDX= :loginIDX
        ORDER BY createTime DESC
        ");
        $query->bindValue(':loginIDX', $loginIDX);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ManagerLoginHistoryCon
    public static function GetDatatableList($data=null)
    {
        $startDate   = $data['startDate'] ?? '1970-01-01';
        $endDate     = $data['endDate'] ?? date('Y-m-d');
        $startDate  .= ' 00:00:00';
        $endDate    .= ' 23:59:59';
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $query = $db->prepare("SELECT
            A.idx,
            ROW_NUMBER() OVER (ORDER BY A.createTime ASC) AS no,
            IFNULL(A.createTime, '-') AS loginTime,
            A.ipAddress,
            B.idx AS managerIDX,
            AES_DECRYPT(B.email,:dataDbKey) AS email
        FROM $dbName.ManagerLoginHistory AS A
        JOIN $dbName.Manager AS B ON A.loginIDX=B.idx
        WHERE (A.createTime BETWEEN '$startDate' AND '$endDate')
        ORDER BY A.createTime DESC
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->execute();
        $result=$query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}