<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientLogMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */



    public static function GetLogList($data=null)
    {
        $targetIDX=$data['targetIDX'];
        $whereType=$data['whereType'];
        $whereArr=$data['whereArr'];
        $whereQuery='';
        if (!empty($whereArr)){
            switch ($whereType) {
                case 'like':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $string = 'A.statusIDX LIKE \'' . $value . '%\'';
                        $arr[] = $string;
                    }
                    $whereQuery = ' AND (' . implode(' OR ', $arr) . ') ';
                break;
                case 'between':
                    $startRange = $whereArr['start'].'000';
                    $endRange = $whereArr['end'].'999';
                    $whereQuery =' AND A.statusIDX BETWEEN '.$startRange.' AND '.$endRange;
                break;
                case 'and':
                    $arr = array();
                    foreach ($whereArr as $value) {
                        $arr[] = $value;
                    }
                    $whereQuery = ' AND A.statusIDX IN (' . implode(',', $arr) . ')';
                break;
                default:
            }
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        -- DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTime,
        A.staffIDX,
        B.memo,
        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,
        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS clientEmail,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS clientName,
        E.ex
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.Client AS D ON A.clientIDX=D.idx
        LEFT JOIN $dbName.ClientLogEx AS E ON A.idx=E.logIDX
        WHERE A.targetIDX =$targetIDX$whereQuery
        -- ORDER BY A.createTime DESC; << ORDER BY idx 추가 20240116 jm
        ORDER BY A.createTime DESC, A.idx DESC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetPtpDetailStatusLog($data=null)
    {
        $targetIDX=$data;

        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        -- DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTimeKst,

        DATE_ADD(A.createTime, INTERVAL 9 HOUR) AS createTimeKst,
        -- A.createTime AS createTime,

        A.staffIDX,
        (SELECT memo AS status FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
        B.memo,

        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,

        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS clientEmail,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS clientName,

        E.ex

        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.Client AS D ON A.clientIDX=D.idx
        LEFT JOIN $dbName.ClientLogEx AS E ON A.idx=E.logIDX
        WHERE A.targetIDX =$targetIDX
        AND A.statusIDX IN (251101, 251102, 251201, 251301, 251401, 251402, 251403)
        GROUP BY A.statusIDX
        ORDER BY A.createTime ASC, A.idx ASC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //고객 목록 전용
    //240213 채팅에서 뿌림 수정시 채팅쪽도 수정해야함
    public static function GetClientLogList($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.targetIDX,
        A.createTime,
        A.staffIDX,
        B.memo,
        AES_DECRYPT(C.email, :dataDbKey) AS staffEmail,
        AES_DECRYPT(C.name, :dataDbKey) AS staffName,
        IFNULL(AES_DECRYPT(D.email, :dataDbKey),'-') AS clientEmail,
        IFNULL(AES_DECRYPT(D.name, :dataDbKey),'-') AS clientName,
        E.ex
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.Staff AS C ON A.staffIDX=C.idx
        LEFT JOIN $dbName.Client AS D ON A.clientIDX=D.idx
        LEFT JOIN $dbName.ClientLogEx AS E ON A.idx=E.logIDX
        WHERE A.clientIDX = :targetIDX
        ORDER BY A.createTime DESC, A.idx DESC
        ");
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //clientLogCon 데이터테이블
    public static function GetDataTableListLoad($data=null)
    {
        $startDate=$data['startDate'];
        $endDate=$data['endDate'];
        if($startDate==""){
            $startDate='1970-01-01 00:00:00';
        }else{
            $startDate.=" 00:00:00";
        }
        if($endDate==""){
            $endDate=date('Y-m-d 23:59:59');
        }else{
            $endDate.=" 23:59:59";
        }
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;

        $Sel = $db->prepare("SELECT
        A.idx,
        A.statusIDX,
        A.clientIDX,
        A.createTime,
        B.memo,
        CASE
            WHEN AES_DECRYPT(C.name, :dataDbKey) IS NULL THEN '-'
            ELSE AES_DECRYPT(C.name, :dataDbKey)
        END AS clientName,
        AES_DECRYPT(C.phone, :dataDbKey) AS phone,
        CASE
            WHEN AES_DECRYPT(E.name, :dataDbKey) IS NULL THEN '-'
            ELSE AES_DECRYPT(E.name, :dataDbKey)
        END AS staffName,
        D.ex
        FROM $dbName.ClientLog AS A
        LEFT JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        INNER JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.ClientLogEx AS D ON A.idx=D.logIDX
        LEFT JOIN $dbName.Staff AS E ON A.staffIDX=E.idx
        WHERE A.createTime BETWEEN :startDate AND :endDate;
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':startDate', $startDate);
        $Sel->bindValue(':endDate', $endDate);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //clientLogCon 디테일
    public static function GetClientDetailData($data=null)
    {
        $targetIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        A.idx,
        A.clientIDX,
        A.createTime,
        A.statusIDX,
        B.memo,
        CONCAT(AES_DECRYPT(C.name,:dataDbKey),' (',AES_DECRYPT(C.email,:dataDbKey),')') AS clientInfo,
        IFNULL(CONCAT(AES_DECRYPT(E.name,:dataDbKey),' (',AES_DECRYPT(E.email,:dataDbKey),')'),'-') AS staffInfo,
        IFNULL(D.ex,'-') AS ex
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        INNER JOIN $dbName.Client AS C ON A.clientIDX=C.idx
        LEFT JOIN $dbName.ClientLogEx AS D ON A.idx=D.logIDX
        LEFT JOIN $dbName.Staff AS E ON A.staffIDX=E.idx
        WHERE A.idx = :targetIDX
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':targetIDX', $targetIDX);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}