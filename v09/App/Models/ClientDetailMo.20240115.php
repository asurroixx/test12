<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class ClientDetailMo extends \Core\Model
{
	/**
	 * Get all the users as an associative array
	 *
	 * @return array
	 */

	//ClientDetailCon 해당 clientIDX 정보 가져오기 (마일리지 입금내역)
	public static function GetTotalClientMileageDeposit($data=null)
	{
		$targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(A.idx) AS totalDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203201 AND clientIDX='$targetIDX') AS manualDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203202 AND clientIDX='$targetIDX') AS autoDeposit,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=203203 AND clientIDX='$targetIDX') AS accountDeposit,
        IFNULL(MAX(B.createTime),'-') AS recentTime,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        JOIN $dbName.ClientMileageDeposit AS B ON A.targetIDX=B.idx
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX IN (203201, 203202, 203203)
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
	}

	//ClientDetailCon 해당 clientIDX 정보 가져오기 (마일리지 출금내역) [db 깨끗한 상태로 한번 확인해볼것]
    public static function GetTotalClientMileageWithDrawal($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        /*
            204101,204102의 총 카운트에서 (4) 204211와 204212가 있는 데이터들을 (1) 조사 한다음
            출금이 취소된 데이터를 빼준 다음 카운트롤 출력해줌 (0이상일때)
        */
        COUNT(A.idx) AS withCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204101 AND clientIDX='$targetIDX') AS manualWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE statusIDX=204102 AND clientIDX='$targetIDX') AS autoWithCount,
        (SELECT COUNT(idx) FROM $dbName.ClientMileage WHERE clientIDX='$targetIDX' AND statusIDX IN (204211, 204212)) AS minusCount,
        IFNULL(MAX(A.createTime),'-') AS recentTime,
        /*
            204101,204102의 총 amount에서 (1,000,000)구한 후
            출금이 취소된 204211,204212의 총 amount (50,000)를 빼준 amount (950,000)의 데이터를 출력해줌(0이상일때)
        */
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS tmpTotalAmount,
        (SELECT IFNULL(FORMAT(SUM(amount),0),'0') FROM $dbName.ClientMileage WHERE clientIDX='$targetIDX' AND statusIDX IN (204211, 204212)) AS minusSUMAmount,

        /*
            avgWithAmount는 총 출금액(출금이 취소된 데이터를 안 뺀 금액)에서 총카운트(출금이 취소 안된 총 횟수)평균만 계산함
        */
        -- IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        (SELECT IFNULL(MAX(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX='$targetIDX' AND statusIDX NOT IN (204211, 204212)) AS tmpMaxWithAmount,
        (SELECT IFNULL(MIN(amount)+fee,'0') FROM $dbName.ClientMileageWithdrawal WHERE clientIDX='$targetIDX' AND statusIDX NOT IN (204211, 204212)) AS tmpMinWithAmount

        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX IN (204101, 204102);
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);

        $withCount=$result['withCount'];
        $manualWithCount=$result['manualWithCount'];
        $autoWithCount=$result['autoWithCount'];
        $minusCount=$result['minusCount'];

        $totalWithdrawal=0;
        $manualWithdrawal=0;
        $autoWithdrawal=0;

        //총 출금횟수
        if($withCount>0){
            $totalWithdrawal=$withCount-$minusCount;
            if($totalWithdrawal < 0) {
                $totalWithdrawal = 0;
            }
        }
        //수동출금수
        if($manualWithCount>0){
            $manualWithdrawal=$manualWithCount-$minusCount;
            if($manualWithdrawal < 0) {
                $manualWithdrawal = 0;
            }
        }
        //자동출금수
        if($autoWithCount>0){
            $autoWithdrawal=$autoWithCount-$minusCount;
            if($autoWithdrawal < 0) {
                $autoWithdrawal = 0;
            }
        }


        // --------------------------
        $tmpTotalAmount = intval(str_replace(',', '', $result['tmpTotalAmount']));
        $minusSUMAmount = intval(str_replace(',', '', $result['minusSUMAmount']));

        $tmpMaxWithAmount = intval(str_replace(',', '', $result['tmpMaxWithAmount']));
        $tmpMinWithAmount = intval(str_replace(',', '', $result['tmpMinWithAmount']));

        $totalAmount=0;
        $totalAvgWithAmount=0;
        $maxWithAmount=0;
        $minWithAmount=0;

        //총 출금액
        if($tmpTotalAmount>0){
            $totalAmountNumber=$tmpTotalAmount-$minusSUMAmount;
            $totalAmount = number_format($totalAmountNumber);
        }

        //출금 평균금액
        if($totalWithdrawal>0){
            $totalAvgWithAmountNumber=$totalAmountNumber/$totalWithdrawal;
            $totalAvgWithAmount = number_format($totalAvgWithAmountNumber);
        }
        //최대 출금액
        if($tmpMaxWithAmount>0){
            $maxWithAmountNumber=$tmpMaxWithAmount;
            $maxWithAmount = number_format($maxWithAmountNumber);
        }
        //최소 출금액
        if($tmpMinWithAmount>0){
            $minWithAmountNumber=$tmpMinWithAmount;
            $minWithAmount = number_format($minWithAmountNumber);
        }



        $result['totalWithdrawal'] = $totalWithdrawal;
        $result['manualWithdrawal'] = $manualWithdrawal;
        $result['autoWithdrawal'] = $autoWithdrawal;
        $result['totalAmount'] = $totalAmount;
        $result['avgWithAmount'] = $totalAvgWithAmount;
        $result['maxWithAmount'] = $maxWithAmount;
        $result['minWithAmount'] = $minWithAmount;



        return $result;
    }

    //MileageWithdrawalCon clientIDX 총 마일리지
    public static function getMileage($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
            COALESCE((
                SUM(
                    /*203201, 203202, 203203, 204211, 204212, 205101, 908102, 251402, 251401 앱*/
                    CASE
                        WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401) THEN A.amount
                        ELSE 0
                    END
                ) - SUM(
                    /*204101, 204102, 205201, 906101, 251201 앱*/
                    CASE
                        WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN A.amount
                        ELSE 0
                    END
                )
            ), 0) AS mileage
            FROM $dbName.ClientMileage AS A WHERE A.clientIDX='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        if(isset($result['mileage']) && $result['mileage'] !=null){
            $mileage=$result['mileage'];
            $resultMileage=number_format($mileage);
            return $resultMileage;
        }else{ return 0;}
    }

    //ClientDetailCon 데이터테이블 (마일리지 입금내역)
    public static function GetClientMileageDepositDataTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        CASE A.statusIDX
            WHEN 203101 THEN '수동신청'
            WHEN 203102 THEN '자동신청'
            WHEN 203103 THEN '가상계좌신청'
            WHEN 203104 THEN '수동신청보류'
            WHEN 203201 THEN '수동완료'
            WHEN 203202 THEN '자동완료'
            WHEN 203203 THEN '가상계좌완료'
            WHEN 203211 THEN '기타취소'
            WHEN 203212 THEN '스태프취소'
            WHEN 203213 THEN '유저취소'
            WHEN 203214 THEN '가상계좌실패'
            WHEN 203215 THEN '크론취소'
            ELSE '' END
        AS Status,
        FORMAT(A.amount, 0) AS Amount,
        A.createTime AS CreateTime,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS AccountNumber,
        F.name AS AccountName,
        IFNULL((SELECT AES_DECRYPT(email, :dataDbKey) FROM $dbName.Staff WHERE idx=G.StaffIDX),'-') AS StaffEmail,
        IFNULL(G.createTime,'-') AS UpdateTime
        FROM $dbName.ClientMileageDeposit AS A
        JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        JOIN $dbName.Client AS C ON B.clientIDX=C.idx
        JOIN $dbName.EbuyBank AS D ON A.ebuyBankIDX=D.idx
        JOIN $dbName.Bank AS E ON D.bankIDX=E.idx
        JOIN $dbName.Bank AS F ON B.bankIDX=F.idx
        LEFT JOIN $dbName.ClientLog AS G
            ON ( A.statusIDX = 203201 AND G.statusIDX = 203201 AND  A.idx = G.targetIDX )
            OR ( A.statusIDX = 203212 AND G.statusIDX = 203212 AND  A.idx = G.targetIDX )
        WHERE A.clientIDX=:clientIDX ORDER BY A.createTime DESC

        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetialCon 데이터테이블 (마일리지 출금내역)
    public static function GetClientMileageWithdrawalDataTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $Sel = $db->prepare("SELECT
        CASE A.statusIDX
            WHEN 204101 THEN '수동신청'
            WHEN 204102 THEN '자동신청'
            WHEN 204201 THEN '수동완료'
            WHEN 204202 THEN '자동완료'
            WHEN 204211 THEN '기타취소'
            WHEN 204212 THEN '스태프취소'
            ELSE '' END
        AS Status,
        FORMAT(A.amount, 0) AS Amount,
        FORMAT(A.fee, '0') AS Fee,
        A.createTime AS CreateTime,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS AccountNumber,
        D.name AS AccountName,
        IFNULL((SELECT AES_DECRYPT(email, :dataDbKey) FROM $dbName.Staff WHERE idx=E.StaffIDX),'-') AS StaffEmail,
        IFNULL(E.createTime,'-') AS UpdateTime
        FROM $dbName.ClientMileageWithdrawal AS A
        JOIN $dbName.ClientBank AS B ON A.clientBankIDX=B.idx
        JOIN $dbName.Client AS C ON B.clientIDX=C.idx
        JOIN $dbName.Bank AS D ON B.bankIDX=D.idx
        LEFT JOIN $dbName.ClientLog AS E
            ON ( A.statusIDX = 204201 AND E.statusIDX = 204201 AND  A.idx = E.targetIDX )
            OR ( A.statusIDX = 204212 AND E.statusIDX = 204212 AND  A.idx = E.targetIDX )
        WHERE A.clientIDX=:clientIDX ORDER BY A.createTime DESC
        ");
        $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->bindValue(':clientIDX', $clientIDX);
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 마일리지 변동내역 데이터테이블
    // public static function GetClientMileageVariationHistoryTable($data=null)
    // {
    //     $clientIDX=$data;
    //     $db = static::GetDB();
    //     $dbName= self::MainDBName;

    //     // 20231219 205101 , 205201 이걸 빼야할지 넣어야할지 회의 후 결정
    //     // 스태프에 의한 마일리지 증,차감

    //     // 세션 변수 초기화
    //     $db->exec("SET @resultAmount := 0;");

    //     $Sel = $db->query("SELECT
    //     (SELECT
    //         CASE
    //             WHEN idx IN(203201,203202,203203,205101) THEN '증감'
    //             WHEN idx IN(204201,204202,205201) THEN '차감'
    //             ELSE '' END
    //         AS status
    //     FROM $dbName.Status WHERE A.statusIDX=idx) AS status,
    //     A.createTime AS createTime,
    //     IFNULL(FORMAT(B.amount, 0),'-') AS increaseAmount,
    //     CASE
    //         WHEN (C.amount IS NULL OR C.amount = 0) AND (C.fee IS NULL OR C.fee = 0) THEN '-'
    //         ELSE FORMAT(IFNULL(C.amount, 0) + IFNULL(C.fee, 0), 0)
    //     END AS deductibleAmount,
    //     @resultAmount := @resultAmount + (IFNULL(B.amount, 0) - IFNULL(C.amount, 0) - IFNULL(C.fee, 0)) AS resultAmount
    //     FROM $dbName.ClientLog AS A
    //     LEFT JOIN $dbName.ClientMileageDeposit AS B ON A.targetIDX=B.idx AND A.statusIDX IN (203201,203202,203203,205101)
    //     LEFT JOIN $dbName.ClientMileageWithdrawal AS C ON A.targetIDX=C.idx AND A.statusIDX IN (204201,204202,205201)
    //     WHERE A.clientIDX='$clientIDX' AND A.statusIDX IN (203201,203202,203203,205101,204201,204202,205201) ORDER BY A.createTime DESC , C.createTime DESC
    //     ");
    //     $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
    //     return $result;
    // }

    //ClientDetailCon 마일리지 변동내역 데이터테이블
    public static function GetClientMileageVariationHistoryTable($data=null)
    {
        $clientIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        // 세션 변수 초기화
        $db->exec("SET @resultAmount := 0;");
        $Sel = $db->query("SELECT
        CASE
            WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212) THEN '마일리지 증감'
            WHEN A.statusIDX IN (205101) THEN '스태프 증감'
            WHEN A.statusIDX IN (908102) THEN '출금대행 증감'
            WHEN A.statusIDX IN (251402, 251401) THEN 'P2P거래 증감'
            WHEN A.statusIDX IN (204101, 204102) THEN '마일리지 차감'
            WHEN A.statusIDX IN (205201) THEN '스태프 차감'
            WHEN A.statusIDX IN (906101) THEN '지불대행 차감'
            WHEN A.statusIDX IN (251201) THEN 'P2P거래 차감'
            ELSE ''
        END AS Status,
        A.createTime AS CreateTime,
        CASE
            WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401) THEN IFNULL(A.amount, 0)
            ELSE NULL
        END AS IncreaseAmount,
        CASE
            WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN IFNULL(A.amount, 0)
            ELSE NULL
        END AS DecreaseAmount,
        @resultAmount := @resultAmount + (
            CASE
                WHEN A.statusIDX IN (203201, 203202, 203203, 204211, 204212, 205101 , 908102, 251402, 251401) THEN IFNULL(A.amount, 0)
                WHEN A.statusIDX IN (204101, 204102, 205201, 906101, 251201) THEN -IFNULL(A.amount, 0)
                ELSE 0
            END
        ) AS ResultAmount
        FROM ebuy.ClientMileage AS A
        WHERE A.clientIDX='$clientIDX'
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 클라이언트 기본정보
    public static function GetClientDetailData($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        A.ci,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.birth,:dataDbKey) AS birth,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        A.gradeIDX,
        CASE A.statusIDX
            WHEN 226101 THEN '정상'
            WHEN 226201 THEN '탈퇴'
            WHEN 226202 THEN '스태프차단'
            WHEN 226203 THEN '앱미가입'
            ELSE '' END
        AS status,
        A.createTime,
        B.name AS gradeName,
        B.defaultFeeKRW,
        B.tradeFeePer,
        B.tranFeePer,
        B.withFeePer,
        B.withFeeKRW,
        B.withMinKRW,
        B.upto,
        C.deviceNumber,
        CASE C.deviceType
            WHEN 'A' THEN '안드로이드'
            WHEN 'I' THEN '아이폰'
            ELSE '' END
        AS deviceType
        FROM $dbName.Client AS A
        LEFT JOIN $dbName.ClientGrade AS B ON A.gradeIDX=B.idx
        LEFT JOIN $dbName.ClientDevice AS C ON A.deviceIDX=C.idx
        LEFT JOIN $dbName.ClientBank AS D ON A.idx=D.clientIDX
        WHERE A.idx=:targetIDX
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':targetIDX', $targetIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 클라이언트 idx 등록은행
    public static function GetTargetClientData($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        A.idx,
        AES_DECRYPT(A.name,:dataDbKey) AS name,
        AES_DECRYPT(A.phone,:dataDbKey) AS phone,
        IFNULL(B.createTime,'-') AS createTime,
        AES_DECRYPT(B.accountNumber,:dataDbKey) AS accountNumber,
        AES_DECRYPT(B.accountHolder,:dataDbKey) AS accountHolder,
        C.name AS bankName,
        C.code AS bankCode
        FROM $dbName.Client AS A
        JOIN $dbName.ClientBank AS B ON A.idx = B.clientIDX
        JOIN $dbName.Bank AS C ON B.BankIDX = C.idx
        WHERE A.idx = :clientIDX
        ORDER BY B.createTime DESC
        LIMIT 1;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 클라이언트 이메일
    public static function GetTargetClientEmailLog($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $dataDbKey=self::dataDbKey;
        $query = $db->prepare("SELECT
        AES_DECRYPT(A.email,:dataDbKey) AS email,
        IFNULL(B.createTime,'-') AS createTime
        FROM $dbName.Client AS A
        JOIN $dbName.ClientLog AS B ON A.idx = B.clientIDX
        WHERE A.idx = :clientIDX AND B.statusIDX=228101
        ORDER BY B.createTime DESC
        LIMIT 1;
        ");
        $query->bindValue(':dataDbKey', $dataDbKey);
        $query->bindValue(':clientIDX', $clientIDX);
        $query->execute();
        $result=$query->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function GetClientLogList($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        A.createTime AS CreateTime,
        B.memo AS Memo,
        IFNULL(C.ex,'-') AS Ex
        FROM $dbName.ClientLog AS A
        INNER JOIN $dbName.Status AS B ON A.statusIDX=B.idx
        LEFT JOIN $dbName.ClientLogEx AS C ON A.idx=C.logIDX
        WHERE A.clientIDX = '$targetIDX'
        ORDER BY A.createTime DESC;
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 clientIDX 정보 가져오기 (지불대행)
    public static function GetTotalClientDeposit($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(A.idx) AS totalDeposit,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX = 906101
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 client 지불대행 테이블
    public static function GetClientDepositDataTable($data=null)
    {
        $referenceIdArr = $data;
        $query = '';

        // 배열이 비어있지 않다면 IN 절 생성
        if (!empty($referenceIdArr)) {
            $query = ' IN (';

            // 배열을 순회하며 각 이메일을 쿼리에 추가
            foreach ($referenceIdArr as $key) {
                $referenceId = $key['referenceId'];
                $query .= "'" . $referenceId . "',";
            }

            // 뒤의 콤마(,) 제거
            $query = rtrim($query, ',');
            $query .= ')';
        } else {
            // 배열이 비어있다면 빈 문자열로 설정
            $query = '';
        }

        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->prepare("SELECT
            CASE A.statusIDX
                WHEN 906101 THEN '지불대행 완료'
                ELSE '' END
            AS Status,
            B.memo AS Memo,
            MAX(A.createTime) AS CreateTime,
            A.marketCode AS MarketCode,
            A.amount AS Amount,
            A.invoiceID AS InvoiceID,
            A.marketUserEmail AS Email
        FROM $dbName.MarketDepositLog AS A
        LEFT JOIN $dbName.Status AS B ON A.statusIDX = B.idx
        WHERE marketUserEmail ".$query." AND A.statusIDX = 906101
        GROUP BY A.invoiceID, A.idx, A.statusIDX, A.marketCode, A.amount, A.marketUserEmail, B.memo
        ORDER BY CreateTime DESC;
        ");
        // $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result = $Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 clientIDX 정보 가져오기 (출금대행)
    public static function GetTotalClientWithdrawal($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(A.idx) AS totalDeposit,
        IFNULL(FORMAT(SUM(A.amount),0),'0') AS totalAmount,
        IFNULL(FORMAT(AVG(A.amount), 0), '0') AS avgWithAmount,
        IFNULL(FORMAT(MAX(A.amount), 0), '0') AS maxWithAmount,
        IFNULL(FORMAT(MIN(A.amount), 0), '0') AS minWithAmount
        FROM $dbName.ClientMileage AS A
        WHERE A.clientIDX='$targetIDX' AND A.statusIDX = 908102
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //ClientDetailCon 해당 client 출금대행 테이블
    public static function GetClientWithdrawalDataTable($data=null)
    {
        $referenceIdArr = $data;
        $query = '';

        // 배열이 비어있지 않다면 IN 절 생성
        if (!empty($referenceIdArr)) {
            $query = ' IN (';

            // 배열을 순회하며 각 이메일을 쿼리에 추가
            foreach ($referenceIdArr as $key) {
                $referenceId = $key['referenceId'];
                $query .= "'" . $referenceId . "',";
            }

            // 뒤의 콤마(,) 제거
            $query = rtrim($query, ',');
            $query .= ')';
        } else {
            // 배열이 비어있다면 빈 문자열로 설정
            $query = '';
        }

        $db     = static::GetApiDB();
        $dbName = self::EbuyApiDBName;
        $dataDbKey = self::dataDbKey;
        $Sel = $db->prepare("SELECT
            CASE A.statusIDX
                WHEN 908102 THEN '출금대행 완료'
                ELSE '' END
            AS Status,
            B.memo AS Memo,
            MAX(A.createTime) AS CreateTime,
            A.marketCode AS MarketCode,
            A.amount AS Amount,
            A.invoiceID AS InvoiceID,
            A.marketUserEmail AS Email
        FROM $dbName.MarketWithdrawalLog AS A
        LEFT JOIN $dbName.Status AS B ON A.statusIDX = B.idx
        WHERE marketUserEmail ".$query." AND A.statusIDX = 908102
        GROUP BY A.invoiceID, A.idx, A.statusIDX, A.marketCode, A.amount, A.marketUserEmail, B.memo
        ORDER BY CreateTime DESC;
        ");
        // $Sel->bindValue(':dataDbKey', $dataDbKey);
        $Sel->execute();
        $result = $Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    
}