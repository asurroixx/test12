<?php

namespace App\Models;

use PDO;

class ClientMileageMo extends \Core\Model
{

    //PartnerCon 데이터테이블
    public static function GetApiWithdrawalCount($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(idx) as idxCount,
        MAX(createTime) as createTime
        FROM $dbName.ClientMileage
        WHERE clientIDX = '$clientIDX'
        AND statusIDX LIKE '908%'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    //PartnerCon 데이터테이블
    public static function GetApiDepositCount($data=null)
    {
        $clientIDX = $data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        COUNT(idx) as idxCount,
        MAX(createTime) as createTime
        FROM $dbName.ClientMileage
        WHERE clientIDX = '$clientIDX'
        AND statusIDX LIKE '906%'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //20240116 jm getMileage는 ClientDetailMo.php 에서 함
}
