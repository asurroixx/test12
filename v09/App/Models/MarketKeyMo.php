<?php

namespace App\Models;

use PDO;

class MarketKeyMo extends \Core\Model
{

    //MarketKey 데이터테이블
    public static function DataTableListLoad($data=null)
    {

        $db = static::GetDB();
        $dbName= self::MainDBName;

        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;


        $Sel = $apiDb->prepare("SELECT
        A.idx,
        A.marketCode,
        A.marketKey,
        A.ip,
        CASE A.statusIDX
            WHEN 429101 THEN '정상'
            WHEN 429201 THEN '삭제대기'
            WHEN 429301 THEN '삭제(매니저)'
            WHEN 139101 THEN '삭제(크론)'
            ELSE '' END
        AS status,
        A.managerIDX,
        A.createTime,
        IFNULL(A.deletionTime,'-') AS deletionTime
        FROM $apiDbName.MarketKey AS A
        ");
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);

        $dataDbKey=self::dataDbKey;
        foreach ($result as $key => $row) {
            $apiManagerIDX=$row['managerIDX'];
            $apiMarketCode=$row['marketCode'];
            $Sel2 = $db->prepare("SELECT
            AES_DECRYPT(B.email,:dataDbKey) AS email,
            C.name AS marketName
            FROM $dbName.MarketManager AS A
            LEFT JOIN $dbName.Manager AS B ON A.managerIDX=B.idx
            LEFT JOIN $dbName.Market AS C ON A.marketIDX=C.idx
            WHERE A.managerIDX = :apiManagerIDX OR C.code = :apiMarketCode
            ");
            $Sel2->bindValue(':dataDbKey', $dataDbKey);
            $Sel2->bindValue(':apiManagerIDX', $apiManagerIDX);
            $Sel2->bindValue(':apiMarketCode', $apiMarketCode);
            $Sel2->execute();
            $mainDbResult=$Sel2->fetch(PDO::FETCH_ASSOC);

            if (isset($mainDbResult['email'])) {
                $email = $mainDbResult['email'];
            } else {
                $email = null;
            }

            if (isset($mainDbResult['marketName'])) {
                $marketName = $mainDbResult['marketName'];
            } else {
                $marketName = null;
            }


            $result[$key]['email'] = $email;
            $result[$key]['marketName'] = $marketName;

        }

        return $result;
    }

    //MarketKeyCon 해당 데이터 조사
    public static function GetTargetMarketKey($data=null)
    {
        $marketCode=$data;
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $Sel = $apiDb->prepare("SELECT
        A.idx,
        A.statusIDX
        FROM $apiDbName.MarketKey AS A
        WHERE A.marketCode='$marketCode'
        ");
        $Sel->execute();
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketKeyCon 해당 마켓키 있는지 조사 , MarketCon 마켓 인서트 할때 api서버에 마켓키 있는지 조사
    public static function MarketkApiKeyChkByMarketKey($data=null)
    {
        $marketKey=$data;
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $Sel = $apiDb->prepare("SELECT
        A.idx
        FROM $apiDbName.MarketKey AS A
        WHERE A.marketKey='$marketKey'
        ");
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function MarketkApiKeyCountByMarketKey($data=null)
    {
        $marketCode=$data;
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $Sel = $apiDb->prepare("SELECT
        COUNT(idx) AS count
        FROM $apiDbName.MarketKey
        WHERE marketCode=:marketCode
        ");
        $Sel->bindValue(':marketCode', $marketCode);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //dayJob 사용가능한 api키 가져오기(이메일)
    public static function GetMarketkKey($data=null)
    {
        $marketCode=$data;
        $apiDb = static::GetApiDB();
        $apiDbName= self::EbuyApiDBName;
        $Sel = $apiDb->prepare("SELECT
        idx,
        marketKey
        FROM $apiDbName.MarketKey
        WHERE marketCode=:marketCode AND statusIDX=429101
        ORDER BY idx DESC LIMIT 1
        ");
        $Sel->bindValue(':marketCode', $marketCode);
        $Sel->execute();
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //dayJob 삭제예정인 마켓키 데이잡으로 삭제시키깅
    public static function GetMarketKeyDeletionTime($data=null)
    {
        $type=$data;
        $query = ' AND DATE(deletionTime) <= CURDATE()';
        if($type == 'twoDaysAgo'){
            $query = ' AND DATE(deletionTime) = DATE_SUB(CURDATE(), INTERVAL 2 DAY) AND DATE(deletionTime) < NOW()';
        }
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        $Sel = $db->query("SELECT
        idx,
        marketCode,
        marketKey,
        deletionTime
        FROM $dbName.MarketKey
        WHERE statusIDX = 429201 $query
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }



}