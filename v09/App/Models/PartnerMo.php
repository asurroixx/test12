<?php

namespace App\Models;

use PDO;

class PartnerMo extends \Core\Model
{

    //PartnerCon 데이터테이블
    public static function GetDatatableList($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        createTime
        FROM $dbName.Partner
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //PartnerCon 디테일
    public static function GetPartnerDetail($data=null)
    {
        $targetIDX=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        createTime
        FROM $dbName.Partner
        WHERE idx='$targetIDX'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //MarketCon 파트너 기본 리스트
    public static function GetPartnerListData($data=null)
    {
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code,
        name,
        createTime
        FROM $dbName.Partner
        ORDER BY name ASC
        ");
        $result=$Sel->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //PartnerCon 코드 받아오기
    public static function GetPartnerCode($data=null)
    {
        $code=$data;
        $db = static::GetDB();
        $dbName= self::MainDBName;
        $Sel = $db->query("SELECT
        idx,
        code
        FROM $dbName.Partner
        WHERE code='$code'
        ");
        $result=$Sel->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


}