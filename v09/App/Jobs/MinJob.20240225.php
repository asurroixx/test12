<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';



use PDO;
use \Core\View;
use \Core\Controller;
use \Core\DefinAll;
use \Core\GlobalsVariable;

use App\Models\SystemMo;
use App\Models\ExchangeRateMo;
use App\Models\MarketKeyMo;
use App\Models\EbuyBankLogMo;
use App\Models\ClientMileageDepositMo;
use App\Models\WithdrawalOldMo;
use App\Models\DepositOldMo;
use App\Models\StaffMo;

use App\Models\XxxJunmoxxXMo;//현금영수증때문에..
use App\Models\ClientPeerToPeerMo;//피투피중 구매자입금신청이 완료되었나~ 안되었나~ 찾는 친구가 들어있는 친구에요.




// use App\Models\OmcMenu_M;
/**
* Home controller
*
* PHP version 7.0
*/

class MinJob extends \Core\Controller
{
    public function __construct() {
        // $controller = new \Core\Controller();
        // $this::dDosUnblockAndInsBlacklist();
        // $this::dDosTxtCleaner();
        $this::loginSessionDelete();
        $this::ClientDepositAutoCancel();
        $this::ClientPinBlockUpdate();
        $this::WithdrawalOld();
        $this::DepositOld();
        $this::staffLoginDumpDelete();
        $this::appStaffDepositErrAlarm();
        $this::JobExchangeRate();
        $this::CashReceiptFirstWorker();
        $this::PtpDepositCheck();
    }



    //수동신청보류인 상태인 애들 5분 지나면 모두 크론취소
    public static function ClientDepositAutoCancel(){
        $getTwoDaysAgoList=ClientMileageDepositMo::ClientMileageDepositAutoCancelList();
        $db = static::getDB();
        $dbName= self::MainDBName;
        $preCreateTime=date("Y-m-d H:i:s");
        foreach ($getTwoDaysAgoList as $key) {

            $rn=$key['rn'];
            $targetIDX = $key['idx'];
            $statusIDX = $key['statusIDX'];
            $clientIDX = $key['clientIDX'];
            $createTime = $key['createTime'];
            $accountHolder = $key['accountHolder'];

            $logArr=[
                'createTime'=>$createTime,
                'accountHolder'=>$accountHolder,
            ];

            $getEbuyBankLogList=EbuyBankLogMo::GetTargetMinJobEbuyBankLog($logArr);
            if(!isset($getEbuyBankLogList['idx'])&&empty($getEbuyBankLogList['idx'])){
                if($statusIDX=='203201'||$statusIDX=='203202'||$statusIDX=='203203'){
                    $errMsg='이미 성공한 상태입니다.';
                    $errOn=$this::errExport($errMsg);
                }
                $updateStatusIDX = 203215;

                $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                    statusIDX=:updateStatusIDX,
                    completeTime=:createTime
                    WHERE idx=:targetIDX
                ");
                $stat1->bindValue(':updateStatusIDX', $updateStatusIDX);
                $stat1->bindValue(':createTime', $preCreateTime);
                $stat1->bindValue(':targetIDX', $targetIDX);
                $stat1->execute();

                $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                    (statusIDX,targetIDX,clientIDX,createTime)
                    VALUES
                    (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                ");
                $stat2->bindValue(':updateStatusIDX', $updateStatusIDX);
                $stat2->bindValue(':targetIDX', $targetIDX);
                $stat2->bindValue(':clientIDX', $clientIDX);
                $stat2->bindValue(':createTime', $preCreateTime);
                $stat2->execute();

                //유저에게 알람을 쏴주자
                $alarmArr=[
                    'clientIDX'=>$clientIDX,
                    'statusIDX'=>$updateStatusIDX,
                    'targetIDX'=>$targetIDX,
                ];

                $AppMainIoUri= self::AppMainIoUri;
                $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                static::sendCurl($alarmArr,$AppMainIoAddr);
                // static::appPushAlarm($alarmArr);
                //스태프 입금신청 테이블 소켓
                static::socketTable($targetIDX);

                $scrParam=[
                    'rn'=>$rn,
                    'statusIDX'=>$updateStatusIDX,
                ];
                $AppMainIoAddr =$AppMainIoUri.'/rechargeResultAlarm';
                static::sendCurl($scrParam,$AppMainIoAddr);

                $dataPack2=ClientMileageDepositMo::tmpTargetData($targetIDX);
                if(isset($dataPack2['idx'])&&!empty($dataPack2['idx'])){
                    $migrationIDX=$dataPack2['migrationIDX'];
                    $amount=$dataPack2['amount'];
                    if($migrationIDX != 0){
                        //구바이에도 보내자
                        $field = 'https://partners-api.ebuycompany.com/MagApi/depositCancel';
                        $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount;
                        // $nextStepUri='https://partners-api.ebuycompany.com/MagApi/'.$fieldName;

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_REFERER, "$field");
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "$field",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "$postFields",
                            CURLOPT_HTTPHEADER => array(
                                "cache-control: no-cache",
                                "content-type: application/x-www-form-urlencoded"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                    }
                }
            }


        }

    }

    //5분 지난 Y 데이터들 N으로 업데이트
    public static function ClientPinBlockUpdate(){
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientPinBlock SET
            inBlock=:inBlock
            WHERE createTime < DATE_SUB(NOW(), INTERVAL 6 MINUTE)
        ");
        $stat1->bindValue(':inBlock', 'N');
        $stat1->execute();
    }

    //출금대행 구바이랑 뉴바이 비교
    public static function WithdrawalOld(){
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        //구바이 서버
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $date = date('Y-m-d H:i:s');
        $timestamp = strtotime($date . ' +9 hours');//한국시간으로
        $timestamp = $timestamp - (2 * 60); // 2분 빼기
        $kst = date('Y-m-d H:i:s', $timestamp);


        // 일단 구바이 출금대행 목록을 가져오자
        $query = $oldDb->prepare("SELECT
            APR_ID AS idx,
            APR_EB_USER_NAME AS name,
            APR_AGENT_M_CODE AS marketCode,
            APR_EB_USER_CHAM_ID AS refereceId,
            APR_SUC_QTY_TOTAL AS amount,
            partnerOrderId AS invoiceId,
            APR_SUC_REG AS createTime
            FROM $oldDbName.AA_AGNET_P_REFUND
            WHERE APR_STAT = 2 AND (APR_SUC_REG BETWEEN '2024-01-22 10:00:00' AND '$kst')
        ");
        $query->execute();
        $resultTable=$query->fetchAll(PDO::FETCH_ASSOC);

        // 구바이 데이터를 돌려서 하나씩 꺼내옴
        foreach ($resultTable as $key) {
            $idx = $key['idx'];
            $name = $key['name'];
            $marketCode = $key['marketCode'];
            $refereceId = $key['refereceId'];
            $amount = $key['amount'];
            $invoiceId = $key['invoiceId'];
            $createTime = $key['createTime'];

            $targetData=WithdrawalOldMo::GetTargetData($idx);
            //받아온 oldIDX가 있으면 인서트하고 없으면 하지마라
            if(!isset($targetData['oldIDX'])&&empty($targetData['oldIDX'])){
                //차라리 여기서 처음부터 인서트 시키자
                $stat2 = $db->prepare("INSERT INTO $dbName.WithdrawalOld
                    (code, name, refereceId, amount, invoiceId, createTime, status, oldIDX)
                    VALUES
                    (:code, :name, :refereceId, :amount, :invoiceId, :createTime, :status, :oldIDX)
                ");
                $stat2->bindValue(':code', $marketCode);
                $stat2->bindValue(':name', $name);
                $stat2->bindValue(':refereceId', $refereceId);
                $stat2->bindValue(':amount', $amount);
                $stat2->bindValue(':invoiceId', $invoiceId);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':status', 1);
                $stat2->bindValue(':oldIDX', $idx);
                $stat2->execute();
            }

            //가져온값들로 조사해보자
            $paramArr=[
                'marketCode'=>$marketCode,
                'refereceId'=>$refereceId,
                'amount'=>$amount,
                'invoiceId'=>$invoiceId,
            ];

            // 4개의 파람값 중복 데이터가 있는지 조사 (뉴바이)
            $newData=WithdrawalOldMo::GetIssetNewDatatableList($paramArr);
            if(isset($newData['idx'])&&!empty($newData['idx'])){
                $newDataMarketCode = $newData['marketCode'];
                $newDataMarketUserEmail = $newData['marketUserEmail'];
                $newDataAmount = $newData['amount'];
                $newDataInvoiceID = $newData['invoiceID'];

                if ($newDataMarketCode == $marketCode &&
                    $newDataMarketUserEmail == $refereceId &&
                    $newDataAmount == $amount &&
                    $newDataInvoiceID == $invoiceId) {

                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET
                        status=3
                        WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->execute();
                }

                if($newDataMarketCode != $marketCode){
                    $memo = 'Job : 마켓코드가 맞지 않습니다. 뉴바이코드 : '.$newDataMarketCode .' / 구바이코드 : '.$marketCode;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataMarketUserEmail != $refereceId){
                    $memo = 'Job : 참조이메일이 맞지 않습니다. 뉴바이참ID : '.$newDataMarketUserEmail .' / 구바이참ID : '.$refereceId;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataAmount != $amount){
                    $memo = 'Job : 금액이 맞지 않습니다. 뉴바이금액 : '.$newDataAmount .' / 구바이금액 : '.$amount;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataInvoiceID != $invoiceId){
                    $memo = 'Job : InvoiceID가 맞지 않습니다. 뉴바이인ID : '.$newDataInvoiceID .' / 구바이인ID : '.$invoiceId;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }
            }
        }


    }

    //지불대행 구바이랑 뉴바이 비교
    public static function DepositOld(){
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        //구바이 서버
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $date = date('Y-m-d H:i:s');
        $timestamp = strtotime($date . ' +9 hours');
        $timestamp = $timestamp - (2 * 60); // 2분 빼기
        $kst = date('Y-m-d H:i:s', $timestamp);


        // 일단 구바이 출금대행 목록을 가져오자
        $query = $oldDb->prepare("SELECT
            APH_ID AS idx,
            APH_NAME AS name,
            APH_AGENT_M_CODE AS marketCode,
            APH_AGENT_USER_ID AS refereceId,
            APH_APPLY_QTY AS amount,
            partnerOrderId AS invoiceId,
            APH_SUC_P_ADM_REG AS createTime
            FROM $oldDbName.AA_AGENT_PARTNER_HIS
            WHERE APH_STAT = 2 AND (APH_SUC_P_ADM_REG BETWEEN '2024-01-22 10:00:00' AND '$kst')
        ");
        $query->execute();
        $resultTable=$query->fetchAll(PDO::FETCH_ASSOC);

        // 구바이 데이터를 돌려서 하나씩 꺼내옴
        foreach ($resultTable as $key) {
            $idx = $key['idx'];
            $name = $key['name'];
            $marketCode = $key['marketCode'];
            $refereceId = $key['refereceId'];
            $amount = $key['amount'];
            $invoiceId = $key['invoiceId'];
            $createTime = $key['createTime'];

            $oldArr=[
                'idx'=>$idx,
                'createTime'=>$createTime,
            ];

            $targetData=DepositOldMo::GetTargetData($oldArr);
            //받아온 oldIDX가 있으면 인서트하고 없으면 하지마라
            if(!isset($targetData['oldIDX'])&&empty($targetData['oldIDX'])){
                //차라리 여기서 처음부터 인서트 시키자
                $stat2 = $db->prepare("INSERT INTO $dbName.DepositOld
                    (code, name, refereceId, amount, invoiceId, createTime, status, oldIDX)
                    VALUES
                    (:code, :name, :refereceId, :amount, :invoiceId, :createTime, :status, :oldIDX)
                ");
                $stat2->bindValue(':code', $marketCode);
                $stat2->bindValue(':name', $name);
                $stat2->bindValue(':refereceId', $refereceId);
                $stat2->bindValue(':amount', $amount);
                $stat2->bindValue(':invoiceId', $invoiceId);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':status', 1);
                $stat2->bindValue(':oldIDX', $idx);
                $stat2->execute();
            }

            //가져온값들로 조사해보자
            $paramArr=[
                'marketCode'=>$marketCode,
                'refereceId'=>$refereceId,
                'amount'=>$amount,
                'invoiceId'=>$invoiceId,
            ];

            // 4개의 파람값 중복 데이터가 있는지 조사 (뉴바이)
            $newData=DepositOldMo::GetIssetNewDatatableList($paramArr);

            if(isset($newData['idx'])&&!empty($newData['idx'])){
                $newDataMarketCode = $newData['marketCode'];
                $newDataMarketUserEmail = $newData['marketUserEmail'];
                $newDataAmount = $newData['amount'];
                $newDataInvoiceID = $newData['invoiceID'];

                if ($newDataMarketCode == $marketCode &&
                    $newDataMarketUserEmail == $refereceId &&
                    $newDataAmount == $amount &&
                    $newDataInvoiceID == $invoiceId) {

                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET
                        status=3
                        WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->execute();
                }

                if($newDataMarketCode != $marketCode){
                    $memo = 'Job : 마켓코드가 맞지 않습니다. 뉴바이코드 : '.$newDataMarketCode .' / 구바이코드 : '.$marketCode;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataMarketUserEmail != $refereceId){
                    $memo = 'Job : 참조이메일이 맞지 않습니다. 뉴바이참ID : '.$newDataMarketUserEmail .' / 구바이참ID : '.$refereceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataAmount != $amount){
                    $memo = 'Job : 금액이 맞지 않습니다. 뉴바이금액 : '.$newDataAmount .' / 구바이금액 : '.$amount;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataInvoiceID != $invoiceId){
                    $memo = 'Job : InvoiceID가 맞지 않습니다. 뉴바이인ID : '.$newDataInvoiceID .' / 구바이인ID : '.$invoiceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }
            }
        }

    }


    public static function dDosUnblockAndInsBlacklist() {
        $blockFile=DefinAll::dDosBlockFile;//블랙리스트 등록파일
        if (!file_exists($blockFile)){
            return false;
        }

        $nowReg=date("Y-m-d H:i:s");
        $time1 = strtotime($nowReg);

        $handle = fopen($blockFile, 'r'); // 파일을 추가 모드로 엽니다.
        if (!$handle) {  return false;  } //파일열기 실패
        $filesize = filesize($blockFile); // 파일의 크기 가져오기
        if($filesize>0){
            $blockFileCon = fread($handle, $filesize); // 파일 내용을 읽어옵니다.
            $pattern = '/,(\s*)$/'; // 정규식 패턴
            $blockFileCon = preg_replace($pattern, '$1', $blockFileCon); // 콤마 제거
            $blockFileCon = explode(',',$blockFileCon);
            $content = file_get_contents($blockFile);
            $newContent = $content;
            foreach ($blockFileCon as $key => $value) {
                // $txtIp = explode('||',$value)[0]; // 텍스트파일의 ip
                $txtReg = explode('||',$value)[1]; //텍스트파일의 reg

                $time2 = strtotime($txtReg);
                $diffInSeconds = $time1 - $time2;
                $diffInMinutes = floor($diffInSeconds / 60);
                if($diffInMinutes>DefinAll::dDosReleaseTime){//n분이 지났으면 풀어주자.
                    // 파일 읽기
                    $newContent = str_replace($value.',', '', $newContent);
                    // $db = static::getDB();
                    // $QrStringIst = $db->query("INSERT INTO sendipay.Ban SET
                    //     ipAddress='$txtIp',
                    //     createTime='$nowReg',
                    //     `from`='staff'
                    // ");
                }
            }
            file_put_contents($blockFile, $newContent);
            fclose($handle); // 파일을 닫습니다.
        }
    }



    public static function dDosTxtCleaner() {
        $dDosFile=DefinAll::dDosIpRegFile;
        if (!file_exists($dDosFile)){
            return false;
        }

        $nowReg=date("Y-m-d H:i:s");
        $time1 = strtotime($nowReg);
        $handle = fopen($dDosFile, 'r'); // 파일을 추가 모드로 엽니다.
        if (!$handle) {  return false;  } //파일열기 실패
        $filesize = filesize($dDosFile); // 파일의 크기 가져오기
        if($filesize>0){
            $dDosFileCon = fread($handle, $filesize); // 파일 내용을 읽어옵니다.
            $pattern = '/,(\s*)$/'; // 정규식 패턴
            $dDosFileCon = preg_replace($pattern, '$1', $dDosFileCon); // 콤마 제거
            $dDosFileCon = explode(',',$dDosFileCon);
            $content = file_get_contents($dDosFile);
            $newContent = $content;
            foreach ($dDosFileCon as $key => $value) {
                $txtIp = explode('||',$value)[0]; // 텍스트파일의 ip
                $txtReg = explode('||',$value)[1]; //텍스트파일의 reg
                $time2 = strtotime($txtReg);
                $diffInSeconds = $time1 - $time2; //현재시간이 무조건 길지는 않을것(24:00:00 와 01:00:01 같은경우)
                $diffInMinutes = floor($diffInSeconds / 60);
                if($diffInMinutes>DefinAll::dDosFileCleanTime){//7분이 지나면 삭제
                    // 파일 읽기
                    $newContent = str_replace($value.',', '', $newContent);
                }
            }
            file_put_contents($dDosFile, $newContent);
            fclose($handle); // 파일을 닫습니다.
        }
    }

    public static function loginSessionDelete() {
        $loginTime=DefinAll::LoginTime;
        $loginKey=DefinAll::LoginKey;

        $sessionDir = '/var/lib/php/session';
        // 디렉토리 내의 파일 목록을 가져옵니다.

        $files = scandir($sessionDir);
        if ($files === false) {
            return false;
        }

        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                $filePath = $sessionDir . '/' . $file;
                $createTime = filectime($filePath);
                $currentDateTime = new \DateTime();
                $sessionDateTime = new \DateTime();
                $sessionDateTime->setTimestamp($createTime);
                $interval = $currentDateTime->diff($sessionDateTime);

                $years = $interval->y;
                $months = $interval->m;
                $days = $interval->d;
                $hours = $interval->h;
                $minutes = $interval->i;
                $seconds = $interval->s;
                // 총 분으로 계산
                $gapTimeMin = ($years * 525600) + ($months * 43800) + ($days * 1440) + ($hours * 60) + $minutes;
                if($gapTimeMin>=$loginTime){
                    unlink($filePath);
                }
            }
        }
    }



    public static function staffLoginDumpDelete() {
        //로그인시 발급되는 코드 지우기
        $db     = static::getDB();
        $dbName = self::MainDBName;
        $stat2 = $db->prepare("DELETE FROM $dbName.StaffLoginDump
            WHERE createTime < DATE_SUB(NOW(), INTERVAL 6 MINUTE)");
        $stat2->execute();
    }

    //1분에 한번씩 환율쏘기 (스태프에 쏨)
    public static function JobExchangeRate(){
        $standKRWresult=ExchangeRateMo::ExchangeRate();
        if(!isset($standKRWresult['standKRW'])||empty($standKRWresult['standKRW'])){
            $errMsg='standKRW 정보가 없습니다.';
            exit();
        }
        $standKRWresultone=$standKRWresult['standKRW'];
        $DomainUri=self::DomainUri;
        $staffUri = $DomainUri.':16001/JobExchangeRate';
        $dataPack=['data'=>$standKRWresultone];
        $result=self::curlSend($dataPack,$staffUri);
    }



    //앱 스태프 입금이상 알람용
    public static function appStaffDepositErrAlarm($data=null)
    {
        $alarmData=ClientMileageDepositMo::ClientMileageDepositStaffAlarmList();

        $db = static::getDB();
        $dbName= self::MainDBName;

        foreach ($alarmData as $key) {
            $targetIDX = $key['idx'];
            $clientName = $key['name'];

            $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                appStaffAlarm=:appStaffAlarm
                WHERE idx=:idx
            ");
            $stat1->bindValue(':appStaffAlarm', 'Y');
            $stat1->bindValue(':idx', $targetIDX);
            $stat1->execute();

            $staffData=StaffMo::GetAppStaffAlarmList();
            foreach ($staffData as $key2) {
                $staffClientIDX = $key2['clientIDX'];
                $staffStatusIDX = $key2['statusIDX'];

                if($staffStatusIDX == 302101){
                     //유저에게 알람을 쏴주자
                    $alarmArr=[
                        'clientIDX'=>$staffClientIDX,
                        'statusIDX'=>102201,
                        'targetIDX'=>$targetIDX,
                        'param'=>['clientName'=>$clientName]
                    ];
                    $AppMainIoUri= self::AppMainIoUri;
                    $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                    static::sendCurl($alarmArr,$AppMainIoAddr);
                }
            }
        }
    }


    //20240215 ClientAllAmount 자동등업을 위한 클라이언트 토탈 어마운트 USD 추가
    public static function CashReceiptFirstWorker($data=null)
    {
        $startDate=date('Y-m-d H:i:s', strtotime('-3 minutes'));
        $endDate=date('Y-m-d H:i:s');

        $dataArr=array(
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        );
        $dataPack=XxxJunmoxxXMo::GetClientMileageIsApiDepositForCashReceiptJob($dataArr);
        foreach ($dataPack as $item) {
            $insStatusIDX=811101;//기본 발행하지않음

            $ClientMileageIDX=$item['idx'];
            $clientIDX=$item['clientIDX'];

            $clientName=$item['clientName'];
            $clientTel=$item['clientTel'];
            $clientEmail=$item['clientEmail'];

            $ReceiptTypeNo=$item['ReceiptTypeNo'];
            $ClientCashReceiptSettingIDX=$item['ClientCashReceiptSettingIDX'];
            $ClientCashReceiptSettingStatusIDX=$item['ClientCashReceiptSettingStatusIDX'];
            if($ClientCashReceiptSettingStatusIDX!='229101'&&$ClientCashReceiptSettingStatusIDX!='229201'){
                $ClientCashReceiptSettingStatusIDX='229101';
            }
            $ClientCashReceiptSettingCreateTime=$item['ClientCashReceiptSettingCreateTime'];
            if(strlen($ReceiptTypeNo)!=11){//없을리는 없겠지만은..
                $ReceiptTypeNo=$clientTel;
            }

            $IsStatusIDX=$item['statusIDX'];
            if($IsStatusIDX=='906101'){//지불대행
                $MarketDepositLogIDX=$item['targetIDX'];
                $TRAN_REQ_ID='MD'.$MarketDepositLogIDX.'T'.date('ymdHis');

                $ReceiptAmt=0;
                $db     = static::GetApiDB();
                $dbName = self::EbuyApiDBName;
                $query = $db->prepare("SELECT
                    clientFee
                    ,amount
                FROM $dbName.MarketDepositLog
                WHERE idx = '$MarketDepositLogIDX'
                AND NOT EXISTS (
                    SELECT 1
                    FROM $dbName.ClientCashReceipt
                    WHERE targetIDX = '$MarketDepositLogIDX'
                    AND targetStatusIDX= '906101'
                )");
                $query->execute();
                $sel=$query->fetch(PDO::FETCH_ASSOC);
                if(isset($sel['clientFee'])){
                    $MarketDepositLogAmount=$sel['amount'];
                    $ReceiptAmt=$sel['clientFee'];
                    $insEX='알수없음';
                    if( $ClientCashReceiptSettingStatusIDX=='229101' || ($ClientCashReceiptSettingStatusIDX=='229201' && $ReceiptAmt>99999) ){
                        if($ClientCashReceiptSettingStatusIDX=='229101'){//229101 받겠다
                            if($ReceiptAmt<100){//99원 이하라고? 받지말어
                                $insEX='소액이라발행안하겠음 '.$ReceiptAmt.'원';//기본수수료가 3000원인데 이럴리가 없겠지..
                            }else{
                                $insStatusIDX=811201;
                                $insEX='발행대기';
                            }
                        }else{//229201 안받겠다고 했는데 십만원보다 크다고?
                            $insStatusIDX=811201;
                            $insEX='발행거부했지만 강제발행대기. 발행금액:'.$ReceiptAmt.'원 거부일시:'.$ClientCashReceiptSettingCreateTime;
                        }
                    }else{
                        if($ClientCashReceiptSettingStatusIDX=='229201'){//229101받겠다
                            $insEX='발행거부 거부일시:'.$ClientCashReceiptSettingCreateTime;
                        }else{// 없다고..?
                            $insEX='ClientCashReceiptSetting이 없는것 같다.';// 왜 없는데..?
                        }
                    }
                    // 20240222 자꾸 회원가입해..
                    if(
                        ($clientName=='박세준'&&$clientTel=='01049115371')
                        || ($clientName=='김준모'&&$clientTel=='01025110435')
                        || ($clientName=='이동하'&&$clientTel=='01030604234')
                        || ($clientName=='김성환'&&$clientTel=='01086801234')
                        || ($clientName=='김동주'&&$clientTel=='01099720894')
                        || ($clientName=='김장배'&&$clientTel=='01083698856')
                        || ($clientName=='박제이'&&$clientTel=='01036955956')
                        || ($clientName=='제상영'&&$clientTel=='01099254222')
                        || ($clientName=='이선영'&&$clientTel=='01083228541')
                        || ($clientName=='나성은'&&$clientTel=='01088208005')
                        || ($clientName=='박희진'&&$clientTel=='01032039087')
                        || ($clientName=='장윤희'&&$clientTel=='01024504830')
                        || ($clientName=='김현지'&&$clientTel=='01082241025')
                        || ($clientName=='박수빈'&&$clientTel=='01054983757')
                    ){
                        $insStatusIDX=811101;//기본 발행하지않음
                        $insEX='나 민잡인데 발행하지 않을꺼야.';
                    }

                    $nowTime=date("Y-m-d H:i:s");
                    $insert =$db->prepare("INSERT INTO $dbName.ClientCashReceipt
                        (
                            targetStatusIDX,targetIDX,statusIDX,ReceiptAmt,ClientCashReceiptSettingIDX,ReceiptTypeNo,clientIDX,clientTel,clientName,clientEmail,TRAN_REQ_ID,createTime,resultTime,result_msg
                        )
                        VALUES
                        (
                            :targetStatusIDX,:targetIDX,:statusIDX,:ReceiptAmt,:ClientCashReceiptSettingIDX,:ReceiptTypeNo,:clientIDX,:clientTel,:clientName,:clientEmail,:TRAN_REQ_ID,:createTime,:resultTime,:result_msg
                        )
                    ");
                    $insert->bindValue(':targetStatusIDX', $IsStatusIDX);
                    $insert->bindValue(':targetIDX', $MarketDepositLogIDX);
                    $insert->bindValue(':statusIDX', $insStatusIDX);
                    $insert->bindValue(':ReceiptAmt', $ReceiptAmt);
                    $insert->bindValue(':ClientCashReceiptSettingIDX', $ClientCashReceiptSettingIDX);
                    $insert->bindValue(':ReceiptTypeNo', $ReceiptTypeNo);
                    $insert->bindValue(':clientIDX', $clientIDX);
                    $insert->bindValue(':clientTel', $clientTel);
                    $insert->bindValue(':clientName', $clientName);
                    $insert->bindValue(':clientEmail', $clientEmail);
                    $insert->bindValue(':TRAN_REQ_ID', $TRAN_REQ_ID);
                    $insert->bindValue(':createTime', $nowTime);
                    $insert->bindValue(':resultTime', $nowTime);
                    $insert->bindValue(':result_msg', $insEX);
                    $insert->execute();
                    $ClientCashReceiptIDX=$db->lastInsertId();
                    if($clientIDX*1>1){
                        $db = static::getDB();
                        $dbName= self::MainDBName;
                        $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                            (statusIDX,targetIDX,clientIDX,createTime)
                            VALUES
                            (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                        ");
                        $stat2->bindValue(':updateStatusIDX', $insStatusIDX);
                        $stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
                        $stat2->bindValue(':clientIDX', $clientIDX);
                        $stat2->bindValue(':createTime', $nowTime);
                        $stat2->execute();
                        $ClientLogIDX=$db->lastInsertId();
                        $stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
                            (logIDX,ex)
                            VALUES
                            (:logIDX,:ex)
                        ");
                        $stat3->bindValue(':logIDX', $ClientLogIDX);
                        $stat3->bindValue(':ex', $insEX);
                        $stat3->execute();

                        //20240215 ClientAllAmount 자동등업을 위한 클라이언트 토탈 어마운트 USD 추가
                        $stat4=$db->prepare("INSERT INTO $dbName.ClientAllAmount
                            (clientIDX,amount,statusIDX,targetIDX,createTime)
                            VALUES
                            (:clientIDX,:amount,:statusIDX,:targetIDX,:createTime)
                        ");
                        $stat4->bindValue(':clientIDX', $clientIDX);
                        $stat4->bindValue(':amount', $MarketDepositLogAmount);
                        $stat4->bindValue(':statusIDX', $IsStatusIDX);
                        $stat4->bindValue(':targetIDX', $MarketDepositLogIDX);
                        $stat4->bindValue(':createTime', $nowTime);
                        $stat4->execute();

                        //20240215 ClientAllAmount 등업도 여기서 해야함
                        //20240223 등급업할까말까 분리함 ClientGradeUpCheck
                        self::ClientGradeUpCheck($clientIDX);
                    }
                }
            }elseif($IsStatusIDX=='251401'){//20240223피트피
                $ClientPeerToPeerIDX=$item['targetIDX']; //ClientPeerToPeer
                $TRAN_REQ_ID='PTP'.$ClientPeerToPeerIDX.'T'.date('ymdHis');
                //하이~~
            }
        }
    }

    public static function ClientGradeUpCheck($clientIDX=null)
    {
        if(isset($clientIDX)&&!empty($clientIDX)&&$clientIDX*1>1){
            $nowTime=date("Y-m-d H:i:s");

            //이미 최고 등급인 플래티넘이라면 제외
            $db = static::getDB();
            $dbName= self::MainDBName;
            $que11 = $db->prepare("SELECT
                    B.gradeIDX,
                    B.migrationIDX,
                    B.statusIDX,
                    ROUND(SUM(A.amount), 2) AS totAmount,
                    IFNULL(D.upto, 0) AS nextUpto
                FROM $dbName.ClientAllAmount AS A
                LEFT JOIN $dbName.Client AS B ON B.idx = A.clientIDX
                LEFT JOIN $dbName.ClientGrade AS D ON D.idx = B.gradeIDX + 1
                WHERE B.gradeIDX != 3
                AND A.clientIDX = :clientIDX
                GROUP BY A.clientIDX
                -- HAVING ROUND(SUM(A.amount), 2) >= IFNULL(D.upto, 0)
                HAVING ROUND(SUM(A.amount), 2) >= IFNULL(nextUpto, 0)
            ");
            $que11->bindValue(':clientIDX', $clientIDX);
            $que11->execute();
            $sel11=$que11->fetch(PDO::FETCH_ASSOC);
            if(isset($sel11['gradeIDX'])){
                $totAmount=$sel11['totAmount'];
                $gradeIDX=$sel11['gradeIDX'];
                $nextGradeIDX=$gradeIDX+1;
                $ex='(잡) 등급이 '.$gradeIDX.'에서 '.$nextGradeIDX.'로 변경됐습니다.(totAmount:'.$totAmount.')';
                $que12=$db->prepare("INSERT INTO $dbName.ClientLog
                    (statusIDX,targetIDX,clientIDX,createTime)
                    VALUES
                    (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                ");
                $que12->bindValue(':updateStatusIDX', 225101);
                $que12->bindValue(':targetIDX', $clientIDX);
                $que12->bindValue(':clientIDX', $clientIDX);
                $que12->bindValue(':createTime', $nowTime);
                $que12->execute();
                $ClientLogIDX=$db->lastInsertId();
                $que13=$db->prepare("INSERT INTO $dbName.ClientLogEx
                    (logIDX,ex)
                    VALUES
                    (:logIDX,:ex)
                ");
                $que13->bindValue(':logIDX', $ClientLogIDX);
                $que13->bindValue(':ex', $ex);
                $que13->execute();

                $que14=$db->prepare("UPDATE $dbName.Client SET
                gradeIDX=:gradeIDX
                WHERE idx=:targetIDX
                ");
                $que14->bindValue(':gradeIDX', $nextGradeIDX);
                $que14->bindValue(':targetIDX', $clientIDX);
                $que14->execute();

                //구바이시작
                if(
                    isset($sel11['migrationIDX'])
                    && !empty($sel11['migrationIDX'])
                    && $sel11['migrationIDX']*1>1
                    && $sel11['statusIDX']!=226101
                ){
                    $migrationIDX=$sel11['migrationIDX'];
                    if($nextGradeIDX==1){
                        $GuLevel=2;
                        $GuLevelName='SILVER VIP';
                    }elseif($nextGradeIDX==2){
                        $GuLevel=3;
                        $GuLevelName='GOLD VIP';
                    }elseif($nextGradeIDX==3){
                        $GuLevel=6;
                        $GuLevelName='PLATINUM VIP';
                    }
                    $oldDb = static::GetOldDB();
                    $oldDbName= self::EbuyOldDBName;
                    $stat11=$oldDb->prepare("UPDATE $oldDbName.eb_member SET
                        M_LEVEL=:M_LEVEL
                        ,M_LEVELNAME=:M_LEVELNAME
                        WHERE member_srl=:migrationIDX
                    ");
                    $stat11->bindValue(':M_LEVEL', $GuLevel);
                    $stat11->bindValue(':M_LEVELNAME', $GuLevelName);
                    $stat11->bindValue(':migrationIDX', $migrationIDX);
                    $stat11->execute();

                    $timestamp = strtotime($nowTime . ' +9 hours');//한국시간으로
                    $timestamp = $timestamp - (2 * 60); // 2분 빼기
                    $Gureg = date('Y-m-d H:i:s', $timestamp);
                    $Gudata='srl:'.$migrationIDX.' 뉴바이 잡이 자동등업을 시전하였다! '.$ex;
                    $Guaction='gradeUpByNewbuyMinJob';
                    $Gustep='완룡';
                    $stat21=$oldDb->prepare("INSERT INTO $oldDbName.x_newbuy_all_log
                        (data,reg,action,step)
                        VALUES
                        (:Gudata,:Gureg,:Guaction,:Gustep)
                    ");
                    $stat21->bindValue(':Gudata', $Gudata);
                    $stat21->bindValue(':Gureg', $Gureg);
                    $stat21->bindValue(':Guaction', $Guaction);
                    $stat21->bindValue(':Gustep', $Gustep);
                    $stat21->execute();
                }
            }
        }
    }

    //20240222 피투피 구매자 입금신청이 완료되었나~ 안되었나~
    public static function PtpDepositCheck($data=null)
    {
        $PtpNextCheckJob=ClientPeerToPeerMo::PtpNextCheckJob();
        if(isset($PtpNextCheckJob['idx'])&&!empty($PtpNextCheckJob['idx'])){
            $ClientPeerToPeerIDX=$PtpNextCheckJob['idx'];
            /*
            ClientPeerToPeer 테이블에서 251101 이면서 AAA 를 가진 ClientMileageDeposit 테이블에 상태가 완료처리되었다!(203201, 2, 3)
            ClientMileage 테이블 251201 로 인서트 하면서 구매자 마일리지 차감(구매자 예치금)
            ClientLog 테이블 인서트
            ClientPeerToPeer 테이블 251201 업데이트
            ClientLog 테이블 인서트
            */

            $SellerClientIDX=$PtpNextCheckJob['SellerClientIDX'];
            $BuyerClientIDX=$PtpNextCheckJob['BuyerClientIDX'];
            $BuyerFee=$PtpNextCheckJob['BuyerFee'];
            $rate=$PtpNextCheckJob['rate'];
            $amount=$PtpNextCheckJob['amount'];
            $totPrice=bcmul($rate, $amount,0);
            $BuyerPrice=bcadd($totPrice, $BuyerFee, 0);

            $changeStatus=251201;
            $nowTime=date("Y-m-d H:i:s");
            $db = static::getDB();
            $dbName= self::MainDBName;
            $stat1=$db->prepare("INSERT INTO $dbName.ClientMileage
                (clientIDX,statusIDX,targetIDX,amount,createTime)
                VALUES
                (:clientIDX,:statusIDX,:targetIDX,:amount,:createTime)
            ");
            $stat1->bindValue(':clientIDX', $BuyerClientIDX);
            $stat1->bindValue(':statusIDX', $changeStatus);
            $stat1->bindValue(':targetIDX', $ClientPeerToPeerIDX);
            $stat1->bindValue(':amount', $BuyerPrice);
            $stat1->bindValue(':createTime', $nowTime);
            $stat1->execute();
            $insertIDX = $db->lastInsertId();

            // $logIDX=static::ClientLogInsert($changeStatus,$insertIDX,$BuyerClientIDX);
            $ex='P2P거래 구매자 에스크로 ('.$BuyerPrice.'원)';
            // static::ClientLogExInsert($logIDX,0,0,$ex);
            $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,createTime)
                VALUES
                (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
            ");
            $stat2->bindValue(':updateStatusIDX', $changeStatus);
            $stat2->bindValue(':targetIDX', $insertIDX);
            $stat2->bindValue(':clientIDX', $BuyerClientIDX);
            $stat2->bindValue(':createTime', $nowTime);
            $stat2->execute();
            $ClientLogIDX=$db->lastInsertId();
            $stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
                (logIDX,ex)
                VALUES
                (:logIDX,:ex)
            ");
            $stat3->bindValue(':logIDX', $ClientLogIDX);
            $stat3->bindValue(':ex', $ex);
            $stat3->execute();
            //구매자 말리지 차감 끝

            $update =$db->prepare("UPDATE $dbName.ClientPeerToPeer SET
                statusIDX = :changeStatus
                WHERE idx = :targetIDX
            ");
            $update->bindValue(':changeStatus', $changeStatus);
            $update->bindValue(':targetIDX', $ClientPeerToPeerIDX);
            $update->execute();

            // $logIDX=static::ClientLogInsert($changeStatus,$ClientPeerToPeerIDX,$SellerClientIDX);
            $ex='P2P거래 2단계 판매자';
            // static::ClientLogExInsert($logIDX,0,0,$ex);
            $stat3=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,createTime)
                VALUES
                (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
            ");
            $stat3->bindValue(':updateStatusIDX', $changeStatus);
            $stat3->bindValue(':targetIDX', $ClientPeerToPeerIDX);
            $stat3->bindValue(':clientIDX', $SellerClientIDX);
            $stat3->bindValue(':createTime', $nowTime);
            $stat3->execute();
            $ClientLogIDX=$db->lastInsertId();
            $stat4=$db->prepare("INSERT INTO $dbName.ClientLogEx
                (logIDX,ex)
                VALUES
                (:logIDX,:ex)
            ");
            $stat4->bindValue(':logIDX', $ClientLogIDX);
            $stat4->bindValue(':ex', $ex);
            $stat4->execute();

            // $logIDX=static::ClientLogInsert($changeStatus,$ClientPeerToPeerIDX,$BuyerClientIDX);
            $ex='P2P거래 2단계 구매자(마일리지 차감 완료)';
            // static::ClientLogExInsert($logIDX,0,0,$ex);
            $stat5=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,createTime)
                VALUES
                (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
            ");
            $stat5->bindValue(':updateStatusIDX', $changeStatus);
            $stat5->bindValue(':targetIDX', $ClientPeerToPeerIDX);
            $stat5->bindValue(':clientIDX', $BuyerClientIDX);
            $stat5->bindValue(':createTime', $nowTime);
            $stat5->execute();
            $ClientLogIDX=$db->lastInsertId();
            $stat6=$db->prepare("INSERT INTO $dbName.ClientLogEx
                (logIDX,ex)
                VALUES
                (:logIDX,:ex)
            ");
            $stat6->bindValue(':logIDX', $ClientLogIDX);
            $stat6->bindValue(':ex', $ex);
            $stat6->execute();
        }
    }

}



$dd=new MinJob();