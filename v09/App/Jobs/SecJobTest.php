<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';
require_once('/oplas/v09/Modules/Php/vendor/autoload.php');


use PDO;
use \Core\View;
use \Core\Controller;
use \Core\DefinAll;
use \Core\GlobalsVariable;
use App\Models\JobMo;
use App\Models\SystemMo;
use App\Models\MarketKeyMo;
use App\Models\EbuyBankLogMo;
use App\Models\ClientMileageDepositMo;




// use App\Models\OmcMenu_M;
/**
* Home controller
*
* PHP version 7.0
*/

class SecJobTest extends \Core\Controller
{
    public function __construct() {
        // $controller = new \Core\Controller();
        // $this::dDosUnblockAndInsBlacklist();
        // $this::dDosTxtCleaner();
        

        // $this::clientMileageMatchingJob();

        $this::ExchangeRateAdd();
    }
    

       //10 초에 한번씩 환율 쌓기
    public static function ExchangeRateAdd(){
    
    $client = new \GuzzleHttp\Client();

    $response = $client->request('GET', 'https://api.coinone.co.kr/public/v2/trades/KRW/USDT?size=10', [
    'headers' => [
    'accept' => 'application/json',
    ],
        ]);


    $resultupper= $response->getBody();
    $resultdown = json_decode($resultupper, true);
    $standKRWUSDT=$resultdown['transactions'][0]['price'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.apilayer.net/api/convert?access_key=d718d2a9e721a02bae389ea3148b62e0&from=USD&to=KRW&amount=1',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
    
        // Decode JSON response:
        $conversionResult = json_decode($response, true);
        $conversionR=$conversionResult['result'];
        // access the conversion result
        $Num_stand=number_format($conversionResult['result'], 1);
        $stand=round($conversionResult['result'], 1);
    
        if($stand*1>500){
       
    
        $createTime=date("Y-m-d H:i:s");
        $apidb = static::GetApiDB();
        $apidbName= self::EbuyApiDBName;
    
    
        $insert=$apidb->prepare("INSERT INTO $apidbName.ExchangeRate_copy
                        (
                            standKRW,
                            sellKRWUSD,
                            buyKRWUSD,
                            createTime,
                            standKRWUSDT
                        )
                        VALUES
                        (
                            :standKRW,
                            :sellKRWUSD,
                            :buyKRWUSD,
                            :createTime,
                            :standKRWUSDT
                        )
                    ");
        $insert->bindValue(':standKRW', $stand);
        $insert->bindValue(':sellKRWUSD', $stand);
        $insert->bindValue(':buyKRWUSD', $stand);
        $insert->bindValue(':createTime', $createTime);
        $insert->bindValue(':standKRWUSDT', $standKRWUSDT);
        $insert->execute();

       }

    }

    



}



$dd=new SecJobTest();
