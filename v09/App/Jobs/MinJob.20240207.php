<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';



use PDO;
use \Core\View;
use \Core\Controller;
use \Core\DefinAll;
use \Core\GlobalsVariable;

use App\Models\SystemMo;
use App\Models\ExchangeRateMo;
use App\Models\MarketKeyMo;
use App\Models\EbuyBankLogMo;
use App\Models\ClientMileageDepositMo;
use App\Models\WithdrawalOldMo;
use App\Models\DepositOldMo;
use App\Models\StaffMo;





// use App\Models\OmcMenu_M;
/**
* Home controller
*
* PHP version 7.0
*/

class MinJob extends \Core\Controller
{
    public function __construct() {
        // $controller = new \Core\Controller();
        // $this::dDosUnblockAndInsBlacklist();
        // $this::dDosTxtCleaner();
        $this::loginSessionDelete();
        $this::ClientDepositAutoCancel();
        $this::ClientPinBlockUpdate();
        $this::WithdrawalOld();
        $this::DepositOld();
        $this::staffLoginDumpDelete();
        $this::appStaffDepositErrAlarm();
        $this::JobExchangeRate();
    }



    //수동신청보류인 상태인 애들 5분 지나면 모두 크론취소
    public static function ClientDepositAutoCancel(){
        $getTwoDaysAgoList=ClientMileageDepositMo::ClientMileageDepositAutoCancelList();
        $db = static::getDB();
        $dbName= self::MainDBName;
        $createTime=date("Y-m-d H:i:s");
        foreach ($getTwoDaysAgoList as $key) {

            $rn=$key['rn'];
            $targetIDX = $key['idx'];
            $statusIDX = $key['statusIDX'];
            $clientIDX = $key['clientIDX'];
            $createTime = $key['createTime'];
            $accountHolder = $key['accountHolder'];

            $logArr=[
                'createTime'=>$createTime,
                'accountHolder'=>$accountHolder,
            ];

            $getEbuyBankLogList=EbuyBankLogMo::GetTargetMinJobEbuyBankLog($logArr);
            if(!isset($getEbuyBankLogList['idx'])&&empty($getEbuyBankLogList['idx'])){
                if($statusIDX=='203201'||$statusIDX=='203202'||$statusIDX=='203203'){
                    $errMsg='이미 성공한 상태입니다.';
                    $errOn=$this::errExport($errMsg);
                }
                $updateStatusIDX = 203215;

                $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                    statusIDX=:updateStatusIDX,
                    completeTime=:createTime
                    WHERE idx=:targetIDX
                ");
                $stat1->bindValue(':updateStatusIDX', $updateStatusIDX);
                $stat1->bindValue(':createTime', $createTime);
                $stat1->bindValue(':targetIDX', $targetIDX);
                $stat1->execute();

                $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                    (statusIDX,targetIDX,clientIDX,createTime)
                    VALUES
                    (:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
                ");
                $stat2->bindValue(':updateStatusIDX', $updateStatusIDX);
                $stat2->bindValue(':targetIDX', $targetIDX);
                $stat2->bindValue(':clientIDX', $clientIDX);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->execute();

                //유저에게 알람을 쏴주자
                $alarmArr=[
                    'clientIDX'=>$clientIDX,
                    'statusIDX'=>$updateStatusIDX,
                    'targetIDX'=>$targetIDX,
                ];

                $AppMainIoUri= self::AppMainIoUri;
                $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                static::sendCurl($alarmArr,$AppMainIoAddr);
                // static::appPushAlarm($alarmArr);
                //스태프 입금신청 테이블 소켓
                static::socketTable($targetIDX);

                $scrParam=[
                    'rn'=>$rn,
                    'statusIDX'=>$updateStatusIDX,
                ];
                $AppMainIoAddr =$AppMainIoUri.'/rechargeResultAlarm';
                static::sendCurl($scrParam,$AppMainIoAddr);

                $dataPack2=ClientMileageDepositMo::tmpTargetData($targetIDX);
                if(isset($dataPack2['idx'])&&!empty($dataPack2['idx'])){
                    $migrationIDX=$dataPack2['migrationIDX'];
                    $amount=$dataPack2['amount'];
                    if($migrationIDX != 0){
                        //구바이에도 보내자
                        $field = 'https://partners-api.ebuycompany.com/MagApi/depositCancel';
                        $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount;
                        // $nextStepUri='https://partners-api.ebuycompany.com/MagApi/'.$fieldName;

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_REFERER, "$field");
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "$field",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "$postFields",
                            CURLOPT_HTTPHEADER => array(
                                "cache-control: no-cache",
                                "content-type: application/x-www-form-urlencoded"
                            ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                    }
                }
            }


        }

    }

    //5분 지난 Y 데이터들 N으로 업데이트
    public static function ClientPinBlockUpdate(){
        $db = static::getDB();
        $dbName= self::MainDBName;
        $stat1=$db->prepare("UPDATE $dbName.ClientPinBlock SET
            inBlock=:inBlock
            WHERE createTime < DATE_SUB(NOW(), INTERVAL 6 MINUTE)
        ");
        $stat1->bindValue(':inBlock', 'N');
        $stat1->execute();
    }

    //출금대행 구바이랑 뉴바이 비교
    public static function WithdrawalOld(){
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        //구바이 서버
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $date = date('Y-m-d H:i:s');
        $timestamp = strtotime($date . ' +9 hours');//한국시간으로
        $timestamp = $timestamp - (2 * 60); // 2분 빼기
        $kst = date('Y-m-d H:i:s', $timestamp);


        // 일단 구바이 출금대행 목록을 가져오자
        $query = $oldDb->prepare("SELECT
            APR_ID AS idx,
            APR_EB_USER_NAME AS name,
            APR_AGENT_M_CODE AS marketCode,
            APR_EB_USER_CHAM_ID AS refereceId,
            APR_SUC_QTY_TOTAL AS amount,
            partnerOrderId AS invoiceId,
            APR_SUC_REG AS createTime
            FROM $oldDbName.AA_AGNET_P_REFUND
            WHERE APR_STAT = 2 AND (APR_SUC_REG BETWEEN '2024-01-22 10:00:00' AND '$kst')
        ");
        $query->execute();
        $resultTable=$query->fetchAll(PDO::FETCH_ASSOC);

        // 구바이 데이터를 돌려서 하나씩 꺼내옴
        foreach ($resultTable as $key) {
            $idx = $key['idx'];
            $name = $key['name'];
            $marketCode = $key['marketCode'];
            $refereceId = $key['refereceId'];
            $amount = $key['amount'];
            $invoiceId = $key['invoiceId'];
            $createTime = $key['createTime'];

            $targetData=WithdrawalOldMo::GetTargetData($idx);
            //받아온 oldIDX가 있으면 인서트하고 없으면 하지마라
            if(!isset($targetData['oldIDX'])&&empty($targetData['oldIDX'])){
                //차라리 여기서 처음부터 인서트 시키자
                $stat2 = $db->prepare("INSERT INTO $dbName.WithdrawalOld
                    (code, name, refereceId, amount, invoiceId, createTime, status, oldIDX)
                    VALUES
                    (:code, :name, :refereceId, :amount, :invoiceId, :createTime, :status, :oldIDX)
                ");
                $stat2->bindValue(':code', $marketCode);
                $stat2->bindValue(':name', $name);
                $stat2->bindValue(':refereceId', $refereceId);
                $stat2->bindValue(':amount', $amount);
                $stat2->bindValue(':invoiceId', $invoiceId);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':status', 1);
                $stat2->bindValue(':oldIDX', $idx);
                $stat2->execute();
            }

            //가져온값들로 조사해보자
            $paramArr=[
                'marketCode'=>$marketCode,
                'refereceId'=>$refereceId,
                'amount'=>$amount,
                'invoiceId'=>$invoiceId,
            ];

            // 4개의 파람값 중복 데이터가 있는지 조사 (뉴바이)
            $newData=WithdrawalOldMo::GetIssetNewDatatableList($paramArr);
            if(isset($newData['idx'])&&!empty($newData['idx'])){
                $newDataMarketCode = $newData['marketCode'];
                $newDataMarketUserEmail = $newData['marketUserEmail'];
                $newDataAmount = $newData['amount'];
                $newDataInvoiceID = $newData['invoiceID'];

                if ($newDataMarketCode == $marketCode &&
                    $newDataMarketUserEmail == $refereceId &&
                    $newDataAmount == $amount &&
                    $newDataInvoiceID == $invoiceId) {

                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET
                        status=3
                        WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->execute();
                }

                if($newDataMarketCode != $marketCode){
                    $memo = 'Job : 마켓코드가 맞지 않습니다. 뉴바이코드 : '.$newDataMarketCode .' / 구바이코드 : '.$marketCode;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataMarketUserEmail != $refereceId){
                    $memo = 'Job : 참조이메일이 맞지 않습니다. 뉴바이참ID : '.$newDataMarketUserEmail .' / 구바이참ID : '.$refereceId;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataAmount != $amount){
                    $memo = 'Job : 금액이 맞지 않습니다. 뉴바이금액 : '.$newDataAmount .' / 구바이금액 : '.$amount;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataInvoiceID != $invoiceId){
                    $memo = 'Job : InvoiceID가 맞지 않습니다. 뉴바이인ID : '.$newDataInvoiceID .' / 구바이인ID : '.$invoiceId;
                    $stat1 = $db->prepare("UPDATE $dbName.WithdrawalOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }
            }
        }


    }

    //지불대행 구바이랑 뉴바이 비교
    public static function DepositOld(){
        $db = static::GetApiDB();
        $dbName= self::EbuyApiDBName;
        //구바이 서버
        $oldDb = static::GetOldDB();
        $oldDbName= self::EbuyOldDBName;

        $date = date('Y-m-d H:i:s');
        $timestamp = strtotime($date . ' +9 hours');
        $timestamp = $timestamp - (2 * 60); // 2분 빼기
        $kst = date('Y-m-d H:i:s', $timestamp);


        // 일단 구바이 출금대행 목록을 가져오자
        $query = $oldDb->prepare("SELECT
            APH_ID AS idx,
            APH_NAME AS name,
            APH_AGENT_M_CODE AS marketCode,
            APH_AGENT_USER_ID AS refereceId,
            APH_APPLY_QTY AS amount,
            partnerOrderId AS invoiceId,
            APH_SUC_P_ADM_REG AS createTime
            FROM $oldDbName.AA_AGENT_PARTNER_HIS
            WHERE APH_STAT = 2 AND (APH_SUC_P_ADM_REG BETWEEN '2024-01-22 10:00:00' AND '$kst')
        ");
        $query->execute();
        $resultTable=$query->fetchAll(PDO::FETCH_ASSOC);

        // 구바이 데이터를 돌려서 하나씩 꺼내옴
        foreach ($resultTable as $key) {
            $idx = $key['idx'];
            $name = $key['name'];
            $marketCode = $key['marketCode'];
            $refereceId = $key['refereceId'];
            $amount = $key['amount'];
            $invoiceId = $key['invoiceId'];
            $createTime = $key['createTime'];

            $oldArr=[
                'idx'=>$idx,
                'createTime'=>$createTime,
            ];

            $targetData=DepositOldMo::GetTargetData($oldArr);
            //받아온 oldIDX가 있으면 인서트하고 없으면 하지마라
            if(!isset($targetData['oldIDX'])&&empty($targetData['oldIDX'])){
                //차라리 여기서 처음부터 인서트 시키자
                $stat2 = $db->prepare("INSERT INTO $dbName.DepositOld
                    (code, name, refereceId, amount, invoiceId, createTime, status, oldIDX)
                    VALUES
                    (:code, :name, :refereceId, :amount, :invoiceId, :createTime, :status, :oldIDX)
                ");
                $stat2->bindValue(':code', $marketCode);
                $stat2->bindValue(':name', $name);
                $stat2->bindValue(':refereceId', $refereceId);
                $stat2->bindValue(':amount', $amount);
                $stat2->bindValue(':invoiceId', $invoiceId);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':status', 1);
                $stat2->bindValue(':oldIDX', $idx);
                $stat2->execute();
            }

            //가져온값들로 조사해보자
            $paramArr=[
                'marketCode'=>$marketCode,
                'refereceId'=>$refereceId,
                'amount'=>$amount,
                'invoiceId'=>$invoiceId,
            ];

            // 4개의 파람값 중복 데이터가 있는지 조사 (뉴바이)
            $newData=DepositOldMo::GetIssetNewDatatableList($paramArr);

            if(isset($newData['idx'])&&!empty($newData['idx'])){
                $newDataMarketCode = $newData['marketCode'];
                $newDataMarketUserEmail = $newData['marketUserEmail'];
                $newDataAmount = $newData['amount'];
                $newDataInvoiceID = $newData['invoiceID'];

                if ($newDataMarketCode == $marketCode &&
                    $newDataMarketUserEmail == $refereceId &&
                    $newDataAmount == $amount &&
                    $newDataInvoiceID == $invoiceId) {

                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET
                        status=3
                        WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->execute();
                }

                if($newDataMarketCode != $marketCode){
                    $memo = 'Job : 마켓코드가 맞지 않습니다. 뉴바이코드 : '.$newDataMarketCode .' / 구바이코드 : '.$marketCode;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataMarketUserEmail != $refereceId){
                    $memo = 'Job : 참조이메일이 맞지 않습니다. 뉴바이참ID : '.$newDataMarketUserEmail .' / 구바이참ID : '.$refereceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataAmount != $amount){
                    $memo = 'Job : 금액이 맞지 않습니다. 뉴바이금액 : '.$newDataAmount .' / 구바이금액 : '.$amount;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }

                if($newDataInvoiceID != $invoiceId){
                    $memo = 'Job : InvoiceID가 맞지 않습니다. 뉴바이인ID : '.$newDataInvoiceID .' / 구바이인ID : '.$invoiceId;
                    $stat1 = $db->prepare("UPDATE $dbName.DepositOld SET status = 4,memo = :memo WHERE oldIDX=:idx
                    ");
                    $stat1->bindValue(':idx', $idx);
                    $stat1->bindValue(':memo', $memo);
                    $stat1->execute();
                }
            }
        }

    }


    public static function dDosUnblockAndInsBlacklist() {
        $blockFile=DefinAll::dDosBlockFile;//블랙리스트 등록파일
        if (!file_exists($blockFile)){
            return false;
        }

        $nowReg=date("Y-m-d H:i:s");
        $time1 = strtotime($nowReg);

        $handle = fopen($blockFile, 'r'); // 파일을 추가 모드로 엽니다.
        if (!$handle) {  return false;  } //파일열기 실패
        $filesize = filesize($blockFile); // 파일의 크기 가져오기
        if($filesize>0){
            $blockFileCon = fread($handle, $filesize); // 파일 내용을 읽어옵니다.
            $pattern = '/,(\s*)$/'; // 정규식 패턴
            $blockFileCon = preg_replace($pattern, '$1', $blockFileCon); // 콤마 제거
            $blockFileCon = explode(',',$blockFileCon);
            $content = file_get_contents($blockFile);
            $newContent = $content;
            foreach ($blockFileCon as $key => $value) {
                // $txtIp = explode('||',$value)[0]; // 텍스트파일의 ip
                $txtReg = explode('||',$value)[1]; //텍스트파일의 reg

                $time2 = strtotime($txtReg);
                $diffInSeconds = $time1 - $time2;
                $diffInMinutes = floor($diffInSeconds / 60);
                if($diffInMinutes>DefinAll::dDosReleaseTime){//n분이 지났으면 풀어주자.
                    // 파일 읽기
                    $newContent = str_replace($value.',', '', $newContent);
                    // $db = static::getDB();
                    // $QrStringIst = $db->query("INSERT INTO sendipay.Ban SET
                    //     ipAddress='$txtIp',
                    //     createTime='$nowReg',
                    //     `from`='staff'
                    // ");
                }
            }
            file_put_contents($blockFile, $newContent);
            fclose($handle); // 파일을 닫습니다.
        }
    }



    public static function dDosTxtCleaner() {
        $dDosFile=DefinAll::dDosIpRegFile;
        if (!file_exists($dDosFile)){
            return false;
        }

        $nowReg=date("Y-m-d H:i:s");
        $time1 = strtotime($nowReg);
        $handle = fopen($dDosFile, 'r'); // 파일을 추가 모드로 엽니다.
        if (!$handle) {  return false;  } //파일열기 실패
        $filesize = filesize($dDosFile); // 파일의 크기 가져오기
        if($filesize>0){
            $dDosFileCon = fread($handle, $filesize); // 파일 내용을 읽어옵니다.
            $pattern = '/,(\s*)$/'; // 정규식 패턴
            $dDosFileCon = preg_replace($pattern, '$1', $dDosFileCon); // 콤마 제거
            $dDosFileCon = explode(',',$dDosFileCon);
            $content = file_get_contents($dDosFile);
            $newContent = $content;
            foreach ($dDosFileCon as $key => $value) {
                $txtIp = explode('||',$value)[0]; // 텍스트파일의 ip
                $txtReg = explode('||',$value)[1]; //텍스트파일의 reg
                $time2 = strtotime($txtReg);
                $diffInSeconds = $time1 - $time2; //현재시간이 무조건 길지는 않을것(24:00:00 와 01:00:01 같은경우)
                $diffInMinutes = floor($diffInSeconds / 60);
                if($diffInMinutes>DefinAll::dDosFileCleanTime){//7분이 지나면 삭제
                    // 파일 읽기
                    $newContent = str_replace($value.',', '', $newContent);
                }
            }
            file_put_contents($dDosFile, $newContent);
            fclose($handle); // 파일을 닫습니다.
        }
    }

    public static function loginSessionDelete() {
        $loginTime=DefinAll::LoginTime;
        $loginKey=DefinAll::LoginKey;

        $sessionDir = '/var/lib/php/session';
        // 디렉토리 내의 파일 목록을 가져옵니다.

        $files = scandir($sessionDir);
        if ($files === false) {
            return false;
        }

        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                $filePath = $sessionDir . '/' . $file;
                $createTime = filectime($filePath);
                $currentDateTime = new \DateTime();
                $sessionDateTime = new \DateTime();
                $sessionDateTime->setTimestamp($createTime);
                $interval = $currentDateTime->diff($sessionDateTime);

                $years = $interval->y;
                $months = $interval->m;
                $days = $interval->d;
                $hours = $interval->h;
                $minutes = $interval->i;
                $seconds = $interval->s;
                // 총 분으로 계산
                $gapTimeMin = ($years * 525600) + ($months * 43800) + ($days * 1440) + ($hours * 60) + $minutes;
                if($gapTimeMin>=$loginTime){
                    unlink($filePath);
                }
            }
        }
    }



    public static function staffLoginDumpDelete() {
        //로그인시 발급되는 코드 지우기
        $db     = static::getDB();
        $dbName = self::MainDBName;
        $stat2 = $db->prepare("DELETE FROM $dbName.StaffLoginDump
            WHERE createTime < DATE_SUB(NOW(), INTERVAL 6 MINUTE)");
        $stat2->execute();
    }

    //1분에 한번씩 환율쏘기 (스태프에 쏨)
    public static function JobExchangeRate(){
        $standKRWresult=ExchangeRateMo::ExchangeRate();
        if(!isset($standKRWresult['standKRW'])||empty($standKRWresult['standKRW'])){
            $errMsg='standKRW 정보가 없습니다.';
            exit();
        }
        $standKRWresultone=$standKRWresult['standKRW'];
        $DomainUri=self::DomainUri;
        $staffUri = $DomainUri.':16001/JobExchangeRate';
        $dataPack=['data'=>$standKRWresultone];
        $result=self::curlSend($dataPack,$staffUri);
    }



     //앱 스태프 입금이상 알람용
    public static function appStaffDepositErrAlarm($data=null)
    {
        $alarmData=ClientMileageDepositMo::ClientMileageDepositStaffAlarmList();

        $db = static::getDB();
        $dbName= self::MainDBName;

        foreach ($alarmData as $key) {
            $targetIDX = $key['idx'];
            $clientName = $key['name'];

            $stat1=$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                appStaffAlarm=:appStaffAlarm
                WHERE idx=:idx
            ");
            $stat1->bindValue(':appStaffAlarm', 'Y');
            $stat1->bindValue(':idx', $targetIDX);
            $stat1->execute();

            $staffData=StaffMo::GetAppStaffAlarmList();
            foreach ($staffData as $key2) {
                $staffClientIDX = $key2['clientIDX'];
                $staffStatusIDX = $key2['statusIDX'];

                if($staffStatusIDX == 302101){
                     //유저에게 알람을 쏴주자
                    $alarmArr=[
                        'clientIDX'=>$staffClientIDX,
                        'statusIDX'=>102201,
                        'targetIDX'=>$targetIDX,
                        'param'=>['clientName'=>$clientName]
                    ];
                    $AppMainIoUri= self::AppMainIoUri;
                    $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                    static::sendCurl($alarmArr,$AppMainIoAddr);
                }
            }
        }
    }

}



$dd=new MinJob();
