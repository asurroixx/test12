<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';



use PDO;
use \Core\View;
use \Core\Controller;
use \Core\DefinAll;
use \Core\GlobalsVariable;

// use App\Models\SystemMo;

// use App\Models\OmcMenu_M;
/**
* Home controller
*
* PHP version 7.0
*/

class TwoMinJob extends \Core\Controller
{
	public function __construct() {
		$this::cashReceiptsIssue();
	}

	public function AESEncrypt($str)
	{
		// $iv = "LYL8luyXBo9g1Vb6";
		// $key = "C1vaiLCdzawpy4OrCxuvFojwYkAjeP4T";
		$iv = "OeKkc7062o4hMCV9";
		$key = "jqjlnRQH4HMSwaMBU5G6SpYicmda6p4U";
		return base64_encode(openssl_encrypt($str, "AES-256-CBC", $key, 5,  $iv));
	}
	public function AESDecrypt($str1)
	{
		// $iv = "LYL8luyXBo9g1Vb6";
		// $key = "C1vaiLCdzawpy4OrCxuvFojwYkAjeP4T";
		$iv = "OeKkc7062o4hMCV9";
		$key = "jqjlnRQH4HMSwaMBU5G6SpYicmda6p4U";
		return openssl_decrypt(base64_decode($str1), "AES-256-CBC", $key, 5,  $iv);
	}


	//현금영수증 발행
	public function cashReceiptsIssue(){

		$encryptedLogibpw=$this::AESEncrypt('dlqkdl5565@');
		$encryptedPw=$this::AESEncrypt('0925');
		$db     = static::GetApiDB();
		$dbName = self::EbuyApiDBName;
		$query = $db->prepare("SELECT
		idx
		,targetIDX
		,ReceiptAmt
		,clientIDX
		,clientTel
		,clientName
		,clientEmail
		,ReceiptTypeNo
		,TRAN_REQ_ID
		FROM $dbName.ClientCashReceipt
		WHERE statusIDX = 811201
		AND NOT EXISTS (
			SELECT 1
			FROM $dbName.ClientCashReceipt
			WHERE statusIDX = 811301
		)
		LIMIT 1
		");
		$query->execute();
		$sel=$query->fetch(PDO::FETCH_ASSOC);
		if(isset($sel['idx'])){
			$ClientCashReceiptIDX=$sel['idx'];
			$clientIDX=$sel['clientIDX'];

			$nowTime=date("Y-m-d H:i:s");
			$nextStatus=811301;
			$insEX='현금영수증 대기중 -> 발행중 상태로 업데이트되었습니다.';
			$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
				statusIDX = :changeStatus
				,resultTime = :tmpTime
				,result_msg = :tmpMsg
				WHERE idx = :targetIDX
			");
			$update->bindValue(':changeStatus', $nextStatus);
			$update->bindValue(':tmpTime', $nowTime);
			$update->bindValue(':tmpMsg', $insEX);
			$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
			$update->execute();
			if($clientIDX*1>1){
				$db = static::getDB();
				$dbName= self::MainDBName;
				$stat2=$db->prepare("INSERT INTO $dbName.ClientLog
					(statusIDX,targetIDX,clientIDX,createTime)
					VALUES
					(:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
				");
				$stat2->bindValue(':updateStatusIDX', $nextStatus);
				$stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
				$stat2->bindValue(':clientIDX', $clientIDX);
				$stat2->bindValue(':createTime', $nowTime);
				$stat2->execute();
				$ClientLogIDX=$db->lastInsertId();
				$stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
					(logIDX,ex)
					VALUES
					(:logIDX,:ex)
				");
				$stat3->bindValue(':logIDX', $ClientLogIDX);
				$stat3->bindValue(':ex', $insEX);
				$stat3->execute();
			}

			$targetIDX=$sel['targetIDX'];
			$ReceiptAmt=$sel['ReceiptAmt'];
			$clientTel=$sel['clientTel'];
			$clientName=$sel['clientName'];
			$clientEmail=$sel['clientEmail'];
			$ReceiptTypeNo=$sel['ReceiptTypeNo'];
			$TRAN_REQ_ID=$sel['TRAN_REQ_ID'];

			$ReceiptType='1';
			$GoodsName='거래 수수료';
			$ReceiptSubNum='1588800568';
			$ReceiptSubCoNm='이바이컴퍼니';
			$ReceiptSubBossNm='이바이컴퍼니';
			$ReceiptSubTel='15225565';
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://api.mydatahub.co.kr/kwichub/Cash/Issue',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => 'UTF-8',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS =>'{
					"ReceiptType" : "'.$ReceiptType.'",
					"GoodsName" : "'.$GoodsName.'",
					"ReceiptSubNum" : "'.$ReceiptSubNum.'",
					"ReceiptSubCoNm" : "'.$ReceiptSubCoNm.'",
					"ReceiptSubBossNm" : "'.$ReceiptSubBossNm.'",
					"ReceiptSubTel" : "'.$ReceiptSubTel.'",
					"ReceiptTypeNo" : "'.$ReceiptTypeNo.'",
					"ReceiptAmt" : "'.$ReceiptAmt.'",
					"TRAN_REQ_ID" : "'.$TRAN_REQ_ID.'",
					"BuyerEmail" : "'.$clientEmail.'",
					"BuyerTel" : "'.$clientTel.'",
					"BuyerName" : "'.$clientName.'"
				}',
				CURLOPT_HTTPHEADER => array(
					'Authorization: Token ab75a3f450024a119835de4352bc1fc60b5fdbd6',
					'Content-Type: application/json;charset=UTF-8'
				)
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$json=json_decode($response, true);
			$erro=$json["errMsg"];
			$receiveJsonData=json_encode($json);
			if($erro=="success"){
				$responseData=$json["data"];
				$res_ResultCode=$responseData["ResultCode"];
				$res_ResultMsg=$responseData["ResultMsg"];
				$res_ResultTID=$responseData["ResultTID"];
				if($res_ResultCode=='7001'){//정상!
					$nextStatus=811401;
				}else{// 7001 외 실패
					$nextStatus=811402;
				}
				$nowTime=date("Y-m-d H:i:s");
				$db     = static::GetApiDB();
				$dbName = self::EbuyApiDBName;
				$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
					statusIDX = :changeStatus
					,resultTime = :resultTime
					,result_TID = :result_TID
					,result_code = :result_code
					,result_msg = :result_msg
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $nextStatus);
				$update->bindValue(':resultTime', $nowTime);
				$update->bindValue(':result_TID', $res_ResultTID);
				$update->bindValue(':result_code', $res_ResultCode);
				$update->bindValue(':result_msg', $res_ResultMsg);
				$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
				$update->execute();
				if($clientIDX*1>1){
					$db = static::getDB();
					$dbName= self::MainDBName;
					$stat2=$db->prepare("INSERT INTO $dbName.ClientLog
						(statusIDX,targetIDX,clientIDX,createTime)
						VALUES
						(:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
					");
					$stat2->bindValue(':updateStatusIDX', $nextStatus);
					$stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
					$stat2->bindValue(':clientIDX', $clientIDX);
					$stat2->bindValue(':createTime', $nowTime);
					$stat2->execute();
					$ClientLogIDX=$db->lastInsertId();
					$stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
						(logIDX,ex)
						VALUES
						(:logIDX,:ex)
					");
					$stat3->bindValue(':logIDX', $ClientLogIDX);
					$stat3->bindValue(':ex', $res_ResultMsg);
					$stat3->execute();
				}
			}else{
				$nextStatus=811403;
				$nowTime=date("Y-m-d H:i:s");
				$db     = static::GetApiDB();
				$dbName = self::EbuyApiDBName;
				$update =$db->prepare("UPDATE $dbName.ClientCashReceipt SET
					statusIDX = :changeStatus
					,resultTime = :resultTime
					WHERE idx = :targetIDX
				");
				$update->bindValue(':changeStatus', $nextStatus);
				$update->bindValue(':resultTime', $nowTime);
				$update->bindValue(':targetIDX', $ClientCashReceiptIDX);
				$update->execute();
				if($clientIDX*1>1){
					$db = static::getDB();
					$dbName= self::MainDBName;
					$stat2=$db->prepare("INSERT INTO $dbName.ClientLog
						(statusIDX,targetIDX,clientIDX,createTime)
						VALUES
						(:updateStatusIDX,:targetIDX,:clientIDX,:createTime)
					");
					$stat2->bindValue(':updateStatusIDX', $nextStatus);
					$stat2->bindValue(':targetIDX', $ClientCashReceiptIDX);
					$stat2->bindValue(':clientIDX', $clientIDX);
					$stat2->bindValue(':createTime', $nowTime);
					$stat2->execute();
					$ClientLogIDX=$db->lastInsertId();
					$stat3=$db->prepare("INSERT INTO $dbName.ClientLogEx
						(logIDX,ex)
						VALUES
						(:logIDX,:ex)
					");
					$stat3->bindValue(':logIDX', $ClientLogIDX);
					$stat3->bindValue(':ex', $res_ResultMsg);
					$stat3->execute();
				}
			}
			$nowTime=date("Y-m-d H:i:s");
			$db     = static::GetApiDB();
			$dbName = self::EbuyApiDBName;
			$insert =$db->prepare("INSERT INTO $dbName.ClientCashReceiptFullData
				(
					ClientCashReceiptIDX,
					fullData,
					createTime
				)
				VALUES
				(
					:ClientCashReceiptIDX,
					:fullData,
					:createTime
				)
			");
			$insert->bindValue(':ClientCashReceiptIDX', $ClientCashReceiptIDX);
			$insert->bindValue(':fullData', $receiveJsonData);
			$insert->bindValue(':createTime', $nowTime);
			$insert->execute();
		}
	}
}



$dd=new TwoMinJob();