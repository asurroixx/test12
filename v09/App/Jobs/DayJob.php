<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';



use PDO;
use \Core\View;
use \Core\Controller;
use App\Models\MarketKeyMo;
use App\Models\MarketMo;
use App\Controllers\StaffEmailCon;
/**
* Home controller
*
* PHP version 7.0
*/

class DayJob extends \Core\Controller
{
    public function __construct() {
        $this->DeleteDayJob();
        $this::MarketKeyDeletionJob();
        $this::MarketKeyDeletionWarningJob();
        
    }



    public function DeleteDayJob($val=null)
    {
        $nowTime=date('Y-m-d H:i:s');
        $db = static::getDB();

        $stat1=$db->prepare("DELETE FROM ebuy.PageToken
            WHERE createTime < SUBDATE(NOW(), INTERVAL 3 DAY)
        ");
        $stat1->execute();

        //읽은건 15일 뒤에 삭제
        $stat2=$db->prepare("DELETE FROM ebuy.StaffAlarm
            WHERE createTime < SUBDATE(NOW(), INTERVAL 15 DAY) AND viewStatusIDX=305101
        ");
        $stat2->execute();

        //읽든 안읽든 30일 뒤면 삭제
        $stat3=$db->prepare("DELETE FROM ebuy.StaffAlarm
            WHERE createTime < SUBDATE(NOW(), INTERVAL 30 DAY)
        ");
        $stat3->execute();

        //qna 3일뒤에 자동종료
        $createTime = date("Y-m-d H:i:s");
        $db = static::getDB();
        $dbName = self::MainDBName;

        // 3일 지난 statusIDX가 340101인 행을 340301로 업데이트 (샌드박스는 자동종료 시키면 안됨)
        $stat4 = $db->prepare("UPDATE $dbName.Qna SET
            statusIDX=:statusIDX
            WHERE expectedTime < SUBDATE(NOW(), INTERVAL 3 DAY) AND statusIDX=340101 AND ticketCode NOT LIKE 'SA%'
        ");
        $stat4->bindValue(':statusIDX', 340301);
        $stat4->execute();
        $updatedRowCount = $stat4->rowCount();


        $updatedQnaIdxList = [];

        if ($updatedRowCount > 0) {
            // 340301인 행의 idx를 가져옴
            $stat5 = $db->prepare("SELECT
            idx
            FROM $dbName.Qna WHERE statusIDX = 340301 ");
            $stat5->execute();
            $updatedQnaIdxList = $stat5->fetchAll(PDO::FETCH_COLUMN);

            $content = '3 days after the answer was completed, it was automatically terminated automatically.';
            $staffContent = '답변 완료 후 3일이 지나 자동 종료 되었습니다.';

            foreach ($updatedQnaIdxList as $qnaIDX) {
                // 해당 Qna 레코드의 상태를 확인
                $statCheckStatus = $db->prepare("SELECT statusIDX FROM $dbName.Qna WHERE idx = :qnaIDX");
                $statCheckStatus->bindValue(':qnaIDX', $qnaIDX);
                $statCheckStatus->execute();
                $qnaStatus = $statCheckStatus->fetch(PDO::FETCH_COLUMN);


                if ($qnaStatus == 340301) {
                    // 이미 340301로 업데이트 된 경우에만 QnaAnswer에 insert
                    $stat6 = $db->prepare("INSERT INTO $dbName.QnaAnswer
                        (qnaIDX, content, staffContent, statusIDX, createTime, staffIDX)
                        VALUES
                        (:qnaIDX, :content, :staffContent, :statusIDX, :createTime, :staffIDX)
                    ");

                    $stat6->bindValue(':qnaIDX', $qnaIDX);
                    $stat6->bindValue(':content', $content);
                    $stat6->bindValue(':staffContent', $staffContent);
                    $stat6->bindValue(':statusIDX', 341103);
                    $stat6->bindValue(':createTime', $createTime);
                    $stat6->bindValue(':staffIDX', 0);
                    $stat6->execute();
                }

                //시스템 답변을 했으니 자동종료로 다시 바꿔줘
                $stat7 = $db->prepare("UPDATE $dbName.Qna SET
                    statusIDX=:statusIDX
                    WHERE idx=:qnaIDX
                ");
                $stat7->bindValue(':statusIDX', 140301);
                $stat7->bindValue(':qnaIDX', $qnaIDX);
                $stat7->execute();
            }
        }

        //qna 3일뒤에 자동종료 끝















        // $StaffAlarmDel = $db->query("DELETE FROM sendipay.StaffAlarm
        //     WHERE createTime < SUBDATE(NOW(), INTERVAL 30 DAY)
        // ");


        // $LoginDumpDel = $db->query("DELETE FROM sendipay.StaffLoginDump
        //     WHERE createTime < SUBDATE(NOW(), INTERVAL 30 DAY)
        // ");

    }

    //apikey 삭제 2일전 경고메일
    public static function MarketKeyDeletionWarningJob(){
        $type='twoDaysAgo';
        $getTwoDaysAgoList=MarketKeyMo::GetMarketKeyDeletionTime($type);
        foreach ($getTwoDaysAgoList as $key) {
            $idx=$key['idx'];
            $marketCode=$key['marketCode'];
            $deletionTime=$key['deletionTime'];
            $marketKey=$key['marketKey'];
            $getMarketIDX=MarketMo::GetMarketCode($marketCode);
            $marketIDX='';
            if(isset($getMarketIDX['idx'])||!empty($getMarketIDX['idx'])){
                $marketIDX=$getMarketIDX['idx'];
                $marketname=$getMarketIDX['name'];
                $oldstringLength = strlen($marketKey);
                $oldapi = str_repeat('*', $oldstringLength - 10) . substr($marketKey, -10);

                $getNewMarketKey=MarketKeyMo::GetMarketkKey($marketCode);
                //정상키 못가져오면 오류기때문에(삭제대기상태 마켓키가있음 정상키가 무조건 있어야함) 이메일 안보내는게 맞는거같음..
                if(isset($getNewMarketKey['idx'])&&!empty($getNewMarketKey['idx'])){
                    $newMarketKey=$getNewMarketKey['marketKey'];
                    $newstringLength = strlen($newMarketKey);
                    $newapi = str_repeat('*', $newstringLength - 10) . substr($newMarketKey, -10);

                    $oldstringLength = strlen($marketKey);
                    $oldapi = str_repeat('*', $oldstringLength - 10) . substr($marketKey, -10);
                    //이메일을 보내주자
                    $emailParamVal = [
                        'marketname'=>$marketname,
                        'oldapi'=>$oldapi,
                        'newapi'=>$newapi,
                        'deltime'=>$deletionTime,
                    ];
                    $emailParam=[
                        'targetIDX'=>$idx,
                        'statusIDX'=>139102,
                        'marketIDX'=>$marketIDX,
                        'param'=>$emailParamVal,
                        'type'=>'group'
                    ];
                    StaffEmailCon::sendEmailToPortal($emailParam);
                }
                //이메일을 보내주자
            }
        }
    }


    //2주 지난 삭제대기 마켓키 삭제
    public static function MarketKeyDeletionJob(){
        $type='day';
        $getList=MarketKeyMo::GetMarketKeyDeletionTime($type);
        $apidb = static::GetApiDB();
        $apidbName= self::EbuyApiDBName;
        $db = static::getDB();
        $dbName= self::MainDBName;
        $createTime=date("Y-m-d H:i:s");
        foreach ($getList as $key) {
            $idx=$key['idx'];
            $updateStatusIDX = 139101;//삭제상태
            $marketCode=$key['marketCode'];
            $marketKey=$key['marketKey'];

            $getMarketIDX=MarketMo::GetMarketCode($marketCode);
            if(isset($getMarketIDX['idx'])||!empty($getMarketIDX['idx'])){
                $marketIDX=$getMarketIDX['idx'];
                $marketname=$getMarketIDX['name'];

                $query = $apidb->prepare("UPDATE $apidbName.MarketKey SET
                    statusIDX=:statusIDX
                WHERE idx=:idx");
                $query->bindValue(':statusIDX',$updateStatusIDX);
                $query->bindValue(':idx', $idx);
                $query->execute();

                $stat2=$db->prepare("INSERT INTO $dbName.PortalLog
                    (statusIDX,targetIDX,createTime)
                    VALUES
                    (:statusIDX,:targetIDX,:createTime)
                ");
                $stat2->bindValue(':statusIDX', $updateStatusIDX);
                $stat2->bindValue(':targetIDX', $idx);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->execute();

                $getNewMarketKey=MarketKeyMo::GetMarketkKey($marketCode);
                //정상키 못가져오면 오류기때문에(삭제대기상태 마켓키가있음 정상키가 무조건 있어야함) 이메일 안보내는게 맞는거같음..
                if(isset($getNewMarketKey['idx'])&&!empty($getNewMarketKey['idx'])){
                    //이메일을 보내주자
                    $newMarketKey=$getNewMarketKey['marketKey'];
                    $newstringLength = strlen($newMarketKey);
                    $newapi = str_repeat('*', $newstringLength - 10) . substr($newMarketKey, -10);
    
                    $oldstringLength = strlen($marketKey);
                    $oldapi = str_repeat('*', $oldstringLength - 10) . substr($marketKey, -10);
                    //이메일을 보내주자
                    $emailParamVal = [
                        'marketname'=>$marketname,
                        'oldapi'=>$oldapi,
                        'newapi'=>$newapi,
                    ];
                    $emailParam=[
                        'targetIDX'=>$idx,
                        'statusIDX'=>$updateStatusIDX,
                        'marketIDX'=>$marketIDX,
                        'param'=>$emailParamVal,
                        'type'=>'group'
                    ];
                    StaffEmailCon::sendEmailToPortal($emailParam);
                }
            }
        }
    }

}



$dd=new DayJob();
