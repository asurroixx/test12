<?php
namespace App\Jobs;

require dirname(__DIR__) . '/../vendor/autoload.php';
require_once('/oplas/v09/Modules/Php/vendor/autoload.php');


use PDO;
use \Core\View;
use \Core\Controller;
use \Core\DefinAll;
use \Core\GlobalsVariable;
use App\Models\JobMo;
use App\Models\SystemMo;
use App\Models\MarketKeyMo;
use App\Models\EbuyBankLogMo;
use App\Models\ClientMileageDepositMo;
use App\Models\ExchangeRateMo;




// use App\Models\OmcMenu_M;
/**
* Home controller
*
* PHP version 7.0
*/

class SecJob extends \Core\Controller
{
    public function __construct() {
        // $controller = new \Core\Controller();
        // $this::dDosUnblockAndInsBlacklist();
        // $this::dDosTxtCleaner();
        

        // $this::clientMileageMatchingJob();



        $this::AutoMileageDepositMatching();
        $this::JobExchangeRate();
        $this::ExchangeRateAdd();
    }
    

    //입금신청 자동일 때 실제 은행 스크래핑테이블(EbuyBankLogMo)과 매칭하는 잡
    public static function AutoMileageDepositMatching(){
        //시스템에서 입금대기마감시간 가져오기
        $systemclosingTimeIDX=8;
        $getSystemclosingTime=SystemMo::GetSystemStatusInfo($systemclosingTimeIDX);
        if(!isset($getSystemclosingTime['idx'])&&empty($getSystemclosingTime['idx'])){
            $errMsg='systemclosingTimeIDX 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $systemclosingTime=$getSystemclosingTime['value'];
        //입금대기시간 + 뱅크로그잡도는시간2분 + 매칭잡도는시간1분
        //최대 오차 범위인 3분 더함
        $systemclosingTime=$systemclosingTime+3;

        //입금대기시간이 지난 자동입금신청건 가져오기
        $getAutoDepositList=ClientMileageDepositMo::MinJobAutoMileageDepositList($systemclosingTime);

        $db = static::getDB();
        $dbName= self::MainDBName;

        $apidb = static::GetApiDB();
        $apidbName= self::EbuyApiDBName;
        $createTime=date("Y-m-d H:i:s");

        //20240110 수동입금신청이 아닌 수동신청보류로 바꿔줌
        //$passiveStatusIDX=203101 > 203104

        //1. 자동입금신청 -> 수동입금신청으로 변경
        $passiveStatusIDX=203104;
        foreach ($getAutoDepositList as $key) {
            $autoDepositIDX=$key['idx'];
            $autoDepositClientIDX=$key['clientIDX'];
            $update =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                statusIDX = :passiveStatusIDX,
                completeTime = :completeTime
                WHERE idx = :autoDepositIDX
            ");
            $update->bindValue(':passiveStatusIDX', $passiveStatusIDX);
            $update->bindValue(':completeTime', $createTime);
            $update->bindValue(':autoDepositIDX', $autoDepositIDX);
            $update->execute();
            //로그엔 하나의 신청건에 자동과 수동이 둘 다 쌓여있음

            $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                VALUES
                (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
            ");
            $stat2->bindValue(':statusIDX', $passiveStatusIDX);
            $stat2->bindValue(':targetIDX', $autoDepositIDX);
            $stat2->bindValue(':clientIDX', $autoDepositClientIDX);
            $stat2->bindValue(':loginIDX', 0);
            $stat2->bindValue(':createTime', $createTime);
            $stat2->bindValue(':ipAddress', 0);
            $stat2->execute();

            static::socketTable($autoDepositIDX);
        }

        //2. 자동처리 시스템 상태 가져오기
        $systemMatchingIDX=10;
        $getSystemMatchingStatus=SystemMo::GetSystemStatusInfo($systemMatchingIDX);

        if(!isset($getSystemMatchingStatus['idx'])&&empty($getSystemMatchingStatus['idx'])){
            $errMsg='systemMatchingStatus 정보가 없습니다.';
            $errOn=$this::errExport($errMsg);
        }
        $systemMatchingStatus=$getSystemMatchingStatus['status'];

        // 시스템 실행잠금이면 나가라
        if($systemMatchingStatus==2){
            exit();
        }

        //3. EbuyBankLog테이블과 ClientMileageDeposit테이블 매칭시작
        //EbuyBankLog테이블에서 처리 전인 건들 가져오기
        $getEbuyBankLogList=EbuyBankLogMo::MinJobEbuyBankLogList();

        $ebuyBankLogStatusIDX=101102;
        foreach ($getEbuyBankLogList as $key) {
            $ebuyBankLogIDX=$key['idx'];
            $depositAmount=$key['depositAmount'];
            $ebuyBankIDX=$key['ebuyBankIDX'];
            $clientBankIDX=$key['clientBankIDX'];
            $clientAccountHolder=$key['clientAccountHolder'];
            $tradeTime=$key['tradeTime'];
            //농축협 농협은행으로 맞춤
            $clientBank=$key['clientBank'];
            if($clientBank == '농축협'){
                $clientBankIDX = 5; // 농협은행
            }

            
            //한번 읽은 EbuyBankLog데이터들은 매칭이 됐든 안됐든 처리완료 처리하기
            $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
                statusIDX = :ebuyBankLogStatusIDX
                WHERE idx = :ebuyBankLogIDX
            ");
            $update->bindValue(':ebuyBankLogStatusIDX', $ebuyBankLogStatusIDX);
            $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
            $update->execute();
            //스크래핑 업데이트 (소켓테이블보다 먼저되면 안됨)
            static::ScrapingData($ebuyBankLogIDX);

            //ClientMileageDeposit테이블 테이블조사하기
            // 입금한 계좌주 + 입금한 금액 + 입금한 은행 + (트레이드시간 > 신청시간)
            $matchingArr=[
                'depositAmount'=>$depositAmount,
                'ebuyBankIDX'=>$ebuyBankIDX,
                'clientBankIDX'=>$clientBankIDX,
                'clientAccountHolder'=>$clientAccountHolder,
                'tradeTime'=>$tradeTime
            ];
            $getAutoDepositMatchingData=ClientMileageDepositMo::MinJobAutoMileageDepositMatching($matchingArr);
            // 4. 매칭성공시 성공완료처리하기
            if(isset($getAutoDepositMatchingData['idx'])&&!empty($getAutoDepositMatchingData['idx'])){
                $matchingDepositIDX=$getAutoDepositMatchingData['idx'];
                $matchingDepositClientIDX=$getAutoDepositMatchingData['clientIDX'];
                $matchingDepositAmount=$getAutoDepositMatchingData['amount'];
                $migrationIDX=$getAutoDepositMatchingData['migrationIDX'];

                $amount = str_replace(',', '', $matchingDepositAmount); 
                $amountAsInt = intval($amount); 
                $matchingDepositStatusIDX=203202;
                //입금신청건 완료처리
                $depositUpdate =$db->prepare("UPDATE $dbName.ClientMileageDeposit SET
                    statusIDX = :matchingDepositStatusIDX,
                    completeTime = :completeTime
                    WHERE idx = :matchingDepositIDX
                ");
                $depositUpdate->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
                $depositUpdate->bindValue(':completeTime', $createTime);
                $depositUpdate->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $depositUpdate->execute();

                //마일리지테이블에 쌓기
                $insert=$db->prepare("INSERT INTO $dbName.ClientMileage
                    (
                        clientIDX,
                        statusIDX,
                        targetIDX,
                        amount,
                        createTime
                    )
                    VALUES
                    (
                        :matchingDepositClientIDX,
                        :matchingDepositStatusIDX,
                        :matchingDepositIDX,
                        :amountAsInt,
                        :createTime
                    )
                ");
                $insert->bindValue(':matchingDepositClientIDX', $matchingDepositClientIDX);
                $insert->bindValue(':matchingDepositStatusIDX', $matchingDepositStatusIDX);
                $insert->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $insert->bindValue(':amountAsInt', $amountAsInt);
                $insert->bindValue(':createTime', $createTime);
                $insert->execute();

                //EbuyBankLog테이블에 입금신청건IDX 업데이트
                $update =$apidb->prepare("UPDATE $apidbName.EbuyBankLog SET
                    targetIDX = :matchingDepositIDX
                    WHERE idx = :ebuyBankLogIDX
                ");
                $update->bindValue(':matchingDepositIDX', $matchingDepositIDX);
                $update->bindValue(':ebuyBankLogIDX', $ebuyBankLogIDX);
                $update->execute();

                $stat2=$db->prepare("INSERT INTO $dbName.ClientLog
                    (statusIDX,targetIDX,clientIDX,staffIDX,createTime,ip)
                    VALUES
                    (:statusIDX,:targetIDX,:clientIDX,:loginIDX,:createTime,:ipAddress)
                ");
                $stat2->bindValue(':statusIDX', $matchingDepositStatusIDX);
                $stat2->bindValue(':targetIDX', $matchingDepositIDX);
                $stat2->bindValue(':clientIDX', $matchingDepositClientIDX);
                $stat2->bindValue(':loginIDX', 0);
                $stat2->bindValue(':createTime', $createTime);
                $stat2->bindValue(':ipAddress', 0);
                $stat2->execute();
                static::socketTable($matchingDepositIDX);
                $alarmArr=[
                    'clientIDX'=>$matchingDepositClientIDX,
                    'statusIDX'=>$matchingDepositStatusIDX,
                    'targetIDX'=>$matchingDepositIDX,
                    'param'=>['amount'=>$amount]
                ];

                $AppMainIoUri= self::AppMainIoUri;
                $AppMainIoAddr =$AppMainIoUri.'/appPushAlarm';
                static::sendCurl($alarmArr,$AppMainIoAddr);

                if(isset($getAutoDepositMatchingData['rn'])&&!empty($getAutoDepositMatchingData['rn'])){
                    $rn=$getAutoDepositMatchingData['rn'];
                    $scrParam=[
                        'rn'=>$rn,
                        'statusIDX'=>$matchingDepositStatusIDX,
                    ];
                    $AppMainIoAddr =$AppMainIoUri.'/rechargeResultAlarm';
                    static::sendCurl($scrParam,$AppMainIoAddr);
                }



                // if($migrationIDX!=0){
                //     $field = 'https://partners-api.ebuycompany.com/MagApi/depositComplete';
                //     $postFields='migrationIDX='.$migrationIDX.'&amount='.$amount;
            
                //     $curl = curl_init();
                //     curl_setopt($curl, CURLOPT_REFERER, "$field");
                //     curl_setopt_array($curl, array(
                //         CURLOPT_URL => "$field",
                //         CURLOPT_RETURNTRANSFER => true,
                //         CURLOPT_ENCODING => "",
                //         CURLOPT_MAXREDIRS => 10,
                //         CURLOPT_TIMEOUT => 30,
                //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                //         CURLOPT_CUSTOMREQUEST => "POST",
                //         CURLOPT_POSTFIELDS => "$postFields",
                //         CURLOPT_HTTPHEADER => array(
                //             "cache-control: no-cache",
                //             "content-type: application/x-www-form-urlencoded"
                //         ),
                //     ));
                //     $response = curl_exec($curl);
                //     $err = curl_error($curl);
                //     curl_close($curl);
                // }



            }
        }
    }



    //10 초에 한번씩 환율쏘기
    public static function JobExchangeRate(){

        //20240319

        $standKRWresultone = '0';
        $getExchangeRateType = SystemMo::GetSystemStatusInfo(11);
        if (isset($getExchangeRateType['status'])) {
            $exchangeRateType = $getExchangeRateType['status'];

            $GetExchangeRate = ExchangeRateMo::ExchangeRate();
            if ($exchangeRateType == 2 || $exchangeRateType == 3) {
                if ($exchangeRateType == 2) {
                    $standKRWresultone = $GetExchangeRate['standKRW'];
                }elseif($exchangeRateType == 3){
                    $standKRWresultone = $GetExchangeRate['standKRWUSDT'];
                }
            } elseif ($exchangeRateType == 4) {
                $standKRWresultone = $getExchangeRateType['value'];
            }
        }

        //20240319 주석
        // $standKRWresult=JobMo::ExchangeRate();
        // if(!isset($standKRWresult['standKRW'])||empty($standKRWresult['standKRW'])){
        //     $errMsg='standKRW 정보가 없습니다.';
        //     exit();
        // }
        // $standKRWresultone=$standKRWresult['standKRW'];
        $AppIoUri=self::AppIoUri;
        $PortalIoUri=self::PortalIoUri;
        $AppIoUri=$AppIoUri."/JobExchangeRate";
        $PortalIoUri=$PortalIoUri."/JobExchangeRate";
        $dataPack=['data'=>$standKRWresultone];
        $result=self::curlSend($dataPack,$PortalIoUri);
        $result=self::curlSend($dataPack,$AppIoUri);
    }

       //10 초에 한번씩 환율 쌓기
    public static function ExchangeRateAdd(){
    
    $client = new \GuzzleHttp\Client();

    $response = $client->request('GET', 'https://api.coinone.co.kr/public/v2/trades/KRW/USDT?size=10', [
    'headers' => [
    'accept' => 'application/json',
    ],
        ]);


    $resultupper= $response->getBody();
    $resultdown = json_decode($resultupper, true);
    $standKRWUSDT=$resultdown['transactions'][0]['price'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.apilayer.net/api/convert?access_key=d718d2a9e721a02bae389ea3148b62e0&from=USD&to=KRW&amount=1',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
    
        // Decode JSON response:
        $conversionResult = json_decode($response, true);
        $conversionR=$conversionResult['result'];
        // access the conversion result
        $Num_stand=number_format($conversionResult['result'], 1);
        $stand=round($conversionResult['result'], 1);
    
        if($stand*1>500){
       
    
        $createTime=date("Y-m-d H:i:s");
        $apidb = static::GetApiDB();
        $apidbName= self::EbuyApiDBName;
    
    
        $insert=$apidb->prepare("INSERT INTO $apidbName.ExchangeRate
                        (
                            standKRW,
                            sellKRWUSD,
                            buyKRWUSD,
                            createTime,
                            standKRWUSDT
                        )
                        VALUES
                        (
                            :standKRW,
                            :sellKRWUSD,
                            :buyKRWUSD,
                            :createTime,
                            :standKRWUSDT
                        )
                    ");
        $insert->bindValue(':standKRW', $stand);
        $insert->bindValue(':sellKRWUSD', $stand);
        $insert->bindValue(':buyKRWUSD', $stand);
        $insert->bindValue(':createTime', $createTime);
        $insert->bindValue(':standKRWUSDT', $standKRWUSDT);
        $insert->execute();

       }

    }

    //구바이뉴바이회원마일리지히스토리매칭
    // public static function clientMileageMatchingJob(){

    //     $oldDB = static::GetOldDB();
    //     $newDB = static::GetDB();

    //     $now = new \DateTime('now', new \DateTimeZone('UTC'));
    //     $now->setTimezone(new \DateTimeZone('Asia/Seoul'));
    //     $nowInSeoul = $now->format('Y-m-d H:i:s');
    //     $createTime=date('Y-m-d H:i:s');


    //     $result1 = $oldDB->query("SELECT
    //         MH_ID AS targetIDX,
    //         MH_SRL AS migrationIDX,
    //         MH_MILEAGE AS amount,
    //         MH_ACTION,
    //         MH_REG AS time
    //     FROM ebuy.AA_MILEAGE_HIS
    //     WHERE MH_REG >= '{$nowInSeoul}' - INTERVAL 10 MINUTE
    //     ORDER BY MH_REG DESC
    //     ");
    //     $result1f=$result1->fetchAll(PDO::FETCH_ASSOC);

    //     foreach ($result1f as $key => $item) {
    //         $targetIDX=$item['targetIDX'];
    //         $migrationIDX=$item['migrationIDX'];
    //         $amount=$item['amount'];
    //         $MH_ACTION=$item['MH_ACTION'];
    //         $time=$item['time'];
    //         $koreaTimeZone = new \DateTimeZone('Asia/Seoul');
    //         $koreaDateTime = new \DateTime($time, $koreaTimeZone);
    //         $koreaDateTime->setTimezone(new \DateTimeZone('UTC'));
    //         $utcTime = $koreaDateTime->format('Y-m-d H:i:s');
    //         if($MH_ACTION==6){
    //             $statusIDX='203201,203202';
    //             }else if($MH_ACTION==20){
    //                 $statusIDX='204211,204212';
    //             }else if($MH_ACTION==5){
    //                 $statusIDX='204101,204102';
    //             }else if($MH_ACTION==8){
    //                 $statusIDX='205101';
    //             }else if($MH_ACTION==18){
    //                 $statusIDX='908102';
    //             }else if($MH_ACTION==7){
    //                 $statusIDX='205201';
    //             }else if($MH_ACTION==16){
    //                 $statusIDX='906101';
    //         }
    //         $result1f[$key]['statusIDX'] = $statusIDX;

    //         $result2 = $newDB->query("SELECT
    //             idx
    //         FROM ebuy.Client
    //         WHERE migrationIDX='$migrationIDX'
    //         ");
    //         $result2=$result2->fetch(PDO::FETCH_ASSOC);

    //         if(isset($result2['idx'])){
    //             $clientIDX=$result2['idx'];
    //             $result1f[$key]['clientIDX'] = $clientIDX;

    //             $result3 = $newDB->query("SELECT
    //                 idx,
    //                 migrationIDX,
    //                 statusIDX
    //             FROM ebuy.ClientMileage
    //             WHERE clientIDX='$clientIDX' AND statusIDX IN ('$statusIDX') AND amount = '$amount' AND createTime >= NOW() - INTERVAL 20 MINUTE;
    //             ");
    //             $result3=$result3->fetch(PDO::FETCH_ASSOC);

    //             if(isset($result3['idx'])){//뉴바이에있다!
    //                 $stat1=$newDB->prepare("
    //                     DELETE FROM ebuy.MileageHistoryMgErrorLog
    //                     WHERE targetIDX='$targetIDX'
    //                 ");
    //                 $stat1->execute();

    //             }else{//뉴바이에없엉

    //                 $result4 = $newDB->query("SELECT
    //                     idx
    //                 FROM ebuy.MileageHistoryMgErrorLog
    //                 WHERE targetIDX='$targetIDX'
    //                 ");
    //                 $result4=$result4->fetch(PDO::FETCH_ASSOC);
    //                 if(!isset($result4['idx'])){
    //                     $insertStmt = $newDB->prepare("INSERT INTO ebuy.MileageHistoryMgErrorLog (
    //                         clientIDX, 
    //                         statusIDX,
    //                         amount,
    //                         createTime,
    //                         targetIDX,
    //                         migrationIDX
    //                     )VALUES(
    //                         :clientIDX,
    //                         :statusIDX,
    //                         :amount,
    //                         :createTime,
    //                         :targetIDX,
    //                         :migrationIDX)
    //                     ");
    //                     $insertStmt->bindValue(':clientIDX', $clientIDX);
    //                     $insertStmt->bindValue(':amount', $amount);
    //                     $insertStmt->bindValue(':statusIDX', $statusIDX);
    //                     $insertStmt->bindValue(':createTime', $createTime);
    //                     $insertStmt->bindValue(':targetIDX', $targetIDX);
    //                     $insertStmt->bindValue(':migrationIDX', $migrationIDX);
    //                     $insertStmt->execute();
    //                 }


    //             }
    //         }

    //     }

    // }

       



}



$dd=new SecJob();
